package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.RenderedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;
//import org.sikuli.script.Pattern;
//import org.sikuli.script.Screen;

public class FNAAlbumPage extends LoadableComponent<FNAAlbumPage> {
	WebDriver driver;
	private boolean isPageLoaded;

	@FindBy(xpath = ".//*[@id='divFirstAlbum']/div/div/button")
	@CacheLookup
	WebElement createalbum;
	@FindBy(css = "#txtAlbumName")
	@CacheLookup
	WebElement EnterAlbumName;
	@FindBy(css = "#btnAlbumSave")
	@CacheLookup
	WebElement Savebtn;

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded = true;
		SkySiteUtils.waitTill(7000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, createalbum, 60);
		driver.switchTo().defaultContent();

	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FNAAlbumPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Method to generate album name Scripted By:Tarak
	 * 
	 * @return
	 */
	public String Random_Albumname() {

		String str = "Album";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * MEthod to generate random random rename of album
	 * 
	 * @return
	 */
	public String Random_Albumrename() {

		String str = "Realbum";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method to create random name for multiple album
	 * 
	 */
	public String Random_MultiAlbumname() {

		String str = "MultiAlbum";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method to check if any album exist
	 * 
	 */
	public void checkAnyAlbumExist() 
	{
		Log.message("Album Page Landed");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("swtiched into frame");
		SkySiteUtils.waitTill(2000);
		
		if (!(driver.findElement(By.xpath("//*[contains(text(),'You have no album')]")).isDisplayed())) 
		{
			Log.message("Already album exist");
			driver.findElement(By.xpath(".//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")).click();
			SkySiteUtils.waitTill(4000);
			Moreoption.click();
			Log.message("More option is clicked");
			
			SkySiteUtils.waitTill(4000);
			Removefolderbtn.click();
			Log.message("Remove folder button is clicked");
			SkySiteUtils.waitTill(4000);
			driver.switchTo().defaultContent();
			Log.message("Switching to default content");
			btnyes.click();
			Log.message("Yes button is clicked");
			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver, createalbum, 70);
			Log.message("Created album is deleted");

		} 
		else 
		{
			Log.message("No album exist ");
		}
	}

	/**
	 * Method to create album and verify the creation scripted by:Tarak
	 * 
	 */
	public boolean createAlbum(String albumname) {
		SkySiteUtils.waitTill(2000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("swtiched into frame");
		// SkySiteUtils.waitForElement(driver,createalbum,40);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", createalbum);
		Log.message("Create album button clicked");
		SkySiteUtils.waitTill(4000);
		EnterAlbumName.sendKeys(albumname);
		Log.message("Album name has been entered:");
		SkySiteUtils.waitTill(4000);
		Savebtn.click();
		Log.message("Save button has been clicked");
		SkySiteUtils.waitTill(5000);
		// Verify creation of album at collection level
		int Count_no = driver.findElements(By.xpath(
				"//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
				.size();
		Log.message("the number of album present are " + Count_no);
		SkySiteUtils.waitTill(5000);
		int k = 0;
		for (k = 1; k <= Count_no; k++) {

			String albumpresent = driver.findElement(By.xpath(
					"(//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
							+ k + "]"))
					.getText();

			if (albumpresent.contains(albumname)) {
				Log.message("Created album is present at " + k + " position in the tree");
				driver.findElement(By.xpath(
						"(.//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
								+ k + "]"))
						.click();
				Log.message("Created album is selected");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		String albumpresent1 = driver.findElement(By.xpath(
				"(.//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
						+ k + "]"))
				.getText();
		Log.message("Album created is " + albumpresent1);
		if (albumpresent1.contains(albumname))
			return true;
		else
			return false;

	}

	@FindBy(css = "#btnRemoveFolder")
	WebElement Removefolderbtn;
	@FindBy(css = ".btn.btn-primary.am-show-tooltip.test-tool")
	@CacheLookup
	WebElement Moreoption;
	@FindBy(xpath = "//*[@id='button-1']")

	WebElement btnyes;

	/**
	 * Method to delete the created album Scripted by :Tarak
	 */
	public void deleteAlbum() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("swtiched into frame");
		SkySiteUtils.waitTill(4000);
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		Removefolderbtn.click();
		Log.message("Remove folder button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");
		btnyes.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, createalbum, 70);
		Log.message("Created album is deleted");

	}

	@FindBy(xpath = ".//*[@id='dvAlbumImageContainer']/button")
	@CacheLookup
	WebElement uploadbtn;
	@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")

	WebElement btnselctFile;

	/**
	 * Method to upload photo to a album
	 * 
	 * @throws IOException
	 * 
	 */
	public boolean uploadPhoto(String albumname, String folderpath, String filename, String tempfilepath)
			throws IOException {
		// Log.message("upload photo to the album" +albumname);

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("swtiched into frame");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='dvAlbumImageContainer']/button")).click();
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = tempfilepath + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, false));

		String expFilename = null;
		File[] files = new File(folderpath).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + folderpath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(8000);
		driver.findElement(By.xpath("//button[@id='btnFileUpload']")).click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(8000);
		// String filename=PropertyReader.getProperty("Filename");
		// Log.message("File to be upload is " +filename);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(20000);
		String uploadedfilename = driver.findElement(By.xpath(".//*[@id='dvAlbumImageContainer']/div/div/span"))
				.getText();
		/* Check if the file is uploaded in the webpage */
		if (driver.findElement(By.xpath(".//*[@id='dvAlbumImageContainer']/div/div/a/img")).isDisplayed()
				&& uploadedfilename.contains(filename))
			return true;
		else
			return false;
	}

	@FindBy(xpath = ".//*[@id='button-0']")
	WebElement btnno;

	/**
	 * Method to verify album is deleted successfully scripted by : Tarak
	 */
	public boolean verifyDeletionOfAlbum(String albumname) {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("swtiched into frame");
		SkySiteUtils.waitTill(4000);
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		Removefolderbtn.click();
		Log.message("Remove folder button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");

		btnyes.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched into frame");
		int Count_no = driver.findElements(By.xpath(
				"//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
				.size();
		Log.message("the number of album present are " + Count_no);
		SkySiteUtils.waitTill(5000);
		if (Count_no == 0) {
			Log.message("Album is not present in the list anymore !!!!");
			return true;
		} else {

			Log.message("Album is present in the tree");
			return false;

		}
	}

	@FindBy(xpath = ".//*[@id='Div1']/nav/div[2]/div/ul/li[2]/a")
	@CacheLookup
	WebElement renamealbumoption;

	/**
	 * Method to rename album Scripted by :Tarak
	 */
	public boolean renameAlbum(String renamealbum) {
		Log.message("Renaming of added album");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("swtiched into frame");
		SkySiteUtils.waitTill(4000);
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		renamealbumoption.click();
		Log.message("Rename option is clicked");
		SkySiteUtils.waitTill(4000);
		EnterAlbumName.sendKeys(renamealbum);
		Log.message("Rename for the album is entered");
		SkySiteUtils.waitTill(4000);
		Savebtn.click();
		Log.message("Save button has been clicked");
		SkySiteUtils.waitTill(5000);
		int Count_no = driver.findElements(By.xpath(
				"//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
				.size();
		Log.message("the number of album present are " + Count_no);
		int k = 0;
		for (k = 1; k <= Count_no; k++) {

			String albumpresent = driver.findElement(By.xpath(
					"(//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
							+ k + "]"))
					.getText();

			if (albumpresent.contains(renamealbum)) {
				Log.message("Renamed album is present at " + k + " position in the tree");
				driver.findElement(By.xpath(
						"(.//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
								+ k + "]"))
						.click();
				Log.message("renamed album is selected");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		String albumpresent1 = driver.findElement(By.xpath(
				"(.//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
						+ k + "]"))
				.getText();
		Log.message("Renamed album is  " + albumpresent1);
		if (albumpresent1.contains(renamealbum))
			return true;
		else
			return false;

	}

	/**
	 * Method to delete all the files from download path
	 * 
	 */
	public boolean Delete_Files_From_Folder(String Folder_Path) {
		try {
			SkySiteUtils.waitTill(5000);
			Log.message("Cleaning download folder!!! ");
			File file = new File(Folder_Path);
			String[] myFiles;
			if (file.isDirectory()) {
				myFiles = file.list();
				for (int i = 0; i < myFiles.length; i++) {
					File myFile = new File(file, myFiles[i]);
					myFile.delete();
					SkySiteUtils.waitTill(5000);
				}
				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
		} // end try
		catch (Exception e) {
			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		}
		return false;
	}

	@FindBy(xpath = ".//*[@id='Div1']/nav/div[2]/div/ul/li[3]/a")
	@CacheLookup
	WebElement downloadmenu;

	/**
	 * Method to download Scripted by :Tarak
	 */
	public boolean downloadAlbum(String downloadpath, String albumname) {
		Log.message("Download the album");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		this.Delete_Files_From_Folder(downloadpath);
		SkySiteUtils.waitTill(4000);
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		downloadmenu.click();
		Log.message("Download menu is clicked");
		SkySiteUtils.waitTill(70000);
		driver.switchTo().defaultContent();
		if (this.isFileDownloaded(downloadpath, albumname) == true)
			return true;
		else
			return false;

	}

	/**
	 * Method to check if file is downloaded Scripted By : Tarak
	 * 
	 */
	public boolean isFileDownloaded(String downloadfolderpath, String albumname) {
		Log.message("Searching the downloaded album");
		String Filename = albumname + ".zip";
		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		SkySiteUtils.waitTill(5000);
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(Filename)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Method to check if file is downloaded Scripted By : Tarak
	 * 
	 */
	public boolean isFileDownloaded(String downloadfolderpath) {
		Log.message("Searching contact file downloaded");
		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(PropertyReader.getProperty("Filename"))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Method to check if file is downloaded Scripted By : Tarak
	 * 
	 */
	public boolean isFileDownloadedForCommunication(String downloadfolderpath, String filename) {
		Log.message("Searching  downloaded album ");
		SkySiteUtils.waitTill(4000);

		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		SkySiteUtils.waitTill(4000);
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(filename)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Method to delete file from folder Scripted by:Tarak
	 * 
	 * @param Folder_Path
	 * @return
	 */
	// Deleting files from a folder
	public boolean Delete_Files_From_Downloadfolder(String Folder_Path) {
		try {
			SkySiteUtils.waitTill(5000);
			Log.message("Cleaning download folder!!! ");
			File file = new File(Folder_Path);
			String[] myFiles;
			if (file.isDirectory()) {
				myFiles = file.list();
				for (int i = 0; i < myFiles.length; i++) {
					File myFile = new File(file, myFiles[i]);
					myFile.delete();
					SkySiteUtils.waitTill(5000);
				}
				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
		} // end try
		catch (Exception e) {
			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		}
		return false;
	}

	// @FindBy(xpath="//*[@value='Send via communication']")
	// @FindBy(id="Button3")
	// @FindBy(xpath="//*[@id='btnEmailClient']/following-sibling::button")
	@FindBy(xpath = "//*[text()='Send via communication']")
	WebElement SendViaCommunication;
	@FindBy(css = "#btnOpenShareView")
    WebElement sharebuttn;
	
	@FindBy(xpath = ".//*[@title='Create link of selected photos']")
    WebElement sharelinkbuttn;
	
	@FindBy(xpath = "//*[@id='tinymce']/a/img")
    WebElement image;


	@FindBy(xpath = "//input[@id='lblToReceipients']")
	WebElement Tofield;
	
	@FindBy(xpath = "//button[@id='btnToReceipients']")
	WebElement PlusbtnTofield;

	@FindBy(xpath = "//input[@id='chkAll']")
	WebElement CheckAll;
	
	@FindBy(xpath = "//input[@value='Save & Close']")
	WebElement SaveandClosebtn;
	
	@FindBy(xpath = "//input[@id='txtSubject' and  @onkeydown='return PreventEnterKey(event);']")
	WebElement Subjectfield;
	
	@FindBy(xpath = "//input[@value='Save & send']")
	WebElement SaveandSendbutton;
	
	
	
	/**
	 * Method to share via communication Scripted By : Tarak
	 * 
	 * @throws AWTException
	 * Modified by Trinanjwan | 21-Nov-2018
	 */
	public void shareViaCommunication(String downloadpath, String downloadalbumname, String SubjectCom) throws AWTException {
		this.Delete_Files_From_Downloadfolder(downloadpath);
		SkySiteUtils.waitTill(4000);
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		sharealbumoption.click();
		Log.message("Share album option is clicked");
		SkySiteUtils.waitTill(4000);
		// SkySiteUtils.waitTill(7000);
		JavascriptExecutor executor2 = (JavascriptExecutor) driver;
		executor2.executeScript("arguments[0].click();", SendViaCommunication);
        Log.message("Send via communication is clicked");
        SkySiteUtils.waitTill(10000);
        driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver, Tofield, 30);
		Log.message("Communication page is landed successfully");
		SkySiteUtils.waitForElement(driver, PlusbtnTofield, 30);
		Log.message("Plus button is visibled now");
		PlusbtnTofield.click();
		Log.message("Clicked on Plus button");

		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, CheckAll, 30);
		Log.message("Check box button is visibled now");
		executor2.executeScript("arguments[0].click();", CheckAll);
		//CheckAll.click();
		Log.message("Clicked on Check All button");
		
		
		
		
		SkySiteUtils.waitForElement(driver, SaveandClosebtn, 30);
		Log.message("Save and Close button is visibled now");
		SaveandClosebtn.click();
		Log.message("Clicked on Save and Close button");
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		
		
		SkySiteUtils.waitForElement(driver, Subjectfield, 30);
		Log.message("Subject field is visibled now");
		
		Subjectfield.sendKeys(SubjectCom);
		Log.message("Subject send is: "+SubjectCom);
		
		SkySiteUtils.waitForElement(driver, SaveandSendbutton, 30);
		Log.message("Save and Send button is visibled now");
		SaveandSendbutton.click();
		Log.message("Clicked on Save and Send button");
		
		SkySiteUtils.waitTill(20000);
		
		/**		
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitForElement(driver, Tofield, 120);
		Log.message("To field is visible now and the communication page is landed successfully");
		
		
	
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		// SkySiteUtils.waitTill(5000);

		WebElement R1 = driver.findElement(By.xpath("//*[@id='tinymce']/p/a"));
		Actions builder = new Actions(driver);
		Log.message("selete the path frame");
		SkySiteUtils.waitTill(10000);
		builder.click(R1).contextClick(R1).sendKeys(R1, Keys.ARROW_DOWN).build().perform();
		Log.message("Image clicked and context click down");
		SkySiteUtils.waitTill(10000);
		// SkySiteUtils.waitTill(10000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_DOWN);
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_DOWN);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Log.message("image opened in new tab");
		SkySiteUtils.waitTill(9000);
		if (this.isFileDownloadedForCommunication(downloadpath, downloadalbumname) == true) {
			Log.message("File is downloaded in the folder");
			**/

	}

	/**
	 * method to delete multiple album
	 * 
	 */
	public void deleteMultipleAlbum(String albumname) {
		// Verify creation of album at collection level
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		int Count_no = driver.findElements(By.xpath(
				"//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
				.size();
		Log.message("the number of album present are " + Count_no);
		SkySiteUtils.waitTill(5000);
		int k = 0;
		for (k = 1; k <= Count_no; k++) {

			String albumpresent = driver.findElement(By.xpath(
					"(//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
							+ k + "]"))
					.getText();

			if (albumpresent.contains(albumname)) {
				Log.message("Created album is present at " + k + " position in the tree");
				driver.findElement(By.xpath(
						"(.//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
								+ k + "]"))
						.click();
				Log.message("Created album is selected");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		Removefolderbtn.click();
		Log.message("Remove folder button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");
		btnyes.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, createalbum, 70);
		Log.message("Created album is deleted");
	}

	@FindBy(xpath = ".//*[@id='Div1']/nav/div[1]/div/button[2]")
	@CacheLookup
	WebElement managealbumbtn;
	@FindBy(xpath = "//*[@title='Update Other Information']")
	@CacheLookup
	WebElement updateicon;
	@FindBy(xpath = ".//*[@id='txt_title']")
	@CacheLookup
	WebElement updatetitle;
	@FindBy(xpath = ".//*[@id='txt_desc']")
	@CacheLookup
	WebElement updatedescription;
	@FindBy(xpath = ".//*[@id='txt_tags']")
	@CacheLookup
	WebElement updatetags;
	@FindBy(xpath = ".//*[@id='div_otherInfoTable']/div[3]/button[1]")
	@CacheLookup
	WebElement updatebtn;
	@FindBy(xpath = ".//*[@id='div_otherInfoTable']/div[3]/button[2]")

	WebElement cancelbtn;
	@FindBy(xpath = ".//*[@id='albumOperation']/div/div/div[3]/button[3]")
	@CacheLookup
	WebElement closebtn;

	/**
	 * Method to manage album for updating contact scripted by :Tarak
	 */
	public boolean manageAlbumUpdateInfo() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		managealbumbtn.click();
		Log.message("Manage album button is clicked");
		SkySiteUtils.waitTill(5000);
		updateicon.click();
		Log.message("Update icon is clicked");
		SkySiteUtils.waitTill(5000);
		String titlename = PropertyReader.getProperty("titlename");
		updatetitle.clear();
		Log.message("update title box cleared");
		updatetitle.sendKeys(titlename);
		Log.message("Updated title is " + titlename);
		SkySiteUtils.waitTill(5000);
		String descriptionimage = PropertyReader.getProperty("description");
		updatedescription.sendKeys(descriptionimage);
		Log.message("Updated description is " + descriptionimage);
		SkySiteUtils.waitTill(5000);
		String tagname = PropertyReader.getProperty("tag");
		updatetags.sendKeys(tagname);
		Log.message("Updated tag is " + tagname);
		SkySiteUtils.waitTill(4000);
		updatebtn.click();
		Log.message("Update button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@title='Update Other Information']")).click();
		Log.message("update icon is clicked");
		String title_entered = updatetitle.getAttribute("value");
		Log.message("title entered is " + title_entered);
		SkySiteUtils.waitTill(2000);
		String desc_entered = updatedescription.getAttribute("value");
		Log.message("desc entered is " + desc_entered);
		SkySiteUtils.waitTill(5000);
		String tagentered = updatetags.getAttribute("value");
		Log.message("tag entered is " + tagentered);
		cancelbtn.click();
		Log.message("Cancel button is clicked");
		SkySiteUtils.waitTill(4000);
		closebtn.click();
		Log.message("Close button is clicked");
		SkySiteUtils.waitTill(4000);
		if (title_entered.contains(titlename) && desc_entered.contains(descriptionimage)
				&& tagentered.contains(tagname))
			return true;
		else
			return false;

	}

	@FindBy(xpath = "//*[@type='checkbox']")
	WebElement image1;
	@FindBy(xpath = ".//*[@id='Button1']")
	@CacheLookup
	WebElement deletephoto;
	@FindBy(xpath = ".//*[@id='dvAlbumImageContainer']/button")
	@CacheLookup
	WebElement uploadbtndlbumcontainer;

	@FindBy(xpath = "//h2[text()='No photo available']")
	@CacheLookup
	WebElement photoerror;

	/**
	 * Method to delete image using manage album option Scripted By:Tarak Modified
	 * By trinanjwan on 16-11-2018 | photoerror message validation is removed.
	 */
	public boolean deleteImageManageAlbumOption() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		managealbumbtn.click();
		Log.message("Manage album button is clicked");
		SkySiteUtils.waitTill(5000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", image1);
		Log.message("image checkbox is ticked");
		SkySiteUtils.waitTill(4000);
		deletephoto.click();
		Log.message("Delete photo button is clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		btnyes.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		closebtn.click();
		Log.message("Close button is clicked");
		SkySiteUtils.waitTill(4000);
		if (uploadbtndlbumcontainer.isDisplayed()) {
			Log.message("uploaded image of the album is deleted successfully");
			return true;
		} else {
			return false;

		}
	}

	@FindBy(xpath = ".//*[@id='btnAddFolder']")
	WebElement createAlbum;

	/**
	 * Method to add multiple album scripted by :Tarak
	 * 
	 */
	public void addMultipleAlbum(String multialbumname) {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		driver.findElement(
				By.xpath(".//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span"))
				.click();
		Log.message("Collection is selected ");
		SkySiteUtils.waitTill(3000);
		createAlbum.click();
		Log.message("Create album button is clicked");
		SkySiteUtils.waitTill(4000);
		EnterAlbumName.sendKeys(multialbumname);
		Log.message("Album name has been entered:");
		SkySiteUtils.waitTill(4000);
		Savebtn.click();
		Log.message("Save button has been clicked");
		SkySiteUtils.waitTill(5000);
	}

	/**
	 * Method to navigate to any particular album from project level
	 * 
	 */
	public boolean navigateToAlbumFromCollection(String albumname) {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		driver.findElement(
				By.xpath(".//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span"))
				.click();
		Log.message("Collection is selected ");
		SkySiteUtils.waitTill(3000);
		int Count_list = driver.findElements(By.xpath("//*[@id='dvAlbumContainer']/div/div/a")).size();
		Log.message("total album in the collection are :" + Count_list);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= Count_list; i++) {

			String albumnamefound = driver
					.findElement(By.xpath("(//*[@id='dvAlbumContainer']/div/div/a/span)[" + i + "]")).getText();
			// Log.message("Contact name is:"+contactName);
			SkySiteUtils.waitTill(5000);
			// Checking contact name equal with each row of table
			if (albumnamefound.contains(albumname)) {
				Log.message(albumname + " Is present in the collection");
				driver.findElement(By.xpath("(//*[@id='dvAlbumContainer']/div/div/a/i)[" + i + "]")).click();
				Log.message("The album is clicked ");
				SkySiteUtils.waitTill(6000);
				break;
			}
		}
		String filename = PropertyReader.getProperty("photoname");
		Log.message("File to be upload is " + filename);
		SkySiteUtils.waitTill(9000);
		String uploadedfilename = driver.findElement(By.xpath(".//*[@id='dvAlbumImageContainer']/div/div/span"))
				.getText();
		if (driver.findElement(By.xpath(".//*[@id='dvAlbumImageContainer']/div/div/a/img")).isDisplayed()
				&& uploadedfilename.contains(filename)) {
			Log.message("Album is opened and the image is displayed");
			return true;
		}

		else {
			return false;
		}
	}

	@FindBy(css = "#btnOpenFullscreen")
	@CacheLookup
	WebElement maximizephoto;
	@FindBy(css = "#ProjectFiles>a")
	@CacheLookup
	WebElement documenttab;
	@FindBy(css = "#btnOpenTilesView")
	@CacheLookup
	WebElement minimizebutn;

	/**
	 * Method to maximize the album photo
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 *             | By Tarak
	 * 
	 *  Modified by Trinanjwan | 21-Nov-2018
	 */
	public boolean maximizePhoto(String screenshotpath, String defaultscreenshotpath)
			throws IOException, InterruptedException {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='dvAlbumImageContainer']/div/div/a/img")).click();
		Log.message("image is clicked");
		SkySiteUtils.waitTill(10000);
		// driver.findElement(By.xpath(".//*[@id='divMore']/div/button")).click();
		// Log.message("More option is clicked");
		// SkySiteUtils.waitTill(8000);
		WebElement fullscreenbuttn = driver.findElement(By.xpath("//i[@id='fullscreenicon']"));
		SkySiteUtils.waitForElement(driver, fullscreenbuttn, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", fullscreenbuttn);
		Log.message("maximize button is clicked");
		SkySiteUtils.waitTill(10000);

		String screenshotname = screenshotpath + ".\\imageafter.PNG";
		String defaultscreenshotname = defaultscreenshotpath + ".\\imagebefore.PNG";
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File(screenshotname));
		File fileInput = new File(defaultscreenshotname);
		File fileOutPut = new File(screenshotname);
		int lengthofinputfile = (int) fileInput.length();
		Log.message("Length of input file is " + lengthofinputfile);
		int lengthofoutputfile = (int) fileOutPut.length();
		Log.message("Length of output file is " + lengthofoutputfile);
		if (lengthofinputfile < lengthofoutputfile) {
			Log.message("image is maximizeed");
			return true;

		} else {
			Log.message("image is not maximixed");
			return false;
		}

	}

	/**
	 * Method to minimize photo
	 * 
	 * @throws AWTException
	 */
	@FindBy(css = "btn btn-primary am-show-tooltip pull-right")
	WebElement moreoptionformax;

	public void minimizePhoto(String screnshotpath) throws AWTException {
		this.delteScreenShot(screnshotpath);
		Log.message("Screen shot is deleted ");
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_ESCAPE);
		rb.keyRelease(KeyEvent.VK_ESCAPE);
		SkySiteUtils.waitTill(9000);
		Log.message("Escape button is clicked");

		/*
		 * SkySiteUtils.waitTill(8000); moreoptionformax.click();
		 * Log.message("More option is clicked"); SkySiteUtils.waitTill(9000);
		 * WebElement minimizebutn
		 * =driver.findElement(By.xpath("//*[contains(text(),'Tile view')]"));
		 * SkySiteUtils.waitForElement(driver, minimizebutn, 50); JavascriptExecutor
		 * executor = (JavascriptExecutor)driver;
		 * executor.executeScript("arguments[0].click();", minimizebutn);
		 * Log.message("Minimize button is clicked"); SkySiteUtils.waitTill(9000);
		 * this.delteScreenShot(screnshotpath); Log.message("Screen shot is deleted ");
		 */

	}

	/**
	 * Method to delete screen shot created
	 * 
	 * @param Screenshotpath
	 * @return
	 */
	public boolean delteScreenShot(String Screenshotpath) {
		Log.message("Searching screenshot downloaded");
		File file = new File(Screenshotpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains("imageafter.PNG")) {
					Myfile.delete();
					return true;
				}
			}
		}
		return false;
	}

	@FindBy(xpath = ".//*[@id='Div1']/nav/div[2]/div/ul/li[4]/a")
	@CacheLookup
	WebElement sharealbumoption;
	@FindBy(css = "#btnEmailClient")
	@CacheLookup
	WebElement SendViaEmailbtn;

	/**
	 * Method to share album with send via email option
	 * 
	 * @throws IOException
	 *             Scripted by : tarak
	 * @throws InterruptedException
	 */
	public boolean sendviaEmailOption() throws IOException, InterruptedException {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		Moreoption.click();
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		sharealbumoption.click();
		Log.message("Share album option is clicked");
		SkySiteUtils.waitTill(4000);
		SendViaEmailbtn.click();
		// SendViaEmailbtn.click();
		Log.message("Send via email button is clicked");
		SkySiteUtils.waitTill(20000);
		File fis = new File(PropertyReader.getProperty("AutoITsendviaemailpath"));
		String filepath = fis.getAbsolutePath().toString();
		String autoitpath = filepath + "\\MYautoit.exe";
		Process scriptexecution = Runtime.getRuntime().exec(autoitpath);
		scriptexecution.waitFor();
		if (scriptexecution.exitValue() == 1) {
			Log.message("Autoit script is executed and outlook window appear and is closed");
			return true;
		} else {
			Log.message("Auto It script cannot handle the outlook");
			return false;
		}

	}

	@FindBy(xpath = ".//*[@id='divAlbumShareLink']/div/div/div[3]/button[4]")
	@CacheLookup
	WebElement closebtnsharealbum;

	/**
	 * Click close button of share album
	 * 
	 */
	public void clickClose() {
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(4000);
		closebtnsharealbum.click();
		Log.message("Close button is clicked");

	}

	/**
	 * Generate Random Communication Subject
	 * By Trinanjwan
	 */
	
	
	public String generateRandomCmnctnSbjct()
	
	{
		
		  String str="SubjectCom";
          Random r = new Random( System.currentTimeMillis() );
          Random r1= new Random( System.currentTimeMillis());
          String abc = str+String.valueOf((10000 + r.nextInt(20000)))+String.valueOf((20000 + r1.nextInt(30000)));
          return abc;
		
		
	}
	
	/**
	 * Method to land into the communication page
	 * By Trinanjwan
	 */
	
	
	@FindBy(css = "#ProjectMenu1_Communications > div:nth-child(1) > a:nth-child(1)")
	@CacheLookup
	WebElement Communicationbutton;
	
	
	
	public CommunicationPage landingcomnctnpage()
	
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		Log.message("Switched to default content");
		SkySiteUtils.waitForElement(driver, Communicationbutton, 30);
		Log.message("Communication button is visibled now");
		Communicationbutton.click();
		Log.message("Clicked on Communication button");
		SkySiteUtils.waitTill(10000);
		return new CommunicationPage(driver).get();

	}
	
	
	
	
}
