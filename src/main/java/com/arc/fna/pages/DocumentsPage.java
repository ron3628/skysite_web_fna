package com.arc.fna.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import com.arc.fna.utils.SkySiteUtils;

public class DocumentsPage extends LoadableComponent<DocumentsPage> {

	protected void load() {}

	@Override
	protected void isLoaded() throws Error {
		SkySiteUtils.waitForElement(driver, documentTreeContainer, 40);
		directoryTree = new DirectoryTree(documentTreeContainer);
	}
	
	WebDriver driver;
	private DirectoryTree directoryTree;
	
	public DocumentsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	private class DirectoryTree {
		
		public DirectoryTree(WebElement container) {
			List<WebElement> roots = container.findElements(By.xpath("./tr"));
			rootElement = roots.get(0);
			String title = getRootElementTitle();
			for (int i = 1; i < roots.size(); i++) {
				WebElement child = roots.get(i).findElement(By.xpath("./td[2]"));
				WebElement childContainer = child.findElement(By.xpath("./table/tbody"));
				WebElement folderExpander = childContainer.findElement(By.xpath("./tr/td/div"));
				String styleText = folderExpander.getAttribute("style");
				String[] styleComponents = styleText.split("; ");
				for (int j = 0; j < styleComponents.length; j++) {
					String[] styleComponent = styleComponents[j].split(": ");
					if (styleComponent[0].equals("background-image") && styleComponent[1].contains("plus")) {
						folderExpander.click();
						try { Thread.sleep(5000); }
						catch (InterruptedException e) {}
						break;
					}
				}
				childContainer = child.findElement(By.xpath("./table/tbody"));
				childElements.add(new DirectoryTree(childContainer));
			}
		}
		
		public String getRootElementTitle() {
			return rootElement.findElement(By.xpath("./td[4]/span")).getText();
		}
		
		public void clickOnDirectory(ArrayList<String> directoryPath) throws IllegalArgumentException {
			if (directoryPath.size() == 0) {
				throw new IllegalArgumentException("Size of Directory Path is 0.");
			}
			else if (getRootElementTitle().equals(directoryPath.get(0))) {
				if (directoryPath.size() == 1) {
					rootElement.findElement(By.xpath("./td[4]/span")).click();
				}
				else {
					directoryPath.remove(0);
					for (int i = 0; i < childElements.size(); i++) {
						if (childElements.get(i).getRootElementTitle().equals(directoryPath.get(0))) {
							childElements.get(i).clickOnDirectory(directoryPath);
							break;
						}
						else if ((i + 1) == childElements.size()) {
							throw new IllegalArgumentException("Path Element is not located.");
						}
					}
				}
			}
			else {
				throw new IllegalArgumentException("Path Element is not Root Element.");
			}
		}

		WebElement rootElement = null;
		List<DirectoryTree> childElements = new ArrayList<DirectoryTree>();
		
	}

	@FindBy(how=How.XPATH, using="//div[@class='containerTableStyle']/table/tbody/tr[2]/td[2]/table/tbody")
	WebElement documentTreeContainer;

	public void openCollectionDirectory(ArrayList<String> directoryPath) throws IllegalArgumentException {
		directoryTree.clickOnDirectory(directoryPath);
		try { Thread.sleep(5000); }
		catch (InterruptedException e) {}
	}

	@FindBy(how=How.XPATH, using="//div[@class='objbox']/table/tbody")
	WebElement documentsContainer;

	public ViewerPage openDocumentInViewer(String documentName) {
		List<WebElement> documentItems = documentsContainer.findElements(By.xpath("./tr"));
		if (documentItems.size() != 1) {
			for (int i = 1; i < documentItems.size(); i++) {
				WebElement document = documentItems.get(i).findElement(By.xpath("./td[2]/a[3]"));
				if (document.getText().equals(documentName)) {
					document.click();
					try { Thread.sleep(10000); }
					catch (InterruptedException e) {}
					break;
				}
			}
		}
		String viewerHandle = driver.getWindowHandles().toArray(new String[0])[1];
		driver.switchTo().window(viewerHandle);
		try { Thread.sleep(5000); }
		catch (InterruptedException e) {}
		return new ViewerPage(driver).get();
	}

}