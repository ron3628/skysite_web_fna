package com.arc.fna.pages;


import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;


public class ModuleSearchPage extends LoadableComponent<ModuleSearchPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public ModuleSearchPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	/**
	 * Method written for Random ReName
	 *  Scripted By: Sekhar
	 * @return
	 */

	public String Random_ReName()
	{
		String str = "Rename";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Title
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Title()
	{
		String str = "Title";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Subject
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Subject()
	{
		String str = "Subject";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Description
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Description()
	{
		String str = "Description";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	
	
	@FindBy(xpath = "//i[@class='icon icon-info icon-orange']")
	WebElement btniconorange;
	
	@FindBy(css = "#txt_fileName")
	WebElement txtfileName;
	
	@FindBy(css = "#txt_title")
	WebElement txttitle;
	
	//@FindBy(xpath = "//*[@id='div_otherInfo']/div/div/div[3]/input[1]")
	@FindBy(xpath = ".//*[@id='btnupdateclose']")
	WebElement btnupdateclose;
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement fileName;
	
	/** 
     * Method written for Edit the file in file level.
     * Scripted By: Sekhar      
     */
	
    public boolean Edit_File_CollectionFolder(String Rename,String Title)
    {  
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
       	btniconorange.click();
    	Log.message("update button has been clicked");
    	SkySiteUtils.waitTill(5000);    	
    	txttitle.clear();
    	Log.message("title has been cleared");
    	SkySiteUtils.waitTill(2000);
    	txttitle.sendKeys(Title);
    	Log.message("title has been entered "+Title);
    	SkySiteUtils.waitTill(2000);
    	txtfileName.clear();
    	Log.message("file name has been cleared");
    	SkySiteUtils.waitTill(2000);
    	txtfileName.sendKeys(Rename);
    	Log.message("file name has been entered "+Rename);
    	SkySiteUtils.waitTill(2000);
    	btnupdateclose.click();
    	Log.message("update & close button has been entered");
    	SkySiteUtils.waitTill(15000);
    	String File_Name=fileName.getText().toString();
    	Log.message("File name is:"+File_Name);    	
    	if(File_Name.contains(Rename))
    	{
    		Log.message("File renamed successfully");
    		return true;
    	}
    	else
    	{
    		Log.message("File renamed Unsuccessfully");
    		return false;
    	}
    }
    
    @FindBy(xpath = "//*[@id='liGlobalSearch']/a/i")
	WebElement btnGlobalSearch;
    
    @FindBy(xpath = "//*[@id='idSearchcontroleDropDown']/li[1]/div/div[2]/div[1]/div")
	WebElement btnSearchcontroleDropDown;
    
    @FindBy(css = "#searchiconbutton")
   	WebElement btnsearchiconbutton;
    
    @FindBy(xpath = "//*[@id='div_SearchContent']/li/div/div[1]/h4/a")
   	WebElement btngetfilename;
    
    /** 
     * Method written for Module search in file level.
     * Scripted By: Sekhar      
     */
	
    public boolean Module_Search_FileLevel(String Rename)
    {  
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnGlobalSearch.click();   
    	Log.message("Search button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnSearchcontroleDropDown.click();
    	Log.message("Search button has been clicked");
       	SkySiteUtils.waitTill(3000);  
       	
       	if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}       
    	SkySiteUtils.waitTill(5000);
		driver.findElement(By.id("SearchsettingSelectall")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.id("ChkSearchSettingFile")).click();        
		if ( !driver.findElement(By.id("ChkSearchSettingFile")).isSelected())
		{
			driver.findElement(By.id("ChkSearchSettingFile")).click();
		} 
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).click();
		Log.message("Global text box has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Rename);
		Log.message("File name has been entered");
		SkySiteUtils.waitTill(50000);	
		btnsearchiconbutton.click();
		Log.message("search icon button has been clicked");
		SkySiteUtils.waitTill(5000);		
		String file_Name = btngetfilename.getText().toString();
		Log.message("File name is:"+file_Name);
		SkySiteUtils.waitTill(3000);
		if(file_Name.contains(Rename))
			return true;
		else
			return false;		
    }
    
    @FindBy(xpath = "//*[@id='div_SearchContent']/li/div/div[1]/h4")
   	WebElement btnSearchContent;
    
    /** 
     * Method written for Module search in communications level.
     * Scripted By: Sekhar      
     */
	
    public boolean Module_Search_CommunicationsLevel(String Subject)
    {  
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnGlobalSearch.click();   
    	Log.message("Search button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnSearchcontroleDropDown.click();
    	Log.message("Search button has been clicked");
       	SkySiteUtils.waitTill(3000);  
       	
       	if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}       
    	SkySiteUtils.waitTill(5000);
		driver.findElement(By.id("SearchsettingSelectall")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.id("ChkSearchSettingComm")).click();        
		if ( !driver.findElement(By.id("ChkSearchSettingComm")).isSelected())
		{
			driver.findElement(By.id("ChkSearchSettingComm")).click();
		} 
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).click();
		Log.message("Global text box has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Subject);
		Log.message("File name has been entered");
		SkySiteUtils.waitTill(30000);	
		btnsearchiconbutton.click();
		Log.message("searcn icon button has been clicked");
		SkySiteUtils.waitTill(6000);		
		String file_Name = btnSearchContent.getText().toString();
		Log.message("Subject name is:"+file_Name);
		if(file_Name.contains(Subject))
			return true;
		else
			return false;		
    }
    
    @FindBy(xpath = "//i[@class='icon icon-star ico-lg icon-album']")
   	WebElement btniconalbum;
    
    @FindBy(xpath = "//span[@class='selectedTreeRow']")
   	WebElement btnselectedalbum;
    
    @FindBy(xpath = "//table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
   	WebElement btnalbum;
    
    @FindBy(xpath = "//i[@class='icon icon-settings ico-lg']")
   	WebElement btniconsettings;
    
    @FindBy(xpath = "//div/table/tbody/tr/td[1]/img")
   	WebElement btnupdate;
    
    @FindBy(css = "#txt_title")
   	WebElement albtxttitle;
    
    @FindBy(css = "#txt_desc")
   	WebElement albtxtdesc;
    
    @FindBy(xpath = "//*[@id='div_otherInfoTable']/div[3]/button[1]")
   	WebElement btnupdate1;
    
    @FindBy(xpath = "//*[@id='albumOperation']/div/div/div[3]/button[3]")
   	WebElement btnclose;
    
    @FindBy(xpath = "//*[@id='dvAlbumImageContainer']/div/div/span")
   	WebElement btnImageContainer;
    
    
    /**
	 * Method written for select Album
	 *  Scripted By: Sekhar
	 * @return
	 */
	public boolean selectCollectionAlbum(String Collection_Name,String Title_Name,String Description_Name) 
	{
		boolean result1=false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btniconalbum, 60);		
		btniconalbum.click();
		Log.message("Album button has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame		
		SkySiteUtils.waitForElement(driver, btnselectedalbum, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = btnselectedalbum.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(Collection_Name)) 
		{
			result1=true;
			Log.message("Select Collection Album successfull");
			SkySiteUtils.waitTill(2000);		
		} 
		else
		{
			result1=false;
			Log.message("Select Collection Album Unsuccessfull");
			SkySiteUtils.waitTill(2000);	
		}
		SkySiteUtils.waitTill(2000);
		btnalbum.click();
		Log.message("Album has been clicked");
		SkySiteUtils.waitTill(2000);
		btniconsettings.click();
		Log.message("Manage Album button has been clicked");
		SkySiteUtils.waitTill(2000);
		btnupdate.click();
		Log.message("Update button has been clicked");
		SkySiteUtils.waitTill(2000);
		albtxttitle.clear();
		Log.message("Title name has been cleared");
		SkySiteUtils.waitTill(2000);
		albtxttitle.sendKeys(Title_Name);
		Log.message("Title name has been entered");
		SkySiteUtils.waitTill(2000);
		albtxtdesc.clear();
		Log.message("Description has been cleared");
		SkySiteUtils.waitTill(2000);
		albtxtdesc.sendKeys(Description_Name);
		Log.message("Description has been entered");
		SkySiteUtils.waitTill(2000);
		btnupdate1.click();
		Log.message("Update button has been clicked");
		SkySiteUtils.waitTill(10000);
		btnclose.click();
		Log.message("close button has been clicked");
		SkySiteUtils.waitTill(8000);
		String Photo_Name=btnImageContainer.getText().toString();
		Log.message("Photo name is:"+Photo_Name);
		if((Photo_Name.contains(Title_Name))&&(result1==true))
			return true;
		else
			return false;
	}
	
	 @FindBy(xpath = ".//*[@id='div_SearchContent']/li/div/div[1]/h5[1]")
	 WebElement btnSearchContent1;
	    
	 /** 
	  * Method written for Module search in Photo level.
	  * Scripted By: Sekhar      
	  */
		
	 public boolean Module_Search_Photo(String Title,String Description)
	 {  
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().defaultContent();
		 btnGlobalSearch.click();   
		 Log.message("Search button has been clicked");
		 SkySiteUtils.waitTill(2000);
		 btnSearchcontroleDropDown.click();
		 Log.message("Search button has been clicked");
		 SkySiteUtils.waitTill(3000);  
		 
		 if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		 {
			driver.findElement(By.id("SearchsettingSelectall")).click();
		 }	       
		 SkySiteUtils.waitTill(5000);
		 driver.findElement(By.id("SearchsettingSelectall")).click();
		 SkySiteUtils.waitTill(3000);
		 driver.findElement(By.id("ChkSearchSettingPhoto")).click();        
		 if(!driver.findElement(By.id("ChkSearchSettingPhoto")).isSelected())
		 {
			 driver.findElement(By.id("ChkSearchSettingPhoto")).click();
		 } 
		 SkySiteUtils.waitTill(3000);
		 driver.findElement(By.xpath("//input[@id='txtSearchKey']")).click();
		 Log.message("Global text box has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Title);
		 Log.message("File name has been entered");
		 SkySiteUtils.waitTill(30000);	
		 btnsearchiconbutton.click();
		 Log.message("searcn icon button has been clicked");
		 SkySiteUtils.waitTill(10000);		
		 String Photo_Name = btnSearchContent.getText().toString();
		 Log.message("Title name is:"+Photo_Name);				
		 String Des_name = btnSearchContent1.getText().toString();
		 Log.message("Des name is:"+Des_name);
		 SkySiteUtils.waitTill(3000);
		 if((Photo_Name.contains(Title))&&(Des_name.contains(Description)))
			 return true;
		 else
			 return false;		
	 }
	 
	 @FindBy(css = "#txtSearchValue")
	 WebElement txtSearchValue;
	 
	 @FindBy(xpath = "(//*[@id='btnSearch'])[1]")
	 WebElement btnSearch;
	 
	 @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	 WebElement txtname;
	    
	 /** 
	  * Method written for Grid search in File level.
	  * Scripted By: Sekhar      
	  */
		
	 public boolean Grid_Search_FileLevel(String FileName)
	 {  
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 	
		 txtSearchValue.sendKeys(FileName);
		 Log.message("document name has been entered");
		 SkySiteUtils.waitTill(3000);
		 btnSearch.click();
		 Log.message("Search has been clicked");
		 SkySiteUtils.waitTill(3000);	
		 String Document_Name = txtname.getText().toString();
		 Log.message("Document name is:"+Document_Name);
		 if(Document_Name.trim().contains(FileName.trim()))
			 return true;
		 else
			 return false;		 	
	 }
	 
	    
}