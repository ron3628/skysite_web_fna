package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Random;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class FNA_ReportsPage extends LoadableComponent<FNA_ReportsPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;
	
	@FindBy(xpath = "//*[@id='frmCommonLandingInfoLinks']/a")
	WebElement GotofacilityAndArchive;
	
	@FindBy(css = "#ProjectMenu1_liReport>a")
	WebElement Reports;	
	
	@FindBy(xpath = "//a[@tabindex='0'  and @aria-label='addTemplate' ]")
	WebElement addtemplate;
	
	@FindBy(xpath = "(//a[@aria-label='addTemplate' and @tabindex='0'])[2]")
	WebElement addtemplate1;
	
	@FindBy(xpath = "(//a[@aria-label='addTemplate' and @tabindex='0'])[2]")
	WebElement Export_template;	
	
	@FindBy(css = ".icon.icon-pr-delete")
	WebElement Deletetemplate;	
	@FindBy(xpath = "//button [@id='yes']")
	WebElement yes_button;
	
	@FindBy(xpath = "//label [@class='asset-status-approve ng-star-inserted']")
	WebElement Approve_Status;
	
	
	@FindBy(css = ".report-nav.valign")
	WebElement Approve_Button;
	
	
	
	@FindBy(xpath = "//div [@class='chat-body clearfix']")
	WebElement Comment_Status;	
	
	
	@FindBy(css = ".icon.icon-pr-edit")
	WebElement Edittemplate;	
	
	@FindBy(xpath = "//*[@id='modal-section']/perfect-scrollbar/div/div[1]/div[1]/div/div/button")
	WebElement ChooseaFile;
	
	@FindBy(xpath = "//input [@aria-label='templatename']")
	WebElement TemplateName;
	

	@FindBy(xpath = "(//button [@class='btn btn-primary'])[1]")
	WebElement create;
	
	@FindBy(xpath = "//textarea[@placeholder='Add comments']")
	WebElement comment;	
	
	@FindBy(xpath = "//button[contains(text(),'Submit')]")
	WebElement SubmitButton;	
	
	@FindBy(xpath = "//button[contains(text(),'Draft')]")
	WebElement DraftButton;
	
	@FindBy(xpath = "//span[@class='span-seperator']")
	WebElement Text_validation;
	
	@FindBy(xpath = "//input[@name='Program_Name' and @spellcheck='true']")
	WebElement Programme_name;
	
	@FindBy(xpath = "(//button[@ id='ToolbarButton-custom'])[1]")
	WebElement Report_SaveButton;
	
	@FindBy(xpath = "//*[@id='modal-section']/perfect-scrollbar/div/div[1]/div[2]/div/div[1]/input")
	WebElement EditTemplateName;
	
	@FindBy(xpath = "//div[@class='report-nav valign']")
	WebElement allbutton;	

	@FindBy(xpath = "//textarea [@class='form-control ng-untouched ng-pristine ng-valid']")
	WebElement txtcomment;
	
	
	@FindBy(xpath = "//div[1]/div/div[2]/div[2]/div[2]/button[2]")
	WebElement btnSubmit;
	
	@FindBy(xpath = "//*[@id='yes']")
	WebElement btnyes;	
	
	
	
	@FindBy(xpath = ".//*[@id='1']/div[3]/div/ul/li[4]/label")
	WebElement Pending;
	
	@FindBy(xpath = "//span[@class='badge']")
	WebElement Arrow_third;
	
	
	
	
	@FindBy(xpath = "//button[@class='btn btn-primary' and @role='button']")
	WebElement ApproveButton;
	
	@FindBy(xpath = "//button[text()='Reject']")
	WebElement RejectButton;
	
	@FindBy(xpath = "//a [@aria-label='DuplicateReport']")
	WebElement DuplicateButton;
	
	@FindBy(xpath = "//input [@class='form-control ng-untouched ng-pristine']")
	WebElement ReportNo;
	
	
	@FindBy(xpath = "(//a [@class='dropdown-toggle'])[1]")
	WebElement Addphoto;
	
	
	@FindBy(xpath = "(//a [@class='dropdown-toggle'])[2]")
	WebElement Addfile;
	
	@FindBy(xpath = "//i [@class='icon icon-pr-computer']")
	WebElement AddComputer;
	
	@FindBy(xpath = "//i [@class='icon icon-pr-download']")
	WebElement downloadButton;
	
	@FindBy(xpath = "(//i [@class='icon icon-pr-tags-collapse-arrow'])[2]")
	WebElement PhotoArrow;
	
	
	
	@FindBy(xpath = "//ul[@id='assetImagesRef']")
	WebElement Photo_layer;
	
	@FindBy(xpath = "//div [@class='tag-file-card--name']")
	WebElement FileText;
	
	
	
	@FindBy(xpath = "//button[text()='Reject']")
	WebElement rejectButton;
	
	
	@FindBy(xpath = "//select [@class='form-control select__field' and  @aria-label='requiredapprovals']")
	WebElement RequiredApproval;
	
	
	//@FindBy(xpath = "(//input[@type='text'])[3]")
	//WebElement SelectUserWhocanApproveReport;
	
	@FindBy(xpath = "//div/div[1]/div[2]/div/div[3]/tag-input/div/div/tag-input-form/form/input")
	WebElement SelectUserWhocanApproveReport;	
	
	@FindBy(xpath = "//div [@class='ng2-menu-item ng2-menu-item--selected']")
	WebElement emailSelect;	
	
	@FindBy(xpath = "//div [@class='ng-tns-c2-31 ng2-tag-input']")
	WebElement usertonotify;
	
	@FindBy(xpath = "(//select [@class='form-control'])[2]")
	WebElement Schedule;
	
	@FindBy(xpath = "//*[contains(@type,'button') and text()='Save'] ")
	WebElement savebutton;
	
	@FindBy(xpath = "//*[@id='searchTemplate']")
	WebElement SearchTemplate_inbox;	

	@FindBy(xpath = "//h4 [@class='media-heading']")
	WebElement Aftercreation_template;
	
	@FindBy(xpath = "//div[@class='no-content-item']")
	WebElement content_validation;
	
	
	//@FindBy(xpath = "//span [@class='temp-approver']")
	
	@FindBy(xpath = "(//span [@class='temp-approver'])[1]")	
	WebElement AnyOneCanApprove;
	
	@FindBy(xpath = "//span [@class='icon icon-pr-download']")
	WebElement Download_Link;
	
	
	@FindBy(xpath = "//i [@class='icon icon-pr-export']")
	WebElement ExportButton;
	
	@FindBy(css = ".icon.icon-star.ico-lg.icon-teams")
	WebElement Team;
	
	@FindBy(xpath = "(//button[@data-original-title='More'])[2]")
	WebElement More;
	
	@FindBy(xpath = "(//i [@class='icon icon-add-teamMemberAddress'])[3]")
	WebElement Addteammember;
	
	

	
	
	
	
	
	
	
	

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, Reports, 60);
	}

	@Override
	protected void isLoaded() throws Error
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FNA_ReportsPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	public void NavigateToFNA() {
		SkySiteUtils.waitTill(4000);
		if(GotofacilityAndArchive.isDisplayed()){
		SkySiteUtils.waitForElement(driver, GotofacilityAndArchive, 60);
		GotofacilityAndArchive.click();
		Log.message("Go to Facilities & Archive button Selected Sucessfully");
		SkySiteUtils.waitTill(4000);
	}
	}
	
	
	
public void Reports() throws InterruptedException
	{		
			SkySiteUtils.waitTill(15000);		
			Reports.click();
			Log.message("Report Link Clicked Sucessfully");
			
	}


public boolean Element_visible() throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
				
		Log.message("Switch to Default frame ");
		if(content_validation.isDisplayed())
			return true;
		else
			return false;

		
}


public boolean UploadTemplate(String FolderPath) throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, addtemplate1, 60);
		addtemplate1.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		// ===External And Internal File Upload		
		SkySiteUtils.waitTill(10000);
		//outer Upload
	    UploadFiles_old(FolderPath);	    
	    Log.message("Upload Happen File Uploded Sucessfully");
	    SkySiteUtils.waitTill(15000);
	    String template =TemplateName.getAttribute("value");
		if (template.contains(template))
			return true;
		else
			return false;
	    
}


public boolean ApprovalScreenValidation() throws InterruptedException, AWTException, IOException, ParseException 
{		
	    SkySiteUtils.waitTill(5000);	    
	    SkySiteUtils.waitForElement(driver, create, 60);
	    create.click();
	    SkySiteUtils.waitTill(30000);
	    Log.message("Create Button Clicked Sucessfully");	    
	    String comment1 = PropertyReader.getProperty("comment");	   
	    Log.message("Comment from Testdate:-"+comment1);	
	    SkySiteUtils.waitForElement(driver, comment, 60);
	    comment.sendKeys(comment1);
	    SkySiteUtils.waitTill(5000);
	    Log.message("Comment Entered Sucessfully");	
	    SkySiteUtils.waitForElement(driver, SubmitButton, 60);
	    SubmitButton.click();
	    SkySiteUtils.waitTill(5000);
	    Log.message("Submit Button clicked  Sucessfully");
	    SkySiteUtils.waitForElement(driver, yes_button, 60);
		yes_button.click();
		Log.message("Yes Button Sucessfully");
		   SkySiteUtils.waitTill(5000);
	    if(Approve_Button.isDisplayed())
	    	
	    {	
	    	Log.message(" Pending review/approval screen  verified Sucessfully");
			return true;
	    }
		else
		{
			Log.message(" Pending review/approval screen  verified Sucessfully");
			return false;
		}
			
	    
}


public boolean ApprovalSubmit_validation() throws InterruptedException, AWTException, IOException, ParseException 
{		
	    SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, create, 60);
	    create.click();
	    SkySiteUtils.waitTill(60000);
	    Log.message("Create Button Clicked Sucessfully");	    
	    String comment1 = PropertyReader.getProperty("comment");	   
	    Log.message("Comment from Testdate:-"+comment1);	
	    SkySiteUtils.waitForElement(driver, comment, 60);
	    comment.sendKeys(comment1);
	    Log.message("Comment Entered Sucessfully");	
	    SkySiteUtils.waitForElement(driver, SubmitButton, 60);
	    SubmitButton.click();
	    Log.message("Submit Button clicked  Sucessfully");
	    SkySiteUtils.waitForElement(driver, yes_button, 60);
		yes_button.click();
		Log.message("Yes Button Sucessfully");	   
		SkySiteUtils.waitTill(30000);
	    if(Approve_Status.isDisplayed())
	    	
	    {	
	    	Log.message("Approval Status  verified Sucessfully");
			return true;
	    }
		else
		{
			Log.message("Approval Status Not verified Sucessfully");
			return false;
		}
			
	    
}

public boolean CommentValidation() throws InterruptedException, AWTException, IOException, ParseException 
{		
	    SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, create, 60);
	    create.click();
	    SkySiteUtils.waitTill(60000);
	    Log.message("Create Button Clicked Sucessfully");	    
	    String comment1 = PropertyReader.getProperty("comment");	   
	    Log.message("Comment from Testdate:-"+comment1);	
	    SkySiteUtils.waitForElement(driver, comment, 60);
	    comment.sendKeys(comment1);
	    Log.message("Comment Entered Sucessfully");	
	    SkySiteUtils.waitForElement(driver, SubmitButton, 60);
	    SubmitButton.click();
	    Log.message("Submit Button clicked  Sucessfully");
	    SkySiteUtils.waitForElement(driver, yes_button, 60);
		yes_button.click();
		Log.message("Yes Button Sucessfully");	   
		SkySiteUtils.waitTill(20000);
		Approve_Status.click();
		Log.message("Approval status button Clicked  Sucessfully");
		
		SkySiteUtils.waitTill(10000);
		
	    if(Comment_Status.isDisplayed())
	    	
	    {	
	    	Log.message("Comment Status  verified Sucessfully");
			return true;
	    }
		else
		{
			Log.message("Comment Status Not verified Sucessfully");
			return false;
		}
			
	    
}

public boolean SaveAsDraft_validation() throws InterruptedException, AWTException, IOException, ParseException 
{		
	    SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, create, 60);
	    create.click();
	    SkySiteUtils.waitTill(60000);
	    Log.message("Create Button Clicked Sucessfully");	   
	    SkySiteUtils.waitForElement(driver, DraftButton, 60);
	    DraftButton.click();
	    Log.message("Draft Button Clicked Sucessfully");
	    SkySiteUtils.waitForElement(driver, Text_validation, 60);
	    String text = Text_validation.getText();
	    SkySiteUtils.waitTill(5000);
	    Log.message("Draft Count:---"+text);
		SkySiteUtils.waitTill(5000);
	    if(text.contains("1"))
	    	
	    {	
	    	Log.message("Draft Report  verified Sucessfully");
			return true;
	    }
		else
		{
			Log.message("Draft Report  Not verified Sucessfully");
			return false;
		}
			
	    
}


public boolean Reportsubmit_Edit_validation() throws InterruptedException, AWTException, IOException, ParseException, FindFailed 
{		
	    SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, create, 60);
	    create.click();
	    SkySiteUtils.waitTill(60000);
	    Log.message("Create Button Clicked Sucessfully");	
	   // driver.switchTo().frame(driver.findElement(By.id("pspdfkit")));
	    //#-------Report edit Field-----------------
	    String comments = PropertyReader.getProperty("comment");	
	    SkySiteUtils.waitTill(5000);
	    
	    
	    File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\edit.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		
		s.type("Programme Name Entered into Report");
	   // Programme_name.sendKeys(comments);
	     Log.message("Programme Name Entered into Report");
	    SkySiteUtils.waitTill(15000);
	   // WebElement element = driver.findElement(By.xpath("(//button[@ id='ToolbarButton-custom'])[1]"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		
	    SkySiteUtils.waitForElement(driver, Report_SaveButton, 60);
	    Report_SaveButton.click();
	    Log.message("Save Button Clicked Sucessfully");
	    SkySiteUtils.waitTill(5000);
	    String comment1 = PropertyReader.getProperty("comment");	   
	    Log.message("Comment from Testdate:-"+comment1);	
	    SkySiteUtils.waitForElement(driver, create, 60);
	    comment.sendKeys(comment1);
	    Log.message("Comment Entered Sucessfully");	
	    SkySiteUtils.waitForElement(driver, SubmitButton, 60);
	    SubmitButton.click();
	    Log.message("Submit Button clicked  Sucessfully");
	    SkySiteUtils.waitForElement(driver, yes_button, 60);
		yes_button.click();
		Log.message("Yes Button Sucessfully");	   
		SkySiteUtils.waitTill(30000);		
		WebElement readonly=driver.findElement(By.xpath("//input[@name='Program_Name']"));		
		if(Report_SaveButton.isDisplayed()){
			Log.message("programe Report Field is ReadOnly and not editable ");
			return true;
		 }
		else {
			Log.message("programe Report Field is not  ReadOnly and editable");
			return false;
		 }
	   
}

	
public boolean CreateTemplate(String FolderPath,String Approver) throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, addtemplate1, 60);
		addtemplate1.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		// ===External And Internal File Upload		
		SkySiteUtils.waitTill(10000);
		//outer Upload
	    UploadFiles_old(FolderPath);	    
	    Log.message("Upload Happen File Uploded Sucessfully");
	    SkySiteUtils.waitTill(15000);
		//SkySiteUtils.waitForElement(driver, addtemplate, 60);
		//String Template = randomFileName.RandamName();
		
		//TemplateName.sendKeys(Template);
		String template =TemplateName.getAttribute("value");
		Log.message("Temp name is:"+template);
		 SkySiteUtils.waitTill(5000);
		 
		 WebElement element = driver.findElement(By.xpath("//select [@class='form-control select__field' and  @aria-label='requiredapprovals']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			
			 SkySiteUtils.waitTill(10000);		 
			 Select drpcountry = new Select(driver.findElement(By.xpath("//select [@class='form-control select__field' and  @aria-label='requiredapprovals']")));
			 drpcountry.selectByVisibleText(Approver);
			 Log.message("select required!!!!!!");
			//SkySiteUtils.waitForElement(driver, SelectUserWhocanApproveReport, 60);
			SkySiteUtils.waitTill(5000);		
		
		 
		 
		/*SkySiteUtils.waitForElement(driver, SelectUserWhocanApproveReport, 60);
		String Approver1 = PropertyReader.getProperty("ApproverEmail");
		SelectUserWhocanApproveReport.sendKeys(Approver1);
		Log.message("Selected Dorodown Approval Option Sucessfully:--" +Approver1);*/
		/*SkySiteUtils.waitForElement(driver, usertonotify, 60);
		String usertomail = PropertyReader.getProperty("ApproverEmail");
		SkySiteUtils.waitTill(5000);
		usertonotify.sendKeys("usertomail");
		Log.message("User to Notify Option Selected  Sucessfully:--" +usertomail);*/
		SkySiteUtils.waitTill(5000);
		/*SkySiteUtils.waitForElement(driver, Schedule, 60);
		String Daily = PropertyReader.getProperty("Status1");
		Select drpdaily = new Select(Schedule);
		drpdaily.selectByVisibleText(Daily);
		Log.message("Selected Daily Option  Sucessfully:--" +usertomail);
		SkySiteUtils.waitTill(5000);*/
		SkySiteUtils.waitForElement(driver, savebutton, 60);
		savebutton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(60000);
		String Template = Aftercreation_template.getText();
		SkySiteUtils.waitTill(5000);		
		if(template.contentEquals(Template))
		{
			Log.message("Template Name Verified  Sucessfully");
			return true;
		}
		else
		{
			Log.message("Template Name Not Verified  Sucessfully");
			return false;
		}
	}

@FindBy(xpath = "//select [@class='form-control select__field' and  @aria-label='requiredapprovals']")
WebElement btnrequired;

public boolean CreateTemplate_Any_One_Approve(String FolderPath,String Template) throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, addtemplate, 60);
		addtemplate.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		// ===External And Internal File Upload		
		SkySiteUtils.waitTill(10000);
		//outer Upload
	    UploadFiles_old(FolderPath);	    
	    Log.message("Upload Happen File Uploded Sucessfully");
	    SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, TemplateName, 60);	
		TemplateName.clear();
		 SkySiteUtils.waitTill(5000);
		TemplateName.sendKeys(Template);
		SkySiteUtils.waitTill(5000);
		String template =TemplateName.getAttribute("value");
		Log.message("Temp name is:"+template);
		 SkySiteUtils.waitTill(5000);
		 
		WebElement element = driver.findElement(By.xpath("//select [@class='form-control select__field' and  @aria-label='requiredapprovals']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		 Log.message("select required Approval");
		 SkySiteUtils.waitTill(10000);
		 
		 Select drpcountry = new Select(driver.findElement(By.xpath("//select [@class='form-control select__field' and  @aria-label='requiredapprovals']")));
		 drpcountry.selectByVisibleText("Single approval required");
		 Log.message("select required!!!!!!");
		//SkySiteUtils.waitForElement(driver, SelectUserWhocanApproveReport, 60);
		SkySiteUtils.waitTill(5000);
		SelectUserWhocanApproveReport.clear();
		Log.message("Cleared the required Approval ");	
		String Approver1 = PropertyReader.getProperty("ApproverEmail");
		SelectUserWhocanApproveReport.click();
		SkySiteUtils.waitTill(5000);		
		SelectUserWhocanApproveReport.sendKeys(Approver1);		
		Log.message("Selected Dorodown Approval Option Sucessfully:--" +Approver1);	
		SkySiteUtils.waitTill(5000);		
		driver.findElement(By.xpath("//b")).click();
		Log.message("Drop Down List Clicked Sucessfully");	
		SkySiteUtils.waitTill(5000);
		/*SkySiteUtils.waitForElement(driver, usertonotify, 60);
		String usertomail = PropertyReader.getProperty("ApproverEmail");
		SkySiteUtils.waitTill(5000);
		usertonotify.sendKeys("usertomail");
		Log.message("User to Notify Option Selected  Sucessfully:--" +usertomail);*/
	
		/*SkySiteUtils.waitForElement(driver, Schedule, 60);
		String Daily = PropertyReader.getProperty("Status1");
		Select drpdaily = new Select(Schedule);
		drpdaily.selectByVisibleText(Daily);
		Log.message("Selected Daily Option  Sucessfully:--" +usertomail);
		SkySiteUtils.waitTill(5000);*/
		SkySiteUtils.waitForElement(driver, savebutton, 60);
		savebutton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		SearchTemplate_inbox.sendKeys(Template);		
		// robot = new Robot(); 
		//robot.keyPress(KeyEvent.VK_TAB);
		//SkySiteUtils.waitTill(5000);
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(10000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(20000);						
		String Anyone = AnyOneCanApprove.getText();
		SkySiteUtils.waitTill(5000);		
		if(Anyone.contains("Single approval required"))
		{
			Log.message("Single approval required Verified  Sucessfully");
			return true;
		}
		else
		{
			Log.message("Single approval required  Not Verified  Sucessfully");
			return false;
		}
	}

public boolean CreateTemplate1(String FolderPath,String Template) throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, addtemplate1, 60);
		addtemplate1.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		// ===External And Internal File Upload		
		SkySiteUtils.waitTill(10000);
		//outer Upload
	    UploadFiles_old(FolderPath);	    
	    Log.message("Upload Happen File Uploded Sucessfully");
	    SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, TemplateName, 60);	
		TemplateName.clear();
		 SkySiteUtils.waitTill(5000);
		TemplateName.sendKeys(Template);
		SkySiteUtils.waitTill(5000);
		String template =TemplateName.getAttribute("value");
		Log.message("Temp name is:"+template);
		 SkySiteUtils.waitTill(5000);
		 
		WebElement element = driver.findElement(By.xpath("//select [@class='form-control select__field' and  @aria-label='requiredapprovals']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		
		 SkySiteUtils.waitTill(10000);
		 String approve = PropertyReader.getProperty("Approval_detail");
		 SkySiteUtils.waitTill(5000);
		 Select drpcountry = new Select(driver.findElement(By.xpath("//select [@class='form-control select__field' and  @aria-label='requiredapprovals']")));
		 drpcountry.selectByVisibleText(approve);
		 Log.message("select required!!!!!!");
		//SkySiteUtils.waitForElement(driver, SelectUserWhocanApproveReport, 60);
		SkySiteUtils.waitTill(5000);
		SelectUserWhocanApproveReport.clear();
		Log.message("Cleared the required Approval ");	
		String Approver1 = PropertyReader.getProperty("ApproverEmail");
		SelectUserWhocanApproveReport.click();
		SkySiteUtils.waitTill(5000);		
		SelectUserWhocanApproveReport.sendKeys(Approver1);		
		Log.message("Selected Dorodown Approval Option Sucessfully:--" +Approver1);	
		SkySiteUtils.waitTill(5000);		
		driver.findElement(By.xpath("//b")).click();
		Log.message("Drop Down List Clicked Sucessfully");	
		SkySiteUtils.waitTill(5000);
		/*SkySiteUtils.waitForElement(driver, usertonotify, 60);
		String usertomail = PropertyReader.getProperty("ApproverEmail");
		SkySiteUtils.waitTill(5000);
		usertonotify.sendKeys("usertomail");
		Log.message("User to Notify Option Selected  Sucessfully:--" +usertomail);*/
	
		/*SkySiteUtils.waitForElement(driver, Schedule, 60);
		String Daily = PropertyReader.getProperty("Status1");
		Select drpdaily = new Select(Schedule);
		drpdaily.selectByVisibleText(Daily);
		Log.message("Selected Daily Option  Sucessfully:--" +usertomail);
		SkySiteUtils.waitTill(5000);*/
		SkySiteUtils.waitForElement(driver, savebutton, 60);
		savebutton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		SearchTemplate_inbox.sendKeys(Template);		
		// robot = new Robot(); 
		//robot.keyPress(KeyEvent.VK_TAB);
		//SkySiteUtils.waitTill(5000);
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(10000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(20000);						
		String Anyone = AnyOneCanApprove.getText();
		SkySiteUtils.waitTill(5000);	
		String Approval = PropertyReader.getProperty("ApproverEmail");
		if(Anyone.contains(Approval))
		{
			Log.message("Single approval Verified  Sucessfully");
			return true;
		}
		else
		{
			Log.message("Single approval approval Not Verified  Sucessfully");
			return false;
		}
	}
public boolean newUploadvalidation() throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		//SkySiteUtils.waitForElement(driver, addtemplate, 60);
		//addtemplate.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		SkySiteUtils.waitTill(20000);						
		String Anyone = addtemplate.getText();
		SkySiteUtils.waitTill(5000);		
		if(Anyone.contains("Add template"))
		{
			Log.message("Add Template creation button  Validation verified sucessfully");
			return true;
		}
		else
		{
			Log.message("Add Template creation button  Validation Not verified sucessfully");
			return false;
		}
	}



public boolean TemplateSearch(String FolderPath,String Template) throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, addtemplate1, 60);
		addtemplate1.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		// ===External And Internal File Upload		
		SkySiteUtils.waitTill(10000);
		//outer Upload
	    UploadFiles_old(FolderPath);	    
	    Log.message("Upload Happen File Uploded Sucessfully");
	    SkySiteUtils.waitTill(25000);
		SkySiteUtils.waitForElement(driver, addtemplate, 60);	
		TemplateName.clear();
		 SkySiteUtils.waitTill(5000);
		TemplateName.sendKeys(Template);
		SkySiteUtils.waitTill(3000);
		String template =TemplateName.getAttribute("value");
		Log.message("Temp name is:--"+template);
		SkySiteUtils.waitTill(3000);		 
		
		SkySiteUtils.waitForElement(driver, savebutton, 60);
		savebutton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SearchTemplate_inbox.sendKeys(Template);		
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(5000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(40000);	
		//Reports.click();
		SkySiteUtils.waitTill(3000);
		String templatesearch = Aftercreation_template.getText();
		SkySiteUtils.waitTill(5000);		
		if(template.contains(templatesearch))
		{
			Log.message("Template Name Verified  Sucessfully");
			return true;
		}
		else
		{
			Log.message("Template Name Not Verified  Sucessfully");
			return false;
		}
	}




public boolean Template_Validation(String FolderPath,String Template) throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(10000);	
		SkySiteUtils.waitForElement(driver, Reports, 60);
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(15000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, addtemplate1, 60);
		addtemplate1.click();
		Log.message("Add template Link Clicked Sucessfully");
	
		// ===External And Internal File Upload		
		SkySiteUtils.waitTill(10000);
		//outer Upload
	    UploadFiles_old(FolderPath);	    
	    Log.message("Upload Happen File Uploded Sucessfully");
	    SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, addtemplate, 60);	
		TemplateName.clear();
		 SkySiteUtils.waitTill(5000);
		TemplateName.sendKeys(Template);
		SkySiteUtils.waitTill(5000);
		String template =TemplateName.getAttribute("value");
		Log.message("Temp name is:"+template);
		SkySiteUtils.waitTill(5000);		 
		
		SkySiteUtils.waitForElement(driver, savebutton, 60);
		savebutton.click();
		SkySiteUtils.waitTill(5000);
		SearchTemplate_inbox.sendKeys(Template);		
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(5000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(20000);
		
		String Template1 = Aftercreation_template.getText();
		SkySiteUtils.waitTill(5000);		
		if(template.contains(Template1))
		{
			Log.message("Template Name Verified  Sucessfully");
			return true;
		}
		else
		{
			Log.message("Template Name Not Verified  Sucessfully");
			return false;
		}
		
}



public boolean Export_Template_validation(String DownloadPath, String FolderName1) throws AWTException, IOException 
{

	SkySiteUtils.waitTill(5000);
	ExportButton.click();
	
	Log.message("Export button has been clicked");
	SkySiteUtils.waitTill(8000);

	// Get Browser name on run-time.
	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	String browserName = caps.getBrowserName();
	Log.message("Browser name on run-time is: " + browserName);

	if (browserName.contains("firefox")) 
	{
		// Handling Download PopUp of firefox browser using robot
		Robot robot = null;
		robot = new Robot();

		robot.keyPress(KeyEvent.VK_ALT);
		SkySiteUtils.waitTill(2000);
		robot.keyPress(KeyEvent.VK_S);
		SkySiteUtils.waitTill(2000);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_S);
		SkySiteUtils.waitTill(3000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(20000);
	}
	SkySiteUtils.waitTill(25000);
	// After checking whether download folder or not
	String ActualFoldername = null;

	File[] files = new File(DownloadPath).listFiles();

	for (File file : files)
	{
		if (file.isFile()) 
		{
			ActualFoldername = file.getName();// Getting Folder Name into a variable
			Log.message("Actual Folder name is:" + ActualFoldername);
			SkySiteUtils.waitTill(1000);
			Long ActualFolderSize = file.length();
			Log.message("Actual Folder size is:" + ActualFolderSize);
			SkySiteUtils.waitTill(5000);
			if (ActualFolderSize != 0) 
			{
				Log.message("Downloaded folder is available in downloads!!!");
			} 
			else
			{
				Log.message("Downloaded folder is NOT available in downloads!!!");
			}
		}
	}
	if (ActualFoldername.contains(FolderName1))
		return true;
	else
		return false;
}


public boolean Export_Templates_Validate_validate_Template(String Randome_TemplateName,String Input_ApprovalType,String Sys_Download_Path) throws IOException, AWTException, InterruptedException 

{

    boolean result1 = false;
	boolean result2 = false;

	//Counting available list of templates

    int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
    Log.message("Available Templates are : "+NoOf_Templates);
	if (NoOf_Templates>=1)
	{
		Log.message("Templates are available. User can Export.");		
		Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		//SkySiteUtils.waitTill(5000);
		//driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);
		
		WebElement element = ExportButton;
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		//ExportButton.click();
		Log.message("Clicked on Export button.");
		SkySiteUtils.waitTill(25000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
        {

		// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		    }
		   //Actual file checking After Download
		    String ActualFilename = null;
		    File[] files = new File(Sys_Download_Path).listFiles();
		    for (File file : files) 
		     {
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains("Template_export_")) && (ActualFileSize != 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				 }	}

		        }

		       String usernamedir=System.getProperty("user.name");
  		       String XLSXFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\"+ActualFilename;
  		       Log.message("Reading values from XLSX file is: "+XLSXFileToRead);
  		//Reading data from XLSX file
  		File XLSXfile = new File(XLSXFileToRead);
 		FileInputStream inputStream = new FileInputStream(XLSXfile);
  		XSSFWorkbook my_xls_workbook = new XSSFWorkbook(inputStream);
  		XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
		int RowCount_HaveData = my_worksheet.getLastRowNum()-my_worksheet.getFirstRowNum();// row count 
		Log.message("Row count is Display: "+RowCount_HaveData);
		int i = 0;
		int SuccessCount = 0;
		for(i=1;i<=RowCount_HaveData;i++)
		{
			String Template_Name = my_worksheet.getRow(i).getCell(0).getStringCellValue();
			Log.message("Actual Template Name is: "+Template_Name);
			String Approval_Type = my_worksheet.getRow(i).getCell(2).getStringCellValue();
			Log.message("Actual Approval Type is: "+Approval_Type);
			//String Report_Count = my_worksheet.getRow(i).getCell(6).getStringCellValue();

			//String Report_Count = NumberToTextConverter.toText(my_worksheet.getRow(i).getCell(2).getNumericCellValue());

			//Log.message("Actual Report count is: "+Report_Count);
			String Creater_Name = my_worksheet.getRow(i).getCell(7).getStringCellValue();

			Log.message("Actual Creater name is: "+Creater_Name);

			if((Template_Name.contentEquals(Randome_TemplateName)) )

			{
				SuccessCount=SuccessCount+1;
				Log.message("Success count is: "+SuccessCount);
			}

		}
		//Close the stream
		inputStream.close();
		//  (RowCount_HaveData==NoOf_Templates)
		if((SuccessCount==1))
		{
			result2 = true;
			Log.message("Expected  recards are available ");

		}

	} 

	if((result1==true) && (result2==true))

	{

		Log.message("Template Export is working sucessfully");

		return true;

	}

	else

	{

		Log.message("Template Export is working Sucessfully");

		return false;

	}

}

public boolean Employee_Template_Any_One_Approve(String Template) throws InterruptedException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(5000);
		SearchTemplate_inbox.sendKeys(Template);		
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(20000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(20000);		
		SkySiteUtils.waitForElement(driver, create, 60);
		create.click();
		Log.message("create Link Clicked Sucessfully");
		SkySiteUtils.waitTill(50000);
		String comment = PropertyReader.getProperty("employee_comment");
		txtcomment.sendKeys(comment);
		Log.message("comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnSubmit.click();
		Log.message("submit button has been clicked");
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("yes button has been clicked");
		SkySiteUtils.waitForElement(driver, Pending, 60);
		Pending.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Pending Button Clicked Sucessfully");
		ApproveButton.click();		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Approve_Status, 60);
		 if(Approve_Status.isDisplayed())
		    	
		    {	
		    	Log.message("Approval Status  verified Sucessfully");
				return true;
		    }
			else
			{
				Log.message("Approval Status Not verified Sucessfully");
				return false;
			}
		
	}


public boolean Employee_Reject_report(String Template) throws InterruptedException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(5000);
		SearchTemplate_inbox.sendKeys(Template);		
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(20000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(20000);		
		SkySiteUtils.waitForElement(driver, create, 60);
		create.click();
		Log.message("create Link Clicked Sucessfully");
		SkySiteUtils.waitTill(60000);
		String comment = PropertyReader.getProperty("employee_comment");
		txtcomment.sendKeys(comment);
		Log.message("comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnSubmit.click();
		Log.message("submit button has been clicked");
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		btnyes.click();
		SkySiteUtils.waitTill(5000);
		Log.message("yes button has been clicked");
		SkySiteUtils.waitForElement(driver, Pending, 60);
		Pending.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Pending Button Clicked Sucessfully");
		RejectButton.click();		
		SkySiteUtils.waitTill(10000);
		
		return true;
	}



public boolean resubmit_report(String Template) throws InterruptedException 
{		
		SkySiteUtils.waitTill(5000);		
		Reports.click();
		Log.message("Report Link Clicked Sucessfully");
		SkySiteUtils.waitTill(25000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		SkySiteUtils.waitTill(5000);		
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(5000);
		SearchTemplate_inbox.sendKeys(Template);		
		Log.message("Entered the Template Name in search Inbox field :-"+Template);
		SkySiteUtils.waitTill(20000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		SkySiteUtils.waitTill(20000);		
		SkySiteUtils.waitForElement(driver, create, 60);
		create.click();
		Log.message("create Link Clicked Sucessfully");
		SkySiteUtils.waitTill(50000);
		String comment = PropertyReader.getProperty("employee_comment");
		txtcomment.sendKeys(comment);
		Log.message("comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnSubmit.click();
		Log.message("submit button has been clicked");
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("yes button has been clicked");
		SkySiteUtils.waitForElement(driver, Pending, 60);
		Pending.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Pending Button Clicked Sucessfully");
		RejectButton.click();		
		SkySiteUtils.waitTill(5000);
		/*SkySiteUtils.waitForElement(driver, Approve_Status, 60);
		 if(Approve_Status.isDisplayed())
		    	
		    {	
		    	Log.message("Approval Status  verified Sucessfully");
				return true;
		    }
			else
			{
				Log.message("Approval Status Not verified Sucessfully");
				return false;
			}*/
		return true;
	}





public boolean DuplicateReport_validation(String Template) throws InterruptedException 
{		
		
		SkySiteUtils.waitTill(10000);		
		SkySiteUtils.waitForElement(driver, create, 60);
		create.click();
		Log.message("create Link Clicked Sucessfully");
		SkySiteUtils.waitTill(50000);
		String comment = PropertyReader.getProperty("employee_comment");
		txtcomment.sendKeys(comment);
		Log.message("comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnSubmit.click();
		Log.message("submit button has been clicked");
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("yes button has been clicked");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Pending, 60);
		Pending.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Pending Button Clicked Sucessfully");
		DuplicateButton.click();		
		SkySiteUtils.waitTill(15000);
		//String reportNum = ReportNo.getAttribute("Value");
		String reportNum1 = driver.findElement(By.xpath("//input [@class='form-control ng-untouched ng-pristine']")).getAttribute("value");
		Log.message("Duplicate Button Clicked Sucessfully1:"+reportNum1);
		SkySiteUtils.waitTill(5000);
		//SkySiteUtils.waitForElement(driver, Approve_Status, 60);
		 if(reportNum1.equals("2"))
		    	
		    {	
		    	Log.message("Duplicate reports creation  verified Sucessfully");
				return true;
		    }
			else
			{
				Log.message("Duplicate reports creation Not verified Sucessfully");
				return false;
			}
		
	}



public boolean AttachPhoto() throws InterruptedException, AWTException, IOException 
{			
		SkySiteUtils.waitTill(10000);		
		SkySiteUtils.waitForElement(driver, create, 60);
		create.click();
		SkySiteUtils.waitTill(30000);	
		Log.message("create Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Addphoto, 60);
		SkySiteUtils.waitTill(5000);		
		Addphoto.click();
		Log.message("add Photo Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AddComputer, 60);
		AddComputer.click();
		SkySiteUtils.waitTill(5000);
		Log.message("add Computor Link Clicked Sucessfully");		
		File Path=new File(PropertyReader.getProperty("Upload_TestData_photo_SingleFile"));				
		String FolderPath = Path.getAbsolutePath().toString();
		UploadPhoto(FolderPath);
		
		SkySiteUtils.waitTill(50000);
		String comment = PropertyReader.getProperty("employee_comment");
		txtcomment.sendKeys(comment);
		Log.message("comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnSubmit.click();
		Log.message("submit button has been clicked");
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("yes button has been clicked");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Pending, 60);
		Pending.click();
		Log.message("Pending Button List clicked");
		SkySiteUtils.waitTill(10000);
		PhotoArrow.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Photo Arrow Button clicked");
		SkySiteUtils.waitTill(10000);		
		//SkySiteUtils.waitForElement(driver, downloadButton, 60);
		//downloadButton.click();
		//Log.message("Download Button Clicked Sucessfully");		
		SkySiteUtils.waitForElement(driver, Photo_layer, 60);
		if(Photo_layer.isDisplayed())
		{		
		Log.message("Photo Attached Visible Sucessfully");	
		return true;
		}
		else
		{
		Log.message("Photo Attached Not Visible  Sucessfully");	
		return false;	
		}
		
	   }


public boolean AttachlinkFile() throws InterruptedException, AWTException, IOException 
{			
		SkySiteUtils.waitTill(10000);		
		SkySiteUtils.waitForElement(driver, create, 60);
		create.click();
		SkySiteUtils.waitTill(30000);	
		Log.message("create Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Addfile, 60);
		SkySiteUtils.waitTill(5000);		
		Addfile.click();
		Log.message("add Photo Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AddComputer, 60);
		AddComputer.click();
		SkySiteUtils.waitTill(5000);
		Log.message("add Computor Link Clicked Sucessfully");		
		File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
		String FolderPath = Path.getAbsolutePath().toString();
		UploadPhoto(FolderPath);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitTill(30000);
		String comment = PropertyReader.getProperty("employee_comment");
		txtcomment.sendKeys(comment);
		Log.message("comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnSubmit.click();
		Log.message("submit button has been clicked");
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("yes button has been clicked");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Pending, 60);
		Pending.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Pending Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Arrow_third, 60);
		Arrow_third.click();
		Log.message("Arrow Button Clicked Sucessfully");		
	    String file = FileText.getText();
		String file_resource =PropertyReader.getProperty("FileUpload");
		
		if(file.contains(file_resource))
		{		
		Log.message("File Attached Visible Sucessfully");	
		return true;
		}
		else
		{
		Log.message("File Attached Not Visible  Sucessfully");	
		return false;	
		}
		
	   }



public boolean DeleteTemplate() throws InterruptedException, AWTException, IOException, ParseException 
{		
		
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		//Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, Deletetemplate, 60);
		SkySiteUtils.waitTill(5000);
		Deletetemplate.click();
		Log.message("Delete Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, yes_button, 60);
		yes_button.click();
		SkySiteUtils.waitForElement(driver, content_validation, 60);
		SkySiteUtils.waitTill(5000);
		if(content_validation.isDisplayed())
			return true;
		else
			return false;
	
		
	}
	

	

public boolean EditTemplate() throws InterruptedException, AWTException, IOException, ParseException 
{		
		SkySiteUtils.waitTill(5000);		
		SkySiteUtils.waitForElement(driver, Edittemplate, 60);
		Edittemplate.click();
		Log.message("Edit Template Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, EditTemplateName, 60);
		EditTemplateName.clear();
		Log.message("Edit Template Inbox cleared");
	    SkySiteUtils.waitTill(5000);
	    String temp = PropertyReader.getProperty("Template");
	    EditTemplateName.sendKeys(temp);
	    SkySiteUtils.waitTill(5000);
		//String template =EditTemplateName.getAttribute("value");
		Log.message("Enter the Value In edit template field");
		Log.message("Temp name is:"+temp);
		 SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, savebutton, 60);
		savebutton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		String Template = Aftercreation_template.getText();
		Log.message("After creation template get text()"+ Template);
		SkySiteUtils.waitTill(5000);		
		if(temp.contains(Template))
		{
			Log.message("Template Name Verified  Sucessfully");
			return true;
		}
		else
		{
			Log.message("Template Name Not Verified  Sucessfully");
		   return false;
		}
	}
	
	

public void UploadFiles_old(String FolderPath)throws InterruptedException, AWTException, IOException {
	//boolean result = false;
	SkySiteUtils.waitTill(5000);
	Log.message("Waiting for upload file button to be appeared");
	SkySiteUtils.waitForElement(driver, ChooseaFile, 120);
	SkySiteUtils.waitTill(5000);
	ChooseaFile.click();
	Log.message("choose button has been clicked");
	SkySiteUtils.waitTill(10000);
	// =================================
	// Writing File names into a text file for using in AutoIT Script
	
	BufferedWriter output;
	randomFileName rn = new randomFileName();
	//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
	String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
	output = new BufferedWriter(new FileWriter(tmpFileName, true));
	
	String expFilename = null;
	File[] files = new File(FolderPath).listFiles();
	
	for (File file : files) 
	{
		if (file.isFile())
		{
			expFilename = file.getName();// Getting File Names into a variable
			Log.message("Expected File name is:" + expFilename);
			output.append('"' + expFilename + '"');
			output.append(" ");
			SkySiteUtils.waitTill(5000);
		}
	}
	output.flush();
	output.close();
	String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
	// Executing .exe autoIt file
	Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
	Log.message("AutoIT Script Executed!!");
	SkySiteUtils.waitTill(30000);
	
	try 
	{
		File file = new File(tmpFileName);
		if (file.delete()) 
		{
			Log.message(file.getName() + " is deleted!");
		}
		else
		{
			Log.message("Delete operation is failed.");
		}
	}
	catch (Exception e)
	{
		Log.message("Exception occured!!!" + e);
	}
	SkySiteUtils.waitTill(5000);	
}


public void UploadPhoto(String FolderPath)throws InterruptedException, AWTException, IOException {
	//boolean result = false;
	SkySiteUtils.waitTill(5000);	
	// =================================
	// Writing File names into a text file for using in AutoIT Script
	
	BufferedWriter output;
	randomFileName rn = new randomFileName();
	//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
	String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
	output = new BufferedWriter(new FileWriter(tmpFileName, true));
	
	String expFilename = null;
	File[] files = new File(FolderPath).listFiles();
	
	for (File file : files) 
	{
		if (file.isFile())
		{
			expFilename = file.getName();// Getting File Names into a variable
			Log.message("Expected File name is:" + expFilename);
			output.append('"' + expFilename + '"');
			output.append(" ");
			SkySiteUtils.waitTill(5000);
		}
	}
	output.flush();
	output.close();
	String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
	// Executing .exe autoIt file
	Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
	Log.message("AutoIT Script Executed!!");
	SkySiteUtils.waitTill(30000);
	
	try 
	{
		File file = new File(tmpFileName);
		if (file.delete()) 
		{
			Log.message(file.getName() + " is deleted!");
		}
		else
		{
			Log.message("Delete operation is failed.");
		}
	}
	catch (Exception e)
	{
		Log.message("Exception occured!!!" + e);
	}
	SkySiteUtils.waitTill(5000);	
}



public boolean DownloadFiledownload(String Downloadpath) throws InterruptedException, AWTException, IOException {

	boolean result1 = false;
	SkySiteUtils.waitTill(10000);
	// Calling "Deleting download folder files" method
	String Template = Aftercreation_template.getText();
	String fileName = Template +".pdf";
	Log.message("Download Path is: " + Downloadpath);
	
	SkySiteUtils.waitTill(5000);
	Delete_ExistedFiles_From_DownloadFolder(Downloadpath);
	SkySiteUtils.waitTill(10000);
	SkySiteUtils.waitForElement(driver, Download_Link, 120);
	Download_Link.click();
	Log.message("Clicked on download icon from RFI creationed icon for download- Files Downloded Sucessfully");

	// Get Browser name on run-time.
	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	String browserName = caps.getBrowserName();
	Log.message("Browser name on run-time is: " + browserName);

	if (browserName.contains("firefox")) {
		// Handling Download PopUp of firefox browser using robot
		Robot robot = null;
		robot = new Robot();
		robot.keyPress(KeyEvent.VK_ALT);
		SkySiteUtils.waitTill(2000);
		robot.keyPress(KeyEvent.VK_S);
		SkySiteUtils.waitTill(2000);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_S);
		SkySiteUtils.waitTill(3000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);
	}
	SkySiteUtils.waitTill(40000);
	// After Download, checking whether Downloaded file is existed under
	// download folder or not
	String ActualFilename = null;
	
	File[] files = new File(Downloadpath).listFiles();
	
	//File dwFile = new File(Downloadpath) ;
	//Log.message("===============start clean ====================");
	//SkySiteUtils.waitTill(50000);
	//FileUtils.cleanDirectory(dwFile);
	
	for (File file : files)
	{
		if (file.isFile()) 
		{
			ActualFilename = file.getName();// Getting Folder Name into a variable
			Log.message("Actual Folder name is:" + ActualFilename);
			SkySiteUtils.waitTill(1000);
			Long ActualFolderSize = file.length();
			Log.message("Actual Folder size is:" + ActualFolderSize);
			SkySiteUtils.waitTill(5000);
			if (ActualFolderSize != 0) 
			{
				result1=true;
				Log.message("Downloaded folder is available in downloads!!!");
			} 
			else
			{
				result1=false;
				Log.message("Downloaded folder is NOT available in downloads!!!");
			}
		}
	}
	if(result1==true)
		return true;
	else
		return false;		
}


/** 
 * Method written for Deleting Existed files from the download folder.
 * Scripted By : NARESH BABU
 * @return
 */
//Deleting files from a folder
public void Delete_ExistedFiles_From_DownloadFolder(String Sys_Download_Path)throws InterruptedException 
{	
	try 
	{ 
		Log.message("Going to Clean existed files from download folder!!!");
		File file = new File(Sys_Download_Path);      
	    String[] myFiles;    
	    if(file.isDirectory())
	    {
	    	myFiles = file.list();
	        for (int i=0; i<myFiles.length; i++) 
	        {
	        	File myFile = new File(file, myFiles[i]); 
	            myFile.delete();
	            //SkySiteUtils.waitTill(2000);
	        }
	        Log.message("Available Folders/Files are deleted from download folder successfully!!!");
	    }
	           
	}//End of try block

	catch(Exception e)
	{
		Log.message("Unable to delete Available Folders/File from download folder!!!"+e);
	}
}

	
}
