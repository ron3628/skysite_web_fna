package com.arc.fna.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FnaRecyclebinTabPage extends LoadableComponent<FnaRecyclebinTabPage>{
	   WebDriver driver;
	    private  boolean isPageLoaded;
	    
	    /**
		 * Identifying web elements using FindfBy annotation.
		 */ 
	  @FindBy(css="#btnRecycleSetting")
	  WebElement settingbtn;
	    
	    
	    
	    
	    
	    @Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver,settingbtn,30);
		driver.switchTo().defaultContent();
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	public FnaRecyclebinTabPage(WebDriver driver) {
	    this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	/** Method to check Manage Recycle bin page is landed
	 * 
	 */
	public boolean RecycleBinPageLanded() {
		driver.switchTo().defaultContent();
		Log.message("SWitched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(5000);
		if(settingbtn.isDisplayed()) {
			Log.message("Setting button is displayed and Recycle bin tab is opened");
			return true;
		}
		else {
			return false;
		}
		
	}
	/**Method to verify the deleted folder in recycle bin
	 * Scripted by : Tarak
	 */
	public boolean verifyDeletedFolderInRecycleBin(String foldername) {
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(4000);
	    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("Total list count is:"+Count_list);			 
			//Loop start '2' as projects list start from tr[2]
			int i=0;
			for(i = 1; i <= Count_list; i++) 
			{
					
                String foldernamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				//Log.message("Contact name is:"+contactName);
				SkySiteUtils.waitTill(5000);			
				//Checking contact name equal with each row of table
				if(foldernamepresent.contains(foldername))
				{	   
					Log.message("folder name matched is : " +foldernamepresent);
					Log.message("Deleted Folder Presence Validated Successfully!!!");		                  
					break;
				}		
			}
			String foldernamepresent12=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			Log.message("****Folder name is**** " +foldernamepresent12);
			SkySiteUtils.waitTill(3000);
			if(foldernamepresent12.contains(foldername))	
				return true;
			else
				return false;
	
	}
	/** Method to verify deleted file in the recyclebin
	 * Scripted by : Tarak
	 */
		public boolean verifyDeletedFileinRecycleBin(String foldername) {
			driver.switchTo().defaultContent();
				Log.message("Swtiched to default content");
				SkySiteUtils.waitTill(4000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				Log.message("Switched to frame");
				SkySiteUtils.waitTill(4000);
				String Deletedfilename=PropertyReader.getProperty("DeleteFilename");
				Log.message("Deleted file to be searched in recycle bin !! " +Deletedfilename);
			    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
					Log.message("Total list count is:"+Count_list);			 
					//Loop start '2' as projects list start from tr[2]
					int i=0;
					for(i = 1; i <= Count_list; i++) 
					{
							
						String filepresentinrecyclebin=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
						//Log.message("Contact name is:"+contactName);
						String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+i+"]")).getText();
						SkySiteUtils.waitTill(5000);			
						//Checking contact name equal with each row of table
						if(filepresentinrecyclebin.contains(Deletedfilename)&&pathfoldername.contains(foldername))
						{	   
							Log.message("Deleted file name : " +filepresentinrecyclebin+ " Found in the list and the folder path of the file is  " +pathfoldername);
							Log.message("Deleted File Presence Validated Successfully!!!");		                  
							break;
						}		
					}
					String filepresentafterdeletion=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					//Log.message("****Folder name is**** " +foldernamepresent12);
					SkySiteUtils.waitTill(3000);
					if(filepresentafterdeletion.contains(Deletedfilename))	
						return true;
					else
						return false;
	
		}
		@FindBy(xpath=".//*[@id='ProjectFiles']/a")
		WebElement Documents;
		/**Method to restore deleted folder
		 * Scripted By:Tarak
		 */
		public FnaHomePage restoreDeletedFolder(String foldername) {
			driver.switchTo().defaultContent();
			Log.message("Swtiched to default content");
			SkySiteUtils.waitTill(4000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switched to frame");
			SkySiteUtils.waitTill(4000);
		    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);			 
				//Loop start '2' as projects list start from tr[2]
				int i=0;
				for(i = 1; i <= Count_list; i++) 
				{
						
					String foldernamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					//Log.message("Contact name is:"+contactName);
					SkySiteUtils.waitTill(5000);			
					//Checking contact name equal with each row of table
					if(foldernamepresent.contains(foldername))
					{	   
						
						
						Log.message("folder name matched is : " +foldernamepresent);
						Log.message("Deleted Folder Presence Validated Successfully!!!");		                  
						break;
					}		
				}
				String foldernamepresent12=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				Log.message("****Folder name is**** " +foldernamepresent12);
				SkySiteUtils.waitTill(5000);
				WebElement restorebutton=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[8]/a)["+i+"]"));
				restorebutton.click();
				SkySiteUtils.waitTill(6000);
				Log.message("Restore button has been clicked");
				SkySiteUtils.waitTill(6000);
			    driver.switchTo().defaultContent();
			    Log.message("Switching to default content");
			    Documents.click();
			    Log.message("Documents is clicked");
			    SkySiteUtils.waitTill(8000);
			    return new FnaHomePage(driver).get();
			   }
		/**Method to restore deleted file
		 * Scripted By : Tarak
		 */
		public FnaHomePage restoreDeletedFile(String foldername) {
			
			driver.switchTo().defaultContent();
			Log.message("Swtiched to default content");
			SkySiteUtils.waitTill(4000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switched to frame");
			SkySiteUtils.waitTill(4000);
			String Deletedfilename=PropertyReader.getProperty("DeleteFilename");
		    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);			 
				//Loop start '2' as projects list start from tr[2]
				int i=0;
				for(i = 1; i <= Count_list; i++) 
				{
						
					String filenamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					//Log.message("Contact name is:"+contactName);
					String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+i+"]")).getText();
					
					SkySiteUtils.waitTill(5000);			
					//Checking contact name equal with each row of table
					if(filenamepresent.contains(Deletedfilename)&&pathfoldername.contains(foldername))
					{	   
						
						
						Log.message("file name matched is : " +filenamepresent+ " Under the folder path : " +pathfoldername);
						Log.message("Deleted file Presence Validated Successfully!!!");		                  
						break;
					}		
				}
				String filenamepresent12=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				Log.message("****Deleted file name is**** " +filenamepresent12);
				SkySiteUtils.waitTill(5000);
				WebElement restorebutton=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[8]/a)["+i+"]"));
				restorebutton.click();
				SkySiteUtils.waitTill(6000);
				Log.message("Restore button has been clicked");
				SkySiteUtils.waitTill(6000);
			    driver.switchTo().defaultContent();
			    Log.message("Switching to default content");
			    Documents.click();
			    Log.message("Documents is clicked");
			    SkySiteUtils.waitTill(8000);
			    return new FnaHomePage(driver).get();
			
		 }
		  @FindBy(xpath="//*[@id='button-1']")
		    WebElement btnyes;
		  @FindBy(xpath=".//*[@id='button-0']")
		  WebElement btnno;

		/**Method for Single Delete in Recycle bin for deleted folder and verify folder is not present after delete
		 * Scripted By : Tarak
		 */
		public boolean singleDeleteForDeletedFolder(String Foldername) {
			driver.switchTo().defaultContent();
			Log.message("Swtiched to default content");
			SkySiteUtils.waitTill(4000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switched to frame");
			SkySiteUtils.waitTill(4000);
		    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);			 
				//Loop start '2' as projects list start from tr[2]
				int i=0;
				for(i = 1; i <= Count_list; i++) 
				{
						
					String foldernamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					//Log.message("Contact name is:"+contactName);
					SkySiteUtils.waitTill(5000);			
					//Checking contact name equal with each row of table
					if(foldernamepresent.contains(Foldername))
					{	   
						
						Log.message("Deleted Folder Presence Validated Successfully!!!");		                  
						break;
					}		
				}
				String foldernamepresent12=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				Log.message("Deleted Folder name present in list is**** " +foldernamepresent12);
				SkySiteUtils.waitTill(3000);
				WebElement Deletebtn=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[9]/a)["+i+"]"));
				Deletebtn.click();
				Log.message("Delete button is clicked for the folder " +foldernamepresent12);
				SkySiteUtils.waitTill(6000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(6000);
				btnyes.click();
				Log.message("Yes button clicked");
				SkySiteUtils.waitTill(5000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				SkySiteUtils.waitTill(6000);
				Log.message("Search the deleted folder in the list ");
				int Noofrows=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Noofrows);			
				List<WebElement> mylist=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]"));
			    //List containing the folder path of files and folder
				List<WebElement> mylistpath=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span"));
		         //Check after deletion folder or file are present in the list
			    for(WebElement var:mylist)	
			    	for(WebElement path:mylistpath)
			           if(var.getText().contains(Foldername)&&path.getText().contains(Foldername)){
						Log.message("Folder is present in the list even after clicking the single delete button");;
						return false;
					}
					return true;
				}

		
		@FindBy(css="#txtSearchRecycle")
		@CacheLookup
		WebElement searchbox;
		@FindBy(xpath=".//*[@id='aspnetForm']/div[5]/nav/div[2]/div/div/button")
		WebElement dropdownbutton;
		@FindBy(xpath=".//*[@id='aspnetForm']/div[5]/nav/div[2]/div/div/div/ul/li[1]/a")
		WebElement Folderoption;
		@FindBy(xpath=".//*[@id='aspnetForm']/div[5]/nav/div[2]/div/div/div/ul/li[2]/a")
		WebElement  fileoption;
		@FindBy(xpath=".//*[@id='btnSearch']")
        WebElement searchbtn;
		
		@FindBy(xpath="//button[@id='btnResetSearch']")
        WebElement resetRecycle;

		
		/** Method to verify search with file
		 * Scripted By : Tarak
		 * Modified by Trinanjwan | 21-Nov-2018
		 */
		public boolean  searchAndVerifyWithFileName(String Foldername,String filename) {
			
			
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(6000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			SkySiteUtils.waitTill(5000);
			Log.message("Switched to frame");
			SkySiteUtils.waitTill(5000);
			resetRecycle.click();
			Log.message("Clicked on Reset button to avoid stale element exception");
			SkySiteUtils.waitTill(6000);
			searchbox.sendKeys(filename);
			Log.message("file name entered in the search box is " +filename);
			SkySiteUtils.waitTill(4000);
			dropdownbutton.click();
			Log.message("dropdown button hs been clicked");
			SkySiteUtils.waitTill(5000);
		    fileoption.click();
			Log.message("file option has been selected");
			SkySiteUtils.waitTill(5000);
			searchbtn.click();
			Log.message("Search button is clicked");
			Log.message("Deleted file to be searched  !! " +filename);
		    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);			 
				//Loop start '2' as projects list start from tr[2]
				int i=0;
				for(i = 1; i <= Count_list; i++) 
				{
						
					String filepresentinrecyclebin=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					//Log.message("Contact name is:"+contactName);
					String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+i+"]")).getText();
					SkySiteUtils.waitTill(5000);			
					//Checking contact name equal with each row of table
					if(filepresentinrecyclebin.contains(filename)&&pathfoldername.contains(Foldername))
					{	   
						Log.message("Deleted file name : " +filepresentinrecyclebin+ " Found in the list and the folder path of the file is  " +pathfoldername);
						Log.message("Deleted File Search Validated Successfully!!!");		                  
						break;
					}		
				}
				String filepresentafterdeletion=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				//Log.message("****Folder name is**** " +foldernamepresent12);
				SkySiteUtils.waitTill(3000);
				if(filepresentafterdeletion.contains(filename))	
					return true;
				else
					return false;

	    }
		/**Method to search the deleted folder
		 * Scripted By : Tarak
		 * Modified by Trinanjwan | 21-Nov-2018
		 */
		public boolean searchAndVerifyWithFolderName(String foldername) {

			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(6000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			SkySiteUtils.waitTill(5000);
			Log.message("Switched to frame");
			SkySiteUtils.waitTill(5000);
			resetRecycle.click();
			Log.message("Clicked on Reset button to avoid stale element exception");
			SkySiteUtils.waitTill(6000);
			searchbox.sendKeys(foldername);
			Log.message("folder name entered in the search box is " +foldername);
			SkySiteUtils.waitTill(4000);
			dropdownbutton.click();
			Log.message("dropdown button hs been clicked");
			SkySiteUtils.waitTill(5000);
			Folderoption.click();
			Log.message("Folder option has been selected");
			searchbtn.click();
			Log.message("Search button is clicked");
			int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);	
				SkySiteUtils.waitTill(4000);
				//Loop start '2' as projects list start from tr[2]
				int i=0;
				for(i = 1; i <= Count_list; i++) 
				{
						
					String foldernamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
					//Log.message("Contact name is:"+contactName);
					SkySiteUtils.waitTill(5000);			
					String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+i+"]")).getText();
					//Checking contact name equal with each row of table
					if(foldernamepresent.contains(foldername)&&pathfoldername.contains(foldername))
					{	   
						Log.message("folder name matched is : " +foldernamepresent);
						Log.message("Deleted Folder Presence Validated Successfully using search criteria!!!");		                  
						break;
					}		
				}
				String foldernamepresent12=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				Log.message("****Folder name is**** " +foldernamepresent12);
				SkySiteUtils.waitTill(3000);
				if(foldernamepresent12.contains(foldername))	
					return true;
				else
					return false;
			}
		@FindBy(css="#btnResetSearch")
		@CacheLookup
		WebElement resetbtn;
		@FindBy(xpath=".//*[@id='aspnetForm']/div[5]/nav/div[2]/div/div/button")
		@CacheLookup
		WebElement defaultdropdownbtn;
		/**Method to verify search reset option
		 * Scripted By : Tarak
		 */
		
		public boolean resetOptionInRecycleBin() {
			driver.switchTo().defaultContent();
			Log.message("Switching to default content");
			SkySiteUtils.waitTill(4000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switch to frame");
			SkySiteUtils.waitTill(4000);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(6000);
			String defaultvalue=defaultdropdownbtn.getAttribute("title");
			Log.message("Value selected by default is " +defaultvalue);
        	if(defaultvalue.contains("File")&&searchbox.getText().isEmpty()){
        		Log.message("Search box is cleared also File option selected by default and search  reset functioanlity is working ");
        		return true;
        		
        	}
             return false;
        }
		@FindBy(xpath=".//*[@id='liRecycle']/a")
		WebElement Recyclebinbtn;
		@FindBy(xpath=".//*[@id='rdDeletePermanently']")
		WebElement Permanentdelete;
		@FindBy(xpath=".//*[@id='btnSaveRecycleSettings']")
		WebElement savebutton;
		/** Method to select Delete Permanent button of setting tab
		 * Scripted By : Tarak
		 */
		public void selectDeletePermanentOption() {
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(5000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("switch to frame ");
			SkySiteUtils.waitTill(7000);
			settingbtn.click();
			Log.message("Setting button is clicked");
			SkySiteUtils.waitTill(6000);
		    Permanentdelete.click();
			Log.message("Permanent Delete button selected");
			SkySiteUtils.waitTill(4000);
			savebutton.click();
			Log.message("Save button is clicked");
			SkySiteUtils.waitTill(9000);
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(6000);
		}
		/** Method to Folder deleted permanently on selecting the delete permanent button
		 * Scripted By : Tarak
		 */
		public boolean verifyfolderdeletedpermanently(String foldername) {
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(5000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("switch to frame ");
			SkySiteUtils.waitTill(7000);
			SkySiteUtils.waitTill(4000);
		    int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);			 
				//Loop start '2' as projects list start from tr[2]
				List<WebElement> mylist=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]"));
			    //List containing the folder path of files and folder
			List<WebElement> mylistpath=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span"));
		         //Check after deletion folder or file are present in the list
			    for(WebElement var:mylist)	
			    	for(WebElement path:mylistpath)
			           if((var.getText().contains(foldername)&&path.getText().contains(foldername))){
						Log.message("Folder deleted is still present in recyle bin");;
						return false;
					}
					return true;
		}
		 @FindBy(xpath=".//*[@id='rdMoveToRecycle']")
		 WebElement Movetorecyclebinbtn;
		
		/** Method to select Move to recyle bin button on deletion
		 * Scripted By : Tarak
		 */
	
		public void selectMovetoRecyclebinbtn() {
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(5000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("switch to frame ");
			SkySiteUtils.waitTill(7000);
			settingbtn.click();
			Log.message("Setting button is clicked");
			SkySiteUtils.waitTill(6000);
			Movetorecyclebinbtn.click();
			Log.message("Move to Recycle bin selected");
			SkySiteUtils.waitTill(4000);
			savebutton.click();
			Log.message("Save button is clicked");
			SkySiteUtils.waitTill(9000);
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(6000);
		}
		@FindBy(xpath=".//*[@id='btnDelete']")
		WebElement deletebtn;
		/**Method to select and delete multiple folder and delete from recycle bin
		 * @return 
		 * Scripted by:Tarak
		 */
		public boolean selectFolderFileAndDelete(String foldername1,String Foldername1) {
			  driver.switchTo().defaultContent();
			  Log.message("Swtiched to default content");
			  SkySiteUtils.waitTill(4000);
			  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			  Log.message("Switched to frame");
			  SkySiteUtils.waitTill(4000);
			   String Deletedfilename=PropertyReader.getProperty("DeleteFilename");
			   Log.message("Deleted file to be searched in recycle bin !! " +Deletedfilename);
		       int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count is:"+Count_list);			 
				//Loop start '2' as projects list start from tr[2]
				int i=0;
				for(i = 1; i <= Count_list; i++) 
				{
						
					String foldernamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	
					SkySiteUtils.waitTill(5000);			
					//Checking contact name equal with each row of table
					if(foldernamepresent.contains(foldername1))
					{	   
						Log.message(" 1st folder name matched is : " +foldernamepresent);
						Log.message("Deleted Folder Presence Validated Successfully!!!");		                  
						break;
					}		
				}
				String foldernamepresent12=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				Log.message("folder 1 matched is " +foldernamepresent12);
				SkySiteUtils.waitTill(4000);
		        WebElement checkbox=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img)["+i+"]"));
		        checkbox.click();
		        SkySiteUtils.waitTill(4000);
		        Log.message("Deleted folder is selected");
		        int j=0;
				for(j = 1; j <= Count_list; j++) 
				{
						
					String filenamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
			
					String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+j+"]")).getText();
					SkySiteUtils.waitTill(5000);			
		
					if(filenamepresent.contains(Deletedfilename)&&pathfoldername.contains(Foldername1))
					{	   
						Log.message(" file name matched is : " +filenamepresent);
						Log.message("Deleted file Presence Validated Successfully!!!");		                  
						break;
					}		
				}
				String filenamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
				Log.message("File matched  is " +filenamepresent);
				String pathfoldername12=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+j+"]")).getText();
				Log.message("Path of the file is " +pathfoldername12);
				SkySiteUtils.waitTill(4000);
		        WebElement checkbox2=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img)["+j+"]"));
		        checkbox2.click();
		        Log.message("Deleted file is selected");
		       deletebtn.click();
		        Log.message("Delete button is clicked");
		       SkySiteUtils.waitTill(8000);
		        driver.switchTo().defaultContent();
		       Log.message("Logged into default content to click the yes button");
		        SkySiteUtils.waitTill(3000);
		       btnyes.click();
		       Log.message("Yes button is  clicked");
		        SkySiteUtils.waitTill(9000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				Log.message("Switched back to frame");
				int Count_item=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
				Log.message("Total list count after deletion  is:"+Count_item);			 
			    //List for the recycle bin item
				List<WebElement> mylist=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]"));
			    //List containing the folder path of files and folder
				List<WebElement> mylistpath=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span"));
		         //Check after deletion folder or file are present in the list
			    for(WebElement var:mylist)	
			    	for(WebElement path:mylistpath)
			           if(var.getText().contains(foldername1)||(var.getText().contains(Deletedfilename)&&path.getText().contains(Foldername1))){
						Log.message("Folder or file is present in the list even after clicking the delete button");;
						return false;
					}
					return true;
				}

		
		 @FindBy(xpath=".//*[@id='btnProjectListSearch']")
		 @CacheLookup
	      WebElement collectioname;
		
		/**Method to verify Collection selected in recycle bin is equal to collection in document tab
		 * 
		 */
		public boolean verifyCollectionSelectedinRecyclebin(String collectionname) {
	    	  driver.switchTo().defaultContent();
	    	   SkySiteUtils.waitTill(4000);
	    		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				  Log.message("Switched to frame");
				  SkySiteUtils.waitTill(4000);
	    	     String collectionameselected=collectioname.getText();
	    	   Log.message("Collection selected in recyclebin tab is" +collectionameselected);
	    	   SkySiteUtils.waitTill(4000);
	    	  if(collectionameselected.contains(collectionname))
	    		  return true;
	    	  else
	    		  return false;
	    	   
	    	  }
	@FindBy(css="#btnProjectListSearch")
	WebElement projectlistbtn;
	@FindBy(xpath=".//*[@id='ProjectListSearch_txtSearchValue']")
	WebElement searchbox2;
	@FindBy(xpath=".//*[@id='btnSearch']")
	WebElement searchbtn2;
		
	/**Method to verify change in collection change the content in recycle bin
	 * 
     */
	        public boolean changeCollectionInRecycleBin(String collectioname2,String Foldername2,String filepath ,String collectionclick1,String Folder1) {
	        	   driver.switchTo().defaultContent();
		 			  Log.message("Swtiched to default content");
		 			  SkySiteUtils.waitTill(4000);
		 			  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 			  Log.message("Switched to frame");
		 				String Deletedfilename=PropertyReader.getProperty("DeleteFilename");
						Log.message("Deleted file to be searched in recycle bin !! " +Deletedfilename);
		 			 int Count_list=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
						Log.message("Total list count is:"+Count_list);			 
						//Loop start '2' as projects list start from tr[2]
						int i=0;
						for(i = 1; i <= Count_list; i++) 
						{
							
							String filepresentinrecyclebin=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
							//Log.message("Contact name is:"+contactName);
							String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+i+"]")).getText();
							SkySiteUtils.waitTill(5000);			
							//Checking contact name equal with each row of table
							if(filepresentinrecyclebin.contains(Deletedfilename)&&pathfoldername.contains(Foldername2))
							{	   
								Log.message("Deleted file name : " +filepresentinrecyclebin+ " Found in the list and the folder path of the file is  " +pathfoldername);
								Log.message("Deleted File Presence Validated Successfully!!!");		                  
								break;
						    }
						}
						String filepresentafterdeletion=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
						
						String pathfoldername=driver.findElement(By.xpath("(.//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2]/span)["+i+"]")).getText();
						
						SkySiteUtils.waitTill(3000);
						if(filepresentafterdeletion.contains(Deletedfilename)&&pathfoldername.contains(Foldername2))
						{
							Log.message("File :  " +filepresentafterdeletion + "is present Under collection :  " +collectioname2 + "   in recyclebin");
						}
						else
						{
							Log.message("File is not present in recycle bin");
						}
		 			  SkySiteUtils.waitTill(4000);
		 			  projectlistbtn.click();
		 			  Log.message("Projectlistbtn is clicked");
		 			  SkySiteUtils.waitTill(5000);
		 			  searchbox2.sendKeys(collectionclick1);
		 			  Log.message("collection searched is " +collectionclick1);
		 			 searchbox2.sendKeys(Keys.ENTER);
		 			 Log.message("Enter pressed in search box ");
		 			 SkySiteUtils.waitTill(5000);
		 			  SkySiteUtils.waitTill(4000);
		 			 WebElement colect=driver.findElement(By.xpath(".//*[@id='plctrl_divProjectList']/ul/li/a"));
		         	  colect.click(); 
		         	  Log.message("Collection is clicked");
		          	  SkySiteUtils.waitTill(8000);
		 			  String collectionameselected=collectioname.getText();
			    	   Log.message("Collection now selected in recyclebin tab is   " +collectionameselected);
			    	   SkySiteUtils.waitTill(4000);
			    	  if(collectionameselected.contains(collectionclick1))
			    	  {
			    		  Log.message("Collection selection is changed and now selection collection is  "   +collectionameselected);
			    	  }
			    	  else {
			    		  
			    		  
			    		  Log.message("Collection is not selected");
			    	  }
		                  	   
			    	   int itemlist=driver.findElements(By.xpath("//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[1]/img")).size();
						Log.message("Total list count is:"+itemlist);			 
						//Loop start '2' as projects list start from tr[2]
						int j=0;
						for(j = 1; j<= Count_list; j++) 
						{
								
							String foldernamepresent=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
					
							SkySiteUtils.waitTill(5000);			
							//Checking contact name equal with each row of table
							if(foldernamepresent.contains(Folder1))
							{	   
								Log.message("folder name matched is : " +foldernamepresent);
								Log.message(" Deleted Folder Presence Validated Successfully!!!");		                  
								break;
							}		
						}
						String folder1deleted=driver.findElement(By.xpath("(//*[@id='divRecycleItem']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
						Log.message("****Folder name is**** " +folder1deleted);
						SkySiteUtils.waitTill(3000);
						if(folder1deleted.contains(Folder1))	
							return true;
						else
							return false;   
		        	   
		    }
	           @FindBy(css="#btnPlctrlResetSearch")
	           @CacheLookup
	           WebElement resetbtn2;
	           /**Method to search a collection in recycle bin tab
	            * 
	            */
	             public boolean searchAndResetCollectioninRecycletab(String collectionname) {
	            	  driver.switchTo().defaultContent();
		 			  Log.message("Swtiched to default content");
		 			  SkySiteUtils.waitTill(4000);
		 			  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 			  Log.message("Switched to frame");
		 			  SkySiteUtils.waitTill(4000);
		 			  projectlistbtn.click();
		 			  Log.message("Projectlistbtn is clicked");
		 			  SkySiteUtils.waitTill(5000);
		 			  searchbox2.sendKeys(collectionname);
		 			  Log.message("collection searched is " +collectionname);
		 			  searchbox2.sendKeys(Keys.ENTER);
		 			  Log.message("Enter pressed in search box ");
		 			  SkySiteUtils.waitTill(5000);
		 			  int Count_list=driver.findElements(By.xpath("//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label")).size();
						Log.message("Total Collection list count is:"+Count_list);			 
						//Loop start '2' as projects list start from tr[2]
						int i=0;
						for(i = 1; i <= Count_list; i++) 
						{
							
							String Collectionpresent=driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)["+i+"]")).getText();
						
						
							SkySiteUtils.waitTill(5000);			
							//Checking contact name equal with each row of table
							if(Collectionpresent.contains(collectionname))
							{	   
							
								Log.message("Collection is searched succesfully: " +Collectionpresent);		                  
								break;
						    }
						}
						String Collectionpresentonsearching=driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)["+i+"]")).getText();
	            	   if(Collectionpresentonsearching.contains(collectionname))
	            	   {
	            		   
	            		   Log.message("Search functionality for collection is working !!!!");
	            	   }
	            	   else {
	            		   
	            		   Log.message("Collection search is not working !!!");
	            	   }
	            	   SkySiteUtils.waitTill(4000);
	            	   resetbtn2.click();
	            	   Log.message("reset button is clicked");
	            	   SkySiteUtils.waitTill(9000);
	            	   if(searchbox2.getText().isEmpty()==true)
	            		   return true;
	            	   else
	            		   return false;
	            	   
	          }
	             @FindBy(css="#btnFavSearch")
	             @CacheLookup
	             WebElement favbtn;
	             /**Method to verify and view the selected favourite collection
	              * 
	              */
	             public boolean verifyFavouriteCollection(String Collectionname) {
	            	 driver.switchTo().defaultContent();
		 			  Log.message("Swtiched to default content");
		 			  SkySiteUtils.waitTill(4000);
		 			  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 			  Log.message("Switched to frame");
		 			  SkySiteUtils.waitTill(4000);
		 			  projectlistbtn.click();
		 			  Log.message("Projectlistbtn is clicked");
		 			  SkySiteUtils.waitTill(5000);
		 			 favbtn.click();
		 			 Log.message("Favourite icon clicked");
		 			 SkySiteUtils.waitTill(6000);
		 			 SkySiteUtils.waitTill(5000);
		 		
		 			  int Count_list=driver.findElements(By.xpath("//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label")).size();
						Log.message("Total Collection list count is:"+Count_list);			 
						//Loop start '2' as projects list start from tr[2]
						int i=0;
						for(i = 1; i <= Count_list; i++) 
						{
							
							String Collectionpresent=driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)["+i+"]")).getText();
						
						
							SkySiteUtils.waitTill(5000);			
							//Checking contact name equal with each row of table
							if(Collectionpresent.contains(Collectionname))
							{	   
							
								Log.message("Favourite collection found is " +Collectionpresent);		                  
								break;
						    }
						}
						String Collectionpresentonsearching=driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)["+i+"]")).getText();
	            	   
						
						if(Collectionpresentonsearching.contains(Collectionname))
	            	  
	            		   
	            		 return true;
	            	  
	            	   else 
	            		   
	            		return false;
	            	  
	            	
	        }
	
 @FindBy(xpath=".//*[@id='Collections']/a")
 WebElement btnTopCollection;
 
 /**Method to verify and view the selected favourite collection
  * @return
  * Author: Ranadeep
  */
 public FnaHomePage navigateToCollectionList() 
 {
	 
	 SkySiteUtils.waitTill(5000);
	 
	 Log.message("Swtiching to default content");
 	 driver.switchTo().defaultContent();	  
	 SkySiteUtils.waitTill(4000);
	 
	 btnTopCollection.click();
	 Log.message("Clicked on Collection button at the top of the page");
	 SkySiteUtils.waitTill(5000);
		  
	 return new FnaHomePage(driver).get();
	 
 }
             
}

		
	           

