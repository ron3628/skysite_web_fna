package com.arc.fna.pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;


public class UtilitiesPage extends LoadableComponent<UtilitiesPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public UtilitiesPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	 @FindBy(xpath = "//a[@class='download-app' and @href='#']")
	 WebElement btnutilities;
	 
	 @FindBy(xpath = "//a[@id='Footer1_acrOutLook']")
	 WebElement btnoutlookaddin;
	    
	/** 
	 * Method written for Download the Outlook add-in in Utilities
	 * Scripted By: Sekhar     
	 * @throws AWTException 
	 */
	 
	 public boolean Download_Outlook_Addin_Utilities(String DownloadPath) throws AWTException 
	 {   
		 boolean result1=false;
		 
		 SkySiteUtils.waitTill(3000); 
		 btnutilities.click();
		 Log.message("Utilities button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().defaultContent();
		 fnaHomePage = new FnaHomePage(driver).get();
		// Calling delete files from download folder script
		 fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		 SkySiteUtils.waitTill(5000); 
		 btnoutlookaddin.click();
		 Log.message("Outlook add-in button has been clicked");
		 SkySiteUtils.waitTill(3000);		 
		 
		// Get Browser name on run-time.
		 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		 String browserName = caps.getBrowserName();
		 Log.message("Browser name on run-time is: " + browserName);
		 
		 if (browserName.contains("firefox"))
		 {
			 // Handling Download PopUp of firefox browser using robot
			 Robot robot = null;
			 robot = new Robot();   			
			 robot.keyPress(KeyEvent.VK_LEFT);
			 robot.keyRelease(KeyEvent.VK_LEFT);		
			 SkySiteUtils.waitTill(3000);		
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			 SkySiteUtils.waitTill(10000);
	   	}	
		SkySiteUtils.waitTill(25000);		
		// After checking whether download folder or not
		String ActualFoldername = null;		
		File[] files = new File(DownloadPath).listFiles();
		 
		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					result1=true;							
					break;
	   			}	 
				else
				{
					result1=false;	
					Log.message("outlook add-in Download Unsuccessfully");			
	   			}	
			}
	    }
		if(result1==true)
			return true;
	   	else
	   		return false;			
	 }	
	 
	 @FindBy(xpath = "//a[@id='Footer1_acrTurboAcceleraor']")
	 WebElement btnAcceleratorTubro;
	    
	/** 
	 * Method written for Accelerator Download in Utilities
	 * Scripted By: Sekhar     
	 * @throws AWTException 
	 */
	 
	 public boolean Accelerator_Download_Utilities(String DownloadPath) throws AWTException 
	 {   
		 boolean result1=false;
		 
		 SkySiteUtils.waitTill(3000); 
		 btnutilities.click();
		 Log.message("Utilities button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().defaultContent();
		 fnaHomePage = new FnaHomePage(driver).get();
		// Calling delete files from download folder script
		 fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		 SkySiteUtils.waitTill(5000); 
		 btnAcceleratorTubro.click();
		 Log.message("Accelerator Tubro has been clicked");
		 SkySiteUtils.waitTill(3000);		 
		 
		// Get Browser name on run-time.
		 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		 String browserName = caps.getBrowserName();
		 Log.message("Browser name on run-time is: " + browserName);
		 
		 if (browserName.contains("firefox"))
		 {
			 // Handling Download PopUp of firefox browser using robot
			 Robot robot = null;
			 robot = new Robot();   			
			 robot.keyPress(KeyEvent.VK_LEFT);
			 robot.keyRelease(KeyEvent.VK_LEFT);		
			 SkySiteUtils.waitTill(3000);		
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			 SkySiteUtils.waitTill(10000);
	   	}	
		SkySiteUtils.waitTill(25000);		
		// After checking whether download folder or not
		String ActualFoldername = null;		
		File[] files = new File(DownloadPath).listFiles();
		 
		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();
				Log.message("Actual name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if(ActualFolderSize != 0) 
				{
					result1=true;
					Log.message("Accelerator Tubro Download Successfully");		
					break;
	   			}	 
				else
				{
					result1=false;	
					Log.message("Accelerator Tubro Download Unsuccessfully");			
	   			}	
			}
	    }
		if(result1==true)
			return true;
	   	else
	   		return false;			
	 }
	 
	 //@FindBy(xpath = "//a[@class='facilities-syncapp-download']")	 
	 @FindBy(xpath = "//*[@id='projects-app-download']/div/div/div[2]/div[1]/div[1]/div/div/button")
	 WebElement btnsyncapp;
	 
	 @FindBy(xpath = "//a[@id='Footer1_anchrPWCSynchApp64Bit']")
	 WebElement btnSynchApp64Bit;
	    
	/** 
	 * Method written for Sync app 64 bit Download in Utilities
	 * Scripted By: Sekhar     
	 * @throws AWTException 
	 */
	 
	 public boolean Syncapp64bit_Download_Utilities(String DownloadPath) throws AWTException 
	 {   
		 boolean result1=false;
		 
		 SkySiteUtils.waitTill(3000); 
		 btnutilities.click();
		 Log.message("Utilities button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().defaultContent();
		 fnaHomePage = new FnaHomePage(driver).get();
		 // Calling delete files from download folder script
		 fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		 SkySiteUtils.waitTill(5000); 
		 btnsyncapp.click();
		 Log.message("sync app button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 btnSynchApp64Bit.click();
		 Log.message("sync app 64 bit button has been clicked");
		 SkySiteUtils.waitTill(3000);		 
		 
		// Get Browser name on run-time.
		 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		 String browserName = caps.getBrowserName();
		 Log.message("Browser name on run-time is: " + browserName);
		 
		 if (browserName.contains("firefox"))
		 {
			 // Handling Download PopUp of firefox browser using robot
			 Robot robot = null;
			 robot = new Robot();   			
			 robot.keyPress(KeyEvent.VK_LEFT);
			 robot.keyRelease(KeyEvent.VK_LEFT);		
			 SkySiteUtils.waitTill(3000);		
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			 SkySiteUtils.waitTill(10000);
	   	}	
		SkySiteUtils.waitTill(35000);		
		// After checking whether download folder or not
		String ActualFoldername = null;		
		File[] files = new File(DownloadPath).listFiles();
		 
		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();
				Log.message("Actual name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if(ActualFolderSize != 0) 
				{
					result1=true;
					Log.message("Sync app 64 bit Download Successfully");		
					break;
	   			}	 
				else
				{
					result1=false;	
					Log.message("Sync app 64 bit Download Unsuccessfully");			
	   			}	
			}
	    }
		if(result1==true)
			return true;
	   	else
	   		return false;			
	 }
	 
	 @FindBy(xpath = "//a[@id='Footer1_anchrPWCSynchApp32Bit']")
	 WebElement btnSynchApp32Bit;
	    
	/** 
	 * Method written for Sync app 32 bit Download in Utilities
	 * Scripted By: Sekhar     
	 * @throws AWTException 
	 */
	 
	 public boolean Syncapp32bit_Download_Utilities(String DownloadPath) throws AWTException 
	 {   
		 boolean result1=false;
		 
		 SkySiteUtils.waitTill(3000); 
		 btnutilities.click();
		 Log.message("Utilities button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().defaultContent();
		 fnaHomePage = new FnaHomePage(driver).get();
		 // Calling delete files from download folder script
		 fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		 SkySiteUtils.waitTill(5000); 
		 btnsyncapp.click();
		 Log.message("sync app button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 btnSynchApp32Bit.click();
		 Log.message("sync app 32 bit button has been clicked");
		 SkySiteUtils.waitTill(3000);		 
		 
		 // Get Browser name on run-time.
		 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		 String browserName = caps.getBrowserName();
		 Log.message("Browser name on run-time is: " + browserName);
		 
		 if (browserName.contains("firefox"))
		 {
			 // Handling Download PopUp of firefox browser using robot
			 Robot robot = null;
			 robot = new Robot();   			
			 robot.keyPress(KeyEvent.VK_LEFT);
			 robot.keyRelease(KeyEvent.VK_LEFT);		
			 SkySiteUtils.waitTill(3000);		
			 robot.keyPress(KeyEvent.VK_ENTER);
			 robot.keyRelease(KeyEvent.VK_ENTER);
			 SkySiteUtils.waitTill(10000);
	   	}	
		SkySiteUtils.waitTill(35000);		
		// After checking whether download folder or not
		String ActualFoldername = null;		
		File[] files = new File(DownloadPath).listFiles();
		 
		for (File file : files)
		{
			if(file.isFile()) 
			{
				ActualFoldername = file.getName();
				Log.message("Actual name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					result1=true;
					Log.message("Sync app 32 bit Download Successfully");		
					break;
	   			}	 
				else
				{
					result1=false;	
					Log.message("Sync app 32 bit Download Unsuccessfully");			
	   			}	
			}
	    }
		if(result1==true)
			return true;
	   	else
	   		return false;			
	 }
	
}
