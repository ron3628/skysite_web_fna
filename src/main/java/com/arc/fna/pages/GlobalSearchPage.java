package com.arc.fna.pages;


import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;


public class GlobalSearchPage extends LoadableComponent<GlobalSearchPage>
{

	WebDriver driver;
	private boolean isPageLoaded;	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public GlobalSearchPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	/**
	 * Method written for Random Collection name
	 *  Scripted By: Sekhar
	 * @return
	 */

	public String Random_Collectionname()
	{
		String str = "Collect";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Edit Collection name
	 *  Scripted By: Sekhar
	 * @return
	 */

	public String Random_EditCollectionname()
	{
		String str = "EditCollect";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Test name
	 *  Scripted By: Sekhar
	 * @return
	 */

	public String Random_TestName()
	{
		String str = "Test";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
		
	
	 @FindBy(xpath = "//*[@id='liGlobalSearch']/a/i")
	 WebElement btnGlobalSearch;
	    
	 @FindBy(xpath = "//*[@id='idSearchcontroleDropDown']/li[1]/div/div[2]/div[1]/div")
	 WebElement btnSearchcontroleDropDown;
	    
	 @FindBy(css = "#searchiconbutton")
	 WebElement btnsearchiconbutton;
	    
	 @FindBy(xpath = "//*[@id='div_SearchContent']/li/div/div[1]/h4/a")
	 WebElement btngetfilename;
	 
	/** 
     * Method written for search with project name
     * Scripted By: Sekhar      
     */
	
    public boolean Collection_GlobalSearch(String CollectionName)
    {  
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnGlobalSearch.click();   
    	Log.message("Global Search button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnSearchcontroleDropDown.click();
    	Log.message("Down Search icon button has been clicked");
       	SkySiteUtils.waitTill(3000);  
       	
       	if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}       
    	SkySiteUtils.waitTill(5000);
		driver.findElement(By.id("SearchsettingSelectall")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.id("SearchsettingSelectall")).click();        
		if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		} 
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).click();
		Log.message("Global text box has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(CollectionName);
		Log.message("File name has been entered");
		SkySiteUtils.waitTill(30000);	
		btnsearchiconbutton.click();
		Log.message("search icon button has been clicked");
		SkySiteUtils.waitTill(5000);
		String collName=driver.findElement(By.xpath(".//*[@id='div_SearchContent']/li/div/div[1]/h4")).getText().toString();
		Log.message("collection name is:"+collName);
		if(collName.contains(CollectionName))
			return true;
		else
			return false;    	
    }  
    
    @FindBy(xpath = "//*[@id='Collections']/a")
   	WebElement clkcollection;
       
       @FindBy(xpath = "//*[@id='btnNewProject']")

   	WebElement btncollection;

   	@FindBy(xpath = "//*[@id='txtProjectName']")

   	WebElement txtProjectName;

   	@FindBy(xpath = "//*[@id='txtProjectNumber']")

   	WebElement txtProjectNumber;

   	@FindBy(css = "#txtProjectStartDate")

   	WebElement txtProjectStartDate;

   	@FindBy(css = ".next")

   	WebElement nxtdatemonth;

   	@FindBy(xpath = "(//td[@class='day'])[8]")

   	WebElement sltdate;

   	@FindBy(css = "#txtProjectDesc")

   	WebElement txtProjectDesc;

   	@FindBy(css = "#txtProjectAddress1")

   	WebElement txtProjectAddress1;

   	@FindBy(css = "#txtProjectCity")

   	WebElement txtProjectCity;

   	@FindBy(css = "#txtProjectZip")

   	WebElement txtProjectZip;

   	@FindBy(css = "#chkFav")

   	WebElement chkFav;

   	@FindBy(css = "#btnSave")

   	WebElement btnSave;
   	
   	@FindBy(css = ".selectedTreeRow")

   	WebElement selectedTreeRow;

   	@FindBy(xpath = "//*[@id='btnProjectListSearch']")

   	WebElement spaHeader;

   	@FindBy(css = "#btnFavSearch")

   	WebElement btnFavSearch;

   	@FindBy(xpath = "(//i[@class='icon icon-favourite icon-green ico-lg'])[1]")

   	WebElement icongreen;
       
       /**
   	 * Method written for create collections
   	 * Scripted By: Sekhar
   	 * 
   	 * @return
   	 */
   	public boolean Create_Collections_Search(String CollectionName,String Descrip,String Address,String City,String Zip,String Country,String State) throws IOException 
   	{
   		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver, clkcollection, 30);
   		SkySiteUtils.waitTill(5000);
   		clkcollection.click();// click on collection Selection
   		Log.message("collection has been clicked.");
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
   		SkySiteUtils.waitTill(2000);
   		btncollection.click();
   		Log.message("Add collection has been clicked.");
   		SkySiteUtils.waitTill(5000);
   		String parentHandle = driver.getWindowHandle();
   		Log.message(parentHandle);
   		for (String winHandle : driver.getWindowHandles())
   		{
   			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
   													// your newly opened window)
   		}
   		SkySiteUtils.waitTill(2000);
   		// String CollectionName=FnaHomePage.this.Random_Collectionname();
   		Log.message(CollectionName);
   		txtProjectName.sendKeys(CollectionName);
   		SkySiteUtils.waitTill(2000);
   		txtProjectNumber.sendKeys(CollectionName);
   		Log.message("Collection number is:" + CollectionName);
   		SkySiteUtils.waitTill(2000);
   		txtProjectStartDate.click();
   		Log.message("Start date has been clicked");
   		SkySiteUtils.waitTill(3000);
   		nxtdatemonth.click();
   		Log.message("next month button has been clicked");
   		SkySiteUtils.waitTill(3000);
   		sltdate.click();
   		Log.message("date button has been clicked");
   		SkySiteUtils.waitTill(2000);
   		
   		//String Descrip = PropertyReader.getProperty("Description");
   		txtProjectDesc.sendKeys(Descrip);
   		SkySiteUtils.waitTill(2000);
   		txtProjectAddress1.sendKeys(Address);
   		Log.message("Address has been entered");
   		SkySiteUtils.waitTill(2000);
   		txtProjectCity.sendKeys(City);
   		Log.message("city has been entered");
   		SkySiteUtils.waitTill(2000);
   		txtProjectZip.sendKeys(Zip);
   		Log.message("Zip has been entered");
   		SkySiteUtils.waitTill(5000);
   		Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
   		drpcountry.selectByVisibleText(Country);
   		Log.message("Country has been selected");
   		SkySiteUtils.waitTill(3000);
   		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
   		drpstate.selectByVisibleText(State);
   		Log.message("state has been selected");
   		SkySiteUtils.waitTill(3000);
   		btnSave.click();
   		Log.message("save button has been clicked");
   		SkySiteUtils.waitTill(2000);
   		driver.switchTo().window(parentHandle);// Switch back to folder page
   		SkySiteUtils.waitTill(2000);
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
   		SkySiteUtils.waitTill(2000);
   		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
   		//SkySiteUtils.waitTill(5000);
   		String Collection = selectedTreeRow.getText();
   		Log.message("Collection name is:" + Collection);
   		SkySiteUtils.waitTill(2000);
   		if (Collection.contains(CollectionName)) 
   		{
   			Log.message("Create Collection successfull" + CollectionName);
   			SkySiteUtils.waitTill(2000);
   			return true;
   		}
   		else
   		{
   			Log.message("Create Collection Unsuccessfull" + CollectionName);
   			SkySiteUtils.waitTill(2000);
   			return false;
   		}
   	}

   	/** 
     * Method written for global search with project name after collection info
     * Scripted By: Sekhar      
     */
	
    public boolean GlobalSearch_Collection_Info(String CollectionName,String Address,String City,String Zip,String Country,String State)
    {  
    	SkySiteUtils.waitTill(3000);
    	driver.findElement(By.xpath("//i[@class='icon icon-info ico-lg']")).click();
    	Log.message("collection info button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); 													
		}
		SkySiteUtils.waitTill(3000);
    	String collectionName=driver.findElement(By.xpath(".//*[@id='lblProjectName']")).getText();
    	Log.message("collection name is :"+collectionName);
    	SkySiteUtils.waitTill(3000);
    	String collectionNumber=driver.findElement(By.xpath(".//*[@id='lblProjectNumber']")).getText();
    	Log.message("collection number is :"+collectionNumber);
    	SkySiteUtils.waitTill(3000);
    	String AddressName=driver.findElement(By.xpath(".//*[@id='lblProjectAddress']")).getText();
    	Log.message("collection Address is :"+AddressName);
    	SkySiteUtils.waitTill(3000);
    	String CityName=driver.findElement(By.xpath(".//*[@id='lblCity']")).getText();
    	Log.message("collection City is :"+CityName);
    	SkySiteUtils.waitTill(3000);
    	String ZipCode=driver.findElement(By.xpath(".//*[@id='lblZip']")).getText().toString();
    	Log.message("collection Zipcode is :"+ZipCode);
    	SkySiteUtils.waitTill(3000);    	
    	String CountryName=driver.findElement(By.xpath(".//*[@id='lblCountry']")).getText();
    	Log.message("collection Country Name is :"+CountryName);
    	SkySiteUtils.waitTill(3000);
    	String StateName=driver.findElement(By.xpath(".//*[@id='lblState']")).getText();
    	Log.message("collection State Name is :"+StateName);
    	SkySiteUtils.waitTill(3000);    
    	if((collectionName.contains(CollectionName))&&(collectionNumber.contains(CollectionName))
    			&&(Address.contains(AddressName))&&(CityName.contains(City))
    			&&(ZipCode.contains(Zip))&&(CountryName.contains(Country))&&(StateName.contains(State)))
    		return true;
    	else
    		return false;    	
    }   
    
    /** 
     * Method written for search with project name
     * Scripted By: Sekhar      
     */
	
    public boolean File_GlobalSearch_Fourbuttons(String CollectionName)
    {  
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
    	String File_Name=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).getText();
    	Log.message("File Name is:"+File_Name);
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnGlobalSearch.click();   
    	Log.message("Global Search button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnSearchcontroleDropDown.click();
    	Log.message("Down Search icon button has been clicked");
       	SkySiteUtils.waitTill(3000);  
       	
       	if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}       
    	SkySiteUtils.waitTill(5000);
		driver.findElement(By.id("SearchsettingSelectall")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.id("SearchsettingSelectall")).click();        
		if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		} 
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).click();
		Log.message("Global text box has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(File_Name);
		Log.message("File name has been entered");
		SkySiteUtils.waitTill(40000);	
		btnsearchiconbutton.click();
		Log.message("searcn icon button has been clicked");
		SkySiteUtils.waitTill(10000);		
		int FileCount = driver.findElements(By.xpath(".//*[@id='div_SearchContent']/li")).size();
		Log.message("File Count is:" + FileCount);	
		int i = 0;
		for (i = 1; i<= FileCount; i++) 
		{		
			String filName = driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[1]/h4/a)["+i+"]")).getText();
			Log.message("Exp file name is :"+filName);
			SkySiteUtils.waitTill(2000);
			String collName = driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[1]/h5)["+i+"]")).getText();
			Log.message("Exp Collection name is :"+collName);
			SkySiteUtils.waitTill(2000);			
			if((filName.contains(File_Name))&&(collName.contains(CollectionName)))
			{
				Log.message("Expected File name and collection name Successfully");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);			
		if((driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[2]/ul/li[1]/a)["+i+"]")).isDisplayed())
				&&(driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[2]/ul/li[2]/a)["+i+"]")).isDisplayed())
				&&(driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[2]/ul/li[3]/a)["+i+"]")).isDisplayed())
				&&(driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[2]/ul/li[4]/a)["+i+"]")).isDisplayed()))
			return true;
		else
			return false;  
	}

    
    /** 
     * Method written for Edit collection
     * Scripted By: Sekhar      
     */
	
    public boolean Edit_Collection(String CollectionName,String Edit_CollectionName)
    {  
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	driver.findElement(By.xpath(".//*[@id='Collections']/a")).click();
    	Log.message("clicked on collections on header");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
    	SkySiteUtils.waitTill(5000);
    	int prjCount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("prjCount :" + prjCount);
	
		int i = 0;
		for (i = 1; i <= prjCount; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(2000);
			// Checking project name equal with each row of table
			if (prjName.equals(CollectionName)) 
			{
				Log.message("Select Collection button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)[" + i + "]")).click();
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); 													
		}
		SkySiteUtils.waitTill(3000);
		Log.message(CollectionName);
		txtProjectName.clear();
		SkySiteUtils.waitTill(2000);
   		txtProjectName.sendKeys(Edit_CollectionName);
   		Log.message("Collection number is:" + Edit_CollectionName);
   		SkySiteUtils.waitTill(2000);
   		txtProjectNumber.clear();
   		SkySiteUtils.waitTill(2000);
   		txtProjectNumber.sendKeys(Edit_CollectionName);
   		Log.message("Collection number is:" + Edit_CollectionName);
   		SkySiteUtils.waitTill(2000);
   		btnSave.click();
   		Log.message("save button has been clicked");
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().window(parentHandle);// Switch back to folder page
   		SkySiteUtils.waitTill(2000);
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
   		SkySiteUtils.waitTill(2000);
   		int prjCount1 = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("prjCount :" + prjCount1);
	
		int j = 0;
		for (j = 1; j <= prjCount1; j++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String EditprjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + j + "]")).getText();
			Log.message(EditprjName);
			SkySiteUtils.waitTill(2000);
			// Checking project name equal with each row of table
			if (EditprjName.contains(Edit_CollectionName)) 
			{
				Log.message("Select Collection button has been clicked!!!");
				result1=true;
				break;
			}
		}
		SkySiteUtils.waitTill(5000);   		
   		if (result1==true) 
   		{
   			Log.message("Edit Collection successfull" + Edit_CollectionName);
   			SkySiteUtils.waitTill(2000);
   			return true;
   		}
   		else
   		{
   			Log.message("Edit Collection Unsuccessfull" + Edit_CollectionName);
   			SkySiteUtils.waitTill(2000);
   			return false;
   		}
    }
    
    @FindBy(xpath = ".//*[@id='divCustomFieldSearchOption']/div/ul/li[1]/div/div[1]/label")
   	WebElement btnwithdoclabel;
       
    @FindBy(xpath = ".//*[@id='divCustomFieldSearchOption']/div/ul/li[2]/div/div[1]/label")
    WebElement btnwithinvoicelabel;
       
    /** 
     * Method written for NewRegistration to CustomAttributes GlobalSearch
     * Scripted By: Sekhar      
     */
   	
    public boolean NewRegistration_CustomAttributes_GlobalSearch(String FristName,String State,String Phonenum,String password)
    {      	
    	SkySiteUtils.waitTill(20000);
       	driver.findElement(By.xpath("//a[@class='flipform']")).click();//click on Register
       	Log.message("register button has been clicked");
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("//input[@id='txtFirstName']")).sendKeys(FristName);
   		Log.message("Frist Name has been entered");
   		driver.findElement(By.xpath("//input[@id='txtLastName']")).sendKeys(FristName);
   		Log.message("Last Name has been entered");
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("//input[@id='txtEmail']")).sendKeys(FristName+"@yopmail.com");
   		Log.message("Email has been entered");
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath(".//*[@id='txtASState']")).sendKeys(State);
   		Log.message("Drop down button has been clicked");
   		SkySiteUtils.waitTill(3000);		
   		driver.findElement(By.xpath(".//*[@id='txtASPhone']")).sendKeys(Phonenum);
   		Log.message("Phone number has been entered");
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("//input[@id='txtPassword']")).sendKeys(password);
   		Log.message("password has been entered");
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("//button[@id='aSubmit']")).click();
   		Log.message("Create button has been clicked");
   		SkySiteUtils.waitTill(20000);
   		driver.findElement(By.xpath(".//*[@id='closeWelcomeVideo']")).click();
   		Log.message("close video button has been clicked");
   		SkySiteUtils.waitTill(5000);
   		driver.findElement(By.xpath(".//*[@id='mainPageHeader']/nav/div[2]/ul/li[1]/div/a/i")).click();
   		Log.message("Home header button has been clicked");
   		SkySiteUtils.waitTill(5000);		
   		driver.findElement(By.xpath(".//*[@id='mainPageHeader']/nav/div[2]/ul/li[1]/div/ul/li/a")).click();
   		Log.message("Home icon button has been clicked");
   		SkySiteUtils.waitTill(5000);
   		driver.findElement(By.xpath(".//*[@id='frmCommonLandingInfoLinksActivate']/a")).click();
   		Log.message("Activate button has been clicked");
   		SkySiteUtils.waitTill(5000);		
   		driver.findElement(By.xpath(".//*[@id='divWelcomeVid']/div/div/div[1]/button")).click();
   		Log.message("close video button has been clicked");
   		SkySiteUtils.waitTill(5000);
   		btnGlobalSearch.click();   
       	Log.message("Search button has been clicked");
       	SkySiteUtils.waitTill(2000);
       	if((btnwithdoclabel.isDisplayed())&&(btnwithinvoicelabel.isDisplayed()))
       	{
       		Log.message("Custom attributes showing");
   			SkySiteUtils.waitTill(2000);
       		return true;
       	}
       	else
       	{
       		Log.message("Custom attributes Not showing");
   			SkySiteUtils.waitTill(2000);
       		return false;
       	}
    }
       
       
}