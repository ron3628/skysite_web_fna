package com.arc.fna.pages;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FnAMyProfilePage extends LoadableComponent<FnAMyProfilePage> {
WebDriver driver;
private  boolean isPageLoaded;

/** Find Elements using FindBy annotation
 * 
 */
@FindBy(css="#btn_saveDown")
@CacheLookup
WebElement Savebtn;

	

@FindBy(css="#user-info")
@CacheLookup
WebElement MyProfbtn;

@FindBy(css="#txt_firstName")
@CacheLookup
WebElement FirstName;


@FindBy(css="#txt_lastName")
@CacheLookup
WebElement LastName;

@FindBy(css="#txt_Title")
@CacheLookup
WebElement Title;


@FindBy(css="#txt_CompName")
@CacheLookup
WebElement CompanyName;

@FindBy(css="#txt_add1")
@CacheLookup
WebElement Address1field;


@FindBy(css="#txt_add2")
@CacheLookup
WebElement Address2field;

@FindBy(css="#ddl_country")
@CacheLookup
WebElement Country;

@FindBy(css="#ddl_state")
@CacheLookup
WebElement State;

@FindBy(css="#txt_city")
@CacheLookup
WebElement Cityfield;

@FindBy(css="#txt_zip")
@CacheLookup
WebElement Zipfield;


@FindBy(css="#DDLCompBussiness")
@CacheLookup
WebElement Businessdropdown;

@FindBy(css="#DDLCompOccupation")
@CacheLookup
WebElement Occupationdropdown;

@FindBy(css="#ddlProjectRole")
@CacheLookup
WebElement ProjectRoledropdown;

//@FindBy(xpath="//div[text()='User profile successfully updated.']")
//@CacheLookup
//WebElement Savesuccessmsg;

@FindBy(className="noty_text")
@CacheLookup
WebElement Savesuccessmsg;


@FindBy(css="#user-info")
@CacheLookup
WebElement MyProfilebtn;

@FindBy(css="#limyprofile > a:nth-child(1)")
@CacheLookup
WebElement MyProfilebtndrpdwn;


@FindBy(css="#changePassword > u:nth-child(1)")
@CacheLookup
WebElement ChangePasswordbtn;

@FindBy(css="#txtOldPassword")
@CacheLookup
WebElement OldPasswordfield;	
		
@FindBy(css="#txtNewPassword")
@CacheLookup
WebElement NewPasswordfield;	

@FindBy(css="#txtConfirmPassword")
@CacheLookup
WebElement ConfirmPasswordfield;

@FindBy(css="#passwordupdateid")
@CacheLookup
WebElement Updatebtn;


@FindBy(css="#txt_Email")
@CacheLookup
WebElement Emailfield;


@FindBy(xpath=".//*[@id='nvHomeAndSSProjLink']/li/a/i")
@CacheLookup
WebElement Homebutton;

@FindBy(xpath=".//*[@id='liSSProjLink']/a")
@CacheLookup
WebElement Prjnav;

@FindBy(css="img.img-circle")
@CacheLookup
WebElement PrjMyProfile;


@FindBy(css=".sldmenu-Myprofile > a:nth-child(1)")
@CacheLookup
WebElement PrjMyProfilenav;


@FindBy(css="#txt_phone")
@CacheLookup
WebElement Phonenumberfield;


@FindBy(css="#txt_loginID")
@CacheLookup
WebElement Userid;

@FindBy(css="#txt_CompOccupation")
@CacheLookup
WebElement otherfield;


@FindBy(css="#Collections")
@CacheLookup
WebElement Collectionbtn;




    @Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;
		SkySiteUtils.waitForElement(driver,MyProfbtn,10);
		Log.message("Page is loaded");
	}

    
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FnAMyProfilePage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	
	   /** Method for My Profile Page landing verification
     *  Scripted By :Trinanjwan
     */
	
	public boolean verifyLandingOfMyProfilepage() 
	{
		SkySiteUtils.waitTill(6000);
	    driver.switchTo().defaultContent();
		Log.message("Switched into default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched into Frame");
		if(Savebtn.isDisplayed()==true) {
			Log.message("Save Button is displayed");
			return true;
		}
			return false;
	}
	
	
	
    /** Method for creating random first name
     *  Scripted By :Trinanjwan
     */


	public String generateRandomfirstname()
	
	{
	 String str="FirstName";
	 Random r = new Random( System.currentTimeMillis() );
	 return str+String.valueOf((10000 + r.nextInt(20000)));
	}

	
	
    /** Method for creating random last name
     *  Scripted By :Trinanjwan
     */


	public String generateRandomlastname()
	
	{
	 String str="LastName";
	 Random r = new Random( System.currentTimeMillis() );
	 return str+String.valueOf((10000 + r.nextInt(20000)));
	}
	
	
    /** Method for creating random title
     *  Scripted By :Trinanjwan
     */


	public String generateRandomtitle()
	
	{
	 String str="Title";
	 Random r = new Random( System.currentTimeMillis() );
	 return str+String.valueOf((10000 + r.nextInt(20000)));
	}
	
	
	
    /** Method for creating random company name
     *  Scripted By :Trinanjwan
     */


	public String generateRandomcompanyname()
	
	{
	 String str="CompanyNameARC";
	 Random r = new Random( System.currentTimeMillis() );
	 return str+String.valueOf((10000 + r.nextInt(20000)));
	}
	
	
	 /** Method for creating random company name
     *  Scripted By :Trinanjwan
     */


	public String generateRandompassword()
	
	{
	 String str="PassNew";
	 Random r = new Random( System.currentTimeMillis() );
	 return str+String.valueOf((10000 + r.nextInt(20000)));
	}
	
	
	
	/** Method to create Random Email 
	 * By Trinanjwan
	 */



	public String Random_Email()
    {
           
           String str="Emailtest12";
           String str2="@yopmail.com";
           Random r = new Random( System.currentTimeMillis() );
           String abc = str+String.valueOf((10000 + r.nextInt(20000)))+str2;
           return abc;
           
    }
	
	
	/** Method to create Random User ID
	 * By Trinanjwan
	 */



	public String Random_Userid()
    {
           
           String str="UserID";
           Random r = new Random( System.currentTimeMillis() );
           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
           return abc;
           
    }
	
	
	
	
	  /** Method for saving my profile data
     *  Scripted By :Trinanjwan
     */

	
	public boolean savingOfMyProfileData(String Randomfirstname, String Randomlastname, String Randomtitle, String Randomcompany)
	
	{
		 SkySiteUtils.waitTill(2000);
		 driver.switchTo().defaultContent();
		 Log.message("Switch to default content");
         SkySiteUtils.waitTill(5000);
         driver.switchTo().frame("myFrame");
         Log.message("Switch to My Frame");
         SkySiteUtils.waitTill(5000);
        
         FirstName.clear();
         Log.message("First Name field is clear now");
         FirstName.sendKeys(Randomfirstname);
         Log.message("First Name entered is: "+Randomfirstname);
         SkySiteUtils.waitTill(2000);
         
         LastName.clear();
         Log.message("Last Name field is clear now");
         LastName.sendKeys(Randomlastname);
         Log.message("Last Name entered is: "+Randomlastname);
         SkySiteUtils.waitTill(2000);
         
         Title.clear();
         Log.message("Title field is clear now");
         Title.sendKeys(Randomtitle);
         Log.message("Title entered is: "+Randomtitle);
         SkySiteUtils.waitTill(2000);
         
         CompanyName.clear();
         Log.message("Company is clear now");
         CompanyName.sendKeys(Randomcompany);
         Log.message("Company entered is: "+Randomcompany);
         SkySiteUtils.waitTill(2000);
		
         String address1=PropertyReader.getProperty("Address1new");
         Address1field.clear();
         Log.message("Address1 field is clear now");
         Address1field.sendKeys(address1);
         Log.message("Address1 entered is: "+address1);
         SkySiteUtils.waitTill(2000);
         
         String address2=PropertyReader.getProperty("Address2new");
         Address2field.clear();
         Log.message("Address2 field is clear now");
         Address2field.sendKeys(address2);
         Log.message("Address2 entered is: "+address2);
         SkySiteUtils.waitTill(2000);
         
         String country=PropertyReader.getProperty("CountryMyProf");
         Select selectcountry=new Select(Country);
         selectcountry.selectByVisibleText(country);
         Log.message(country+ "is selected");
         SkySiteUtils.waitTill(5000);
         
         String state=PropertyReader.getProperty("StateMyProf");
         Select selectstate=new Select(State);
         selectstate.selectByVisibleText(state);
         Log.message(state+ "is selected");
         SkySiteUtils.waitTill(5000);
         
         String city=PropertyReader.getProperty("CityMyProf");
         Cityfield.clear();
         Log.message("City field is clear now");
         Cityfield.sendKeys(city);
         Log.message("City entered is: "+city);
         SkySiteUtils.waitTill(2000);


         String zip=PropertyReader.getProperty("ZIPMyProf");
         Zipfield.clear();
         Log.message("Zip field is clear now");
         Zipfield.sendKeys(zip);
         Log.message("ZIP entered is: "+zip);
         SkySiteUtils.waitTill(2000);
         
         String Businesnew=PropertyReader.getProperty("Business1");
         Select selectbusiness=new Select(Businessdropdown);
         selectbusiness.selectByVisibleText(Businesnew);
         Log.message(Businesnew+ "is selected");
         SkySiteUtils.waitTill(5000);
         
         
         String occupationnew=PropertyReader.getProperty("Occupation1");
         Select selectoccupation=new Select(Occupationdropdown);
         selectoccupation.selectByVisibleText(occupationnew);
         Log.message(occupationnew+ "is selected");
         SkySiteUtils.waitTill(5000);
         
         JavascriptExecutor js = (JavascriptExecutor) driver;
	     js.executeScript("window.scrollBy(0,1000)");
         
         String projectrolenew=PropertyReader.getProperty("Projectrole1");
         Select selectprojectrole=new Select(ProjectRoledropdown);
         selectprojectrole.selectByVisibleText(projectrolenew);
         Log.message(projectrolenew+ "is selected");
         SkySiteUtils.waitTill(5000);
         
         Savebtn.click();
         Log.message("Clicked on Save button");
         driver.switchTo().defaultContent();
		 Log.message("Switch to default content");
         
         SkySiteUtils.waitForElement(driver, Savesuccessmsg, 20);
 		 String expectedsavesuccessMsg = Savesuccessmsg.getText();
 		 String actualsavesuccessMsg = "User profile successfully updated.";
 		 if(expectedsavesuccessMsg.equalsIgnoreCase(actualsavesuccessMsg))
 		 {
 			Log.message("Notification alert is displayed");
 			return true;
 		 }
 			else
 			{
 			Log.message("Notification alert is not displayed");	
 			return false;
 			}
 	}
         

	  /** Method for Refreshing the My Profile page
   *  Scripted By :Trinanjwan
   */
         
		public FnAMyProfilePage refreshingMyProfile()
		
		{
         
			SkySiteUtils.waitTill(5000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
			Log.message("Waiting for My Profile button to display");
			SkySiteUtils.waitForElement(driver, MyProfilebtn, 10);
			Log.message("My Profile button is displayed");
			SkySiteUtils.waitTill(8000);
			MyProfilebtn.click();
			SkySiteUtils.waitTill(5000);
			Log.message("Clicked on My Profile button");
			SkySiteUtils.waitTill(2000);
			
			Log.message("Waiting for My Profile button in drop down to display");
			SkySiteUtils.waitForElement(driver, MyProfilebtndrpdwn, 10);
			Log.message("My Profile button is displayed");
			MyProfilebtndrpdwn.click();
			Log.message("Clicked on My Profile button from list");
			return new FnAMyProfilePage(driver).get();
	
	
		}

		@FindBy(css="#txt_firstName")
		@CacheLookup
		WebElement FirstName1;

		  /** Method for validating the my profile data
	     *  Scripted By :Trinanjwan
		 * 
	     */

		
		public boolean validatingMyProfileData(String Randomfname, String Randomlname, String Randomtitle, String RandomCompany)
		{
			
			 SkySiteUtils.waitTill(5000);
			 driver.switchTo().defaultContent();
			 Log.message("Switch to default content");
	         SkySiteUtils.waitTill(5000);
	         driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	         SkySiteUtils.waitTill(5000);
	  

	         
	         //Validating first name
	         FirstName.isDisplayed();
	         Log.message("First name field is displayed");
	         String fname=FirstName.getAttribute("value");
	         Log.message("Actual First name is:" +fname);
	         Log.message("Expected first name:" +Randomfname);
	         
	         //Validating last name
	         LastName.isDisplayed();
	         Log.message("Last name field is displayed");
	         String Lname=LastName.getAttribute("value");
	         Log.message("Actual Last name is:" +Lname);
	         Log.message("Expected Last name is:" +Randomlname);
	         
	         //Validating title
	         Title.isDisplayed();
	         Log.message("Title field is displayed");
	         String Ttle=Title.getAttribute("value");
	         Log.message("Actual Title is" +Ttle);
	         Log.message("Expected Title is" +Randomtitle);
	         
	         //Validating company
	         CompanyName.isDisplayed();
	         Log.message("Company field is displayed");
	         String compname=CompanyName.getAttribute("value");
	         Log.message("Actual Company is:" +compname); 
	         Log.message("Expected Company is" +RandomCompany);
	         
	         SkySiteUtils.waitTill(2000);
	         
	         Country.isDisplayed();
	         Log.message("Country is displayed");
	         
	         State.isDisplayed();
	         Log.message("State is displayed");
	           
	         
	         String country1=PropertyReader.getProperty("CountryMyProf");
	         Log.message("Expected country is: "+country1);
	         
	         String state1=PropertyReader.getProperty("StateMyProf");
	         Log.message("Expected state is: "+state1);
	         
	         
	         Select select = new Select(driver.findElement(By.cssSelector("#ddl_country"))); 
	         String option = select.getFirstSelectedOption().getText();
	         Log.message("Actual Country is "+option );

	         
	         Select select1 = new Select(driver.findElement(By.cssSelector("#ddl_state"))); 
	         String option1 = select1.getFirstSelectedOption().getText();
	         Log.message("Actual State is "+option1 );
	         

	         if(option.contentEquals(country1) && option1.contentEquals(state1) &&
	        		 fname.contentEquals(Randomfname) &&Lname.contentEquals(Randomlname) &&Ttle.contentEquals(Randomtitle)
	        		 &&compname.contentEquals(RandomCompany)) 
	         {
	        	 return true;
	         }
		 else
				return false;
		
		
		}

		
		
		/** Method for changing the password
         * Scripted By :Trinanjwan
 		*
 		*/
		


		public LoginPage changingOfPassword(String RandomPassword)
		{
			//Log.message("Waiting for change Password button to be appeared");
			//SkySiteUtils.waitForElement(driver, ChangePasswordbtn, 20);
			//Log.message("Change Password button is appeared");
			
		
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        Log.message("Switch to My Frame");
	        SkySiteUtils.waitTill(5000);
			ChangePasswordbtn.click();
			Log.message("Clicked on Change password button");
			SkySiteUtils.waitTill(3000);
			
			String oldpasswrd=PropertyReader.getProperty("pwdtri");
			OldPasswordfield.click();
			OldPasswordfield.sendKeys(oldpasswrd);
	        Log.message(oldpasswrd+ "is provided in the old password field");
	        SkySiteUtils.waitTill(2000);
	        
	        NewPasswordfield.click();
	        NewPasswordfield.sendKeys(RandomPassword);
	        Log.message(RandomPassword+ "is provided in the new password field");
	        SkySiteUtils.waitTill(2000);
	        
	        ConfirmPasswordfield.click();
	        ConfirmPasswordfield.sendKeys(RandomPassword);
	        Log.message(RandomPassword+ "is provided in the confirm password field");
	        SkySiteUtils.waitTill(2000);
	        
	        Updatebtn.click();
	        SkySiteUtils.waitTill(7000);
	        
	        
	        //Handling the session time out part it will navigate to login window
	        
	      
	    	int Clickhere = driver.findElements(By.cssSelector("#ancLoginBack")).size();
	        Log.message("Checking the session time out page is landed");
	        if(Clickhere>0)
	        {
	               driver.findElement(By.cssSelector("#ancLoginBack")).click();
	               Log.message("Clicked on 'Click here to login' link!!!"); 
	               
	        
	        
	        }    
	        return new LoginPage(driver).get();
		}
	
		
		/** Method to update the Email ID and navigate into PROJECTS 
         * Scripted By :Trinanjwan
 		*
 		*/
		
		public ProjectsMyProfPage updateEmail(String RandomEmail)
		
		{
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        Log.message("Switch to My Frame");
	        SkySiteUtils.waitTill(5000);
			String presentemail=Emailfield.getAttribute("value");
			Log.message("Present Email is:"+presentemail);
			SkySiteUtils.waitTill(2000);
			Emailfield.clear();
			Log.message("Email field is cleared");			
			Emailfield.sendKeys(RandomEmail);
			Log.message("New Email is:"+RandomEmail);
			SkySiteUtils.waitTill(2000);
			Savebtn.click();
			Log.message("Clicked on Save button");
			SkySiteUtils.waitTill(7000);
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			SkySiteUtils.waitTill(2000);
			Homebutton.click();
			Log.message("Clicked on Home button");
			SkySiteUtils.waitTill(6000);
			Prjnav.click();
			Log.message("Clicked on Projects button to navigate to PROJECTS");
			SkySiteUtils.waitTill(20000);
			PrjMyProfile.click();
			Log.message("Clicked on My Profile icon");
			SkySiteUtils.waitTill(2000);
			PrjMyProfilenav.click();
			Log.message("Clicked on My Profile to navigate to PROJECTS");
			return new ProjectsMyProfPage(driver).get();
		
			
		}
		
		/** Method to update the Company Name and navigate into PROJECTS 
         * Scripted By :Trinanjwan
 		*
 		*/
		
		public ProjectsMyProfPage updatecompany(String RandomCompany)
		{
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        Log.message("Switch to My Frame");
	        SkySiteUtils.waitTill(5000);
			String presentcomp=CompanyName.getAttribute("value");
			Log.message("Present Company is:"+presentcomp);
			SkySiteUtils.waitTill(2000);
			CompanyName.clear();
			Log.message("Company Name field is cleared");			
			CompanyName.sendKeys(RandomCompany);
			Log.message("New Company Name is:"+RandomCompany);
			SkySiteUtils.waitTill(2000);
			Savebtn.click();
			SkySiteUtils.waitTill(7000);
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			SkySiteUtils.waitTill(2000);
			Homebutton.click();
			Log.message("Clicked on Home button");
			SkySiteUtils.waitTill(6000);
			Prjnav.click();
			Log.message("Clicked on Projects button to navigate to PROJECTS");
			SkySiteUtils.waitTill(20000);
			PrjMyProfile.click();
			Log.message("Clicked on My Profile icon");
			SkySiteUtils.waitTill(2000);
			PrjMyProfilenav.click();
			Log.message("Clicked on My Profile to navigate to PROJECTS");
			return new ProjectsMyProfPage(driver).get();
			
		}
		
		
		/** Method to update the Phone number and navigate into PROJECTS 
         * Scripted By :Trinanjwan
 		*
 		*/
		
	
		public ProjectsMyProfPage updatePhone()
		{
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        Log.message("Switch to My Frame");
	        SkySiteUtils.waitTill(5000);
			String presentcomp=Phonenumberfield.getAttribute("value");
			Log.message("Present Phone No. is:"+presentcomp);
			SkySiteUtils.waitTill(2000);
			Phonenumberfield.clear();
			Log.message("Phone number field is cleared");	
			String newph=PropertyReader.getProperty("Phonenumbchange");
			Log.message("Taken the new phone number from property file");
			Phonenumberfield.sendKeys(newph);
			Log.message("New Ph No. is:"+newph);
	        SkySiteUtils.waitTill(2000);
			Savebtn.click();
			Log.message("Clicked on Save button");
			SkySiteUtils.waitTill(7000);
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			SkySiteUtils.waitTill(2000);
			Homebutton.click();
			Log.message("Clicked on Home button");
			SkySiteUtils.waitTill(6000);
			Prjnav.click();
			Log.message("Clicked on Projects button to navigate to PROJECTS");
			SkySiteUtils.waitTill(20000);
			PrjMyProfile.click();
			Log.message("Clicked on My Profile icon");
			SkySiteUtils.waitTill(2000);
			PrjMyProfilenav.click();
			Log.message("Clicked on My Profile to navigate to PROJECTS");
			return new ProjectsMyProfPage(driver).get();
			
		}
		
		
		
		
		/** Method to update the UserID
         * Scripted By :Trinanjwan
 		*
 		*/
		
		public void updateUserid(String RaduserID)
		{
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        Log.message("Switch to My Frame");
	        SkySiteUtils.waitTill(5000);
	        
	        Userid.clear();
			Log.message("User ID field is cleared");	
			SkySiteUtils.waitTill(5000);
			Userid.sendKeys(RaduserID);
	        SkySiteUtils.waitTill(2000);
			Savebtn.click();
			Log.message("Clicked on Save button");
			SkySiteUtils.waitTill(7000);
			
		}
		
		
		/** Method to check the other field data
         * Scripted By :Trinanjwan
 		*
 		*/


		public void checkOtherField()
		
		{
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        Log.message("Switch to My Frame");
	        SkySiteUtils.waitTill(5000);
	        String occupationother=PropertyReader.getProperty("OCCUOTHER");
	        Select selectoccuother=new Select(Occupationdropdown);
	        selectoccuother.selectByVisibleText(occupationother);
	        Log.message(occupationother+ "is selected");
	        SkySiteUtils.waitForElement(driver, otherfield, 6);
	        Log.message("OTHER field is displayed");
	        String occupationothervalue=PropertyReader.getProperty("OTHER");
	        otherfield.sendKeys(occupationothervalue);
	        Log.message(occupationothervalue+" is entered in the Other field");
	        Savebtn.click();
			Log.message("Clicked on Save button");
		}
		
		/** Method to validate the data in OTHER field
         * Scripted By :Trinanjwan
 		*
 		*/

			public boolean validateOtherfielddata()
			
			
			{
				SkySiteUtils.waitTill(10000);
				String Actualother=otherfield.getAttribute("value");
				Log.message("The actual other is:"+Actualother);
				String occupationother=PropertyReader.getProperty("OTHER");
				Log.message("The expected other is:"+occupationother);
				if((Actualother).contentEquals(occupationother))
					return true;
				else
					return false;
		
			}
			
			
			/** Method to update the Country & State and navigate into PROJECTS 
	         * Scripted By :Trinanjwan
	 		*
	 		*/

		public ProjectsMyProfPage updateCountryState()
		{
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
			SkySiteUtils.waitTill(5000);
			driver.switchTo().frame("myFrame");
			Log.message("Switch to My Frame");
			SkySiteUtils.waitTill(5000);
			
			String country=PropertyReader.getProperty("CountryChange");
	        Select selectcountry=new Select(Country);
	        selectcountry.selectByVisibleText(country);
	        Log.message(country+ "is selected");
	        SkySiteUtils.waitTill(5000);
	         
	        String state=PropertyReader.getProperty("StateChange");
	        Select selectstate=new Select(State);
	        selectstate.selectByIndex(2);
	        Log.message(state+ "is selected");
	        SkySiteUtils.waitTill(5000);
	        
	        Savebtn.click();
	        Log.message("Clicked on Save button");
	        SkySiteUtils.waitTill(7000);
	        driver.switchTo().defaultContent();
	        Log.message("Switched to default content");
	        SkySiteUtils.waitTill(2000);
	        Homebutton.click();
	        Log.message("Clicked on Home button");
	        SkySiteUtils.waitTill(6000);
	        Prjnav.click();
	        Log.message("Clicked on Projects button to navigate to PROJECTS");
	        SkySiteUtils.waitTill(20000);
	        PrjMyProfile.click();
	        Log.message("Clicked on My Profile icon");
	        SkySiteUtils.waitTill(2000);
	        PrjMyProfilenav.click();
	        Log.message("Clicked on My Profile to navigate to PROJECTS");
	        return new ProjectsMyProfPage(driver).get();
	
		}
		
		/** Method to navigate into collection list from My Profile
         * Scripted By :Trinanjwan
 		*
 		*/
		
		
		public FnaHomePage navigateToCollection()
		{
			driver.switchTo().defaultContent();
			Log.message("Switch to default content");
			SkySiteUtils.waitForElement(driver, Collectionbtn, 10);
			Log.message("The collection button is visible now");
			
			Actions action1=new Actions(driver);
			action1.moveToElement(Collectionbtn).click(Collectionbtn).build().perform();
			Log.message("Clicked on collection button");
	        return new FnaHomePage(driver).get();
	
		}
		

		/** Method to update the Security questions
         * Scripted By :Trinanjwan
 		*
 		*/
		
		@FindBy(css="#ddlSecurityQuestions1")
		@CacheLookup
		WebElement Question1;
		
		@FindBy(css="#ddlSecurityQuestions2")
		@CacheLookup
		WebElement Question2;
		
		@FindBy(css="#ddlSecurityQuestions3")
		@CacheLookup
		WebElement Question3;
		
		@FindBy(css="#txtSecurityAnswer1")
		@CacheLookup
		WebElement Answer1;
			
		@FindBy(css="#txtSecurityAnswer2")
		@CacheLookup
		WebElement Answer2;
		
		@FindBy(css="#txtSecurityAnswer3")
		@CacheLookup
		WebElement Answer3;
		
			
		public void updateSecurityQuestions()
		{
		
		SkySiteUtils.waitForElement(driver, FirstName, 40);
		driver.switchTo().defaultContent();
        SkySiteUtils.waitTill(7000);
        driver.switchTo().frame("myFrame");
        SkySiteUtils.waitTill(7000);
        Log.message("Switched to my frame");
   		JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,10000)");
        Log.message("Scroll down below");
        
        //Selecting question 1
        SkySiteUtils.waitForElement(driver, Question1, 40);
        Select question1=new Select(Question1);
        question1.selectByIndex(1);
        Log.message("Question 1 is selected now");
        String answer1=PropertyReader.getProperty("Answer1tri");
        SkySiteUtils.waitForElement(driver, Answer1, 40);
        Answer1.sendKeys(answer1);
        Log.message(answer1+ " is provided in Answer 1 field");
        
        //Selecting question 2
        SkySiteUtils.waitForElement(driver, Question2, 40);
        Select question2=new Select(Question2);
        question2.selectByIndex(2);
        Log.message("Question 2 is selected now");
        String answer2=PropertyReader.getProperty("Answer2tri");
        SkySiteUtils.waitForElement(driver, Answer2, 40);
        Answer2.sendKeys(answer2);
        Log.message(answer2+ " is provided in Answer 2 field");
        
        
        //Selecting question 3
        SkySiteUtils.waitForElement(driver, Question3, 40);
        Select question3=new Select(Question3);
        question3.selectByIndex(3);
        Log.message("Question 3 is selected now");
        String answer3=PropertyReader.getProperty("Answer3tri");
        SkySiteUtils.waitForElement(driver, Answer3, 40);
        Answer3.sendKeys(answer3);
        Log.message(answer3+ " is provided in Answer 3 field");
        js.executeScript("window.scrollBy(0,-10000)");
        Log.message("Scrolled up");
        
        SkySiteUtils.waitForElement(driver, Savebtn, 40);
        Savebtn.click();
        Log.message("Clicked on Save button");
        
		}
		
		
		/**
		 * Method for landing into the Account Settings page from My profile page | Scripted Trinanjwan
		 */
		
		
		@FindBy(css = "#setting")
		@CacheLookup
		WebElement Settingsbtn;
		
		
		@FindBy(css = "#liAccountSettings > a:nth-child(1)")
		@CacheLookup
		WebElement AccountSettingsbtn;
		
		


		public FnaAccountSettingsPage landingAccountSettingsPageprof()

		{
			
			driver.switchTo().defaultContent();
	        SkySiteUtils.waitTill(5000);
			Log.message("Waiting for Settings button to display");
			SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
			Log.message("Settings button is displayed");
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", Settingsbtn);
			Log.message("Clicked on Settings button");
			SkySiteUtils.waitForElement(driver, AccountSettingsbtn, 10);
			executor.executeScript("arguments[0].click();", AccountSettingsbtn);
			Log.message("Clicked on Account Settings button from list");
			return new FnaAccountSettingsPage(driver).get();

		}
		
	
		/**
		 * Method for to navigate into PROJECTS module from account settings page | Scripted Trinanjwan
		 */
		
		public ProjectsMyProfPage navigateToProjects()
		
		{
			
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			SkySiteUtils.waitTill(7000);
			Homebutton.click();
			Log.message("Clicked on Home button");
			SkySiteUtils.waitTill(7000);
			Prjnav.click();
			Log.message("Clicked on Projects button to navigate to PROJECTS");
			return new ProjectsMyProfPage(driver).get();
		}
	

		/**
		 * Method to verify the answers of the security questions | Scripted Trinanjwan
		 */
		

		public boolean verifyAnswers()
		
		{
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, FirstName, 10);
			driver.switchTo().defaultContent();
	        SkySiteUtils.waitTill(5000);
	        driver.switchTo().frame("myFrame");
	        SkySiteUtils.waitTill(5000);
	        Log.message("Switched to my frame");
	   		JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("window.scrollBy(0,10000)");
	        Log.message("Scroll down below");
	        
	        String expectedanswer1=PropertyReader.getProperty("Answer1tri");
	        SkySiteUtils.waitForElement(driver, Answer1, 10);       
	        String actualanswer1=Answer1.getAttribute("value");
	        Log.message("Expected Answer1:"+expectedanswer1);
	        Log.message("Actual Answer1:"+actualanswer1);
	        
	        String expectedanswer2=PropertyReader.getProperty("Answer2tri");
	        SkySiteUtils.waitForElement(driver, Answer2, 10);
	        String actualanswer2=Answer2.getAttribute("value");
	        Log.message("Expected Answer2:"+expectedanswer2);
	        Log.message("Actual Answer2:"+actualanswer2);
	        
	        	        
	        String expectedanswer3=PropertyReader.getProperty("Answer3tri");
	        SkySiteUtils.waitForElement(driver, Answer3, 10);
	        String actualanswer3=Answer3.getAttribute("value");
	        Log.message("Expected Answer3:"+expectedanswer3);
	        Log.message("Actual Answer3:"+actualanswer3);
	        
	        if(expectedanswer1.contentEquals(actualanswer1) && expectedanswer2.contentEquals(actualanswer2) &&
	        		expectedanswer3.contentEquals(actualanswer3))
	        	return true;
	        else
	        	return false;
			
	     
			
			
		}
		




	}
		

	
		