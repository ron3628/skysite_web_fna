package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class CollectionTeamsPage extends LoadableComponent<CollectionTeamsPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public CollectionTeamsPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	
	/**
	 * Method written for Random(Team)
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Teams()
	{

		String str = "Team";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for Random(contact)
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Contact()
	{

		String str = "Contact";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for Random(edit)
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Edit()
	{

		String str = "Edit";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	@FindBy(css = "#ProjectMenu1_Teams>a")
	WebElement btnteam;
	
	@FindBy(xpath = "//*[@id='btnAddNewTeam']")
	WebElement btnaddteam;
	
	@FindBy(xpath = "//*[@id='txtTeamName']")
	WebElement txtTeamName;
	
	@FindBy(xpath = "//i[@class='icon icon-option ico-lg']")
	WebElement moreiconoption;
	
	@FindBy(xpath = "//i[@class='icon icon-add-teamMemberAddress']")
	WebElement addteamMemberAddress;
	
	@FindBy(xpath = "//*[@id='btnAddClose']")
	WebElement btnAddClose;
	
	@FindBy(xpath = "//*[@id='btnSaveTop']")
	WebElement btnSaveTop;
	
	@FindBy(xpath = ".//*[@id='divTeamGridData']/div[2]/table/tbody/tr[2]/td[2]")
	WebElement TeamGridData;
	
	@FindBy(xpath=".//*[@id='btnAddTUFromAB']")
	WebElement btnAddContactToTeam;
	
	/** 
     * Method written for Adding Team member
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Add_team_member(String Teams,String ContactName)
    {
           boolean result1=false;
           
           SkySiteUtils.waitTill(5000);
           driver.switchTo().defaultContent();
           btnteam.click();
           Log.message("Teams button has been clicked");
           SkySiteUtils.waitTill(5000);
           driver.switchTo().frame(driver.findElement(By.id("myFrame")));
           SkySiteUtils.waitTill(5000);
           btnaddteam.click();
           Log.message("add button has been clicked");
           SkySiteUtils.waitTill(5000);          
           txtTeamName.sendKeys(Teams);
           Log.message("Team name has been entered");
           SkySiteUtils.waitTill(3000);  
           
           if(btnAddContactToTeam.isDisplayed())
           {
        	   SkySiteUtils.waitTill(3000);
        	   btnAddContactToTeam.click();
        	   Log.message("Clicked to add contact button");
        	   SkySiteUtils.waitTill(3000);
        	   
           }
           else
           {
        	   SkySiteUtils.waitTill(3000);
        	   moreiconoption.click();
               Log.message("more option button has been clicked");
               SkySiteUtils.waitTill(5000);  
               addteamMemberAddress.click();
               Log.message("add team member button has been clicked");
               SkySiteUtils.waitTill(5000);
               
           }
                      
           
           int contact_count=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   			Log.message("contact check box Count is:"+contact_count);			 
   			SkySiteUtils.waitTill(5000);
   			//Loop start '2' as contact list start from tr[2]
   			int i=0;
   			for(i = 1; i <= contact_count; i++) 
   			{
   				//x path as dynamically providing all rows(i) tr[i] 		
   				String Contact_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
   				Log.message("Exp Contact name is:"+Contact_Name);
   				SkySiteUtils.waitTill(5000); 			
   				Log.message("Act Contact name is:"+ContactName);
   				//Checking contact name equal with each row of table
   				if(Contact_Name.trim().equalsIgnoreCase(ContactName))
   				{	        	 
   					Log.message("Exp contact name has been validated!!!");					
   					break;
   				}		
   			}
   			SkySiteUtils.waitTill(5000);
   			driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
   			Log.message("check box button has been clicked");
            SkySiteUtils.waitTill(5000);            
   			btnAddClose.click();
   			Log.message("add&close button has been clicked");
            SkySiteUtils.waitTill(8000);  
   			btnSaveTop.click();
   			Log.message("save&close button has been clicked");
            SkySiteUtils.waitTill(8000);  
   			
           
            int Team_count=driver.findElements(By.xpath("html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]")).size();
   			Log.message("contact check box Count is:"+Team_count);			 
   			SkySiteUtils.waitTill(5000);
   			//Loop start '2' as contact list start from tr[2]
   			int j=0;
   			for(j = 1; j <= Team_count; j++) 
   			{
   				//x path as dynamically providing all rows(i) tr[i] 		
   				String Team_Name=driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).getText();
   				Log.message("Exp Team name is:"+Team_Name);
   				SkySiteUtils.waitTill(3000);    				
   				//Checking contact name equal with each row of table
   				if(Team_Name.contains(Teams))
   				{	        	 
   					Log.message("Exp Team name has been validated!!!");					
   					break;
   				}		
   			}
   			SkySiteUtils.waitTill(5000);
   			driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).click();
   			SkySiteUtils.waitTill(5000);
   			if(TeamGridData.isDisplayed())
   				return true;
   			else
   				return false;
    }
    
    @FindBy(xpath = "//*[@id='Button1']")
	WebElement btnButton1;
    
    @FindBy(xpath = "(//i[@class='icon icon-add-existingTeamMem'])[2]")
	WebElement addexistingTeamMem;
    
    @FindBy(xpath = "//*[@id='btnAdddedExistedAndClose']")
   	WebElement btnAdddedExistedAndClose;
	
    /** 
     * Method written for Adding Existing Team member
     * Scripted By: Sekhar     
     * @return
     */
    public boolean Add_Existing_team_member(String ContactName)
    {
               
           btnButton1.click();
           Log.message("more option button has been clicked");
           SkySiteUtils.waitTill(5000);                
           addexistingTeamMem.click();
           Log.message("add existing team button has been clicked");
           SkySiteUtils.waitTill(5000);   
           int contact_count=driver.findElements(By.xpath("//*[@id='divPTMemebrsGridNew']/div[2]/table/tbody/tr/td[1]/img")).size();
  			Log.message("contact check box Count is:"+contact_count);			 
  			SkySiteUtils.waitTill(5000);
  			//Loop start '2' as contact list start from tr[2]
  			int i=0;
  			for(i = 1; i <= contact_count; i++) 
  			{
  				//x path as dynamically providing all rows(i) tr[i] 		
  				String Contact_Name=driver.findElement(By.xpath("(//*[@id='divPTMemebrsGridNew']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
  				Log.message("Exp Contact name is:"+Contact_Name);
  				SkySiteUtils.waitTill(5000); 			
  				
  				//Checking contact name equal with each row of table
  				if(Contact_Name.trim().equalsIgnoreCase(ContactName))
  				{	        	 
  					Log.message("Exp contact name has been validated!!!");					
  					break;
  				}		
  			}
  			SkySiteUtils.waitTill(5000);
  			driver.findElement(By.xpath("(//*[@id='divPTMemebrsGridNew']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
  			Log.message("check box button has been clicked");
  			SkySiteUtils.waitTill(5000); 
  			btnAdddedExistedAndClose.click();
  			Log.message("add&close button has been clicked");
  			SkySiteUtils.waitTill(5000); 
  			
  			 int contact_count1=driver.findElements(By.xpath("//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[1]/img")).size();
   			Log.message("contact check box Count is:"+contact_count1);			 
   			SkySiteUtils.waitTill(5000);
   			//Loop start '2' as contact list start from tr[2]
   			int j=0;
   			for(j = 1; j <= contact_count1; j++) 
   			{
   				//x path as dynamically providing all rows(i) tr[i] 		
   				String Contact_Name1=driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
   				Log.message("Exp Contact name is:"+Contact_Name1);
   				SkySiteUtils.waitTill(5000); 			
   				
   				//Checking contact name equal with each row of table
   				if(Contact_Name1.trim().equalsIgnoreCase(ContactName))
   				{	        	 
   					Log.message("Exp contact name has been validated!!!");					
   					break;
   				}		
   			}
   			SkySiteUtils.waitTill(5000);
   			String Contactna=driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
   			SkySiteUtils.waitTill(5000);
   			if(Contactna.contains(ContactName))
   				return true;
   			else
   				return false;
  			
    }
    
    @FindBy(xpath = "(//i[@class='icon icon-delete'])[2]")
   	WebElement btnicondelete;
    
    @FindBy(xpath = "//*[@id='button-1']")
   	WebElement btnbutton1;
    
    /** 
     * Method written for Delete Team member
     * Scripted By: Sekhar     
     * @return
     */
    public boolean Delete_team_member(String ContactName)
    {
    	SkySiteUtils.waitTill(5000);       
    	int contact_count1=driver.findElements(By.xpath("//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[1]/img")).size();
    	Log.message("contact check box Count is:"+contact_count1);			 
    	SkySiteUtils.waitTill(5000);
    	//Loop start '2' as contact list start from tr[2]
    	int j=0;
    	for(j = 1; j <= contact_count1; j++) 
    	{
    		//x path as dynamically providing all rows(i) tr[i] 		
    		String Contact_Name1=driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
    		Log.message("Exp Contact name is:"+Contact_Name1);
    		SkySiteUtils.waitTill(5000); 			
    		
    		//Checking contact name equal with each row of table
    		if(Contact_Name1.trim().equalsIgnoreCase(ContactName))
    		{	        	 
    			Log.message("Exp contact name has been validated!!!");					
    			break;
    		}		
    	}
    	SkySiteUtils.waitTill(5000);
    	driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
    	Log.message("Exp contact check box has been clicked");
    	SkySiteUtils.waitTill(5000);
    	
    	String File_Count=driver.findElement(By.xpath(".//*[@id='divTeamGridPagination']/div[2]/div/div")).getText();//Getting label counts
        Log.message("count is: "+File_Count); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] x= File_Count.split(" ");
        String Avl_Count=x[3];        
        int BeforeTotalcount=Integer.parseInt(Avl_Count);  		
        Log.message("Before Total Avl count is:"+BeforeTotalcount); //Getting counts     	
    	btnButton1.click();
    	Log.message("more option button has been clicked");
    	SkySiteUtils.waitTill(5000);                
    	btnicondelete.click();
    	Log.message("Delete member button has been clicked");
    	SkySiteUtils.waitTill(5000);     	
    	driver.switchTo().defaultContent();
    	btnbutton1.click();
    	Log.message("Yes button has been clicked");
    	SkySiteUtils.waitTill(5000); 
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
        SkySiteUtils.waitTill(5000);
    	String Aftercontact_Count=driver.findElement(By.xpath(".//*[@id='divTeamGridPagination']/div[2]/div/div")).getText();//Getting label counts
        Log.message("After count is: "+Aftercontact_Count); //Getting  counts        
        SkySiteUtils.waitTill(5000);       
        String[] y= Aftercontact_Count.split(" ");
        String Avl_Count1=y[3];        
        int AfterTotalcount=Integer.parseInt(Avl_Count1);  		
        Log.message("After Total Avl count is:"+AfterTotalcount); //Getting counts  
        SkySiteUtils.waitTill(5000); 
        if(AfterTotalcount==(BeforeTotalcount-1))
        	return true;
        else
        	return false;
    }
    
    @FindBy(xpath = "//*[@id='txtSerachValueFromTeam']")
   	WebElement txtSerachValueFromTeam;
    
    @FindBy(xpath = "//*[@id='btnSearch']")
   	WebElement btnSearch;
    
    @FindBy(xpath = ".//*[@id='divTeamGridData']/div[2]/table/tbody/tr[2]/td[2]")
   	WebElement teamGridData;
    
    
    /** 
     * Method written for Search team member
     * Scripted By: Sekhar     
     * @return
     */
    public boolean Search_team_member(String ContactName)
    {
    	txtSerachValueFromTeam.sendKeys(ContactName);
    	Log.message("Contact name has been entered");
    	SkySiteUtils.waitTill(5000); 
    	btnSearch.click();
    	Log.message("Search icon button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	String After_Search=teamGridData.getText();
    	Log.message("After search contact name is:"+After_Search);
    	Log.message(ContactName);
    	SkySiteUtils.waitTill(5000);
    	if(ContactName.equalsIgnoreCase(After_Search.trim()))
    		return true;
    	else
    		return false;
    }
    
    @FindBy(xpath = "(//i[@class='icon icon-add-teamMemberAddress'])[3]")
   	WebElement addteamMemberaddress;
    
    @FindBy(xpath = "//*[@id='btnAddClose']")
   	WebElement btnAClose;
	
    /** 
     * Method written for Add Team member from address book
     * Scripted By: Sekhar     
     * @return
     */
    public boolean Add_team_member_fromaddressbook(String ContactName)
    {
               
           btnButton1.click();
           Log.message("more option button has been clicked");
           SkySiteUtils.waitTill(5000);                
           addteamMemberaddress.click();
           Log.message("add team member from address book button has been clicked");
           SkySiteUtils.waitTill(5000);   
           int contact_count=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
  			Log.message("contact check box Count is:"+contact_count);			 
  			SkySiteUtils.waitTill(5000);
  			//Loop start '2' as contact list start from tr[2]
  			int i=0;
  			for(i = 1; i <= contact_count; i++) 
  			{
  				//x path as dynamically providing all rows(i) tr[i] 		
  				String Contact_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
  				Log.message("Exp Contact name is:"+Contact_Name);
  				SkySiteUtils.waitTill(5000); 			
  				
  				//Checking contact name equal with each row of table
  				if(Contact_Name.trim().equalsIgnoreCase(ContactName))
  				{	        	 
  					Log.message("Exp contact name has been validated!!!");					
  					break;
  				}		
  			}
  			SkySiteUtils.waitTill(5000);
  			driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
  			Log.message("check box button has been clicked");
  			SkySiteUtils.waitTill(5000); 
  			btnAClose.click();
  			Log.message("add&close button has been clicked");
  			SkySiteUtils.waitTill(5000); 
  			
  			 int contact_count1=driver.findElements(By.xpath("//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[1]/img")).size();
   			Log.message("contact check box Count is:"+contact_count1);			 
   			SkySiteUtils.waitTill(5000);
   			//Loop start '2' as contact list start from tr[2]
   			int j=0;
   			for(j = 1; j <= contact_count1; j++) 
   			{
   				//x path as dynamically providing all rows(i) tr[i] 		
   				String Contact_Name1=driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
   				Log.message("Exp Contact name is:"+Contact_Name1);
   				SkySiteUtils.waitTill(5000); 			
   				
   				//Checking contact name equal with each row of table
   				if(Contact_Name1.trim().equalsIgnoreCase(ContactName))
   				{	        	 
   					Log.message("Exp contact name has been validated!!!");					
   					break;
   				}		
   			}
   			SkySiteUtils.waitTill(5000);
   			String Contactna=driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
   			SkySiteUtils.waitTill(5000);
   			if(Contactna.contains(ContactName))
   				return true;
   			else
   				return false;  			
    }
    
    //@FindBy(xpath = ".//*[@id='divTeamGridData']/div[2]/table/tbody/tr[2]/td[2]")
	//WebElement TeamGridData;
	
	/** 
     * Method written for Edit team and delete teams
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Editteam_Deleteteams(String Teams)
    {      
           
    	int Team_count=driver.findElements(By.xpath("html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]")).size();
    	Log.message("contact check box Count is:"+Team_count);			 
    	SkySiteUtils.waitTill(5000);
    	//Loop start '2' as contact list start from tr[2]
    	int j=0;
    	for(j = 1; j <= Team_count; j++) 
    	{
    		//x path as dynamically providing all rows(i) tr[i] 		
    		String Team_Name=driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).getText();
    		Log.message("Exp Team name is:"+Team_Name);
    		SkySiteUtils.waitTill(3000);    				
    		//Checking contact name equal with each row of table
    		if(Team_Name.contains(Teams))
    		{	        	 
    			Log.message("Exp Team name has been validated!!!");					
    			break;
    		}		
    	}	
    	SkySiteUtils.waitTill(5000);  		  			
    	if((driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[1]/a)["+j+"]")).isDisplayed())
    			&&(driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[2]/a)["+j+"]")).isDisplayed()))
    		return true;
    	else	
    		return false;
          
    }
    
    @FindBy(xpath = "html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[2]/a")
  	WebElement btnDeleteowner;
    
    @FindBy(xpath = "//span[@class='noty_text']")
	WebElement notytext;
  	
  	/** 
       * Method written for delete the Owner teams
       * Scripted By: Sekhar     
       * @throws AWTException 
       */
  	
    public boolean Delete_Ownerteam()
    {    
    	  
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().defaultContent();
    	btnteam.click();
    	Log.message("Teams button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(5000);   
    	btnDeleteowner.click();
    	Log.message("Delete owner team button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().defaultContent();
    	SkySiteUtils.waitTill(3000);
    	btnbutton1.click();
    	Log.message("yes button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	SkySiteUtils.waitForElement(driver, notytext, 60);           
    	if(notytext.isDisplayed())
    		return true;
    	else
    		return false;          
      }
      
      @FindBy(xpath = "//i[@class='icon icon-option ico-lg']")
      WebElement btnmoreoption;
      
      @FindBy(xpath = "//i[@class='icon icon-account-team']")
      WebElement btnaddnewteam;
      
      @FindBy(css = "#ctl00_DefaultContent_rdoHostedUserType")
      WebElement btnrdoHostedUser;
      
      @FindBy(css = "#ctl00_DefaultContent_txtFirstName")
      WebElement btntxtFirstName;
      
      @FindBy(css = "#ctl00_DefaultContent_txtEmail")
      WebElement btntxtEmail;
      
      @FindBy(css = "#ctl00_DefaultContent_Button3")
      WebElement btnButton3;
      
      @FindBy(xpath = "(//i[@class='icon icon-reset ico-lg'])[1]")
      WebElement btniconiconreset;
      
      @FindBy(xpath = "//i[@class='icon icon-delete']")
      WebElement btniconicondelete;
      
    /** 
       * Method written for Edit team button after add the contact and edit & delete contact in to team
       * Scripted By: Sekhar     
       * @throws AWTException 
     */
  	
  public boolean Editteam_Addmember_edit_Delete_Contact(String Teams,String Contactname,String Editname)
   {      
          
	   SkySiteUtils.waitTill(5000);
	   driver.switchTo().defaultContent();
       btnteam.click();
       Log.message("Teams button has been clicked");
       SkySiteUtils.waitTill(5000);
       driver.switchTo().frame(driver.findElement(By.id("myFrame")));
       SkySiteUtils.waitTill(5000);
	   
      	int Team_count=driver.findElements(By.xpath("html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]")).size();
      	Log.message("contact check box Count is:"+Team_count);			 
      	SkySiteUtils.waitTill(5000);
      	//Loop start '2' as contact list start from tr[2]
      	int j=0;
      	for(j = 1; j <= Team_count; j++) 
      	{
      		//x path as dynamically providing all rows(i) tr[i] 		
      		String Team_Name=driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).getText();
      		Log.message("Exp Team name is:"+Team_Name);
      		SkySiteUtils.waitTill(3000);    				
      		//Checking contact name equal with each row of table
      		if(Team_Name.contains(Teams))
      		{	        	 
      			Log.message("Exp Team name has been validated!!!");					
      			break;
      		}		
      	}	
      	SkySiteUtils.waitTill(5000);  		  			
      	driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[1]/a)["+j+"]")).click();
      	Log.message("Edit team icon has been clicked");
      	SkySiteUtils.waitTill(10000); 
      	btnmoreoption.click();
      	Log.message("more option button has been clicked");
      	SkySiteUtils.waitTill(3000);
      	btnaddnewteam.click();
      	Log.message("add newteam member button has been clicked");
      	SkySiteUtils.waitTill(5000);
      	String parentHandle = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}		
		SkySiteUtils.waitForElement(driver, btnrdoHostedUser, 60);
		btnrdoHostedUser.click();
		Log.message("employee radio button has been clicked");
      	SkySiteUtils.waitTill(3000);  		
      	btntxtFirstName.sendKeys(Contactname);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		btntxtEmail.sendKeys(Contactname + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		btnButton3.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		btniconiconreset.click();
		Log.message("Reset button has been clicked");
		SkySiteUtils.waitTill(3000);
		int Contact_count = driver.findElements(By.xpath("//*[@id='divTeamGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("contact Count is:" + Contact_count);
		SkySiteUtils.waitTill(5000);      	
		//Loop start '2' as contact list start from tr[2]
      	int i=0;
      	for(i = 1; i <= Contact_count; i++) 
      	{
      		//x path as dynamically providing all rows(i) tr[i] 		
      		String Contact_Name=driver.findElement(By.xpath("(//*[@id='divTeamGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
      		Log.message("Exp Contact name is:"+Contact_Name);
      		SkySiteUtils.waitTill(3000);
      		Log.message(Contactname);
      		//Checking contact name equal with each row of table
      		if(Contactname.equalsIgnoreCase(Contact_Name.trim()))
      		{	        	 
      			Log.message("Exp Contact name has been validated!!!");					
      			break;
      		}		
      	}	
      	SkySiteUtils.waitTill(5000);  	
      	driver.findElement(By.xpath("(//*[@id='divTeamGrid']/div[2]/table/tbody/tr/td[8]/a)["+i+"]")).click();
      	Log.message("Edit icon has been clicked");
      	SkySiteUtils.waitTill(5000);
      	
      	String parentHandle1 = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		SkySiteUtils.waitForElement(driver, btntxtFirstName, 60);		
		btntxtFirstName.clear();
		Log.message("frist name has been cleared");
      	SkySiteUtils.waitTill(3000);  		
      	btntxtFirstName.sendKeys(Editname);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		btntxtEmail.clear();
		Log.message("email has been cleared");
      	SkySiteUtils.waitTill(3000); 
		btntxtEmail.sendKeys(Editname + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		btnButton3.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle1);// Switch back to page
		SkySiteUtils.waitTill(3000);		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		btniconiconreset.click();
		Log.message("Reset button has been clicked");
		SkySiteUtils.waitTill(3000);
		
		int Contact_count1 = driver.findElements(By.xpath("//*[@id='divTeamGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("contact Count is:" + Contact_count1);
		SkySiteUtils.waitTill(5000);
      	
		//Loop start '2' as contact list start from tr[2]
      	int k=0;
      	for(k = 1; k <= Contact_count1; k++) 
      	{
      		//x path as dynamically providing all rows(i) tr[i] 		
      		String Edit_Contact_Name=driver.findElement(By.xpath("(//*[@id='divTeamGrid']/div[2]/table/tbody/tr/td[2])["+k+"]")).getText();
      		Log.message("Exp Edit Contact name is:"+Edit_Contact_Name);
      		SkySiteUtils.waitTill(3000);    				
      		//Checking contact name equal with each row of table
      		if(Editname.equalsIgnoreCase(Edit_Contact_Name.trim()))
      		{	        	 
      			Log.message("Exp Edit Contact name has been validated!!!");					
      			break;
      		}		
      	}	
      	SkySiteUtils.waitTill(5000);  	
      	driver.findElement(By.xpath("(//*[@id='divTeamGrid']/div[2]/table/tbody/tr/td[1]/img)["+k+"]")).click();
      	Log.message("Edit icon has been clicked");
      	SkySiteUtils.waitTill(5000);
      	
      	String Beforecontact_Count=driver.findElement(By.xpath("//*[@id='divTeamGridPagnation']/div[2]/div/div")).getText();//Getting label counts
        Log.message("Before count is: "+Beforecontact_Count); //Getting  counts        
        SkySiteUtils.waitTill(5000);       
        String[] x=Beforecontact_Count.split(" ");
        String Avl_Count=x[3];        
        int BeforeTotalcount=Integer.parseInt(Avl_Count);  		
        Log.message("Before Total Avl count is:"+BeforeTotalcount); //Getting counts        	
        SkySiteUtils.waitTill(3000);
      	btnmoreoption.click();
      	Log.message("more option button has been clicked");
      	SkySiteUtils.waitTill(3000);
      	btniconicondelete.click();
      	Log.message("Delete member button has been clicked");
      	SkySiteUtils.waitTill(3000);
      	driver.switchTo().defaultContent();
    	SkySiteUtils.waitTill(3000);
      	btnbutton1.click();
      	Log.message("Yes button has been clicked");
      	SkySiteUtils.waitTill(5000);
      	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);		
		String Aftercontact_Count=driver.findElement(By.xpath("//*[@id='divTeamGridPagnation']/div[2]/div/div")).getText();//Getting label counts
        Log.message("After count is: "+Aftercontact_Count); //Getting  counts        
        SkySiteUtils.waitTill(5000);       
        String[] y=Aftercontact_Count.split(" ");
        String Avl_Count1=y[3];        
        int AfterTotalcount=Integer.parseInt(Avl_Count1);  		
        Log.message("After Total Avl count is:"+AfterTotalcount); //Getting counts
        SkySiteUtils.waitTill(3000);
        
        if(AfterTotalcount==(BeforeTotalcount-1))
        	return true;
        else
        	return false;
      }
  
  @FindBy(xpath = "//i[@class='icon icon-add-teamMemberAddress']")
  WebElement btniconiconaddteamMemberAddress;
  
  @FindBy(xpath = "//i[@class='icon icon-add-existingTeamMem']")
  WebElement btniconiconaddexistingTeamMem;
  
  @FindBy(xpath = "//i[@class='icon icon-resend']")
  WebElement btniconiconresend;
    
    
  /** 
   * Method written for verify all options in edit team
   * Scripted By: Sekhar     
   * @throws AWTException 
   */
	
  public boolean Verify_alloption_Editteam(String Teams)
  {    
	  
	  SkySiteUtils.waitTill(5000);
	  driver.switchTo().defaultContent();
	  btnteam.click();
	  Log.message("Teams button has been clicked");
	  SkySiteUtils.waitTill(5000);
	  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	  SkySiteUtils.waitTill(5000);
	   
	  int Team_count=driver.findElements(By.xpath("html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]")).size();
	  Log.message("contact check box Count is:"+Team_count);			 
	  SkySiteUtils.waitTill(5000);
	  //Loop start '2' as contact list start from tr[2]
	  int j=0;
	  for(j = 1; j <= Team_count; j++) 
	  {
		  //x path as dynamically providing all rows(i) tr[i] 		
		  String Team_Name=driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).getText();
		  Log.message("Exp Team name is:"+Team_Name);
		  SkySiteUtils.waitTill(3000);    				
		  //Checking contact name equal with each row of table
		  if(Team_Name.contains(Teams))
		  {	        	 
			  Log.message("Exp Team name has been validated!!!");					
			  break;
		  }		
	  }	
	  SkySiteUtils.waitTill(5000);  		  			
	  driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[1]/a)["+j+"]")).click();
	  Log.message("Edit team icon has been clicked");
	  SkySiteUtils.waitTill(10000); 
	  btnmoreoption.click();
	  Log.message("more option button has been clicked");
	  SkySiteUtils.waitTill(3000);
	  if((btniconiconaddteamMemberAddress.isDisplayed())&&(btniconiconaddexistingTeamMem.isDisplayed())
			  &&(btniconicondelete.isDisplayed())&&(btnaddnewteam.isDisplayed())&&(btniconiconresend.isDisplayed()))
		  return true;
	  else
		  return false;     
  	}

  
  @FindBy(xpath=".//*[@id='btnManageTeamPermission']")
  WebElement btnManagePermission;
  
  @FindBy(xpath=".//*[@id='btnPerSave']")
  WebElement btnSavePermissionPopover;
  
  @FindBy(xpath=".//*[@id='divPermissionActionForCallingPage']/button[3]")
  WebElement btnClosePermissionPopover;
  
  
  /** 
   * Method written for verify all options in edit team
   * Scripted By: Sekhar     
   * 
   */
  public void AddTeamPermission(String TeamName, String PermissionName) 
  {
	
	  SkySiteUtils.waitTill(5000);
	  SkySiteUtils.waitForElement(driver, btnManagePermission, 30);
	  btnManagePermission.click();
	  Log.message("Clicked on manage permission button");
	  SkySiteUtils.waitTill(3000);
	 
      Log.message("Switching frame");
      SkySiteUtils.waitForElement(driver, driver.findElement(By.id("ifrmPermission")), 30);
      driver.switchTo().frame(driver.findElement(By.id("ifrmPermission")));
      SkySiteUtils.waitTill(5000);
      
      int count = driver.findElements(By.xpath(".//*[@id='divSelTeamOrMemberGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
      
      for(int i=1; i<=count; i++)
      {
    	  
    	  if(driver.findElement(By.xpath(".//*[@id='divSelTeamOrMemberGrid']/div[2]/table/tbody/tr["+i+"+"+1+"]/td[3]")).getText().contains(TeamName))
    	  {
    		  
    		  driver.findElement(By.xpath(".//*[@id='divSelTeamOrMemberGrid']/div[2]/table/tbody/tr["+i+"+"+1+"]/td[1]/img")).click();
    		  Log.message("Clicked on desired group/user");
    		  SkySiteUtils.waitTill(3000);
    		  
    	  }
      }
         
      int count1 = driver.findElements(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr/td[1]")).size();
      
      for(int j=1; j<count1; j++)
      {
    	  
    	  if(driver.findElement(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr["+j+"+"+1+"]/td[1]")).getText().contains(PermissionName))
    	  {
    		  
    		  driver.findElement(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr[2]/td[2]/img")).click();
    		  SkySiteUtils.waitTill(3000);
    		  driver.findElement(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr[2]/td[2]/img")).click();
    		  SkySiteUtils.waitTill(3000);
    		  driver.findElement(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr["+j+"+"+1+"]/td[2]/img")).click();
    		  SkySiteUtils.waitTill(3000); 		  
    		  Log.message("Clicked on desired permission");
    		  SkySiteUtils.waitTill(3000);
    		  
    		  Log.message("Switching to default content");
    		  driver.switchTo().defaultContent();
    		  SkySiteUtils.waitTill(5000);
    		  Log.message("Switing frame");
    	      driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	      SkySiteUtils.waitTill(5000);
    		  
    		  SkySiteUtils.waitForElement(driver, btnSavePermissionPopover, 30);
    		  btnSavePermissionPopover.click();
    		  Log.message("Clicked on Save Permission popover button");
    		  SkySiteUtils.waitTill(5000);
    		  
    		  SkySiteUtils.waitForElement(driver, btnClosePermissionPopover, 30);
    		  btnClosePermissionPopover.click();
    		  Log.message("Clicked on Close Permission popover button");
    		  SkySiteUtils.waitTill(5000);
    		  
    		  break;
    		  
    	  }
      }
       
  } 
  
  	
  
  	
  @FindBy(xpath = "//*[@id='user-info']")
  WebElement btnuserinfo;
	
  @FindBy(xpath = "//*[@id='user-info']")
  WebElement clkprofile;
	
  @FindBy(xpath = "//i[@class='icon icon-logout icon-lg']")
  WebElement pfloutput;
	
  @FindBy(xpath = "//*[@id='button-1']")
  WebElement btnyes;
  
 /**
 * Method written for logout
 * By: Ranadeep Ghosh
 * @return
 */
  public boolean logout() 
  {
	  Log.message("Switching to default content");
	  driver.switchTo().defaultContent();
	  SkySiteUtils.waitTill(5000);
		
	  SkySiteUtils.waitForElement(driver, btnuserinfo, 30);
	  clkprofile.click();
	  Log.message("myprofile has been clicked.");
	  SkySiteUtils.waitTill(3000);
		
	  SkySiteUtils.waitForElement(driver, pfloutput, 30);
	  pfloutput.click();
	  Log.message("log out button has been clicked.");
	  SkySiteUtils.waitTill(5000);
		
	  SkySiteUtils.waitForElement(driver, btnyes, 30);
	  btnyes.click();
	  Log.message("yes button has been clicked.");
	  SkySiteUtils.waitTill(7000);
		
	  String Exptitle = "Sign in - SKYSITE";// Expected Page title
	  String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
	  SkySiteUtils.waitTill(5000);
	  Log.message("Logout Validation:");
	  SkySiteUtils.waitTill(5000);
	
	  if (Acttitle_AfterLogin.equals(Exptitle))
	  {	
		  return true;		
	
	  }		
	  else	
	  {		
		  return false;
			
	  }
  }

  
  
  /**
   * Method written for adding no permission to a Team
   * By: Ranadeep Ghosh
   * @return
   */
  public void AddTeamNoPermission(String TeamName) 
  {
	  SkySiteUtils.waitTill(5000);
	  SkySiteUtils.waitForElement(driver, btnManagePermission, 30);
	  btnManagePermission.click();
	  Log.message("Clicked on manage permission button");
	  SkySiteUtils.waitTill(3000);
	 
      Log.message("Switching frame");
      SkySiteUtils.waitForElement(driver, driver.findElement(By.id("ifrmPermission")), 30);
      driver.switchTo().frame(driver.findElement(By.id("ifrmPermission")));
      SkySiteUtils.waitTill(5000);
      
      int count = driver.findElements(By.xpath(".//*[@id='divSelTeamOrMemberGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
      
      for(int i=1; i<=count; i++)
      {
    	  
    	  if(driver.findElement(By.xpath(".//*[@id='divSelTeamOrMemberGrid']/div[2]/table/tbody/tr["+i+"+"+1+"]/td[3]")).getText().contains(TeamName))
    	  {
    		  
    		  driver.findElement(By.xpath(".//*[@id='divSelTeamOrMemberGrid']/div[2]/table/tbody/tr["+i+"+"+1+"]/td[1]/img")).click();
    		  Log.message("Clicked on desired group/user");
    		  SkySiteUtils.waitTill(3000);   		  
    	  }
      }
      		  
	  driver.findElement(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr[2]/td[2]/img")).click();
	  SkySiteUtils.waitTill(3000);
	  driver.findElement(By.xpath(".//*[@id='divPermissionTable']/div[2]/table/tbody/tr[2]/td[2]/img")).click();
	  SkySiteUtils.waitTill(3000);
	  Log.message("No permision set for the selected group");
    
	  Log.message("Switching to default content");
	  driver.switchTo().defaultContent();
	  SkySiteUtils.waitTill(5000);
	  Log.message("Switing frame");
      driver.switchTo().frame(driver.findElement(By.id("myFrame")));
      SkySiteUtils.waitTill(5000);
	  
	  SkySiteUtils.waitForElement(driver, btnSavePermissionPopover, 30);
	  btnSavePermissionPopover.click();
	  Log.message("Clicked on Save Permission popover button");
	  SkySiteUtils.waitTill(5000);
	  
	  SkySiteUtils.waitForElement(driver, btnClosePermissionPopover, 30);
	  btnClosePermissionPopover.click();
	  Log.message("Clicked on Close Permission popover button");
	  SkySiteUtils.waitTill(5000);
		
  }
  
}
