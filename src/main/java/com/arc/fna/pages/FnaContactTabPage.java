package com.arc.fna.pages;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;




public class FnaContactTabPage extends LoadableComponent<FnaContactTabPage> {
    WebDriver driver;
    private  boolean isPageLoaded;
  
    /** Find Elements using FindBy annotation
     * 
     */
    @FindBy(css="#btnCreateGroup")
    WebElement CreateGrpbtn;
    @FindBy(xpath=".//*[@id='btnNewContact']")
    WebElement NewContactBtn;
    @FindBy(css="#ctl00_DefaultContent_rdoHostedUserType")
    WebElement Employeebtn;
    @FindBy(css="#ctl00_DefaultContent_txtFirstName")
    WebElement Firstname;
    @FindBy(css="#ctl00_DefaultContent_txtLastName")
    WebElement Lastname;
    @FindBy(css="#ctl00_DefaultContent_txtCity")
    WebElement city;
    @FindBy(css="#ctl00_DefaultContent_txtZip")
    WebElement Postalcode;
    @FindBy(css="#ctl00_DefaultContent_txtTitle")
    WebElement Title;
    @FindBy(css="#ctl00_DefaultContent_ucCountryState_ddlCountry")
    WebElement Country;
    @FindBy(css="#ctl00_DefaultContent_txtCompanyName")
    WebElement Company;
    @FindBy(css="#ctl00_DefaultContent_ucCountryState_ddlState")
    WebElement statedropdown;
    @FindBy(css="#ctl00_DefaultContent_txtAddress1")
    WebElement Address1;
    @FindBy(css="#ctl00_DefaultContent_txtEmail")
    WebElement Email;
    @FindBy(css="#ctl00_DefaultContent_txtAddress2")
    WebElement Address2;
    @FindBy(css="#ctl00_DefaultContent_txtPhoneWork")
    WebElement PhoneNo;
    @FindBy(css="#ctl00_DefaultContent_txtWebURL")
    WebElement Companywebsite;
    @FindBy(css="#ctl00_DefaultContent_ddlProjectRole")
    WebElement ProjectRole;
    @FindBy(css="#ctl00_DefaultContent_DDLCompBussiness")
    WebElement Buisness;
    @FindBy(css="#ctl00_DefaultContent_DDLCompOccupation")
    WebElement Occupation;
    @FindBy(css="#ctl00_DefaultContent_Button3")
	WebElement Savebutton;
    @FindBy(css="#btnExpImpAddBook")
    WebElement Exporticon;
    @FindBy(xpath=".//*[@id='tdbtnExpImpAddBook']/ul/li[2]/a")
    WebElement ExportExceloption;
    @FindBy(css="#txtSerachValue")
    WebElement Searchboxnew;
    
    
    
    @FindBy(css="#btnSearch")
    WebElement Searchbtn;
  
    @FindBy(css=".ev_material > td:nth-child(2) > a:nth-child(2)")
    WebElement Grpnamelist;
    
    @FindBy(css="#chkAllPT")
    @CacheLookup
    WebElement SelctAlldel;
    
    @FindBy(css="#btnRemoveContact")
    @CacheLookup
    WebElement Deletebtn;
    
    
    @FindBy(css="#ctl00_DefaultContent_rdoContactType")
    @CacheLookup
    WebElement Cntctselbtn;
    
    
    @FindBy(css="#divFinalAlertForDeletion > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(1)")
    @CacheLookup
    WebElement delconfirmbtn;
    
    @FindBy(css="#opt1")
    @CacheLookup
    WebElement delradiobtn;
    
    
    @FindBy(css="#divFinalAlertForDeletion > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)")
    @CacheLookup
    WebElement cancelbtn;

    @Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;

		driver.switchTo().frame("myFrame");
		SkySiteUtils.waitForElement(driver,CreateGrpbtn,30);
		driver.switchTo().defaultContent();
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FnaContactTabPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
    /** Method for Address Page landing verification
     *  Scripted By :Tarak
     */
	public boolean AddressPage_IsLanded() {
		driver.switchTo().frame("myFrame");
		Log.message("Swtiched into Frame");
		if(NewContactBtn.isDisplayed()==true) {
			Log.message("AddNewContact Button is displayed");
			return true;
		}
			return false;
		
	}
	/** Method for Random Contactname
	 * Scripted By: Tarak
	 * @return
	 */
	 public String Random_Contactname()
	    {
	           
	           String str="Contact";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	 /** Method for Random Email
	  * Scripted By : Tarak
	  * @return
	  */
	 public String Random_Email()
	    {
	           
	           String str1="derasri";
	           String str2="@gmail.com";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str1+String.valueOf((10000 + r.nextInt(20000)))+str2;
	           return abc;
	           
	    }
	 /** Method for Random Company name
	  * Scripted By : Tarak
	  * @return
	  */
	 public String Random_CompanyName()
	    {
	           
	           String str1="Companyname";

	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str1+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	 /** Method for Random update Email
	  * Scripted By : Tarak
	  * @return
	  */
	 public String Random_updateEmail()
	    {
	           
	           String str1="Test1";
	           String str2="@gmail.com";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str1+String.valueOf((10000 + r.nextInt(20000)))+str2;
	           return abc;
	           
	    }
	 public String Random_LastName() {
		 String str1="Lastname";
		 Random r = new Random( System.currentTimeMillis());
		 String abc=str1+String.valueOf((10000 + r.nextInt(20000)));
		 return abc;
		}
	 /** Method to deletefile for folder
	  * 
	  * @param Folder_Path
	  * @return
	  */
	//Deleting files from a folder
		public boolean Delete_Files_From_Folder(String Folder_Path) 
		{
			try
			{	
				SkySiteUtils.waitTill(5000);		
				Log.message("Cleaning download folder!!! ");
				File file = new File(Folder_Path);      
			    String[] myFiles;    
			    if(file.isDirectory())
			    {
			    	myFiles = file.list();
			        for(int i=0; i<myFiles.length; i++) 
			        {
			           File myFile = new File(file, myFiles[i]); 
			           myFile.delete();
			           SkySiteUtils.waitTill(5000);
			        }
			        Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			    }	         
			}//end try
			catch(Exception e)
			{
				 Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
			return false;
		}
	@FindBy(xpath="//input[@value='rdoHostedUserType']")
	WebElement employeeuserbtn;
	
	/** Method to Create Newcontact
	 * Scripted By : Tarak
	 * 
	 */
	public boolean CreateNewContact(String Newcontactname,String Emailid,String Lastnamerandom)
	{
		driver.switchTo().defaultContent();//switch to default window after adress page landed
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame("myFrame");//Again switch to frame to click the addnewcontact button
		SkySiteUtils.waitTill(6000);
		NewContactBtn.click();
		Log.message("AddNewContact button is clicked");
		SkySiteUtils.waitTill(9000);
		String MainWindow=driver.getWindowHandle();
		Log.message(MainWindow);
		for(String childwindow:driver.getWindowHandles())
		{
			driver.switchTo().window(childwindow);//swtich to child window
		
		}
		SkySiteUtils.waitForElement(driver,employeeuserbtn, 50);
		employeeuserbtn.click();
		Log.message("Employee type user is selected");
		SkySiteUtils.waitTill(5000);
		Firstname.click();
		Firstname.sendKeys(Newcontactname);
		Log.message("Contactname entered is : " +Newcontactname);
		SkySiteUtils.waitTill(5000);
		Lastname.sendKeys(Lastnamerandom);
		Log.message("Lastname entered is " +Lastnamerandom);
		SkySiteUtils.waitTill(5000);
		String CityName=PropertyReader.getProperty("City");
		city.sendKeys(CityName);
		Log.message("City entered is " +CityName);
		SkySiteUtils.waitTill(5000);
		String Pcode=PropertyReader.getProperty("Postcode");
		Postalcode.sendKeys(Pcode);
		Log.message("postal code entered is " +Pcode);
		SkySiteUtils.waitTill(5000);
		String TName=PropertyReader.getProperty("Titlename");
		Title.sendKeys(TName);
		Log.message("Title entered is " +TName);
		SkySiteUtils.waitTill(5000);
		String Cname=PropertyReader.getProperty("Countryname");
		Select sc=new Select(Country);
		sc.selectByVisibleText(Cname);
		Log.message("Country selected is :" +Cname);
		SkySiteUtils.waitTill(5000);
		String Companynm=PropertyReader.getProperty("Company");
		Company.sendKeys(Companynm);
		Log.message("Companyname entered is " +Companynm);
		SkySiteUtils.waitTill(5000);
		String statenm=PropertyReader.getProperty("Statenm");
		Select selectstate=new Select(statedropdown);
	    selectstate.selectByVisibleText(statenm);
	    Log.message("State selected is " +statenm);
		SkySiteUtils.waitTill(5000);
	    String addressfirst=PropertyReader.getProperty("Address1");
	    Address1.sendKeys(addressfirst);
	    Log.message("Address1 entered is" +addressfirst);
		SkySiteUtils.waitTill(5000);
        Email.sendKeys(Emailid);
        Log.message("Email id entered is" +Emailid);
    	SkySiteUtils.waitTill(5000);
        String addresssecond=PropertyReader.getProperty("Address2");
        Address2.sendKeys(addresssecond);
        Log.message("Address2 entered is " +addresssecond);
    	SkySiteUtils.waitTill(5000);
        String Phnno=PropertyReader.getProperty("Phoneno");
        PhoneNo.sendKeys(Phnno);
        Log.message("Phone no entered is : " +Phnno);
    	SkySiteUtils.waitTill(5000);
        String Compwebsite=PropertyReader.getProperty("CompanyWebSite");
        Companywebsite.sendKeys(Compwebsite);
        Log.message("Company website entered : " +Compwebsite);
    	SkySiteUtils.waitTill(5000);
    	JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        Log.message("Scrolled down to window");
        SkySiteUtils.waitTill(5000);
        String Projectroll=PropertyReader.getProperty("Projectrole");
        Select scrole=new Select(ProjectRole);
        scrole.selectByVisibleText(Projectroll);
    	SkySiteUtils.waitTill(5000);
        Log.message("ProjectRoll selected is " +Projectroll);
        String Buisnessname=PropertyReader.getProperty("Buisnessdetails");
        Select selectBuisness=new Select(Buisness);
        selectBuisness.selectByVisibleText(Buisnessname);
        Log.message("Buisness selected is " +Buisnessname);
        SkySiteUtils.waitTill(5000);
        String Occupationname=PropertyReader.getProperty("Occupationdetails");
        Select selectoccupation=new Select(Occupation);
        selectoccupation.selectByVisibleText(Occupationname);
        Log.message("Occupation selected is " +Occupationname);
        SkySiteUtils.waitTill(5000);
        Savebutton.click();
        Log.message("Save button is clicked");
        SkySiteUtils.waitTill(5000);
        driver.switchTo().window(MainWindow);//switch to main window
        Log.message("Switched to main window");
        SkySiteUtils.waitTill(6000);
        driver.switchTo().frame(driver.findElement(By.id("myFrame")));
        Log.message("Switched to frame");
        SkySiteUtils.waitTill(6000);
        
        SkySiteUtils.waitForElement(driver, SearchContact, 30);
        SearchContact.clear();
        SearchContact.sendKeys(Newcontactname);
        Log.message("Contact name entered for search is:- "+Newcontactname);
        SkySiteUtils.waitTill(3000);
        
        driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
        Log.message("Clicked on search contact button");
        SkySiteUtils.waitTill(6000);
        
        String searchedContact = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
        Log.message("Searched contact name is"+searchedContact);
        
        if(searchedContact.contains(Newcontactname))     
        {
       	 	return true;
        }             
        else
        {
       	 	return false;
        }
	}
	@FindBy(xpath=".//*[@id='ctl00_DefaultContent_rdoSponsoredUserType']")
	WebElement shreduserbtn;
	/** Method to Create New Shareduser
	 * Scripted By : Tarak
	 * 
	 */
	public boolean CreateNewSharedUser(String Newcontactname,String Emailid,String Lastnamerandom,String companyname)
	{
		driver.switchTo().defaultContent();//switch to default window after adress page landed
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame("myFrame");//Again switch to frame to click the addnewcontact button
		SkySiteUtils.waitTill(6000);
		NewContactBtn.click();
		Log.message("AddNewContact button is clicked");
		SkySiteUtils.waitTill(9000);
		String MainWindow=driver.getWindowHandle();
		Log.message(MainWindow);
		for(String childwindow:driver.getWindowHandles())
		{
			driver.switchTo().window(childwindow);//swtich to child window
		
		}
		SkySiteUtils.waitForElement(driver,employeeuserbtn, 50);
		shreduserbtn.click();
		Log.message("Employee type user is selected");
		SkySiteUtils.waitTill(5000);
		Firstname.click();
		Firstname.sendKeys(Newcontactname);
		Log.message("Contactname entered is : " +Newcontactname);
		SkySiteUtils.waitTill(5000);
		Lastname.sendKeys(Lastnamerandom);
		Log.message("Lastname entered is " +Lastnamerandom);
		SkySiteUtils.waitTill(5000);
		String CityName=PropertyReader.getProperty("City");
		city.sendKeys(CityName);
		Log.message("City entered is " +CityName);
		SkySiteUtils.waitTill(5000);
		String Pcode=PropertyReader.getProperty("Postcode");
		Postalcode.sendKeys(Pcode);
		Log.message("postal code entered is " +Pcode);
		SkySiteUtils.waitTill(5000);
		String TName=PropertyReader.getProperty("Titlename");
		Title.sendKeys(TName);
		Log.message("Title entered is " +TName);
		SkySiteUtils.waitTill(5000);
		String Cname=PropertyReader.getProperty("Countryname");
		Select sc=new Select(Country);
		sc.selectByVisibleText(Cname);
		Log.message("Country selected is :" +Cname);
		SkySiteUtils.waitTill(5000);
		//String Companynm=PropertyReader.getProperty("Company");
		Company.sendKeys(companyname);
		Log.message("Companyname entered is " +companyname);
		SkySiteUtils.waitTill(5000);
		String statenm=PropertyReader.getProperty("Statenm");
		Select selectstate=new Select(statedropdown);
	    selectstate.selectByVisibleText(statenm);
	    Log.message("State selected is " +statenm);
		SkySiteUtils.waitTill(5000);
	    String addressfirst=PropertyReader.getProperty("Address1");
	    Address1.sendKeys(addressfirst);
	    Log.message("Address1 entered is" +addressfirst);
		SkySiteUtils.waitTill(5000);
        Email.sendKeys(Emailid);
        Log.message("Email id entered is" +Emailid);
    	SkySiteUtils.waitTill(5000);
        String addresssecond=PropertyReader.getProperty("Address2");
        Address2.sendKeys(addresssecond);
        Log.message("Address2 entered is " +addresssecond);
    	SkySiteUtils.waitTill(5000);
        String Phnno=PropertyReader.getProperty("Phoneno");
        PhoneNo.sendKeys(Phnno);
        Log.message("Phone no entered is : " +Phnno);
    	SkySiteUtils.waitTill(5000);
        String Compwebsite=PropertyReader.getProperty("CompanyWebSite");
        Companywebsite.sendKeys(Compwebsite);
        Log.message("Company website entered : " +Compwebsite);
    	SkySiteUtils.waitTill(5000);
    	JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        Log.message("Scrolled down to window");
        SkySiteUtils.waitTill(5000);
        String Projectroll=PropertyReader.getProperty("Projectrole");
        Select scrole=new Select(ProjectRole);
        scrole.selectByVisibleText(Projectroll);
    	SkySiteUtils.waitTill(5000);
        Log.message("ProjectRoll selected is " +Projectroll);
        String Buisnessname=PropertyReader.getProperty("Buisnessdetails");
        Select selectBuisness=new Select(Buisness);
        selectBuisness.selectByVisibleText(Buisnessname);
        Log.message("Buisness selected is " +Buisnessname);
        SkySiteUtils.waitTill(5000);
        String Occupationname=PropertyReader.getProperty("Occupationdetails");
        Select selectoccupation=new Select(Occupation);
        selectoccupation.selectByVisibleText(Occupationname);
        Log.message("Occupation selected is " +Occupationname);
        SkySiteUtils.waitTill(5000);
        Savebutton.click();
        Log.message("Save button is clicked");
        SkySiteUtils.waitTill(5000);
        driver.switchTo().window(MainWindow);//switch to main window
        Log.message("Switched to main window");
        SkySiteUtils.waitTill(6000);
        driver.switchTo().frame(driver.findElement(By.id("myFrame")));
        Log.message("Switched to frame");
        SkySiteUtils.waitTill(6000);
        
        SkySiteUtils.waitForElement(driver, SearchContact, 30);
        SearchContact.clear();
        SearchContact.sendKeys(Newcontactname);
        Log.message("Contact name entered for search is:- "+Newcontactname);
        SkySiteUtils.waitTill(3000);
        
        driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
        Log.message("Clicked on search contact button");
        SkySiteUtils.waitTill(6000);
        
        String searchedContact = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
        Log.message("Searched contact name is"+searchedContact);
        
        if(searchedContact.contains(Newcontactname))     
        {
       	 return true;
        }             
        else
        {
       	 return false;
        }
             
        /*int Count_list=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("Total list count is:"+Count_list);	
		//Loop start '2' as projects list start from tr[2]
		int i=0;
		for(i = 1; i <= Count_list; i++) 
		{
				
			String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			//Log.message("Contact name is:"+contactName);
			SkySiteUtils.waitTill(5000);			
			//Checking contact name equal with each row of table
			if(contactName.contains(Newcontactname))
			{	   
				Log.message("Contact name matched is : " +contactName);
				Log.message("Contact name added has been validated!!!");		                  
				break;
			}		
		}
		String contactName1=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
		Log.message("****New Contact added is**** " +contactName1);
		if(contactName1.contains(Newcontactname))	
			return true;
		else
			return false;*/
		}
	   /** Method for Export Address book
	    * Scripted By: Tarak
	 * @throws IOException 
	    * 
	    */
	
	    public boolean exportAddressBook(String downloadpath) throws IOException {
           driver.switchTo().defaultContent();
           SkySiteUtils.waitTill(5000);
           this.Delete_Files_From_Folder(downloadpath);
           Log.message("downloadpath is : " +downloadpath);
           SkySiteUtils.waitTill(5000);
           driver.switchTo().frame(driver.findElement(By.id("myFrame")));
           Log.message("swtiched to frame!!");
           SkySiteUtils.waitTill(3000);
           Exporticon.click();
           Log.message("Export icon is clicked");
           SkySiteUtils.waitForElement(driver, ExportExceloption, 80);
           int Count_list=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	 	   Log.message("Total number of contact are:"+Count_list);	
	 	   SkySiteUtils.waitTill(4000);
           ExportExceloption.click();
           Log.message("Export Excel option clicked");
           SkySiteUtils.waitTill(4000);
           driver.switchTo().defaultContent();
           Log.message("Switch to default content");
           SkySiteUtils.waitTill(5000);
           if ((this.isFileDownloaded(downloadpath)==true)&&(this.readEntry(downloadpath,Count_list)==true))
        	   return true;
           else 
        	   return false;
         }
	    /**Method to check if file is downloaded
	     * Scripted By : Tarak
	     * 
	     */
	     public boolean isFileDownloaded(String downloadfolderpath) {
	    	 Log.message("Searching contact file downloaded");
	    	 File file=new File(downloadfolderpath);
	    	 File[] Filearray=file.listFiles();
	    	 if(file.isDirectory()) {
	    		 for(File Myfile:Filearray) {
	    			 if(Myfile.getName().contains(PropertyReader.getProperty("Exportfilename"))) {
	    				return true;
	    			 }
	    		 }
	    	 }
	    	return false;
	   }
	     /**Method to read entry of downloaded excel
	     * @throws IOException 
	      * Scripted By : Tarak
	      */
	     public boolean readEntry(String downloadpath,int count) throws IOException {
	    	 int rowcount;
	    	  
	    		 SkySiteUtils.waitTill(4000);
	    		 FileInputStream fis=new FileInputStream(downloadpath + "\\Contacts.xlsx");
	    		  XSSFWorkbook workbook = new XSSFWorkbook(fis);
	              XSSFSheet sheet = workbook.getSheetAt(0);
	             rowcount=sheet.getLastRowNum()-sheet.getFirstRowNum();
	             Log.message("the row count in the excel is " +rowcount);
	             SkySiteUtils.waitTill(5000);
	             if(rowcount==count) {
	 	 		Log.message("Number of entry in the excel matched with the number of contacts");
	 	 	      return true;
	 	 		}
	 	 	   else {
	 	 		   return false;
	 	 				   
	 	 	   }
	    	   	
	     }
	    	 
	    	 
	    	 
	
	     
	     @FindBy(css="#txtSerachValue")
	     WebElement searchbox;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/button")
	     WebElement dropdownbtn;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[1]/a/span[1]")
	     WebElement ContactGroupnamesearchoptiopn;
	     @FindBy(xpath=".//*[@id='btnSearch']")
	     WebElement Searchbuttonicon;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[2]/a/span[1]")
	     WebElement ContactLastNamesearchoption;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[4]/a")
	     WebElement Emailoption;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[3]/a")
	     WebElement Companyoption;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[5]/a")
	     WebElement Phoneoption;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[6]/a")
	     WebElement PRole;
	     @FindBy(xpath=".//*[@id='divProjectRole']/div/button")
	     @CacheLookup
	     WebElement Roledropdownbutton;
	     @FindBy(xpath=".//*[@id='divProjectRole']/div/div/ul/li[4]/a")
	     @CacheLookup
	     WebElement Consultantoption;
	     @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[7]/a")
	     @CacheLookup
	     WebElement Userlicensetypeoption;
	     @FindBy(xpath=".//*[@id='divUserLicenseType']/div/button")
	     @CacheLookup
	     WebElement Usertypedropdownbutton;
	     @FindBy(xpath=".//*[@id='divUserLicenseType']/div/div/ul/li[1]/a")
	     @CacheLookup
	     WebElement EmployeeTypeoption;
	   /** Method to Search a added contact with contactname
	    * 
	    */
	  public boolean searchForContact(String contacttobeseached) 
	  {
		  driver.switchTo().defaultContent();
		  Log.message("Switched to default content");
		  SkySiteUtils.waitTill(4000);
		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		  Log.message("Switching into frame ");
		  SkySiteUtils.waitTill(4000);
		  
		  SkySiteUtils.waitForElement(driver, btnResetContactSearch, 30);
          btnResetContactSearch.click();
          Log.message("Clicked on Reset button to clear previous search");
          SkySiteUtils.waitTill(5000);
		  
          SkySiteUtils.waitForElement(driver, searchbox, 30);
          searchbox.clear();
		  searchbox.sendKeys(contacttobeseached);
		  Log.message("Contact name entered in the search box is " +contacttobeseached);
		  SkySiteUtils.waitTill(5000);
		  
		  SkySiteUtils.waitForElement(driver, dropdownbtn, 30);
		  dropdownbtn.click();
		  SkySiteUtils.waitTill(5000);
		  Log.message("dropdown button is clicked");
		  
		  SkySiteUtils.waitForElement(driver, ContactGroupnamesearchoptiopn, 30);
		  ContactGroupnamesearchoptiopn.click();
		  Log.message("Contact/Group name search option is clicked");
		  SkySiteUtils.waitTill(5000);
		  
		  	driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
	        Log.message("Clicked on search contact button");
	        SkySiteUtils.waitTill(6000);
	        
	        String searchedContact = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
	        Log.message("Searched contact name is"+searchedContact);
	        
	        if(searchedContact.contains(contacttobeseached))     
	        {
	       	 	return true;
	        }             
	        else
	        {
	       	 	return false;
	        }
		  
		  
		  
		 /* int resultantrow=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		  Log.message("Number of  row item found is:" +resultantrow);
		  SkySiteUtils.waitTill(4000);
		  int i=0;
			for(i = 1; i <= resultantrow; i++) 
			{
					
				String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				// Log.message("Contact name is:"+contactName);
				SkySiteUtils.waitTill(5000);			
				//Checking contact name equal with each row of table
				if(contactName.contains(contacttobeseached))
				{	   
				
					Log.message("Contactname Search  has been validated!!!");
				    break;
				}	
			
			}
			String Contactfoundinlist=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			Log.message("Contact  found in list is  " +Contactfoundinlist);
			if(Contactfoundinlist.contains(contacttobeseached))	
				return true;
			else
				return false;*/
	  }
	  /** Method for Search with Lastname
	   * Scripted By:Tarak
	   */
	  public boolean searchForLastName(String LastNameToBeSearched) {
		  driver.switchTo().defaultContent();
		  Log.message("Switched to default content");
		  SkySiteUtils.waitTill(4000);
		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		  Log.message("Switching into frame ");
		  SkySiteUtils.waitTill(4000);
		  searchbox.clear();
		  Log.message("search box cleared");
		  searchbox.sendKeys(LastNameToBeSearched);
		  Log.message("Lastname entered in the search box is " +LastNameToBeSearched);
		  SkySiteUtils.waitTill(5000);
		  dropdownbtn.click();
		  SkySiteUtils.waitTill(5000);
		  Log.message("dropdown button is clicked");
		  ContactLastNamesearchoption.click();;
		  Log.message("Contact Last name option has been selected");
		  SkySiteUtils.waitTill(5000);
		  Searchbuttonicon.click();
		  Log.message("Searchbutton icon is clicked");
		  SkySiteUtils.waitTill(4000);
		  int resultantrow=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		  Log.message("Number of  row item found is:" +resultantrow);
		  int i=0;
			for(i = 1; i <= resultantrow; i++) 
			{
					
				String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				//Log.message("Contact name is:"+contactName);
				SkySiteUtils.waitTill(5000);			
				//Checking contact name equal with each row of table
				if(contactName.contains(LastNameToBeSearched))
				{	   
				
					Log.message("Contact Last name search has been validated!!!");
				    break;
				}	
			
			}
			String Contactwithlastname=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			Log.message("Contact found in list is " +Contactwithlastname);
			if(Contactwithlastname.contains(LastNameToBeSearched))	
				return true;
			else
				return false;
		}
	  /** Method for Search with Email
	     *    Scripted By :Tarak
	     */
	     public boolean searchForEmail(String Emailidtobesearched) {
	      driver.switchTo().defaultContent();
		  Log.message("Switched to default content");
		  SkySiteUtils.waitTill(4000);
		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		  Log.message("Switching into frame ");
		  SkySiteUtils.waitTill(4000);
		  searchbox.clear();
		  Log.message("search box cleared");
		  searchbox.sendKeys(Emailidtobesearched);
		  Log.message("Email entered in the search box is " +Emailidtobesearched);
		  SkySiteUtils.waitTill(5000);
		  dropdownbtn.click();
		  SkySiteUtils.waitTill(5000);
		  Log.message("dropdown button is clicked");
		  Emailoption.click();;
		  Log.message("Email option has been selected");
		  SkySiteUtils.waitTill(5000);
		  Searchbuttonicon.click();
		  Log.message("Searchbutton icon is clicked");
		  SkySiteUtils.waitTill(4000);
		  String resultemail=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[4]")).getText();
		  Log.message("Email found in the list is " +resultemail);
		  if(resultemail.contains(Emailidtobesearched))
             return true;
             else
          	   return false;
	     }
	     /**Method for Search with Phone Number
	        * Scripted by:Tarak
	        */
	            public boolean searchWithPhone() {
	        	   	  driver.switchTo().defaultContent();
	        		  Log.message("Switched to default content");
	        		  SkySiteUtils.waitTill(4000);
	        		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	        		  Log.message("Switching into frame ");
	        		  SkySiteUtils.waitTill(4000);
	        		  String Phonenotobesearched=PropertyReader.getProperty("Phoneno");
	        		  searchbox.clear();
	           		  Log.message("search box cleared");
	           		  searchbox.sendKeys(Phonenotobesearched);
	           		  Log.message("Phone no to be searched is " +Phonenotobesearched);
	           		  SkySiteUtils.waitTill(4000);
	          		  dropdownbtn.click();
	          		  SkySiteUtils.waitTill(5000);
	          		  Log.message("dropdown button is clicked");
	        	      Phoneoption.click();
	        	      Log.message("Phone option is selected");
	        	      SkySiteUtils.waitTill(5000);
	          		  Searchbuttonicon.click();
	          		  Log.message("Searchbutton icon is clicked");
	          		  SkySiteUtils.waitTill(4000);
	          		  int resultantrow=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	          		  Log.message("Resultant no of row is " +resultantrow);
	          		  String phonenointhelist=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[5]")).getText();
	          		  Log.message("Phone no found in the list " +phonenointhelist);
	          		  if(phonenointhelist.contains(Phonenotobesearched))
	          			  return true;
	          		  else
	          			  return false;
	          }
	            /**Method for Search with Role
	             * 
	             */
	           public boolean searchWithRole(String contactnameadded) {
	        	   driver.switchTo().defaultContent();
	        	   Log.message("Switched to default content");
	     		  SkySiteUtils.waitTill(4000);
	     		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	     		  Log.message("Switching into frame ");
	     		  SkySiteUtils.waitTill(4000);
	     		  dropdownbtn.click();
	      		  SkySiteUtils.waitTill(5000);
	      		  Log.message("dropdown button is clicked");
	      		  PRole.click();
	      		  Log.message("Project role option is selected ");
	      		  SkySiteUtils.waitTill(5000);
	      		  Roledropdownbutton.click();
	      		  Log.message("Role dropdown button is clicked");
	      		  SkySiteUtils.waitTill(5000);
	      		  Consultantoption.click();
	      		  SkySiteUtils.waitTill(5000);
	      		  Log.message("Consultant option has been selected");
	      		  Searchbuttonicon.click();
	    		  Log.message("Searchbutton icon is clicked");
	    		  SkySiteUtils.waitTill(4000);
	    		  String Roleentered=PropertyReader.getProperty("Projectrole");
	    		  int resultantrow=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	    		  Log.message("Resultant no of row is " +resultantrow);
	    		  int i=0;
	    			for(i = 1; i <= resultantrow; i++) 
	    			{
	    					
	    				String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	    				//Log.message("Contact name is:"+contactName);
	    				SkySiteUtils.waitTill(5000);			
	    				//Checking contact name equal with each row of table
	    				if(contactName.contains(contactnameadded))
	    				{	   
	    				
	    					Log.message("Contactname : " + contactName + " with project role : " +Roleentered+ " Is found in the list ");
	    				    break;
	    				}	
	    			
	    			}
	    			String Contactnamematched=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	    			Log.message("Contact found in list is " +Contactnamematched);
	    			if(Contactnamematched.contains(contactnameadded))	
	    				return true;
	    			else
	    				return false;
	       }
	           
	     	  
	           /** Method for Search with Company name
	            * Scripted By :Tarak
	            */
	          public boolean searchWithCompanyName(String companyname) {
	       	   driver.switchTo().defaultContent();
	      		  Log.message("Switched to default content");
	      		  SkySiteUtils.waitTill(4000);
	      		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	      		  Log.message("Switching into frame ");
	      		  SkySiteUtils.waitTill(4000);
	      		 
	      		  searchbox.clear();
	      		  Log.message("search box cleared");
	      		  searchbox.sendKeys(companyname);
	      		  Log.message("Companyname entered in the search box is :" +companyname );
	      		  SkySiteUtils.waitTill(4000);
	      		  dropdownbtn.click();
	   		  SkySiteUtils.waitTill(5000);
	   		  Log.message("dropdown button is clicked");
	       	  Companyoption.click();
	       	  Log.message("Companyname option has been selected in the dropdown");
	       	  SkySiteUtils.waitTill(5000);
	     		  Searchbuttonicon.click();
	     		  Log.message("Searchbutton icon is clicked");
	     		  SkySiteUtils.waitTill(4000);
	     		  String Companynameinlist=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[3]")).getText();
	     		  Log.message("Company name observed in the fisrt row of company column is " +Companynameinlist);
	     		  if(Companynameinlist.contains(companyname))
	     			  return true;
	     		  else
	     			return  false;
	     	}
	          /** Method to Search with User License Type
	           * 
	            */
	             public boolean searchUserWithLicenseType(String Employeecontactname) {
	           	   driver.switchTo().defaultContent();
	           	   Log.message("Switched to default content");
	        		  SkySiteUtils.waitTill(4000);
	        		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	        		  Log.message("Switching into frame ");
	        		  SkySiteUtils.waitTill(4000);
	        		  Log.message("Search Employee type user added");
	        		  dropdownbtn.click();
	        		  SkySiteUtils.waitTill(5000);
	        		  Log.message("dropdown button is clicked");
	        		  Userlicensetypeoption.click();
	        		  Log.message("User License Type user is selected");
	        		  SkySiteUtils.waitTill(5000);
	        		  Usertypedropdownbutton.click();
	        		  Log.message("Role dropdown button is clicked");
	        		  SkySiteUtils.waitTill(5000);
	        		  EmployeeTypeoption.click();
	        		  Log.message("Employee type user is selected");
	        		  SkySiteUtils.waitTill(5000);
	        		  Searchbuttonicon.click();
	      		      Log.message("Searchbutton icon is clicked");
	      		      SkySiteUtils.waitTill(4000);
	      		      int resultantrow=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	      		      Log.message("Resultant no of row is " +resultantrow);
	      		   int i=0;
	   			for(i = 1; i <= resultantrow; i++) 
	   			{
	   					
	   				String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	   				//Log.message("Contact name is:"+contactName);
	   				SkySiteUtils.waitTill(5000);			
	   				//Checking contact name equal with each row of table
	   				if(contactName.contains(Employeecontactname))
	   				{	   
	   				
	   					Log.message("Employee contact name added is found in the list on searching!!!! ");
	   				    break;
	   				}	
	   			
	   			}
	   			String Contactnamematched=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	   			Log.message("Contact found in list is " +Contactnamematched);
	   			if(Contactnamematched.contains(Employeecontactname))	
	   				return true;
	   			else
	   				return false;
	           	  
	          }
	             
	             @FindBy(css=".icon.icon-edit.icon-orange")
	             @CacheLookup
	             WebElement editicon;
	         /** Method for edit functionality option
	          * SCripted By : Tarak
	          */
	           public boolean editFunctionalityoption(String Emailupdate, String ContactName) 
	           {
	        	    driver.switchTo().defaultContent();
	           		Log.message("Switched to default content");
	           		SkySiteUtils.waitTill(4000);
	      		  	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	      		  	Log.message("Switching into frame ");
	      		  	SkySiteUtils.waitTill(4000);
	      		  	
	      		    SkySiteUtils.waitForElement(driver, SearchContact, 30);
	                SearchContact.clear();
	                SearchContact.sendKeys(ContactName);
	                Log.message("Contact name entered for search is:- "+ContactName);
	                SkySiteUtils.waitTill(3000);
	              
	                driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
	                Log.message("Clicked on search contact button");
	                SkySiteUtils.waitTill(6000);
	              
	                String searchedContact = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
	                Log.message("Searched contact name is"+searchedContact);
	      		  	
	                if(searchedContact.contains(ContactName))
	                {
	                	Log.message("Searched contact is displayed");
	                	
	                	driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr/td[7]/a")).click();
	                	Log.message("CLicked on edit icon for the searched contact");
	                	
	                	SkySiteUtils.waitTill(20000);
	    	           	String MainWindow=driver.getWindowHandle();
	    	       		Log.message(MainWindow);
	    	       		for(String childwindow:driver.getWindowHandles())
	    	       		{
	    	       			driver.switchTo().window(childwindow);//swtich to child window
	    	       		
	    	       		}
	                	
	    	       		SkySiteUtils.waitTill(6000);
	    	       		SkySiteUtils.waitForElement(driver, Email, 40);
	    	       		Email.click();
	    	       		Email.clear();
	    	       		Email.sendKeys(Emailupdate);
	    	       		Log.message("Email entered to be updated " +Emailupdate);
	    	       		SkySiteUtils.waitTill(5000);
	    	       		JavascriptExecutor js = (JavascriptExecutor) driver;
    	                js.executeScript("window.scrollBy(0,1000)");
	    	            Log.message("Scrolled down to window");
	    	            SkySiteUtils.waitTill(5000);
	    	            Savebutton.click();
	    	            Log.message("savebutton is clicked");
	    	            SkySiteUtils.waitTill(4000);
	    	            driver.switchTo().window(MainWindow);//switch to main window
	    	            Log.message("Switched to main window");
	    	            SkySiteUtils.waitTill(4000);
	    	            driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	    	            Log.message("Switched to frame");
	    	            SkySiteUtils.waitTill(6000);
	    	         
	                	
	                }
	                else
	                {
	                	Log.message("Searched contact is not dispayed!!!");
	                	return false;
	                	
	                }

    	            SkySiteUtils.waitForElement(driver, btnResetContactSearch, 30);
                    btnResetContactSearch.click();
                    Log.message("Clicked on Reset button to clear previous search");
                    SkySiteUtils.waitTill(5000);
                    
                    SkySiteUtils.waitForElement(driver, SearchContact, 30);
	                SearchContact.clear();
	                SearchContact.sendKeys(ContactName);
	                Log.message("Contact name entered for search is:- "+ContactName);
	                SkySiteUtils.waitTill(3000);
	              
	                driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
	                Log.message("Clicked on search contact button");
	                SkySiteUtils.waitTill(6000);
	              
	                String searchedContactEmail = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[4]")).getText();
	                Log.message("Searched contact email is:- "+searchedContactEmail);
                    
	                if(searchedContactEmail.contains(Emailupdate))
	                {
	                	return true;
	                	
	                }
	                else 
	                {
	                	return false;
	                	
	                }
	                	
	             
	                
	      		  	/*editicon.click();
	           	Log.message("edit icon clicked ");
	           	SkySiteUtils.waitTill(20000);
	           	String MainWindow=driver.getWindowHandle();
	       		Log.message(MainWindow);
	       		for(String childwindow:driver.getWindowHandles())
	       		{
	       			driver.switchTo().window(childwindow);//swtich to child window
	       		
	       		}
	       		SkySiteUtils.waitTill(6000);
	       		SkySiteUtils.waitForElement(driver, Email, 40);
	       		Email.click();
	       		Email.clear();
	       		Email.sendKeys(Emailupdate);
	       		Log.message("Email entered to be updated " +Emailupdate);
	       		SkySiteUtils.waitTill(5000);
	       		JavascriptExecutor js = (JavascriptExecutor) driver;
	               js.executeScript("window.scrollBy(0,1000)");
	               Log.message("Scrolled down to window");
	               SkySiteUtils.waitTill(5000);
	               Savebutton.click();
	               Log.message("savebutton is clicked");
	               SkySiteUtils.waitTill(4000);
	               driver.switchTo().window(MainWindow);//switch to main window
	               Log.message("Switched to main window");
	               SkySiteUtils.waitTill(4000);
	               driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	               Log.message("Switched to frame");
	               SkySiteUtils.waitTill(6000);
	     		  	int resultantrow=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	     		  	Log.message("Number of  row item found is:" +resultantrow);
	     		  	int i=0;
	     			for(i = 1; i <= resultantrow; i++) 
	     			{
	     					
	     				String Emailname=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText();
	     				//Log.message("Contact name is:"+contactName);
	     				SkySiteUtils.waitTill(5000);			
	     				//Checking contact name equal with each row of table
	     				if(Emailname.contains(Emailupdate))
	     				{	   
	     				
	     					Log.message("Email has been updated and visible in the list!!!");
	     				    break;
	     				}	
	     			
	     			}
	     			String Updatedemail=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText();
	     			Log.message("Email found in list is " +Updatedemail);
	     			if(Updatedemail.contains(Emailupdate))	
	     				return true;
	     			else
	     				return false;*/
	          }
	           
	           
	           
	           @FindBy(css="#btnResetSearch")
	           @CacheLookup
	           WebElement resetbutton;
	           @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/button")
	           @CacheLookup
	           WebElement dropdownoptiondefault;
	           /**Method to check rest functionality for search option
	            * Scripted By : Tarak
	            */
	           public boolean resetFunctionality(String contactnamesearched) {
	           	driver.switchTo().defaultContent();
	           	Log.message("Swithcing into default content");
	           	SkySiteUtils.waitTill(4000);
	           	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	           	Log.message("switching into frame");
	           	SkySiteUtils.waitTill(4000);
	           	resetbutton.click();
	           	Log.message("restbutton is clicked ");
	           	SkySiteUtils.waitTill(6000);
	           	String valueselected=dropdownoptiondefault.getAttribute("title");
	           	Log.message("Value selected by default is " +valueselected);
	           	if(valueselected.contains("Contact/Group Name")&&searchbox.getText().isEmpty()){
	           		Log.message("Search box is cleared also contactname option selected by default and reset functioanlity is working ");
	           		return true;
	           		
	           	}
	                return false;
	           }
	             
	            /**Method to generate random excel value
	             * Scripted By : Tarak
	             * 
	             */
	              public String random_ExcelValue() {
	              	
	               String str="ExcelUpdateContact";
	   	           Random r = new Random( System.currentTimeMillis() );
	   	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	   	           return abc;
	             }
	            /** Method to update the excel value
	             * SCripted By:Tarak
	             * @param contactname12
	             * @param downloadpathforimport
	           * @throws IOException 
	             */
	            
	        public void searchValueIntable(String downloadpathforimport,String excelvaluetobeset) throws IOException
	            {
	      	
	  	          driver.switchTo().defaultContent();
	  	          Log.message("Switched to default content");
	  	          SkySiteUtils.waitTill(5000);
	  	          driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	  	          Log.message("Switched to frame");
	  	          SkySiteUtils.waitTill(5000);
	        	  FileInputStream file = new FileInputStream(downloadpathforimport + "\\" + "Contacts.xlsx");
	        	  XSSFWorkbook workbook = new XSSFWorkbook(file);
	              XSSFSheet sheet = workbook.getSheet("Sheet1");
	              Cell cell = null;
	              cell = sheet.getRow(2).getCell(0);
	              cell.setCellValue(excelvaluetobeset);
	              file.close();
	              FileOutputStream outFile =new FileOutputStream(downloadpathforimport + "\\" + "Contacts.xlsx");
	              workbook.write(outFile);
	              outFile.close();
	              Log.message("File writing done");
	              Log.message("Value set in excel is :" +excelvaluetobeset);
	       }
	     

	        	@FindBy(css="#btnimport")
	            @CacheLookup
	            WebElement importAddressbookicon;
	            @FindBy(css="#File1")
	            @CacheLookup
	            WebElement Selectfileoption;
	            @FindBy(css="#btUpload")
	            @CacheLookup
	            WebElement nextbtn;
	            @FindBy(css="#rdoOverwrite")
	            @CacheLookup
	            WebElement overrideimportoption;
	            @FindBy(css="#btNext")
	            @CacheLookup
	            WebElement nextbtn2;
	            @FindBy(xpath=".//input[@value='Close']")
	            @CacheLookup
	            WebElement clsbutton;
	            @FindBy(css="#ddlTableName")
	            @CacheLookup
	            WebElement dropdown;
	            
	            /** Method for Address book import and verification of import 
	             * Scripted By : Tarak
	             */
	            
	            public boolean importAddressBook(String FolderPath,String valuetobesetinexcel,String tempfilepath) throws IOException 
	            {
	          	  SkySiteUtils.waitTill(4000);
	          	  driver.switchTo().defaultContent();
	          	  Log.message("Switching to default content");
	          	  SkySiteUtils.waitTill(4000);
	          	  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	          	  Log.message("Switched into frame");
	          	  SkySiteUtils.waitTill(5000);
	          	  importAddressbookicon.click();
	          	  Log.message("Import addresss book option is clicked");
	          	  SkySiteUtils.waitTill(5000);
	          	  driver.switchTo().defaultContent();
	          	  Log.message("Switch to default content");
	          	  SkySiteUtils.waitTill(5000);
	          	  driver.switchTo().frame(driver.findElement(By.id("iImportContact")));
	          	  SkySiteUtils.waitTill(5000);
	          	  Selectfileoption.click();
	          	  Log.message("select file option is clicked");
	          	  
	          	//Writing File names into a text file for using in AutoIT Script
	        		BufferedWriter output;			
	        		randomFileName rn=new randomFileName();
	        		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
	        		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
	        				
	        		String expFilename=null;
	        		File[] files = new File(FolderPath).listFiles();
	        				
	        		for(File file : files)
	        		{
	        			if(file.isFile()) 
	        			{
	        				expFilename=file.getName();//Getting File Names into a variable
	        				Log.message("Expected File name is:"+expFilename);	
	        				output.append('"' + expFilename + '"');
	        				output.append(" ");
	        				SkySiteUtils.waitTill(5000);	
	        			}	
	        		}	
	        		output.flush();
	        		output.close();	
	        				
	        		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
	        		//Executing .exe autoIt file		 
	        		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
	        		Log.message("AutoIT Script Executed!!");				
	        		SkySiteUtils.waitTill(30000);
	        				
	        		try
	        		{
	        			File file = new File(tmpFileName);
	        			if(file.delete())
	        			{
	        				Log.message(file.getName() + " is deleted!");
	        			}	
	        			else
	        			{
	        				Log.message("Delete operation is failed.");
	        			}
	        		}
	        		catch(Exception e)
	        		{			
	        			Log.message("Exception occured!!!"+e);
	        		}			
	        		SkySiteUtils.waitTill(5000);
	        		nextbtn.click();
	        		Log.message("Next button is clicked");
	        		SkySiteUtils.waitTill(6000);
	        		Select sc=new Select(dropdown);
	                sc.selectByVisibleText("Sheet1");
	                Log.message("Sheet1 selected for import");
	                SkySiteUtils.waitTill(7000);
	            	JavascriptExecutor js = (JavascriptExecutor) driver;
	                js.executeScript("window.scrollBy(0,1000)");
	                Log.message("Scrolled down to window");
	                SkySiteUtils.waitTill(7000);
	                overrideimportoption.click();
	                Log.message("Override option for import selected");
	                SkySiteUtils.waitTill(9000);
	                nextbtn2.click();
	                Log.message("Second next button is clicked");
	                SkySiteUtils.waitTill(40000);
	                SkySiteUtils.waitForElement(driver, clsbutton,60);
	                clsbutton.click();
	                Log.message("Close button clicked");
	                SkySiteUtils.waitTill(11000);
	                driver.switchTo().defaultContent();
	                SkySiteUtils.waitTill(7000);
	               driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	                Log.message("Switched to frame");
	                SkySiteUtils.waitTill(6000);
	              
	              int Count_list=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	      		  Log.message("Total Contact list is: " +Count_list);			
	      		
	      		//Loop start '2' as projects list start from tr[2]
	      		int i=0;
	      		for(i = 1; i <= Count_list; i++) 
	      		{
	      				
	      			String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	      			//Log.message("Contact name is:"+contactName);
	      			SkySiteUtils.waitTill(5000);			
	      			//Checking contact name equal with each row of table
	      			if(contactName.contains(valuetobesetinexcel))
	      			{	   
	      				Log.message("Updated contact after import in the list is "  +contactName);
	      				Log.message("Contact import successfully with override option and the updated excel value is seen in the table after import !!!");		                  
	      				break;
	      			}		
	      		}
	      		String contactNameinlist=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	        	if(contactNameinlist.contains(valuetobesetinexcel))	
	        		return true;
	        	else
	        		return false;
	     }
	            
	            
	            @FindBy(xpath="//*[@placeholder='Search']")
	            WebElement SearchContact;
	            
	            /** Method to create contact with type as contact
	              * By Trinanjwan
	              */
	       
	        
	               public boolean CreateNewContact_cntcsel(String Newcontactname,String Emailid,String Lastnamerandom)
	               {
	                   driver.switchTo().defaultContent();//switch to default window after adress page landed
	                   SkySiteUtils.waitTill(5000);
	                   
	                   driver.switchTo().frame("myFrame");//Again switch to frame to click the addnewcontact button
	                   SkySiteUtils.waitTill(6000);
	                   
	                   NewContactBtn.click();
	                   Log.message("AddNewContact button is clicked");
	                   SkySiteUtils.waitTill(15000);
	                   
	                   String MainWindow=driver.getWindowHandle();
	                   Log.message(MainWindow);
	                   
	                   for(String childwindow:driver.getWindowHandles())
	                   {
	                          driver.switchTo().window(childwindow);//swtich to child window
	                   
	                   }
	                   
	                   SkySiteUtils.waitForElement(driver, Employeebtn, 50);
	                   Cntctselbtn.click();
	                   Log.message("Contact user is selected");
	                   SkySiteUtils.waitTill(15000);
	                   
	                   Firstname.click();
	                   Firstname.sendKeys(Newcontactname);
	                   Log.message("Contactname entered is : " +Newcontactname);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   Lastname.sendKeys(Lastnamerandom);
	                   Log.message("Lastname entered is " +Lastnamerandom);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   String CityName=PropertyReader.getProperty("City");
	                   city.sendKeys(CityName);
	                   Log.message("City entered is " +CityName);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   String Pcode=PropertyReader.getProperty("Postcode");
	                   Postalcode.sendKeys(Pcode);
	                   Log.message("postal code entered is " +Pcode);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   String TName=PropertyReader.getProperty("Titlename");
	                   Title.sendKeys(TName);
	                   Log.message("Title entered is " +TName);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   String Cname=PropertyReader.getProperty("Countryname");
	                   Select sc=new Select(Country);
	                   sc.selectByVisibleText(Cname);
	                   Log.message("Country selected is :" +Cname);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   String Companynm=PropertyReader.getProperty("Company");
	                   Company.sendKeys(Companynm);
	                   Log.message("Companyname entered is " +Companynm);
	                   SkySiteUtils.waitTill(5000);
	                   
	                   String statenm=PropertyReader.getProperty("Statenm");
	                   Select selectstate=new Select(statedropdown);
	                selectstate.selectByVisibleText(statenm);
	                Log.message("State selected is " +statenm);
	                   SkySiteUtils.waitTill(5000);
	                String addressfirst=PropertyReader.getProperty("Address1");
	                Address1.sendKeys(addressfirst);
	                Log.message("Address1 entered is" +addressfirst);
	                   SkySiteUtils.waitTill(5000);
	             Email.sendKeys(Emailid);
	             Log.message("Email id entered is" +Emailid);
	            SkySiteUtils.waitTill(5000);
	             String addresssecond=PropertyReader.getProperty("Address2");
	             Address2.sendKeys(addresssecond);
	             Log.message("Address2 entered is " +addresssecond);
	            SkySiteUtils.waitTill(5000);
	             String Phnno=PropertyReader.getProperty("Phoneno");
	             PhoneNo.sendKeys(Phnno);
	             Log.message("Phone no entered is : " +Phnno);
	            SkySiteUtils.waitTill(5000);
	             String Compwebsite=PropertyReader.getProperty("CompanyWebSite");
	             Companywebsite.sendKeys(Compwebsite);
	             Log.message("Company website entered : " +Compwebsite);
	            SkySiteUtils.waitTill(5000);
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	             js.executeScript("window.scrollBy(0,1000)");
	             Log.message("Scrolled down to window");
	             SkySiteUtils.waitTill(5000);
	             String Projectroll=PropertyReader.getProperty("Projectrole");
	             Select scrole=new Select(ProjectRole);
	             scrole.selectByVisibleText(Projectroll);
	            SkySiteUtils.waitTill(5000);
	             Log.message("ProjectRoll selected is " +Projectroll);
	             String Buisnessname=PropertyReader.getProperty("Buisnessdetails");
	             Select selectBuisness=new Select(Buisness);
	             selectBuisness.selectByVisibleText(Buisnessname);
	             Log.message("Buisness selected is " +Buisnessname);
	             SkySiteUtils.waitTill(5000);
	             Select selectoccupation=new Select(Occupation);
	             selectoccupation.selectByIndex(2);
	             Log.message("Occupation is selected");
	            
	             SkySiteUtils.waitTill(5000);
	             Savebutton.click();
	             Log.message("Save button is clicked");
	             SkySiteUtils.waitTill(5000);
	             driver.switchTo().window(MainWindow);//switch to main window
	             Log.message("Switched to main window");
	             SkySiteUtils.waitTill(6000);
	             driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	             Log.message("Switched to frame");
	             SkySiteUtils.waitTill(6000);
	             
	             SkySiteUtils.waitForElement(driver, SearchContact, 30);
	             SearchContact.clear();
	             SearchContact.sendKeys(Newcontactname);
	             Log.message("Contact name entered for search is:- "+Newcontactname);
	             SkySiteUtils.waitTill(3000);
	             
	             driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
	             Log.message("Clicked on search contact button");
	             SkySiteUtils.waitTill(6000);
	             
	             String searchedContact = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
	             Log.message("Searched contact name is"+searchedContact);
	             
	             if(searchedContact.contains(Newcontactname))     
	             {
	            	 return true;
	             }             
	             else
	             {
	            	 return false;
	             }
                              
               }

	       

	       

	       /** Method to Check the presence of Create Group Button
	       * By Trinanjwan
	       */
	              
	       public boolean presenceOfCreateGroupButton()
	       {
	              //SkySiteUtils.waitForElement(driver, CreateGrpbtn, 20);
	              driver.switchTo().frame("MyFrame");
	              Log.message("The Create group button to be appeared");
	              if(CreateGrpbtn.isDisplayed())
	                     return true;
	                     else
	                     return false;
	              }

	       /** Method for verification of created group  
	        * By Trinanjwan
	       */
	       
	       public FnaCreateGroupPage clickingOnCreateGroupButton() throws AWTException
	       
	       {
	              driver.switchTo().defaultContent();
	              SkySiteUtils.waitTill(5000);
	              driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	              SkySiteUtils.waitTill(5000);
	              CreateGrpbtn.click();
	              SkySiteUtils.waitTill(5000);
	              Log.message("Clicked on Create Group button");
	              return new FnaCreateGroupPage(driver).get();
	              
	              
	              
	       }
	       
	       /** Method for validation of created group  
	        * By Trinanjwan
	       */
	       
	       
	       public boolean verificationOfCreatedGroup(String Groupname2) throws AWTException
	       
	       {
	              	 SkySiteUtils.waitTill(10000);
	              
	              	 SkySiteUtils.waitForElement(driver, SearchContact, 30);
		             SearchContact.clear();
		             SearchContact.sendKeys(Groupname2);
		             Log.message("Group name entered for search is:- "+Groupname2);
		             SkySiteUtils.waitTill(3000);
		             
		             driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
		             Log.message("Clicked on search contact/group button");
		             SkySiteUtils.waitTill(6000);
		             
		             String searchedGroup = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
		             Log.message("Searched group name is"+searchedGroup);
		             
		             if(searchedGroup.contains(Groupname2))     
		             {
		            	 SkySiteUtils.waitForElement(driver, btnResetContactSearch, 30);
	                     btnResetContactSearch.click();
	                     Log.message("Clicked on Reset button to clear previous search");
	                     SkySiteUtils.waitTill(5000);
	                     
		            	 return true;
		             }             
		             else
		             {
		            	 return false;
		             }
	              
	              /*int Count_list1=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	              
	              
	              //Log.message("Count is:"+Count_list1);              
	              int i=0;
	              for(i = 1; i <= Count_list1; i++) 
	              {
	                     
	                     String Groupname1=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	                     
	                     //Log.message("Group name is:"+Groupname1);
	                     SkySiteUtils.waitTill(5000);                   
	                     if(Groupname1.contains(Groupname2))
	                     {                    
	                           Log.message("Group name exists in the list");                          
	                           break;
	                     }             
	              }
	              String GroupName3=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	              Log.message("Group name is:"+GroupName3);
	              if(GroupName3.contains(Groupname2))    
	                     return true;
	              else
	                     return false;*/
	              }

	       /** Method for navigation inside the created group  
	        * By Trinanjwan
	       */


	       public FnaCreateGroupPage navigationInsideTheCreatedGroup(String Groupname4)
	       
	       {
	          
	          Searchboxnew.click();
	          Log.message("Clicked on the Search field");
	          Searchboxnew.sendKeys(Groupname4);
	          Log.message("Value entered in the search field");
	          SkySiteUtils.waitTill(5000);
	          Searchbtn.click();
	          Log.message("Clicked on search button");
	          SkySiteUtils.waitTill(5000);
	          Grpnamelist.click();
	          Log.message("Clicked on the group from the list");
	          SkySiteUtils.waitTill(5000);
	          return new FnaCreateGroupPage(driver).get();
	              
	       }

	       /** Method for search of contact from the list  
	        * By Trinanjwan
	        */

	              public void searchForContactFrmList(String new_cntname)
	              
	              {
	                        Searchboxnew.click();
	                 Log.message("Clicked on the Search field");
	                 Searchboxnew.sendKeys(new_cntname);
	                 Log.message("Value entered in the search field");
	                 SkySiteUtils.waitTill(5000);
	                 Searchbtn.click();
	                 Log.message("Clicked on search button");
	                 SkySiteUtils.waitTill(5000);         
	                     
	              }
	              
	           
	              @FindBy(xpath="//button[@id='btnResetSearch']/i")
	              WebElement btnResetContactSearch;
	              
	       /** Method for deletion of contact from the list  
	        * By Trinanjwan
	       */

	              public void deletionOfContactFrmList(String new_cntname)
	              
	              {
	                     
	                     SkySiteUtils.waitForElement(driver, btnResetContactSearch, 30);
	                     btnResetContactSearch.click();
	                     Log.message("Clicked on Reset button to clear previous search");
	                     SkySiteUtils.waitTill(5000);
	                     
	                     SkySiteUtils.waitForElement(driver, SearchContact, 30);
	    	             SearchContact.clear();
	    	             SearchContact.sendKeys(new_cntname);
	    	             Log.message("Contact name entered for search is:- "+new_cntname);
	    	             SkySiteUtils.waitTill(3000);
	    	             
	    	             driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
	    	             Log.message("Clicked on search contact button");
	    	             SkySiteUtils.waitTill(6000);
	                     
	    	             String searchedContact = driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[2]")).getText();
	    	             Log.message("Searched contact name is"+searchedContact);
	    	             
	    	             if(searchedContact.contains(new_cntname))
	    	             {
	    	            	 driver.findElement(By.xpath("//div[@class='objbox']/table/tbody/tr[2]/td[1]/img")).click();
	    	            	 Log.message("Clicked checked box for selecting searched contact");
	    	            	 SkySiteUtils.waitTill(3000);
	    	            	 
	    	            	 SkySiteUtils.waitForElement(driver, Deletebtn, 30);
	    	            	 Deletebtn.click();
	    	            	 Log.message("Clicked on Delete button to delete searched contact");
	    	            	 SkySiteUtils.waitTill(3000);
		                     
	    	            	 SkySiteUtils.waitForElement(driver,delconfirmbtn,30);
		                     delradiobtn.click();
		                     Log.message("Clicked on the radio button_ Delete the contact(s) and remove them from all collections, tasks and communications");
		                     SkySiteUtils.waitTill(3000);
		                     delconfirmbtn.click();
		                     Log.message("Clicked on Confirm button");
		                     SkySiteUtils.waitTill(5000);   
	    	            	 
	    	            	 
	    	             }
	    	             else
	    	             {
	    	            	 Log.message("Searched contact not found!!!");
	    	            	 
	    	             }
	                     
	                       
	                     
	              }
	              
	              
	              
	       /** Method for verification of deleted contact
	       * By Trinanjwan
	       */
	              
	              
	                     public boolean verificationOfDeletedCntct(String cntct2)
	                     
	                     {
	                           
	                    	 SkySiteUtils.waitForElement(driver, btnResetContactSearch, 30);
		                     btnResetContactSearch.click();
		                     Log.message("Clicked on Reset button to clear previous search");
		                     SkySiteUtils.waitTill(5000);
	                    	 
	                    	 SkySiteUtils.waitForElement(driver, SearchContact, 30);
		    	             SearchContact.clear();
		    	             SearchContact.sendKeys(cntct2);
		    	             Log.message("Contact name entered for search is:- "+cntct2);
		    	             SkySiteUtils.waitTill(3000);
		    	             
		    	             driver.findElement(By.xpath("//*[@onclick='javascript: OnSearchClick();']")).click();
		    	             Log.message("Clicked on search contact button");
		    	             SkySiteUtils.waitTill(6000);
		    	             
		    	             int count = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]")).size();
		    	             Log.message("Searched results displayed are:- "+count);
		    	             if(count<1)
		    	             {
		    	            	 return true;
		    	             }
		    	             else
		    	             {
		    	            	 return false;
		    	             }
		                     

	                     }

	                           
	                     
	                     
	                                         
	              
	              
	       
	              public boolean searchgroupname(String Groupname4 )
	             
	              {
	                    
	                     SkySiteUtils.waitTill(5000);
	                    Searchboxnew.click();
	                    Log.message("Clicked on the search field");
	           Searchboxnew.sendKeys(Groupname4);
	           Log.message(Groupname4+"entered in the search field");
	           SkySiteUtils.waitTill(2000);
	           Searchbtn.click();
	           Log.message("Clicked on search button");
	           SkySiteUtils.waitTill(4000);
	           String Groupname1=driver.findElement(By.xpath("/html/body/form/div[3]/section[2]/div/div/div[1]/div[2]/div[2]/table/tbody/tr[2]/td[2]/a")).getText();
	           Log.message("Group name is"+Groupname1);
	           if(Groupname4.contains(Groupname1))
	           {
	              return true;
	           }
	               else
	                          return false;
	     
	    }

	    
  	 }