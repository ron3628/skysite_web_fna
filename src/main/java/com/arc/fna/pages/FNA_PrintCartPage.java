package com.arc.fna.pages;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FNA_PrintCartPage extends LoadableComponent<FNA_PrintCartPage> {
	WebDriver driver;
	private boolean isPageLoaded;

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FNA_PrintCartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@id='lftpnlMore']")
	WebElement moreoption;
	@FindBy(xpath = ".//*[@id='btnFolderAddToCart']/a")
	WebElement printcartoption;
	@FindBy(xpath = ".//*[@id='liPrintCart']/a")
	WebElement printcarttab;
	@FindBy(xpath = ".//*[text()='TestFolder']")
	WebElement foldername;
	@FindBy(xpath = "//*[text()='TestFolder (2 files)']")
	WebElement foldernameinprintcart;

	/**
	 * Method to add folder to printcart from folder level
	 * 
	 * 
	 * 
	 */
	public boolean addFolderToPrintCart() {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		// WebElement
		// foldername=driver.findElement(By.xpath(".//*[text()='TestFolder']"));
		SkySiteUtils.waitForElement(driver, foldername, 50);
		String folderincollectn = foldername.getText();
		Log.message("Folder name which will be added to cart is " + folderincollectn);
		foldername.click();
		Log.message("Folder selected");
		SkySiteUtils.waitForElement(driver, moreoption, 50);
		moreoption.click();
		Log.message("more option is clicked");
		SkySiteUtils.waitForElement(driver, printcartoption, 50);
		printcartoption.click();
		Log.message("Printcart option is clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, printcarttab, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", printcarttab);
		Log.message("print cart tab is clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("switched to frame");
		// WebElement
		// foldernameinprintcart=driver.findElement(By.xpath(".//*[@id='collapse1']/div/div/div[1]/div/div[2]/h4"));
		SkySiteUtils.waitForElement(driver, foldernameinprintcart, 50);
		String foldernamepresent = foldernameinprintcart.getText();
		Log.message("Folder name present in print cart is " + foldernamepresent);
		String[] folderinprintcart = foldernamepresent.split(" ");
		String foldernamepresentinprintcart = folderinprintcart[0];
		Log.message("Acutal foldername present in printcart is " + foldernamepresentinprintcart);
		if (foldernamepresentinprintcart.equals(folderincollectn))
			return true;
		else
			return false;

	}

	@FindBy(xpath = "//*[@class='icon icon-trash icon-lg']")
	WebElement deleteicon;

	/**
	 * method to delete added item in cart
	 * 
	 */
	public void deleteItemFromCart() {
		SkySiteUtils.waitForElement(driver, deleteicon, 60);
		deleteicon.click();
		Log.message("delete icon is clicked and item is deleted");
		SkySiteUtils.waitTill(4000);
	}

	// @FindBy(xpath=".//*[@id='rgtpnlMore']")
	//@FindBy(xpath = "//*[@class='icon icon-export ico-lg']/following::button[1]")
	@FindBy(css = "#Button1")
	WebElement morebutton;
	//@FindBy(xpath = ".//*[@id='liAddToCart1']/a ")
	
	//@FindBy(xpath="//*[@class='icon icon-cart ']")
	@FindBy(xpath="//a[@onclick='javascript: AddToCartMultipleFiles();']")
	WebElement printcartmenu;
	@FindBy(xpath = "//*[@title='1 mb.pdf']")
	WebElement fileaddedinpcart;
	
	@FindBy (xpath = "//*[@id='rgtpnlMore']")
    WebElement more1 ;

    @FindBy(css = "#Button1")
    WebElement more2;


	/**
	 * add file to print cart and verify
	 * 
	 */
	public boolean addFileToPrintCart() 
	{
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
	
		SkySiteUtils.waitForElement(driver, foldername, 50);
		String folderincollectn = foldername.getText();
		Log.message("Folder name which will be added to cart is " + folderincollectn);
		foldername.click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(3000);
		
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")).click();
		Log.message("File is clicked");
		
		if (more1.isDisplayed()) 
		{
            more1.click();
            Log.message("Clicked on Local more");
            SkySiteUtils.waitTill(5000);
        
		}
		else 
		{
            more2.click();
            Log.message("Clicked on remote more");
            SkySiteUtils.waitTill(5000);
            
		}
	
		SkySiteUtils.waitForElement(driver, printcartmenu, 50);
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", printcartmenu);
		Log.message("print cart menu is clicked");
		SkySiteUtils.waitTill(3000);
		
		driver.switchTo().defaultContent();
		
		SkySiteUtils.waitForElement(driver, printcarttab, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", printcarttab);
		Log.message("print cart tab is clicked");
		SkySiteUtils.waitTill(3000);
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("switched to frame");
		
		String filenamepresentintprintcart = fileaddedinpcart.getText();
		Log.message("File name present in print cart is " + filenamepresentintprintcart);
		String fileaddedtoprintcart = PropertyReader.getProperty("filetobeadded");
		Log.message("File name to be compared is " + fileaddedtoprintcart);
		
		if (filenamepresentintprintcart.equals(fileaddedtoprintcart))
			return true;
		else
			return false;

	}

	
	
	@FindBy(xpath = ".//*[text()='Remove all']")
	WebElement removeallbutn;

	/**
	 * Verify Deletion of added items in print cart
	 * 
	 */
	public boolean deletionOfMultiItem() {
		int count = driver.findElements(By.xpath(".//*[@id='collapse1']/div/div/div[3]/a/i")).size();
		Log.message("total no of item in print cart is " + count);
		int i = 0;
		for (i = 1; i <= count; i++) {

			Log.message("deleting the " + i + "item in the list");
			SkySiteUtils.waitForElement(driver, deleteicon, 60);
			deleteicon.click();
			Log.message("delete icon is clicked and item is deleted");
			SkySiteUtils.waitTill(4000);
			if (i == count) {
				break;
			}

		}
		List<WebElement> removebutn = driver.findElements(By.xpath(".//*[text()='Remove all']"));
		int elesize = removebutn.size();
		SkySiteUtils.waitTill(4000);
		int countdeleticon = driver.findElements(By.xpath(".//*[@id='collapse1']/div/div/div[3]/a/i")).size();
		if (countdeleticon == 0 && elesize < 1) {
			Log.message("All items are deleted successfully");
			return true;
		} else {

			Log.message("Item are still presnt in the print cart and cannot be deleted");
			return false;
		}
	}

	@FindBy(xpath = "//*[text()='Remove all']")
	WebElement removallbutton;
	@FindBy(xpath = "//*[text()='Yes']")
	WebElement yesbtn;

	/**
	 * verify remove all option
	 * 
	 */
	/*public boolean verifyRemoveAllOption() {
		int count = driver.findElements(By.xpath(".//*[@id='collapse1']/div/div")).size();
		Log.message("no of item in the list are " + count);
		SkySiteUtils.waitForElement(driver, removallbutton, 40);
		removallbutton.click();
		Log.message("remove button is clicked");
		yesbtn.click();
		Log.message("yes button is clicked");
		int countofitem = driver.findElements(By.xpath(".//*[@id='collapse1']/div/div")).size();
		Log.message("count after deletion is " + countofitem);
		if (countofitem == 0) {
			Log.message("remove all button working fine and all item are deleted succssfully");
			return true;
		} else {
			Log.message("remove all button is not working and item present are not deleted");
			return false;
		}
	}*/

	/**
	 * Method to verify remove all button functionality
	 * 
	 * 
	 */
	public boolean removeAllButton() {
		driver.switchTo().defaultContent();
		Log.message("Switch to default content");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		int noofitem = driver.findElements(By.xpath(".//*[@id='collapse1']/div/div")).size();
		Log.message("no of item in the list are " + noofitem);
		List<WebElement> removebutn = driver.findElements(By.xpath(".//*[text()='Remove all']"));
		int elesize = removebutn.size();
		SkySiteUtils.waitForElement(driver, removeallbutn, 60);
		removeallbutn.click();
		Log.message("Remove all button is clicked");
		SkySiteUtils.waitForElement(driver, yesbtn, 60);
		yesbtn.click();
		Log.message("yes button is clicked");
		SkySiteUtils.waitTill(4000);
		int countafterdeltion = driver.findElements(By.xpath(".//*[@id='collapse1']/div/div")).size();
		Log.message("count after deletion is " + countafterdeltion);
		if (countafterdeltion == 0) {
			Log.message("All items are deleted successfully");
			return true;
		} else {

			Log.message("Item are still presnt in the print cart and cannot be deleted");
			return false;
		}

	}

	@FindBy(xpath = "//*[text()='Back to files']")
	WebElement backtofileoption;
	@FindBy(xpath = "//*[@id='plctrl_lblProjectName']")
	WebElement projectname;

	/**
	 * Method to verify back to file option
	 * 
	 */
	public boolean BackToFileOption(String collectionselected) {
		SkySiteUtils.waitForElement(driver, backtofileoption, 40);
		backtofileoption.click();
		Log.message("back to file option is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		String collectionnamepresent = projectname.getText();
		Log.message("Collection name present afte clicking backtofile option is " + collectionnamepresent);
		if (collectionnamepresent.equals(collectionselected)) {
			Log.message("right collection is navigated");
			return true;
		} else {
			Log.message("right collection is navigated");
			return false;
		}

	}
    /**verify the no of files for folder added in print cart
     * 
     */
	public boolean verifyNoOfFilesforFolder() 
	{
		SkySiteUtils.waitForElement(driver, foldernameinprintcart, 50);
		
		String foldernamepresent = foldernameinprintcart.getText();
		Log.message("Folder name present in print cart is " + foldernamepresent);
		
		String[] folderinprintcart = foldernamepresent.split(" ");
		String foldernamepresentinprintcart = folderinprintcart[0];
		String nooffileswithfolder=folderinprintcart[1];
		Log.message("No of files in the folder is :-"+nooffileswithfolder);
		
		String[] nooffile=nooffileswithfolder.split(" " );
		String filewithbracket=nooffile[0];
		Log.message("file with brackt is " +filewithbracket);
		
	    String fileinfolder=PropertyReader.getProperty("Nooffile");
	    
		if(filewithbracket.contains(fileinfolder)) 
		{
			Log.message("same no of file is present with folder");
            return true;
		}           
		else 
		{
        	 Log.message("different file is present with folder");
             return false;	
         }
	
	}
	
		
		
		
		
	
	/**method to verify no of copies in print cart for each item added
	 * 
	 */
	public boolean verifyNoOfCopiesForItemAdded(int noofcopies) {
       String text=driver.findElement(By.xpath(".//*[@id='collapse1']/div/div/div[2]/div/div/input")).getAttribute("value");
       Log.message("no of copies present is " +text);
	   int noofcopiesinprintcart=Integer.parseInt(text);
	   if(noofcopiesinprintcart==noofcopies) {
		   Log.message("No of copies is correct");
		   return true;
	}
	
else {
			   Log.message("No of copies is incorrect");
			   return false;
		   }
		
		}
	/**Method to generate first name
	 * Scripted By:Tarak
	 * @return
	 */
	  public String Random_FirstName()
	    {
	           
	           String str="FirstName";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
		/**Method to generate first name
		 * Scripted By:Tarak
		 * @return
		 */
		  public String Random_Email()
		    {
		           
		           String str="testskysite";
		           String str3="@gmail.com";
		           Random r = new Random( System.currentTimeMillis() );
		           String abc = str+String.valueOf((10000 + r.nextInt(20000)))+str3;
		           return abc;
		           
		    }
   @FindBy(xpath="//a[@class='changeTxt pointer']")
   WebElement changeoption;
   @FindBy(xpath="//*[@placeholder='Full name']")
   WebElement firstname;
   @FindBy(xpath="//*[@placeholder='Phone']")
   WebElement Phone;
   @FindBy(xpath="//*[@placeholder='Email']")
   WebElement Emailbox;
   @FindBy(xpath=".//*[@id='addressinput']")
   WebElement addressbox;
   @FindBy(xpath="//*[text()='Save']")
   WebElement savebtn;
   /**Method to change the delivery address
    * 
    */
	public boolean deliveryAddress(String firstname12,String emailid) {
		SkySiteUtils.waitForElement(driver, changeoption,50);
		changeoption.click();
		Log.message("change option is clicked");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='editHld']/div[1]/div/div/div/a/i")).click();
		Log.message("add new address option is clicked:");
		SkySiteUtils.waitForElement(driver, firstname, 50);
		firstname.sendKeys(firstname12);
		Log.message("First name enteed is " +firstname);
		SkySiteUtils.waitForElement(driver, Phone, 50);
		Phone.sendKeys("2524234242");
		SkySiteUtils.waitForElement(driver, Emailbox, 50);
		Emailbox.sendKeys(emailid);
		Log.message("Email id entered is " +emailid);
		SkySiteUtils.waitTill(2000);
		addressbox.sendKeys("Kolkata WestBengal ");
		Log.message("address is entered");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
String emailchanged=driver.findElement(By.xpath(".//*[@id='orddetail']/div[1]/div/div[2]/div/div/div/div[1]/a")).getAttribute("title");
		Log.message("Changed email in pcart is " +emailchanged);
		if(emailchanged.equals(emailid))
		{
			Log.message("address is changed successfully");
			return true;
		}
		else {
			Log.message("address cannot be changed");
			return false;
		}
		
}
	@FindBy(xpath="//*[text()='Upload file']")
	WebElement uploadbtn;
	/**Method to upload file in printcart
	 * @throws IOException 
	 * 
	 */
	public void uploadfileinprintcart() throws IOException {
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  js.executeScript("window.scrollBy(0,1000)");
		  Log.message("page scrolled");
		  driver.switchTo().defaultContent();
		  Log.message("switched to default content");
		  driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, uploadbtn, 50);
		Actions act=new Actions(driver);
	    act.moveToElement(uploadbtn).click(uploadbtn).build().perform();
		Log.message("Upload button is clicked");
		SkySiteUtils.waitTill(4000);
		File f=new File("Autoit");
		File fs=new File(f,"Autoitmultifile.exe");
		File f1=new File("FileForPrintCart");
		File fs1=new File(f1,"1_Document.pdf");
		Runtime.getRuntime().exec(fs.getAbsolutePath() + " " + f1.getAbsolutePath());
		Log.message("Auto it script is executed");
		SkySiteUtils.waitTill(6000);
		List<WebElement> filename=driver.findElements(By.xpath("//*[@class='docOutr row']"));
		for(WebElement var:filename) {
			Log.message("File name is " +var.getText());
		}
		
		int nooffile=driver.findElements(By.xpath(".//*[@id='orddetail']/div[3]/div/div/div/div/div[2]/a/i")).size();
		Log.message("No of file present is " +nooffile);
		//String filenameuploaded=driver.findElement(By.xpath("//*[@title='1_Document.pdf']")).getText();
		//Log.message("file uploaded is " +filenameuploaded);
		//if(driver.findElement(By.xpath(".//*[@id='orddetail']/div[3]/div/div/div/div[3]/div[1]/span")).isDisplayed()==true) {
		//	driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
			//Log.message("delete button is clicked");
			
		}

	
	
	  //Get The Current Day
    private String getCurrentDay (){
        //Create a Calendar Object
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
 
        //Get Current Day as a number
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
      Log.message("Today Int: " + todayInt +"\n");
 
        //Integer to String Conversion
        String todayStr = Integer.toString(todayInt);
        Log.message("Today Str: " + todayStr + "\n");
 
        return todayStr;
    }
	@FindBy(xpath=".//*[@name='Delivery']")
	WebElement dropdownfordelivery;
	@FindBy(xpath=".//*[@id='duedate']/a")
	WebElement duedatedropdown;
	@FindBy(xpath=".//*[@id='dailoge']/div[2]/span[2]/select")
	WebElement timedropdown;
	@FindBy(xpath=".//*[@id='dailoge']/div[3]/button")
	WebElement donebtn;
	@FindBy(xpath=".//*[@id='setinstruction']/textarea")
	WebElement instructionarea;
	
	/**Method to fill details and confirm order
	 * 
	 */
	public void fillDetailsAndConfirmOrder( ) {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(".//*[@name='Delivery']")).click();
		Log.message("dropdown is clicked");
		SkySiteUtils.waitTill(3000);
	    Select sc=new Select(dropdownfordelivery);
		sc.selectByValue("7");
        Log.message("Delivery option is selected");	
        SkySiteUtils.waitTill(3000);
        duedatedropdown.click();
        Log.message("dropdown for date time is clicked");
        String today = getCurrentDay();
        Log.message("Today's number: " + today + "\n");
        //Body of calender is handled
        WebElement dateWidgetFrom = driver.findElement(By.xpath(".//*[@id='dailoge']/div[1]/div[2]"));
        
        List<WebElement> columns = dateWidgetFrom.findElements(By.tagName("td"));
 
        //DatePicker is a table. Thus we can navigate to each cell
        //and if a cell matches with the current date then we will click it.
        for (WebElement cell: columns) {
            /*
            //If you want to click 18th Date
            if (cell.getText().equals("18")) {
            */
            //Select Today's Date
            if (cell.getText().equals(today)) {
                cell.click();
                Log.message("Today day is selected");
                break;
            }
        }
        Log.message("date has been selected in calendar");
        SkySiteUtils.waitForElement(driver, timedropdown,60);
        timedropdown.click();
        Log.message("time dropdown is clicked");
        SkySiteUtils.waitTill(4000);
        driver.findElement(By.xpath(".//*[@id='dailoge']/div[2]/span[2]/select/option[9]")).click();
        Log.message("option 9 is selected in time dropdown");
        SkySiteUtils.waitForElement(driver,donebtn, 50);
        donebtn.click();
        Log.message("done button is clicked");
        instructionarea.sendKeys("Testing functionality ignore");
        Log.message("Text has been entered in testing production");
        SkySiteUtils.waitTill(5000);
        driver.findElement(By.xpath("//*[text()='Confirm order']")).click();
        Log.message("confirm order has been clicked");
	}
	@FindBy(xpath="//*[@id='user-info']")
	WebElement myprofilebtn;
	@FindBy(xpath=".//*[@id='limyorder']/a")
	WebElement myorderoption;
	
	/**Method to validate the order placed
	 * 
	 */
	public boolean validateOrderPlaced() {
		Log.message("!!!!  Validating the order placed with order id !!!!!!! " );
		SkySiteUtils.waitTill(5000);
		String orderid=driver.findElement(By.id("ordid")).getText();
        Log.message("Order id after placing order is " +orderid);	
        driver.switchTo().defaultContent();
        Log.message("Swtiched to default content");
        SkySiteUtils.waitTill(3000);
        myprofilebtn.click();
        Log.message("my profile button is clicked");
        SkySiteUtils.waitTill(3000);
        SkySiteUtils.waitForElement(driver, myorderoption, 40);
        myorderoption.click();
        Log.message("my order option is clicked");
        SkySiteUtils.waitTill(5000);
        driver.switchTo().defaultContent();
        Log.message("switched to default content");
        
        driver.switchTo().frame(driver.findElement(By.id("myFrame")));
        Log.message("Switched to frame");
        int nooforderpresent=driver.findElements(By.xpath(".//*[@id='aspnetForm']/div[3]/div[2]/app-root/ng-component/div/div/div/div/div/div/div/div[1]/div/div[1]/i")).size();
        Log.message("No of orders present in list is " +nooforderpresent);
        SkySiteUtils.waitTill(2000);
   
        int i=0;
        for(i=1; i<=nooforderpresent; i++) {
            SkySiteUtils.waitTill(2000);
        	String orderidinmyoders=driver.findElement(By.xpath("(.//*[@id='aspnetForm']/div[3]/div[2]/app-root/ng-component/div/div/div/div/div/div/div/div[1]/div/div[2]/h4/strong)["+i+"]")).getText();
	          if(orderidinmyoders.contains(orderid))
	        	  Log.message("Order is placed successfully  " +orderidinmyoders+ "   Is the order id " );
	              break;
        }
	
        String orderidinmyoders2=driver.findElement(By.xpath("(.//*[@id='aspnetForm']/div[3]/div[2]/app-root/ng-component/div/div/div/div/div/div/div/div[1]/div/div[2]/h4/strong)["+i+"]")).getText();
		if(orderidinmyoders2.contains(orderid)) {
			Log.message("Order is placed successfully and placed in my order list");
		    return true;
		}
		    else {
		    	Log.message("Order is not placed and not present in my orders");
		    	return false;
		
		    }
		}
	/**Click print cart tab
	 * 
	 */
	public void ClickPrintCartTab() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, printcarttab, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", printcarttab);
		Log.message("print cart tab is clicked");
	}
	}

