package com.arc.fna.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.arc.fna.utils.SkySiteUtils;

public class CollectionsPage extends LoadableComponent<CollectionsPage> {

	protected void load() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver expDriver) {
				return expDriver.findElement(By.id("txtSearchValue")).isDisplayed();
		    }
		});
	}

	@Override
	protected void isLoaded() throws Error {}
	
	WebDriver driver;
	
	public CollectionsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(how=How.ID, using="myFrame")
	WebElement iFrameModule;
	
	@FindBy(how=How.XPATH, using="//div[@class='objbox']/table/tbody")
	WebElement collectionsContainer;
	
	public DocumentsPage openCollection(String collectionName) {
		SkySiteUtils.waitForElement(driver, collectionsContainer, 40);
		List<WebElement> collectionsList = collectionsContainer.findElements(By.xpath("./tr"));
		for (int i = 1; i < collectionsList.size(); i++) {
			WebElement collectionLink = collectionsList.get(i).findElement(By.xpath("./td[3]/a"));
			if (collectionLink.getText().equals(collectionName)) {
				collectionLink.click();
				break;
			}
		}
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(10);
		SkySiteUtils.waitForElement(driver, iFrameModule, 40);
		driver.switchTo().frame(iFrameModule);
		SkySiteUtils.waitTill(10);
		return new DocumentsPage(driver).get();
	}
	
}