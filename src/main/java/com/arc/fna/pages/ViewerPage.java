package com.arc.fna.pages;

import com.arc.fna.utils.SkySiteUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import java.awt.*;
import java.util.List;

public class ViewerPage extends LoadableComponent<ViewerPage> {

    protected void load() {}

    @Override
    protected void isLoaded() throws Error {}

    WebDriver driver;

    public ViewerPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how= How.XPATH, using="//li[@id='btnLoadTags']/a")
    WebElement tagsAccumulatorLink;

    public void clickOnTagsAccumulatorLink() {
        SkySiteUtils.waitForElement(driver, tagsAccumulatorLink, 40);
        tagsAccumulatorLink.click();
    }

    @FindBy(how= How.XPATH, using="//div[@id='divTagList']/ul/li/div/div[4]/button")
    WebElement tagAccumulatorListToggleButton;

    public void toggleVisibilityOfTheTag() {
        SkySiteUtils.waitForElement(driver, tagAccumulatorListToggleButton, 40);
        tagAccumulatorListToggleButton.click();
        try { Thread.sleep(2000); } catch (InterruptedException e) {}
        driver.findElement(By.xpath("//div[@id='imageViewerContainer']")).click();
        try { Thread.sleep(5000); } catch (InterruptedException e) {}
    }

    @FindBy(how= How.XPATH, using="//a[@class='leaflet-draw-draw-tag']")
    WebElement photoTaggingAnnotation;

    public void clickOnPhotoTaggingAnnotation() {
        SkySiteUtils.waitForElement(driver, photoTaggingAnnotation, 40);
        photoTaggingAnnotation.click();
    }

    //@FindBy(how= How.ID, using="choose-icon")
    @FindBy(how= How.XPATH, using="//div[@id='createTagDropdown']/button")
    WebElement photoTaggingSelectIconButton;

    public void clickOnPhotoTaggingSelectIconButton() {
        SkySiteUtils.waitForElement(driver, photoTaggingSelectIconButton, 40);
        photoTaggingSelectIconButton.click();
    }

    public String getSelectedPhotoTaggingIconText() {
        return photoTaggingSelectIconButton.findElement(By.xpath("./span/span[2]")).getText();
    }

    @FindBy(how= How.ID, using="tagIconSearchInput")
    WebElement photoTaggingIconDropdownSearch;

    public void typeInPhotoTaggingIconDropdownSearch(String searchString) {
        photoTaggingIconDropdownSearch.clear();
        photoTaggingIconDropdownSearch.sendKeys(searchString);
    }

    @FindBy(how= How.XPATH, using="//div[@class='dropdown-menu pull-right']/ul")
    WebElement photoTaggingIconDropdownContainer;

    public void selectIntendedPhotoTaggingIcon(String tagType) {
        typeInPhotoTaggingIconDropdownSearch(tagType);
        try { Thread.sleep(10000); } catch (InterruptedException e) {}
        photoTaggingIconDropdownContainer.findElement(By.xpath("./li/a")).click();
    }

    public boolean isPhotoTaggingTypePresentInList(String tagType, boolean onlyTag) {
        try { Thread.sleep(2000); } catch (InterruptedException e) {}
        List<WebElement> dropdownItems = photoTaggingIconDropdownContainer.findElements(By.xpath("./li"));
        if (dropdownItems.size() == 0) {
            return false;
        }
        else if (onlyTag) {
            if (dropdownItems.size() > 1) {
                return false;
            }
            else {
                return dropdownItems.get(0).findElement(By.xpath("./a")).getText().equals(tagType);
            }
        }
        else {
            boolean isPresent = false;
            for (int i = 0; i < dropdownItems.size(); i++) {
                WebElement itemLink = dropdownItems.get(i).findElement(By.xpath("./a"));
                if (itemLink.getText().equals(tagType)) {
                    isPresent = true;
                    break;
                }
            }
            return isPresent;
        }
    }

    @FindBy(how= How.XPATH, using="//perfect-scrollbar[@class='modal-body']/div/div/div[3]/textarea")
    WebElement photoTaggingDescriptionField;

    public void typeInPhotoTaggingDescriptionField(String description) {
        SkySiteUtils.waitForElement(driver, photoTaggingDescriptionField, 40);
        photoTaggingDescriptionField.sendKeys(description);
    }

    @FindBy(how= How.XPATH, using="//perfect-scrollbar[@class='modal-body']/div/div/div[4]/div/div[2]/div/button")
    WebElement equipmentAttachButton;

    public void clickOnEquipmentAttachButton() {
        SkySiteUtils.waitForElement(driver, equipmentAttachButton, 40);
        equipmentAttachButton.click();
        try { Thread.sleep(5000); } catch (InterruptedException e) {}
    }

    @FindBy(how= How.XPATH, using="//div[@class='add-equipment-modal']/div[2]/div[2]/ul")
    WebElement addEquipmentListContainer;

    @FindBy(how= How.ID, using="equipmentSearch")
    WebElement attachEquipmentSearch;

    public void typeInPhotoTaggingEquipmentSearch(String searchString) {
        attachEquipmentSearch.clear();
        attachEquipmentSearch.sendKeys(searchString);
    }

    public void selectIntendedEquipmentFromList(String equipment) {
        typeInPhotoTaggingEquipmentSearch(equipment);
        try { Thread.sleep(10000); } catch (InterruptedException e) {}
        addEquipmentListContainer.findElement(By.xpath("./li/div/div[2]/span")).click();
    }

    @FindBy(how= How.XPATH, using="//div[@class='add-equipment-modal']/div[3]/button")
    WebElement photoTaggingEquipmentDoneButton;

    public void clickOnPhotoTaggingEquipmentDoneButton() {
        SkySiteUtils.waitForElement(driver, photoTaggingEquipmentDoneButton, 40);
        photoTaggingEquipmentDoneButton.click();
        try { Thread.sleep(5000); } catch (InterruptedException e) {}
    }

    @FindBy(how= How.XPATH, using="//perfect-scrollbar[@class='modal-body']")
    WebElement photoTaggingModalScrollArea;

    @FindBy(how= How.XPATH, using="//perfect-scrollbar[@class='modal-body']/div/div/div[7]/textarea")
    WebElement photoTaggingNoteField;

    public void typeInPhotoTaggingNoteField(String note) throws AWTException {
        Robot robot = new Robot();
        robot.mouseWheel(5000);
        SkySiteUtils.waitForElement(driver, photoTaggingNoteField, 40);
        photoTaggingNoteField.sendKeys(note);
    }

    //@FindBy(how= How.XPATH, using="//div[@class='modal fade create-doc-tagging-modal document-tagging in']/div/div/div[2]/button[2]")
    //@FindBy(how= How.XPATH, using="//div[@class='create-doc-tagging-modal document-tagging']/div[2]/button[2]")
    @FindBy(how= How.XPATH, using="//div[@class='create-doc-tagging-modal document-tagging']/div[2]/button")
    WebElement photoTaggingCreateButton;

    public void clickOnPhotoTaggingCreateButton() {
        photoTaggingCreateButton.click();
        try { Thread.sleep(5000); } catch (InterruptedException e) {}
    }

     //@FindBy(how= How.XPATH, using="//div[@class='modal fade create-doc-tagging-modal document-tagging in']/div/div/div[2]/button")
    @FindBy(how= How.XPATH, using="//div[@class='create-doc-tagging-modal document-tagging']/div[2]/button")
    WebElement photoTaggingDeleteButton;

    public void clickOnPhotoTaggingDeleteButton() {
        SkySiteUtils.waitForElement(driver, photoTaggingDeleteButton, 40);
        photoTaggingDeleteButton.click();
    }

    @FindBy(how= How.XPATH, using="//div[@id='confirmation-box']/div/button[2]")
    WebElement photoTaggingDeleteYesButton;

    public void clickOnPhotoTaggingDeleteYesButton() {
        SkySiteUtils.waitForElement(driver, photoTaggingDeleteYesButton, 40);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", photoTaggingDeleteYesButton);
        try { Thread.sleep(5000); } catch (InterruptedException e) {}
    }

}