package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class CollectionPackagePage extends LoadableComponent<CollectionPackagePage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public CollectionPackagePage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	/**
	 * Method written for Random Name
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_PackageName()
	{
		String str = "Package";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	@FindBy(css = "#ProjectMenu1_ancFileMenuDownIcon")
	WebElement btnFileMenuDownIcon;
	
	@FindBy(css = ".icon.icon-doc-package")
	WebElement btnicondocpackage;
	
	@FindBy(css = "#btnLinkPackage")
	WebElement btnLinkPackage;
	
	@FindBy(css = "#btnNewPackage")
	WebElement btnNewPackage;
	
	@FindBy(css = "#btnSavePackage")
	WebElement btnSavePackage;
	
	@FindBy(css = "#txtPackName")
	WebElement txtPackName;
	
	/** 
     * Method written for create Package Within Packages
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean AddPackage_Within_Packages(String PackageName)
    {    	   	 
    	SkySiteUtils.waitTill(3000);    	
    	driver.switchTo().defaultContent();
    	btnFileMenuDownIcon.click();
    	Log.message("Filesmenu drop down button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnicondocpackage.click();
    	Log.message("Packages button has been clicked");
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(3000);
    	btnNewPackage.click();
    	Log.message("Add button has been clicked");
    	SkySiteUtils.waitTill(3000);    	
    	txtPackName.sendKeys(PackageName);
    	Log.message("Package name has been entered");
    	SkySiteUtils.waitTill(3000);
    	btnSavePackage.click();
    	Log.message("save & close button has been clicked");
    	SkySiteUtils.waitTill(8000);
    	String Package_name= driver.findElement(By.xpath("//*[@id='divGridPackagesList']/div[2]/table/tbody/tr[2]/td[3]/a")).getText();
    	Log.message("Package Name is:"+Package_name);
    	SkySiteUtils.waitTill(3000);
    	if(Package_name.contentEquals(PackageName))
    		return true;
    	else 
    		return false;	    	
    }
    
    
    @FindBy(css = "#txtNote")
	WebElement txtNote;
    
    @FindBy(css = "#btnAddProjectDocuments")
   	WebElement btnAddProjectDocuments;
    
    @FindBy(css = ".ev_material>td>img")
   	WebElement btnchkimg;
    
    @FindBy(css = "#btnPopAdd")
   	WebElement btnPopAdd;
    
    @FindBy(css = "#btnPopClose")
   	WebElement btnPopClose;
    
    
	
	/** 
     * Method written for Add attachment from Collection in packages
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Add_Attachment_FromCollection(String PackageName,String Description)
    {    	   	 
    	SkySiteUtils.waitTill(3000);    	
    	driver.switchTo().defaultContent();
    	btnFileMenuDownIcon.click();
    	Log.message("Filesmenu drop down button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnicondocpackage.click();
    	Log.message("Packages button has been clicked");
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(3000);
    	btnNewPackage.click();
    	Log.message("Add button has been clicked");
    	SkySiteUtils.waitTill(3000);    	
    	txtPackName.sendKeys(PackageName);
    	Log.message("Package name has been entered");
    	SkySiteUtils.waitTill(3000);
    	txtNote.sendKeys(Description);
    	Log.message("note has been entered");
    	SkySiteUtils.waitTill(3000);    
    	btnAddProjectDocuments.click();
    	Log.message("Add collection file button has been clicked");
    	SkySiteUtils.waitTill(3000);  
    	driver.switchTo().defaultContent();
    	btnchkimg.click();
    	Log.message("file check box button has been clicked");
    	SkySiteUtils.waitTill(3000); 
    	btnPopAdd.click();
    	Log.message("Add button has been clicked");
    	SkySiteUtils.waitTill(3000); 
    	btnPopClose.click();
    	Log.message("close button has been clicked");
    	SkySiteUtils.waitTill(5000); 
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(3000);
    	if(driver.findElement(By.xpath("//*[@id='divGridPackageItems']/div[2]/table/tbody/tr[2]/td[2]/a[2]")).isDisplayed())
    		return true;
    	else 
    		return false;	    	
    }
    
   
    @FindBy(css = "#chkSelItemsGridAll")
   	WebElement chkSelItemsGridAll;
    
    @FindBy(css = ".btn.btn-primary.PopRemove")
   	WebElement btnprimaryPopRemove;
    
    @FindBy(xpath = "//*[@id='divSourceItems']/div[2]/table/tbody/tr[2]/td[2]")
   	WebElement btnfilename;
    
    @FindBy(css = "#btnPopClose")
   	WebElement btnPopClo;
    
    /** 
     * Method written for Add attachment and Edit remove collection files in packages
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Add_Attachment_Edit_remove_collectionfiles()
    {  
    	boolean result1=false;
    	btnAddProjectDocuments.click();
    	Log.message("modify button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();    
    	chkSelItemsGridAll.click();
    	Log.message("all check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnprimaryPopRemove.click();
    	Log.message("Remove button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	if(btnfilename.isDisplayed())
    	{
    		result1=true;
    		Log.message("remove file from selected collectionfile successfully");
    	}
    	else
    	{
    		result1=false;
    		Log.message("remove file from selected collectionfile Unsuccessfully");
    	}
    	SkySiteUtils.waitTill(3000);
    	btnPopClo.click();
    	Log.message("close button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(3000);
    	if((driver.findElement(By.xpath("(//i[@class='icon icon-plus ico-lg'])[2]")).isDisplayed())&&(result1==true))
    		return true;
    	else
    		return false;
    }
    
}