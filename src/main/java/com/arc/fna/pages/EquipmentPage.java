package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Desktop.Action;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;











import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class EquipmentPage extends LoadableComponent<EquipmentPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	FnaHomePage fnaHomePage;
	
	//testing sekhar
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;
	
	@FindBy(xpath = "//*[@id='user-info']")
	WebElement btnuserinfo;
	
	@FindBy(xpath = "//*[@id='user-info']")
	WebElement clkprofile;
	
	@FindBy(xpath = "//i[@class='icon icon-logout icon-lg']")
	WebElement pfloutput;
	
	@FindBy(xpath = "//*[@id='button-1']")
	WebElement btnyes;
	
	@FindBy(xpath="//*[@placeholder='Search']")
	WebElement txtBoxSearchEquipment;
	

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error
	{
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public EquipmentPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
		
	@FindBy(css = "#ProjectMenu1_liEquipment>a")
	WebElement liEquipment;
	
	@FindBy(xpath = "(//div/div/button[2])[1]")
	WebElement btnadd;
	
	
	/** 
     * Method written for Equipment Management launch in FNA.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Equipment_Management_launch()
    {
    	 SkySiteUtils.waitTill(5000);
    	 liEquipment.click();
    	 Log.message("Equipment button has been clicked");
    	 SkySiteUtils.waitTill(10000);
    	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	 Log.message("Switch to My frame");
         SkySiteUtils.waitTill(5000);
    	 if(btnadd.isDisplayed())
    	 {	 
    		 Log.message("Equipment Management required button displayed");
    		 return true;
    	 }
    	 else
    	 {
    		 Log.message("Equipment Management required button not displayed");
    		 return false;   
    	 }
    }    
    /**
	 * Method written for Random Name
	 *  Scripted By: Sekhar	
	 * @return
	 */
	public String Random_Name() 
	{
		String str = "Name";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}	
	/**
	 * Method written for Random EditName
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_EditName() 
	{
		String str = "EditName";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}		
	/**
	 * Method written for Random TagName
	 *  Scripted By: Sekhar		
	 * @return
	 */

	public String Random_TagName() 
	{
		String str = "TagName";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}	
	/**
	 * Method written for Random Serial
	 *  Scripted By: Sekhar	  
	 * @return
	 */

	public String Random_SerialNumber() 
	{
		String str = "Serial";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}	
	/**
	 * Method written for Random Description
	 *  Scripted By: Sekhar
	 * @return
	 */
	public String Random_Description() 
	{
		String str = "Description";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Building
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Building() 
	{
		String str = "Building";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}	
	/**
	 * Method written for Random Floor
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Floor() 
	{
		String str = "Floor";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	/**
	 * Method written for Random Area
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Area() 
	{
		String str = "Area";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Model
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Model() 
	{
		String str = "Model";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
    
	/**
	 * Method written for Random Manufacture
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Manufacture() 
	{
		String str = "Manufacture";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Random Parts
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Parts() 
	{
		String str = "Parts";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
    
    @FindBy(xpath = "//input[@formcontrolname='AssetName']")
	//@FindBy(css = ".form-control.ng-untouched.ng-pristine.ng-invalid")
	WebElement txtname;
	
    
    @FindBy(xpath = "//input[@aria-label='Serial number' and @formcontrolname='SerialNumber']")
   	WebElement txtSerialnumber;
    
    @FindBy(xpath = "//input[@formcontrolname='Building']")
   	WebElement txtBuilding;
    
    @FindBy(xpath = "//input[@formcontrolname='Area']")
   	WebElement txtArea;
    
    @FindBy(xpath = "//input[@formcontrolname='Floor']")
   	WebElement txtFloor;
    
    @FindBy(xpath = "//button[@class='btn asset-btn-primary' and @type='button']")
   	WebElement btnsave;
    
    //@FindBy(css = ".asset-list-box-content>a>h4")
    @FindBy(xpath = "//div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")
   	WebElement Expname;
    
    @FindBy(xpath = "//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[1]/span")
   	WebElement txtserialnum;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[2]/div[1]/div/span[2]/div")
   	WebElement ActName;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[2]/div[1]/div/span[2]/input")
   	WebElement ActName1;    
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div[1]/span[2]/div")
   	WebElement ActSerialNum;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div[1]/span[2]/input")
   	WebElement ActSerialNum1;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[2]/div/div[1]/div[1]/div/span[2]/input")
   	WebElement ActBuilding;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[2]/div/div[1]/div[2]/div/span[2]/input")
   	WebElement ActFoolr;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[2]/div/div[1]/div[3]/div/span[2]/input")
   	WebElement ActArea;
    
    /** 
     * Method written for addition of an equipment with all the required fields.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Equipment_addition_requiredfields(String Name,String Serialnumber,String Building) throws Exception
    {    
    	boolean result1=false;
    	 SkySiteUtils.waitTill(5000);
    	 btnadd.click();
    	 Log.message("Add button has been clicked");
    	 SkySiteUtils.waitTill(20000);    	 
    	//driver.findElement(By.xpath("//*[@id='mCSB_2_container']/form/div[2]/div/button")).click();
    	 driver.findElement(By.xpath(".//*[@id='mCSB_2_container']/div/form/div[2]/div[1]/button")).click();
    	 Log.message("Add photo has been clicked");
    	 SkySiteUtils.waitTill(5000);    	 
    	 WebElement element = driver.findElement(By.xpath("//div[3]/div[1]/div[1]/div/div/input"));
    	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    	 SkySiteUtils.waitTill(5000);   
    	 Log.message("Exp element has been selected");    		 		 
 		 txtname.clear();
 		 Log.message("name has been cleared");
 		 SkySiteUtils.waitTill(5000); 
 		 txtname.sendKeys(Name);
 		 Log.message("name has been entered"+Name);
 		 SkySiteUtils.waitTill(5000);   		 
 		 WebElement element1 = driver.findElement(By.xpath("//input[@aria-label='Serial number' and @formcontrolname='SerialNumber']"));
    	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
    	 SkySiteUtils.waitTill(5000);
    	 Log.message("Exp element has been selected");
    	 txtSerialnumber.sendKeys(Serialnumber);
    	 Log.message("Serial number has been Entered"+Serialnumber);
    	 SkySiteUtils.waitTill(5000);    	
    	 txtBuilding.sendKeys(Building);
    	 Log.message("Building has been Entered:"+Building);    	
    	 SkySiteUtils.waitTill(5000);   
    	 txtArea.sendKeys("Testing");
    	 Log.message("Area has been Entered");
    	 SkySiteUtils.waitTill(5000);     	
    	 txtFloor.sendKeys("7th floor");
    	 Log.message("Floor has been Entered");
    	 SkySiteUtils.waitTill(5000);
    	 btnsave.click();
    	 Log.message("Save button has been clicked");
    	 SkySiteUtils.waitTill(15000); 		
    	 String name = Expname.getText();
    	 Log.message("Exp name is:"+name);
    	 SkySiteUtils.waitTill(2000);
    	 String Serial_num = txtserialnum.getText();
    	 Log.message("Exp serial num is:"+Serial_num);
    	 SkySiteUtils.waitTill(5000);    	 
    	 if((name.equals(Name))&&(Serial_num.equals(Serialnumber)))
    	 {
    		 result1 =true;
    		 Log.message("Equipment required fields addition validation successfully"); 
    		 SkySiteUtils.waitTill(5000);
    		 Expname.click();
    		 Log.message("Recently Equipment button has been clicked");
        	 SkySiteUtils.waitTill(10000);     		 
    	 }
    	 else
    	 {
    		 result1 =false;
    		 Log.message("Equipment required fields addition validation Failed");     	
    	 } 
    	 SkySiteUtils.waitTill(5000);
    	 String Exp_Name = ActName.getText();
    	 Log.message("Exp Name is:"+Exp_Name);
    	 SkySiteUtils.waitTill(3000);
    	 String Exp_SerialNum = ActSerialNum.getText();
    	 Log.message("Exp Serial Num is:"+Exp_SerialNum);
    	 SkySiteUtils.waitTill(3000);    	 
    	 String Exp_Building = ActBuilding.getAttribute("value");
    	 Log.message("Exp Building is:"+Exp_Building);
    	 SkySiteUtils.waitTill(3000);
    	 String Exp_Floor = ActFoolr.getAttribute("value");
    	 Log.message("Exp Floor is:"+Exp_Floor);
    	 SkySiteUtils.waitTill(3000);
    	 String Exp_Area = ActArea.getAttribute("value");
    	 Log.message("Exp Area is:"+Exp_Area);
    	 SkySiteUtils.waitTill(5000);    	 
    	 if((Exp_Name.equals(Name))&&(Exp_SerialNum.equals(Serialnumber))&&(Exp_Building.equals(Building))
    			 &&(Exp_Floor.equals("7th floor")) &&(Exp_Area.equals("Testing")))
    		 return true;
    	 else
    		 return false;
    }    
    
   @FindBy(xpath = "(//i[@class='icon icon-am-plus asset-fa-sm'])[5]")
   WebElement btnadddetails;
   
   @FindBy(css = ".form-control.ng-untouched.ng-pristine.ng-valid")
   WebElement addTags;
   
   @FindBy(css = ".btn.asset-btn-primary")
   WebElement btnsaveaddtag;
   
   @FindBy(xpath = "(//div[2]/div/div[2]/div/div[5]/div[2]/ul/li)[4]")
   WebElement btnsavetag;
   
    /** 
     * Method written for Add Tag in Equipment details.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Equipment_Add_Tag(String TagName) throws AWTException
    {
    	 SkySiteUtils.waitTill(5000);    	
    	 btnadddetails.click();
    	 Log.message("Tag add button has been clicked");
    	 SkySiteUtils.waitTill(5000);
    	 addTags.sendKeys(TagName);
    	 Log.message("Add Tag has been Entered:"+TagName);
    	 SkySiteUtils.waitTill(5000);    	
    	 btnsaveaddtag.click();
    	 Log.message("save button has been clicked");
    	 SkySiteUtils.waitTill(5000);
    	 String tagname= btnsavetag.getText();
    	 Log.message("Tag name is:"+tagname);
    	 SkySiteUtils.waitTill(3000);
    	 if(tagname.contains(TagName))
    	 {
    		 Log.message("Add Tag validation in Equipment details Successfully");
    		 return true;
    	 }
    	 else
    	 {
    		 Log.message("Add Tag validation in Equipment details Failed");
    		 return false; 
    	 }
    }
    
    @FindBy(xpath = "//input[@class='form-control ng-untouched ng-pristine ng-valid' and @type='text']")
    WebElement btnmodulesearch;
    
    @FindBy(xpath = "//div/div[1]/h2")
    WebElement txtsearchresult;
    
    /** 
     * Method written for Module Search With TagName in Equipment.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Equipment_ModuleSearch_WithTagName(String TagName) throws AWTException
    {
    	boolean result1=false;
    	SkySiteUtils.waitTill(5000);    	
    	driver.switchTo().defaultContent();
    	liEquipment.click();
    	Log.message("Equipment button has been clicked");
    	SkySiteUtils.waitTill(25000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to My frame");
    	btnmodulesearch.sendKeys(TagName);
    	Log.message("tag name has been entered:"+TagName);
    	SkySiteUtils.waitTill(5000);    	
    	Actions action = new Actions(driver); 
    	action.sendKeys(Keys.ENTER).build().perform();
    	SkySiteUtils.waitTill(15000);
    	Log.message("Enter action performed");
    	String searchresult= txtsearchresult.getText();
    	Log.message("search result is:"+searchresult);	
    	SkySiteUtils.waitTill(5000);
    	if(driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).isDisplayed())
    	{    
    		result1=true;
    		Log.message("module search with tag name successfully");    		
    	}	
    	else
    	{
    		result1=false;
    		Log.message("module search with tag name got Failed");    	
    	}	
    	SkySiteUtils.waitTill(5000);
    	driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
    	Log.message("Exp Equipment has been clicked");
    	SkySiteUtils.waitTill(10000);
    	String Tagname =driver.findElement(By.xpath("(//div[2]/div/div[2]/div/div[5]/div[2]/ul/li)[4]")).getText();
    	Log.message("Exp Tag Name is:"+Tagname);
    	SkySiteUtils.waitTill(5000);
    	if((result1==true)&&(TagName.equals(Tagname)))
    		return true;
    	else
    		return false;
    }
    
    @FindBy(xpath = "//div/div[2]/div[1]/div/div/div/div[2]/a/h4")
    WebElement txtEquipmentname;
    /** 
     * Method written for user can delete equipment from Recently Viewed Equipment list.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Delete_Equipment_Recently_ViewedEquipmentlist(String Name) throws AWTException
    {    	
    	SkySiteUtils.waitTill(5000);    	
    	driver.switchTo().defaultContent();
    	liEquipment.click();
    	Log.message("Equipment button has been clicked");
    	SkySiteUtils.waitTill(25000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to My frame");    	
    	int Equipment = driver.findElements(By.xpath("//div/div[2]/div[1]/div/div/div/button[2]")).size();
		Log.message("Before delete Equipment Count is:" + Equipment);
		int i = 0;
		for (i = 1; i <= Equipment; i++) 
		{			
			String Exp_Equipment_Name = driver.findElement(By.xpath("(//div/div[2]/div[1]/div/div/div/div[2]/a/h4)[" +i+ "]")).getText();
			Log.message("Exp Equipment Name is:" + Exp_Equipment_Name);
			SkySiteUtils.waitTill(5000);			
			if (Exp_Equipment_Name.contains(Name))
			{
				Log.message("Exp Equipment name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//div/div[2]/div[1]/div/div/div/button[2])["+i+"]")).click();
		Log.message("delete icon Equipment has been clicked");
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("//button[@id='yes']")).click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(8000);
		int Equipment1 = driver.findElements(By.xpath("//div/div[2]/div[1]/div/div/div/button[2]")).size();
		Log.message("After delete Equipment Count is:" + Equipment1);
		SkySiteUtils.waitTill(5000);
		if(Equipment1==(Equipment-1)) 	
			return true;
		else
			return false;
    	
    }
    
    /** 
     * Method written for user can delete equipment from Equipment details page.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Delete_Equipment_detailspage(String Name) throws AWTException
    {    	
    	SkySiteUtils.waitTill(5000);    	
    	driver.switchTo().defaultContent();
    	liEquipment.click();
    	Log.message("Equipment button has been clicked");
    	SkySiteUtils.waitTill(30000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to My frame");
    	
    	int Equipment = driver.findElements(By.xpath("//div/div[2]/div[1]/div/div/div/button[2]")).size();
		Log.message("Before delete Equipment Count is:" + Equipment);
		int i = 0;
		for (i = 1; i <= Equipment; i++) 
		{			
			String Exp_Equipment_Name = driver.findElement(By.xpath("(//div/div[2]/div[1]/div/div/div/div[2]/a/h4)[" +i+ "]")).getText();
			Log.message("Exp Equipment Name is:" + Exp_Equipment_Name);
			SkySiteUtils.waitTill(5000);			
			if (Exp_Equipment_Name.contains(Name))
			{
				Log.message("Exp Equipment name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//div/div[2]/div[1]/div/div/div/div[2]/a/h4)["+i+"]")).click();
		Log.message("Equipment has been clicked");
		SkySiteUtils.waitTill(10000);	
		driver.findElement(By.xpath("//div[1]/div[1]/div/ul/li[2]/button")).click();
		Log.message("Remove button has been clicked");
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("//button[@id='yes']")).click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(8000);
		int Equipment1 = driver.findElements(By.xpath("//div/div[2]/div[1]/div/div/div/button[2]")).size();
		Log.message("After delete Equipment Count is:" + Equipment1);
		SkySiteUtils.waitTill(5000);
		if(Equipment1==(Equipment-1)) 	
			return true;
		else
			return false;
    	
    }
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button")
    WebElement btnEdit;
    
    @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button")
    WebElement btneqdesave;
    
    /** 
     * Method written for user can Edit Equipment name from Equipment details page.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Edit_Equipmentname_Equipment_detailspage(String EditName) throws AWTException
    {    	
    	SkySiteUtils.waitTill(5000);    	
    	btnEdit.click();
    	Log.message("Edit button has been clicked");
		SkySiteUtils.waitTill(8000);
		ActName1.clear();
		Log.message("name has been cleared");
		SkySiteUtils.waitTill(5000);
		ActName1.sendKeys(EditName);
		Log.message("name has been entered");
		SkySiteUtils.waitTill(5000);
		btneqdesave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(10000);		
		String Editna = ActName.getText();
		Log.message("Edit name is:"+Editna);
		SkySiteUtils.waitTill(5000);
		if(Editna.contains(EditName))
			return true;
		else
			return false;
    }
    
    
    /** 
     * Method written for user can Filter Search with Equipment name.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
 public boolean Filter_Search_Equipmentname(String EditName) throws AWTException
 {  
	 boolean result1=false;
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(10000);
	 btnEdit.click();
 	 Log.message("Edit button has been clicked");
 	 SkySiteUtils.waitTill(8000);
 	 ActName1.clear();
 	 Log.message("name has been cleared");
 	 SkySiteUtils.waitTill(5000);
 	 ActName1.sendKeys(EditName);
 	 Log.message("name has been entered");
 	 SkySiteUtils.waitTill(5000);
 	 btneqdesave.click();
 	 Log.message("save button has been clicked");
 	 SkySiteUtils.waitTill(15000);		
 	 String Editna = ActName.getText();
 	 Log.message("Edit name is:"+Editna);
 	 SkySiteUtils.waitTill(5000);
 	 if(Editna.contains(EditName))
 	 {
 		 result1=true;
 		 Log.message("Equipment edited successfully!!!");
 	 }
 	 else
 	 {
 		result1=false;
 		Log.message("Equipment edited Failed!!!");
 	 }
 	 SkySiteUtils.waitTill(5000);
 	 driver.switchTo().defaultContent();
 	 liEquipment.click();
 	 Log.message("Equipment button has been clicked");
 	 SkySiteUtils.waitTill(25000);
 	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 	 Log.message("Switch to My frame");
 	 btnmodulesearch.sendKeys(EditName);
 	 Log.message("name has been entered");
 	 SkySiteUtils.waitTill(5000);
 	 Actions action = new Actions(driver); 
 	 action.sendKeys(Keys.ENTER).build().perform();
 	 SkySiteUtils.waitTill(15000);
 	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
 	 Log.message("Filter icon has been clicked");
 	 SkySiteUtils.waitTill(5000);
 	 driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click();
 	 Log.message("Equipment name chech box  has been clicked");
 	 SkySiteUtils.waitTill(10000);
 	 String Equipmentname= txtEquipmentname.getText();
 	 Log.message("Equipment name is:"+Equipmentname);
 	 if((Equipmentname.contains(EditName))&&(result1==true))
 		 return true;
 	 else
 		 return false; 	 
 }
 
 @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div[1]/span[2]/div")
 WebElement ActSerial;
 @FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div[1]/span[2]/input")
 WebElement ActSerial1;
 /** 
  * Method written for Equipment is searchable using Serial Number(Filter Search).
  * Scripted By: Sekhar     
  * @throws AWTException 
  */
public boolean Filter_Search_SerialNumber(String SerialNum) throws AWTException
{  
	 boolean result1=false;
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnEdit.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 ActSerial1.clear();
	 Log.message("Serial num has been cleared");
	 SkySiteUtils.waitTill(5000);
	 ActSerial1.sendKeys(SerialNum);
	 Log.message("SerialNum has been entered");
	 SkySiteUtils.waitTill(15000);	 
     WebElement element = driver.findElement(By.xpath("//div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button"));
     JavascriptExecutor executor = (JavascriptExecutor)driver;
     executor.executeScript("arguments[0].click();", element);     	
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(15000);		
	 String Serialnumber = ActSerial.getText();
	 Log.message("Serial num is:"+Serialnumber);
	 SkySiteUtils.waitTill(5000);
	 if(Serialnumber.contains(SerialNum))
	 {
		 result1=true;
		 Log.message("Serial Num edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Serial Num edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(SerialNum);
	 Log.message("name has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click();
	 Log.message("Serial numb chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);
	 String Serial_Numb= driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[1]/span")).getText();
	 Log.message("Seriel numb is:"+Serial_Numb);
	 if((Serial_Numb.contains(SerialNum))&&(result1==true))
		 return true;
	 else
		 return false;
}

@FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[2]/div[3]/div/span[2]/input")
WebElement ActDescri;

@FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[2]/div[3]/div/span[2]/div")
WebElement ActdivDescri;


/** 
 * Method written for Equipment is searchable using Description.(Filter Search).
 * Scripted By: Sekhar     
 * @throws AWTException 
 */
public boolean Filter_Search_Description(String Description) throws AWTException
{  
	 boolean result1=false;
	 boolean result2=false;
	 boolean result3=false;
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnEdit.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 ActDescri.clear();
	 Log.message("Description has been cleared");
	 SkySiteUtils.waitTill(5000);
	 ActDescri.sendKeys(Description);
	 Log.message("Description has been entered");
	 SkySiteUtils.waitTill(15000);	 
	 WebElement element = driver.findElement(By.xpath("//div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button"));
	 JavascriptExecutor executor = (JavascriptExecutor)driver;
	 executor.executeScript("arguments[0].click();", element);     	
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(15000);		
	 String Exp_Description = ActdivDescri.getText();
	 Log.message("Exp Description is:"+Exp_Description);
	 SkySiteUtils.waitTill(5000);
	 if(Description.contains(Exp_Description))
	 {
		 result1=true;
		 Log.message("Description edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Description edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Description);
	 Log.message("Description has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[3]")).click();
	 Log.message("Description chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);
	 if(driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).isDisplayed())
	 {
		 result2=true;
		 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
		 Log.message("Exp Equipment has been clicked");
		 SkySiteUtils.waitTill(15000);
		 String Aftersearch_Description = ActdivDescri.getText();
		 Log.message("After search Exp Description is:"+Aftersearch_Description);
		 if(Description.contains(Aftersearch_Description))
		 {
			 result3=true;
			 Log.message("After search Equipment Description Successfully");
		 }
		 else
		 {
			 result3=false;
			 Log.message("After search Equipment Description Failed");
		 }
	 }
	 else
	 {
		 result2=false; 
		 Log.message("Exp Equipment has been not displayed");
	 }
	if((result1==true)&&(result2==true)&&(result3==true))
		return true;
	else
		return false;
}

@FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[1]/div[1]/button")
WebElement btnedit2;

@FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[2]/div/div[1]/div[1]/div/span[2]/input")
WebElement txtbuild;

@FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[1]/div[1]/button")
WebElement btnsave2;

@FindBy(xpath = "//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[2]/span")
WebElement txtbuilding;


/** 
 * Method written for Equipment is searchable using Building.(Filter Search).
 * Scripted By: Sekhar     
 * @throws AWTException 
 */
public boolean Filter_Search_Building(String Building) throws AWTException
{  
	 boolean result1=false;	
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnedit2.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 txtbuild.clear();
	 Log.message("Building has been cleared");
	 SkySiteUtils.waitTill(5000);
	 txtbuild.sendKeys(Building);
	 Log.message("Building has been entered");
	 SkySiteUtils.waitTill(15000);	 
	 btnsave2.click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(15000);		
	 String Exp_Building = txtbuild.getAttribute("value");
	 Log.message("Exp Building is:"+Exp_Building);
	 SkySiteUtils.waitTill(5000);
	 if(Building.contains(Exp_Building))
	 {
		 result1=true;
		 Log.message("Building edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Building edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Building);
	 Log.message("Building has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[4]")).click();
	 Log.message("Building chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);	 
	 String Aftersearch_Building = txtbuilding.getText();
	 Log.message("After search Exp Building is:"+Aftersearch_Building);
	 if((Building.contains(Aftersearch_Building))&&(result1==true))
		 return true;
	 else
		 return false; 
}

@FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[2]/div/div[1]/div[2]/div/span[2]/input")
WebElement txtFloor11;

@FindBy(xpath = "//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[3]/span")
WebElement txtFloordiv;

/** 
 * Method written for Equipment is searchable using Floor.(Filter Search).
 * Scripted By: Sekhar     
 * @throws AWTException 
 */
public boolean Filter_Search_Floor(String Floor) throws AWTException
{  
	 boolean result1=false;	 
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnedit2.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 txtFloor11.clear();
	 Log.message("Floor has been cleared");
	 SkySiteUtils.waitTill(5000);
	 txtFloor11.sendKeys(Floor);
	 Log.message("Floor has been entered");
	 SkySiteUtils.waitTill(15000);	  
	 btnsave2.click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(15000);		
	 String Exp_Floor = txtFloor11.getAttribute("value");
	 Log.message("Exp Floor is:"+Exp_Floor);
	 SkySiteUtils.waitTill(5000);
	 if(Floor.contains(Exp_Floor))
	 {
		 result1=true;
		 Log.message("Floor edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Floor edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Floor);
	 Log.message("Floor has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 WebElement element1 = driver.findElement(By.xpath("(//input[@type='checkbox'])[5]"));
	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[5]")).click();
	 Log.message("Floor chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);	 
	 String Aftersearch_Floor = txtFloordiv.getText();
	 Log.message("After search Exp Floor is:"+Aftersearch_Floor);
	 if((Floor.contains(Aftersearch_Floor))&&(result1==true))
		 return true;
	 else
		 return false; 
}

@FindBy(xpath = "//div[2]/div/div[1]/div/div[3]/div[2]/div/div[1]/div[3]/div/span[2]/input")
WebElement txtarea11;

@FindBy(xpath = "//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[4]/span")
WebElement txtareadiv;

/** 
 * Method written for Equipment is searchable using Area.(Filter Search).
 * Scripted By: Sekhar     
 * @throws AWTException 
 */
public boolean Filter_Search_Area(String Area) throws AWTException
{  
	 boolean result1=false;	 
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnedit2.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 txtarea11.clear();
	 Log.message("Area has been cleared");
	 SkySiteUtils.waitTill(5000);
	 txtarea11.sendKeys(Area);
	 Log.message("Area has been entered");
	 SkySiteUtils.waitTill(15000);	  
	 btnsave2.click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(15000);		
	 String Exp_Area = txtarea11.getAttribute("value");
	 Log.message("Exp Area is:"+Exp_Area);
	 SkySiteUtils.waitTill(5000);
	 if(Area.contains(Exp_Area))
	 {
		 result1=true;
		 Log.message("Area edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Area edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Area);
	 Log.message("Area has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);	 
	 WebElement element1 = driver.findElement(By.xpath("(//input[@type='checkbox'])[6]"));
	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);	
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[6]")).click();
	 Log.message("Area chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);	 
	 String Aftersearch_Area = txtareadiv.getText();
	 Log.message("After search Exp Area is:"+Aftersearch_Area);
	 if((Area.contains(Aftersearch_Area))&&(result1==true))
		 return true;
	 else
		 return false; 
}

@FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div[2]/span[2]/div")
WebElement txtmodel;

@FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div[2]/span[2]/input")
WebElement txtmodel1;

/** 
 * Method written for Equipment is searchable using Model.(Filter Search).
 * Scripted By: Sekhar     
 * @throws AWTException 
 */
public boolean Filter_Search_Model(String Model) throws AWTException
{  
	 boolean result1=false;	 
	 boolean result2=false;	 
	 boolean result3=false;	 
	 
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("recently equipment has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnEdit.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 txtmodel1.clear();
	 Log.message("Model has been cleared");
	 SkySiteUtils.waitTill(5000);
	 txtmodel1.sendKeys(Model);
	 Log.message("Model has been entered");
	 SkySiteUtils.waitTill(15000);	  
	 WebElement element = driver.findElement(By.xpath("//div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button"));
	 JavascriptExecutor executor = (JavascriptExecutor)driver;
	 executor.executeScript("arguments[0].click();", element);     	
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 String Exp_Model = txtmodel.getText();
	 Log.message("Exp Model is:"+Exp_Model);
	 SkySiteUtils.waitTill(5000);
	 if(Model.contains(Exp_Model))
	 {
		 result1=true;
		 Log.message("Model edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Model edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Model);
	 Log.message("Model has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 WebElement element1 = driver.findElement(By.xpath("(//input[@type='checkbox'])[7]"));
	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[7]")).click();
	 Log.message("Model chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);		
	 if(driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).isDisplayed())
	 {
		 result2=true;
		 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
		 Log.message("Exp Equipment has been clicked");
		 SkySiteUtils.waitTill(15000);
		 String Aftersearch_Model = txtmodel.getText();
		 Log.message("After search Exp Model is:"+Aftersearch_Model);
		 if(Model.contains(Aftersearch_Model))
		 {
			 result3=true;
			 Log.message("After search Equipment Model Successfully");
		 }
		 else
		 {
			 result3=false;
			 Log.message("After search Equipment Model Failed");
		 }
	 }
	 else
	 {
		 result2=false; 
		 Log.message("Exp Equipment has been not displayed");
	 }
	if((result1==true)&&(result2==true)&&(result3==true))
		return true;
	else
		return false;
}

@FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div[1]/span[2]/div")
WebElement txtManufacture;

@FindBy(xpath = "//div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div[1]/span[2]/input")
WebElement txtManufacture1;

/** 
 * Method written for Equipment is searchable using Manufacture.(Filter Search).
 * Scripted By: Sekhar     
 * @throws AWTException 
 */
public boolean Filter_Search_Manufacture(String Manufacture) throws AWTException
{  
	 boolean result1=false;	 
	 boolean result2=false;	 
	 boolean result3=false;	 
	 
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 btnEdit.click();
	 Log.message("Edit button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 txtManufacture1.clear();
	 Log.message("Manufacture has been cleared");
	 SkySiteUtils.waitTill(5000);
	 txtManufacture1.sendKeys(Manufacture);
	 Log.message("Manufacture has been entered");
	 SkySiteUtils.waitTill(15000);	  
	 WebElement element = driver.findElement(By.xpath("//div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button"));
	 JavascriptExecutor executor = (JavascriptExecutor)driver;
	 executor.executeScript("arguments[0].click();", element);     	
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 String Exp_Manufacture = txtManufacture.getText();
	 Log.message("Exp Manufacture is:"+Exp_Manufacture);
	 SkySiteUtils.waitTill(5000);
	 if(Manufacture.contains(Exp_Manufacture))
	 {
		 result1=true;
		 Log.message("Manufacture edited successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Manufacture edited Failed!!!");
	 }
	 SkySiteUtils.waitTill(5000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Manufacture);
	 Log.message("Manufacture has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 WebElement element1 = driver.findElement(By.xpath("(//input[@type='checkbox'])[8]"));
	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[8]")).click();
	 Log.message("Model chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);		
	 if(driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).isDisplayed())
	 {
		 result2=true;
		 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
		 Log.message("Exp Equipment has been clicked");
		 SkySiteUtils.waitTill(15000);
		 String Aftersearch_Manufacture = txtManufacture.getText();
		 Log.message("After search Exp Manufacture is:"+Aftersearch_Manufacture);
		 if(Manufacture.contains(Aftersearch_Manufacture))
		 {
			 result3=true;
			 Log.message("After search Equipment Manufacture Successfully");
		 }
		 else
		 {
			 result3=false;
			 Log.message("After search Equipment Manufacture Failed");
		 }
	 }
	 else
	 {
		 result2=false; 
		 Log.message("Exp Equipment has been not displayed");
	 }
	if((result1==true)&&(result2==true)&&(result3==true))
		return true;
	else
		return false;
}

/** 
* Method written for Equipment is searchable using Parts.(Filter Search).
* Scripted By: Sekhar     
* @throws AWTException 
*/
public boolean Filter_Search_Parts(String Parts) throws AWTException
{  
	 boolean result1=false;	 
	 boolean result2=false;	 
	 boolean result3=false;	 

	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[3]/div[1]/div[1]/button")).click();
	 Log.message("Add part button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//div/div[1]/div/input")).sendKeys(Parts);
	 Log.message("Part name has been entered");
	 SkySiteUtils.waitTill(8000);
	 driver.findElement(By.xpath(".//*[@id='addPartsModal']/div[3]/button")).click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 String Exp_Parts = driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).getText();
	 Log.message("Exp Parts is:"+Exp_Parts);
	 SkySiteUtils.waitTill(5000);
	 if(Parts.contains(Exp_Parts))
	 {
		 result1=true;
		 Log.message("Parts added successfully!!!");
	 }
	 else
	 {
		result1=false;
		Log.message("Parts added Failed!!!");
	 }
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().defaultContent();
	 liEquipment.click();
	 Log.message("Equipment button has been clicked");
	 SkySiteUtils.waitTill(25000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 btnmodulesearch.sendKeys(Parts);
	 Log.message("Parts has been entered");
	 SkySiteUtils.waitTill(5000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-filter asset-fa-sm']")).click();
	 Log.message("Filter icon has been clicked");
	 SkySiteUtils.waitTill(5000);
	 WebElement element1 = driver.findElement(By.xpath("(//input[@type='checkbox'])[10]"));
	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
	 driver.findElement(By.xpath("(//input[@type='checkbox'])[10]")).click();
	 Log.message("Parts chech box  has been clicked");
	 SkySiteUtils.waitTill(10000);	 
	 if(driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[1]/span")).isDisplayed())
	 {
		 result2=true;
		 driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[1]/span")).click();
		 Log.message("Exp Equipment has been clicked");
		 SkySiteUtils.waitTill(15000);
		 String Aftersearch_Parts =  driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).getText();
		 Log.message("After search Exp Parts is:"+Aftersearch_Parts);
		 if(Parts.contains(Aftersearch_Parts))
		 {
			 result3=true;
			 Log.message("After search Equipment Parts Successfully");
		 }
		 else
		 {
			 result3=false;
			 Log.message("After search Equipment Parts Failed");
		 }
	 }
	 else
	 {
		 result2=false; 
		 Log.message("Exp Equipment has been not displayed");
	 }
	if((result1==true)&&(result2==true)&&(result3==true))
		return true;
	else
		return false;
}

/** 
* Method written for add parts with name in Equipment details.
* Scripted By: Sekhar     
* @throws AWTException 
*/
public boolean addparts_name_Equipmentdetails(String Parts) throws AWTException
{  
	 boolean result1=false;	  
	 SkySiteUtils.waitTill(5000);    	
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[3]/div[1]/div[1]/button")).click();
	 Log.message("Add part button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//div/div[1]/div/input")).sendKeys(Parts);
	 Log.message("Part name has been entered:"+Parts);
	 SkySiteUtils.waitTill(8000);
	 driver.findElement(By.xpath(".//*[@id='addPartsModal']/div[3]/button")).click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 if(driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).isDisplayed())			
		 return true; 	 
	else		
		return false;	
}

/** 
* Method written for delete parts in Equipment details.
* Scripted By: Sekhar     
* @throws AWTException 
*/
public boolean Deleteparts_Equipmentdetails(String Parts) throws AWTException
{  
	 boolean result1=false;	  
	 SkySiteUtils.waitTill(5000);   
	 Expname.click();
	 Log.message("Recently Equipment has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[3]/div[1]/div[1]/button")).click();
	 Log.message("Add part button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//div/div[1]/div/input")).sendKeys(Parts);
	 Log.message("Part name has been entered");
	 SkySiteUtils.waitTill(8000);
	 driver.findElement(By.xpath(".//*[@id='addPartsModal']/div[3]/button")).click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(8000);	 
	 if(driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).isDisplayed())
	 {
		 result1=true;
		 Log.message("Parts added successfully!!!");		 
	 }
	 else
	 {
		result1=false;
		Log.message("Parts added Failed!!!");
	 }	
	 SkySiteUtils.waitTill(10000);	
	 int Before_Part_List = driver.findElements(By.xpath("//div/div/div[2]/h5")).size();
	 Log.message("Delete before Part List is:" + Before_Part_List);	
	 int i = 0;
	 for (i = 1; i <= Before_Part_List; i++) 
	 {
		 String Partname = driver.findElement(By.xpath("(//div/div/div[2]/h5)["+i+"]")).getText();
		 Log.message(Partname);
		 SkySiteUtils.waitTill(5000);					
		 if (Partname.equals(Parts))
		 {					
			 Log.message("Exp Parts has been validated!!!");
			 break;
		 }
	 }
	 SkySiteUtils.waitTill(3000);		 
	 driver.findElement(By.xpath("(//button[@class='list-file-delete'])["+i+"]")).click();
	 Log.message("Delete has been clicked");
	 SkySiteUtils.waitTill(10000);	
	 int After_Part_List = driver.findElements(By.xpath("//div/div/div[2]/h5")).size();
	 Log.message("After Part List is:"+ After_Part_List);
	 SkySiteUtils.waitTill(5000);	 
	if((After_Part_List==(Before_Part_List-1))&&(result1==true))
		return true;
	else
		return false;
}


/** 
* Method written for Edit part with name in Equipment details page.
* Scripted By: Sekhar     
* @throws AWTException 
*/
public boolean Editpart_Equipmentdetails(String Parts,String EditParts) throws AWTException
{  
	 boolean result1=false;	 
	 boolean result2=false;	
	 SkySiteUtils.waitTill(5000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[3]/div[1]/div[1]/button")).click();
	 Log.message("Add part button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//div/div[1]/div/input")).sendKeys(Parts);
	 Log.message("Part name has been entered");
	 SkySiteUtils.waitTill(8000);
	 driver.findElement(By.xpath(".//*[@id='addPartsModal']/div[3]/button")).click();
	 Log.message("save button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 String Exp_Parts = driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).getText();
	 Log.message("Exp Parts is:"+Exp_Parts);
	 SkySiteUtils.waitTill(5000);
	 if(Parts.contains(Exp_Parts))
	 {
		 result1=true;
		 Log.message("Parts added successfully!!!");	
		 driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).click();
		 SkySiteUtils.waitTill(5000);
		 driver.findElement(By.xpath("//button[@class='btn asset-btn-primary ng-star-inserted']")).click();
		 Log.message("Edit icon button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 driver.findElement(By.xpath("//div/div[1]/div/input")).clear();
		 Log.message("Part name has been cleared");
		 SkySiteUtils.waitTill(5000);
		 driver.findElement(By.xpath("//div/div[1]/div/input")).sendKeys(EditParts);
		 Log.message("Part name has been entered");
		 SkySiteUtils.waitTill(5000);
		 driver.findElement(By.xpath("//button[@class='btn asset-btn-primary ng-star-inserted']")).click();
		 Log.message("save button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 driver.findElement(By.xpath("//i[@class='icon icon-am-cross']")).click();
		 Log.message("cancel cross button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().defaultContent();
		 liEquipment.click();
    	 Log.message("Equipment button has been clicked");
    	 SkySiteUtils.waitTill(10000);
    	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	 Log.message("Switch to My frame");
    	 Expname.click();
    	 Log.message("Recently Equipment has been clicked");
    	 SkySiteUtils.waitTill(15000);
    	 String Exp_EditParts = driver.findElement(By.xpath("(//div[@class='list-file-name'])[1]")).getText();
    	 Log.message("Exp Edit Parts is:"+Exp_EditParts);
    	 if(EditParts.equals(Exp_EditParts))
    	 {
    		 result2=true;
    		 Log.message("Parts Edited Successfully!!!"); 
    	 }    		
    	 else
    	 {
    		 result2=false;
    		 Log.message("Parts Edited Failed!!!"); 
    	 }    		
	 }
	 else
	 {
		result1=false;
		Log.message("Parts added Failed!!!");
	 }	
	if((result1==true)&&(result2==true))	
		return true;
	else
		return false;	
}


/** 
* Method written for Add linked files from computer in Equipment details page.
* Scripted By: Sekhar     
* @throws AWTException 
 * @throws IOException 
*/
public boolean Addlinkedfiles_computer_Equipmentdetails(String FolderPath) throws AWTException, IOException
{  	
	 SkySiteUtils.waitTill(5000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/button")).click();
	 Log.message("Add the linked files button has been clicked");
	 SkySiteUtils.waitTill(5000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/ul/li[2]/a")).click();
	 Log.message("computer button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 
	// Writing File names into a text file for using in AutoIT Script
	 BufferedWriter output;
	 randomFileName rn = new randomFileName();
	 //String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
	 String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
	 output = new BufferedWriter(new FileWriter(tmpFileName, true));
	 
	 String expFilename = null;
	 File[] files = new File(FolderPath).listFiles();
	 
	 for (File file : files) 
	 {
		 if (file.isFile())
		 {
			 expFilename = file.getName();// Getting File Names into a variable
			 Log.message("Expected File name is:" + expFilename);
			 output.append('"' + expFilename + '"');
			 output.append(" ");
			 SkySiteUtils.waitTill(5000);
		 }
	 }
	 output.flush();
	 output.close();	 
	 String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
	 // Executing .exe autoIt file
	 Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
	 Log.message("AutoIT Script Executed!!");
	 SkySiteUtils.waitTill(30000);	 
	 try 
	 {
		 File file = new File(tmpFileName);
		 if (file.delete()) 
		 {
			 Log.message(file.getName() + " is deleted!");
		 }
		 else
		 {
			 Log.message("Delete operation is failed.");
		 }
	 }
	 catch (Exception e)
	 {
		 Log.message("Exception occured!!!" + e);
	 }
	 SkySiteUtils.waitTill(25000);	 
	 if(driver.findElement(By.xpath("//div[@class='list-file-name']")).isDisplayed())
		 return true;
	 else
		 return false;
}


/** 
* Method written for Add linked files from SKYSITE in Equipment details page.
* Scripted By: Sekhar     
* @throws AWTException 
 * @throws IOException 
*/
public boolean Addlinkedfiles_SKYSITE_Equipmentdetails() throws AWTException, IOException
{  	 	
	 SkySiteUtils.waitTill(5000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/button")).click();
	 Log.message("Add the linked files button has been clicked");
	 SkySiteUtils.waitTill(3000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/ul/li[3]/a")).click();
	 Log.message("SKYSITE button has been clicked");
	 SkySiteUtils.waitTill(10000);
	 driver.findElement(By.xpath("//div[@class='asset-list-box-content']")).click();
	 Log.message("Exp floder has been clicked");
	 SkySiteUtils.waitTill(8000);
	 driver.findElement(By.xpath("//div[@class='asset-list-box-content']")).click();
	 Log.message("Exp file check box has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("//button[@class='btn asset-btn-primary']")).click();
	 Log.message("Done button has been clicked");
	 SkySiteUtils.waitTill(10000);
	 if(driver.findElement(By.xpath("//div[@class='list-file-name']")).isDisplayed())
		 return true;
	 else
		 return false; 	
}

/** 
* Method written for remove file from linked files in Equipment details page.
* Scripted By: Sekhar   
* @throws IOException 
*/
public boolean Removefile_linkedfiles_Equipmentdetails() throws AWTException
{  	 	
	 SkySiteUtils.waitTill(5000);
	 Expname.click();
	 Log.message("Recently Equipment has been clicked");
	 SkySiteUtils.waitTill(15000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/button")).click();
	 Log.message("Add the linked files button has been clicked");
	 SkySiteUtils.waitTill(3000);	 
	 driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/ul/li[3]/a")).click();
	 Log.message("SKYSITE button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.findElement(By.xpath("//div[@class='asset-list-box-content']")).click();
	 Log.message("Exp folder has been clicked");
	 SkySiteUtils.waitTill(10000);
	 driver.findElement(By.xpath("//div[@class='asset-list-box-content']")).click();
	 Log.message("Exp file check box has been clicked");
	 SkySiteUtils.waitTill(8000);
	 driver.findElement(By.xpath("//*[@id='largeModal']/div[3]/div[2]/button")).click();
	 Log.message("Done button has been clicked");
	 SkySiteUtils.waitTill(10000);
	 int Before_filecount = driver.findElements(By.xpath("//div[@class='list-file-item cursor-pointer']")).size();
	 Log.message("remove before file List is:" + Before_filecount);		 
	 SkySiteUtils.waitTill(3000);		 
	 driver.findElement(By.xpath("//div[1]/div/div[3]/div/button")).click();
	 Log.message("file more button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("(//i[@class='icon icon-am-delete asset-fa-md'])[2]")).click();
	 Log.message("Remove button has been clicked");
	 SkySiteUtils.waitTill(8000);
	 int After_filecount = driver.findElements(By.xpath("//div[@class='list-file-item cursor-pointer']")).size();
	 Log.message("remove After file List is:" + After_filecount);
	 if(After_filecount==(Before_filecount-1))
		 return true;
	 else
		 return false;	 
}

/** 
* Method written for file information view on linked files in Equipment.
* Scripted By: Sekhar 
*/
public boolean Fileinformation_View_linkedfiles_Equipmentdetails() 
{  	 	
	 SkySiteUtils.waitTill(5000);	 
	 String filename = driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div/div[2]/h5")).getText();
	 Log.message("File name is:" + filename);		 
	 SkySiteUtils.waitTill(3000);	
	 driver.findElement(By.xpath("//button[@class='asset-more-btn']")).click();
	 Log.message("more button from file has been clicked");		 
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-info asset-fa-md']")).click();
	 Log.message("information button has been clicked");		 
	 SkySiteUtils.waitTill(3000);
	 String Infor_name = driver.findElement(By.xpath("(//div/h4)[3]")).getText();
	 Log.message("Information file name is:"+Infor_name);
	 if(Infor_name.equals(filename))
		 return true;
	 else
		 return false;
}

/** 
* Method written for file information delete on linked files in Equipment.
* Scripted By: Sekhar 
 * @throws IOException 
 * @throws AWTException 
*/
public boolean Fileinformation_Delete_linkedfiles_Equipmentdetails() throws AWTException, IOException 
{  	 	
	 SkySiteUtils.waitTill(5000);	
	 Expname.click();
	 Log.message("Exp Equipment has been clicked");		 
	 SkySiteUtils.waitTill(8000);
	 
	 this.Addlinkedfiles_SKYSITE_Equipmentdetails();
	 
	 int Exp_filecount = driver.findElements(By.xpath("//div[@class='list-file-name']")).size();
	 Log.message("Exp File count is:" + Exp_filecount);		 
	 SkySiteUtils.waitTill(3000);	
	 driver.findElement(By.xpath("(//button[@class='asset-more-btn'])[1]")).click();
	 Log.message("more button from file has been clicked");		 
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("(//i[@class='icon icon-am-delete asset-fa-md'])[2]")).click();
	 Log.message("remove button has been clicked");		 
	 SkySiteUtils.waitTill(5000);
	 int Act_filecount = driver.findElements(By.xpath("//div[@class='list-file-name']")).size();
	 Log.message("File name count is:" + Act_filecount);		 
	 SkySiteUtils.waitTill(3000);	
	 if(Act_filecount==(Exp_filecount-1))
		 return true;
	 else
		 return false;
}

/** 
* Method written for file download from information on linked files in Equipment.
* Scripted By: Sekhar 
 * @throws IOException 
 * @throws AWTException 
*/
public boolean FileDownload_information_linkedfiles_Equipmentdetails(String DownloadPath) throws AWTException, IOException 
{  	 	
	 SkySiteUtils.waitTill(5000);	
	 String filename = driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div/div[2]/h5")).getText();
	 Log.message("File name is:" + filename);		 
	 SkySiteUtils.waitTill(3000);	
	 driver.findElement(By.xpath("//button[@class='asset-more-btn']")).click();
	 Log.message("more button from file has been clicked");		 
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-info asset-fa-md']")).click();
	 Log.message("information button has been clicked");		 
	 SkySiteUtils.waitTill(5000);	 
	 driver.findElement(By.xpath("(//button[@class='asset-more-btn'])[2]")).click();
	 Log.message("information more button has been clicked");
	 SkySiteUtils.waitTill(3000);
	// Calling delete files from download folder script
	 fnaHomePage = new FnaHomePage(driver).get(); 
	 fnaHomePage.Delete_Files_From_Folder(DownloadPath);
	 SkySiteUtils.waitTill(10000);	 	 
	 driver.findElement(By.xpath("//i[@class='icon icon-am-download asset-fa-md']")).click();
	 Log.message("Download button has been clicked");
	 SkySiteUtils.waitTill(3000);
	 
	// Get Browser name on run-time.
	 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	 String browserName = caps.getBrowserName();
	 Log.message("Browser name on run-time is: " + browserName);
	 
	 if (browserName.contains("firefox")) 
	 {
		 // Handling Download PopUp of firefox browser using robot
		 Robot robot = null;
		 robot = new Robot();		 
		 robot.keyPress(KeyEvent.VK_ALT);
		 SkySiteUtils.waitTill(2000);
		 robot.keyPress(KeyEvent.VK_S);
		 SkySiteUtils.waitTill(2000);
		 robot.keyRelease(KeyEvent.VK_ALT);
		 robot.keyRelease(KeyEvent.VK_S);
		 SkySiteUtils.waitTill(3000);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 SkySiteUtils.waitTill(20000);
	 }
	 SkySiteUtils.waitTill(25000);
	 // After checking whether download folder or not
	 String ActualFilename = null;
	 
	 File[] files = new File(DownloadPath).listFiles();
	 
	 for (File file : files)
	 {
		 if (file.isFile()) 
		 {
			 ActualFilename = file.getName();// Getting File Name into a variable
			 Log.message("Actual File name is:" + ActualFilename);
			 SkySiteUtils.waitTill(1000);
			 Long ActualFileSize = file.length();
			 Log.message("Actual File size is:" + ActualFileSize);
			 SkySiteUtils.waitTill(5000);
			 if (ActualFileSize != 0) 
			 {
				 Log.message("Downloaded file is available in downloads!!!");
			 } 
			 else
			 {
				 Log.message("Downloaded file is NOT available in downloads!!!");
			 }
		 }
	 }
	 if(ActualFilename.contains(filename))
		 return true;
	 else
		 return false;		
}

/** 
* Method written for Add file using Add to SKYSITE on linked files.
* Scripted By: Sekhar 
*/
public boolean AddFile_linkedfiles_AddtoSKYSITE_Equipmentdetails() 
{  	 	
	 SkySiteUtils.waitTill(5000);	 
	 String filename = driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div/div[2]/h5")).getText();
	 Log.message("File name is:" + filename);		 
	 SkySiteUtils.waitTill(3000);	
	 driver.findElement(By.xpath("//button[@class='asset-more-btn']")).click();
	 Log.message("more button from file has been clicked");		 
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-computer asset-fa-md']")).click();
	 Log.message("Add to SKYSITE button has been clicked");		 
	 SkySiteUtils.waitTill(3000);	 
	 driver.findElement(By.xpath("//div[@class='asset-list-box-content']")).click();
	 Log.message("Exp floder has been clicked");
	 SkySiteUtils.waitTill(8000);	 
	 driver.findElement(By.xpath("//button[@class='btn asset-btn-primary']")).click();
	 Log.message("Done button has been clicked");
	 SkySiteUtils.waitTill(10000);
	 driver.switchTo().defaultContent();
	 driver.findElement(By.xpath("//i[@class='icon icon-star ico-lg icon-files']")).click();
	 Log.message("Files button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 if(driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())	
		 return true;
	 else
		 return false;
}

/** 
* Method written for Add file using Add to SKYSITE from information on linked files.
* Scripted By: Sekhar 
*/
public boolean AddFile_information_AddtoSKYSITE_Equipmentdetails() 
{  	 	
	 SkySiteUtils.waitTill(5000);	 
	 String filename = driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div/div[2]/h5")).getText();
	 Log.message("File name is:" + filename);		 
	 SkySiteUtils.waitTill(3000);	
	 driver.findElement(By.xpath("//button[@class='asset-more-btn']")).click();
	 Log.message("more button from file has been clicked");		 
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-info asset-fa-md']")).click();
	 Log.message("information button has been clicked");		 
	 SkySiteUtils.waitTill(5000);	 
	 driver.findElement(By.xpath("(//button[@class='asset-more-btn'])[2]")).click();
	 Log.message("information more button has been clicked");
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-skysite-gallery asset-fa-md']")).click();
	 Log.message("Add to SKYSITE button has been clicked");		 
	 SkySiteUtils.waitTill(3000);	 
	 driver.findElement(By.xpath("//div[@class='asset-list-box-content']")).click();
	 Log.message("Exp floder has been clicked");
	 SkySiteUtils.waitTill(8000);	 
	 driver.findElement(By.xpath("//button[@class='btn asset-btn-primary']")).click();
	 Log.message("Done button has been clicked");
	 SkySiteUtils.waitTill(10000);
	 driver.switchTo().defaultContent();
	 driver.findElement(By.xpath("//i[@class='icon icon-star ico-lg icon-files']")).click();
	 Log.message("Files button has been clicked");
	 SkySiteUtils.waitTill(15000);
	 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 Log.message("Switch to My frame");
	 if(driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
		 return true;
	 else 
		 return false;

}

/** 
* Method written for send links view in equipment details.
* Scripted By: Sekhar 
*/
public boolean sendlinks_View_Equipmentdetails() 
{  	 	
	 SkySiteUtils.waitTill(5000);
	 String Exp_name =ActName.getText();
	 Log.message("name is:"+Exp_name);
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//button[@class='asset-send-template']")).click();
	 Log.message("send link button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-copy asset-fa-md']")).click();
	 Log.message("clipboard icon button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 String link= driver.findElement(By.xpath("//input[@class='form-control ng-pristine ng-valid ng-touched']")).getAttribute("value");
	 Log.message("link is:"+link);	
	 SkySiteUtils.waitTill(10000);
	 driver.get(link);	 	
	 SkySiteUtils.waitTill(10000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
 	 SkySiteUtils.waitTill(15000);
 	 Log.message("link copied in new tab"); 	  
 	 String Act_name = driver.findElement(By.xpath("//div/div/div[2]/div/div[1]/div/div[2]/div[2]/div[1]/div/span[2]")).getText();
 	 Log.message("name is:"+Act_name);
 	 if(Exp_name.equals(Act_name))
 		 return true;
 	 else
 		 return false;	 
}

/** 
* Method written for send links Download in equipment details.
* Scripted By: Sekhar 
 * @throws AWTException 
*/
public boolean sendlinks_Download_Equipmentdetails(String DownloadPath) throws AWTException 
{  	 	
	 SkySiteUtils.waitTill(5000);
	 String Exp_Filename =driver.findElement(By.xpath("//div[@class='list-file-name']")).getText();
	 Log.message("File name is:"+Exp_Filename);
	 SkySiteUtils.waitTill(3000);
	 driver.findElement(By.xpath("//button[@class='asset-send-template']")).click();
	 Log.message("send link button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 driver.findElement(By.xpath("//i[@class='icon icon-am-copy asset-fa-md']")).click();
	 Log.message("clipboard icon button has been clicked");
	 SkySiteUtils.waitTill(5000);
	 String link= driver.findElement(By.xpath("//input[@class='form-control ng-pristine ng-valid ng-touched']")).getAttribute("value");
	 Log.message("link is:"+link);
	 SkySiteUtils.waitTill(10000);
	 driver.get(link);	 	
	 SkySiteUtils.waitTill(10000);
	 Actions action = new Actions(driver); 
	 action.sendKeys(Keys.ENTER).build().perform();
 	 SkySiteUtils.waitTill(15000);
 	 Log.message("link copied in new tab"); 	  
 	// Calling delete files from download folder script
 	 fnaHomePage = new FnaHomePage(driver).get(); 
 	 fnaHomePage.Delete_Files_From_Folder(DownloadPath);
 	 SkySiteUtils.waitTill(10000);	
 	// driver.switchTo().defaultContent();
 	 driver.findElement(By.xpath("//*[@id='mCSB_2_container']/div/div/a")).click();
 	 Log.message("Download button has been clicked");
 	 SkySiteUtils.waitTill(3000); 		 
 	 // Get Browser name on run-time.
 	 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
 	 String browserName = caps.getBrowserName();
 	 Log.message("Browser name on run-time is: " + browserName);
 		 
 	 if (browserName.contains("firefox")) 
 	 {
 		 // Handling Download PopUp of firefox browser using robot
 		 Robot robot = null;
 		 robot = new Robot();		 
 		 robot.keyPress(KeyEvent.VK_ALT);
 		 SkySiteUtils.waitTill(2000);
 		 robot.keyPress(KeyEvent.VK_S);
 		 SkySiteUtils.waitTill(2000);
 		 robot.keyRelease(KeyEvent.VK_ALT);
 		 robot.keyRelease(KeyEvent.VK_S);
 		 SkySiteUtils.waitTill(3000);
 		 robot.keyPress(KeyEvent.VK_ENTER);
 		 robot.keyRelease(KeyEvent.VK_ENTER);
 		 SkySiteUtils.waitTill(20000);
 	 }
 	 SkySiteUtils.waitTill(25000);
 	 // After checking whether download folder or not
 	 String ActualFilename = null;
 	 Long ActualFileSize1 = null;
 	 File[] files = new File(DownloadPath).listFiles();
 	 
 	 for (File file : files)
 	 {
 		 if (file.isFile()) 
 		 {
 			 ActualFilename = file.getName();// Getting File Name into a variable
 			 Log.message("Actual File name is:" + ActualFilename);
 			 SkySiteUtils.waitTill(1000);
 			 ActualFileSize1 = file.length();
 			 Log.message("Actual File size is:" + ActualFileSize1);
 			 SkySiteUtils.waitTill(5000);
 			 if (ActualFileSize1 != 0) 
 			 {
 				 Log.message("Downloaded file is available in downloads!!!");
 			 } 
 			 else
 			 {
 				 Log.message("Downloaded file is NOT available in downloads!!!");
 			 }
 		 }
 	 }
 	 if(ActualFileSize1!=0)
 		 return true;
 	 else
 		 return false;		 
}

 //.................................................. Ranadeep Ghosh .............................................
    
    /** 
     * Method written for validating latest added equipment is displayed on the top of the Recently Viewed Equipment List
     * Scripted By: Ranadeep Ghosh    
     * @throws AWTException 
     */
	public boolean Latest_Added_Viewed_Equipment_On_Top_Recently_Viewed(String name, String serialNumber, String building, String area, String floor) 
	{
		SkySiteUtils.waitTill(8000);

		String equipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
		Log.message("Latest Equipment name on top of the Recently Viewed list is:- "+equipmentName);
		
		String equipmentSerialNo = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[1]/span")).getText();
		Log.message("Latest Equipment Serial no on top of the Recently Viewed list is:- "+equipmentSerialNo);
		
		String equipmentBulding = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[2]/span")).getText();
		Log.message("Latest Equipment Building on top of the Recently Viewed list is:- "+equipmentBulding);
		
		String equipmentFloor = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[3]/span")).getText();
		Log.message("Latest Equipment Floor on top of the Recently Viewed list is:- "+equipmentFloor);

		String equipmentArea = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[4]/span")).getText();
		Log.message("Latest Equipment Area on top of the Recently Viewed list is:- "+equipmentArea);
		
		if(equipmentName.contains(name) && equipmentSerialNo.contains(serialNumber) && equipmentBulding.contains(building) && equipmentFloor.contains(floor) && equipmentArea.contains(area))
		{
			return true;			
		}
		else
		{
			return false;			
		}		
	}
	
	/** 
     * Method written for validating latest viewed equipment is displayed on the top of the Recently Viewed Equipment List
     * Scripted By: Ranadeep Ghosh    
     * @throws AWTException 
     */
	public boolean Search_Equipment(String Name, String SerialNumber, String Building, String Area, String Floor) 
	{
		
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Name);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentName=null;
		String equipmentSerialNo=null;
		String equipmentBulding=null;
		String equipmentFloor=null;
		String equipmentArea=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the name "+Name);
			
		}
		else
		{
			equipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).getText();
			Log.message("Searched Equipment name is:- "+equipmentName);
			
			equipmentSerialNo = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[1]/span")).getText();
			Log.message("Searched Equipment Serial no is:- "+equipmentSerialNo);
			
			equipmentBulding = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[2]/span")).getText();
			Log.message("Searched Equipment Building is:- "+equipmentBulding);
			
			equipmentFloor = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[3]/span")).getText();
			Log.message("Searched Equipment Floor is:- "+equipmentFloor);
			
			equipmentArea = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[4]/span")).getText();
			Log.message("Searched Equipment Area is:- "+equipmentArea);
					
			driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
			Log.message("Clicked to enter searched equipment");
			SkySiteUtils.waitTill(8000);
			
		}
					
		if(equipmentName.equals(Name) && equipmentSerialNo.equals(SerialNumber) && equipmentBulding.equals(Building) && equipmentFloor.equals(Floor) && equipmentArea.equals(Area))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}		
	}
	
	/**
	 * Method written for logout Scripted 
	 * Scripted By: Ranadeep Ghosh
	 * 
	 * @return
	 */

	public boolean logout() 
	{

		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnuserinfo, 30);
		clkprofile.click();
		Log.message("myprofile has been clicked.");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, pfloutput, 30);
		pfloutput.click();
		Log.message("log out button has been clicked.");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnyes, 30);
		btnyes.click();
		Log.message("yes button has been clicked.");
		SkySiteUtils.waitTill(7000);
		
		String Exptitle = "Sign in - SKYSITE";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(5000);
		Log.message("Logout Validation:");
		SkySiteUtils.waitTill(5000);
		if (Acttitle_AfterLogin.equals(Exptitle))
		{
			return true;
			
		}	
		else
		{
			return false;
		
		}
	}	
	
	@FindBy(xpath="//*[@formcontrolname='AssetName']")
	WebElement textboxEquipName;
	/** 
     * Method written for addition of an equipment with all the visible fields in the list fields.
     * Scripted By: Ranadeep Ghosh 
     * @throws AWTException 
     */
    public boolean Equipment_Addition(String Name,String Serialnumber,String Building,String Area,String Floor) throws Exception
    {    
    	
    	SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);    	
    	 
    	SkySiteUtils.waitForElement(driver, txtname, 30);
 		txtname.clear();
 		txtname.sendKeys(Name);
 		Log.message("Equipment name entered is:- "+Name);
 		SkySiteUtils.waitTill(5000); 
 		
    	WebElement e = driver.findElement(By.xpath("//*[@id='mCSB_2_dragger_vertical']"));
    	Actions move = new Actions(driver);
    	move.moveToElement(e).clickAndHold().moveByOffset(0,44).release().perform();
    	SkySiteUtils.waitTill(3500);
 		Log.message("x,y coordinates taken"); 	
 		 		
 		move.moveToElement(e).clickAndHold().moveByOffset(0,64).release().perform();
 		SkySiteUtils.waitTill(5000);
 		SkySiteUtils.waitForElement(driver, txtSerialnumber, 30);
 		txtSerialnumber.clear();
    	txtSerialnumber.sendKeys(Serialnumber);
    	Log.message("Equipment Serial number entered is:- "+Serialnumber);
    	SkySiteUtils.waitTill(5000); 
    	
    	move.moveToElement(e).clickAndHold().moveByOffset(0,84).release().perform();
    	SkySiteUtils.waitForElement(driver, txtBuilding, 30);
    	txtBuilding.clear();
    	txtBuilding.sendKeys(Building);
    	Log.message("Building entered is:- "+Building);
    	SkySiteUtils.waitTill(5000);
    	
    	SkySiteUtils.waitTill(5000);  
    	SkySiteUtils.waitForElement(driver, txtArea, 30);
    	txtArea.clear();
    	txtArea.sendKeys(Area);
    	Log.message("Area entered is:- "+Area);
    	SkySiteUtils.waitTill(5000);     	
    	 
    	SkySiteUtils.waitForElement(driver, txtFloor, 30);
    	txtFloor.clear();
    	txtFloor.sendKeys(Floor);
    	Log.message("Floor entered is:- "+Floor);
    	SkySiteUtils.waitTill(5000);
    	 
    	SkySiteUtils.waitForElement(driver, btnsave, 30);
    	btnsave.click();
    	Log.message("Clicked on Save button");
    	SkySiteUtils.waitTill(15000); 

		String equipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
		Log.message("Latest Equipment name on top of the Recently Viewed list is:- "+equipmentName);
		
		String equipmentSerialNo = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[1]/span")).getText();
		Log.message("Latest Equipment Serial no on top of the Recently Viewed list is:- "+equipmentSerialNo);
		
		String equipmentBulding = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[2]/span")).getText();
		Log.message("Latest Equipment Building on top of the Recently Viewed list is:- "+equipmentBulding);
		
		String equipmentFloor = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[3]/span")).getText();
		Log.message("Latest Equipment Floor on top of the Recently Viewed list is:- "+equipmentFloor);

		String equipmentArea = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/ul/li[4]/span")).getText();
		Log.message("Latest Equipment Area on top of the Recently Viewed list is:- "+equipmentArea);
		
		if(equipmentName.contains(Name) && equipmentSerialNo.contains(Serialnumber) && equipmentBulding.contains(Building) && equipmentFloor.contains(Floor) && equipmentArea.contains(Area))
		{
			
			driver.switchTo().defaultContent();
			Log.message("Switching to default content");
			SkySiteUtils.waitTill(5000);
			liEquipment.click();
	    	Log.message("Equipment button has been clicked");
	    	SkySiteUtils.waitTill(5000);
	    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	    	Log.message("Switch to My frame");
	        SkySiteUtils.waitTill(5000);
	    	
			return true;	
		}
		else
		{
			return false;
			
		}
    	 	 
    }   
    
    /** 
     * Method written for verifying list of equipments in the Recently Viewed List
     * Scripted By: Ranadeep Ghosh 
     * @throws AWTException 
     */
    public boolean Verify_Recently_Viewed_Equipment_count(String count)
    {
    	SkySiteUtils.waitTill(8000);
    	
    	int count1 = 0;   	
    	int count2 = Integer.parseInt(count);
    	
    	while(count1<=count2)
    	{
    		JavascriptExecutor js = (JavascriptExecutor) driver;
    		js.executeScript("window.scrollBy(0,6000)");
    		SkySiteUtils.waitTill(5000);
    		Log.message("Scrolling to the bottom");
    		
    		count1 = driver.findElements(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).size();
    		SkySiteUtils.waitTill(3000);
    		Log.message("No of equipments in the list is:- "+count1);       		
    	}
    	if(count1 >= count2)
    	{
    		return true;    		
    	}
    	else
    	{
    		return false;    		
    	}
    }

    
    /** 
     * Method written for verifying equipment search by Name (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * 
     */
	public boolean Search_Equipment_Name(String name) 
	{
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(name);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment with Name");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentName=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the name "+name);
			
		}
		else
		{
			equipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).getText();
			Log.message("Searched Equipment name is:- "+equipmentName);

			SkySiteUtils.waitTill(8000);
			
		}
					
		if(equipmentName.equals(name))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
	}

	
	/** 
     * Method written for verifying equipment search by Serial Number (Modular Search)
     * Scripted By: Ranadeep Ghosh 
     * 
     */
	public boolean Search_Equipment_SerialNo(String serialNo) 
	{
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(serialNo);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment with Serial Number");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentSerialNo=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the Serial Number "+serialNo);
			
		}
		else
		{
			equipmentSerialNo = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/ul/li[1]/span")).getText();
			Log.message("Searched Equipment Serial Number is:- "+equipmentSerialNo);

			SkySiteUtils.waitTill(8000);
			
		}
					
		if(equipmentSerialNo.equals(serialNo))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
	}

	
	/** 
     * Method written for verifying equipment search by Description (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * 
     */
	public boolean Search_Equipment_Description(String descriptionText) 
	{
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(descriptionText);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment with Description");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentDescription=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the Description "+descriptionText);
			
		}
		else
		{
			driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
			Log.message("Clicked to enter searched equipment ");
			SkySiteUtils.waitTill(8000);
			
			equipmentDescription=driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-asset-detail/div[2]/div/div[1]/div/div[2]/div[2]/div[3]/div/span[2]/div")).getText();
			Log.message("Actual Equipment description is:- "+equipmentDescription);
			SkySiteUtils.waitTill(3000);
			
		}
					
		if(equipmentDescription.contains(descriptionText))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
	}

	
	
	/** 
     * Method written for verifying equipment search by Manufacturer (Modular Search)
     * Scripted By: Ranadeep Ghosh 
     * 
     */
	public boolean Search_Equipment_Manufacturer(String manufacturer) 
	{
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(manufacturer);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment with Manufacturer");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentManufacturer=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the Manufacturer "+manufacturer);
			
		}
		else
		{
			driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
			Log.message("Clicked to enter searched equipment ");
			SkySiteUtils.waitTill(8000);
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
    		js.executeScript("window.scrollBy(0,3000)");
    		SkySiteUtils.waitTill(5000);
    		
			equipmentManufacturer=driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-asset-detail/div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div[1]/span[2]/input")).getText();
			Log.message("Actual Equipment Manufacturer is:- "+equipmentManufacturer);
			SkySiteUtils.waitTill(3000);
			
		}
					
		if(equipmentManufacturer.equals(manufacturer))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
	}

	
	/** 
     * Method written for verifying equipment search by Model (Modular Search)
     * Scripted By: Ranadeep Ghosh 
     * 
     */
	public boolean Search_Equipment_Model(String model) 
	{
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(model);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment with Model");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentModel=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the Manufacturer "+model);
			
		}
		else
		{
			driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
			Log.message("Clicked to enter searched equipment ");
			SkySiteUtils.waitTill(8000);
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
    		js.executeScript("window.scrollBy(0,3000)");
    		SkySiteUtils.waitTill(5000);
    		
			equipmentModel=driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-asset-detail/div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div[2]/span[2]/input")).getText();
			Log.message("Actual Equipment Model is:- "+equipmentModel);
			SkySiteUtils.waitTill(3000);
			
		}
					
		if(equipmentModel.equals(model))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
	}

	/** 
     * Method written for verifying equipment search by Type (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * 
     */
	public boolean Search_Equipment_Type(String type) 
	{
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, txtBoxSearchEquipment, 30);
		txtBoxSearchEquipment.clear();
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(type);
		SkySiteUtils.waitTill(4000);
		txtBoxSearchEquipment.sendKeys(Keys.RETURN);
		SkySiteUtils.waitTill(8000);
		Log.message("Searching the Equipment with Type");
		
		String searchResultsCount = driver.findElement(By.xpath("//*[@class='asset-sub-heading']//h2//span")).getText();
		String equipmentType=null;
		
		if(searchResultsCount.contains("0"))
		{
			Log.message("No equipment by the Type "+type);
			
		}
		else
		{
			driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
			Log.message("Clicked to enter searched equipment ");
			SkySiteUtils.waitTill(8000);
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
    		js.executeScript("window.scrollBy(0,3000)");
    		SkySiteUtils.waitTill(5000);
    		
    		equipmentType=driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-asset-detail/div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div[2]/span[2]/input")).getAttribute("value");
			Log.message("Actual Equipment Type is:- "+equipmentType);
			SkySiteUtils.waitTill(3000);
			
		}
					
		if(equipmentType.equals(type))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
		
	}

	
	
	/** 
     * Method written for verifying equipment pre-filled name (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * 
     */
	public boolean Equipment_Addition_Default_Name() 
	{
		SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);
    	
    	SkySiteUtils.waitForElement(driver, textboxEquipName, 30);
    	String EquipmentName = textboxEquipName.getAttribute("value");
    	Log.message("Prefilled equipment name is:- "+EquipmentName);
    	 
    	SkySiteUtils.waitForElement(driver, btnsave, 30);
    	btnsave.click();
    	Log.message("Clicked on Save button to save equipment with default name");
    	SkySiteUtils.waitTill(15000);
	    	
    	String ActualEquipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
		Log.message("Created Equipment name is:- "+ActualEquipmentName);
		SkySiteUtils.waitTill(5000);

    	if(EquipmentName.contains(ActualEquipmentName))
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
 
	}
	
	@FindBy(xpath=".//*[@class='asset-browse-btn-photo']")
	WebElement btnAddPhoto;
	
	@FindBy(xpath=".//*[@class='icon icon-am-computer']//..")
	WebElement btnAddMediaComputer;
	
	@FindBy(xpath=".//*[@class='icon icon-am-skysite-gallery']//..")
	WebElement btnAddMediaSkysite;
	
	
	/**
	 * Method to upload media to an equipment 
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public void uploadMediaFromComputer(String filepath, String tempfilepath)throws IOException 
	{
		
		SkySiteUtils.waitTill(5000);    	 
   	    SkySiteUtils.waitForElement(driver, btnAddPhoto, 30);
   	    btnAddPhoto.click();
   	    Log.message("Add media button has been clicked");
   	    SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddMediaComputer, 30);
		btnAddMediaComputer.click();
		Log.message("Clicked on Upload from Computer option");		
		SkySiteUtils.waitTill(5000);

		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = tempfilepath + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, false));

		String expFilename = null;
		File[] files = new File(filepath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + filepath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
				
			} 
			else 
			{
				Log.message("Delete operation is failed.");
				
			}
		} 
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
			
		}
		SkySiteUtils.waitTill(8000);
		//return parentHandle;
		
	}
	
	
	
	/**
	 * Method to create equipment with photo 
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Addition_With_Photo(String name, String filepath, String tempfilepath) throws IOException 		
	{
		
		SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);
    	
    	Log.message("Adding photo to the equipment");
    	this.uploadMediaFromComputer(filepath,tempfilepath); 	
    	SkySiteUtils.waitTill(30000);
    	
    	String ActualEquipmentName=null;
    	
    	if(!btnAddMediaComputer.isDisplayed())
    	{
    		Log.message("Photo upload successfull");
    		
    		SkySiteUtils.waitForElement(driver, textboxEquipName, 30);
        	textboxEquipName.clear();
        	textboxEquipName.sendKeys(name);
        	Log.message("Equipment name entered is:- "+name);
        	 
        	SkySiteUtils.waitForElement(driver, btnsave, 30);
        	btnsave.click();
        	Log.message("Clicked on Save button to save equipment with photo");
        	SkySiteUtils.waitTill(15000);
    	    	
        	ActualEquipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
    		Log.message("Created Equipment name is:- "+ActualEquipmentName);
    		SkySiteUtils.waitTill(5000);
      	
    	}
    	else
    	{
    		Log.message("Photo upload failed!!!");
    		return false;
    	}
    	
    	if(ActualEquipmentName.equals(name))
    	{
    		return true;
    		
    	}
    	else
    	{
    		return false;
    		
    	}
    	
	}
	
	/**
	 * Method to create equipment with video 
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Addition_With_Video(String name, String filepath, String tempfilepath) throws IOException 
	{
		SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);
    	
    	Log.message("Adding video to the equipment");
    	this.uploadMediaFromComputer(filepath,tempfilepath); 	
    	SkySiteUtils.waitTill(30000);
    	
    	String ActualEquipmentName=null;
    	
    	if(!btnAddMediaComputer.isDisplayed())
    	{
    		Log.message("Video upload successfull");
    		
    		SkySiteUtils.waitForElement(driver, textboxEquipName, 30);
        	textboxEquipName.clear();
        	textboxEquipName.sendKeys(name);
        	Log.message("Equipment name entered is:- "+name);
        	 
        	SkySiteUtils.waitForElement(driver, btnsave, 30);
        	btnsave.click();
        	Log.message("Clicked on Save button to save equipment with video");
        	SkySiteUtils.waitTill(15000);
    	    	
        	ActualEquipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
    		Log.message("Created Equipment name is:- "+ActualEquipmentName);
    		SkySiteUtils.waitTill(5000);
      	
    	}
    	else
    	{
    		Log.message("Video upload failed!!!");
    		return false;
    	}
    	
    	if(ActualEquipmentName.equals(name))
    	{
    		return true;
    		
    	}
    	else
    	{
    		return false;
    		
    	}
	}

	
	/**
	 * Method to create equipment with Multiple Media 
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Addition_With_Multiple_Media(String name, String filepath, String tempfilepath) throws IOException 
	{
		SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);
    	
    	Log.message("Adding media to the equipment");	
    	this.uploadMediaFromComputer(filepath,tempfilepath); 	
    	SkySiteUtils.waitTill(30000);
    	
    	String ActualEquipmentName=null;
    	
    	if(!btnAddMediaComputer.isDisplayed())
    	{
    		Log.message("Multiple Media upload successfull");
    		
    		SkySiteUtils.waitForElement(driver, textboxEquipName, 30);
        	textboxEquipName.clear();
        	textboxEquipName.sendKeys(name);
        	Log.message("Equipment name entered is:- "+name);
        	 
        	SkySiteUtils.waitForElement(driver, btnsave, 30);
        	btnsave.click();
        	Log.message("Clicked on Save button to save equipment with Multiple Media");
        	SkySiteUtils.waitTill(15000);
    	    	
        	ActualEquipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
    		Log.message("Created Equipment name is:- "+ActualEquipmentName);
    		SkySiteUtils.waitTill(5000);
      	
    	}
    	else
    	{
    		Log.message("Media upload failed!!!");
    		return false;
    	}
    	
    	if(ActualEquipmentName.equals(name))
    	{
    		return true;
    		
    	}
    	else
    	{
    		return false;
    		
    	}
	}

	
	
	/**
	 * Method to create equipment with photo from Skysite
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Addition_Photo_From_Skysite(String name, String EquipmentAlbum) 
	{
		
		SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);
    	  	 
   	    SkySiteUtils.waitForElement(driver, btnAddPhoto, 30);
   	    btnAddPhoto.click();
   	    Log.message("Add media button has been clicked");
   	    SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, btnAddMediaSkysite, 30);
		btnAddMediaSkysite.click();
		Log.message("Clicked on Upload from Skysite option");		
		SkySiteUtils.waitTill(10000);
		
		String AlbumPhotoCount1 = PropertyReader.getProperty("EQUIPALBUMPHOTOCOUNT1");
		String AlbumPhotoCount2 = PropertyReader.getProperty("EQUIPALBUMPHOTOCOUNT2");
		
		int count1 = Integer.parseInt(AlbumPhotoCount1);	
		int count2 = Integer.parseInt(AlbumPhotoCount2);	
		SkySiteUtils.waitTill(3000);
		
		List<WebElement> element= driver.findElements(By.xpath("//*[@class='asset-list-box-wrapper cursor-pointer']//h4"));				
		int count = 0;
			
		for(WebElement we:element)
		{
			SkySiteUtils.waitTill(5000);
			if(we.getText().equals(EquipmentAlbum)) 
			{
				SkySiteUtils.waitTill(3000);
				we.click();
				SkySiteUtils.waitTill(6000);
			
				count = driver.findElements(By.xpath("//*[@class='asset-list-box-content']//h5")).size();
				Log.message("Total no of photos is "+count);
				
				if(count == count1)
				{
					SkySiteUtils.waitTill(3000);
					Log.message("Condition 1 matched");
					driver.findElement(By.xpath("//*[@class='asset-list-box-content']//h5")).click();			    
			    	Log.message("Clicked on photo ");
			    	SkySiteUtils.waitTill(3000);
			    	
				}
				
				
				
				if(count == count2)
				{
					for(int i=1;i<=count;i++)
					{
						SkySiteUtils.waitTill(3000);
						Log.message("Condition 2 matched");
						driver.findElement(By.xpath("html/body/modal-container[2]/div/div/app-image-picker/div/div[2]/div[2]/div[1]/div/div[2]/div["+i+"]/div/div[2]/h5")).click();
				    	Log.message("Clicked on photo "+i);
				    	SkySiteUtils.waitTill(3000);
						
					}
				}
								
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("//*[text()='Done']")).click();
				Log.message("Clicked on Done button");
				SkySiteUtils.waitTill(5000);
				Log.message("All photos are added to the Equipment");
				break;
			}
						
		}
			
		String ActualEquipmentName=null;
		int count3 = driver.findElements(By.xpath(".//*[@class='photo-video-hover-layer']")).size();
				
		if(count3 == count)
    	{
    		Log.message("Photo upload successfull from Skysite");
    		
    		SkySiteUtils.waitForElement(driver, textboxEquipName, 30);
        	textboxEquipName.clear();
        	textboxEquipName.sendKeys(name);
        	Log.message("Equipment name entered is:- "+name);
        	 
        	SkySiteUtils.waitForElement(driver, btnsave, 30);
        	btnsave.click();
        	Log.message("Clicked on Save button to save equipment with photo from Skysite");
        	SkySiteUtils.waitTill(5000);
    	    	
        	ActualEquipmentName = driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div[1]/div/div[2]/a/h4")).getText();
    		Log.message("Created Equipment name is:- "+ActualEquipmentName);
    		SkySiteUtils.waitTill(5000);
      	
    	}
    	else
    	{
    		Log.message("Photo upload from Skysite failed!!!");
    		return false;
    	}
    	
    	if(ActualEquipmentName.equals(name))
    	{
    		return true;
    		
    	}
    	else
    	{
    		return false;
    		
    	}
		
	}

	
	
	/**
	 * Method to create equipment with photo from Skysite
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Addition_Blank_Name_Validation(String name) 
	{
		SkySiteUtils.waitTill(5000);
    	SkySiteUtils.waitForElement(driver, btnadd, 30);
    	btnadd.click();
    	Log.message("Clicked on Add Equipment button");
    	SkySiteUtils.waitTill(20000);
    	
    	SkySiteUtils.waitForElement(driver, textboxEquipName, 30);
    	textboxEquipName.clear();
    	textboxEquipName.sendKeys(name);
    	Log.message(" Blank Equipment name is entered is");
    	
    	
    	if(driver.findElement(By.xpath("//button[@disabled='']")).isDisplayed())
    	{
    		return true;
    		
    	}
    	else
    	{
    		return false;
    		
    	}
    	
	}

	
	@FindBy(xpath="//*[text()='Edit photo']")
	WebElement EditPhotoModal;
	
	/**
	 * Method to view photo in an equipment
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_View_Media(String name) 
	{

		driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
		Log.message("Clicked to enter created equipment ");
		SkySiteUtils.waitTill(8000);
		
		int ActualEquipmentMediaCount = driver.findElements(By.xpath("html/body/app-root/app-asset-management/app-asset-detail/div[2]/div/div[1]/div/div[1]/div[2]/ul/div[1]/div/li/div[1]")).size();
		Log.message("No of media in the "+ActualEquipmentMediaCount);	
		
		String RequiredEquipmentMediaCount = PropertyReader.getProperty("EQUIPMEDIACOUNT");
		int count = Integer.parseInt(RequiredEquipmentMediaCount);			
		SkySiteUtils.waitTill(3000);
						
		WebElement e = driver.findElement(By.xpath("//*[@class='photo-video-hover-layer']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", e);		
    	Log.message("Clicked to view media");
    	  	
		if(EditPhotoModal.isDisplayed() && count == ActualEquipmentMediaCount)
		{
			return true;
			
		}
		else
		{
			return false;	
		}
		
	}

	
	/**
	 * Method to delete media from an equipment
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Delete_Media() throws Exception
	{
		driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
		Log.message("Clicked to enter created equipment ");
		SkySiteUtils.waitTill(5000);
		
		int ActualEquipmentMediaCount = driver.findElements(By.xpath("//*[@id='assetImagesRef']//div//div//li//img")).size();
		Log.message("No of media in the Equipment is "+ActualEquipmentMediaCount);	
		
		String RequiredEquipmentMediaCount = PropertyReader.getProperty("EQUIPMEDIACOUNT");
		int count = Integer.parseInt(RequiredEquipmentMediaCount);			
		SkySiteUtils.waitTill(3000);
		
		if(count == ActualEquipmentMediaCount)
		{
			
			WebElement element = driver.findElement(By.xpath("//*[@id='assetImagesRef']//div//div//li//img"));			
			Actions action = new Actions(driver);
			action.moveToElement(element).perform();
			Log.message("Hovering on equipment media");
			SkySiteUtils.waitTill(3000);
			
			driver.findElement(By.xpath(".//*[@class='asset-more-btn']/i")).click();
			Log.message("Clicked on more options for equipment media");
	    	SkySiteUtils.waitTill(5000);
	    	
	    	WebElement element1 = driver.findElement(By.xpath(".//*[@class='dropdown-menu dropdown-menu-right asset-dropdown-style dynamic-dropdown-width ng-star-inserted']//li[1]//a//i"));			
			action.moveToElement(element1).click().build().perform();
			Log.message("Clicked to delete media");
			SkySiteUtils.waitTill(3000);
			
		}
		
		List<WebElement> elements = driver.findElements(By.xpath("//*[@id='assetImagesRef']//div//div//li//img"));	
		int count2 =elements.size();
		Log.message("Equipment Media count is: "+count2);
		
		if(count2<1)
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
		
	}

	
	/**
	 * Method to select display picture
	 * Scripted By: Ranadeep Ghosh
	 * @throws IOException
	 * 
	 */
	public boolean Equipment_Dislay_Picture_Addition(String EquipmentAlbum)
	{
		driver.findElement(By.xpath("html/body/app-root/app-asset-management/app-dashboard/app-recently-viewed/div/div[2]/div[1]/div/div/div/div[2]/a/h4")).click();
		Log.message("Clicked to enter created equipment ");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAddPhoto, 30);
   	    btnAddPhoto.click();
   	    Log.message("Add media button has been clicked");
   	    SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, btnAddMediaSkysite, 30);
		btnAddMediaSkysite.click();
		Log.message("Clicked on Upload from Skysite option");		
		SkySiteUtils.waitTill(10000);
		
		String AlbumPhotoCount1 = PropertyReader.getProperty("EQUIPALBUMPHOTOCOUNT1");
		
		int count1 = Integer.parseInt(AlbumPhotoCount1);	
		SkySiteUtils.waitTill(3000);
		
		List<WebElement> element= driver.findElements(By.xpath("//*[@class='asset-list-box-content']//h4"));				
		int count = 0;
			
		for(WebElement we:element)
		{
			SkySiteUtils.waitTill(5000);
			if(we.getText().equals(EquipmentAlbum)) 
			{
				SkySiteUtils.waitTill(3000);
				we.click();
				SkySiteUtils.waitTill(3000);
			
				count = driver.findElements(By.xpath("//*[@class='asset-list-box-content']//h5")).size();
				Log.message("Total no of photos is "+count);
				
				if(count == count1)
				{
					SkySiteUtils.waitTill(3000);
					driver.findElement(By.xpath("//*[@class='asset-list-box-content']//h5")).click();			    
			    	Log.message("Clicked on photo");
			    	SkySiteUtils.waitTill(3000);
			    	
				}
								
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("//*[text()='Done']")).click();
				Log.message("Clicked on Done button");
				SkySiteUtils.waitTill(5000);
				Log.message("Photos added to the Equipment");
				break;
			}
						
		}
		
		WebElement element2 = driver.findElement(By.xpath("//*[@id='assetImagesRef']//div//div//li//img"));			
		Actions action = new Actions(driver);
		action.moveToElement(element2).perform();
		Log.message("Hovering on equipment media");
		SkySiteUtils.waitTill(3000);
		
		driver.findElement(By.xpath(".//*[@class='asset-more-btn']/i")).click();
		Log.message("Clicked on more options for equipment media");
    	SkySiteUtils.waitTill(5000);
    	
    	WebElement element1 = driver.findElement(By.xpath(".//*[@class='dropdown-menu dropdown-menu-right asset-dropdown-style dynamic-dropdown-width']//li[2]//a//i"));			
		action.moveToElement(element1).click().build().perform();
		Log.message("Clicked to make added media as display picture");
		SkySiteUtils.waitTill(3000);
		
		action.moveToElement(btnAddPhoto).perform();
		Log.message("Hovering change to dismiss dropdown");
		SkySiteUtils.waitTill(3000);
		
		String EquipmentDisplayPicLabel = PropertyReader.getProperty("EQUIPDISPLAYPICTURELABEL");
		Log.message("Expected Equipment display label is: "+EquipmentDisplayPicLabel);
		
		String ActualEquipmentDisplayPicLable= driver.findElement(By.xpath("//*[text()=' Display picture ']")).getText();
		Log.message("Actual Equipment display label is: "+ActualEquipmentDisplayPicLable);
		
		if(EquipmentDisplayPicLabel.contains(ActualEquipmentDisplayPicLable))
		{
			return true;
			
		}
		else
		{
			return false;
		}
		
	}
}