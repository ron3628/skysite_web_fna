package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;


public class LoginPage extends LoadableComponent<LoginPage> {

	WebDriver driver;
	private boolean isPageLoaded;

	/**
	 * Identifying web elements using FindfBy annotation.
	 */

	@FindBy(css="#UserID")
	WebElement txtBoxUserName;
	
	@FindBy(css="#Password")
	WebElement txtBoxPassword;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	
	/*@FindBy(xpath="//button[@id='btnNewProject']")
    WebElement buttonAddCollection;*/

	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */

	public LoginPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/** 
	 * Method written for doing login with valid credential.
	 * @throws AWTException 
	 */

	public FnaHomePage loginWithValidCredential(String uName,String pWord) throws AWTException
	{
		
		 SkySiteUtils.waitTill(15000);   
		 SkySiteUtils.waitTill(3000);
		 String currentURL = driver.getCurrentUrl();
		 Log.message("Current url is:- "+currentURL);
		//driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		/***edited by tarak **/
		 if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
		 {
			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		 }
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		//String uName = PropertyReader.getProperty("Username");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been entered in Username text box." );
		//String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 60);
		SkySiteUtils.waitTill(4000);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		return new FnaHomePage(driver).get();		
	}
	
	/** 
	 * Method written for doing login with valid credential.
	 * @throws AWTException 
	 */
	public HomePage performLoginOperation(String uName,String pWord) throws AWTException
	{
		 SkySiteUtils.waitTill(15000);
		//driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		/***edited by tarak **/
		 if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
		 {
			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		 }
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		//String uName = PropertyReader.getProperty("Username");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been engtered in Username text box." );
		//String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been engtered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 60);
		SkySiteUtils.waitTill(4000);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		return new HomePage(driver).get();		
	}
	
	/** 
     * Method written for doing login with Employee a/c valid credential.
    * @throws AWTException 
     */
    public FnaHomePage loginWithEmployeeUserCredential(String uName,String pWord) throws AWTException
    {
           SkySiteUtils.waitTill(5000);
           if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
           {
  			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
           }
           SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
           //String uName = PropertyReader.getProperty("Employeeusername");
           txtBoxUserName.clear();
           txtBoxUserName.sendKeys(uName);
           Log.message("Username: " + uName + " " + "has been entered in Username text box." );
          // String pWord = PropertyReader.getProperty("Password");
           txtBoxPassword.clear();
           txtBoxPassword.sendKeys(pWord);
           Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
           SkySiteUtils.waitForElement(driver, btnLogin, 60);
           btnLogin.click();
           Log.message("LogIn button clicked.");           
          // SkySiteUtils.waitForElement(driver, buttonAddCollection, 20);
           return new FnaHomePage(driver).get();        
    }   
    
    /** 
     * Method written for doing login with shared a/c valid credential.
    * @throws AWTException 
     */
    public FnaHomePage loginWithSharedUserCredential() throws AWTException
    {
           SkySiteUtils.waitTill(5000);
           if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
  		 {
  			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
  		 }
           SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
           String uName = PropertyReader.getProperty("Sharedusername");
           txtBoxUserName.clear();
           txtBoxUserName.sendKeys(uName);
           Log.message("Username: " + uName + " " + "has been entered in Username text box." );
           String pWord = PropertyReader.getProperty("Password");
           txtBoxPassword.clear();
           txtBoxPassword.sendKeys(pWord);
           Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
           SkySiteUtils.waitForElement(driver, btnLogin, 60);
           btnLogin.click();
           Log.message("LogIn button clicked.");           
          // SkySiteUtils.waitForElement(driver, buttonAddCollection, 20);
           return new FnaHomePage(driver).get();        
    }      

    /** 
     * Method written for doing login with Employee a/c valid credential.
    * @throws AWTException 
     */
    public FnaHomePage loginWithLiteUserCredential(String uName1,String pWord1) throws AWTException
    {
           SkySiteUtils.waitTill(5000);
           if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
  		 {
  			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
  		 }
           SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
          // String uName = PropertyReader.getProperty("Liteusername");
           txtBoxUserName.clear();
           txtBoxUserName.sendKeys(uName1);
           Log.message("Username: " + uName1 + " " + "has been entered in Username text box." );
           //String pWord = PropertyReader.getProperty("Password");
           txtBoxPassword.clear();
           txtBoxPassword.sendKeys(pWord1);
           Log.message("Password: " + pWord1 + " " + "has been entered in Password text box." );
           SkySiteUtils.waitTill(10000);
           SkySiteUtils.waitForElement(driver, btnLogin, 60);
           btnLogin.click();
           Log.message("LogIn button clicked.");           
          // SkySiteUtils.waitForElement(driver, buttonAddCollection, 40);
           return new FnaHomePage(driver).get();        
    }   
    
    /** 
     * Method written for login with PIN | Scripted by TRINANJWAN
    * @throws AWTException 
     */
    
    
    @FindBy(css = "#txtUserPin")
	WebElement Pinfield;
    
    @FindBy(css = "#btnPin")
   	WebElement Pinsubmitbtn;
    
    
    public FnaHomePage loginWithPIN(String Pin) throws AWTException
    
    {
    	SkySiteUtils.waitForElement(driver, Pinfield, 60);
    	Log.message("Pin field is displayed now");
    	SkySiteUtils.waitTill(5000);
    	Pinfield.clear();
    	Log.message("Pin field is clear now");
    	Pinfield.sendKeys(Pin);
    	Log.message(Pin+" is provided in the Pin field");
    	SkySiteUtils.waitForElement(driver, Pinsubmitbtn, 60);
    	Log.message("Pin submit button is displayed now");
    	Pinsubmitbtn.click();
    	Log.message("Clicked on Pin Submit button");
    	
    	 return new FnaHomePage(driver).get();
    }
    
    
    /** 
     * Method written to verify lock message for providing 5times wrong password | Scripted by TRINANJWAN
     ** @throws AWTException
     */
    
    @FindBy(css = "#dvAccountLocked > span:nth-child(1)")
   	@CacheLookup
   	WebElement Lockmessagesection;
    
  
    public boolean passwordLockMessageVerification() throws AWTException
    
    {
    	SkySiteUtils.waitForElement(driver, Lockmessagesection, 15);
    	Log.message("The lock message is visible now");
    	String actuallockmessage=Lockmessagesection.getText();
    	Log.message("Actual message is:"+actuallockmessage);
    	String expectedlockmessage="Your account has been locked due to too many invalid login attempts. A reset password link has been sent to the registered email. Please follow the email instructions to unlock and access your account.";
    	Log.message("Expected message is:"+expectedlockmessage);
    	if(actuallockmessage.contentEquals(expectedlockmessage))
    		return true;
    	else
    		return false;
 	
    }
    

    
    /** 
     * Method written to verify lock message for providing 5times wrong PIN | Scripted by TRINANJWAN
     ** @throws AWTException
     */
    
    @FindBy(css = "#dvAccountLocked > span:nth-child(1)")
   	@CacheLookup
   	WebElement LockmessagesectionPIN;
    
  
    public boolean passwordLockMessageVerificationpin() throws AWTException
    
    {
    	SkySiteUtils.waitForElement(driver, LockmessagesectionPIN, 15);
    	Log.message("The lock message is visible now");
    	String actuallockmessage=LockmessagesectionPIN.getText();
    	Log.message("Actual message is:"+actuallockmessage);
    	String expectedlockmessage="Your account is locked due to invalid login attempt. A reset PIN link has been sent to your email. Please follow the email instruction to unlock your account.";
    	Log.message("Expected message is:"+expectedlockmessage);
    	if(actuallockmessage.contentEquals(expectedlockmessage))
    		return true;
    	else
    		return false;
 	
    }
    
    /** 
     * Method written to handle the notification alert | Scripted by TRINANJWAN
     */
    
    
    public void handleToastMessage()
    
    {

    			SkySiteUtils.waitTill(5000);
				int Feedback_Alert = driver.findElements(By.cssSelector(".noty_close")).size();
				Log.message("Checking cross button is there or not : " + Feedback_Alert);
				if (Feedback_Alert > 0) {
					driver.findElement(By.cssSelector(".noty_close")).click();
					Log.message("Clicked on closed button!!!");
				}
			}
    
    
    /** 
     * Method written to verify the Update Password section for the employee | Scripted by TRINANJWAN
     */  
    
    @FindBy(css = "#txtCurrentPassword")
   	@CacheLookup
   	WebElement UpdatePasswordfield;
    
    @FindBy(css = "#txtNewPassword")
   	@CacheLookup
   	WebElement NewPasswordfield;
   
    public boolean verificationofUpdatePasswordsection()
    
    {
    	SkySiteUtils.waitForElement(driver, UpdatePasswordfield, 10);
    	Log.message("Update Password field is displayed");
    	
    	SkySiteUtils.waitForElement(driver, NewPasswordfield, 10);
    	Log.message("New Password field is displayed");
    	
    	if(UpdatePasswordfield.isDisplayed() && NewPasswordfield.isDisplayed())
    		return true;
    	else
    		return false;
    
    }
    
    
    /** 
     * Method written to login with Security Question for Password | Scripted by TRINANJWAN
     */  
    
    //Relative Xpath used for this
    @FindBy(xpath = "//p[text()='Please answer your security question']/following-sibling::p")
   	@CacheLookup
   	WebElement SecurityQuesSection;
    
    @FindBy(css = "#txtSecurityAnswer")
   	@CacheLookup
   	WebElement Answerfield;
    
    @FindBy(css = "#btnLogin")
   	@CacheLookup
   	WebElement SignInButton;
   

    public void loginWithSecurityQues()
    
    {
    	SkySiteUtils.waitForElement(driver, SecurityQuesSection, 15);
    	Log.message("The Security Question Section is displayed now in the login page");
    	
    	String ActualSecurityQuestion=SecurityQuesSection.getText();
    	Log.message("Actual Question is:"+ActualSecurityQuestion);
    	
    	String ExpectedSecurityQuestion1 = PropertyReader.getProperty("Ques1tri");
    	String ExpectedSecurityQuestion2 = PropertyReader.getProperty("Ques2tri");
    	String ExpectedSecurityQuestion3 = PropertyReader.getProperty("Ques3tri");
    	
    	String Answer1 = PropertyReader.getProperty("Answer1tri");
    	String Answer2 = PropertyReader.getProperty("Answer2tri");
    	String Answer3 = PropertyReader.getProperty("Answer3tri");
    	
    	String Password = PropertyReader.getProperty("complexpasswordtri");

    	if(ActualSecurityQuestion.contentEquals(ExpectedSecurityQuestion1))
    	{
    		SkySiteUtils.waitForElement(driver, Answerfield, 15);
    		Answerfield.sendKeys(Answer1);
    		Log.message("Answer is:"+Answer1);
    		SkySiteUtils.waitForElement(driver, txtBoxPassword, 15);
    		txtBoxPassword.clear();
    		txtBoxPassword.sendKeys(Password);
    		Log.message("Password is "+Password+" provided in the password field");
    		SignInButton.click();
    		Log.message("Clicked on Sign in button");
    		
    	}
    	
    	else if(ActualSecurityQuestion.contentEquals(ExpectedSecurityQuestion2))
    	{
    		SkySiteUtils.waitForElement(driver, Answerfield, 15);
    		Answerfield.sendKeys(Answer2);
    		Log.message("Answer is:"+Answer2);
    		SkySiteUtils.waitForElement(driver, txtBoxPassword, 15);
    		txtBoxPassword.clear();
    		txtBoxPassword.sendKeys(Password);
    		Log.message("Password is "+Password+" provided in the password field");
    		SignInButton.click();
    		Log.message("Clicked on Sign in button");
    		
    	}
    	
    	else if(ActualSecurityQuestion.contentEquals(ExpectedSecurityQuestion3))
    	{
    		SkySiteUtils.waitForElement(driver, Answerfield, 15);
    		Answerfield.sendKeys(Answer3);
    		Log.message("Answer is:"+Answer3);
    		SkySiteUtils.waitForElement(driver, txtBoxPassword, 15);
    		txtBoxPassword.clear();
    		txtBoxPassword.sendKeys(Password);
    		Log.message("Password is "+Password+" provided in the password field");
    		SignInButton.click();
    		Log.message("Clicked on Sign in button");
    		
    	}
    	
    }
    
    /** 
     * Method written to login with Security Question for PIN | Scripted by TRINANJWAN
     */  
    
    
 public void loginWithSecurityQues_pin()
    
    {
    	SkySiteUtils.waitForElement(driver, SecurityQuesSection, 15);
    	Log.message("The Security Question Section is displayed now in the login page");
    	
    	String ActualSecurityQuestion=SecurityQuesSection.getText();
    	Log.message("Actual Question is:"+ActualSecurityQuestion);
    	
    	String ExpectedSecurityQuestion1 = PropertyReader.getProperty("Ques1tri");
    	String ExpectedSecurityQuestion2 = PropertyReader.getProperty("Ques2tri");
    	String ExpectedSecurityQuestion3 = PropertyReader.getProperty("Ques3tri");
    	
    	String Answer1 = PropertyReader.getProperty("Answer1tri");
    	String Answer2 = PropertyReader.getProperty("Answer2tri");
    	String Answer3 = PropertyReader.getProperty("Answer3tri");
    	
    	String Pin = PropertyReader.getProperty("pinoritri");

    	if(ActualSecurityQuestion.contentEquals(ExpectedSecurityQuestion1))
    	{
    		SkySiteUtils.waitForElement(driver, Answerfield, 15);
    		Answerfield.sendKeys(Answer1);
    		Log.message("Answer is:"+Answer1);
    		SkySiteUtils.waitForElement(driver, Pinfield, 15);
    		Pinfield.clear();
    		Pinfield.sendKeys(Pin);
    		Log.message("Pin is "+Pin+" provided in the password field");
    		Pinsubmitbtn.click();
    		Log.message("Clicked on Sign in button");
    		
    	}
    	
    	else if(ActualSecurityQuestion.contentEquals(ExpectedSecurityQuestion2))
    	{
    		SkySiteUtils.waitForElement(driver, Answerfield, 15);
    		Answerfield.sendKeys(Answer2);
    		Log.message("Answer is:"+Answer2);
    		SkySiteUtils.waitForElement(driver, Pinfield, 15);
    		Pinfield.clear();
    		Pinfield.sendKeys(Pin);
    		Log.message("Pin is "+Pin+" provided in the password field");
    		Pinsubmitbtn.click();
    		Log.message("Clicked on Sign in button");
    		
    	}
    	
    	else if(ActualSecurityQuestion.contentEquals(ExpectedSecurityQuestion3))
    	
    	{
    		SkySiteUtils.waitForElement(driver, Answerfield, 15);
    		Answerfield.sendKeys(Answer3);
    		Log.message("Answer is:"+Answer3);
    		SkySiteUtils.waitForElement(driver, Pinfield, 15);
    		Pinfield.clear();
    		Pinfield.sendKeys(Pin);
    		Log.message("Pin is "+Pin+" provided in the password field");
    		Pinsubmitbtn.click();
    		Log.message("Clicked on Sign in button");
    		
    	}
    	
    }
    
    
    
    }
    

