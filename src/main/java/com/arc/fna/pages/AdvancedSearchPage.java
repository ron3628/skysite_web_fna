package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class AdvancedSearchPage extends LoadableComponent<AdvancedSearchPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;		
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public AdvancedSearchPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(css = "#Button6")
	WebElement btnadvsearchicon;
	
	@FindBy(css = "#txtContentSearch")
	WebElement txtContentSearch;
	
	@FindBy(css = "#btnAdvSearch")
	WebElement btnAdvSearch;
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[4]")
	WebElement viewkeyword;
	
	@FindBy(xpath = ".//*[@id='divContentGrid']/li/label")
	WebElement txtContentGrid;
	
	/** 
	 * Method written content search
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean content_search(String Contentname)
	{	
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	btnadvsearchicon.click();
	 	Log.message("advanced search icon button has been clicked");
	 	SkySiteUtils.waitTill(3000);				
	 	txtContentSearch.sendKeys(Contentname);
		Log.message("search keyword has been entered");
	 	SkySiteUtils.waitTill(3000);
	 	btnAdvSearch.click();
	 	Log.message("Search button has been clicked");
	 	SkySiteUtils.waitTill(5000);
	 	if(viewkeyword.isDisplayed())
	 	{
	 		result1=true;
	 		Log.message("Search is successfully");		
	 		
	 	}
	 	else
	 	{
	 		result1=false;
	 	}	
	 	SkySiteUtils.waitTill(3000);
	 	viewkeyword.click();
	 	Log.message("view keyword button has been clicked");
	 	SkySiteUtils.waitTill(5000);
 		String contentname= txtContentGrid.getText();
 		Log.message("Content name is:"+contentname);
 		if(contentname.contains(Contentname))
 		{
 			result2=true;
 		}
 		else
 		{
 			result2=false;
 		}
 		if((result1==true)&&(result2==true))
 			return true;
 		else
 			return false;
	}
	
	
	@FindBy(xpath = "//*[@id='chkAllProjectdocs']")
	WebElement chkAllProjectdocs;
	
	@FindBy(xpath = "//*[@id='Button1']")
	WebElement btnmore;
	
	@FindBy(css = "#liSetAttribute1>a")
	WebElement liSetAttribute1;
	
	@FindBy(xpath = "//*[@id='btnPreviewReport']")
	WebElement btnPreviewReport;
	
	@FindBy(xpath = "//*[@id='btnBackFromReport']")
	WebElement btnBackFromReport;
	
	@FindBy(xpath = "//input[@class='dhxcombo_input']")
	WebElement documenttype;
	
	@FindBy(xpath = ".//*[@id='btnSaveNClose']")
	WebElement btnSaveNClose;
	
	/** 
	 * Method written Edit attribute preview
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean EditAttributes_Preview(String DocumenttypeName)
	{	
		boolean result1 = false;		
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		chkAllProjectdocs.click();
		Log.message("all select check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liSetAttribute1.click();
		Log.message("modify attribute has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
		SkySiteUtils.waitTill(3000);
		int filename_Count = driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td")).size();
		Log.message("File name Count is:" + filename_Count);
		SkySiteUtils.waitTill(5000);
		btnPreviewReport.click();
		Log.message("preview button has been clicked");
		SkySiteUtils.waitTill(3000);
		int Preview_filename_Count = driver.findElements(By.xpath("//*[@id='divPreviewReportGrid']/div[2]/table/tbody/tr/td[1]")).size();
		Log.message("Preview file name Count is:" + Preview_filename_Count);
		if(filename_Count==Preview_filename_Count)
		{
			result1=true;
			Log.message("selected Preview files successfully");
		}
		else
		{
			result1=false;
			Log.message("selected Preview files got failed");
		}
		SkySiteUtils.waitTill(3000);
		btnBackFromReport.click();
		Log.message("back button has been clicked");
		SkySiteUtils.waitTill(3000);
		documenttype.sendKeys(DocumenttypeName);
		Log.message("document type has been entered");
		SkySiteUtils.waitTill(3000);
		btnSaveNClose.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(7000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(7000);
		String docuname = driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[8]")).getText();
		Log.message("Document type name is:"+docuname);
		SkySiteUtils.waitTill(3000);
		if((docuname.contains(DocumenttypeName))&&(result1==true))
			return true;
		else
			return false;		
	}	
	
	/** 
	 * Method written Modify attribute search
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean ModifyAttributes_Search(String DocumenttypeName)
	{	
		boolean result1 = false;	
		boolean result2 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		chkAllProjectdocs.click();
		Log.message("all select check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liSetAttribute1.click();
		Log.message("modify attribute has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
		SkySiteUtils.waitTill(3000);
		documenttype.sendKeys(DocumenttypeName);
		Log.message("document type has been entered");
		SkySiteUtils.waitTill(3000);
		btnSaveNClose.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(7000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(7000);
		String docuname = driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[8]")).getText();
		Log.message("Document type name is:"+docuname);
		SkySiteUtils.waitTill(3000);
		if(docuname.contains(DocumenttypeName))
		{
			result1=true;
			Log.message("Modify attribute successfully");			
		}
		else
		{
			result1=false;
			Log.message("Modify attribute got failed");
		}
		SkySiteUtils.waitTill(3000);
		btnadvsearchicon.click();
		Log.message("Advanced search button has been clicked");
		SkySiteUtils.waitTill(50000);
		documenttype.sendKeys(DocumenttypeName);
		Log.message("document type has been entered");
		SkySiteUtils.waitTill(15000);
		btnAdvSearch.click();
		String docuname1 = driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[8]")).getText();
		Log.message("Document type name is:"+docuname1);
		SkySiteUtils.waitTill(3000);
		if(docuname1.contains(DocumenttypeName))
		{
			result2=true;
			Log.message("Modify attribute search successfully");
		}
		else
		{
			result2=false;
			Log.message("Modify attribute search got failed");
		}
		if((result1==true)&&(result2==true))
			return true;
		else
			return false;		
	}
	
	@FindBy(xpath = "//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span")
	WebElement btn1stfolder;
	
	@FindBy(xpath = "//*[@id='Button6']")	
	WebElement btnButton6;
	
	/** 
	 * Method written Sub folder search results
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean Subfolder_searchresults(String FolderName,String DocumenttypeName)
	{			
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		Log.message("Switch to frame");		
		driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[3]")).click();
		Log.message("Exp Folder name is selection Successfully!!!!");			
		SkySiteUtils.waitTill(5000);
		btnButton6.click();
		Log.message("advanced search icon button has been clicked");
		SkySiteUtils.waitTill(3000);
		documenttype.sendKeys(DocumenttypeName);
		SkySiteUtils.waitTill(50000);
		btnAdvSearch.click();
		SkySiteUtils.waitTill(5000);
		String docuname1 = driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[8]")).getText();
		Log.message("Document type name is:"+docuname1);
		if(docuname1.contains(DocumenttypeName))	
		{			
			Log.message("Advanced search successfully");		
			return true;
		}
		else
		{		
			Log.message("Advanced search got failed");	
			return false;
		}			
	}
	
	@FindBy(xpath = "//*[@id='lftpnlMore']")
	WebElement lftpnlMore;
	
	@FindBy(css = "#tdMoveFolder>a")
	WebElement tdMoveFolder;
	
	
	@FindBy(xpath = "//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement movefolder1st;
	
	@FindBy(xpath = "//*[@id='popup']/div/div/div[3]/div[2]/input[1]")
	WebElement btnok;
	
	@FindBy(xpath = "//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")
	WebElement selfolder2nd;
	
	/** 
	 * Method written Sub folder search results
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean Subfolder_Movetodiffolder_searchresults(String DocumenttypeName)
	{			
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		Log.message("Switch to frame");			
		lftpnlMore.click();
		Log.message("Folder more option button has been clicked ");	
		SkySiteUtils.waitTill(3000);
		tdMoveFolder.click();
		Log.message("Folder more option button has been clicked ");	
		SkySiteUtils.waitTill(3000);		
		movefolder1st.click();
		Log.message("select 1stFolder has been clicked ");	
		SkySiteUtils.waitTill(3000);
		btnok.click();
		Log.message("Ok button has been clicked ");	
		SkySiteUtils.waitTill(3000);
		selfolder2nd.click();
		Log.message("2nd folder has been clicked ");	
		SkySiteUtils.waitTill(3000);		
		btnButton6.click();
		Log.message("advanced search icon button has been clicked");
		SkySiteUtils.waitTill(3000);
		documenttype.sendKeys(DocumenttypeName);
		SkySiteUtils.waitTill(50000);
		btnAdvSearch.click();
		Log.message("search icon button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		String Notify_msg=driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		System.out.println("Notify message is:"+Notify_msg);
		if(Notify_msg.contains("No result found"))
		{			
			Log.message("attribute search using 1st foot folder Not Found Result Coming:::");		
			return true;
		}
		else
		{		
			Log.message("attribute search in 1st foot folder Not Found Result  NOT Coming:::");	
			return false;
		}			
	}
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement filename1st;
	
	/** 
	 * Method written Sub folder search results
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean Sorting_AfterAdvancedresults(String DocumenttypeName)
	{			
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		Log.message("Switch to frame");				
		String Before_filename = filename1st.getText();
		Log.message("Before file name is:" + Before_filename);
		SkySiteUtils.waitTill(6000);		
		int Filecount = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("File count is:" + Filecount);
		SkySiteUtils.waitTill(3000);
		btnButton6.click();
		Log.message("advanced search icon button has been clicked");
		SkySiteUtils.waitTill(3000);
		documenttype.sendKeys(DocumenttypeName);
		Log.message("document name has been entered");
		SkySiteUtils.waitTill(50000);
		btnAdvSearch.click();
		Log.message("search icon button has been clicked");
		SkySiteUtils.waitTill(5000);		
		String After_FileName = driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2]/a[3])[" + Filecount + "]")).getText();
		Log.message("After search the file name is:" + After_FileName);
		SkySiteUtils.waitTill(5000);
		if(Before_filename.equals(After_FileName))
			return true;
		else
			return false;		
	}
	
	
}
