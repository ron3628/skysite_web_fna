package com.arc.fna.pages;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FnaCreateGroupPage extends LoadableComponent<FnaCreateGroupPage> {
WebDriver driver;
private  boolean isPageLoaded;

/** Find Elements using FindBy annotation
 * 
 */
@FindBy(css="#btnAddTUFromAB")
@CacheLookup
WebElement Addgrpmemberbutton;
	

@FindBy(css="#txtGroupName")
@CacheLookup
WebElement GroupNamefield;
	
@FindBy(css="#txtGroupNotes")
@CacheLookup
WebElement GroupNotefield;	

@FindBy(css="#btnSaveGroup")
@CacheLookup
WebElement Savebtn;

@FindBy(xpath="//*[@id='btn_back']")
@CacheLookup
WebElement Backbtn;

@FindBy(css="#btnNewContact")
@CacheLookup
WebElement Addnewcntct1;

@FindBy(css="#txtSerachValue")
@CacheLookup
WebElement Addressbooksearchfield;




@FindBy(css="#btnCreateGroup")
WebElement CreateGrpbtn;
@FindBy(css="#btnNewContact")
WebElement NewContactBtnforgroup;
@FindBy(css="#ctl00_DefaultContent_rdoHostedUserType")
WebElement Employeebtn;
@FindBy(css="#ctl00_DefaultContent_txtFirstName")
WebElement Firstname;
@FindBy(css="#ctl00_DefaultContent_txtLastName")
WebElement Lastname;
@FindBy(css="#ctl00_DefaultContent_txtCity")
WebElement city;
@FindBy(css="#ctl00_DefaultContent_txtZip")
WebElement Postalcode;
@FindBy(css="#ctl00_DefaultContent_txtTitle")
WebElement Title;
@FindBy(css="#ctl00_DefaultContent_ucCountryState_ddlCountry")
WebElement Country;
@FindBy(css="#ctl00_DefaultContent_txtCompanyName")
WebElement Company;
@FindBy(css="#ctl00_DefaultContent_ucCountryState_ddlState")
WebElement statedropdown;
@FindBy(css="#ctl00_DefaultContent_txtAddress1")
WebElement Address1;
@FindBy(css="#ctl00_DefaultContent_txtEmail")
WebElement Email;
@FindBy(css="#ctl00_DefaultContent_txtAddress2")
WebElement Address2;
@FindBy(css="#ctl00_DefaultContent_txtPhoneWork")
WebElement PhoneNo;
@FindBy(css="#ctl00_DefaultContent_txtWebURL")
WebElement Companywebsite;
@FindBy(css="#ctl00_DefaultContent_ddlProjectRole")
WebElement ProjectRole;
@FindBy(css="#ctl00_DefaultContent_DDLCompBussiness")
WebElement Buisness;
@FindBy(css="#ctl00_DefaultContent_DDLCompOccupation") 
WebElement Occupation;
@FindBy(css="#ctl00_DefaultContent_Button3")
WebElement Savebutton;
@FindBy(css=".//*[@id='txtSerachValue']")
WebElement Searchbox;

@FindBy(css="#btnNewContact")
@CacheLookup
WebElement Addnewcntctbtn;

@FindBy(css="#txtSerachValue")
@CacheLookup
WebElement Addressbooksearch;

@FindBy(xpath="/html/body/form/div[3]/div/div/div/div[2]/div[1]/nav/div[2]/div/div/div/button") 
@CacheLookup
WebElement Searchtypedropdown;

@FindBy(css="#btnSearch")
@CacheLookup
WebElement Addressbooksearchbutton;

@FindBy(css="#chkAllPT")
@CacheLookup
WebElement Selectthecontactfromlist;


@FindBy(css="#btnAddClose")
@CacheLookup
WebElement Addandclosebutton;





    @Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;
        driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver,Addgrpmemberbutton,30);
		driver.switchTo().defaultContent();
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FnaCreateGroupPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
    /** Method for New Group Page landing verification
     *  Scripted By :Trinanjwan
     */
	
	public boolean verifylandingofcontactgrouppage() 
	{
		
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched into Frame");
		if(Addgrpmemberbutton.isDisplayed()==true) {
			Log.message("Add group members Button is displayed");
			return true;
		}
			return false;
	}
	
	
	
		    /** Method for creating new group
		     *  Scripted By :Trinanjwan
		     */
				
		public boolean create_newgroup(String Groupname) throws AWTException
	{
	
	    
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		GroupNamefield.clear();
		GroupNamefield.sendKeys(Groupname);
		Log.message("Group Name: "+Groupname + " " + "has been entered in GROUP NAME text box." );
		SkySiteUtils.waitTill(5000);
		String GroupNote = PropertyReader.getProperty("GroupNote");
		GroupNotefield.clear();
		GroupNotefield.sendKeys(GroupNote);
		Log.message("Group Name: "+GroupNote + " " + "has been entered in GROUP NOTE text box." );
		SkySiteUtils.waitTill(2000);
		Savebtn.click();
		Log.message("Clicked on the Save button to create the group");
		SkySiteUtils.waitTill(10000);
		String group_name = GroupNamefield.getAttribute("value");
		SkySiteUtils.waitTill(2000);
		Log.message("Group name is:"+group_name);
		SkySiteUtils.waitTill(2000);
		if((group_name).contains(Groupname))
		{		
			Log.message("Adding the Group validation successfull ");
			return true;
		}	
		else
		{		
			Log.message("Adding the Group validation Unsuccessfull ");	
			return false;
		}		
	}
		
		
	    /** Method for addition of contact in the group
	     *  Scripted By :Trinanjwan
	     */
 		
		
		
		
		public void additionOfNewContactInGroup(String Newcontactname, String newlastname, String newemail ) throws AWTException
		
		{	
		SkySiteUtils.waitTill(5000);
		Addgrpmemberbutton.click();
		Log.message("Clicked on the Create Group Member button");
		
		SkySiteUtils.waitForElement(driver, Addnewcntctbtn, 20);
		Log.message("Address Book pop over is populated");
		SkySiteUtils.waitTill(5000);
		Addnewcntct1.click();
		Log.message("Clicked on Add New Contact button");
		
		driver.switchTo().defaultContent();//switch to default window after adress page landed
		driver.switchTo().frame("myFrame");//Again switch to frame to click the addnewcontact button
		NewContactBtnforgroup.click();
		Log.message("AddNewContact button is clicked");
		SkySiteUtils.waitTill(8000);
		String MainWindow=driver.getWindowHandle();
		Log.message(MainWindow);
		for(String childwindow:driver.getWindowHandles())
		{
			driver.switchTo().window(childwindow);//swtich to child window
		}
		Employeebtn.click();
		Log.message("Employee type user is selected");
		SkySiteUtils.waitTill(5000);
		Firstname.click();
		Firstname.sendKeys(Newcontactname);
		Log.message("Contactname entered is : " +Newcontactname);
		SkySiteUtils.waitTill(5000);
		Lastname.sendKeys(newlastname);
		Log.message("Lastname entered is " +newlastname);
		SkySiteUtils.waitTill(5000);
		String CityName=PropertyReader.getProperty("City");
		city.sendKeys(CityName);
		Log.message("City entered is " +CityName);
		SkySiteUtils.waitTill(5000);
		String Pcode=PropertyReader.getProperty("Postcode");
		Postalcode.sendKeys(Pcode);
		Log.message("postal code entered is " +Pcode);
		SkySiteUtils.waitTill(5000);
		String TName=PropertyReader.getProperty("Titlename");
		Title.sendKeys(TName);
		Log.message("Title entered is " +TName);
		SkySiteUtils.waitTill(5000);
		String Cname=PropertyReader.getProperty("Countryname");
		Select sc=new Select(Country);
		sc.selectByVisibleText(Cname);
		Log.message("Country selected is :" +Cname);
		SkySiteUtils.waitTill(5000);
		String Companynm=PropertyReader.getProperty("Company");
		Company.sendKeys(Companynm);
		Log.message("Companyname entered is " +Companynm);
		SkySiteUtils.waitTill(5000);
		String statenm=PropertyReader.getProperty("Statenm");
		Select selectstate=new Select(statedropdown);
	    selectstate.selectByVisibleText(statenm);
	    Log.message("State selected is " +statenm);
		SkySiteUtils.waitTill(5000);
	    String addressfirst=PropertyReader.getProperty("Address1");
	    Address1.sendKeys(addressfirst);
	    Log.message("Address1 entered is" +addressfirst);
		SkySiteUtils.waitTill(5000);
        Email.sendKeys(newemail);
        Log.message("Email id entered is" +newemail);
    	SkySiteUtils.waitTill(5000);
        String addresssecond=PropertyReader.getProperty("Address2");
        Address2.sendKeys(addresssecond);
        Log.message("Address2 entered is " +addresssecond);
    	SkySiteUtils.waitTill(5000);
        String Phnno=PropertyReader.getProperty("Phoneno");
        PhoneNo.sendKeys(Phnno);
        Log.message("Phone no entered is : " +Phnno);
    	SkySiteUtils.waitTill(5000);
        String Compwebsite=PropertyReader.getProperty("CompanyWebSite");
        Companywebsite.sendKeys(Compwebsite);
        Log.message("Company website entered : " +Compwebsite);
    	SkySiteUtils.waitTill(5000);
    	JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        Log.message("Scrolled down to window");
        SkySiteUtils.waitTill(5000);
        String Projectroll=PropertyReader.getProperty("Projectrole");
        Select scrole=new Select(ProjectRole);
        scrole.selectByVisibleText(Projectroll);
    	SkySiteUtils.waitTill(5000);
        Log.message("ProjectRoll selected is " +Projectroll);
        String Buisnessname=PropertyReader.getProperty("Buisnessdetails");
        Select selectBuisness=new Select(Buisness);
        selectBuisness.selectByVisibleText(Buisnessname);
        Log.message("Buisness selected is " +Buisnessname);
        SkySiteUtils.waitTill(7000);
        Select selectoccupation=new Select(Occupation);
        selectoccupation.selectByIndex(2);
        Log.message("Occupation is selected");
        SkySiteUtils.waitTill(10000);
        Savebutton.click();
        Log.message("Save button is clicked");
        SkySiteUtils.waitTill(5000);
        driver.switchTo().window(MainWindow);
        Log.message("Switched to main window");
        SkySiteUtils.waitTill(5000);
        driver.switchTo().defaultContent();
        Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to My Frame");
        SkySiteUtils.waitTill(10000);
        Addressbooksearch.clear();
        Log.message("Search field is getting cleared");
        Addressbooksearch.sendKeys(Newcontactname);
        Log.message(Newcontactname+" entered in the field to search");
        SkySiteUtils.waitTill(5000);
        Addressbooksearchbutton.click();
        Log.message("Address book search button is clicked");
        SkySiteUtils.waitTill(6000);
        
        
        Selectthecontactfromlist.click();
        Log.message("Contact name is selected from the list");
        SkySiteUtils.waitTill(5000);
        Addandclosebutton.click();
        Log.message("Clicked on Add & Close button");
        SkySiteUtils.waitTill(10000);
        
		}
		
	    /** Method for click on back button and land in Contact page/Address book page
	     *  Scripted By :Trinanjwan
	     */
		
		public void clickingOnBackBtn() throws AWTException
		
		{
			SkySiteUtils.waitForElement(driver, Backbtn, 40);	
			Log.message("Clicked on the backed button and landed in Contact TAB page");
			Backbtn.click();
			SkySiteUtils.waitTill(7000);
				
		}
		

      

	    /** Method for creating random group name
	     *  Scripted By :Trinanjwan
	     */
	
	
		public String generateRandomgroupname()
		
		{
		 String str="Grpname";
		 Random r = new Random( System.currentTimeMillis() );
		 return str+String.valueOf((10000 + r.nextInt(20000)));
		}



	/** Method for verification of added contact in group
	 *  Scripted By :Trinanjwan
	 */


	public boolean verificationOfaddedCntinGrp()
	
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(2000);
		int Count_list1=driver.findElements(By.xpath("//*[@id=\"divGrid\"]/div[2]/table/tbody/tr/td[1]/img")).size();
	    SkySiteUtils.waitTill(5000);					
		Log.message("Count is:"+Count_list1);	
		String Countfromtestdata = PropertyReader.getProperty("Countdata");
		int total_count = Integer.parseInt(Countfromtestdata);
		 Log.message("Total count is:" +total_count);
		
		 if (Count_list1==total_count)
		 {
			 Log.message("The total count of the contact is correct");
			 return true;
		 }
		 	 
		 else
		 {
		Log.message("The total count of the contact is not correct");
		return false;
		}	
	
	}	
}
	