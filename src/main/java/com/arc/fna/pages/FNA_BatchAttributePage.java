package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.gargoylesoftware.htmlunit.javascript.host.Set;

public class FNA_BatchAttributePage extends LoadableComponent<FNA_BatchAttributePage> {
	 WebDriver driver;
	    private  boolean isPageLoaded;
	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FNA_BatchAttributePage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//@FindBy(css = "#Button1")
  //@FindBy(css = "#rgtpnlMore")
   // WebElement moreoption;
	// @FindBy(css="#rgtpnlMore")
	  @CacheLookup
	  @FindBy(xpath=".//*[@id='rgtpnlMore']")
	  WebElement moreoption;
	
	 @FindBy(xpath="//*[contains(text(),'Modify attributes')]")	 
	  WebElement modifyattribute;

	 @FindBy (xpath = "//*[@id='rgtpnlMore']")
     WebElement moreLocal ;

     @FindBy(xpath = "//ul[@id='FileActionMenu']/li/a[contains( text(),'Delete')]")
     WebElement deletefilesoptionLocal;


     @FindBy(css = "#Button1")
    WebElement moreoptionfordelete;


     @FindBy(css = "#liDeleteFile1")
     WebElement deletefilesoption;

	 
	 
	/**Method to select a file and open modify attirbute  window
	 * Scripted by tarak
	 */
	public void selectFileandModifyAttribute(String Foldername) 
	{
		 	
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
	 		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched to frame");
	 		SkySiteUtils.waitTill(4000);
	 	
	 		int Count_no = driver.findElements(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span")).size();
	 		Log.message("the number folder present are " + Count_no);
	 		SkySiteUtils.waitTill(5000);
	 		int k = 0;
	 		for (k = 1; k <= Count_no; k++) 
	 		{

	 			String foldernamepresent = driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+k+"]")).getText();
	 			if (foldernamepresent.contains(Foldername)) {
	 				Log.message("Required folder is present at " + k + " position in the tree");
	 				driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+ k + "]")).click();
	 				Log.message("Folder is selected");
	 				SkySiteUtils.waitTill(5000);
	 				break;
	 			}
	 		}
	 	
	 		int Itemlist = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("the number files present are " + Itemlist);
			SkySiteUtils.waitTill(5000);
			
			int j = 0;
			for (j = 1; j <= Itemlist; j++) 
			{
					driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1])["+j+"]")).click();
					Log.message("File is selected ");
					SkySiteUtils.waitTill(4000);
					break;
					
		     }
			
			if (moreLocal.isDisplayed()) 
			{
                moreLocal.click();
                Log.message("Clicked on Local more");
                SkySiteUtils.waitTill(5000);
                
			}
			else 
			{
                moreoptionfordelete.click();
                Log.message("Clicked on remote more");
                SkySiteUtils.waitTill(5000);
			}

			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver, modifyattribute, 50);			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", modifyattribute);
			Log.message("modify attribute is clicked");
            SkySiteUtils.waitTill(6000);
			
		}
	
	
	
	/**Method to generate file name
	 * Scripted By:Tarak
	 * @return
	 */
	  public String Random_filename()
	    {
	           
	           String str="Filename";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	  /**Method to generate Title 
		 * Scripted By:Tarak
		 * @return
		 */
		  public String Random_title()
		    {
		           
		           String str="Title";
		           Random r = new Random( System.currentTimeMillis() );
		           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
		           return abc;
		           
		    }
		  /**Method to generate description 
			 * Scripted By:Tarak
			 * @return
			 */
			  public String Random_description()
			    {
			           
			           String str="Description";
			           Random r = new Random( System.currentTimeMillis() );
			           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
			           return abc;
			           
			    }
/**Method to generate Project No 
 * Scripted By:Tarak
	 * @return
 */
  public String Random_Projectno()
	  {
				          
				   String str="ProjectNo";
				   Random r = new Random( System.currentTimeMillis() );
				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
				   return abc;
				           
	  }
  /**Method to generate Project No 
   * Scripted By:Tarak
  	 * @return
   */
    public String Random_UpdateProjectno()
  	  {
  				          
  				   String str="UpdatedProjectNo";
  				   Random r = new Random( System.currentTimeMillis() );
  				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
  				   return abc;
  				           
  	  }
    /**Method to generate Project No 
     * Scripted By:Tarak
    	 * @return
     */
      public String Random_UpdateVendor()
    	  {
    				          
    				   String str="UpdatedVendorno";
    				   Random r = new Random( System.currentTimeMillis() );
    				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
    				   return abc;
    				           
    	  }
  /**Method to generate Project Name 
   * Scripted By:Tarak
  	 * @return
   */
    public String Random_Projectname()
  	  {
  				          
  				   String str="ProjectName";
  				   Random r = new Random( System.currentTimeMillis() );
  				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
  				   return abc;
  				           
  	  }
    /**Method to generate Invoice
     * Scripted By:Tarak
    	 * @return
     */
      public String Random_Invoice()
    	  {
    				          
    				   String str="Invoice";
    				   Random r = new Random( System.currentTimeMillis() );
    				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
    				   return abc;
    				           
    	  }
       

	@FindBy(css="#txt_fileName")
	@CacheLookup
	WebElement filename;
	@FindBy(css="#txt_title")
	@CacheLookup
	WebElement Title;
	@FindBy(css="#txt_desc")
	@CacheLookup
	WebElement Description;
	@FindBy(xpath="//input[@value='Update & close']")
	@CacheLookup
	WebElement updateclosebtn;
	
	/**Method to modify general attributes for file
	 * Scripted by Tarak
	 */
	public boolean modifyGeneralAttributes(String filerename, String title, String description) 
	{
		SkySiteUtils.waitTill(3000);
		filename.clear();
		Log.message("Filename is cleared");
		filename.sendKeys(filerename);
		Log.message("Rename for file entered is " +filerename);
		
		SkySiteUtils.waitTill(4000);
		Title.clear();
		Log.message("Title box is cleared");
		Title.sendKeys(title);
		Log.message("Title entered for file is " +title);
		
		SkySiteUtils.waitTill(4000);
		Description.clear();
		Log.message("Description is cleared");
		Description.sendKeys(description);
		Log.message("File description is " +description);
		
		SkySiteUtils.waitTill(4000);
		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);
		
		int filesCount=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +filesCount);
		
		int i=0;
		String filename= null;
		String Titleoffile= null;
		String descriptionofile= null;
		
		for(i=1; i<=filesCount; i++)
		{
			
			filename=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
			SkySiteUtils.waitTill(2500);
			
			Titleoffile=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[5]")).getText();
			SkySiteUtils.waitTill(2500);
			
			descriptionofile=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[6]")).getText();
			SkySiteUtils.waitTill(2500);
			
		}
		
		SkySiteUtils.waitTill(5000);	
		if(filename.contains(filerename)  &&  Titleoffile.contains(title)  &&  descriptionofile.contains(description))	
		{
			Log.message("File general properties are updated");
			return true;
			
		}	
		else 
		{
			Log.message("File general properties are not updated ");		
			return false;
		}
			
	}
	
	@FindBy(css="#tbProjectKeyDate>a")
	@CacheLookup
	WebElement CoustomPropertiestab;
	@FindBy(xpath="//input[@placeholder='Project number']")
	@CacheLookup
	WebElement Projectnofield;
	@FindBy(xpath=".//*[@id='divCustomFiledData']/div/ul/li[4]/div/div[2]/div/div[2]/a/i")
	@CacheLookup
	WebElement Addicon;
	@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[4]/td[1]/img")
	@CacheLookup
	WebElement selectVendoricon;
	@FindBy(xpath=".//*[@id='lookup_combo_10177904']/div/input[1]")
	@CacheLookup
	WebElement VendorInputBox;
	//for production
	//@FindBy(xpath="//*[@tabindex='3']")
	//@FindBy(xpath="//*[@data-id='10174639']")
	//staging
	//@FindBy(xpath="//*[@data-id='10183928']")
	//for production
	@FindBy(xpath="//*[@data-id='10176181']")
	WebElement vendorinputboxupdate;
	//for production
	@FindBy(xpath="//*[@data-id='10176181']")
	//staging new
	//@FindBy(xpath="//*[@data-id='10183928']")
	//Staging old
		//@FindBy(xpath="//*[@data-id='10178919']")
	//*[@data-id='10183928']
	WebElement vendorinputboxformodify;
	
	@FindBy(xpath="//*[@id='divCustomFiledData']/div/ul/li[3]/div/div[2]/div/div[2]/a/i")
	WebElement plusBtnAddVendor;
	
	@FindBy(xpath=".//*[@id='spnCustomLookUpName']")
	WebElement selectVendorPopOver;
	
	@FindBy(xpath=".//*[@id='txtSerachValue']")
	WebElement selectVendorSearchTxtBox;
	
	@FindBy(xpath=".//*[@id='btnSearch']")
	WebElement selectVendorSearchButton;
	
	@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	WebElement selectSearchedVendor;
	
	
	/**Method to modify custom properties:Vendor & Project No
	 * @throws AWTException 
	 * Scripted by : Tarak
	 */
	public boolean customPropertiesVendorAndPNo(String projectno) throws AWTException
	{
		String vendorname=PropertyReader.getProperty("Vendor");
		SkySiteUtils.waitTill(4000);
		CoustomPropertiestab.click();
		Log.message("Custom Properties tab clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,Projectnofield,40);
		Projectnofield.click();
		Log.message("Project no field is clicked");
		SkySiteUtils.waitTill(3000);	
		Projectnofield.sendKeys(projectno);
		Log.message("Project number entered is " +projectno);
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, plusBtnAddVendor, 30);
		plusBtnAddVendor.click();
		Log.message("CLicked on the plus button to add vendor");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, selectVendorPopOver, 30);
		Log.message("Select vendor popover opened");

		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		Log.message("Switched frame");
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 60);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(vendorname);
		Log.message("Vendor name entered for search is " +vendorname);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 30);
		selectVendorSearchButton.click();
		Log.message("Clicked on Search button");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectSearchedVendor, 30);
		selectSearchedVendor.click();
		Log.message("Vendor is added");
		SkySiteUtils.waitTill(8000);
		
		List<WebElement> element = driver.findElements(By.xpath(".//*[@id='btnSaveNClose']"));
		int count = element.size();
		
		if(count>0)
		{
			btnSaveAndClose.click();
			Log.message("CLicked on Save and Close button after selecting vendor");
			SkySiteUtils.waitTill(5000);	
			
		}			
		
		driver.switchTo().defaultContent();
 		Log.message("Switched to default content");
 		SkySiteUtils.waitTill(5000);
 		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 		Log.message("Switched frame");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, updateclosebtn, 30);
 		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);

		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String Projectnopresent=null;
		String vendorpresent=null;
		
		for(int i=1;i<numberoffiles;i++) 
		{
			Projectnopresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[11]")).getText();
			SkySiteUtils.waitTill(2500);
			Log.message("Updated Project no is:- "+Projectnopresent);
			
			vendorpresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
			SkySiteUtils.waitTill(2500);	
			Log.message("Updated Vendor is:- "+vendorpresent);
			
		}
		
		if(projectno.equals(Projectnopresent) && vendorpresent.contains(vendorname))	
		{
			
		    Log.message("File is present after searching with project number and vendor ");
		   return true;
			
		}
		else {
			
			
			Log.message("File is not present after search with project number and vendor");
			return false;
		}
	}
	
	
	
	@FindBy(css = "#lftpnlMore")
	@CacheLookup
	WebElement Moreoption;
	@FindBy(xpath=".//*[@onclick='javascript:RemoveFolder();']")
	@CacheLookup
	WebElement Removefolderoption;
	@FindBy(xpath = "//*[@id='button-1']")

	WebElement btnyes;
	/**Method to delete folder after execution
	 * 	Scripted by :Tarak
	 */
		public void deleteFolderFromCollection() {
				driver.switchTo().defaultContent();
				Log.message("Switching to default content");
				SkySiteUtils.waitTill(4000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				Log.message("SWitched to frame");
				SkySiteUtils.waitTill(4000);
				// SkySiteUtils.waitForElement(driver, Moreoption, 50);

				Moreoption.click();
				SkySiteUtils.waitTill(4000);
				Log.message("More option is clicked");
				SkySiteUtils.waitTill(4000);
				Removefolderoption.click();
				Log.message("Remove folder button is clicked");
				SkySiteUtils.waitTill(10000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(6000);
				btnyes.click();
				Log.message("yes button has been clicked.");
				
				SkySiteUtils.waitTill(6000);
		        Log.message("Folder has been deleted");
		}
		@FindBy(xpath=".//*[@id='lookup_combo_10176981']/div/input[1]")
		WebElement DocumentTypebox;
		@FindBy(xpath=".//*[@id='10176982']")
		WebElement invoicebox;
		@FindBy(xpath=".//*[@id='10177905']")
		WebElement Projectname;
		@FindBy(xpath=".//*[@id='10177907_true']")
		WebElement aprovalYes;
		@FindBy(xpath=".//*[@class='cflookup_combo']")
		WebElement documenttypesearchbox1;
		@FindBy(xpath="//*[@placeholder='Invoice #']")
		WebElement invoicebxoforupsate;
		@FindBy(xpath="//*[@placeholder='Project name']")
		WebElement projectnameboxupdate;
		/**Method to modify document type,invoice and project name
		 * scripted by Tarak
		 * @throws AWTException 
		 */
		public boolean modifyDocumentTypeInvoiceAndPName(String projectname,String invoice) throws AWTException 
		{
			
			String documenttype=PropertyReader.getProperty("Documenttype");
			SkySiteUtils.waitTill(3000);
			SkySiteUtils.waitForElement(driver, CoustomPropertiestab, 30);
			CoustomPropertiestab.click();
			Log.message("Custom Properties tab is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, documenttypesearchbox1, 30);
			documenttypesearchbox1.click();
			Log.message("Document type box is clicked");
			SkySiteUtils.waitTill(4000);
			
	        Actions act=new Actions(driver);
	        act.sendKeys(documenttype).build().perform();
			Log.message("Document type entered is " +documenttype);
			SkySiteUtils.waitTill(3000);
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, invoicebxoforupsate, 30);
			invoicebxoforupsate.click();
			Log.message("invoice box is clicked");
			invoicebxoforupsate.sendKeys(invoice);
			Log.message("Invoice is entered " +invoice);
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, projectnameboxupdate, 30);
			projectnameboxupdate.click();
			Log.message("Project name box is clicked");
			projectnameboxupdate.sendKeys(projectname);
			Log.message("Project name entered is " +projectname);
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, updateclosebtn, 30);
			updateclosebtn.click();
			Log.message("update button is clicked");
			SkySiteUtils.waitTill(4000);
	
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String Documentypeintable = null;
			String invoiceintable = null;
			String projectnameintable =null;
			
			for(int i=1; i<numberoffiles; i++) 
			{
				Documentypeintable=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[8]")).getText();
				SkySiteUtils.waitTill(2500);
				Log.message("Updated Document Type is:-" +Documentypeintable);
				
				invoiceintable=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[9]")).getText();
				SkySiteUtils.waitTill(2500);
				Log.message("Updated Invoice is:-" +invoiceintable);
				
				projectnameintable=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[12]")).getText();
				SkySiteUtils.waitTill(2500);
				Log.message("Updated Project Name is:-" +projectnameintable);
				
			}
			
			if(Documentypeintable.contains(documenttype) && invoiceintable.contains(invoice) && projectnameintable.contains(projectname))	
			{
				SkySiteUtils.waitTill(2500);
				Log.message("Document & Invoice & project name is updated ");
			    return true;
				
			}
			else 
			{
				
				Log.message("Document & Invoice & project name is not  updated ");
				
				return false;
			}
			
	}
		
		
		
		@FindBy(xpath=".//*[@id='div_otherInfo']/div/div/div[3]/input[2]")
		@CacheLookup
		WebElement closebtn;
			//@FindBy(id="Button6")
		
		@FindBy(xpath=".//*[@id='btnAdvFileSearch']")
		WebElement advancedsearch;
		
		 @FindBy(xpath="//*[contains(text(),'Reset')]")
		// @FindBy(xpath=".//*[@id='divAdvSearchActionForCallingPage']/button[2]")
		WebElement resetbtn;
		@FindBy(xpath="//*[contains(text(),'With Document Type')]")
		
		WebElement checkbox;
		@FindBy(xpath="//input[@class='dhxcombo_input']")
		@CacheLookup
		WebElement documenttypesearchbox;  
		@FindBy(id="btnAdvSearch")
		WebElement Searchbtn;
		
		@FindBy(xpath=".//*[@id='div_otherInfo']/div/div/div[1]/button")
		WebElement btnCross;
		
		
		 /**  Method to search with document type
		 * Scriptedby:Tarak
		 * @throws AWTException 
		 */
		public boolean DocumentTypeSearch() throws AWTException 
		{
			
			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");

			SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkbox, 40);
			actclick.moveToElement(checkbox).click(checkbox).build().perform();
			Log.message("checkbox is clicked");
			
			String documenttypesearch=PropertyReader.getProperty("DocumentTypeSearch");
		    SkySiteUtils.waitForElement(driver,documenttypesearchbox, 40);
		    documenttypesearchbox.sendKeys(documenttypesearch);
		    Log.message("Document type searched " +documenttypesearch);
		    SkySiteUtils.waitTill(3000);
		    
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(8000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String documenttypepresent=null;
			for(int i=1;i<=numberoffiles;i++) 
			{
				documenttypepresent = driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[8]")).getText();
				Log.message("Document type is:- " +documenttypepresent);
			}
			
			if(documenttypesearch.equals(documenttypepresent) && numberoffiles==1)	
			{
			
			    Log.message("Search for document type is validated ");
				return true;
				
			}
			else 
			{
				Log.message("document type searched is not present");
				return false;
				
			}	
			
			
	}
		
		
		@FindBy(xpath="//*[contains(text(),'With Invoice #')]")
		@CacheLookup
		WebElement checkboxforinvoice;
		@FindBy(xpath=".//*[@placeholder='Invoice #']")
		@CacheLookup
		WebElement invoicesearchbox;
		@FindBy(xpath="//select[@id='sddlcond_Search_10177040']")
		@CacheLookup
		WebElement criteriadropdown;
		
		
		/**Method to search with invoice
		 * scripted by :Tarak
		 * @throws AWTException 
		 */
		public boolean searchInvoice() throws AWTException 
		{
			
			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
			actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
			Log.message("Checkbox is clicked");
			SkySiteUtils.waitTill(5000);
			
            SkySiteUtils.waitTill(3000);
			String invoicesearch=PropertyReader.getProperty("Invoice");
		    SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
		    invoicesearchbox.sendKeys(invoicesearch);
		    Log.message("invoice searched is " +invoicesearch);
		    SkySiteUtils.waitTill(3000);
			
		    Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(5000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String invoicepresent=null;
			for(int i =1;i<=numberoffiles;i++) 
			{
				invoicepresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[9]")).getText();
				Log.message("Invoice for the searched file is:- " +invoicepresent);
				
			}
			
			if(invoicepresent.contains(invoicesearch) && numberoffiles==1)	
			{
			
			    Log.message("Search with invoice is validated ");
				return true;
				
			}
			
			else 
			{
				Log.message("search with invoice  is not validated");
				return false;
			}
			
	}
		
		@FindBy(xpath="//input[contains(text(),'With project number')]")
		//@CacheLookup
		WebElement checkboxforprojectnumber;
		@FindBy(xpath="//input[@placeholder='project number']")
		@CacheLookup
		WebElement projectsearchbox;
		
		/**Method to search with Project Number
		 * scripted by:Tarak
		 * @throws AWTException 
		 */
		public boolean searchWithProjectNumber() throws AWTException 
		{
			
			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(2000);
			
			String Projectnosearch=PropertyReader.getProperty("Project_Number");
		    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
		    Projectnobox.click();
		    Log.message("project search box is clicked");
		    SkySiteUtils.waitTill(4000);
		    Projectnobox.sendKeys(Projectnosearch);
		    Log.message("Project number entered is " +Projectnosearch);
		    SkySiteUtils.waitTill(5000);
		    
		    SkySiteUtils.waitForElement(driver, Searchbtn, 30);
		    Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String ProjectNoInList=null;
			for(int i=1; i<=numberoffiles; i++) 
			{
					
				ProjectNoInList =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[11]")).getText();
				Log.message("Project Number for searched file is:- "+ProjectNoInList);	
			
	        }
			
			if(ProjectNoInList.contains(Projectnosearch) && numberoffiles==1)	
			{
				
				    Log.message("Search with Project number is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Project number  is not validated");
					return false;
			}	
			
		}
		
		
		
		@FindBy(xpath="//input[contains(text(),'With vendor')]")
		@CacheLookup
		WebElement checkboxforvendor;
		@FindBy(xpath =".//*[@id='lookup_combo_Search_10177908']/div/input[1]")
		@CacheLookup
		WebElement vendorsearchbox;
		@FindBy(xpath="//*[contains(text(),'With vendor')]")
		WebElement checkboxVendor;
		
		@FindBy(xpath=".//*[@id='divCustomFieldSearchOption']/div/ul/li[3]/div/div[3]/div/div[2]/a/i")
		WebElement plusBtnVendor;
		
		@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
		WebElement checkboxSearchedVendor;
		
		@FindBy(xpath=".//*[@id='btnSaveNClose']")
		WebElement btnSaveAndClose;
	
		/**Search with Vendor
		 * 
		 */
		public boolean searchWithVendor() throws AWTException 
		{

			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(5000);
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
			actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
			Log.message("checkbox is clicked");
			
			String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
			SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
			plusBtnVendor.click();
			Log.message("Clicked on plus button to select vendor");
			SkySiteUtils.waitTill(8000);
			
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
			Log.message("Switched frame");		
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
			selectVendorSearchTxtBox.clear();
			selectVendorSearchTxtBox.sendKeys(Vendorforsearch);
			Log.message("Vendor name entered for search is:- "+Vendorforsearch);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
			selectVendorSearchButton.click();
			Log.message("Clicked on search button to find required Vendor");
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
			checkboxSearchedVendor.click();
			Log.message("Selected searched vendor");
			SkySiteUtils.waitTill(8000);
			
			List<WebElement> element1 = driver.findElements(By.xpath(".//*[@id='btnSaveNClose']"));
			int count = element1.size();
			
			if(count>0)
			{
				btnSaveAndClose.click();
				Log.message("CLicked on Save and Close button after selecting vendor");
				SkySiteUtils.waitTill(5000);	
				
			}			
			
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched frame");
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver, Searchbtn, 30);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String VendorInList = null;
			for(int i=1;i<=numberoffiles;i++) 
			{
					
				VendorInList =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
				Log.message("Vendor of searched file is:- " +VendorInList);	
				
			}
			
			if(VendorInList.contains(Vendorforsearch) && numberoffiles==1)	
			{
				
				    Log.message("Search with Vendor is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Vendor  is not validated");
					return false;
					
			}	
		
		}
		
		
		
		
		/**Method to search for Projectname
		 * Scripted by:Tarak
		 * @throws AWTException 
		 */
	public boolean searchWithProjectName() throws AWTException 
	{
		
		SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
		
	    WebElement projectnamebox=driver.findElement(By.xpath("//input[@placeholder='Project name']"));
	    SkySiteUtils.waitTill(3000);
		String Projectnameforsearch=PropertyReader.getProperty("Project_Name");
	    SkySiteUtils.waitForElement(driver,projectnamebox, 40);
	    projectnamebox.click();
	    Log.message("Project name box is clicked");
	    projectnamebox.sendKeys(Projectnameforsearch);
	    Log.message("Project name searched is " +Projectnameforsearch);
	    SkySiteUtils.waitTill(5000);
	    
	    SkySiteUtils.waitForElement(driver, Searchbtn, 30);
	    Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String searchedFileProjectName=null;
		for(int i=1;i<=numberoffiles;i++) 
		{
				
			searchedFileProjectName = driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[12]")).getText();
			Log.message("Project Name of searched file is:- " +searchedFileProjectName);	
		
		}
			
		if(Projectnameforsearch.contains(searchedFileProjectName) && numberoffiles==1)	
		{
			
			    Log.message("Search with Project name is validated !!!!!");
				return true;
				
		}
		else 
		{
				Log.message("Search with Project name  is not validated");
				return false;
				
		}	
		
	}
	
	
	
	
	@FindBy(xpath=".//*[@id='txtContentSearch']")
	@CacheLookup
	WebElement keywordcontentsearch;
	/**Method for search with Keyword
	 * 
	 */
	public boolean searchWithKeyword() 
	{
		SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
		
		String keywordforsearch=PropertyReader.getProperty("Keywordforsearch");
		keywordcontentsearch.sendKeys(keywordforsearch);
		Log.message("Keyword searched is " +keywordforsearch);
		 SkySiteUtils.waitTill(5000);
		 Searchbtn.click();
	    Log.message("search button is clicked");
	    SkySiteUtils.waitTill(4000);
	    
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String filenamesearched=PropertyReader.getProperty("Filenameforkeywordsearch");
		Log.message("File name to be searched is " +filenamesearched);
		
		String fileinlist=null;
		for(int i =1;i<=numberoffiles;i++) 
		{
				
			fileinlist =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
			Log.message("Name if the searched file is:- "+fileinlist);	
			
		}
			
		if(fileinlist.contains(filenamesearched) && numberoffiles==1)	
		{
			
			    Log.message("Search with keyword is validated !!!!!");
				return true;
				
		}
		else 
		{
				Log.message("Search with  keyword  is not validated");
				return false;
		}	
	
   }
	
	
	
	
	@FindBy(css = "#btnUploadFile")

	WebElement btnUploadFile;

//	@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")
	@FindBy(xpath = ".//*[@id='btnSelectFiles']")
	WebElement btnselctFile;
	
	@FindBy(css="#btnFileAttrLink")
	WebElement addattributelink;
	@FindBy(xpath=".//*[@id='btnSaveNClose']")
	WebElement savebtn;
	@FindBy(xpath = ".//*[@id='chkAllProjectdocs']")

	WebElement checkboxforfileselect;
	/**Method to upload file and add attribute
	 * 
	 */
	
	public void UploadFileAddAttribute(String FolderPath,String tempfilepath,String invoice) throws AWTException, IOException {
		// boolean result1=true;
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(20000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
  		addattributelink.click();
  		Log.message("Add attribute link clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
  		Log.message("switch to frame");
  		checkboxforfileselect.click();
  		Log.message("checkbox clicked");
  		SkySiteUtils.waitTill(5000);
  	//	String filetoselect=PropertyReader.getProperty("Filenameforkeywordsearch");
  	//	String filetoselect=PropertyReader.getProperty("Filenametoaddattribute");
  	//	List<WebElement> filelist=driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td"));
  	//	for (WebElement var:filelist)
  		///	if(var.getText().contains(filetoselect))
  		        //       var.click();
  		driver.findElement(By.xpath(".//*[@id='divAttributeGrid']/div[2]/table/tbody/tr[3]/td")).click();
  		Log.message("file is selected");
  		SkySiteUtils.waitTill(6000);

  		WebElement invoicebox12=driver.findElement(By.xpath("//input[@placeholder='Invoice #']"));
        Actions act4=new Actions(driver);
        act4.moveToElement(invoicebox12).click().build().perform();;
  		Log.message("Invoice box is clicked");
  		SkySiteUtils.waitTill(2000);
  		invoicebox12.sendKeys(invoice);
		Log.message("Invoice is entered " +invoice);
  		SkySiteUtils.waitTill(2000);
  		savebtn.click();
  		Log.message("save button is clicked");
  	
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}
	/**Method to search with invoice for the updated file and invoice attribute
	 * scripted by :Tarak
	 * @throws AWTException 
	 */
	public boolean searchInvoiceonupdatedfile(String invoice) throws AWTException 
	{
		
		SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
			
	    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
	    Actions actclick=new Actions(driver);
		actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
		Log.message("checkbox is clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
	    invoicesearchbox.sendKeys(invoice);
	    Log.message("invoice searched is " +invoice);
	    SkySiteUtils.waitTill(3000);
	    
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String invoicepresent=null;

		for(int i =1;i<=numberoffiles;i++) 
		{
			invoicepresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[9]")).getText();
			Log.message("Invoice of the searched file is:-"+invoicepresent);
		
		}
			
		if(invoicepresent.contains(invoice) && numberoffiles==1)	
		{
		
		    Log.message("Search with invoice is validated ");
			return true;
			
		}
		else 
		{
			Log.message("search with invoice  is not validated");
			return false;
		}	
		
}
	
	
	
	/**Method to upload file and add custom  attribute
	 * 
	 */
	
	public void UploadFileAddCustomAttribute(String FolderPath,String tempfilepath) throws AWTException, IOException 
	{
		// boolean result1=true;
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(10000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(20000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
  		addattributelink.click();
  		Log.message("Add attribute link clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
  		Log.message("switch to frame");
  		checkboxforfileselect.click();
  		Log.message("checkbox clicked");
  		SkySiteUtils.waitTill(6000);
  		
  		//String filetoselect=PropertyReader.getProperty("Filenameforkeywordsearch");
  		//String filetoselect=PropertyReader.getProperty("Filenametoaddattribute");
  		//List<WebElement> filelist=driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td"));
  		//for (WebElement var:filelist)
  		//if(var.getText().contains(filetoselect))
        //var.click();
  		
		driver.findElement(By.xpath(".//*[@id='divAttributeGrid']/div[2]/table/tbody/tr[3]/td")).click();
  		Log.message("file is selected");
  		SkySiteUtils.waitTill(6000);

  	    //Log.message("file is selected");
  	    //SkySiteUtils.waitTill(2000);
        
  		
        //for Productions
  		//WebElement vendorsearchbox=driver.findElement(By.xpath("//*[@data-id='10176181']"));
        //staging new
  		//WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10183928']"));
        
  		String vendordata=PropertyReader.getProperty("VendorforSearch");
     	SkySiteUtils.waitForElement(driver, plusButtonVendor, 30);
     	plusButtonVendor.click();
     	Log.message("Clicked on the plus button to select vendor");
     	SkySiteUtils.waitTill(5000);
     	
     	driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		Log.message("Switched frame");		
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(vendordata);
		Log.message("Vendor name entered for search is:- "+vendordata);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
		selectVendorSearchButton.click();
		Log.message("Clicked on search button to find required Vendor");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
		checkboxSearchedVendor.click();
		Log.message("Clicked on searched vendor checkbox");
		
		driver.switchTo().defaultContent();
 		Log.message("Switched to default content");
 		SkySiteUtils.waitTill(5000);
 		
		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
 		Log.message("Switched to frame");
 		SkySiteUtils.waitTill(4000);
		   	
     	/*vendorsearchbox.click();
  		Log.message("Vendor box is clicked");
  		SkySiteUtils.waitTill(2000);
  		Actions act6=new Actions(driver);
  		act6.sendKeys(vendordata).build().perform();
  		//vendorsearchbox.sendKeys(vendordata);
		Log.message("Vendor  is entered " +vendordata);
  		SkySiteUtils.waitTill(2000);
  		Robot rb=new Robot();
  		rb.keyPress(KeyEvent.VK_ENTER);
  		rb.keyRelease(KeyEvent.VK_ENTER);*/
  		
  		savebtn.click();
  		Log.message("save button is clicked");
  	
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}
	
	@FindBy(xpath=".//*[@id='divAttCustFieldsData']/div/ul/li[3]/div/div[2]/div/div[2]/a/i")
	WebElement plusButtonVendor;
	
  	/**Method search with All attribute using And operator
  	 * @throws AWTException 
  	 * 
  	 */
    public boolean searchWithAllAttributeAndOperator() throws AWTException 
    {
    	
    	SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
		
		Actions actclick=new Actions(driver);
		SkySiteUtils.waitForElement(driver, checkbox, 40);
	    actclick.moveToElement(checkbox).click(checkbox).build().perform();
		Log.message("checkbox is clicked");
		
		String documenttypesearch=PropertyReader.getProperty("DocumentTypeSearch");
		SkySiteUtils.waitForElement(driver,documenttypesearchbox, 40);
		documenttypesearchbox.sendKeys(documenttypesearch);
		Log.message("Document type searched " +documenttypesearch);
		SkySiteUtils.waitTill(3000);
	    
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
		actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
		Log.message("checkbox is clicked");
		SkySiteUtils.waitTill(8000);
 
		String invoicesearch=PropertyReader.getProperty("Invoice");
	    SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
	    invoicesearchbox.sendKeys(invoicesearch);
	    Log.message("invoice searched is " +invoicesearch);
	    SkySiteUtils.waitTill(3000);
		Robot rb1= new Robot();
		rb1.keyPress(KeyEvent.VK_ENTER);
		rb1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		
		String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");		
	    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
		actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
		Log.message("checkbox is clicked");
		
		SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
		plusBtnVendor.click();
		Log.message("Clicked on plus button to select vendor");
		SkySiteUtils.waitTill(8000);
		
		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
		Log.message("Switched frame");		
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(Vendorforsearch);
		Log.message("Vendor name entered for search is:- "+Vendorforsearch);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
		selectVendorSearchButton.click();
		Log.message("Clicked on search button to find required Vendor");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
		checkboxSearchedVendor.click();
		Log.message("Clicked on searched vendor checkbox");
		
		SkySiteUtils.waitForElement(driver, btnSaveAndClose, 40);
		btnSaveAndClose.click();
		Log.message("CLicked on Save and Close button after selecting vendor");
		SkySiteUtils.waitTill(5000);
		
		driver.switchTo().defaultContent();
 		Log.message("Switched to default content");
 		SkySiteUtils.waitTill(5000);
 		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 		Log.message("Switched to frame");
 		SkySiteUtils.waitTill(4000);
			   
		Robot rb2= new Robot();
		rb2.keyPress(KeyEvent.VK_DOWN);
		rb2.keyRelease(KeyEvent.VK_DOWN);
		rb2.keyPress(KeyEvent.VK_ENTER);
		rb2.keyRelease(KeyEvent.VK_ENTER);
		Log.message("Enter is pressed");
		SkySiteUtils.waitTill(5000);
		
		String Projectnosearch=PropertyReader.getProperty("Project_Number");
	    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
	    Actions act4=new Actions(driver);
	    act4.moveToElement(Projectnobox).click(Projectnobox).build().perform();
	    Log.message("poject search box is clicked");
	    SkySiteUtils.waitTill(4000);
	    Projectnobox.sendKeys(Projectnosearch);
	    Log.message("Project number entered is " +Projectnosearch);
	    SkySiteUtils.waitTill(5000);
	    
	    WebElement projectnamebox=driver.findElement(By.xpath("//input[@placeholder='Project name']"));
	    SkySiteUtils.waitTill(3000);
		String Projectnameforsearch=PropertyReader.getProperty("Project_Name");
	    SkySiteUtils.waitForElement(driver,projectnamebox, 40);
	    projectnamebox.click();
	    Log.message("Project name box is clicked");
	    projectnamebox.sendKeys(Projectnameforsearch);
	    Log.message("Project name searched is " +Projectnameforsearch);
	    SkySiteUtils.waitTill(5000);
	    
	    SkySiteUtils.waitForElement(driver, Searchbtn, 30);
		Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		String filenamesearched=PropertyReader.getProperty("filesearchwithallattribute");
		
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String fileinlist =null;
		for(int i =1; i<=numberoffiles;i++) 
		{
			fileinlist =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
			Log.message("Searched file is: "+fileinlist);
						
		}
		
		SkySiteUtils.waitTill(5000);	
		if(fileinlist.contains(filenamesearched) && numberoffiles==1)	
		{
				
		    Log.message("Search with all attribute is validated!!!!!");
			return true;
			
		}
		else 
		{
			Log.message("Search with all attribute is not  validated");
			return false;
		}	
	
   }
    
    
    
    /**Method to update attribute after adding
     * @throws AWTException 
     * 
     */
    public boolean modifyAddedAttributes(String projectno,String vendorname) throws AWTException 
    {   	
    	
		SkySiteUtils.waitTill(4000);
		CoustomPropertiestab.click();
		Log.message("Custome Properties tab clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,Projectnofield,40);
		Projectnofield.click();
		Log.message("Project no field is clicked");
		SkySiteUtils.waitTill(3000);
		
		Projectnofield.clear();
		Log.message("Project no field is cleared");
		Projectnofield.sendKeys(projectno);
		Log.message("Project number entered is " +projectno);
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, plusBtnAddVendor, 30);
		plusBtnAddVendor.click();
		Log.message("CLicked on the plus button to add vendor");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, selectVendorPopOver, 30);
		Log.message("Select vendor popover opened");

		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		Log.message("Switched frame");
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 60);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(vendorname);
		Log.message("Vendor name entered for dearch is " +vendorname);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 30);
		selectVendorSearchButton.click();
		Log.message("Clicked on Search button");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectSearchedVendor, 30);
		selectSearchedVendor.click();
		Log.message("Vendor is added");
		SkySiteUtils.waitTill(8000);
		
		List<WebElement> element1 = driver.findElements(By.xpath(".//*[@id='btnSaveNClose']"));
		int count = element1.size();
		
		if(count>0)
		{
			btnSaveAndClose.click();
			Log.message("CLicked on Save and Close button after selecting vendor");
			SkySiteUtils.waitTill(5000);	
			
		}			
		
		driver.switchTo().defaultContent();
 		Log.message("Switched to default content");
 		SkySiteUtils.waitTill(5000);
 		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 		Log.message("Switched frame");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, updateclosebtn, 30);
 		//updateclosebtn.click();
 		WebElement element = driver.findElement(By.xpath("//input[@value='Update & close']"));
 		JavascriptExecutor executor = (JavascriptExecutor)driver;
 		executor.executeScript("arguments[0].click();", element);
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);
	
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String Projectnopresent=null;
		String vendorpresent=null;
		
		for(int i =1;i<numberoffiles;i++) 
		{
			SkySiteUtils.waitTill(4000);
			Projectnopresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[11]")).getText();
			Log.message("Edited Project No is:- "+Projectnopresent);
			SkySiteUtils.waitTill(2500);
			 
			vendorpresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
			Log.message("Edited Vendor is:- "+vendorpresent);	
			SkySiteUtils.waitTill(2500);
			
		}
		
		if(Projectnopresent.contains(projectno) && vendorpresent.contains(vendorname))	
		{
		
		   Log.message("File is present after searching with project number and vendor ");
		   return true;
			
		}
		else 
		{		
			Log.message("File is not present after search with project number and vendor");
			return false;
		}
		
	}	
    
    
    
        @FindBy(xpath="//input[@placeholder='Project number']")
        WebElement projectsearchbox2;
    	
      
        @FindBy(xpath="//*[text()='Reset']")
        WebElement btnResetAdvSearch;
        
    	/**Method to search the update value 
    	 * @throws AWTException 
    	 * 
    	 */
		public boolean searchUpdatedValue(String updatedprojectno,String vendor) throws AWTException 
		{

			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(8000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,80);		
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		    SkySiteUtils.waitTill(8000);
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(4000);
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
			actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
			Log.message("checkbox is clicked");
			
			SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
			plusBtnVendor.click();
			Log.message("Clicked on plus button to select vendor");
			SkySiteUtils.waitTill(8000);
			
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
			Log.message("Switched frame");		
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
			selectVendorSearchTxtBox.clear();
			selectVendorSearchTxtBox.sendKeys(vendor);
			Log.message("Vendor name entered for search is:- "+vendor);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
			selectVendorSearchButton.click();
			Log.message("Clicked on search button to find required Vendor");
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
			checkboxSearchedVendor.click();
			Log.message("Selected searched vendor");
			SkySiteUtils.waitTill(8000);
			
			if(btnSaveAndClose.isDisplayed())
			{
				btnSaveAndClose.click();
				Log.message("CLicked on Save and Close button after selecting vendor");
				SkySiteUtils.waitTill(5000);
				
				driver.switchTo().defaultContent();
		 		Log.message("Switched to default content");
		 		SkySiteUtils.waitTill(5000);
		 		
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 		Log.message("Switched to frame");
		 		SkySiteUtils.waitTill(5000);
			}
			else
			{
				driver.switchTo().defaultContent();
		 		Log.message("Switched to default content");
		 		SkySiteUtils.waitTill(5000);
		 		
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 		Log.message("Switched to frame");
		 		SkySiteUtils.waitTill(5000);
			}
			
	 		WebElement Projectnobox=driver.findElement(By.xpath("//li[@id='trContentSearchBreak']//li[4]//div[3]//input"));
	 		Projectnobox.click();
	 		Log.message("project number search box is clicked");
	 		Projectnobox.clear();
	 		Projectnobox.sendKeys(updatedprojectno);
	 		Log.message("Project number entered is " +updatedprojectno);
		    SkySiteUtils.waitTill(5000);

			SkySiteUtils.waitForElement(driver, Searchbtn, 30);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			String filenamesearched=PropertyReader.getProperty("Searchfilename");
			
			String fileinlist =null;
			for(int i =1;i<=numberoffiles;i++) 
			{
					
				fileinlist =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
				Log.message("Searched file is:- "+fileinlist);	

			}
			
			if(fileinlist.contains(filenamesearched) && numberoffiles==1)	
			{
				
				    Log.message("Search with Project number and vendor is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Project number and vendor is not validated");
					return false;
			}
			
		}
		
				
		/**Search with Vendor
		 * 
		 */
		public boolean searchWithVendorwhenaddingattribute() throws AWTException 
		{

			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(5000);
					
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
			actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
			Log.message("checkbox is clicked");
			
			String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
			SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
			plusBtnVendor.click();
			Log.message("Clicked on plus button to select vendor");
			SkySiteUtils.waitTill(8000);
			
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
			Log.message("Switched frame");		
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
			selectVendorSearchTxtBox.clear();
			selectVendorSearchTxtBox.sendKeys(Vendorforsearch);
			Log.message("Vendor name entered for search is:- "+Vendorforsearch);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
			selectVendorSearchButton.click();
			Log.message("Clicked on search button to find required Vendor");
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
			checkboxSearchedVendor.click();
			Log.message("Clicked on searched vendor checkbox");
			
			List<WebElement> element1 = driver.findElements(By.xpath(".//*[@id='btnSaveNClose']"));
			int count = element1.size();
			
			if(count>0)
			{
				btnSaveAndClose.click();
				Log.message("CLicked on Save and Close button after selecting vendor");
				SkySiteUtils.waitTill(5000);	
				
			}			
			
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched frame");
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver, Searchbtn, 30);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			String vendorname =null;
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			SkySiteUtils.waitTill(6000);
			
			for(int i =1;i<=numberoffiles;i++) 
			{
					
				vendorname =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
				Log.message("Searched file vendor name is:- "+vendorname);
			}		
			if(vendorname.contains(Vendorforsearch) && numberoffiles==1)	
			{
				
				    Log.message("Search with Project number and vendor is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Project number and vendor is not validated");
					return false;
			}			
		}
		
		
//////////////////////////////////////////      sekhar     ///////////////////////////////////////////	
		
		
   @FindBy(xpath = ".//*[@id='divPaginationBelow']/div[2]/div/div[1]")
   WebElement PaginationBelow;
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[10]")
   WebElement getProjectnum;
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[16]")
   WebElement getvendor;
   
   @FindBy(xpath = "//*[@id='chkAllProjectdocs']")
   WebElement chkAllProjectdocs;
   
   @FindBy(xpath = "//*[@id='Button1']")
   WebElement btnmore1;
   
   @FindBy(xpath = "(//i[@class='icon icon-modify-folder'])[3]")
   WebElement btniconmodifyfolder;
   
   @FindBy(xpath = "(//i[@class='icon icon-plus'])[2]")
   WebElement btniconplus2;
   
   @FindBy(xpath = "(//i[@class='icon icon-plus'])[3]")
   WebElement btniconplus3;
   
   @FindBy(xpath = "//*[@id='btnSaveNClose']")
   WebElement btnSaveNClose;  
   
	  
   /** 
    * Method written for Modify Attribute on ProjectNumber and Vendor
    * Scripted By: Sekhar     
    * @throws AWTException 
    */
		
   public boolean  Modify_Attribute_ProjectNumber_Vendor()
   {
	   boolean result1=false;
	   boolean result2=false;
	   boolean result3=false;	   
	   SkySiteUtils.waitTill(5000);	          
	   driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	   SkySiteUtils.waitTill(5000);
	   String Pagination_Count=PaginationBelow.getText();		
	   Log.message("Page count is: "+Pagination_Count);
	   String[] y= Pagination_Count.split(" ");
	   String AvlPageCount=y[1];
	   int Total_Count=Integer.parseInt(AvlPageCount);
	   Log.message("Total Available File Page count is: "+Total_Count);//Getting Page counts		
	   SkySiteUtils.waitTill(5000);		   
	   String Before_Project_Num=getProjectnum.getText();
	   Log.message("Before Project Num  is:"+Before_Project_Num);
	   SkySiteUtils.waitTill(5000);	
	   String Before_Vendor=getvendor.getText();
	   Log.message("Before Vendor is:"+Before_Vendor);
	   SkySiteUtils.waitTill(5000);		   
	   chkAllProjectdocs.click();
	   Log.message("select all check box button has been clicked");
	   SkySiteUtils.waitTill(3000);	
	   btnmore1.click();
	   Log.message("more option button has been clicked");
	   SkySiteUtils.waitTill(3000);	
	   btniconmodifyfolder.click();
	   Log.message("modify attributes button has been clicked");
	   SkySiteUtils.waitTill(3000);			
	   driver.switchTo().defaultContent();
	   driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
	   Log.message("switch to SetBatchAttribute frame");
	   SkySiteUtils.waitTill(3000);
	   btniconplus2.click();
	   Log.message("2nd'+'button has been clicked");
	   SkySiteUtils.waitTill(5000);		
	   driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
	   Log.message("switch to CustomFieldLookup frame");
	   SkySiteUtils.waitTill(3000);	   
	   //User Radio count		 
	   int Radio_Count=0;
	   int i= 0;
	   List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
	   for (WebElement Element : allElements)
	   { 
		   Radio_Count = Radio_Count+1; 	
	   }	
	   Log.message("Radio Count  is: "+Radio_Count);
	   //Getting Radio count
	   for(i = 2;i<=Radio_Count;i++)				 
	   {					 
		   String ProjectNum_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
		   Log.message("Project Num Type is:"+ProjectNum_Type);
		   
		   if(!ProjectNum_Type.equals(Before_Project_Num))
		   {	
			   result1=true;
			   Log.message("Project Number Selection Successfully!!!!");	
			   break;
		   }
	   }
	   if((result1==true))
	   {	
		   SkySiteUtils.waitTill(5000);			   
		   String Project_Number=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
		   Log.message("Project Number is:"+Project_Number);		   
		   driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
		   SkySiteUtils.waitTill(10000);		   
		    driver.switchTo().defaultContent();
		   driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
		   Log.message("switch to SetBatchAttribute frame");
		   SkySiteUtils.waitTill(3000);
		   btniconplus3.click();
		   Log.message("3rd'+'button has been clicked");
		   SkySiteUtils.waitTill(5000);		
		   driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		   Log.message("switch to CustomFieldLookup frame");
		   SkySiteUtils.waitTill(3000);				
		   //User checkbox count		 
		   int checkbox_count=0;
		   int j= 0;
		   List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
		   for (WebElement Element : allElements1)
		   { 
			   checkbox_count = checkbox_count+1; 	
		   }	
		   Log.message("checkbox Count  is: "+checkbox_count);
		   //Getting checkbox count
		   for(j = 1;j<=checkbox_count;j++)				 
		   {					 
			   String Vendor_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
			   Log.message("Vendor name is:"+Vendor_Type);
			   
			   if(!Vendor_Type.equals(Before_Vendor))
			   {	
				   result2=true;
				   Log.message("Vendor name Selection Successfully!!!!");	
				   break;
			   }
		   }
		   if((result2==true))
		   {	
			   SkySiteUtils.waitTill(5000);				   
			   String Vendor_Name=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
			   Log.message("Vendor Name is:"+Vendor_Name);			   
			   driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
			   SkySiteUtils.waitTill(10000);		
			   btnSaveNClose.click();
			   Log.message("save&close button has been clicked");
			   SkySiteUtils.waitTill(5000);
			   driver.switchTo().defaultContent();
			   driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
			   Log.message("switch to SetBatchAttribute frame");
			   SkySiteUtils.waitTill(3000);			
			   btnSaveNClose.click();			   
			   Log.message("save button has been clicked");
			   SkySiteUtils.waitTill(5000);
			   driver.switchTo().defaultContent();
			   driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			   Log.message("switch to my frame");
			   SkySiteUtils.waitTill(3000);			
					
			   int passcount=0;
			   int failcount=0;
			   int Document_Count=Total_Count;
			   for(int k=1;k<=Document_Count;k++)
			   {
				   String Pre_Project_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+k+"]")).getText().toString();
				   String After_vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+k+"]")).getText().toString();
				   if(Project_Number.equals(Pre_Project_Num)&&(Vendor_Name.equals(After_vendor)))
				   {
					   passcount=passcount+1;
					   Log.message("Pass Count is:"+passcount);
				   }
				   else	
				   {
					   failcount=failcount+1;
					   Log.message("Fail count is:"+failcount);
				   }
			   }				
			   if(passcount==Document_Count)
			   {
				   result3=true;
				   Log.message("Modify Attribute Validation Successfully!!!!!");                             
			   }		              
			   else
			   {      
				   result3=false;
				   Log.message("Modify Attribute Validation Failed!!!!!");             
			   }				
		   }
		   else
		   {
			   result2=false;
			   Log.message("Vendor name Selection Failed!!!!");					
		   }
	   }			
	   else
	   {
		   result1=false;			
		   Log.message("Project Number Selection Failed!!!!");				
	   }
	   if((result1==true)&&(result2==true)&&(result3==true))
	   {				
		   Log.message("Project Num and Vendor Modified Successfully!!!!");
		   return true;
	   }
	   else
	   {				
		   Log.message("Project Num and Vendor Modified Failed!!!!");
		   return false;
	   }   
   }    
 
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[8]")
   WebElement getDocumentname;
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[9]")
   WebElement getInvoice;
   
   @FindBy(xpath = "(//i[@class='icon icon-plus'])[1]")
   WebElement btniconplus1;
   
 
   
   /** 
    * Method written for Modify Attribute on Document name and Invoice and Vendor
    * Scripted By: Sekhar     
    * @throws AWTException 
    */
   
   public boolean Modify_Attribute_Documentname_Invoice_Vendor(String Invoice,String Temp_Invoice) 
   {   		
	   boolean result1=false;
	   boolean result2=false;
   	   boolean result3=false;
   	
   	   SkySiteUtils.waitTill(5000);	          
 	   driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 	   SkySiteUtils.waitTill(5000);
 	   String Pagination_Count=PaginationBelow.getText();		
 	   Log.message("Page count is: "+Pagination_Count);
 	   String[] y= Pagination_Count.split(" ");
 	   String AvlPageCount=y[1];
 	   int Total_Count=Integer.parseInt(AvlPageCount);
 	   Log.message("Total Available File Page count is:"+Total_Count);
 	   SkySiteUtils.waitTill(10000);			
 	   String Before_Document_Name=getDocumentname.getText();
 	   Log.message("Before Document Name is:"+Before_Document_Name);
 	   SkySiteUtils.waitTill(5000);	
 	   String Before_Invoice=getInvoice.getText();
 	   Log.message("Before Invoice is:"+Before_Invoice);
 	   SkySiteUtils.waitTill(5000);	
 	   String Before_Vendor=getvendor.getText();
 	   Log.message("Before Vendor is:"+Before_Vendor);
 	   SkySiteUtils.waitTill(5000);		
 	   chkAllProjectdocs.click();
	   Log.message("select all check box button has been clicked");
	   SkySiteUtils.waitTill(3000);	
	   btnmore1.click();
	   Log.message("more option button has been clicked");
	   SkySiteUtils.waitTill(3000);	
	   btniconmodifyfolder.click();
	   Log.message("modify attributes button has been clicked");
	   SkySiteUtils.waitTill(3000);
	   driver.switchTo().defaultContent();
 	   driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
 	   Log.message("Switch to SetBatchAttribute frame");
 	   SkySiteUtils.waitTill(3000);
 	   btniconplus1.click();
 	   Log.message("1st'+'button has been clicked");
 	   SkySiteUtils.waitTill(5000);		
 	   driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
 	   Log.message("Switch to CustomFieldLookup frame");
 	   SkySiteUtils.waitTill(3000);		
 	   //User Radio count		 
 	   int Radio_Count=0;
 	   int i= 0;
 	   List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
 	   for (WebElement Element : allElements)
 	   { 
 		   Radio_Count = Radio_Count+1; 	
 	   }	
 	   Log.message("Radio Count  is: "+Radio_Count);
 	   //Getting Radio count
 	   for(i = 2;i<=Radio_Count;i++)				 
 	   {					 
 		   String Document_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
 		   Log.message("Document Type is:"+Document_Type);
			
 		   if(!Document_Type.equals(Before_Document_Name))
 		   {	
 			   result1=true;
 			   Log.message("Document type Selection Successfully!!!!");	
 			   break;
 		   }
		}
		if((result1==true))
		{	
			SkySiteUtils.waitTill(5000);			
			String Document_Name=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
			Log.message("Document Name is:"+Document_Name);			
			driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
			SkySiteUtils.waitTill(10000);			
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
			Log.message("Switch to SetBatchAttribute frame");	
			SkySiteUtils.waitTill(3000);			
			if(!Before_Invoice.equals(Invoice))
			{
				driver.findElement(By.xpath("(//input[@class='form-control cf-textbox'])[1]")).sendKeys(Invoice);
				SkySiteUtils.waitTill(5000);	
			}
			else
			{
				driver.findElement(By.xpath("(//input[@class='form-control cf-textbox'])[1]")).sendKeys(Temp_Invoice);		
				SkySiteUtils.waitTill(5000);	
			}	
			SkySiteUtils.waitTill(3000);
			btniconplus3.click();
			Log.message("3rd'+'button has been clicked");
			SkySiteUtils.waitTill(5000);		
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
			Log.message("Switch to CustomFieldLookup frame");
			SkySiteUtils.waitTill(3000);			
			//User checkbox count		 
			int checkbox_count=0;
			int j= 0;
			List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
			for (WebElement Element : allElements1)
			{ 
				checkbox_count = checkbox_count+1; 	
			}	
			Log.message("checkbox Count  is: "+checkbox_count);
			for(j = 1;j<=checkbox_count;j++)				 
			{					 
				String Vendor_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
				Log.message("Vendor name is:"+Vendor_Type);
				
				if(!Vendor_Type.equals(Before_Vendor))
				{	
					result2=true;
					Log.message("Vendor name Selection Successfully!!!!");	
					break;
				}
			}
			if((result2==true))
			{	
				SkySiteUtils.waitTill(5000);					
				String Vendor_Name=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
				Log.message("Vendor Name is:"+Vendor_Name);				
				driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
				SkySiteUtils.waitTill(10000);		
				btnSaveNClose.click();
				Log.message("save button has been clicked ");	
				SkySiteUtils.waitTill(5000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));								
				Log.message("Switch to SetBatchAttribute frame");
				SkySiteUtils.waitTill(5000);	
				btnSaveNClose.click();
				Log.message("save & close button has been clicked ");	
				SkySiteUtils.waitTill(10000);			
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				Log.message("Switch to myFrame");
				SkySiteUtils.waitTill(5000);				
				int passcount=0;
				int failcount=0;
				int Document_Count=Total_Count;
				for(int k=1;k<=Document_Count;k++)
				{
					String Pre_Document_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[8])["+k+"]")).getText().toString();
					Log.message("After document name is:"+Pre_Document_Num);
					String After_Invoice=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+k+"]")).getText().toString();
					Log.message("After Invoice name is:"+After_Invoice);
					String After_Vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+k+"]")).getText().toString();
					Log.message("After Vendor name is:"+After_Vendor);
				
					if((Document_Name.equals(Pre_Document_Num))&&(Invoice.equals(After_Invoice))||(Temp_Invoice.equals(After_Invoice))
							&&(Vendor_Name.equals(After_Vendor)))
					{
						passcount=passcount+1;
						Log.message("Pass Count is:"+passcount);
					}
					else
					{
						failcount=failcount+1;
						Log.message("Fail count is:"+failcount);
					}
				}			
				if(passcount==Document_Count)
				{
					result3=true;
					Log.message("Modify Attribute Validation Successfully!!!!!");                             
				}		              
				else
				{      
					result3=false;
					Log.message("Modify Attribute Validation Failed!!!!!");             
				}	
			}
			else
			{
				result2=false;			
				Log.message("Vendor name Selection Failed!!!!");	
			}
		}			
		else
		{
			result1=false;			
			Log.message("Document type Selection Failed!!!!");				
		}		
		if((result1==true)&&(result2==true)&&(result3==true))
		{				
			Log.message("Modify Attributes Document and Invoice and Vendor Successfully!!!!");
			return true;
		}
		else
		{			
			Log.message("Modify Attributes Document and Invoice and Vendor Failed!!!!");
			return false;
		}
   }   
   
   @FindBy(css = ".day.today")
   WebElement btntoday;
   
   @FindBy(xpath = "//*[@id='divPaginationBelow']/div[2]/div/div[1]")
   WebElement btnpagination;
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[11]")
   WebElement txtprojectname;
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[12]")
   WebElement txtboolean;
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[14]")
   WebElement txtdate;
 
   @FindBy(xpath = "//i[@class='icon icon-calendar']")
   WebElement dateiconbtn;
   
   /** 
    * Method written for Modify Attribute on Project name and Boolean and Date
    * Scripted By: Sekhar     
    * @throws AWTException 
    */
 
   public boolean Modify_Attribute_Projectname_Boolean_Date(String ProjectName,String Temp_ProjectName) throws Exception 
   {
	   	boolean result1=false;
   		boolean result2=false;   	 
   		SkySiteUtils.waitTill(5000); 
   		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
   		SkySiteUtils.waitTill(5000); 
   		String Pagination_Count=btnpagination.getText();		
   		Log.message("Page count is: "+Pagination_Count);  		
   		String[] y= Pagination_Count.split(" ");
   		String AvlPageCount=y[1];
   		int Total_Count=Integer.parseInt(AvlPageCount);
   		Log.message("Total Available File Page count is:"+Total_Count);		
   		SkySiteUtils.waitTill(5000);   		
   		String Before_Project_Name=txtprojectname.getText();
   		Log.message("Before Project Name  is:"+Before_Project_Name);
   		SkySiteUtils.waitTill(5000);	
   		String Before_Boolean=txtboolean.getText();
   		Log.message("Before Boolean is:"+Before_Boolean);
   		SkySiteUtils.waitTill(5000);	
   		String Before_Date=txtdate.getText();
   		Log.message("Before Date is:"+Before_Date);
   		SkySiteUtils.waitTill(5000);	   		
   		chkAllProjectdocs.click();
   		Log.message("select all check box button has been clicked");
   		SkySiteUtils.waitTill(3000);	
   		btnmore1.click();
   		Log.message("more option button has been clicked");
   		SkySiteUtils.waitTill(3000);	
   		btniconmodifyfolder.click();
   		Log.message("modify attributes button has been clicked");
   		SkySiteUtils.waitTill(3000);			
   		driver.switchTo().defaultContent();
   		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   		SkySiteUtils.waitTill(3000);
   		if(!Before_Project_Name.equals(ProjectName))
   		{
   			driver.findElement(By.xpath("(//input[@class='form-control cf-textbox'])[2]")).clear();
   			driver.findElement(By.xpath("(//input[@class='form-control cf-textbox'])[2]")).sendKeys(ProjectName);
   			SkySiteUtils.waitTill(3000);
   		}
   		else
   		{
   			driver.findElement(By.xpath("(//input[@class='form-control cf-textbox'])[2]")).clear();
   			driver.findElement(By.xpath("(//input[@class='form-control cf-textbox'])[2]")).sendKeys(Temp_ProjectName);	
   			SkySiteUtils.waitTill(3000);
   		}		
   		//User Radio count		 
   		int Radio_Count=0;
   		int i= 0;
   		List<WebElement> allElements = driver.findElements(By.xpath("(//input[@type='radio'])"));//Taking Radio Count
   		for (WebElement Element : allElements)
   		{ 
   			Radio_Count = Radio_Count+1; 	
   		}	
   		Log.message("Radio Count  is: "+Radio_Count);
   		//Getting Radio count
   		for(i = 1;i<=Radio_Count;i++)				 
   		{					 
   			String Approved_Type=driver.findElement(By.xpath("(//div[@class='chose-option']/label)["+i+"]")).getText().toString();
   			Log.message("Approved Type is:"+Approved_Type);
   			
   			if(!Approved_Type.equals(Before_Boolean))
   			{	
   				result1=true;
   				Log.message("Boolean Type Selection Successfully!!!!");	
   				break;
   			}
   		}
   		if((result1==true))
   		{	
   			SkySiteUtils.waitTill(5000);	
   			Log.message("Boolean Type Selection Successfully!!!!");	
   			String Approved_BooleanName=driver.findElement(By.xpath("(//div[@class='chose-option']/label)["+i+"]")).getText().toString();
   			Log.message("Approved Name is:"+Approved_BooleanName);   			
   			driver.findElement(By.xpath("(//input[@type='radio'])["+i+"]")).click();
   			Log.message("Boolean radio button has been clicked");
   			SkySiteUtils.waitTill(10000);	   			
   			dateiconbtn.click();
   			Log.message("Date icon button has been clicked");
   			SkySiteUtils.waitTill(8000);   					
   			DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy"); 
   			Date date2 = dateFormat2.parse(dateFormat2.format(new Date()));
   			String today = dateFormat2.format(date2); 	
   			Log.message("Exp Today date is:"+today);
   			btntoday.click();
   			Log.message("Today button has been clicked");
   			SkySiteUtils.waitTill(10000);      				
   			driver.findElement(By.xpath("//button[@id='btnSaveNClose']")).click();
   			Log.message("Save button has been clicked");   			
   			SkySiteUtils.waitTill(8000);  	   			
   			driver.switchTo().defaultContent();
   			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
   			SkySiteUtils.waitTill(3000);				
   			int passcount=0;
   			int failcount=0;
   			int Document_Count=Total_Count;
   			for(int k=1;k<=Document_Count;k++)
   			{
   				String Pre_Project_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+k+"]")).getText().toString();
   				Log.message("After Project name is:"+Pre_Project_Name);
   				String After_Boolean=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+k+"]")).getText().toString();
   				Log.message("After Boolean is:"+After_Boolean);
   				String After_Date=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[14])["+k+"]")).getText().toString();
   				Log.message("After Date is:"+After_Date);
   				SkySiteUtils.waitTill(3000);
   				if((ProjectName.equals(Pre_Project_Name))||(Temp_ProjectName.equals(Pre_Project_Name)))
   				{  						
   					if((Approved_BooleanName.equals(After_Boolean))&&(today.equals(After_Date)))
   					{
   						passcount=passcount+1;
   						Log.message("Pass Count is:"+passcount);
   					}
   					else
   					{
   						failcount=failcount+1;
   						Log.message("Fail count is:"+failcount);
   					}
   				}
   				else
   				{
   					Log.message("Project name successfully!!!!");
   				}
   			}			
   			if(passcount==Document_Count)
   			{
   				result2=true;
   				Log.message("Modify Attribute Validation Successfully!!!!!");                             
   			}		              
   			else
   			{      
   				result2=false;
   				Log.message("Modify Attribute Validation Failed!!!!!");             
   			}			
   		}	
   		else
   		{
   			result1=false;
   			Log.message("Boolean Type Selection Failed!!!!");			
   		}		
   		if((result1==true)&&(result2==true))
   		{	  		
   			Log.message("Modify Attributes Project Name and boolean and Date Successfully!!!!");
   			return true;
   		}
   		else
   		{   			
   			Log.message("Modify Attributes Project Name and boolean and Date Failed!!!!");
   			return false;
   		}
   }
   
   /** 
    * Method written for Modify Attribute on Project number and vendor and date.
    * Scripted By: Sekhar     
    * @throws AWTException 
    */   
 
   public boolean Modify_Attribute_Date_Vendor_ProjectNumber() throws Exception 
   {   	
	   boolean result1=false;
	   boolean result2=false;
   	   boolean result3=false;   	
   	   SkySiteUtils.waitTill(5000);
   	   driver.switchTo().frame(driver.findElement(By.id("myFrame")));
   	   Log.message("Switch to my frame");
   	   String Pagination_Count=PaginationBelow.getText();		
   	   Log.message("Page count is: "+Pagination_Count);//Getting Page counts
   	   String[] y= Pagination_Count.split(" ");
   	   String AvlPageCount=y[1];
   	   int Total_Count=Integer.parseInt(AvlPageCount);
   	   Log.message("Total Available File Page count is: "+Total_Count);//Getting Page counts		
   	   SkySiteUtils.waitTill(5000);   		
   	   String Before_Project_Num=txtprojectname.getText();
   	   Log.message("Before Project Num  is:"+Before_Project_Num);
   	   SkySiteUtils.waitTill(5000);	
   	   String Before_Vendor=getvendor.getText();
   	   Log.message("Before Vendor is:"+Before_Vendor);
   	   SkySiteUtils.waitTill(5000);	
   	   String Before_Date=txtdate.getText();
   	   Log.message("Before Date is:"+Before_Date);
   	   SkySiteUtils.waitTill(5000);   		
   	   chkAllProjectdocs.click();
   	   Log.message("all check box button has been clicked");
   	   SkySiteUtils.waitTill(3000);	
   	   btnmore1.click();
   	   Log.message("more option button has been clicked");
   	   SkySiteUtils.waitTill(3000);	
   	   btniconmodifyfolder.click();
   	   Log.message("modify attributes button has been clicked");
   	   SkySiteUtils.waitTill(3000);			
   	   driver.switchTo().defaultContent();
   	   driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   	   Log.message("Switch to SetBatchAttribute frame");
   	   SkySiteUtils.waitTill(3000);
   	   btniconplus2.click();
   	   Log.message("2nd Plus button has been clicked");
   	   SkySiteUtils.waitTill(5000);		
   	   driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
   	   Log.message("Switch to CustomFieldLookup frame");
   	   SkySiteUtils.waitTill(3000);     		 
   	   int Radio_Count=0;
   	   int i= 0;
   	   List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
   	   for (WebElement Element : allElements)
   	   { 
   		   Radio_Count = Radio_Count+1; 	
   	   }	
   	   Log.message("Radio Count  is: "+Radio_Count);
   	   //Getting Radio count
   	   for(i = 2;i<=Radio_Count;i++)				 
   	   {					 
   		   String ProjectNum_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
   		   Log.message("Project Num Type is:"+ProjectNum_Type);   			
   		   if(!ProjectNum_Type.equals(Before_Project_Num))
   		   {	
   			   result1=true;
   			   Log.message("Project Number Selection Successfully!!!!");	
   			   break;
   		   }
   	   }
   	   if((result1==true))
   	   {	
   		   SkySiteUtils.waitTill(5000);   			
   		   String Project_Number=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
   		   Log.message("Project Number is:"+Project_Number);   			
   		   driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
   		   Log.message("Exp Project num has been clicked");
   		   SkySiteUtils.waitTill(10000);			
   		   driver.switchTo().defaultContent();
   		   driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   		   Log.message("Switch to SetBatchAttribute frame");
   		   SkySiteUtils.waitTill(3000);
   		   btniconplus3.click();
   		   Log.message("3rd plus button has been clicked");
   		   SkySiteUtils.waitTill(5000);		
   		   driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
   		   Log.message("Switch to CustomFieldLookup frame");
   		   SkySiteUtils.waitTill(3000);     					 
   		   int checkbox_count=0;
   		   int j= 0;
   		   List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
   		   for (WebElement Element : allElements1)
   		   { 
   			   checkbox_count = checkbox_count+1; 	
   		   }	
   		   Log.message("checkbox Count  is: "+checkbox_count);
   		   	
   		   for(j = 1;j<=checkbox_count;j++)				 
   		   {					 
   			   String Vendor_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
   			   Log.message("Vendor name is:"+Vendor_Type);
   				
   			   if(!Vendor_Type.equals(Before_Vendor))
   			   {	
   				   result2=true;
   				   Log.message("Vendor name Selection Successfully!!!!");	
   				   break;
   			   }
   		   }
   		   if((result2==true))
   		   {	
   				SkySiteUtils.waitTill(5000);   				
   				String Vendor_Name=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
   				Log.message("Vendor Name is:"+Vendor_Name);   				
   				driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
   				Log.message("Exp Vendor name has been clicked");	
   				SkySiteUtils.waitTill(10000);				
   				btnSaveNClose.click();
   				Log.message("save&close has been clicked");
   				SkySiteUtils.waitTill(5000);
   				driver.switchTo().defaultContent();
   				driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   				Log.message("Switch to SetBatchAttribute frame");
   				SkySiteUtils.waitTill(3000);	
   				dateiconbtn.click();
   				Log.message("date icon has been clicked");
   				SkySiteUtils.waitTill(5000);   				
   				DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy"); 
   				Date date2 = dateFormat2.parse(dateFormat2.format(new Date()));
   				String today = dateFormat2.format(date2); 
   				Log.message("Exp Today date is:"+today);
   	   			btntoday.click();
   	   			Log.message("Today button has been clicked");
   	   			SkySiteUtils.waitTill(10000);  					
   	   			btnSaveNClose.click();
   	   			Log.message("save button has been clicked");
   				SkySiteUtils.waitTill(5000);   				
   				driver.switchTo().defaultContent();
   				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
   				Log.message("Switch to myFrame");
   				SkySiteUtils.waitTill(3000);  			
   				int passcount=0;
   				int failcount=0;
   				int Document_Count=Total_Count;
   				for(int k=1;k<=Document_Count;k++)
   				{
   					String Pre_Project_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+k+"]")).getText().toString();
   					Log.message("After Project name is:"+Pre_Project_Num);
   					String After_vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+k+"]")).getText().toString();
   					Log.message("After Vendor name is:"+After_vendor);
   					String After_Date=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[14])["+k+"]")).getText().toString();
   					Log.message("After Date is:"+After_Date);
   					
   					if(Project_Number.equals(Pre_Project_Num)&&(Vendor_Name.equals(After_vendor))&&(After_Date.equals(today)))
   					{
   						passcount=passcount+1;
   						Log.message("Pass Count is:"+passcount);
   					}
   					else
   					{
   						failcount=failcount+1;
   						Log.message("Fail count is:"+failcount);
   					}
   				}			
   				if(passcount==Document_Count)
   				{
   					result3=true;
   					Log.message("Modify Attribute Validation Successfully!!!!!");                             
   				}		              
   				else
   				{      
   					result3=false;
   					Log.message("Modify Attribute Validation Failed!!!!!");             
   				}				
   			}
   			else
   			{
   				result2=false;
   				Log.message("Vendor name Selection Failed!!!!");					
   			}
   		}			
   		else
   		{
   			result1=false;			
   			Log.message("Project Number Selection Failed!!!!");				
   		}
   		if((result1==true)&&(result2==true)&&(result3==true))
   		{	   			
   			Log.message("Project Num and Vendor Modifyed Successfully!!!!");
   			return true;
   		}   		
   		else
   		{   		
   			Log.message("Project Num and Vendor Modifyed Failed!!!!");
   			return false;
   		}
   	}
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[13]")
   WebElement txtAmount;  
   
   @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[15]")
   WebElement txtChequeNumber;
   
   @FindBy(xpath = "//input[@class='form-control currencyInput']")
   WebElement txtcurrencyInput;   
  
   @FindBy(xpath = "(//input[@class='form-control cf-textbox'])[3]")
   WebElement txtCheque;
   
   @FindBy(xpath = "(//input[@class='form-control cf-textbox'])[2]")
   WebElement txtProjectnam;
   
   @FindBy(xpath = "(//input[@class='form-control cf-textbox'])[1]")
   WebElement txtInvoi;
   
   
   
   /** 
    * Method written for Modify All Attribute on required Validations.
    * Scripted By: Sekhar     
    * @throws AWTException 
    */  
    public boolean Modify_All_Attribute_Required_Validations(String Invoice,String Temp_Invoice,String ProjectName,String Temp_ProjectName,
    		String Amount,String Temp_Amount,String Cheque_Numb,String Temp_Cheque_Numb) throws Exception 
    {
   	
	   	boolean result1=false;
   		boolean result2=false;
   		boolean result3=false;
   		boolean result4=false;
   		boolean result5=false;    
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
   		String Pagination_Count=btnpagination.getText();		
   		Log.message("Page count is: "+Pagination_Count);//Getting Page counts
   		String[] y= Pagination_Count.split(" ");
   		String AvlPageCount=y[1];
   		int Total_Count=Integer.parseInt(AvlPageCount);
   		Log.message("Total Available File Page count is: "+Total_Count);//Getting Page counts		
   		SkySiteUtils.waitTill(5000);   		
   		String Before_DocumentName=getDocumentname.getText();
   		Log.message("Before Documentt Name is:"+Before_DocumentName);
   		SkySiteUtils.waitTill(5000);
   		String Before_Invoice=getInvoice.getText();
   		Log.message("Before Invoice is:"+Before_Invoice);
   		SkySiteUtils.waitTill(3000);	
   		String Before_Project_Num=getProjectnum.getText();
   		Log.message("Before Project Num  is:"+Before_Project_Num);
   		SkySiteUtils.waitTill(3000);
   		String Before_Project_Name=txtprojectname.getText();
   		Log.message("Before Project Name is:"+Before_Project_Name);
   		SkySiteUtils.waitTill(3000);
   		String Before_Approved=txtboolean.getText();
   		Log.message("Before Approved is:"+Before_Approved);
   		SkySiteUtils.waitTill(3000);	
   		String Before_Amount=txtAmount.getText();
   		Log.message("Before Amount is:"+Before_Amount);
   		SkySiteUtils.waitTill(3000);
   		String Before_Date=txtdate.getText();
   		Log.message("Before Date is:"+Before_Date);
   		SkySiteUtils.waitTill(3000);	
   		String Before_Cheque_Number=txtChequeNumber.getText();
   		Log.message("Before Cheque Number is:"+Before_Cheque_Number);
   		SkySiteUtils.waitTill(3000);	
   		String Before_Vendor=getvendor.getText();
   		Log.message("Before Vendor is:"+Before_Vendor);
   		SkySiteUtils.waitTill(5000);   		
   		chkAllProjectdocs.click();
   		Log.message("select all check box button has been clicked");
   		SkySiteUtils.waitTill(3000);	
   		btnmore1.click();
   		Log.message("more option button has been clicked");
   		SkySiteUtils.waitTill(3000);	
   		btniconmodifyfolder.click();
   		Log.message("modify attributes button has been clicked");
   		SkySiteUtils.waitTill(3000);			
   		driver.switchTo().defaultContent();
   		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   		Log.message("Switch to SetBatchAttribute Frame");
   		SkySiteUtils.waitTill(3000);
   		btniconplus1.click();
   		Log.message("1st plus button has been clicked");
   		SkySiteUtils.waitTill(5000);		
   		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
   		Log.message("Switch to CustomFieldLookup Frame");
   		SkySiteUtils.waitTill(3000);   		
   		//User Radio count		 
   		int Radio_Count=0;
   		int i= 0;
   		List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
   		for (WebElement Element : allElements)
   		{ 
   			Radio_Count = Radio_Count+1; 	
   		}	
   		Log.message("Radio Count  is: "+Radio_Count);
   		//Getting Radio count
   		for(i = 2;i<=Radio_Count;i++)				 
   		{					 
   			String Document_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
   			Log.message("Document is:"+Document_Type);
   			
   			if(!Document_Type.equals(Before_DocumentName))
   			{	
   				result1=true;
   				break;
   			}
   		}
   		if((result1==true))
   		{	
   			SkySiteUtils.waitTill(5000);	
   			
   			String Document_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
   			Log.message("Document Type is:"+Document_Type);   			
   			driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
   			Log.message("Document Type Selection Successfully!!!!");	
   			SkySiteUtils.waitTill(10000);   			   			
   			driver.switchTo().defaultContent();
   			driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   			Log.message("Switch to SetBatchAttribute Frame");
   			SkySiteUtils.waitTill(3000);
   			if(!Before_Invoice.equals(Invoice))
   			{
   				txtInvoi.sendKeys(Invoice);
   				Log.message("Invoice has been entered:"+Invoice);
   				SkySiteUtils.waitTill(5000);	
   			}
   			else
   			{
   				txtInvoi.sendKeys(Temp_Invoice);
   				Log.message("Invoice has been entered:"+Temp_Invoice);
   				SkySiteUtils.waitTill(5000);	
   			}
   			SkySiteUtils.waitTill(3000);	
   			btniconplus2.click();
   			Log.message("2nd plus button has been clicked");
   			SkySiteUtils.waitTill(5000);		
   			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
   			Log.message("Switch to CustomFieldLookup Frame");
   			SkySiteUtils.waitTill(3000);
   			//User checkbox count		 
   			int checkbox_count=0;
   			int j= 0;
   			List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
   			for (WebElement Element : allElements1)
   			{ 
   				checkbox_count = checkbox_count+1; 	
   			}	
   			Log.message("checkbox Count  is: "+checkbox_count);
   			//Getting checkbox count
   			for(j = 2;j<=checkbox_count;j++)				 
   			{					 
   				String Project_Numb=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
   				Log.message("Project Number is:"+Project_Numb);
   				
   				if(!Project_Numb.equals(Before_Project_Num))
   				{	
   					result2=true;
   					break;
   				}
   			}
   			if((result2==true))
   			{	
   				SkySiteUtils.waitTill(5000);   				
   				String Project_Numb=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
   				Log.message("Project Number is:"+Project_Numb);   				
   				driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
   				Log.message("Project Number Selection Successfully!!!!");	  
   				SkySiteUtils.waitTill(10000);
   				driver.switchTo().defaultContent();
   				driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   				Log.message("Switch to SetBatchAttribute Frame");	
   				SkySiteUtils.waitTill(3000);   				
   				if(!Before_Project_Name.equals(ProjectName))
   				{
   					txtProjectnam.sendKeys(ProjectName);
   					Log.message("Projectname has been entered:"+ProjectName);
   				}
   				else
   				{
   					txtProjectnam.sendKeys(Temp_ProjectName);	
   					Log.message("Projectname has been entered:"+Temp_ProjectName);
   				}   							
   				//User Radio count		 
   				int App_Radio_Count=0;
   				int k= 0;
   				List<WebElement> allElements2 = driver.findElements(By.xpath("(//input[@type='radio'])"));//Taking Radio Count
   				for (WebElement Element : allElements2)
   				{ 
   					App_Radio_Count = App_Radio_Count+1; 	
   				}	
   				Log.message("Radio Count  is: "+App_Radio_Count);
   				//Getting Radio count
   				for(k = 1;k<=App_Radio_Count;k++)				 
   				{					 
   					String Approved_Type=driver.findElement(By.xpath("(//div[@class='chose-option']/label)["+k+"]")).getText().toString();
   					Log.message("Approved Type is:"+Approved_Type);
   					
   					if(!Approved_Type.equals(Before_Approved))
   					{	
   						result3=true;
   						break;
   					}
   				}
   				if((result3==true))
   				{	
   					SkySiteUtils.waitTill(5000);	
   					Log.message("Boolean Type Selection Successfully!!!!");	
   					String Approved_Name=driver.findElement(By.xpath("(//div[@class='chose-option']/label)["+k+"]")).getText().toString();
   					Log.message("Approved Name is:"+Approved_Name);   					
   					driver.findElement(By.xpath("(//input[@type='radio'])["+k+"]")).click();//clicking on radio button icon
   					Log.message("Boolean Type Selection Successfully!!!!");	   
   					SkySiteUtils.waitTill(10000);
   					if(!Before_Amount.equals(Amount))
   					{
   						txtcurrencyInput.sendKeys(Amount);
   						Log.message("Amount has been entered:"+Amount);
   					}
   					else
   					{
   						txtcurrencyInput.sendKeys(Temp_Amount);	
   						Log.message("Amount has been entered:"+Temp_Amount);
   					}					
   					SkySiteUtils.waitTill(5000);   				
   					dateiconbtn.click();
   					Log.message("Date icon button has been clicked");
   					SkySiteUtils.waitTill(5000);   				
   					DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy"); 
   					Date date2 = dateFormat2.parse(dateFormat2.format(new Date()));
   					String today = dateFormat2.format(date2); 	   					
   					Log.message("Exp Today date is:"+today);
   		   			btntoday.click();
   		   			Log.message("Today button has been clicked");
   		   			SkySiteUtils.waitTill(10000);    				
   					if(!Before_Cheque_Number.equals(Cheque_Numb))
   					{
   						txtCheque.sendKeys(Cheque_Numb);
   						Log.message("Cheque Numb has been entered:"+Cheque_Numb);
   					}
   					else
   					{
   						txtCheque.sendKeys(Temp_Cheque_Numb);
   						Log.message("Cheque Numb has been entered:"+Temp_Cheque_Numb);
   					}				
   					SkySiteUtils.waitTill(5000);				
   					btniconplus3.click();
   					Log.message("3rd plus button has been clicked");
   					SkySiteUtils.waitTill(5000);		
   					driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
   					Log.message("Switch to CustomFieldLookup Frame");
   					SkySiteUtils.waitTill(3000);     							 
   					int Vendorcheck_count=0;
   					int m= 0;
   					List<WebElement> allElements3 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
   					for (WebElement Element : allElements3)
   					{ 
   						Vendorcheck_count = Vendorcheck_count+1; 	
   					}	
   					Log.message("Vendor check box Count  is: "+Vendorcheck_count);
   					for(m = 1;m<=Vendorcheck_count;m++)				 
   					{					 
   						String Vendor_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+m+"]")).getText().toString();
   						Log.message("Vendor name is:"+Vendor_Type);
   					
   						if(!Vendor_Type.equals(Before_Vendor))
   						{	
   							result4=true;
   							break;
   						}
   					}
   					if((result4==true))
   					{	
   						SkySiteUtils.waitTill(5000);						
   						String Vendor_Name=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
   						Log.message("Vendor Name is:"+Vendor_Name);					
   						driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
   						Log.message("Vendor name Selection Successfully!!!!");	
   						SkySiteUtils.waitTill(10000);
   						btnSaveNClose.click();
   						Log.message("save&close button has been clicked");	
   						SkySiteUtils.waitTill(5000);
   						driver.switchTo().defaultContent();
   						driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
   						Log.message("Switch to SetBatchAttribute Frame");
   						SkySiteUtils.waitTill(3000);				
   						btnSaveNClose.click();
   						Log.message("save button has been clicked");
   						SkySiteUtils.waitTill(5000);	
   						driver.switchTo().defaultContent();
   						driver.switchTo().frame(driver.findElement(By.id("myFrame")));
   						Log.message("Switch to myFrame");
   						SkySiteUtils.waitTill(3000);   			
   						int passcount=0;
   						int failcount=0;
   						int Document_Count=Total_Count;
   						for(int n=1;n<=Document_Count;n++)
   						{
   							String After_DocumentName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[8])["+n+"]")).getText();
   							Log.message("After Documentt Name is:"+After_DocumentName);							
   							String After_Invoice=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+n+"]")).getText();
   							Log.message("After Invoice is:"+After_Invoice);						
   							String After_Project_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+n+"]")).getText();
   							Log.message("After Project Num  is:"+After_Project_Num);							
   							String After_Project_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+n+"]")).getText();
   							Log.message("After Project Name is:"+After_Project_Name);							
   							String After_Approved=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+n+"]")).getText();
   							Log.message("After Approved is:"+After_Approved);						
   							String After_Amount=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[13])["+n+"]")).getText();
   							Log.message("After Amount is:"+After_Amount);						
   							String After_Date=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[14])["+n+"]")).getText();
   							Log.message("After Date is:"+After_Date);							
   							String After_Cheque_Number=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[15])["+n+"]")).getText();
   							Log.message("After Cheque Number is:"+After_Cheque_Number);							
   							String After_Vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+n+"]")).getText();
   							Log.message("After Vendor is:"+After_Vendor);   							
   							SkySiteUtils.waitTill(5000);	
   							if((Invoice.equals(After_Invoice))||(Temp_Invoice.equals(After_Invoice)))
   							{
   								if((ProjectName.equals(After_Project_Name))||(Temp_ProjectName.equals(After_Project_Name)))
   								{						
   								if((Amount.equals(After_Amount))||(Temp_Amount.equals(After_Amount)))
   								{
   								if((Cheque_Numb.equals(After_Cheque_Number))||(Temp_Cheque_Numb.equals(After_Cheque_Number)))
   								{
   									if(Document_Type.equals(After_DocumentName)&&(Project_Numb.equals(After_Project_Num))
   											&&(Approved_Name.equals(After_Approved))&&(After_Date.equals(today))
   											&&(Vendor_Name.equals(After_Vendor)))
   									{
   										passcount=passcount+1;
   										Log.message("Pass Count is:"+passcount);
   									}
   									else
   									{
   										failcount=failcount+1;
   										Log.message("Fail count is:"+failcount);
   									}
   								}}}
   							}
   						}			
   						if(passcount==Document_Count)
   						{
   							result5=true;
   							Log.message("Modify Attribute Validation Successfully!!!!!");                             
   						}		              
   						else
   						{      
   							result5=false;
   							Log.message("Modify Attribute Validation Failed!!!!!");             
   						}	
   					}
   					else
   					{
   						result4=false;
   						Log.message("Vendor name Selection Failed!!!!");							
   					}				
   				}
   				else
   				{
   					result3=false;
   					Log.message("Boolean Type Selection Failed!!!!");	
   				}
   			}
   			else
   			{
   				result2=false;
   				Log.message("Project Number Selection Failed!!!!");
   			}
   		}			
   		else
   		{
   			result1=false;			
   			Log.message("Document Type Selection Failed!!!!");	
   		}
   		if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true)&&(result5==true))
   		{	   			
   			Log.message("All Attributes Modified Successfully!!!!");
   			return true;
   		}
   		else
   		{   			
   			Log.message("All Attributes Modified Failed!!!!");
   			return false;
   		}
   	}
   
    @FindBy(xpath = "(//i[@class='icon icon-copy-files'])[2]")
    WebElement iconcopyfiles;
    
    /** 
     * Method written for Modify All Attribute the same files Copied in different folder and validate modified attributes.
     * Scripted By: Sekhar    
     * @throws AWTException 
     */  
    public boolean CopyFiles_Within_Collection_Attributes(String FolderName1) 
    {    	
    	boolean result1=false;
    	boolean result2=false;
    	boolean result3=false;
    	boolean result4=false;    	
    	SkySiteUtils.waitTill(5000);
    	String Before_DocumentName=getDocumentname.getText();
    	Log.message("Before Documentt Name is:"+Before_DocumentName);
    	SkySiteUtils.waitTill(3000);
    	String Before_Invoice=getInvoice.getText();
    	Log.message("Before Invoice is:"+Before_Invoice);
    	SkySiteUtils.waitTill(3000);	
    	String Before_Project_Num=getProjectnum.getText();
    	Log.message("Before Project Num  is:"+Before_Project_Num);
    	SkySiteUtils.waitTill(3000);
    	String Before_Project_Name=txtprojectname.getText();
    	Log.message("Before Project Name is:"+Before_Project_Name);
    	SkySiteUtils.waitTill(3000);
    	String Before_Approved=txtboolean.getText();
    	Log.message("Before Approved is:"+Before_Approved);
    	SkySiteUtils.waitTill(3000);	
    	String Before_Amount=txtAmount.getText();
    	Log.message("Before Amount is:"+Before_Amount);
    	SkySiteUtils.waitTill(3000);
    	String Before_Date=txtdate.getText();
    	Log.message("Before Date is:"+Before_Date);
    	SkySiteUtils.waitTill(3000);	
    	String Before_Cheque_Number=txtChequeNumber.getText();
    	Log.message("Before Cheque Number is:"+Before_Cheque_Number);
    	SkySiteUtils.waitTill(3000);	
    	String Before_Vendor=getvendor.getText();
    	Log.message("Before Vendor is:"+Before_Vendor);
    	SkySiteUtils.waitTill(5000); 
    	chkAllProjectdocs.click();
   		Log.message("select all check box button has been clicked");
   		SkySiteUtils.waitTill(3000);	
   		btnmore1.click();
   		Log.message("more option button has been clicked");
   		SkySiteUtils.waitTill(3000);   		
   		iconcopyfiles.click();
   		Log.message("copy files button has been clicked");
    	SkySiteUtils.waitTill(3000);    		
    	int Folder_Count=0;
    	int j = 0;
    	List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"));//Taking check box Count
    	for (WebElement Element : allElements1)
    	{ 
    		Folder_Count = Folder_Count+1; 	
    	}
    	Log.message("Folder Count is: "+Folder_Count);
    	//Getting Image count
    	for(j = 1;j<=Folder_Count;j++)
    	{					
    		String Exp_FolderName=driver.findElement(By.xpath("(//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+j+"]")).getText().toString();
    		Log.message("Exp Folder Name is:"+Exp_FolderName);			
    		if(Exp_FolderName.trim().contentEquals(FolderName1))
    		{	
    			result1=true;
    			Log.message("Exp Folder Name is selected Successfully!!!!");	
    			break;
    		}
    	}
    	if(result1==true)
    	{			
    		SkySiteUtils.waitTill(3000);	
    		driver.findElement(By.xpath("(//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+j+"]")).click();
    		Log.message("Exp folder has been clicked");
    		SkySiteUtils.waitTill(3000);	
    		driver.findElement(By.xpath("//*[@id='popup']/div/div/div[3]/div[2]/input[1]")).click();		
    		Log.message("Ok Button has been clicked");
    		SkySiteUtils.waitTill(15000); 
    		driver.findElement(By.xpath("(//i[@class='icon icon-refresh ico-lg'])[1]")).click();
    		Log.message("Refresh button has been clicked");
    		SkySiteUtils.waitTill(8000);
    		int Par_Folder_Count=0;
    		int i = 0;
    		List<WebElement> allElements = driver.findElements(By.xpath("//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;']"));//Taking Parent folder Count
    		for (WebElement Element : allElements)
    		{ 
    			Par_Folder_Count = Par_Folder_Count+1; 	
    		}
    		Log.message("Parent Folder Count is: "+Par_Folder_Count);
    		//Getting Image count
    		for(i = 2;i<=Par_Folder_Count;i++)
    		{					
    			String Exp_FolderName=driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])["+i+"]")).getText().toString();
    			Log.message("Exp Parent Folder Name is:"+Exp_FolderName);
    			SkySiteUtils.waitTill(3000);
    			if(Exp_FolderName.trim().equals(FolderName1))
    			{	
    				result2=true;
    				break;
    			}
    		}
    		if(result2==true)
    		{			
    			SkySiteUtils.waitTill(5000);    			
    			driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])["+i+"]")).click();//select the check box
    			Log.message("Exp Parent Folder Name is selected Successfully!!!!"+FolderName1);	
    			SkySiteUtils.waitTill(15000);
    			String AfterUpload_Count=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div")).getText();//Getting label counts
    			Log.message("After Copied File count is: "+AfterUpload_Count);//Getting Lite user counts		
    			SkySiteUtils.waitTill(3000); 
    			String[] x= AfterUpload_Count.split(" ");
    			String AvlAfterUpload_Count=x[1];
    			int CountAfter=Integer.parseInt(AvlAfterUpload_Count);
    			Log.message("Avl Copied File count is: "+CountAfter);//Getting Lite user counts
    			SkySiteUtils.waitTill(5000);    		
    			//User Name count		 
    			int Image_Count=0;		
    			List<WebElement> allElements2 = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
    			for (WebElement Element : allElements2)
    			{ 
    				Image_Count = Image_Count+1; 	
    			}	
    			Log.message("UserName Count  is: "+Image_Count);    											
    			//Getting Contact count		
    			if(Image_Count==CountAfter)
    			{
    				result3=true;
    				Log.message("Exp files Copied Successfully");
    				SkySiteUtils.waitTill(5000);	    					
    				int passcount=0;
    				int failcount=0;
    				int Document_Count=CountAfter;
    				for(int n=1;n<=Document_Count;n++)
    				{
    					String After_DocumentName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[8])["+n+"]")).getText();
    					Log.message("After Documentt Name is:"+After_DocumentName);							
    					String After_Invoice=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+n+"]")).getText();
    					Log.message("After Invoice is:"+After_Invoice);						
    					String After_Project_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+n+"]")).getText();
    					Log.message("After Project Num  is:"+After_Project_Num);							
    					String After_Project_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+n+"]")).getText();
    					Log.message("After Project Name is:"+After_Project_Name);							
    					String After_Approved=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+n+"]")).getText();
    					Log.message("After Approved is:"+After_Approved);						
    					String After_Amount=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[13])["+n+"]")).getText();
    					Log.message("After Amount is:"+After_Amount);						
    					String After_Date=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[14])["+n+"]")).getText();
    					Log.message("After Date is:"+After_Date);							
    					String After_Cheque_Number=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[15])["+n+"]")).getText();
    					Log.message("After Cheque Number is:"+After_Cheque_Number);							
    					String After_Vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+n+"]")).getText();
    					Log.message("After Vendor is:"+After_Vendor);  					
    					SkySiteUtils.waitTill(5000);
    					if(Before_DocumentName.equals(After_DocumentName)&&(Before_Invoice.equals(After_Invoice))&&(Before_Project_Num.equals(After_Project_Num))
    							&&(Before_Project_Name.equals(After_Project_Name))&&(Before_Approved.equals(After_Approved))
    							&&(Before_Amount.equals(After_Amount))&&(Before_Date.equals(After_Date))
    							&&(Before_Cheque_Number.equals(After_Cheque_Number))&&(Before_Vendor.equals(After_Vendor)))
    					{	
    						passcount=passcount+1;
    						Log.message("Pass Count is:"+passcount);
    					}
    					else
    					{
    						failcount=failcount+1;
    						Log.message("Fail count is:"+failcount);								
    					}							
    				}			
    				if(passcount==Document_Count)
    				{
    					result4=true;
    					Log.message("Modify Attribute Copy Files Validation Successfully!!!!!");                             
    				}		              
    				else
    				{      
    					result4=false;
    					Log.message("Modify Attribute Copy Files Validation Failed!!!!!");             
    				}						
    			}
    			else
    			{
    				result3=false;
    				Log.message("Exp files Copied Failed");
    			}
    		}
    		else
    		{
    			result2=false;
    			Log.message("Exp Parent Folder Name is selected Failed!!!!");					
    		}		
    	}
    	else
    	{
    		result1=false;
    		Log.message("Exp Folder Name is selected Failed!!!!");				
    	}
    	if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true))
    	{    			
    		Log.message("Modify Attribute Copy Files different folder Validation Successfully!!!!");
    		return true;
    	}
    	else
    	{    			
    		Log.message("Modify Attribute Copy Files different folder Validation Failed!!!!");
    		return false;
    	}
    } 
    
    @FindBy(xpath = "//i[@class='icon icon-play-single icon-lg']")
    WebElement btnpalyicon;
    
    @FindBy(xpath = "//input[@id='txtCurPage_GridBottom']")
    WebElement btnGridBottom;
    
    @FindBy(xpath = "//*[@id='divPaginationBelow']/div[2]/div/div[1]")
    WebElement btnPaginationBelow;
    
    /** 
     * Method written for Navigate After Modify All Attribute.
     * Scripted By: Sekhar    
     * @throws AWTException 
     */    
  
    public boolean Navigate_Modify_All_Attribute(String Invoice,String Temp_Invoice,String ProjectName,String Temp_ProjectName,
    		String Amount,String Temp_Amount,String Cheque_Numb,String Temp_Cheque_Numb) throws Exception 
    {    	
    	boolean result1=false;
    	boolean result2=false;
    	boolean result3=false;
    	boolean result4=false;
    	boolean result5=false;
    	boolean result6=false;    	 
    	SkySiteUtils.waitTill(15000);	
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to myFrame"); 
    	btnpalyicon.click();
    	Log.message("paly icon button has been clicked"); 
    	SkySiteUtils.waitTill(20000);    		
    	String After_Page_Count=btnGridBottom.getAttribute("value");
    	Log.message("After Navigate Page Count is:"+After_Page_Count);    	
    	if(After_Page_Count.equals("2"))
    	{	
    		result1=true;
    		Log.message("Page navigation Successfully!!!!!!");    		
    		String Pagination_Count=btnPaginationBelow.getText();		
    		Log.message("Page count is: "+Pagination_Count);//Getting Page counts
    		String[] y= Pagination_Count.split(" ");
    		String AvlPageCount=y[1];
    		int Total_Count=Integer.parseInt(AvlPageCount);
    		Log.message("Total Available File Page count is: "+Total_Count);//Getting Page counts		
    		SkySiteUtils.waitTill(5000);    			
    		String Before_DocumentName=getDocumentname.getText();
    		Log.message("Before Documentt Name is:"+Before_DocumentName);
    		SkySiteUtils.waitTill(5000);
    		String Before_Invoice=getInvoice.getText();
    		Log.message("Before Invoice is:"+Before_Invoice);
    		SkySiteUtils.waitTill(3000);	
    		String Before_Project_Num=getProjectnum.getText();
    		Log.message("Before Project Num  is:"+Before_Project_Num);
    		SkySiteUtils.waitTill(3000);
    		String Before_Project_Name=txtprojectname.getText();
    		Log.message("Before Project Name is:"+Before_Project_Name);
    		SkySiteUtils.waitTill(3000);
    		String Before_Approved=txtboolean.getText();
    		Log.message("Before Approved is:"+Before_Approved);
    		SkySiteUtils.waitTill(3000);	
    		String Before_Amount=txtAmount.getText();
    		Log.message("Before Amount is:"+Before_Amount);
    		SkySiteUtils.waitTill(3000);
    		String Before_Date=txtdate.getText();
    		Log.message("Before Date is:"+Before_Date);
    		SkySiteUtils.waitTill(3000);	
    		String Before_Cheque_Number=txtChequeNumber.getText();
    		Log.message("Before Cheque Number is:"+Before_Cheque_Number);
    		SkySiteUtils.waitTill(3000);	
    		String Before_Vendor=getvendor.getText();
    		Log.message("Before Vendor is:"+Before_Vendor);
    		SkySiteUtils.waitTill(5000);    			
    		chkAllProjectdocs.click();
    		Log.message("select all check box has been clicked");
    		SkySiteUtils.waitTill(3000);	
    		btnmore1.click();
    		Log.message("more option button has been clicked");
    		SkySiteUtils.waitTill(3000);	
    		btniconmodifyfolder.click();
    		Log.message("modify attributes button has been clicked");
    		SkySiteUtils.waitTill(3000);					
    		driver.switchTo().defaultContent();
    		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    		Log.message("Switch to SetBatchAttribute Frame"); 
    		SkySiteUtils.waitTill(3000);
    		btniconplus1.click();
    		Log.message("1st plus button has been clicked");
    		SkySiteUtils.waitTill(5000);		
    		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
    		Log.message("Switch to CustomFieldLookup Frame");
    		SkySiteUtils.waitTill(3000);    			
    		//User Radio count		 
    		int Radio_Count=0;
    		int i= 0;
    		List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
    		for (WebElement Element : allElements)
    		{ 
    			Radio_Count = Radio_Count+1; 	
    		}	
    		Log.message("Radio Count  is: "+Radio_Count);
    		//Getting Radio count
    		for(i = 2;i<=Radio_Count;i++)				 
    		{					 
    			String Document_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
    			Log.message("Document is:"+Document_Type);
    				
    			if(!Document_Type.equals(Before_DocumentName))
    			{	
    				result2=true;
    				Log.message("Document Type Selection Successfully!!!!");	
    				break;
    			}
    		}
    		if((result2==true))
    		{	
    			SkySiteUtils.waitTill(5000);				
    			String Document_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
    			Log.message("Document Type is:"+Document_Type);    				
    			driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
    			SkySiteUtils.waitTill(10000);    				
    			driver.switchTo().defaultContent();
    			driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    			Log.message("Switch to SetBatchAttribute Frame");
    			SkySiteUtils.waitTill(3000);
    			if(!Before_Invoice.equals(Invoice))
    			{
    				txtInvoi.sendKeys(Invoice);
    				Log.message("invoice has been entered");
    				SkySiteUtils.waitTill(5000);	
    			}
    			else
    			{
    				txtInvoi.sendKeys(Temp_Invoice);		
    				Log.message("invoice has been entered");
    				SkySiteUtils.waitTill(5000);	
    			}
    			SkySiteUtils.waitTill(3000);	
    			btniconplus2.click();
    			Log.message("2nd plus button has been clicked");
    			SkySiteUtils.waitTill(5000);		
    			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
    			Log.message("Switch to CustomFieldLookup Frame");
    			SkySiteUtils.waitTill(3000);
    			//User checkbox count		 
    			int checkbox_count=0;
    			int j= 0;
    			List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
    			for (WebElement Element : allElements1)
    			{ 
    				checkbox_count = checkbox_count+1; 	
    			}	
    			Log.message("checkbox Count  is: "+checkbox_count);
    			//Getting checkbox count
    			for(j = 2;j<=checkbox_count;j++)				 
    			{					 
    				String Project_Numb=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
    				Log.message("Project Number is:"+Project_Numb);
    					
    				if(!Project_Numb.equals(Before_Project_Num))
    				{	
    					result3=true;
    					break;
    				}
    			}
    			if((result3==true))
    			{	
    				SkySiteUtils.waitTill(5000);	    				
    				String Project_Numb=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
    				Log.message("Collection Number is:"+Project_Numb);    					
    				driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
    				Log.message("Collection Number Selection Successfully!!!!");
    				SkySiteUtils.waitTill(10000);
    				driver.switchTo().defaultContent();
    				driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    				Log.message("Switch to SetBatchAttribute Frame");
    				SkySiteUtils.waitTill(3000);	
    				
    				if(!Before_Project_Name.equals(ProjectName))
    				{
    					txtProjectnam.sendKeys(ProjectName);
    					Log.message("Collection name has been entered");
    				}
    				else
    				{
    					txtProjectnam.sendKeys(Temp_ProjectName);	
    					Log.message("Collection name has been entered");
    				}    							
    				//User Radio count		 
    				int App_Radio_Count=0;
    				int k= 0;
    				List<WebElement> allElements2 = driver.findElements(By.xpath("(//input[@type='radio'])"));//Taking Radio Count
    				for (WebElement Element : allElements2)
    				{ 
    					App_Radio_Count = App_Radio_Count+1; 	
    				}	
    				Log.message("Radio Count  is: "+App_Radio_Count);
    				//Getting Radio count
    				for(k = 1;k<=App_Radio_Count;k++)				 
    				{					 
    					String Approved_Type=driver.findElement(By.xpath("(//*[@id='161184']/div/label)["+k+"]")).getText().toString();
    					Log.message("Approved Type is:"+Approved_Type);
    					
    					if(!Approved_Type.equals(Before_Approved))
    					{	
    						result4=true;
    						//Log.message("Boolean Type Selection Successfully!!!!");	
    						break;
    					}
    				}
    				if((result4==true))
    				{	
    					SkySiteUtils.waitTill(5000);						
    					String Approved_Name=driver.findElement(By.xpath("(//*[@id='161184']/div/label)["+k+"]")).getText().toString();
    					Log.message("Approved Name is:"+Approved_Name);    					
    					driver.findElement(By.xpath("(//input[@type='radio'])["+k+"]")).click();//clicking on radio button icon
    					Log.message("Boolean Type Selection Successfully!!!!");	
    					SkySiteUtils.waitTill(10000);
    					if(!Before_Amount.equals(Amount))
    					{
    						txtcurrencyInput.sendKeys(Amount);
    						Log.message("Amount has been entered");	
    					}
    					else
    					{
    						txtcurrencyInput.sendKeys(Temp_Amount);	
    						Log.message("Amount has been entered");
    					}					
    					SkySiteUtils.waitTill(5000);    					
    					dateiconbtn.click();
       					Log.message("Date icon button has been clicked");
       					SkySiteUtils.waitTill(5000);   				
       					DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy"); 
       					Date date2 = dateFormat2.parse(dateFormat2.format(new Date()));
       					String today = dateFormat2.format(date2); 	   					
       					Log.message("Exp Today date is:"+today);
       		   			btntoday.click();
       		   			Log.message("Today button has been clicked");
    					SkySiteUtils.waitTill(5000);    					
    					if(!Before_Cheque_Number.equals(Cheque_Numb))
    					{
    						txtCheque.sendKeys(Cheque_Numb);
    						Log.message("Cheque num has been entered");
    					}
    					else
    					{
    						txtCheque.sendKeys(Temp_Cheque_Numb);	
    						Log.message("Cheque num has been entered");
    					}				
    					SkySiteUtils.waitTill(5000);				
    					btniconplus3.click();
    					Log.message("3rd plus button has been clicked");
    					SkySiteUtils.waitTill(5000);		
    					driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
    					Log.message("Switch to CustomFieldLookup Frame");
    					SkySiteUtils.waitTill(3000);    					
    					//User checkbox count		 
    					int Vendorcheck_count=0;
    					int m= 0;
    					List<WebElement> allElements3 = driver.findElements(By.xpath("//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img"));//Taking Contact Count
    					for (WebElement Element : allElements3)
    					{ 
    						Vendorcheck_count = Vendorcheck_count+1; 	
    					}	
    					Log.message("Vendor check box Count  is: "+Vendorcheck_count);
    					//Getting checkbox count
    					for(m = 1;m<=Vendorcheck_count;m++)				 
    					{					 
    						String Vendor_Type=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+m+"]")).getText().toString();
    						Log.message("Vendor name is:"+Vendor_Type);
    						
    						if(!Vendor_Type.equals(Before_Vendor))
    						{	
    							result5=true;							
    							break;
    						}
    					}
    					if((result5==true))
    					{	
    						SkySiteUtils.waitTill(5000);						
    						String Vendor_Name=driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText().toString();
    						Log.message("Vendor Name is:"+Vendor_Name);    					
    						driver.findElement(By.xpath("(//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
    						Log.message("Vendor name Selection Successfully!!!!");	
    						SkySiteUtils.waitTill(10000);
    						btnSaveNClose.click(); 
    						Log.message("save&close button has been clicked");    						
    						SkySiteUtils.waitTill(5000);
    						driver.switchTo().defaultContent();
    						driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    						Log.message("Switch to SetBatchAttribute Frame");
    						SkySiteUtils.waitTill(3000);				
    						btnSaveNClose.click();
    						Log.message("save button has been clicked");
    						SkySiteUtils.waitTill(10000);	
    						driver.switchTo().defaultContent();
    						driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    						Log.message("Switch to myFrame");
    						SkySiteUtils.waitTill(3000);												
    						btnpalyicon.click();
    						Log.message("paly icon button has been clicked");
    						SkySiteUtils.waitTill(10000);	   									
    						int passcount=0;
    						int failcount=0;
    						int Document_Count=Total_Count;
    						for(int n=1;n<=Document_Count;n++)
    						{
    							String After_DocumentName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[8])["+n+"]")).getText();
    							Log.message("After Documentt Name is:"+After_DocumentName);							
    							String After_Invoice=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+n+"]")).getText();
    							Log.message("After Invoice is:"+After_Invoice);						
    							String After_Project_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+n+"]")).getText();
    							Log.message("After Project Num  is:"+After_Project_Num);							
    							String After_Project_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+n+"]")).getText();
    							Log.message("After Project Name is:"+After_Project_Name);							
    							String After_Approved=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+n+"]")).getText();
    							Log.message("After Approved is:"+After_Approved);						
    							String After_Amount=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[13])["+n+"]")).getText();
    							Log.message("After Amount is:"+After_Amount);						
    							String After_Date=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[14])["+n+"]")).getText();
    							Log.message("After Date is:"+After_Date);							
    							String After_Cheque_Number=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[15])["+n+"]")).getText();
    							Log.message("After Cheque Number is:"+After_Cheque_Number);							
    							String After_Vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+n+"]")).getText();
    							Log.message("After Vendor is:"+After_Vendor);  
    							SkySiteUtils.waitTill(5000);
    							if((Invoice.equals(After_Invoice))||(Temp_Invoice.equals(After_Invoice)))
    							{
    								if((ProjectName.equals(After_Project_Name))||(Temp_ProjectName.equals(After_Project_Name)))
    								{						
    									if((Amount.equals(After_Amount))||(Temp_Amount.equals(After_Amount)))
    									{
    										if((Cheque_Numb.equals(After_Cheque_Number))||(Temp_Cheque_Numb.equals(After_Cheque_Number)))
    										{
    											if(Document_Type.equals(After_DocumentName)&&(Project_Numb.equals(After_Project_Num))
    													&&(Approved_Name.equals(After_Approved))&&(After_Date.equals(today))
    													&&(Vendor_Name.equals(After_Vendor)))
    											{
    												passcount=passcount+1;
    												Log.message("Pass Count is:"+passcount);
    											}
    											else
    											{
    												failcount=failcount+1;
    												Log.message("Fail count is:"+failcount);
    											}
    										}
    									}
    								}
    							}
    						}			
    						if(passcount==Document_Count)
    						{
    							result6=true;
    							Log.message("Modify Attribute Validation Successfully!!!!!");                             
    						}		              
    						else
    						{      
    							result6=false;
    							Log.message("Modify Attribute Validation Failed!!!!!");             
    						}	
    					}
    					else
    					{
    						result5=false;
    						Log.message("Vendor name Selection Failed!!!!");							
    					}				
    				}
    				else
    				{
    					result4=false;
    					Log.message("Boolean Type Selection Failed!!!!");	
    				}
    			}
    			else
    			{
    				result3=false;
    				Log.message("Project Number Selection Failed!!!!");
    			}
    		}			
    		else
    		{
    			result2=false;			
    			Log.message("Document Type Selection Failed!!!!");	
    		}
    	}
    	else
    	{
    		result1=false;
    		Log.message("Page navigation Failed!!!!!!");				
    	}
    	if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true)&&(result5==true)&&(result6==true))
    	{	    		
    		Log.message("Navigate Modifyed All Attributes validation Successfully!!!!");
    		return true;
    	}
    	else
    	{    		
    		Log.message("Navigate Modifyed All Attributes validation Failed!!!!");
    		return false;
    	}
    }
    
    @FindBy(xpath = "(//i[@class='icon icon-move-folder'])[3]")
     WebElement btniconmovefolder;
    
    @FindBy(xpath = "//*[@id='popup']/div/div/div[3]/div[2]/input[1]")
    WebElement btnOK;
    
    /** 
     * Method written for MoveFiles Within Collection Modify Attributes.
     * Scripted By: Sekhar    
     * @throws AWTException 
     */  
    public boolean MoveFiles_Within_Collection_Modify_Attributes(String FolderName,String FolderName1) 
    {
    
    	boolean result1=false;
    	boolean result2=false;
    	boolean result3=false;
    	boolean result4=false;
    	   	
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
    	int Folder_count=0;
    	int i= 0;
    	List<WebElement> allElements = driver.findElements(By.xpath("//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;']"));//Taking Contact Count
    	for (WebElement Element : allElements)
    	{ 
    		Folder_count = Folder_count+1; 	
    	}	
    	Log.message("Folder Count  is: "+Folder_count);
    	for(i = 2;i<=Folder_count;i++)				 
    	{					 
    		String Folder_Name=driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])["+i+"]")).getText().toString();
    		Log.message("Folder name is:"+Folder_Name);	
    		if(Folder_Name.equals(FolderName))
    		{	
    			result1=true;							
    			break;
    		}
    	}
    	if((result1==true))
    	{	
    		SkySiteUtils.waitTill(2000);						
    		driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])["+i+"]")).click();
    		SkySiteUtils.waitTill(5000);	
    		Log.message("Exp Folder Selected Successfully!!!!");
    		
    		chkAllProjectdocs.click();
    		Log.message("all checkbox button has been clicked");
    		SkySiteUtils.waitTill(5000);
    		btnmore1.click();
    		Log.message("more button has been clicked");
    		SkySiteUtils.waitTill(3000);
    		btniconmovefolder.click();
    		Log.message("move files button has been clicked");
    		SkySiteUtils.waitTill(3000);    					 
    		int Pop_Folder_count=0;
    		int j= 0;
    		List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"));//Taking Contact Count
    		for (WebElement Element : allElements1)
    		{ 
    			Pop_Folder_count = Pop_Folder_count+1; 	
    		}	
    		Log.message("pop up folder Count  is: "+Pop_Folder_count);    		
    		for(j = 1;j<=Pop_Folder_count;j++)				 
    		{					 
    			String Pop_Folder_Name=driver.findElement(By.xpath("(//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+j+"]")).getText().toString();
    			Log.message("Pop Folder name is:"+Pop_Folder_Name);	
    			if(Pop_Folder_Name.equals(FolderName1))
    			{	
    				result2=true;							
    				break;
    			}
    		}
    		if((result2==true))
    		{	
    			SkySiteUtils.waitTill(2000);					
    			driver.findElement(By.xpath("(//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+j+"]")).click();
    			Log.message("Exp Pop up Folder Selected Successfully!!!!!");
    			SkySiteUtils.waitTill(5000);
    			btnOK.click();
    			Log.message("Ok button has been clicked");
    			SkySiteUtils.waitTill(15000);    			   				
    			int After_Folder_count=0;
    			int k= 0;
    			List<WebElement> allElements2 = driver.findElements(By.xpath("//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;']"));//Taking Contact Count
    			for (WebElement Element : allElements2)
    			{ 
    				After_Folder_count = After_Folder_count+1; 	
    			}	
    			Log.message("After Folder Count  is: "+After_Folder_count);
    			//Getting pop up folder count
    			for(k = 2;k<=After_Folder_count;k++)				 
    			{					 
    				String After_Folder_Name=driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])["+k+"]")).getText().toString();
    				Log.message("Pop Folder name is:"+After_Folder_Name);	
    				if(After_Folder_Name.equals(FolderName1))
    				{	
    					result3=true;							
    					break;
    				}
    			}
    			if((result3==true))
    			{	
    				SkySiteUtils.waitTill(2000);					
    				driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])["+k+"]")).click();
    				SkySiteUtils.waitTill(5000);	
    				Log.message("After Folder Selected Successfully!!!!!");											
    				if((driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]"))).isDisplayed())
    				{
    					result4=true;
    					Log.message("Files in Exp Floder Move Successed!!!!");    					
    				}
    				else
    				{
    					result4=false;
    					Log.message("Files in Exp Floder Move Failed!!!!");
    				}
    			}
    			else
    			{
    				result3=false;					
    				Log.message("After Folder Selected Successfully!!!!!");	
    			}    				
    		}
    		else
    		{
    			result2=false;
    			Log.message("Exp Pop up Folder Selected Failed!!!!!");
    		}
    	}
    	else
    	{
    		result1=false;
    		Log.message("Exp Folder Selected Failed!!!!!");		
    	}		
    	if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true))
    	{    			
    		Log.message("MoveFiles Within Collection in File level Successfully");
    		return true;
    	}
    	else
    	{    			
    		Log.message("MoveFiles Within Collection  in File level Failed");
    		return false;
    	}
    }
    
    @FindBy(xpath = ".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[3]/td[2]")
    WebElement btnDocument;
    
    @FindBy(xpath = "//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[3]/td[1]/img")
    WebElement btnradio;
    
    @FindBy(xpath = "//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[3]/td[2]")
    WebElement txtprojnum;  
    
    @FindBy(xpath = "//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[3]/td[1]/img")
    WebElement projenumradio;   
    		
    @FindBy(xpath = "(//input[@type='radio'])[1]")
    WebElement btnyesradio;    
 
    @FindBy(xpath = "//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[3]/td[2]")
    WebElement txtvendorna;    
  
    @FindBy(xpath = " //*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[3]/td[1]/img")
    WebElement btnvendorna;
    
    /** 
     * Method written for Modify All Attribute Move Folder.
     * Scripted By: Sekhar    
     * @throws AWTException 
     */ 
    public boolean Modify_All_Attribute_Move_Folder(String Invoice,String ProjectName,String Amount,String Cheque_Numb) throws Exception 
    {
    	
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(5000);		
    	String Pagination_Count=PaginationBelow.getText();		
    	Log.message("Page count is: "+Pagination_Count);
    	String[] y= Pagination_Count.split(" ");
    	String AvlPageCount=y[1];
    	int Total_Count=Integer.parseInt(AvlPageCount);
    	Log.message("Total Available File Page count is: "+Total_Count);		
    	SkySiteUtils.waitTill(5000);	    		
    	chkAllProjectdocs.click();
    	Log.message(" all check box button has been clicked");
    	SkySiteUtils.waitTill(3000);	
    	btnmore1.click();
    	Log.message("more option button has been clicked");
    	SkySiteUtils.waitTill(3000);	
    	btniconmodifyfolder.click();
    	Log.message("modify attributes button has been clicked");
    	SkySiteUtils.waitTill(3000);			
    	driver.switchTo().defaultContent();
    	driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    	Log.message("Switch to SetBatchAttribute Frame");
    	SkySiteUtils.waitTill(3000);
    	btniconplus1.click();
    	Log.message("1st Plus button has been clicked");
    	SkySiteUtils.waitTill(5000);		
    	driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
    	Log.message("Switch to CustomFieldLookup Frame");
    	SkySiteUtils.waitTill(3000);	
    	String Document_Name=btnDocument.getText();
    	Log.message("Document Name is:"+Document_Name);
    	SkySiteUtils.waitTill(3000);
    	btnradio.click();
    	Log.message("Document radio button has been clicked");
    	SkySiteUtils.waitTill(3000);	
    	driver.switchTo().defaultContent();
    	driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    	Log.message("Switch to SetBatchAttribute Frame");
    	SkySiteUtils.waitTill(3000);
    	txtInvoi.sendKeys(Invoice);
    	Log.message("Invoice has been entered");
    	SkySiteUtils.waitTill(3000);
    	btniconplus2.click();
    	Log.message("2nd plus button has been clicked");
    	SkySiteUtils.waitTill(5000);		
    	driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
    	Log.message("Switch to CustomFieldLookup Frame");
    	SkySiteUtils.waitTill(3000);	
    	String Project_Number=txtprojnum.getText().toString();
    	Log.message("project Number is:"+Project_Number);
    	SkySiteUtils.waitTill(3000);
    	projenumradio.click();
    	Log.message("project Number radio button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    	Log.message("Switch to SetBatchAttribute Frame");
    	SkySiteUtils.waitTill(3000);
    	txtProjectnam.sendKeys(ProjectName);
    	Log.message("Project name has been entered");
    	SkySiteUtils.waitTill(3000);
    	String approved=driver.findElement(By.xpath("//ul/li[5]/div/div[2]/div/div/label[1]")).getText();
    	Log.message("Approved Status is:"+approved);
    	btnyesradio.click();
    	Log.message("yes radio button has been clicked");
    	SkySiteUtils.waitTill(1000);
    	txtcurrencyInput.sendKeys(Amount);
    	Log.message("Amount has been entered");
    	SkySiteUtils.waitTill(3000);
    	dateiconbtn.click();
    	Log.message("Date icon button has been clicked");
    	SkySiteUtils.waitTill(5000);   				
    	DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy"); 
    	Date date2 = dateFormat2.parse(dateFormat2.format(new Date()));
    	String today = dateFormat2.format(date2); 	   					
    	Log.message("Exp Today date is:"+today);
    	btntoday.click();
    	Log.message("Today button has been clicked");
		SkySiteUtils.waitTill(5000);    					
		txtCheque.sendKeys(Cheque_Numb);
		Log.message("cheque num has been entered");
    	SkySiteUtils.waitTill(2000);
    	btniconplus3.click();
    	Log.message("3rd plus button has been clicked");
    	SkySiteUtils.waitTill(5000);		
    	driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
    	Log.message("Switch to CustomFieldLookup Frame");
    	SkySiteUtils.waitTill(3000);		
    	String Vendor_Name=txtvendorna.getText();
    	Log.message("Vendor name is:"+Vendor_Name);
    	SkySiteUtils.waitTill(3000);
    	btnvendorna.click();
    	Log.message("Vendor name radio button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnSaveNClose.click();
    	Log.message("save&close button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
    	Log.message("Switch to SetBatchAttribute Frame");
    	SkySiteUtils.waitTill(3000);
    	btnSaveNClose.click();
    	Log.message("save button has been clicked");
    	SkySiteUtils.waitTill(3000);	  	
    	driver.switchTo().defaultContent();
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));	
    	Log.message("Switch to myFrame");
    	int passcount=0;
    	int failcount=0;
    	int Document_Count=Total_Count;
    	for(int n=1;n<=Document_Count;n++)
    	{
    		SkySiteUtils.waitTill(5000);
    		String After_DocumentName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[8])["+n+"]")).getText();
    		Log.message("After Documentt Name is:"+After_DocumentName);	
    		Log.message(Document_Name);
    		String After_Invoice=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[9])["+n+"]")).getText();
    		Log.message("After Invoice is:"+After_Invoice);	
    		Log.message(Invoice);
    		String After_Project_Num=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[10])["+n+"]")).getText();
    		Log.message("After Project Num  is:"+After_Project_Num);	
    		Log.message(Project_Number);
    		String After_Project_Name=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[11])["+n+"]")).getText();
    		Log.message("After Project Name is:"+After_Project_Name);
    		Log.message(ProjectName);
    		String After_Approved=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[12])["+n+"]")).getText();
    		Log.message("After Approved is:"+After_Approved);
    		Log.message(approved);
    		String After_Amount=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[13])["+n+"]")).getText();
    		Log.message("After Amount is:"+After_Amount);	
    		Log.message(Amount);
    		String After_Date=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[14])["+n+"]")).getText();
    		Log.message("After Date is:"+After_Date);
    		Log.message(today);
    		String After_Cheque_Number=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[15])["+n+"]")).getText();
    		Log.message("After Cheque Number is:"+After_Cheque_Number);	
    		Log.message(Cheque_Numb);
    		String After_Vendor=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[16])["+n+"]")).getText();
    		Log.message("After Vendor is:"+After_Vendor);
    		Log.message(Vendor_Name);
    		SkySiteUtils.waitTill(3000);
    		if(Document_Name.equals(After_DocumentName)&&(Invoice.equals(After_Invoice))&&(Project_Number.equals(After_Project_Num))
    				&&(ProjectName.equals(After_Project_Name))&&(approved.equals(After_Approved))&&(Amount.contentEquals(After_Amount))
    				&&(After_Date.equals(today))&&(Cheque_Numb.equals(After_Cheque_Number))&&(Vendor_Name.equals(After_Vendor)))
    		{	
    			passcount=passcount+1;
    			Log.message("Pass Count is:"+passcount);
    		}
    		else
    		{
    			failcount=failcount+1;
    			Log.message("Fail count is:"+failcount);
    		}						
    	}			
    	if(passcount==Document_Count)
    	{
    		result1=true;
    		Log.message("Modify Attribute Validation Successfully!!!!!");                             
    	}		              
    	else
    	{      
    		result1=false;
    		Log.message("Modify Attribute Validation Failed!!!!!");             
    	}	    	
    	if((result1==true))
    	{	    			
    		Log.message("Move Files All Attributes Modified Successfully!!!!");
    		return true;
    	}
    	else
    	{    		
    		Log.message("Move Files All Attributes Modified Failed!!!!");
    		return false;
    	}
    }
    
    
}






    	
  

	
	

