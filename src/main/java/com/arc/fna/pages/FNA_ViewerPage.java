package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FNA_ViewerPage extends LoadableComponent<FNA_ViewerPage> {
	WebDriver driver;
	private boolean isPageLoaded;

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FNA_ViewerPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Method generate random markup name
	 * 
	 * @return
	 */
	public String Random_Markupname() {

		String str = "Markup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method generate second random markup name
	 * 
	 * @return
	 */
	public String Random_SecondMarkupname() {

		String str = "SecondMarkup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method generate third random markup name
	 * 
	 * @return
	 */
	public String Random_ThirdMarkupname() {

		String str = "ThirdMarkup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method to select folder and file
	 * 
	 */
	public void selectFolder_File(String Foldername) {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(4000);

		int Count_no = driver.findElements(By.xpath(
				"//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
				.size();
		Log.message("the number folder present are " + Count_no);
		SkySiteUtils.waitTill(5000);
		int k = 0;
		for (k = 1; k <= Count_no; k++) {

			String foldernamepresent = driver.findElement(By.xpath(
					"(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
							+ k + "]"))
					.getText();
			if (foldernamepresent.contains(Foldername)) {
				Log.message("Required folder is present at " + k + " position in the tree");
				driver.findElement(By.xpath(
						"(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
								+ k + "]"))
						.click();
				Log.message("Folder is selected");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
	}

	@FindBy(xpath = ".//*[@id='shapeToolMenuBtn']/i")
	@CacheLookup
	WebElement shapemenubtn;
	@FindBy(xpath = ".//*[@id='rectangleCreate']/a/i")
	@CacheLookup
	WebElement drawrectangle;
	@FindBy(xpath = ".//*[@id='lineCreate']/a/i")
	WebElement lineannotation;

	/**
	 * Method to draw square annotation in viewer Scripted by : Tarak
	 * 
	 * @return
	 * @throws AWTException
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 * Modified by Trinanjwan | 22-11-2018
	 */
	public boolean drawLineAnnotation(String markupname, String filepath1)
			throws AWTException, InterruptedException, FindFailed, IOException {
		SkySiteUtils.waitTill(5000);
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		/**
		 * driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 * Log.message("Switched to frame");
		 * driver.findElement(By.xpath(".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")).click();
		 * Log.message("Folder selected"); SkySiteUtils.waitTill(8000);
		 * driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		 * Log.message("File is selected");
		 **/
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		drawlinetool.click();
		Log.message("Linetool is clicked");
		SkySiteUtils.waitTill(4000);
		Actions act = new Actions(driver);
		act.moveToElement(lineannotation).clickAndHold(lineannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Line annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AftersavingmarkupArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("show all markup clikced");
		SkySiteUtils.waitTill(5000);
		//savebtn.click();
		//Log.message("save button is clicked");
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		
		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched pixel: " + pass + " and " + "unmatched pixel: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}
		
		/**
		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
**/
	@FindBy(xpath = ".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")
	WebElement savebtn;
	@FindBy(xpath = ".//*[@id='txtBasicAnnotationTitle']")
	WebElement markupfield;
	@FindBy(xpath = ".//*[@id='btnBasicAnnotationSave']")
	WebElement savemarkupbtn;
	@FindBy(xpath = ".//*[@id='btnLoadMarkup']/a")
	WebElement markupdropdown;
	@FindBy(xpath = ".//*[@id='btnShowAllMarkups']")
	WebElement showallmarkup;
	@FindBy(xpath = ".//*[@id='btnShowAllHyperlinks']")
	WebElement showhyperlinks;
	@FindBy(id = "ulMarkupList")
	List<WebElement> markuplist;
	@FindBy(xpath = "//*[@class='li-striped dropdown-list hyp-annot']")
	List<WebElement> hperlinklist;
	@FindBy(xpath = ".//*[@id='btnShowAllHyperlinks']")
	WebElement showallmarkuphyperlink;

	/**
	 * Method to draw square annotation
	 * 
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws IOException
	 * Modified by Trinanjwan | 22-11-2018
	 * 
	 */
	public boolean drawSquareAnnotation(String markup, String filepath1)
			throws FindFailed, InterruptedException, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(drawrectangle).clickAndHold(drawrectangle).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw rectangle button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markup);
		Log.message("Enter markup name is " + markup);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	    //Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		 File fis1=new File(PropertyReader.getProperty("tempannotation"));
		 String filepathtemp=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupSquare.png";
		String savedscreenshot2 = filepathtemp + "\\output.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		// showallmarkup.click();
		Log.message("show all markup clikced");
		SkySiteUtils.waitTill(4000);
		
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		//savebtn.click();
		//Log.message("save button is clicked");
		SkySiteUtils.waitTill(7000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}


	@FindBy(xpath = ".//*[@id='button-1']")
	WebElement yesbtn;

	/**
	 * Method to delete the markup added
	 * 
	 * @param markup
	 */
	public void deletemarkup(String markup) {

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(8000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
		Log.message("added markup deleted successfully");
		SkySiteUtils.waitTill(10000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", yesbtn);
		// yesbtn.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(10000);
	}
     @FindBy(xpath="//*[@class='icon icon-trash icon-lg']")
     WebElement icondelete;
     
     @FindBy(xpath="//*[@class='icon icon-trash icon-lg']")
     List<WebElement> icondeletelist;
     
    
	/**
	 * Method to delte markup at begining
	 * Modified By Trinanjwan | 30-November-2018
	 */
	public void markupdeletebegin() {
		

			if (markupdropdown.isDisplayed()) {
				 markupdropdown.click();
				//Log.message("markup dropdown clicked");
				int sizeoflist = icondeletelist.size();
				Log.message("size is " + sizeoflist);
				for (int i = 1; i <= sizeoflist; i++) {
					 if (markupdropdown.isDisplayed()) {
					markupdropdown.click();
					
					SkySiteUtils.waitTill(10000);

					if (driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).isDisplayed())
						//driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
					{
						//Actions act=new Actions(driver);
						//act.moveToElement(icondelete).click(icondelete).build().perform();
						icondelete.click();
						Log.message("icon is clicked");
						SkySiteUtils.waitTill(10000);
				
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("arguments[0].click();", yesbtn);
					// yesbtn.click();
					Log.message("Yes button is clicked");
					
					SkySiteUtils.waitTill(10000);
				}
				}
			}
	}
				
			 else 
			{

				Log.message("no markup is present");

			}

		}
		
				
	@FindBy(id="ulMarkupList")
	WebElement markupcontainer;
     /**Delte markup for multipage scenario
      * 
      */
	public void markupdeltemultipage() {
		{
		
			if (markupdropdown.isDisplayed()) {
				markupdropdown.click();
				Log.message("Dropdown is clicked");
				SkySiteUtils.waitTill(5000);
				int size=markupcontainer.findElements(By.xpath("//*[@class='icon icon-trash icon-lg']")).size();
				Log.message("size is " + size);
				SkySiteUtils.waitTill(4000);
				int noofmarkup=markupcontainer.findElements(By.xpath("//span[contains(text(),'Markup')]")).size();
				Log.message("No of markup is " +noofmarkup);
			int index=markuplist.indexOf(driver.findElement(By.xpath("//span[contains(text(),'Markup')]")));
			   Log.message("index is " +index);
				if (driver.findElement(By.xpath("//span[contains(text(),'Markup')]")).isDisplayed())
						//driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
					{
						Log.message("I am in loop");
						
						driver.findElement(By.xpath("//span[contains(text(),'Markup')]")).click();
						SkySiteUtils.waitTill(4000);
						if(index==-1) {
						//Actions act=new Actions(driver);
						//act.moveToElement(icondelete).click(icondelete).build().perform();
			
						driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
						}
					}
					Log.message("added markup deleted successfully");
					SkySiteUtils.waitTill(10000);
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("arguments[0].click();", yesbtn);
					// yesbtn.click();
					Log.message("Yes button is clicked");
					SkySiteUtils.waitTill(10000);
				}
			 else {

				Log.message("no markup is present");

			}

		}
	}
	/**
	 * Method to delete the markup added
	 * 
	 * @param markup
	 */
	public void deleteSecondmarkup(String markup) {
		SkySiteUtils.waitTill(14000);
		WebElement seconddelete1 = driver.findElement(By.xpath("//*[@title='Delete']"));
		Actions act = new Actions(driver);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(8000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				act.moveToElement(seconddelete1).click().build().perform();
		Log.message("added markup deleted successfully");
		SkySiteUtils.waitTill(10000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", yesbtn);
		// yesbtn.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(10000);

	}

	@FindBy(xpath = ".//*[@id='textToolMenuBtn']/i")
	WebElement tootextbtn;
	@FindBy(xpath = ".//*[@id='textCreate']/a/i")
	WebElement textcreatebtn;
	@FindBy(xpath = ".//*[@id='editSheetViewerText']")
	WebElement textfield;
	@FindBy(xpath = ".//*[@id='editTextAnnotation']/div/div[3]/button[2]")
	WebElement okbtn;

	/**
	 * Method to draw Text Annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws AWTException 
	 * Modified by Trinanjwan | 22-11-2018
	 * 
	 */
	public boolean drawTextAnnotation(String markupname, String filepath1) throws FindFailed, IOException, AWTException {
	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegi2();
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textcreatebtn).clickAndHold(textcreatebtn).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw text tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);
		
		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);
		textfield.sendKeys("drawing");
		Log.message("text has been entered");
		Select sc = new Select(driver.findElement(By.xpath(".//*[@id='selectFontSize']")));
		sc.selectByValue("36");
		Log.message("36 font size is selected");
		okbtn.click();
		Log.message("Ok button is clicked");
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkuptext.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkuptext.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(6000);
		
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		//savebtn.click();
		//Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='lineToolMenuBtn']/i")
	WebElement drawlinetool;
	@FindBy(xpath = ".//*[@id='penCreate']/a/i")
	WebElement freehandpen;

	/**
	 * Method to draw free hand annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws AWTException 
	 * Modified by Trinanjwan | 22-11-2018
	 */
	public boolean drawFreeHandAnnotation(String markupname, String filepath1) throws FindFailed, IOException, AWTException {
	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegi2();
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(freehandpen).clickAndHold(freehandpen).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("free hand  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("Screenshotpath"));
		// String filepath1=fis1.getAbsolutePath().toString();
	
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkupfree.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkupfree.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='arrowCreate']/a/i")
	WebElement arrowannotation;

	/**
	 * Method to draw arrow annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws AWTException 
	 * Modified by Trinanjwan | 22-11-2018
	 */
	public boolean drawArrowAnnotation(String markupname, String filepath1) throws IOException, FindFailed, AWTException {
	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegi2();
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(arrowannotation).clickAndHold(arrowannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("arrow annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(15000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='ellipseCreate']/a/i")
	WebElement drawcircleannotation;

	/**
	 * Method to draw Circle Annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws AWTException 
	 * Modified by Trinanjwan | 22-11-2018
	 */

	public boolean drawCircleAnnotation(String markupname, String filepath1) throws FindFailed, IOException, AWTException {

		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegi2();
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(drawcircleannotation).clickAndHold(drawcircleannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw circle button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupCircle.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSCircle.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='cloudCreate']/a/i")
	WebElement cloudannotation;

	/**
	 * Method to draw cloud annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws AWTException 
	 * Modified by Trinanjwan | 22-11-2018
	 */
	public boolean drawCloudAnnotation(String markupname, String filepath1) throws IOException, FindFailed, AWTException {


		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegi2();
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(cloudannotation).clickAndHold(cloudannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw cloud button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();

		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupCloud.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSCloud.png";
		
		
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
//		savebtn.click();
//		Log.message("save button is clicked");
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(css = ".icon.icon-lg.icon-callout2")
	WebElement textCallout;
	@FindBy(xpath = ".//*[@id='Textarea1']")
	WebElement textarea;
	@FindBy(css = ".btn.btn-primary.btn-sm.saveLayer")
	WebElement oksavebutton;

	/**
	 * Method to draw text call out annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws AWTException 
	 * Modified by Trinanjwan | 22-11-2018
	 */
	public boolean drawTextCallOutAnnotation(String markupname, String filepath1) throws IOException, FindFailed, AWTException {
		
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegi2();
		SkySiteUtils.waitForElement(driver, tootextbtn, 50);
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textCallout).clickAndHold(textCallout).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw text callout tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);
		textarea.sendKeys("Test");
		Log.message("text has been entered");
		Select sc = new Select(driver.findElement(By.xpath(".//*[@id='selectCalloutFontSize']")));
		sc.selectByValue("10");
		Log.message("10 font size is selected");
		oksavebutton.click();

		Log.message("Ok button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupTextCallout.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerTextCallout.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		//savebtn.click();
		//Log.message("save button is clicked");
		SkySiteUtils.waitTill(5000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	

	@FindBy(css = ".icon.icon-lg.icon-note")
	WebElement textnoteicon;
	@FindBy(css = "#editNoteText")
	WebElement textnotearea;

	/**
	 * Method to draw text note annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * Modified by Trinanjwan | 22-11-2018 
	 */
	public boolean drawTextNoteAnnotation(String markupname, String filepath1)
			throws FindFailed, IOException, InterruptedException {
	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitForElement(driver, tootextbtn, 50);
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textnoteicon).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(textnoteicon).build().perform();
		Log.message("draw text note tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");
		String note = "My Note";
		SkySiteUtils.waitTill(4000);
		textnotearea.sendKeys(note);
		Log.message("Note has been entered");
		SkySiteUtils.waitForElement(driver, oksavebutton, 40);
		oksavebutton.click();

		Log.message("Ok button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);

		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupTextCallout.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerTextCallout.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		//savebtn.click();
		//Log.message("save button is clicked");
		SkySiteUtils.waitTill(5000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	@FindBy(xpath = "//*[@data-original-title='Draw rectangle highlighter']")
	WebElement hightlightericon;

	/**
	 * Method to draw highlighter annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawHighLighterAnnotation(String markupname, String filepath1) throws IOException, FindFailed {
		/**
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span"))
				.click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		
**/		
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		
		
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(10000);

		hightlightericon.click();
		Log.message("draw highlighter button is clicked");
		SkySiteUtils.waitTill(5000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3= new Pattern(Screenshot3);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		//driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		//Log.message("save button is clicked again");
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkuphighlighter.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerHightlighter.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(6000);
		
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}


	/**
	 * Method to delete screenshot
	 * 
	 * @param Screenshotpath
	 * @return
	 */
	public void delteScreenShot(String Screenshotpath) {
		Log.message("Searching screenshot downloaded");
		File file = new File(Screenshotpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(".png")) {
					Myfile.delete();
					Log.message("File is deleted successfully");
				}
			}
		}

	}

	@FindBy(xpath = ".//*[@id='btnLoadMarkupHL']/a")
	WebElement hyperlinkdropdown;
	@FindBy(xpath = "//*[@class='btn btn-primary btn-sm done saveLayerL']")
	WebElement okbutton;
	@FindBy(xpath = "//*[@class='btn btn-primary btn-sm saveLayerF']")
	WebElement okbuttonexternalhyperlink;
	@FindBy(xpath = ".//*[@id='linkRectCreate']")
	WebElement recthyperlink;
	@FindBy(xpath = ".//*[@id='linkToolMenuBtn']")
	WebElement hyperlinktool;

	/**
	 * Method to draw and save square hyperlink
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public void drawAndSaveSquareHyperLink(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='SameFolderDiffFile']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[13]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
	/*	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	@FindBy(xpath = ".//*[@id='btnCopyLink']")
	WebElement copylink;

	/**
	 * Method to verify working of square hyperlink for same folder diff file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperLinkSameFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(9000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(10000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");

		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFile() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}
	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileForHyperLink() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='1 mb.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}
     
	
	/**
	 * Method to click default file for dwf file
	 * 
	 */
	public void clickDefaultFileForHyperLinkDWFFile() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='dwf123.dwf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}

	/**
	 * Method to draw and save square hyperlink for diff folder and diff file
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public void drawAndSaveSquareHyperLinkForDiffFolderDifffile(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(".//*[text()='FolderViewerTesting_DONotDelete']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(3000);
		//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
	//	Log.message("Page1 is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

	/*	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * Method to verify working of square hyperlink for diff folder diff file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * 
	 */

	public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(4000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(5000);
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to draw and save square hyperlink for diff folder and same file
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public void drawAndSaveSquareHyperLinkForDiffFolderSamefileDiffVersion(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(".//*[text()='ViewerTesting2']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[11]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(3000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		// driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		// Log.message("Page1 is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
	/*	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * Method to verify functionality for square hyperlink with diff folder and same
	 * file and different version
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 */
	public boolean VerifyfunctionalitySquareHyperLinkdiffFolderSameFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[text()='(R-2) 1_Document.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-2) 1_Document.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	@FindBy(xpath = ".//*[@id='linkEllipseCreate']/a/i")
	WebElement circlehyperlinktool;

	/**
	 * Method to draw circle hyperlink and xreate hyper link within same folder diff
	 * file.
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 * 
	 */
	public void drawCircleHyperLink(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span"))
				.click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(circlehyperlinktool).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(circlehyperlinktool).build().perform();
		Log.message("Circle Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointCircleHyperlink.png";
		String Screenshot2 = filepath + "\\EndpointCircleHyperlink.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
	/*	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * Method to verify circle hyperlink working properly
	 * 
	 */
	public boolean VerifyfunctionalityCircleHyperLinkSameFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\temphyperlinkcircle.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file for circle hyperlink
	 * 
	 */
	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileForCircleHyperLink() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='1 mb.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");

	}

	@FindBy(xpath = ".//*[@id='linkCloudCreate']/a/i")
	WebElement cloudtool;

	/**
	 * Method to draw Cloud HyperLink
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public void drawCloudHyperLink(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Viewer_Testing_CircleHyperLink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(6000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(cloudtool).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(cloudtool).build().perform();
		Log.message("Cloud Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointCircleHyperlink.png";
		String Screenshot2 = filepath + "\\EndpointCircleHyperlink.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
	/*	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * Method to verify Cloud hyperlink working properly
	 * 
	 */
	public boolean VerifyfunctionalityCloudHyperLinkSameFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\temphyperlinkcloud.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(5000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * MEthod to draw square hyperlink for parent child folder for different file
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws IOException
	 * @throws FindFailed
	 */
	public void drawAndSaveSquareHyperLinkParentChildFolder(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ParentFolder']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[3]/div/a/div[3]")).click();
		Log.message(" Parent Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[1]/li/div/a/div[3]")).click();
		Log.message("Child Folder is selected");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
	/*	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * MEthod to verify functionality for square hyperlink when linking parent child
	 * folder with diff file
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 */
	public boolean VerifyfunctionalitySquareHyperLinkParentCHildFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(10000);

		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");

		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * MEthod to draw square hyperlink for child parent folder for different file
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws IOException
	 * @throws FindFailed
	 */
	public void drawAndSaveSquareHyperLinkChildParentFolder(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td[2]/table/tbody/tr/td[1]/div"))
				.click();
		Log.message("Expand parent folder");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[text()='ChildFolder']")).click();
		Log.message("Child folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPoint_HYperLink_!_ENAMEFILE.png";
		String Screenshot2 = filepath + "\\EndPoint_HyperLink_EnameFile.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[6]/div/a/div[3]")).click();
		Log.message(" Parent Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[1]/li/div/a/div[3]")).click();
		// Log.message("Child Folder is selected");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[2]/li/div/div[2]/h4/a")).click();
		Log.message("File of parent folder is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
	/*	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * MEthod to verify working of square hyperlink for diff folder(Child--->Parent)
	 * & diff file
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperLinkdiffFolderChildParentDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempscreenshotforENAMEFILE.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[text()='(R-1) 1_Document.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_Document.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileForEname() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[text()='(R-1) 1_EName.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}

	/**
	 * Method to draw and save hyperlink for multipage file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 *  Modified By Trinanjwan | 26-November-2018
	 */
	public boolean drawAndSaveHyperLinkMultiPage(String markupname, String filepath1) throws FindFailed, IOException {

		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='SearchForKeyword2']")).click();
		Log.message("Multi page folder");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPoint_MultiPageFile.png";
		String Screenshot2 = filepath + "\\EndPoint_MultiPageFile.png";
		String Screenshot3 = filepath + "\\Hyperlinkclicknew.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3 = new Pattern(Screenshot3);
		
		

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[9]/div/a/div[3]")).click();
		Log.message(" Mutli Page File Folder is selected again for linking");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		Log.message("File  is selected");
		driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[11]/a")).click();
		Log.message("11th page is selected");
		// driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		// Log.message("1st page folder is clicked");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	
	
	//span[text()='(R-1) sum123.pdf']
			
			
	/**
	 * MEthod to verify functionality of hyperlink for multipage file
	 * @throws InterruptedException 
	 *  Modified By Trinanjwan | 26-November-2018
	 */
	
	
	@FindBy(xpath = "//span[text()='(R-1) sum123.pdf']")
	WebElement filelink;
	
	
	public boolean VerifyfunctionalitySquareHyperLinkMultiPage(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException, InterruptedException {
		driver.switchTo().defaultContent();
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\temphyperlinkmultipage.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(10000);
		String actual=filelink.getText();
		String expected="(R-1) sum123.pdf";
		Log.message("Actual file is:"+actual);
		Log.message("Expected file is:"+expected);
		
		if(actual.contentEquals(expected))
			return true;
		else
			return false;
		
	}
		
//		// String
//		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
//		
//		
//		SkySiteUtils.waitTill(5000);
//		File fistemp12 = new File(PropertyReader.getProperty("tempScreenshot"));
//		String filepathtemp12 = fistemp12.getAbsolutePath().toString();
//		String savedscreenshot1 = filepathtemp12 + "\\Multipagetempfile.png";
//		String savedscreenshot2 = filepath1 + "\\result1.png";
//		SkySiteUtils.waitTill(4000);
//		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
//		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//
//		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));
//
//		Log.message("Screenshot taken of saved markup");
//		SkySiteUtils.waitTill(4000);
//
//		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
//		SkySiteUtils.waitTill(3000);
//		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
//
//		Thread.sleep(2000);
//
//		Log.message("Checking the height and width of both the images are same?");
//
//		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
//			Log.message("image height width are different");
//			return false;
//		}
//
//		int width = imgA.getWidth();
//		int height = imgA.getHeight();
//		for (int x = 0; x < width; x++) {
//			for (int y = 0; y < height; y++) {
//
//				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
//
//					Log.message("Image are different");
//					return false;
//				}
//			}
//
//		}
//
//		return true;
//	}

		

	

	@FindBy(xpath = "//*[@href='#webLink']")
	WebElement externallinltab;
	@FindBy(xpath = ".//*[@id='hyperlinkExternalLinkText']")
	WebElement hyperlinktextarea;

	/**
	 * MEthod to verify square hyperlink getting saved properly for external
	 * hyperlink
	 * 
	 */
	public void drawAndSaveHyperLinKforExternalhyperlink(String markupname, String filepath1)
			throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				"//*[text()='ExternalHyperLink']"))
				.click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		externallinltab.click();
		Log.message("External hyperlink tab is clicked");
		hyperlinktextarea.sendKeys("www.google.com");

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", okbuttonexternalhyperlink);
		// SkySiteUtils.waitForElement(driver, okbutton, 50);
		// okbuttonexternalhyperlink.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
	/*	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * MEthod to verify functionality of hyperlink for External hyperlink
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperExternalHyperlink(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(5000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// WebElement
		// imagelink=driver.findElement(By.xpath("//*[text()='http://www.google.com']"));linkFrameDivWrap
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", imagelink);
		driver.findElement(By.id("linkFrameDivWrap")).click();
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		String currenturl = driver.getCurrentUrl();
		Log.message("URl is " + currenturl);
		driver.switchTo().window(MainWindow);
		if (currenturl.contains("www.google.com")) {
			Log.message("Linked url is opened in new tab");
			return true;
		} else {
			Log.message("Linked url is not opened in new tab");
			return false;
		}

	}

	/**
	 * MEthod to draw and verify hyperlink with folder level
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public void drawAndSaveHyperlinkWithFolderLevel(String markupname, String filepath1)
			throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Viewertesting']")).click();
		Log.message("Folder selected");
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.PNG";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/a/div[3]")).click();
		Log.message(" Folder is selected");
		SkySiteUtils.waitTill(4000);

		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();

		SkySiteUtils.waitForElement(driver, okbuttonexternalhyperlink, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", okbuttonexternalhyperlink);
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
	/*	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * verify hpyerlink functionality for folder level
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 * @throws AWTException
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperWithFolderLevel(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException, AWTException {
		boolean flag = false;
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);

		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		String windowHandle = driver.getWindowHandle();
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(9000);

		WebElement file = driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]"));
		String filename = file.getText();
		Log.message("File name is " + filename);

		if (driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed()
				&& filename.equals("1_EName.pdf")) {
			Log.message("Folder hyperlink works");
			return true;

		}

		else {
			Log.message("Folder hyperlink does not work");
			return false;
		}

	}
	/**
	 * verify hpyerlink functionality for sub  folder level
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 * @throws AWTException
	 */
	public boolean VerifyfunctionalitySquareHyperWithSubFolderLevel(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException, AWTException {
		boolean flag = false;
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);

		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		String windowHandle = driver.getWindowHandle();
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(9000);

		WebElement file = driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]"));
		String filename = file.getText();
		Log.message("File name is " + filename);

		if (driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed()
				&& filename.equals("Contacts.csv")) {
			Log.message(" Sub Folder hyperlink works");
			return true;

		}

		else {
			Log.message(" Sub Folder hyperlink does not work");
			return false;
		}

	}

	/**
	 * MEthod to switch to viewer tab
	 * 
	 * @throws AWTException
	 * 
	 */
	public void switchToViewerTab() throws AWTException {
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

	}

	/**
	 * MEthod to draw and verify hyperlink with folder level
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawAndSaveHyperlinkWithSubFolderLevel(String markupname, String filepath1)
			throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='SubFolderHyperlink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/a/div[3]")).click();
		Log.message("Parent Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[1]/li/div/a/div[3]")).click();
		Log.message("Sub Folder is selected");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();

		SkySiteUtils.waitForElement(driver, okbuttonexternalhyperlink, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", okbuttonexternalhyperlink);
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='rotateMenuBtn']/i")
	WebElement rotatemenu;
	@FindBy(xpath = ".//*[@id='btnRotateAntiClockWise']")
	WebElement Rotateanticlockwisemenu;
	@FindBy(xpath = ".//*[@id='btnSaveRotation']")
	WebElement saveafterrotate;

	/**
	 * Method to verify 90 rotation getting saved properly
	 * 
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public boolean saveRotationofFile(String markupname, String filepath1,String filepathtemp12) throws IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Rotate90']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

		
		SkySiteUtils.waitTill(4000);
		rotatemenu.click();
		Log.message("Rotate tool is clicked");

		SkySiteUtils.waitTill(2000);
		WebElement aniclockicon = driver.findElement(By.id("btnRotateClockWise"));
		SkySiteUtils.waitForElement(driver, Rotateanticlockwisemenu, 40);

		Actions act = new Actions(driver);
		act.moveToElement(aniclockicon).click().build().perform();
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitTill(2000);
		saveafterrotate.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(20000);

		

	//	File fistemp12 = new File(PropertyReader.getProperty("runtimescreenshot"));
	//	String filepathtemp12 = fistemp12.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\90_rotation_pic.png";
	      String savedscreenshot2 = filepathtemp12 + "\\runtimescreenshotof90degree.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
       
        Log.message("Image is compared");
		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	/**Method to change the rotated file to default
	 * 
	 */
	public void changeFileToDefault() {
		SkySiteUtils.waitTill(4000);
		WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

		
		SkySiteUtils.waitTill(4000);
		rotatemenu.click();
		Log.message("Rotate tool is clicked");

		SkySiteUtils.waitTill(2000);
		WebElement aniclockicon = driver.findElement(By.xpath(".//*[@id='btnRotateAntiClockWise']"));
		SkySiteUtils.waitForElement(driver, Rotateanticlockwisemenu, 40);

		Actions act = new Actions(driver);
		act.moveToElement(aniclockicon).click().build().perform();
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitTill(2000);
		saveafterrotate.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(20000);
		Log.message("File is rotated and changed to default state successfully");

	}

	@FindBy(xpath = ".//*[@id='measureToolMenuBtn']/i")
	WebElement calibrator;
	@FindBy(xpath = ".//*[@id='calibrationCreate']/a/i")
	WebElement Calibratorruler;
	@FindBy(xpath = ".//*[@id='measurementSquareCreate']/a/i")
	WebElement squarecreation;
	@FindBy(xpath = "//a[@class='leaflet-control-nav-home']")
	WebElement homeBtnviewer;
	
	
	

	/**
	 * Method to draw and save calibrator
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * Modified by Trinanjwan | 28-11-2018
	 * 
	 */
	public boolean draWaANDSaveCalibrator(String markupname, String filepath1,String filepathforscreenshot)
			throws FindFailed, IOException, InterruptedException {
	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ZCalibratorSaving']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointCalibrator.PNG";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnew.png";
		
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p6 = new Pattern(Screenshot3);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(5000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
//		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
//		Log.message("save button is clicked again");
//		SkySiteUtils.waitTill(7000);
		
		s.click(p6);
		Log.message("Clicked on viewer");
		SkySiteUtils.waitTill(5000);
		homeBtnviewer.click();
		Log.message("Clicked on the home button");	
		SkySiteUtils.waitTill(5000);
		s.click(p6);
		Log.message("Clicked on viewer");
		SkySiteUtils.waitTill(2000);
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkupcal1.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkupcal1.png";
		SkySiteUtils.waitTill(10000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		
		s.click(p6);
		Log.message("Clicked on the viewer to dismiss the dropdown");
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(6000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}
		
/**
		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}
	**/	
	
	
	/*	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	

	/**
	 * Method to delete all the files from download path
	 * 
	 */
	public boolean Delete_Files_From_Folder(String Folder_Path) {
		try {
			SkySiteUtils.waitTill(5000);
			Log.message("Cleaning download folder!!! ");
			File file = new File(Folder_Path);
			String[] myFiles;
			if (file.isDirectory()) {
				myFiles = file.list();
				for (int i = 0; i < myFiles.length; i++) {
					File myFile = new File(file, myFiles[i]);
					myFile.delete();
					SkySiteUtils.waitTill(5000);
				}
				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
		} // end try
		catch (Exception e) {
			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		}
		return false;
	}

	/**
	 * Method to check if file is downloaded Scripted By : Tarak
	 * 
	 */
	public boolean isFileDownloaded(String downloadfolderpath, String Downloadfilename) {
		Log.message("Searching the downloaded file");
		String Filename = Downloadfilename;
		Log.message("Downloaded file name is " + Filename);
		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		SkySiteUtils.waitTill(9000);
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(Filename)) {
					Log.message("File is found");
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * Method to compare two pdf
	 * 
	 */
	public boolean comparetwopdfFile(String downloadpath) throws IOException {
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		File test_file_1 = new File(filepathtemp + "\\1_Document.pdf");
		File test_file_2 = new File(downloadpath + "\\1_Document.pdf");

		boolean compareResult = FileUtils.contentEquals(test_file_1, test_file_2);
		Log.message("Are the files same? " + compareResult);
		if (compareResult == true)
		{
			Log.message("Files are same");
			return true;
			
		}
		else
		{
			Log.message("Files are not same");
			return false;
		}
	}

	@FindBy(xpath = ".//*[@id='workspaceBar']/div[1]/div/ul[2]/li[7]/a")
	WebElement downloadmenu;
	@FindBy(xpath = ".//*[@id='aDownload']/a/span")
	WebElement downloadbutton;
	@FindBy(xpath = ".//*[@id='markupdownload']")
	WebElement showdrawingoption;
    @FindBy(xpath=".//*[@id='allmarkupdownload']")
    WebElement allmarkupoption;
	/**
	 * Method to download drawn calibrator
	 * 
	 * @throws IOException
	 * 
	 */
	public boolean downLoadCalibrator(String downloadpath, String filename) throws IOException {
		this.Delete_Files_From_Folder(downloadpath);
		SkySiteUtils.waitForElement(driver, downloadmenu, 50);
		downloadmenu.click();
		Log.message("Download menu is clicked");
		SkySiteUtils.waitForElement(driver, downloadbutton, 40);
		downloadbutton.click();
		Log.message("download button is clicked");
		SkySiteUtils.waitForElement(driver, showdrawingoption, 40);
		allmarkupoption.click();
		//showdrawingoption.click();
		Log.message("Show drawing option is clicked");
		SkySiteUtils.waitTill(20000);
		if (this.isFileDownloaded(downloadpath, filename) == true && this.comparetwopdfFile(downloadpath))
			return true;
		else
			return false;

	}

	/**
	 * Method to draw and save calibrator and ruler seperatly
	 * 
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 * Modified by Trinanjwan | 28-11-2018
	 */
	public boolean saveCalibratorAndRulerSeperatly(String markupname1, String markupname2, String filepath1)
			throws InterruptedException, FindFailed, IOException {

		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='CalibratorSeperateMarkup']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		

		this.markupdeletebegin();
		
	
		SkySiteUtils.waitTill(10000);
		this.markupdeletebegin();
		SkySiteUtils.waitTill(10000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a/i")).click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitForElement(driver, markupfield, 50);
		markupfield.sendKeys(markupname1);
		Log.message("Enter markup name is " + markupname1);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		String Screenshot6 = filepath + "\\Clickscreenshotnew.png";

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);
		
		Pattern p6 = new Pattern(Screenshot6);


		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		
		s.click(p6);
		Log.message("Safety click on the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(5000);
		homeBtnviewer.click();
		Log.message("Clicked on home button to keep the viewer in accurate position");
		SkySiteUtils.waitTill(5000);
		s.click(p6);
		Log.message("Safety click on the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(5000);

		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkupcal2.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkupcal2.png";
		SkySiteUtils.waitTill(10000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
		Log.message("Screenshot taken of saved markup");
		
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname1))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname1 + "']")).click();
		Log.message("First mark up is loaded on the viewer");
		
		SkySiteUtils.waitTill(5000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname2))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();
		
		Log.message("Second mark up is loaded on the viewer");	
		SkySiteUtils.waitTill(10000);
		showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(5000);
		s.click(p6);
		Log.message("Safety click on the viewer to dismiss the dropdown");
		
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}
		
		
		
	/**	
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\AfterSavingSquareHyperLink12.png";
		String savedscreenshot2 = filepath1 + "\\testoutputpic1.png";

		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
			
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
**/
		
	
	
	/**
	 * Method to draw multiple ruler on a single document.
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws AWTException 
	 * Modified by Trinanjwan | 30-11-2018
	 */
	
	
	@FindBy(xpath = "//button[@id='btnShowAllMarkups']")
	WebElement viewAllMarkUplist;
	
	
	public boolean drawMultipleRulerOnsinglePage(String markupname, String markupname2, String filepath1)
			throws IOException, FindFailed, InterruptedException, AWTException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup

		this.markupdeletebegin();
		
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("9");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();
		
		

		Log.message("save markup clicked and image drawn disappears");
//		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
//		Log.message("save button is clicked again");
		
		SkySiteUtils.waitTill(5000);
		String Screenshot7 = filepath + "\\Clickscreenshotnew.png";
		Pattern p7 = new Pattern(Screenshot7);
		s.click(p7);
		Log.message("Clicked on particular section of the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(5000);
		

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act12 = new Actions(driver);
		act12.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act12.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis2 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath2 = fis2.getAbsolutePath().toString();
		String Screenshotruler = filepath2 + "\\StartpointRuler.png";
		String Screenshot2ruler = filepath2 + "\\EndPointRuler.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshotruler);

		Pattern p6 = new Pattern(Screenshot2ruler);

		s2.drag(p5);

		s2.dropAt(p6);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("22");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("45");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		markupdropdown.click();
		Log.message("Clicked on the mark up dropdown list");
		SkySiteUtils.waitTill(10000);
		
		viewAllMarkUplist.click();
		Log.message("Clicked on the View All to load all the mark ups");
		s.click(p7);
		Log.message("Clicked on particular section of the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		
		
		String savedscreenshot1 = filepath1 + "\\Beforeloadingmarkupcal3.png";
		String savedscreenshot2 = filepath1 + "\\Afterloadingmarkupcal3.png";
		SkySiteUtils.waitTill(10000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
		
		SkySiteUtils.waitTill(10000);
		
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname2))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();

		Log.message("First mark up is clicked to make it disappear");
		
		SkySiteUtils.waitTill(5000);
	
		for (WebElement markupitem1 : markuplist)
			if (markupitem1.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();
		
		
		Log.message("Second mark up is clicked to make it disappear");
		
		SkySiteUtils.waitTill(5000);
		viewAllMarkUplist.click();
		SkySiteUtils.waitTill(5000);
		s.click(p7);
		Log.message("Clicked on particular section of the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");
	
		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}

		
		
	/**	
	 
		
		driver.findElement(By.xpath("btnHideAllGlobal")).click();
		Log.message("Hide all button is clicked");
		SkySiteUtils.waitTill(14000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(5000);
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(5000);
		String MainWindow2 = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);

		// driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		// Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);
		//driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		//Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\MutliRuler.png";
		String savedscreenshot2 = filepath1 + "\\Afterdrawnsnap.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	
	
**/	

	/**
	 * MEthod to verify measurement of drawn calibrator ruler can be changed
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * Modified by Trinanjwan | 28-11-2018
	 */
	public boolean changeMeasurementRuler(String markupname, String filepath1)
			throws FindFailed, IOException, InterruptedException {
		
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='FolderViewerTesting_DONotDelete']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnewfree.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);
		
		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		SkySiteUtils.waitTill(15000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\tmpRulerdefaultMeasurement.PNG";
		Screen sc1 = new Screen();
		Pattern p = new Pattern(savedscreenshot1);
		SkySiteUtils.waitTill(6000);
		sc1.click(p);
		Log.message("drawn ruler is clicked");
		SkySiteUtils.waitTill(5000);
		//driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		driver.findElement(By.xpath("//*[@for='radioMeasureUnitMetric']")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		WebElement field1 = driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']"));
		field1.clear();
		field1.sendKeys("9");
		Log.message("Length in metre is entered");
		WebElement field2 = driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']"));
		field2.clear();
		field2.sendKeys("4");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
        SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
	     SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(10000);
		

		String savedscreenshot2 = filepath1 + "\\Beforesavingmarkupcal3.png";
		String savedscreenshot3 = filepath1 + "\\Aftersavingmarkupcal3.png";
		SkySiteUtils.waitTill(10000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(8000);
	
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
//		savebtn.click();
//		Log.message("save button is clicked");
		SkySiteUtils.waitTill(6000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot3));
		Log.message("Screenshot taken after redrawing");

		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}


	//@FindBy(xpath = "//*[text()='Delete']")
	@FindBy(xpath="//button[text()='Delete']")
	WebElement deleteoption;

	
	
	/**
	 * Method to delete drawn ruler
	 * @throws FindFailed
	 * @throws IOExceptions
	 * @throws InterruptedException
	 * Modified By Trinanjwan | 29-November-2018
	 */
	public boolean deleteDrawnRuler(String filepath1) throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='FolderViewerTesting_DONotDelete']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkupcal4.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkupcal5s.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken without any markup for the first time");
		
		SkySiteUtils.waitTill(7000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		String Screenshot3 = filepath + "\\Clickscreenshotnewfree.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3 = new Pattern(Screenshot3);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		Log.message("Home button is clicked");
		SkySiteUtils.waitTill(10000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot3 = filepathtemp + "\\tmpRulerdefaultMeasurement.PNG";
		Screen sc1 = new Screen();
		Pattern p = new Pattern(savedscreenshot3);
		SkySiteUtils.waitTill(6000);
		sc1.click(p);
		SkySiteUtils.waitTill(9000);
		sc1.click(p);
		Log.message("drawn ruler is clicked now");
		SkySiteUtils.waitTill(9000);
		
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", deleteoption);

	//	deleteoption.click();
		Log.message("Delete option is clicked");
		SkySiteUtils.waitTill(6000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		Log.message("Home button is clicked");
		
		
		
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		// Log.message("Home button is clicked");
		// SkySiteUtils.waitTill(9000);
//		File fistemp2 = new File(PropertyReader.getProperty("tempScreenshot"));
//		String filepathtemp2 = fistemp2.getAbsolutePath().toString();
//		String savedscreenshot12 = filepathtemp2 + "\\outputpic1.png";
//		String savedscreenshot2 = filepath1 + "\\Deleterulercnap.png";
//		SkySiteUtils.waitTill(4000);
//		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
//		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//
//		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));
		
		SkySiteUtils.waitTill(5000);
		s.click(p3);
		Log.message("Clicked on any section of the viewer");
		SkySiteUtils.waitTill(5000);
		
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after deleting the calibration");
		
		
		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;


	}

	@FindBy(xpath = ".//*[@id='measurementCircleCreate']/a/i")
	WebElement circletool;
	
	@FindBy(xpath = "//button[text()='Set active']")
	WebElement setActivebtn;
	
	

	/**
	 * Method Verify whether unit of all the measurment calibrator is getting
	 * changed or not with respect to Active calibrator ruler
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws AWTException
	 * Modified By Trinanjwan | 03-December-2018
	 */
	public boolean MultiCalibratorTakingActiveRulerMeasurementSize(String markupname, String markupname2,
			String filepath1, String filepath3) throws IOException, FindFailed, InterruptedException, AWTException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ViewerTestingMultiCalibrator']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRectnew1.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act12 = new Actions(driver);
		act12.moveToElement(circletool).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(circletool).build().perform();
		Log.message("Circle Calibrator selected");
		String Screenshotcircle = filepath + "\\tempcirclestart1.PNG";
		String Screenshotcircleend = filepath + "\\CircleDrawEnd1.PNG";
		Screen sob = new Screen();

		Pattern pcircle = new Pattern(Screenshotcircle);

		Pattern pcircleend = new Pattern(Screenshotcircleend);

		sob.click(pcircle);

		sob.drag(pcircle);

		sob.dropAt(pcircleend);
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		
		SkySiteUtils.waitTill(5000);
		String Screenshotclick = filepath + "\\Clickscreenshotnew.png";
		
		Pattern pclick = new Pattern(Screenshotclick);
		
		s.click(pclick);
		
		Log.message("Clicked on any section of the viewer to dismiss the dropdown");

//		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
//		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(10000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act123 = new Actions(driver);
		act123.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act123.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis2 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath2 = fis2.getAbsolutePath().toString();
		String Screenshotruler = filepath2 + "\\StartpointRulernew.png";
		String Screenshot2ruler = filepath2 + "\\EndPointRulernew.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshotruler);

		Pattern p6 = new Pattern(Screenshot2ruler);

		s2.drag(p5);

		s2.dropAt(p6);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("9");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		SkySiteUtils.waitTill(10000);
		
		
		//Close the viewer and further re-open it
		

		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(5000);
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ViewerTestingMultiCalibrator']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(5000);
		String MainWindow2 = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		
		SkySiteUtils.waitTill(10000);
		
		String savedscreenshotclickactive = filepath1 + "\\ActiveCalibratorclick.png";
		Pattern pactive = new Pattern(savedscreenshotclickactive);
		
		s.click(pactive);
		Log.message("Clicked on the calibrator to make it active");
		
		SkySiteUtils.waitTill(5000);
		
		setActivebtn.click();
		Log.message("Clicked on Active button to make the calibrator active");
		SkySiteUtils.waitTill(5000);
		
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home button is clicked");
		SkySiteUtils.waitTill(5000);
		
		s.click(pclick);
		Log.message("Safety click on any section of the viewer");
		
		SkySiteUtils.waitTill(5000);
		

		String savedscreenshot1 = filepath3 + "\\beforesaveactive.png";
		String savedscreenshot2 = filepath3 + "\\Aftersaveactivepng";
		
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

/**		
		
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\Afterdrawnsnap.png";
	    String savedscreenshot2 = filepath1 + "\\multicalibrator.png";
		SkySiteUtils.waitTill(4000);
		String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
**/


		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}

	/**
	 * Method to draw square annotation on zooming
	 * 
	 * @param markup
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public boolean drawSquareAnnotationOnZooming(String markup, String filepath1)
			throws FindFailed, InterruptedException, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(drawrectangle).clickAndHold(drawrectangle).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw rectangle button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markup);
		Log.message("Enter markup name is " + markup);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupSquarezoom.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSquarezoom.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw arrow annotation on zooming
	 * 
	 */

	public boolean drawArrowAnnotationOnZooming(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();

		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(3000);
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(arrowannotation).clickAndHold(arrowannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("arrow annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw text call out annotation on zooming
	 * 
	 */
	/**
	 * Method to draw text call out annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawTextCallOutAnnotationOnZooming(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitForElement(driver, tootextbtn, 50);
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textCallout).clickAndHold(textCallout).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw text callout tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);
		textarea.sendKeys("Text Call out drawing");
		Log.message("text has been entered");
		Select sc = new Select(driver.findElement(By.xpath(".//*[@id='selectCalloutFontSize']")));
		sc.selectByValue("8");
		Log.message("8 font size is selected");
		oksavebutton.click();

		Log.message("Ok button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupTextCallout.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerTextCallout.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw free hand annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawFreeHandAnnotationOnZooming(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(freehandpen).clickAndHold(freehandpen).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("free hand  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("Screenshotpath"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupFreehand.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerfreehand.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw and save square hyperlink on zooming
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * Modified By Trinanjwan | 27-November-2018
	 */
	public boolean drawAndSaveSquareHyperLinkonzooming(String markupname, String filepath1)
			throws IOException, FindFailed {
	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup

		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\SquareHyperLinkZoomStartPoint.png";
		String Screenshot2 = filepath + "\\SquareHyperLinkZoomEndPoint.png";
		String Screenshot3 = filepath + "\\Hyperlinkclicknew1.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		
		Pattern p3 = new Pattern(Screenshot3);
		
		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[10]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		//driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		//Log.message("Page is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		// Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(10000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		s.click(p3);
		
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(10000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(10000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(10000);
//		savebtn.click();
//		Log.message("save button is clicked");
		s.click(p3);
		Log.message("Clicked on the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}


	/**
	 * Method to verify working of square hyperlink for same folder diff file on
	 * zooming
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * Modified By Trinanjwan | 27-November-2018
	 */
	
	
	@FindBy(xpath = "//img[@id='linkImgDiv']")
	WebElement hyperlinkdestimage;
	
	@FindBy(xpath = "//span[text()='(R-2) 1_Document.pdf']")
	WebElement destinationfileheader;
	
	
	
	public boolean VerifyfunctionalitySquareHyperLinkSameFolderDiffFileOnZooming(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(6000);
		
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Hyperlinkdestimageclick.png";
		Screen s = new Screen();
		Pattern p1 = new Pattern(Screenshot1);
	
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(10000);
		hyperlinkdestimage.click();
		Log.message("Clicked on the image to navigate to the destination file");
		SkySiteUtils.waitTill(10000);
		String actual=destinationfileheader.getText();
		String expected="(R-2) 1_Document.pdf";
		Log.message("Actual name is:"+actual);
		Log.message("Expected name is:"+expected);
		
		if(actual.contentEquals(expected))
				return true;
		else 
			return false;
		

		
	}
		
	
		
	/**	 
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");

		String filename = driver.findElement(By.xpath("//*[@data-original-title='sum123.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) sum123.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;
**/
	

	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileAfterZoom() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='re1(13).pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}

	/**
	 * Method to draw only multiple calibration without any reference calbrator
	 * ruler on a single document
	 * 
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 * @throws AWTException
	 * Modified By Trinanjwan | 30-November-2018
	 */
	public boolean drawMultipleCalibrationRuler(String markupname, String markupname2, String thirdmarkup,
			String filepath1) throws InterruptedException, FindFailed, IOException, AWTException {

	
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
	
		this.markupdeletebegin();
		
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		/*
		 * driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		 * Log.message("measure tool button is clicked again"); Actions act1=new
		 * Actions(driver);
		 * act1.moveToElement(squarecreation).clickAndHold().build().perform();
		 * SkySiteUtils.waitTill(2000); act.click(squarecreation).build().perform();
		 * Log.message("Square Calibrator selected"); String
		 * Screenshot4=filepath+"\\CalibratorEndRect.png"; String
		 * Screenshot5=filepath+"\\CalibratorstartRect.png"; Screen s1=new Screen();
		 * 
		 * Pattern p3 = new Pattern(Screenshot4);
		 * 
		 * Pattern p4 = new Pattern(Screenshot5);
		 * 
		 * s.click(p3);
		 * 
		 * s.drag(p3);
		 * 
		 * s.dropAt(p4);
		 */

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname2))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act12 = new Actions(driver);
		act12.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act12.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis2 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath2 = fis2.getAbsolutePath().toString();
		String Screenshotruler = filepath2 + "\\StartpointRuler.png";
		String Screenshot2ruler = filepath2 + "\\EndPointRuler.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshotruler);

		Pattern p6 = new Pattern(Screenshot2ruler);

		s2.drag(p5);

		s2.dropAt(p6);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("9");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act123 = new Actions(driver);
		act123.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act123.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis3 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath22 = fis3.getAbsolutePath().toString();
		String Screenshot3startruler = filepath22 + "\\StartPointMultiRuler.png";
		String Screenshot3Endruler = filepath22 + "\\EndPointMultiRuler.png";
		Screen s3 = new Screen();

		Pattern p31 = new Pattern(Screenshot3startruler);

		Pattern p32 = new Pattern(Screenshot3Endruler);

		s3.drag(p31);

		s2.dropAt(p32);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("15");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("9");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(thirdmarkup);
		Log.message("Enter markup name is " + thirdmarkup);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		
		markupdropdown.click();
		Log.message("Clicked on the mark up dropdown list");
		SkySiteUtils.waitTill(10000);
		String Screenshot7 = filepath + "\\Clickscreenshotnew.png";
		Pattern p7 = new Pattern(Screenshot7);
		s.click(p7);
		viewAllMarkUplist.click();
		Log.message("Clicked on the View All to load all the mark ups");
		s.click(p7);
		Log.message("Clicked on particular section of the viewer to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		
		String savedscreenshot1 = filepath1 + "\\Beforeclosing.png";
		String savedscreenshot2 = filepath1 + "\\Afterclosing.png";
		SkySiteUtils.waitTill(10000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
		
		SkySiteUtils.waitTill(10000);
		
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(5000);
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(5000);
		String MainWindow2 = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		// driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		// Log.message("View all button is clicked");
		SkySiteUtils.waitTill(10000);
		
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading after clicking on view all");
		
		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}

		
/**		
		
		
		
		
		

		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\OutPutpicformultiruler.png";
		String savedscreenshot2 = filepath1 + "\\Multiruleroutput.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
**/
	@FindBy(xpath = ".//*[@id='measurementCircleCreate']/a/i")
	WebElement circleicon;
	@FindBy(xpath = ".//*[@id='measurementFreehandLengthCreate']/a/i")
	WebElement frehandicon;

	/**
	 * Method to draw combination of ruler and reference calibrator in one markup
	 * 
	 * @throws FindFailed
	 * @throws IOException 
	 * @throws InterruptedException 
	 * Modified By Trinanjwan | 03-December-2018
	 * 
	 */
	public boolean drawCombinationRulerAndcalibrator(String markupname, String filepath1) throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='MultiCombinationFile']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartRuler43.png";
		String Screenshot2 = filepath + "\\EndRuler43.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		Log.message("home button is clicked");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\StartRect43.png";
		String Screenshot5 = filepath + "\\EndRect43.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act2 = new Actions(driver);
		act2.moveToElement(circleicon).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(circleicon).build().perform();
		Log.message("circle Calibrator selected");

		String Screenshot6 = filepath + "\\StartCircle43.png";
		String Screenshot7 = filepath + "\\EndCircle43.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshot6);

		Pattern p6 = new Pattern(Screenshot7);

		s2.click(p5);

		s2.drag(p5);

		s2.dropAt(p6);
   
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home button is clicked");
/**		
       WebElement linetool=driver.findElement(By.xpath(".//*[@id='measurementLineCreate']/a/i"));
		SkySiteUtils.waitTill(4000);
		Actions act33=new Actions(driver);
		act33.sendKeys(Keys.ESCAPE);
		for(int i=0;i<2;i++)
		{
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act3 = new Actions(driver);
		act3.moveToElement(circleicon).clickAndHold().build().perform();
		SkySiteUtils.waitTill(6000);
		act.click(circleicon).build().perform();
		Log.message("free hand tool  Calibrator selected");
		SkySiteUtils.waitTill(6000);
		}
		String Screenshot8 = filepath + "\\Startfreeruler43.png";
		String Screenshot9 = filepath + "\\EndFreeRuler43.png";
		Screen s3 = new Screen();

		Pattern p7 = new Pattern(Screenshot8);

		Pattern p8 = new Pattern(Screenshot9);

	     s3.click(p7);

		s3.drag(p7);

		s3.dropAt(p8);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
**/		
		
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		/*markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");*/

		SkySiteUtils.waitTill(14000);
		
		
		String savedscreenshot1 = filepath1 + "\\Beforeloadingmarkupcal8.png";
		String savedscreenshot2 = filepath1 + "\\Afterloadingmarkupcal8.png";
		
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
		
		
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();
		
		SkySiteUtils.waitTill(5000);
		String Screenshot9 = filepath + "\\Clickscreenshotnewfree.png";
		
		Pattern p10 = new Pattern(Screenshot9);

		s.click(p10);
		Log.message("Clicked on viewer to dismiss the dropdown");
		
		SkySiteUtils.waitTill(10000);
		
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		
		Log.message("Checking both the images pixel by pixel??");

		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
			BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Log.message("Matched: " + pass + " and " + "unmatched: " + fail);
			
			if(fail>0)
				return false;
			else 
				return true;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		}

/**	
		
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\tarakoutput.png";
		String savedscreenshot2 = filepath1 + "\\Alldrawing.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	
**/

   /**Method to draw square hyperlink to link with non viewable file
    * 
    */
	/**
	 * Method to draw and save square hyperlink
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public void drawAndSaveSquareHyperLinktolinknonviewablefile(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Nonviewablefilelink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);
		

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[16]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");
        //modifeid
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		/*String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}
	/**Method to verify linked non viewable file is downloaded successfully
	 * 
	 */
	public boolean VerifyLinkedNonViewableFileDownloaded(String markupname, String filepath1,String downloadpath,String filename)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		this.Delete_Files_From_Folder(downloadpath);
		SkySiteUtils.waitTill(9000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(10000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(9000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(9000);
		if (this.isLinkedFileDownloaded(downloadpath, filename)==true)
			return true;
		else
			return false;
	
	}
	/**
	 * Method to check if linked file is downloaded in the folder Scripted By : Tarak
	 * 
	 */
	public boolean isLinkedFileDownloaded(String downloadfolderpath, String Downloadfilename) {
		Log.message("Searching the downloaded file");
		String Filename = Downloadfilename;
		Log.message("Downloaded file name is " + Filename);
		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		SkySiteUtils.waitTill(9000);
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().equals(Filename)) {
					Log.message("File is found");
					return true;
				}
			}
		}
		return false;
	}

	
	/**Method to rotate 180 degree counter clockwise
	 * @throws IOException 
	 * @throws InterruptedException 
	 * 
	 */
	public boolean rotateCounterClockWise(String markupname,String filepath1) throws IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Rotate180']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

		
		SkySiteUtils.waitTill(4000);
		rotatemenu.click();
		Log.message("Rotate tool is clicked");
         WebElement counterclockwisetool=driver.findElement(By.xpath(".//*[@id='btnRotateAntiClockWise']"));
         
		SkySiteUtils.waitTill(2000);
		//WebElement aniclockicon = driver.findElement(By.id("btnRotateClockWise"));
		SkySiteUtils.waitForElement(driver, counterclockwisetool, 40);
        for(int i=0;i<2;i++) {
		Actions act = new Actions(driver);
		act.moveToElement(counterclockwisetool).click().build().perform();
		SkySiteUtils.waitTill(5000);
        }
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitTill(2000);
		saveafterrotate.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(20000);

		

		File fistemp12 = new File(PropertyReader.getProperty("180temp"));
		String filepathtemp12 = fistemp12.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp12 + "\\Resultscreenshotafterrotation.png";
		String savedscreenshot2 = filepath1 + "\\output180.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		//Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
  }
	/**Method to change the 180 rotated file to default
	 * 
	 */
	public void changecounterrotatedfile() {

			SkySiteUtils.waitTill(4000);
			WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
			// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
			// String filepathtemp=fistemp.getAbsolutePath().toString();
			// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

			
			SkySiteUtils.waitTill(4000);
			rotatemenu.click();
			Log.message("Rotate tool is clicked");

			SkySiteUtils.waitTill(2000);
			WebElement aniclockicon = driver.findElement(By.xpath(".//*[@id='btnRotateClockWise']"));
			SkySiteUtils.waitForElement(driver, aniclockicon, 40);
            for(int i=0;i<2;i++) {
			Actions act = new Actions(driver);
			act.moveToElement(aniclockicon).click().build().perform();
            }
			// JavascriptExecutor executor = (JavascriptExecutor)driver;
			// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
			SkySiteUtils.waitTill(2000);

			SkySiteUtils.waitTill(2000);
			saveafterrotate.click();
			Log.message("Save button is clicked");
			SkySiteUtils.waitTill(20000);
			Log.message("File is rotated and changed to default state successfully");

		}
      @FindBy(xpath=".//*[@id='btnSubmitPassword']")
      WebElement submitbutton;
	
	/**Method to open password protected file in viewer
	 * @throws InterruptedException 
	 * @throws FindFailed 
	 * @throws IOException 
	 * 
	 */
	public boolean openPasswordProtectedFile(String markup,String filepath1) throws InterruptedException, FindFailed, IOException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Passwordprotected']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		//this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
	    driver.findElement(By.xpath(".//*[@id='txtFilePassword']")).sendKeys("Arcind@123");
	    Log.message("Password is entered");
	    submitbutton.click();
	    Log.message("Submit button is clicked");
	    SkySiteUtils.waitTill(7000);
		File fistemp = new File(PropertyReader.getProperty("pptfolder"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\Passwordprotectfile.png";
		String savedscreenshot2 = filepath1 + "\\output57.png";

		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");
		Thread.sleep(6000);
		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	/**
	 * Method to draw and save hyperlink for multipage file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawAndSaveHyperLinkMultiPagewithmarkup(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Multipagewithmarkup']")).click();
		Log.message("Multi page folder");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeltemultipage();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPoint_MultiPageFile.png";
		String Screenshot2 = filepath + "\\EndPoint_MultiPageFile.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[19]/div/a/div[3]")).click();
		Log.message(" Mutli Page File Folder is selected again for linking");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
		Log.message("File  is selected");
		driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[14]/a")).click();
		Log.message("9th page is selected");
		// driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		// Log.message("1st page folder is clicked");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

			for (int x = 0; x < imgA.getWidth(); x++) {

				for (int y = 0; y < imgB.getHeight(); y++) {

					if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {

						flag = true;
						break;
					}
				}

			}
		}
		if (flag == true) {
			Log.message("images are equal");
			return true;
		} else {
			Log.message("images are not equal");
			return false;
		}

	}
 /**Method to link multipage with markup drawn
 * @throws IOException 
 * @throws FindFailed 
 * @throws InterruptedException 
  * 
  */ 
public boolean multipageLinkWithMarkupDrawing(String markupname,String filepath1) throws IOException, FindFailed, InterruptedException {
	
	driver.switchTo().defaultContent();
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\temphyperlinkmultipage.PNG";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
	SkySiteUtils.waitTill(4000);
	
	WebElement viewallbtn=driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']"));
	// String
	// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
	viewallbtn.click();
	Log.message("view all button is clicked");
	SkySiteUtils.waitTill(4000);
	
	SkySiteUtils.waitTill(5000);
	
	File fistemp12 = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp12 = fistemp12.getAbsolutePath().toString();
	String savedscreenshot1 = filepathtemp12 + "\\Multipagefile.png";
	String savedscreenshot2 = filepath1 + "\\OutputMultipagefile.png";
	SkySiteUtils.waitTill(4000);
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
	SkySiteUtils.waitTill(3000);
	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}
/**
 * Method to draw and save square hyperlink for diff folder and diff file with markup
 * 
 * @throws IOException
 * @throws FindFailed
 * Modified By Trinanjwan | 27-November-2018
 */
public boolean drawAndSaveSquareHyperLinkForDiffFolderDifffileWithMarkup(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='FolderMarkuplink']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	String Screenshot3 = filepath + "\\Clickscreenshotnewfree.png";
	
	
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);
	
	Pattern p3 = new Pattern(Screenshot3);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(3000);
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);

	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	s.click(p3);
	Log.message("Clicked on viewer to dismiss the dropdown");
//	savebtn.click();
//	Log.message("save button is clicked");
	SkySiteUtils.waitTill(10000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}
/**
 * Method to verify working of square hyperlink for diff folder diff file withv markup
 * 
 * @throws FindFailed
 * @throws IOException
 * @throws UnsupportedFlavorException
 * @throws InterruptedException 
 * Modified By Trinanjwan | 27-November-2018
 */


@FindBy(xpath = "//span[text()='(R-1) 1_EName.pdf']")
WebElement destinationfileheader1;

public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileWithMarkup(String markupname, String filepath1)
		throws FindFailed, UnsupportedFlavorException, IOException, InterruptedException {
	// driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(8000);
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);

	SkySiteUtils.waitTill(4000);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", copylink);
	// Log.message("Copy to clipboard clicked");
	/*
	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
	 * toolkit.getSystemClipboard(); String result = (String)
	 * clipboard.getData(DataFlavor.stringFlavor);
	 * System.out.println("String from Clipboard:" + result);
	 */
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
	SkySiteUtils.waitTill(5000);
	
	
	SkySiteUtils.waitTill(10000);
	String actual=destinationfileheader1.getText();
	String expected="(R-1) 1_EName.pdf";
	Log.message("Actual name is:"+actual);
	Log.message("Expected name is:"+expected);
	
	if(actual.contentEquals(expected))
			return true;
	else 
		return false;
	
}
	
	/**
	WebElement viewallbtn=driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']"));
	SkySiteUtils.waitForElement(driver, viewallbtn, 60);
	viewallbtn.click();
	Log.message("view all button is clicked");
	SkySiteUtils.waitTill(4000);
	File fistemp2= new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp2 = fistemp2.getAbsolutePath().toString();
	String savedscreenshot1 = filepathtemp2 + "\\resultlinkfilewithmarkup.png";
	String savedscreenshot2 = filepath1 + "\\outputpicwithmarkup.png";
	SkySiteUtils.waitTill(4000);
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	 
BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
SkySiteUtils.waitTill(3000);
BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

			Thread.sleep(2000);

			Log.message("Checking the height and width of both the images are same?");
			Thread.sleep(6000);
			if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
				Log.message("image height width are different");
				return false;
			}

			int width = imgA.getWidth();
			int height = imgA.getHeight();
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

						Log.message("Image are different");
						return false;
					}
				}

			}

			return true;
		}
		
**/		
		
      @FindBy(xpath=".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[4]/a/i")
      WebElement fullscreen;
/**
 * Method to draw and save square hyperlink for same folder diff file with full screen
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public void drawAndSaveSquareHyperLinkOmnFullScreen(String markupname, String filepath1) throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath("//*[text()='Viewer_Testing_CircleHyperLink']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	fullscreen.click();
	Log.message("Full screen is enabled");
	
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[3]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	Log.message("save button is clicked again");
	SkySiteUtils.waitTill(4000);
	/*String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;*/
}
/**
 * Method to verify working of square hyperlink for same folder diff file on full screen
 * 
 * @throws FindFailed
 * @throws IOException
 * @throws UnsupportedFlavorException
 * @throws AWTException 
 * 
 */
public boolean VerifyfunctionalitySquareHyperLinkSameFolderDiffFileOnFullScreen(String markupname, String filepath1)
		throws FindFailed, UnsupportedFlavorException, IOException, AWTException {
	// driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(9000);
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);

	SkySiteUtils.waitTill(10000);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", copylink);
	// Log.message("Copy to clipboard clicked");
	/*
	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
	 * toolkit.getSystemClipboard(); String result = (String)
	 * clipboard.getData(DataFlavor.stringFlavor);
	 * System.out.println("String from Clipboard:" + result);
	 */
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
    SkySiteUtils.waitTill(4000);
    Robot rb=new Robot();
    rb.keyPress(KeyEvent.VK_ESCAPE);
    rb.keyRelease(KeyEvent.VK_ESCAPE);
    Log.message("Escape is entered");
    SkySiteUtils.waitTill(4000);
	String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
	Log.message("File name is " + filename);
	String expectedfilename = "(R-1) 1_EName.pdf";
	if (filename.equals(expectedfilename))
		return true;

	else
		return false;

}
/**
 * Method to edit hyperlink 
 * 
 * @throws IOException
 * @throws FindFailed
 * Modified By Trinanjwan | 27-November-2018
 */
public boolean drawAndSaveSquareHyperlinkafterChanging(String markupname, String filepath1)
		throws IOException, FindFailed {

	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='Viewertesting']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	String Screenshot3 = filepath + "\\Clickscreenshotnewfree.png";
	
	
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);
	
	Pattern p3 = new Pattern(Screenshot3);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[3]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(3000);
	driver.findElement(By.xpath(".//*[@id='btnhyperLinkEdit']")).click();
	Log.message("Change link option is clicked");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
	Log.message("Original file to be link is selected");
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
//	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
//	Log.message("save button is clicked again");
//	SkySiteUtils.waitTill(4000);
	
	s.click(p3);
	Log.message("Clicked on the viewer to dismiss tooltip/dropdown");

	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	SkySiteUtils.waitTill(5000);
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
//	savebtn.click();
//	Log.message("save button is clicked");
	s.click(p3);
	Log.message("Clicked in order to dismiss the dropdown");
	SkySiteUtils.waitTill(10000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}

/**
 * Method to delete hyperlink 
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public boolean drawHyperLinkAndDelete(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='Viewertesting']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitTill(4000);
	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
	Log.message("screenshot taken of default file");
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[2]/div[2]/div[4]/div/div[1]/div/div[2]/div[3]/button[1]")).click();
	Log.message("Delete button is clicked");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
	Log.message("Home page is clicked");
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	

	Log.message("Screenshot taken after deleting hyperlink");
	SkySiteUtils.waitTill(4000);
	
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}


/**
 * Method to draw and save square hyperlink for diff folder and diff file(DWG File)
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public  void drawAndSaveSquareHyperLinkForDiffFolderDifffileDWGFile(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='ViewerTesting2']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[19]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(3000);
	driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
	Log.message("Page 1 is selected");
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	Log.message("save button is clicked again");
	SkySiteUtils.waitTill(4000);

	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	/*File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;*/
}


/**
 * Method to verify working of square hyperlink for diff folder diff file DWG File
 * 
 * @throws FindFailed
 * @throws IOException
 * @throws UnsupportedFlavorException
 * 
 */

public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileDWGFile(String markupname, String filepath1)
		throws FindFailed, UnsupportedFlavorException, IOException {
	// driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(8000);
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);

	SkySiteUtils.waitTill(4000);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", copylink);
	// Log.message("Copy to clipboard clicked");
	/*
	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
	 * toolkit.getSystemClipboard(); String result = (String)
	 * clipboard.getData(DataFlavor.stringFlavor);
	 * System.out.println("String from Clipboard:" + result);
	 */
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
	SkySiteUtils.waitTill(5000);
	String filename = driver.findElement(By.xpath("//*[@data-original-title='Test.dwg']")).getText();
	Log.message("File name is " + filename);
	String expectedfilename = "(R-1) Test.dwg";
	if (filename.equals(expectedfilename))
		return true;

	else
		return false;

}
@FindBy(xpath=".//*[@id='liGlobalSearch']/a/i")
WebElement Globalsearch;
@FindBy(xpath=".//*[@id='coldivAdvanceFileSearch']/div[3]/div[1]/button[2]")
WebElement resetbtn;
@FindBy(xpath=".//*[@placeholder='Project number']")
WebElement Projnobox;
@FindBy(xpath=".//*[@id='searchiconbutton']")
WebElement searchbtn;
@FindBy(xpath="//*[@class='icon icon-view-files ico-lg']")
WebElement viewfileicon;
/**Method to search file and open the resultant file in viewer
 * 
 */
 public boolean searchAndOpenFileInViewer()
 {
	 SkySiteUtils.waitForElement(driver, Globalsearch, 50);
	 Globalsearch.click();
	 Log.message("Global search option is clicked");
	 SkySiteUtils.waitForElement(driver, resetbtn, 50);
	 resetbtn.click();
	 Log.message("Reset button is clicked");
	 SkySiteUtils.waitForElement(driver, Projnobox, 50);
	 Projnobox.sendKeys("450");
	 Log.message("Project number has been entered");
	 SkySiteUtils.waitForElement(driver, searchbtn,60);
	 searchbtn.click();
	 Log.message("search button is clicked");
	 SkySiteUtils.waitForElement(driver, viewfileicon, 50);
	 viewfileicon.click();
	 Log.message("view file option is clicked");
	 SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");
		SkySiteUtils.waitTill(5000);
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1 mb.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1 mb.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	 
 }
 
 
 
 /**
  * Method to draw and save square hyperlink for diff folder and diff file(DWf File) with pdf file
  * 
  * @throws IOException
  * @throws FindFailed
  * 
  */
 public void drawAndSaveSquareHyperLinkForDiffFolderDifffileDWFFile(String markupname, String filepath1)
 		throws IOException, FindFailed {
 	boolean flag = false;
 	Log.message("Switched to default content");
 	SkySiteUtils.waitTill(5000);
 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 	Log.message("Switched to frame");
 	driver.findElement(By.xpath(".//*[text()='Dwf_file']")).click();
 	Log.message("Folder selected");
 	SkySiteUtils.waitTill(4000);
 	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
 	Log.message("File is selected");
 	SkySiteUtils.waitTill(4000);
 	String MainWindow = driver.getWindowHandle();
 	Log.message(MainWindow);
 	for (String childwindow : driver.getWindowHandles()) {
 		driver.switchTo().window(childwindow);

 	}
 	Log.message("switched to child window");

 	driver.switchTo().defaultContent();
 	SkySiteUtils.waitTill(4000);
 	// method to delete existing markup
 	this.markupdeletebegin();
 	SkySiteUtils.waitTill(4000);
 	hyperlinktool.click();
 	Log.message("Hyperlink tool is clicked");
 	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
 	Actions act = new Actions(driver);
 	act.moveToElement(recthyperlink).clickAndHold().build().perform();
 	SkySiteUtils.waitTill(2000);
 	act.click(recthyperlink).build().perform();
 	Log.message("Rect Hyper Link button is clicked");
 	SkySiteUtils.waitTill(2000);
 	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
 	String filepath = fis.getAbsolutePath().toString();
 	String Screenshot1 = filepath + "\\StartforDwffile.PNG";
 	String Screenshot2 = filepath + "\\EndforDwffile.PNG";
 	Screen s = new Screen();

 	Pattern p1 = new Pattern(Screenshot1);

 	Pattern p2 = new Pattern(Screenshot2);

 	// s.click(p1);

 	s.drag(p1);

 	s.dropAt(p2);
 	Log.message("image is drawn");
 	SkySiteUtils.waitTill(4000);
 	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
 	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]/h4")).click();
 	Log.message("Folder is selected");
 	SkySiteUtils.waitTill(4000);
 	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
 	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
 	Log.message("File is selected");
 	SkySiteUtils.waitTill(3000);
 	driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
 	Log.message("Page 1 is selected");
 	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
// 	Log.message("Page1 is selected");
 	SkySiteUtils.waitForElement(driver, okbutton, 50);
 	okbutton.click();
 	Log.message("Ok button is clicked");

 	SkySiteUtils.waitTill(4000);
 	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
 	Log.message("Home page is clicked");
 	SkySiteUtils.waitForElement(driver, savebtn, 50);
 	savebtn.click();
 	Log.message("save button is clicked");
 	SkySiteUtils.waitTill(4000);
 	markupfield.sendKeys(markupname);
 	Log.message("Enter markup name is " + markupname);
 	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
 	savemarkupbtn.click();
 	Log.message("savemarkupbtn is clicked");
 	SkySiteUtils.waitTill(14000);
 	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
 	Log.message("save button is clicked again");
 	SkySiteUtils.waitTill(4000);

 /*	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
 	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
 	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
 	// String filepathtemp=fistemp.getAbsolutePath().toString();
 	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
 	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

 	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

 	Log.message("Screenshot taken of saved markup");
 	SkySiteUtils.waitTill(4000);
 	hyperlinkdropdown.click();
 	Log.message("hyperlink markup dropdown clicked");
 	SkySiteUtils.waitTill(4000);
 	// for(WebElement markupitem:hperlinklist)
 	// if(markupitem.getText().contains(markupname))
 	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
 	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
 	Log.message("hide all button clicked and image disappears");

 	SkySiteUtils.waitTill(4000);
 	showhyperlinks.click();
 	Log.message("show all hyperlink clikced");
 	SkySiteUtils.waitTill(4000);
 	savebtn.click();
 	Log.message("save button is clicked");
 	SkySiteUtils.waitTill(4000);
 	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

 	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
 	Log.message("Screenshot taken after redrawing");

 	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

 	// Thread.sleep(2000);

 	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

 	// Thread.sleep(2000);

 	Log.message("Checking the height and width of both the images are same?");

 	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
 		Log.message("image height width are different");
 		return false;
 	}

 	int width = imgA.getWidth();
 	int height = imgA.getHeight();
 	for (int y = 0; y < height; y++) {
 		for (int x = 0; x < width; x++) {

 			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

 				Log.message("Image are different");
 				return false;
 			}
 		}

 	}

 	return true;*/
 }
 
 
 /**
  * Method to verify working of square hyperlink for diff folder diff file of DWF File with pdf file
  * 
  * @throws FindFailed
  * @throws IOException
  * @throws UnsupportedFlavorException
  * 
  */

 public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileDWFFile(String markupname, String filepath1)
 		throws FindFailed, UnsupportedFlavorException, IOException {
 	// driver.switchTo().defaultContent();
 	SkySiteUtils.waitTill(8000);
 	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
 	String filepathtemp = fistemp.getAbsolutePath().toString();
 	String tempscreenshotpath = filepathtemp + "\\temforhyperlinkfordwffile.PNG";
 	Screen s = new Screen();

 	Pattern p1 = new Pattern(tempscreenshotpath);

 	SkySiteUtils.waitTill(4000);
 	s.click(p1);
 	Log.message("saved hyperlink is clicked");
 	SkySiteUtils.waitTill(4000);
 	// JavascriptExecutor executor = (JavascriptExecutor)driver;
 	// executor.executeScript("arguments[0].click();", copylink);
 	// Log.message("Copy to clipboard clicked");
 	/*
 	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
 	 * toolkit.getSystemClipboard(); String result = (String)
 	 * clipboard.getData(DataFlavor.stringFlavor);
 	 * System.out.println("String from Clipboard:" + result);
 	 */
 	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
 	JavascriptExecutor executor = (JavascriptExecutor) driver;
 	executor.executeScript("arguments[0].click();", imagelink);
 	Log.message("image is clicked");
 	SkySiteUtils.waitTill(5000);
 	String filename = driver.findElement(By.xpath("//*[@data-original-title='re1(13).pdf']")).getText();
 	Log.message("File name is " + filename);
 	String expectedfilename = "(R-1) re1(13).pdf";
 	if (filename.equals(expectedfilename))
 		return true;

 	else
 		return false;

 }
 /**method to verify document navigation from viewer
  * 
  * 
  */
 public boolean documentNavigation()
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(".//*[text()='Folderhyperlink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='navNextDoc']/i")).click();
		Log.message("Next button icon is clicked");
		SkySiteUtils.waitTill(6000);
		String filename = driver.findElement(By.xpath("//*[@data-original-title='sum123.pdf']")).getText();
		Log.message("File name is " + filename);
		SkySiteUtils.waitTill(6000);
		String expectedfilename = "(R-1) sum123.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

}
 

 /**
	 * Method to draw Multiple markup annotation in viewer Scripted by : Tarak
	 * 
	 * @return
	 * @throws AWTException
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 */
	public void drawMultiMarkupAnnotation(String markupname, String filepath1,int n)
			throws AWTException, InterruptedException, FindFailed, IOException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		/**
		 * driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 * Log.message("Switched to frame");
		 * driver.findElement(By.xpath(".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")).click();
		 * Log.message("Folder selected"); SkySiteUtils.waitTill(8000);
		 * driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		 * Log.message("File is selected");
		 **/
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		int i=0;
		for(i=1;i<n;i++){
		drawlinetool.click();
		Log.message("Linetool is clicked");
		SkySiteUtils.waitTill(4000);
		Actions act = new Actions(driver);
		act.moveToElement(lineannotation).clickAndHold(lineannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Line annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		String markupnamerandom=this.Random_Markupname();
		markupfield.sendKeys(markupnamerandom);
		Log.message("Enter markup name is " + markupnamerandom);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		
		}
	/*	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AftersavingmarkupArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("show all markup clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}
	


/**
 * Method to delte markup at begining
 * @throws AWTException 
 * 
 */
public void markupdeletebegi2() throws AWTException {
	{

		if (markupdropdown.isDisplayed()) {
			int sizeoflist = markuplist.size();
			Log.message("size is " + sizeoflist);
			for (int i = 1; i <= sizeoflist; i++) {
				markupdropdown.click();
				Log.message("markup dropdown clicked");
				SkySiteUtils.waitTill(8000);

				if (driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).isDisplayed())
					//driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
				{
					Actions act=new Actions(driver);
					//act.moveToElement(icondelete).click(icondelete).build().perform();
					icondelete.click();
					Log.message("icon is clicked");
				}
				Log.message("added markup deleted successfully");
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", yesbtn);
				// yesbtn.click();
				Log.message("Yes button is clicked");
				SkySiteUtils.waitTill(10000);
				Robot rb=new Robot();
				rb.keyPress(KeyEvent.VK_CONTROL);
				rb.keyPress(KeyEvent.VK_W);
				rb.keyRelease(KeyEvent.VK_W);
				rb.keyRelease(KeyEvent.VK_CONTROL);
				Log.message("tab is closed");
				SkySiteUtils.waitTill(5000);
				String windowHandle = driver.getWindowHandle();
				ArrayList tabs = new ArrayList(driver.getWindowHandles());
				System.out.println(tabs.size());

				driver.switchTo().window((String) tabs.get(0));
				SkySiteUtils.waitTill(5000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(5000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				SkySiteUtils.waitTill(5000);

				 driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
				 Log.message("file is clicked");
				 String MainWindow = driver.getWindowHandle();
					Log.message(MainWindow);
					for (String childwindow : driver.getWindowHandles()) {
						driver.switchTo().window(childwindow);

					}
					Log.message("switched to child window");

					driver.switchTo().defaultContent();
					SkySiteUtils.waitTill(4000);
				 
			}
		} else {

			Log.message("no markup is present");

		}

	}
}

/**
 * Method to return back to the hyperlink source file
 * @throws AWTException 
 * Modified By Trinanjwan | 27-November-2018
 */



 		public void returnToSource(String filename)
 		
 		{
 			String fileheader = "//span[text()='%s']";
 			WebElement element=driver.findElement(By.xpath(String.format(fileheader, filename)));
 			Actions act = new Actions(driver);
 			act.moveToElement(element).click().build().perform();
 			SkySiteUtils.waitTill(10000);
 			Log.message("Navigate back to the destination file");

 		}
 		}
