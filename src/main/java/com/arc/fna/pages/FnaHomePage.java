package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class FnaHomePage extends LoadableComponent<FnaHomePage> 
{

	WebDriver driver;
	private boolean isPageLoaded;

	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='user-info']")

	WebElement btnuserinfo;
	
	@FindBy(xpath = "//*[@id='setting']")

	WebElement btnSetting;

	@FindBy(css = "#liManageUsers>a")

	WebElement btnManageUser;

	@FindBy(xpath = ".//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[2]/span")

	WebElement lblUsageTotal;

	@FindBy(xpath = ".//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[3]/span")

	WebElement lblUsageAvailable;

	@FindBy(xpath = ".//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[4]/span")

	WebElement lblUsageUsed;

	@FindBy(xpath = ".//*[@id='divUsersListGrid']/div[2]/table/tbody/tr[2]/td[2]")

	WebElement fldUserName;

	@FindBy(xpath = ".//*[@id='divUsersListGrid']/div[2]/table/tbody/tr")

	WebElement tableRow;

	@FindBy(xpath = "//input[@id='txtSerachValue']")

	WebElement txtSerachValue;

	@FindBy(xpath = "//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[1]/a")

	WebElement username;

	@FindBy(xpath = "//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/button")

	WebElement drpbtnasnpet;

	@FindBy(xpath = "//button[@id='btnSearch']")

	WebElement btnsearch;

	@FindBy(xpath = "//*[@id='divUsersListGrid']/div[2]/table/tbody/tr[2]/td[2]")

	WebElement userlistname;

	@FindBy(xpath = "//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[3]/a")

	WebElement mail;

	@FindBy(xpath = "//*[@id='divUsersListGrid']/div[2]/table/tbody/tr[2]/td[4]")

	WebElement listmail;

	@FindBy(xpath = "//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[2]/a")

	WebElement phonework;

	@FindBy(xpath = "//*[@id='divUsersListGrid']/div[2]/table/tbody/tr[2]/td[3]")

	WebElement listphonework;

	@FindBy(xpath = "//*[@id='aspnetForm']/div[3]/nav/div[2]/div/div/div/div/ul/li[4]/a/span[1]")

	WebElement userlicense;

	@FindBy(xpath = "//*[@id='divUserLicenseTypeSearch']/div/button")

	WebElement drpuserlicense;

	@FindBy(xpath = "//*[@id='divUserLicenseTypeSearch']/div/div/ul/li[1]/a")

	WebElement employee;

	@FindBy(xpath = "//*[@id='divUserLicenseTypeSearch']/div/div/ul/li[2]/a")

	WebElement shareduser;

	@FindBy(css = "#litecount")

	WebElement numberOfliteuser;

	@FindBy(xpath = "//*[@id='divUserLicenseTypeSearch']/div/div/ul/li[3]/a")

	WebElement liteuser;

	@FindBy(xpath = "//*[@id='btnResetSearch']")

	WebElement resetsearch;

	@FindBy(xpath = "//*[@id='divUsersListGrid']/div[1]/div")

	WebElement sortascend;

	@FindBy(xpath = "//*[@id='divUsersListGrid']/div[2]/table/tbody/tr[5]/td[2]")

	WebElement lastusername;

	@FindBy(xpath = "//*[@id='ctl00_DefaultContent_rdoSponsoredUserType']")

	WebElement dlfshareduser;

	@FindBy(xpath = "//*[@id='ctl00_DefaultContent_txtFirstName']")

	WebElement dlffristname;

	@FindBy(xpath = ".//*[@id='ctl00_DefaultContent_txtEmail']")

	WebElement dlfemail;

	@FindBy(xpath = "//*[@id='ctl00_DefaultContent_Button3']")

	WebElement dlfsavecls;

	@FindBy(xpath = "//button[@id='btnNewUser']")

	WebElement buttonNewUser;

	@FindBy(xpath = "//*[@id='ctl00_DefaultContent_rdoHostedUserType']")

	WebElement dlfempuser;

	@FindBy(xpath = "//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[3]")

	WebElement availablecnt;

	@FindBy(xpath = "//div/input[@id='UserID']")

	WebElement txtBoxUserName;

	@FindBy(xpath = "//div/input[@id='Password']")

	WebElement txtBoxPassword;

	@FindBy(xpath = "//*[@id='btnLogin']")

	WebElement buttonLogin;

	@FindBy(xpath = "//*[@id='user-info']")

	WebElement clkprofile;

	@FindBy(xpath = "//i[@class='icon icon-logout icon-lg']")

	WebElement pfloutput;

	@FindBy(xpath = "//*[@id='button-1']")

	WebElement btnyes;

	@FindBy(xpath = "//*[@id='Contacts']/a")

	WebElement btncontact;

	@FindBy(xpath = "//*[@id='btnNewContact']")

	WebElement btnnewcontact;

	@FindBy(xpath = "//*[@id='ctl00_DefaultContent_rdoContactType']")

	WebElement radiocontact;

	@FindBy(xpath = "//span[@class='noty_text']")

	WebElement notifymsg;

	@FindBy(xpath = "//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[1]/span")

	WebElement usage;

	@FindBy(xpath = "//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[2]/span")

	WebElement total;

	@FindBy(xpath = "//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[3]/span")

	WebElement available;

	@FindBy(xpath = "//*[@id='ctrlAccountUsageSummary']/div/table/tbody/tr[2]/td[4]/span")

	WebElement used;
	
	@FindBy (xpath = "//*[@id='rgtpnlMore']")
	WebElement moreLocal ;

	@FindBy(css = "#user-info")
	@CacheLookup
	WebElement MyProfilebtn;

	@FindBy(css = "#limyprofile > a:nth-child(1)")
	@CacheLookup
	WebElement MyProfilebtndrpdwn;

	@FindBy(xpath="(.//*[@class='standartTreeImage'])[4]")
	WebElement folderTreeCheck;
	
	@FindBy(xpath=".//*[@id='lblProjCaption']")
	WebElement projectInvitationPopover;
	
	@FindBy(xpath=".//*[@value='Accept selected collections']")
	WebElement btnAcceptSelecetedCollection;
	
	//====== Team Xpath=================
	
	@FindBy(xpath="//a[contains(text(),'Teams')]")
	WebElement TeamLink;
	
	@FindBy(xpath="//*[@id='btnAddNewTeam']")
	WebElement addteam;
	
	@FindBy(xpath="//*[@id='lftpnlMore']")
	WebElement MoreOption;
	
	@FindBy(xpath="//i [@class='icon icon-add-teamMemberAddress']")
	WebElement addTeamMember_Link;
	
	@FindBy(xpath="//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[1]/img")
	WebElement Second_Checkbox;
	
	
	@FindBy(xpath="//*[@id='btnAddClose']")
	WebElement AddAndCloseButton;
	

	
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 10);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	
	public FnaHomePage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	/**
	 * Method written for Random Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_ConactUser() 
	{

		String str = "Contact";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written to verify Usage count for all fields
	 *  Scripted By: Sekhar
	 */

	public boolean verifyUsageCount()
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked");
		SkySiteUtils.waitTill(2000);
		btnManageUser.click();
		Log.message("Manage user button has been clicked");
		driver.switchTo().frame("myFrame");
		SkySiteUtils.waitForElement(driver, lblUsageAvailable, 15);
		int lblUsageTotal1 = 20;
		Log.message("Total seats:" + +lblUsageTotal1);
		int lblUsageAvailable1 = Integer.parseInt(lblUsageAvailable.getText());
		Log.message("Actual available seats:" + +lblUsageAvailable1);
		int lblUsageUsed1 = Integer.parseInt(lblUsageUsed.getText());
		Log.message("Used seats:" + +lblUsageUsed1);
		int lblUsageAvailable2 = lblUsageTotal1 - lblUsageUsed1;
		Log.message("Expected available seats:" + +lblUsageAvailable2);
		if (lblUsageAvailable1 == lblUsageAvailable2)
			return true;
		else
			return false;
	}

	/**
	 * Method written for doing login Employee a/c with valid credential.
	 * 
	 * @throws AWTException
	 */
	public boolean loginWithEmployeeUser() 
	{
		SkySiteUtils.waitTill(5000);
		 if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
		 {
			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		 }
		 SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		String uName4 = PropertyReader.getProperty("Employeeusername");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName4);
		Log.message("Username: " + uName4 + " " + "has been engtered in Username text box.");
		String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been engtered in Password text box.");
		SkySiteUtils.waitForElement(driver, buttonLogin, 20);
		buttonLogin.click();
		Log.message("LogIn button clicked.");
		SkySiteUtils.waitTill(5000);
		String Exptitle = "SKYSITE Facilities & Archive";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(5000);
		/*
		 * int Feedback_Alert =
		 * driver.findElements(By.xpath("//span[@id='btnlater']")).size();//Check
		 * feedback alert comes
		 * Log.message("Checking feedback alert is there or not : "+Feedback_Alert);
		 * if(Feedback_Alert>0) {
		 * driver.findElement(By.xpath("//span[@id='btnlater']")).click();//Click on
		 * 'Ask me later' link
		 * Log.message("Clicked on 'Ask me later' link from feedback!!!"); }
		 */

		if (driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[1]/h4")).isDisplayed()) 
		{
			driver.findElement(By.xpath(".//*[@id='divAutoFeedback']/div/div/div[2]/div[1]/div[2]/div")).click();
			driver.findElement(By.xpath(".//*[@id='autofeedback']")).sendKeys("Hi");
			driver.findElement(By.xpath(".//*[@id='Savefeedback']")).click();
			SkySiteUtils.waitTill(8000);
		}
		Log.message("Login Validation:");
		if (Acttitle_AfterLogin.equals(Exptitle))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Random Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_EditUser() 
	{

		String str = "Edit";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written to verify Used count Scripted By: Sekhar
	 */

	public boolean verifyUsedCount() 
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 10);
		btnSetting.click();
		Log.message("Setting button has been clicked");
		SkySiteUtils.waitTill(2000);
		btnManageUser.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Manage user button has been clicked");
		driver.switchTo().frame("myFrame");

		int totUserInTable = driver.findElements(By.xpath(".//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]"))
				.size();
		Log.message("Total number of users in table:" + +totUserInTable);

		int totalLiteUser = Integer.parseInt(numberOfliteuser.getText());
		Log.message("Total number of Lite users in table:" + +totalLiteUser);

		int lblUsageUsed1 = Integer.parseInt(lblUsageUsed.getText());
		Log.message("Total number of valid users showing in usage count:" + +lblUsageUsed1);

		int totValidUserInTable = totUserInTable - totalLiteUser;
		Log.message("Total number of valid users appearing in table:" + +totValidUserInTable);

		if (totValidUserInTable == lblUsageUsed1)
			return true;
		else
			return false;
	}

	/**
	 * Method written for manage user land page
	 * 
	 * @return
	 */

	public boolean openManageUser() 
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int Avb_count = Integer.parseInt(available.getText());
		Log.message("Available count is:" + +Avb_count);
		SkySiteUtils.waitTill(3000);
		int Used_count = Integer.parseInt(used.getText());
		Log.message("Used count is:" + +Used_count);
		SkySiteUtils.waitTill(3000);
		int Total_count = Avb_count + Used_count;
		Log.message("Total count is:" + +Total_count);
		SkySiteUtils.waitTill(3000);
		int Avl_Total_count = Integer.parseInt(total.getText());
		Log.message("Total count is:" + +Avl_Total_count);
		SkySiteUtils.waitTill(3000);
		if ((usage.isDisplayed()) && (Avl_Total_count == Total_count))
		{
			Log.message("Validation Successfull...");
			return true;
		}
		else
		{
			Log.message("Validation Failed...");
			return false;
		}
	}

	/**
	 * Method written for grid search with user name in manage user page Scripted
	 * By: Sekhar
	 * 
	 * @return
	 */

	public boolean gridSearchWithUserNameManageUser() 
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("Manage User button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		drpbtnasnpet.click();
		Log.message("Drop Down button has been clicked");
		SkySiteUtils.waitTill(2000);
		username.click();
		Log.message("User Name has been selected as search criteria");
		SkySiteUtils.waitTill(2000);
		// txtSerachValue.sendKeys("PWC HEALTH");
		txtSerachValue.sendKeys(PropertyReader.getProperty("UsernameSearchData"));
		Log.message("Search string has been sent");
		// txtSerachValue.sendKeys(PropertyReader.getProperty("EmpUserName"));
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		Log.message("Search button is clicked.");
		// SkySiteUtils.waitForElement(driver, userlistname, 20);
		SkySiteUtils.waitTill(5000);
		String username = userlistname.getText();
		Log.message("User Name is:" + " " + username);
		SkySiteUtils.waitTill(5000);
		if (username.trim().equals(PropertyReader.getProperty("UsernameSearchData")))
			return true;
		else
			return false;

	}

	/**
	 * Method written for grid search with Mail in manage user page Scripted By:
	 * Sekhar
	 * 
	 * @return
	 */

	public boolean gridSearchWithMailManageUser() 
	{

		SkySiteUtils.waitTill(3000);
		drpbtnasnpet.click();
		Log.message("Drop Down button has been clicked");
		SkySiteUtils.waitTill(2000);
		mail.click();
		Log.message("Mail has been selected as search criteria");
		SkySiteUtils.waitTill(2000);
		txtSerachValue.clear();
		txtSerachValue.sendKeys(PropertyReader.getProperty("MailsearchData"));
		// txtSerachValue.sendKeys(PropertyReader.getProperty("EmpUserName"));
		Log.message("Search string has been entered.");
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		Log.message("Search button is clicked.");
		SkySiteUtils.waitTill(5000);
		String Mailname = listmail.getText();
		System.out.println("Mail name is:" + Mailname);
		SkySiteUtils.waitTill(5000);
		if (Mailname.trim().equals(PropertyReader.getProperty("MailsearchData")))
			return true;
		else
			return false;
	}

	/**
	 * Method written for grid search with Phone in manage user page Scripted By:
	 * Sekhar
	 * 
	 * @return
	 */

	public boolean gridSearchWithPhoneManageUser() 
	{

		SkySiteUtils.waitTill(3000);
		drpbtnasnpet.click();
		Log.message("Drop Down button has been clicked");
		SkySiteUtils.waitTill(2000);
		phonework.click();
		Log.message("Phone been selected as search criteria");
		SkySiteUtils.waitTill(2000);
		txtSerachValue.clear();
		txtSerachValue.sendKeys(PropertyReader.getProperty("PhoneSearch"));
		Log.message("Search string has been entered.");
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		Log.message("Search button is clicked.");
		SkySiteUtils.waitTill(5000);
		String Mailname = listphonework.getText();
		System.out.println("Phone work is:" + Mailname);
		SkySiteUtils.waitTill(5000);
		if (Mailname.trim().equals(PropertyReader.getProperty("PhoneSearch")))
			return true;
		else
			return false;
	}

	/**
	 * Method written for grid search using license user with lite user in manage
	 * user page Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean gridSearch_UserLicense_LiteUser_ManageUser() 
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("Manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		drpbtnasnpet.click();
		Log.message("clicked on dropdown the user license.");
		SkySiteUtils.waitTill(2000);
		userlicense.click();
		Log.message("Select has been user license");
		SkySiteUtils.waitTill(2000);
		drpuserlicense.click();
		Log.message("clicked on dropdown the listuser");
		SkySiteUtils.waitTill(2000);
		liteuser.click();
		Log.message("clicked has been liteuser");
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		Log.message("Search button has been clicked.");
		SkySiteUtils.waitTill(5000);
		if (userlistname.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * Method written for grid search using license user with Shared user in manage
	 * user page Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean gridSearch_UserLicense_SharedUser_ManageUser() 
	{
		SkySiteUtils.waitTill(2000);
		drpuserlicense.click();
		Log.message("clicked on dropdown the listuser");
		SkySiteUtils.waitTill(2000);
		shareduser.click();
		Log.message("clicked has been shared user");
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		Log.message("Search button has been clicked.");
		SkySiteUtils.waitTill(5000);
		if (userlistname.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * Method written for grid search using license user with employee in manage
	 * user page Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean gridSearch_UserLicense_Employee_ManageUser() 
	{
		SkySiteUtils.waitTill(2000);
		drpuserlicense.click();
		Log.message("clicked on dropdown the listuser");
		SkySiteUtils.waitTill(2000);
		employee.click();
		Log.message("clicked has been employee");
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		Log.message("Search button has been clicked.");
		SkySiteUtils.waitTill(5000);
		if (userlistname.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * Method written for Search reset button should work properly in manage user
	 * page Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean search_Reset_ManageUser() 
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		drpbtnasnpet.click();
		Log.message("dropdown list clicked.");
		SkySiteUtils.waitTill(2000);
		username.click();
		Log.message("user name has been clicked.");
		SkySiteUtils.waitTill(2000);
		String usernameSearchData = PropertyReader.getProperty("UsernameSearchData");
		txtSerachValue.sendKeys(usernameSearchData);
		Log.message("Nagative User name has been entered.");
		SkySiteUtils.waitTill(2000);
		btnsearch.click();
		SkySiteUtils.waitTill(5000);
		resetsearch.click();
		Log.message("Reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		if (userlistname.isDisplayed() && txtSerachValue.getText().equalsIgnoreCase(""))
			return true;
		else
			return false;
	}

	/**
	 * Method written for user sorting with user name in manage user page 
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean user_Sort_ManageUser() 
	{
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);
		String Before_username = userlistname.getText();
		Log.message("Before user name is:" + Before_username);
		SkySiteUtils.waitTill(6000);
		sortascend.click();
		Log.message("ascending has been clicked.");
		SkySiteUtils.waitTill(5000);
		int usercount = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User count is:" + usercount);
		SkySiteUtils.waitTill(3000);
		String AfteruserName = driver
				.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + usercount + "]"))
				.getText();
		Log.message("After Ascending the user name is:" + AfteruserName);
		SkySiteUtils.waitTill(5000);
		if (Before_username.equals(AfteruserName))
			return true;
		else
			return false;
	}

	/**
	 * Method written for shared user Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean sharedUserManageUser()
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfshareduser.click();
		Log.message("shared user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String SharedName = randomSharedName.generateRandomValue();// Generating random string for first name
		Log.message(SharedName);
		dlffristname.sendKeys(SharedName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(SharedName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String userName = null;
		for (u = 1; u <= usr_count; u++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]")).getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (SharedName.equals(userName.trim()))
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		if (SharedName.equals(userName.trim()))
			return true;
		else
			return false;

	}

	/**
	 * Method written for Random Name(Employee) 
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Randam_User() 
	{

		String str = "Employee";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written for doing login Owner a/c with valid credential.
	 * Scripted By: Sekhar
	 * @throws AWTException
	 */
	public boolean loginWithOwnerUser(String uName5, String pWord5)
	{
		SkySiteUtils.waitTill(5000);
		 if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
		 {
			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		 }
		 SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		// String uName = PropertyReader.getProperty("Username");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName5);
		Log.message("Username: " + uName5 + " " + "has been engtered in Username text box.");
		// String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord5);
		Log.message("Password: " + pWord5 + " " + "has been engtered in Password text box.");
		SkySiteUtils.waitForElement(driver, buttonLogin, 60);
		buttonLogin.click();
		Log.message("LogIn button clicked.");

		SkySiteUtils.waitTill(5000);
		String Exptitle = "SKYSITE Facilities & Archive";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[1]/h4")).isDisplayed()) 
		{
			driver.findElement(By.xpath(".//*[@id='divAutoFeedback']/div/div/div[2]/div[1]/div[2]/div")).click();
			driver.findElement(By.xpath(".//*[@id='autofeedback']")).sendKeys("Hi");
			driver.findElement(By.xpath(".//*[@id='Savefeedback']")).click();
			SkySiteUtils.waitTill(8000);
		}
		Log.message("Login Validation:");
		if (Acttitle_AfterLogin.equals(Exptitle))
			return true;
		else
			return false;
	}

	/**
	 * Method written for owner a/c 
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean OwnerAccount_ValidateManageUser(String EmployeeName) 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String userName = null;
		Log.message("Employee Name is: " + EmployeeName);
		for (u = 1; u <= usr_count; u++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]")).getText();
			Log.message("user Name: " + userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim()))
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		if (EmployeeName.equals(userName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for doing login SecondAdmin a/c with valid credential.
	 * Scripted By: Sekhar
	 * @throws AWTException
	 */
	public boolean loginWithSecondAdminUser()
	{
		SkySiteUtils.waitTill(5000);
		 if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
		 {
			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		 }
		 SkySiteUtils.waitForElement(driver, txtBoxUserName, 30);
		String uName2 = PropertyReader.getProperty("Secondadminusername");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName2);
		Log.message("Username: " + uName2 + " " + "has been engtered in Username text box.");
		String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been engtered in Password text box.");
		SkySiteUtils.waitForElement(driver, buttonLogin, 20);
		buttonLogin.click();
		Log.message("LogIn button clicked.");
		SkySiteUtils.waitTill(5000);
		String Exptitle = "SKYSITE Facilities & Archive";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(5000);		
		if (driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[1]/h4")).isDisplayed()) 
		{
			driver.findElement(By.xpath(".//*[@id='divAutoFeedback']/div/div/div[2]/div[1]/div[2]/div")).click();
			driver.findElement(By.xpath(".//*[@id='autofeedback']")).sendKeys("Hi");
			driver.findElement(By.xpath(".//*[@id='Savefeedback']")).click();
			SkySiteUtils.waitTill(8000);
		}
		Log.message("Login Validation:");
		if (Acttitle_AfterLogin.equals(Exptitle))
			return true;
		else
			return false;
	}

	/**
	 * Method written for second admin a/c
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean SecondAdminAccount_validateManageUser(String EmployeeName) 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String userName = null;
		Log.message("Employee Name is: " + EmployeeName);
		for (u = 1; u <= usr_count; u++)
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]")).getText();
			Log.message("user Name: " + userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim()))
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		if (EmployeeName.equals(userName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Adding employee user for manage user Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean employeeManageUser(String uName1, String pWord1) throws AWTException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(5000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);

		int BeforeAvb_count = Integer.parseInt(availablecnt.getText());
		Log.message("Before available count:" + +BeforeAvb_count);

		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfempuser.click();
		Log.message("employee user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String EmployeeName = FnaHomePage.this.Randam_User();
		Log.message(EmployeeName);
		dlffristname.sendKeys(EmployeeName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EmployeeName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);

		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String userName = null;
		for (u = 1; u <= usr_count; u++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]")).getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim()))
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		int AfterAvb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +AfterAvb_count);
		SkySiteUtils.waitTill(5000);
		if ((EmployeeName.equals(userName.trim())) && (AfterAvb_count == (BeforeAvb_count - 1)))
		{
			Log.message("validated successfull!!!!");
		}
		this.logout();
		this.loginWithOwnerUser(uName1, pWord1);
		this.OwnerAccount_ValidateManageUser(EmployeeName);
		this.logout();
		this.loginWithSecondAdminUser();
		this.SecondAdminAccount_validateManageUser(EmployeeName);

		if (EmployeeName.equals(userName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for logout Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean logout() 
	{

		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnuserinfo, 30);
		clkprofile.click();
		Log.message("myprofile has been clicked.");
		SkySiteUtils.waitTill(3000);
		pfloutput.click();
		Log.message("log out button has been clicked.");
		SkySiteUtils.waitTill(5000);
		btnyes.click();
		Log.message("yes button has been clicked.");
		SkySiteUtils.waitTill(7000);
		String Exptitle = "Sign in - SKYSITE";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(5000);
		Log.message("Logout Validation:");
		SkySiteUtils.waitTill(5000);
		if (Acttitle_AfterLogin.equals(Exptitle))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Available count Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean avaliablecount_Managauser(String uName1, String pWord1) 
	{

		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);

		int BeforeAvb_count = Integer.parseInt(availablecnt.getText());
		Log.message("Before available count:" + +BeforeAvb_count);

		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfempuser.click();
		Log.message("employee user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String EmployeeName = FnaHomePage.this.Randam_User();
		Log.message(EmployeeName);
		dlffristname.sendKeys(EmployeeName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EmployeeName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int Avb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +Avb_count);
		SkySiteUtils.waitTill(5000);
		if (Avb_count == (BeforeAvb_count - 1))
		{
			Log.message("validated successfull!!!!");
		}
		this.logout();
		this.loginWithOwnerUser(uName1, pWord1);
		this.OwnerAccount_ValidatecountManageUser(BeforeAvb_count);
		this.logout();
		this.loginWithSecondAdminUser();
		this.SecondAdminAccount_validatecountManageUser(BeforeAvb_count);

		if (Avb_count == (BeforeAvb_count - 1))
			return true;
		else
			return false;
	}

	/**
	 * Method written for owner a/c Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean OwnerAccount_ValidatecountManageUser(int BeforeAvb_count)
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		Log.message("Before count is:" + BeforeAvb_count);
		int Avb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +Avb_count);
		SkySiteUtils.waitTill(5000);
		if ((Avb_count == (BeforeAvb_count - 1)))
		{
			Log.message("validated successfull!!!!");
		}

		if ((Avb_count == (BeforeAvb_count - 1)))
			return true;
		else
			return false;
	}

	/**
	 * Method written for second admin a/c Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean SecondAdminAccount_validatecountManageUser(int BeforeAvb_count) 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		Log.message("Before count is:" + BeforeAvb_count);
		int Avb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +Avb_count);
		SkySiteUtils.waitTill(5000);
		if ((Avb_count == (BeforeAvb_count - 1))) 
		{
			Log.message("validated successfull!!!!");
		}
		if ((Avb_count == (BeforeAvb_count - 1)))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Adding employee user for manage user in different a/c
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean Create_employee_sharedcontact_ManageUser() throws AWTException
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfempuser.click();
		Log.message("employee user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String EmployeeName = FnaHomePage.this.Randam_User();
		Log.message(EmployeeName);
		dlffristname.sendKeys(EmployeeName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EmployeeName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String userName = null;
		for (u = 1; u <= usr_count; u++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver
					.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]"))
					.getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim())) 
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		if (EmployeeName.equals(userName.trim()))
		{
			Log.message("validated successfull!!!!");
		}
		this.sharedUserManageUser();
		SkySiteUtils.waitTill(5000);

		if (EmployeeName.equals(userName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Adding employee user for manage user in different a/c
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean Create_contactuser_Addressbook() throws AWTException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btncontact, 30);
		btncontact.click();
		Log.message("contact button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnnewcontact.click();
		Log.message("Add new contact button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		radiocontact.click();
		Log.message("contact user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String ContactName = FnaHomePage.this.Random_ConactUser();
		Log.message(ContactName);
		dlffristname.sendKeys(ContactName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(ContactName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(5000);

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int contact_count = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("Contact name Count is:" + contact_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String cnctName = null;
		for (u = 1; u <= contact_count; u++) {
			// x path as dynamically providing all rows(prj) tr[prj]
			cnctName = driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]"))
					.getText();
			Log.message(cnctName);
			// Checking user name equal with each row of table
			if (ContactName.equals(cnctName.trim())) 
			{
				Log.message("Exp contact name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		if (ContactName.equals(cnctName.trim()))
		{
			Log.message("validated successfull!!!!");
		}
		if (ContactName.equals(cnctName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for shared user Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean sharedUserCountManageUser()
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int BeforeAvb_count = Integer.parseInt(availablecnt.getText());
		Log.message("Before available count:" + +BeforeAvb_count);
		SkySiteUtils.waitTill(3000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfshareduser.click();
		Log.message("shared user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String SharedName = randomSharedName.generateRandomValue();// Generating random string for first name
		Log.message(SharedName);
		dlffristname.sendKeys(SharedName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(SharedName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		int Avb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +Avb_count);
		SkySiteUtils.waitTill(5000);
		if (Avb_count == (BeforeAvb_count - 1)) 
		{
			Log.message("validated successfull!!!!");
		}
		if (Avb_count == (BeforeAvb_count - 1))
			return true;
		else
			return false;
	}

	/**
	 * Method written for doing login valid credential.
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean loginValidation() throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		String Exptitle = "SKYSITE Facilities & Archive";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(8000);
		Log.message("Login Validation:");
		/*
		 * int Feedback_Alert =
		 * driver.findElements(By.xpath("//input[@id='Savefeedback']")).size();//Check
		 * feedback alert comes
		 * Log.message("Checking feedback alert is there or not : "+Feedback_Alert);
		 * if(Feedback_Alert>0) {
		 * driver.findElement(By.xpath("//span[@id='btnlater']")).click();//Click on
		 * 'Ask me later' link
		 * Log.message("Clicked on 'Ask me later' link from feedback!!!"); }
		 */

		if (driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[1]/h4")).isDisplayed()) 
		{
			driver.findElement(By.xpath(".//*[@id='divAutoFeedback']/div/div/div[2]/div[1]/div[2]/div")).click();
			driver.findElement(By.xpath(".//*[@id='autofeedback']")).sendKeys("Hi");
			driver.findElement(By.xpath(".//*[@id='Savefeedback']")).click();
			SkySiteUtils.waitTill(8000);
		}
		if (Acttitle_AfterLogin.equals(Exptitle))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Adding employee user for manage user in owner a/c Scripted
	 * By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean Create_employee_EditUser_ManageUser() throws AWTException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfempuser.click();
		Log.message("employee user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String EmployeeName = FnaHomePage.this.Randam_User();
		Log.message(EmployeeName);
		dlffristname.sendKeys(EmployeeName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EmployeeName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int u = 0;
		String userName = null;
		for (u = 1; u <= usr_count; u++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + u + "]")).getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim())) 
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[5]/a[1])[" + u + "]")).click();
		Log.message("Exp edit button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		String EditName = FnaHomePage.this.Random_EditUser();
		Log.message(EditName);
		dlffristname.clear();
		Log.message("frist name has been cleared");
		SkySiteUtils.waitTill(3000);
		dlffristname.sendKeys(EditName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.clear();
		Log.message("email has been cleared");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EditName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle1);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count1 = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count1);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		String userEditName = null;
		for (i = 1; i <= usr_count1; i++)
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userEditName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + i + "]")).getText();
			Log.message(userEditName);
			// Checking user name equal with each row of table
			if (EditName.equals(userEditName.trim())) 
			{
				Log.message("Exp edit user name has been selected");
				break;
			}
		}
		if (EditName.equals(userEditName.trim()))
		{
			Log.message("validated successfull!!!!");
		}
		SkySiteUtils.waitTill(5000);
		this.logout();
		this.loginWithEmployeeUser();
		this.validate_EditUserManageUser(EditName);
		this.logout();
		this.loginWithSecondAdminUser();
		this.validate_EditUserManageUser(EditName);
		SkySiteUtils.waitTill(5000);
		if (EditName.equals(userEditName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for edit user Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean validate_EditUserManageUser(String EditName)
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		String userEditName = null;
		for (i = 1; i <= usr_count; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userEditName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + i + "]")).getText();
			Log.message(userEditName);
			// Checking user name equal with each row of table
			if (EditName.equals(userEditName.trim()))
			{
				Log.message("Exp edit user name has been selected");
				break;
			}
		}
		if (EditName.equals(userEditName.trim())) 
		{
			Log.message("validated successfull!!!!");
		}
		SkySiteUtils.waitTill(5000);
		if (EditName.equals(userEditName.trim()))
			return true;
		else
			return false;
	}

	/**
	 * Method written for edit employee user for manage user in second user a/c
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean Create_employee_EditUser_SecondUser_ManageUser(String uName1, String pWord1) throws AWTException
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnSetting, 30);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfempuser.click();
		Log.message("employee user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String EmployeeName = FnaHomePage.this.Randam_User();
		Log.message(EmployeeName);
		dlffristname.sendKeys(EmployeeName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EmployeeName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int m = 0;
		String userName = null;
		for (m = 1; m <= usr_count; m++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + m + "]")).getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim())) 
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[5]/a[1])[" + m + "]")).click();
		Log.message("Exp edit button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		String EditName = FnaHomePage.this.Random_EditUser();
		Log.message(EditName);
		dlffristname.clear();
		Log.message("frist name has been cleared");
		SkySiteUtils.waitTill(3000);
		dlffristname.sendKeys(EditName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.clear();
		Log.message("email has been cleared");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EditName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle1);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int usr_count1 = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count1);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int n = 0;
		String userEditName = null;
		for (n = 1; n <= usr_count1; n++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userEditName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + n + "]")).getText();
			Log.message(userEditName);
			// Checking user name equal with each row of table
			if (EditName.equals(userEditName.trim()))
			{
				Log.message("Exp edit user name has been selected");
				break;
			}
		}
		if (EditName.equals(userEditName.trim())) 
		{
			Log.message("validated successfull!!!!");
		}
		SkySiteUtils.waitTill(5000);
		this.logout();
		this.loginWithOwnerUser(uName1, pWord1);
		this.validate_EditUserManageUser(EditName);
		this.logout();
		this.loginWithEmployeeUser();
		this.validate_EditUserManageUser(EditName);
		SkySiteUtils.waitTill(5000);
		if (EditName.equals(userEditName.trim()))
			return true;
		else
			return false;

	}

	/**
	 * Method written for release user license for manage user Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean Create_employee_releaseuserlicense_ManageUser() throws AWTException
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);

		int BeforeAvb_count = Integer.parseInt(availablecnt.getText());
		Log.message("Before available count:" + +BeforeAvb_count);
		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfempuser.click();
		Log.message("employee user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String EmployeeName = FnaHomePage.this.Randam_User();
		Log.message(EmployeeName);
		dlffristname.sendKeys(EmployeeName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(EmployeeName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int Avb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +Avb_count);
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int m = 0;
		String userName = null;
		for (m = 1; m <= usr_count; m++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + m + "]")).getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (EmployeeName.equals(userName.trim())) 
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[5]/a[2])[" + m + "]")).click();
		Log.message("Exp release this license icon has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(3000);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(6000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int Act_count = Integer.parseInt(availablecnt.getText());
		Log.message("act available count:" + Act_count);
		SkySiteUtils.waitTill(5000);
		if (Act_count == BeforeAvb_count)
		{
			Log.message("Validation has been Successfully");
			return true;
		}
		else 
		{
			Log.message("Validation has been Failed");
			return false;
		}
	}

	/**
	 * Method written for release user license for manage user Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean Create_Shareduser_releaseuserlicense_ManageUser() throws AWTException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
		btnSetting.click();
		Log.message("Setting button has been clicked.");
		SkySiteUtils.waitTill(3000);
		btnManageUser.click();
		Log.message("manage user button has been clicked.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);

		int BeforeAvb_count = Integer.parseInt(availablecnt.getText());
		Log.message("Before available count:" + +BeforeAvb_count);
		SkySiteUtils.waitTill(5000);
		buttonNewUser.click();
		Log.message("Add button has been clicked.");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(3000);
		dlfshareduser.click();
		Log.message("shaed user radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		String SharedName = randomSharedName.generateRandomValue();// Generating random string for first name
		Log.message(SharedName);
		dlffristname.sendKeys(SharedName);
		Log.message("frist name has been entered");
		SkySiteUtils.waitTill(3000);
		dlfemail.sendKeys(SharedName + "@yopmail.com");
		Log.message("email has been entered");
		SkySiteUtils.waitTill(3000);
		dlfsavecls.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int Avb_count = Integer.parseInt(availablecnt.getText());
		Log.message("After available count:" + +Avb_count);
		SkySiteUtils.waitTill(3000);
		int usr_count = driver.findElements(By.xpath("//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2]")).size();
		Log.message("User Count is:" + usr_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int m = 0;
		String userName = null;
		for (m = 1; m <= usr_count; m++)
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			userName = driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[2])[" + m + "]")).getText();
			Log.message(userName);
			// Checking user name equal with each row of table
			if (SharedName.equals(userName.trim())) 
			{
				Log.message("Exp user name has been selected");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divUsersListGrid']/div[2]/table/tbody/tr/td[5]/a[2])[" + m + "]")).click();
		Log.message("Exp release this license icon has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(3000);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(6000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		int Act_count = Integer.parseInt(availablecnt.getText());
		Log.message("act available count:" + Act_count);
		SkySiteUtils.waitTill(5000);
		if (Act_count == BeforeAvb_count) 
		{
			Log.message("Validation has been Successfully");
			return true;
		} 
		else
		{
			Log.message("Validation has been Failed");
			return false;
		}
	}

	@FindBy(xpath = "//a[contains(text(),'Feedback')]")

	WebElement footfeedback;

	@FindBy(xpath = "//*[@id='txtDescription']")

	WebElement fdbackdes;

	@FindBy(css = "#fileinput")

	WebElement fdbackbrowse;

	@FindBy(css = "#btnCreate")

	WebElement smtfdback;

	@FindBy(css = ".form-group>small")

	WebElement notemsg;

	/**
	 * Method written for enter the data and max size in feedback Scripted By:
	 * Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean enterdata_Maxsize_feedback(String FolderPath) throws IOException
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		footfeedback.click();
		Log.message("feedback button has been clicked");
		SkySiteUtils.waitTill(3000);
		String description = PropertyReader.getProperty("Description11");
		fdbackdes.sendKeys(description);
		Log.message("description has been entered");
		SkySiteUtils.waitTill(10000);

		WebElement browse = driver.findElement(By.cssSelector(".btn.btn-file"));
		browse.click();
		Log.message("browse button has been clicked");
		SkySiteUtils.waitTill(20000);
		
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		
		for (File file : files)
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();
		
		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		
		SkySiteUtils.waitForElement(driver, notifymsg, 60);
		String Notify = notifymsg.getText();
		SkySiteUtils.waitTill(20000);
		
		try
		{
			File file = new File(tmpFileName);
			if (file.delete())
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e) 	
		{
			Log.message("Exception occured!!!" + e);
		}	
		
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		// Maximum file lenth for attachment: 4 MB
		if (Notify.equals("Maximum file lenth for attachment: 4 MB"))
		{
			Log.message("Validation Successfull");
			return true;
		} 
		else
		{
			Log.message("Validation failed");
			return false;
		}
	}

	/**
	 * Method written for thank you in feedback Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean thankyou_feedback() throws IOException 
	{
		driver.switchTo().defaultContent();
		footfeedback.click();
		Log.message("feedback button has been clicked");
		SkySiteUtils.waitTill(5000);
		String description = PropertyReader.getProperty("Description");
		fdbackdes.sendKeys(description);
		Log.message("description has been entered");
		SkySiteUtils.waitTill(10000);
		if (notemsg.isDisplayed())
		{
			Log.message("Note message displayed!!!");
		}
		else
		{
			Log.message("Note message NOT displayed!!!");
		}
		SkySiteUtils.waitTill(5000);
		smtfdback.click();
		Log.message("submit feedback has been clicked");
		SkySiteUtils.waitTill(5000);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		if (Notify.equals("Thank you for your feedback!"))
		{
			Log.message("Validation Successfull");
			return true;
		} 
		else 
		{
			Log.message("Validation failed");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='dvSelectedFileName']")

	WebElement Attachfil;

	/**
	 * Method written for Attchment file in feedback
	 *  Scripted By: Sekhar
	 * @return
	 * @throws IOException
	 */

	public boolean attachmentfile_feedback(String FolderPath) throws IOException
	{
		driver.switchTo().defaultContent();
		footfeedback.click();
		Log.message("feedback button has been clicked");
		SkySiteUtils.waitTill(3000);
		String description = PropertyReader.getProperty("Description");
		fdbackdes.sendKeys(description);
		Log.message("description has been entered");
		SkySiteUtils.waitTill(10000);
		WebElement browse = driver.findElement(By.cssSelector(".btn.btn-file"));
		browse.click();
		Log.message("browse button has been clicked");
		SkySiteUtils.waitTill(20000);		
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files)
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");		
		SkySiteUtils.waitTill(20000);

		try
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else 
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
		}		
		Log.message("File has been attached");
		SkySiteUtils.waitTill(3000);
		smtfdback.click();
		Log.message("submit feedback has been clicked");
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		if (Notify.equals("Thank you for your feedback!"))
		{
			Log.message("Validation Successfull");
			return true;
		} 
		else 
		{
			Log.message("Validation failed");
			return false;
		}
	}

	/**
	 * Method written for release user license for manage user Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean attachmentfile_harddrive_feedback() throws IOException 
	{
		driver.switchTo().defaultContent();
		footfeedback.click();
		Log.message("feedback button has been clicked");
		SkySiteUtils.waitTill(3000);
		String description = PropertyReader.getProperty("Description");
		fdbackdes.sendKeys(description);
		Log.message("description has been entered");
		SkySiteUtils.waitTill(10000);
		WebElement browse = driver.findElement(By.cssSelector(".btn.btn-file"));
		browse.click();
		Log.message("browse button has been clicked");
		SkySiteUtils.waitTill(20000);

		String Network = PropertyReader.getProperty("Network");
		Log.message("AutoIT Name is:" + Network);
		SkySiteUtils.waitTill(10000);
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(Network);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(20000);

		
		
		Log.message("File has been attached");
		SkySiteUtils.waitTill(3000);
		smtfdback.click();
		Log.message("submit feedback has been clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(2000);
		if (Notify.equals("Thank you for your feedback!")) 
		{
			Log.message("Validation Successfull");
			return true;
		} 
		else 
		{
			Log.message("Validation failed");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='setting']")

	WebElement drpSetting;

	@FindBy(xpath = "//*[@id='Collections']/a")

	WebElement clkcollection;

	@FindBy(xpath = "//*[@id='btnResetSearch']")

	WebElement resetcollection;

	@FindBy(xpath = "//span[@class='selectedTreeRow' and @style='padding-left: 5px; padding-right: 5px;']")

	WebElement foldername;

	@FindBy(xpath = "//*[@id='tbPrjSetting']/span/a")

	WebElement clneditsetting;

	/**
	 * Method written for select collection Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean selectcollection_Edit() 
	{
		SkySiteUtils.waitForElement(driver, drpSetting, 30);
		driver.switchTo().defaultContent();
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("prjCount_prjList :" + prjCount_prjList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= prjCount_prjList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(5000);
			String Collection_Name = PropertyReader.getProperty("CollectionName");
			SkySiteUtils.waitTill(5000);
			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name))
			{
				// Click on project name where it equal with specified project name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)[" + prj + "]")).click();
				Log.message("Select Collection edit button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(20000);
		driver.switchTo().defaultContent();
		if (clneditsetting.isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='tbPrjSetting']/span/a")

	WebElement btnprjsettings;

	@FindBy(xpath = "//*[@id='dashboardlable']")

	WebElement vrfylable;

	@FindBy(xpath = "//*[@id='liProjectDashboardSettings']/div[3]/button")

	WebElement selectfile;

	@FindBy(xpath = "//input[@id='RadioFilestructure' and @name='viewSelect']")

	WebElement defaultfiles;

	@FindBy(xpath = "//*[@id='H1']")

	WebElement modalheader;

	/**
	 * Method written for start up page Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean start_Up_Page_Default_View() throws IOException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		btnprjsettings.click();
		Log.message("setting button has been clicked");
		SkySiteUtils.waitTill(3000);

		if (vrfylable.isDisplayed())
		{
			Log.message("Default view displayed!!!!");
		}
		else
		{
			Log.message("Default view NOT displayed!!!!");
		}
		SkySiteUtils.waitTill(3000);
		if (defaultfiles.isDisplayed())
		{
			selectfile.click();
			Log.message("select file button has been clicked");
			return true;
		} 
		else 
		{
			Log.message("select file button NOT clicked");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='hyperlinkSearchText']")

	WebElement hyplinktxt;

	@FindBy(xpath = "//*[@id='hyperlinkSearchButton']")

	WebElement btnhyplinksearch;

	@FindBy(xpath = "//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4")

	WebElement searchfilname;

	@FindBy(xpath = "//*[@id='hyperlinkSearchClearButton']")

	WebElement btnhyplinksearchclear;

	/**
	 * Method written for Select file on search in start up page Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean start_Up_Page_desiredfile_select() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		hyplinktxt.clear();
		Log.message("hyp link text has been cleared");
		SkySiteUtils.waitTill(3000);
		String Filenamedoc = PropertyReader.getProperty("Filename111");
		hyplinktxt.sendKeys(Filenamedoc);
		Log.message("hyp link text has been entered:" + Filenamedoc);
		SkySiteUtils.waitTill(3000);
		btnhyplinksearch.click();
		Log.message("hyp link search button has been clicked");
		SkySiteUtils.waitTill(3000);
		String filename = searchfilname.getText();
		Log.message("File Name is:" + filename);
		SkySiteUtils.waitTill(3000);
		if (Filenamedoc.contains(filename)) 
		{
			Log.message("Search is Successfull with file name");
			return true;
		}
		else
		{
			Log.message("Search is UnSuccessfull with file name");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='hyperlinkSearchClearButton']")

	WebElement dltcross;

	@FindBy(xpath = "//*[@id='hyperLinkFolderList']/ul/li")

	WebElement sltfolder;

	/**
	 * Method written for Delete the Search contain by click on Cross button
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean Delete_Search_Cross() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		dltcross.click();
		Log.message("delete cross button has been clicked");
		SkySiteUtils.waitTill(3000);
		if (sltfolder.isDisplayed()) 
		{
			Log.message("delete cross button has been clicked");
			return true;
		} 
		else
		{
			Log.message("delete cross button has NOT been clicked");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[3]/h4")

	WebElement sltfolder1;

	@FindBy(xpath = "//*[@id='linkPagebar']")

	WebElement linkpaginabar;

	/**
	 * Method written for pagination in selected folder. Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean pagination_SelectFolder() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		String File_Count = driver.findElement(By.xpath("//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[3]/small")).getText();// Getting label counts
		Log.message("count is: " + File_Count); // Getting Lite user counts
		SkySiteUtils.waitTill(5000);

		String[] x = File_Count.split(" ");
		String Avl_Count = x[2];
		Avl_Count = Avl_Count.replace("|", "");
		Log.message("Remove count is:" + Avl_Count); // Getting Lite user counts

		int CountAfter = Integer.parseInt(Avl_Count);
		Log.message("Avl count is:" + CountAfter); // Getting Lite user counts

		SkySiteUtils.waitTill(5000);
		sltfolder1.click();
		Log.message("select folder has been clicked");
		SkySiteUtils.waitTill(3000);
		if (linkpaginabar.isDisplayed()) 
		{
			Log.message("pagination bar has been displayed");
			return true;
		} 
		else
		{
			Log.message("delete cross button has NOT been clicked");
			return false;
		}
	}

	@FindBy(xpath = "//i[@class='icon icon-left-arrow']")

	WebElement leftarrow;

	@FindBy(xpath = "(//i[@class='icon icon-folder-close ico-3x icon-yellow'])[1]")

	WebElement foldericon;

	/**
	 * Method written for go back from folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean goback_Fromfile() throws IOException
	{
		SkySiteUtils.waitTill(5000);
		sltfolder1.click();
		Log.message("Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		if (leftarrow.isDisplayed())
		{
			Log.message("left arrow button has been displayed");
		} 
		else
		{
			Log.message("left arrow button has NOT been displayed");
		}
		SkySiteUtils.waitTill(5000);
		leftarrow.click();
		Log.message("Left arrow button has been clicked");
		SkySiteUtils.waitTill(5000);
		if (foldericon.isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='hyperLinkFolderList']/ul/li/div/div[3]/h4")

	WebElement setfoldername;

	/**
	 * Method written for Search with foldername and file name Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean Search_withfolderandfile() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		String Foldername = PropertyReader.getProperty("FolderNameSearch");
		hyplinktxt.sendKeys(Foldername);
		Log.message("Folder name has been entered:" + Foldername);
		SkySiteUtils.waitTill(5000);
		btnhyplinksearch.click();
		Log.message("search button has been clicked");
		SkySiteUtils.waitTill(3000);
		String setFoldername = setfoldername.getText();
		Log.message("Set folder name is:" + setFoldername);
		SkySiteUtils.waitTill(3000);
		if (Foldername.contains(setFoldername)) 
		{
			Log.message("Search with folder name Successful");
		} 
		else 
		{
			Log.message("Search with folder name UnSuccessful");
		}
		SkySiteUtils.waitTill(5000);
		btnhyplinksearchclear.click();
		Log.message("Delete the cross button has been clicked");
		SkySiteUtils.waitTill(3000);

		String Filename = PropertyReader.getProperty("Filename111");
		hyplinktxt.sendKeys(Filename);
		Log.message("Folder name has been entered:" + Filename);
		SkySiteUtils.waitTill(5000);
		btnhyplinksearch.click();
		Log.message("search button has been clicked");
		SkySiteUtils.waitTill(3000);
		String setFilename = searchfilname.getText();
		Log.message("Set file name is:" + setFilename);
		SkySiteUtils.waitTill(3000);
		if (Filename.contains(setFilename)) 
		{
			Log.message("Search with file name Successful");
			return true;
		} 
		else
		{
			Log.message("Search with file name UnSuccessful");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='savedashboard']")

	WebElement btnset;

	@FindBy(xpath = "//*[@id='liProjectDashboardSettings']/div[3]/button")

	WebElement btnchangestart;

	@FindBy(xpath = "//*[@id='deletedashboard']")

	WebElement btnsetdelete;

	@FindBy(xpath = "//p[@id='dashboardPath' and @ data-original-title='//Folder_123/My6@Sheets.pdf']")

	WebElement lnkPath;

	@FindBy(xpath = "//*[@id='dashboardPath']")

	WebElement nofileslt;

	/**
	 * Method written for change start file Option Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean changestartfile_Option() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		searchfilname.click();
		Log.message("File name has been clicked");
		SkySiteUtils.waitTill(5000);
		btnset.click();
		Log.message("set button has been clicked");
		SkySiteUtils.waitTill(5000);
		if (btnchangestart.isDisplayed() && (lnkPath.isDisplayed()))
		{
			Log.message("change start file button has been displayed");
		} 
		else
		{
			Log.message("change start file button has NOT been displayed");
		}
		SkySiteUtils.waitTill(5000);
		btnchangestart.click();
		Log.message("change start file button has been clicked");
		SkySiteUtils.waitTill(5000);
		btnsetdelete.click();
		Log.message("delete button has been clicked");
		SkySiteUtils.waitTill(5000);
		if (nofileslt.isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='RadioFilestructure']")

	WebElement btnDefaultradio;

	@FindBy(xpath = "//*[@id='button-1']")

	WebElement yesbtn;

	/**
	 * Method written for change default view Scripted By: Sekhar
	 * 
	 * @return
	 * @throws IOException
	 */

	public boolean change_default_view() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		searchfilname.click();
		Log.message("File name has been clicked");
		SkySiteUtils.waitTill(5000);
		btnset.click();
		Log.message("set button has been clicked");
		SkySiteUtils.waitTill(5000);
		if (btnchangestart.isDisplayed() && (lnkPath.isDisplayed()))
		{
			Log.message("change start file button has been displayed");
		}
		else
		{
			Log.message("change start file button has NOT been displayed");
		}
		SkySiteUtils.waitTill(5000);
		btnDefaultradio.click();
		Log.message("Folder and file radio button has been clicked");
		SkySiteUtils.waitTill(3000);
		yesbtn.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(3000);
		String Act_selectf = selectfile.getText();
		Log.message("Act Select file Message is:" + Act_selectf);
		SkySiteUtils.waitTill(3000);
		String EXp_selectfile_Msg = PropertyReader.getProperty("Selectfilemsg");
		Log.message("Exp Select file Message is:" + EXp_selectfile_Msg);
		if (Act_selectf.contains(EXp_selectfile_Msg))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[2]")

	WebElement imgviewer;

	@FindBy(xpath = "//*[@id='pages_label']")

	WebElement srtpagecount;

	/**
	 * Method written for select collection and document 1stpage as start file
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean select_collection_document_1stpage_startfile() throws IOException
	{
		SkySiteUtils.waitForElement(driver, drpSetting, 30);
		driver.switchTo().defaultContent();
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collCount_collList :" + collCount_collList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= collCount_collList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(collName);
			SkySiteUtils.waitTill(5000);
			String Collection_Name = PropertyReader.getProperty("CollectionName1");
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.equals(Collection_Name)) 
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(20000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, imgviewer, 30);
		int imgcount = driver.findElements(By.xpath("//*[@id='pages_label']")).size();
		Log.message("Image page count is:" + imgcount);
		SkySiteUtils.waitTill(5000);
		if (imgcount == 1) 
		{
			Log.message("Document 1st page as start file Successfull");
			return true;
		} 
		else 
		{
			Log.message("Document 1st page as start file UnSuccessfull");
			return false;
		}
	}

	/**
	 * Method written for select collection and document view from employee Scripted
	 * By: Sekhar
	 * 
	 * @return
	 */
	public boolean select_collection_document_view_Employee() throws IOException 
	{
		SkySiteUtils.waitForElement(driver, drpSetting, 30);
		driver.switchTo().defaultContent();
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collCount_collList :" + collCount_collList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= collCount_collList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(collName);
			SkySiteUtils.waitTill(5000);
			String Collection_Name = PropertyReader.getProperty("CollectionName1");
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.equals(Collection_Name)) 
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(20000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, imgviewer, 30);
		int imgcount = driver.findElements(By.xpath("//*[@id='pages_label']")).size();
		Log.message("Image page count is:" + imgcount);
		SkySiteUtils.waitTill(5000);
		if (imgcount == 1) 
		{
			SkySiteUtils.waitTill(3000);
			driver.close();
			Log.message("Close the open tab Successfull");
			SkySiteUtils.waitTill(3000);
			driver.switchTo().window(parentHandle);// Switch back to folder page
			SkySiteUtils.waitTill(5000);
			Log.message("Document view as start file Successfull");
			return true;
		} 
		else 
		{
			Log.message("Document view as start file UnSuccessfull");
			return false;
		}
	}

	/**
	 * Method written for select collection and document view from lite user
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */

	public boolean select_collection_document_view_Liteuser() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collCount_collList :" + collCount_collList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= collCount_collList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(collName);
			SkySiteUtils.waitTill(5000);
			String Collection_Name = PropertyReader.getProperty("CollectionName1");
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.equals(Collection_Name)) 
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(20000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, imgviewer, 30);
		int imgcount = driver.findElements(By.xpath("//*[@id='pages_label']")).size();
		Log.message("Image page count is:" + imgcount);
		SkySiteUtils.waitTill(5000);
		if (imgcount == 1) 
		{
			// driver.switchTo().window(parentHandle);//Switch back to folder page
			SkySiteUtils.waitTill(5000);
			Log.message("Document 1st page as start file Successfull");
			return true;
		} 
		else 
		{
			Log.message("Document 1st page as start file UnSuccessfull");
			return false;
		}
	}

	/**
	 * Method written for Random Name(Employee) Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Collectionname() 
	{

		String str = "Collect";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written for Random Folder Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Foldername() 
	{

		String str = "Folder";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written for Random Rename Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Rename() 
	{

		String str = "Rename";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written for Random AlbumName Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Albumname() 
	{

		String str = "Album";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method written for Random Sub Folder Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_SubFoldername() 
	{

		String str = "SubFolder";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	@FindBy(xpath = "//*[@id='btnNewProject']")

	WebElement btncollection;

	@FindBy(xpath = "//*[@id='txtProjectName']")

	WebElement txtProjectName;

	@FindBy(xpath = "//*[@id='txtProjectNumber']")

	WebElement txtProjectNumber;

	@FindBy(css = "#txtProjectStartDate")

	WebElement txtProjectStartDate;

	@FindBy(css = ".next")

	WebElement nxtdatemonth;

	@FindBy(xpath = "(//td[@class='day'])[8]")

	WebElement sltdate;

	@FindBy(css = "#txtProjectDesc")

	WebElement txtProjectDesc;

	@FindBy(css = "#txtProjectAddress1")

	WebElement txtProjectAddress1;

	@FindBy(css = "#txtProjectCity")

	WebElement txtProjectCity;

	@FindBy(css = "#txtProjectZip")

	WebElement txtProjectZip;

	@FindBy(css = "#chkFav")

	WebElement chkFav;

	@FindBy(css = "#btnSave")

	WebElement btnSave;

	@FindBy(css = ".selectedTreeRow")

	WebElement selectedTreeRow;

	@FindBy(xpath = "//*[@id='btnProjectListSearch']")

	WebElement spaHeader;

	@FindBy(css = "#btnFavSearch")

	WebElement btnFavSearch;

	@FindBy(xpath = "(//i[@class='icon icon-favourite icon-green ico-lg'])[1]")

	WebElement icongreen;

	/**
	 * Method written for create collection Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Create_Collection_Favourite() throws IOException
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		btncollection.click();
		Log.message("Add collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		String CollectionName = FnaHomePage.this.Random_Collectionname();
		Log.message(CollectionName);
		txtProjectName.sendKeys(CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(CollectionName);
		Log.message("Collection number is:" + CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(5000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(5000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);
		String Descrip = PropertyReader.getProperty("Description");
		txtProjectDesc.sendKeys(Descrip);
		SkySiteUtils.waitTill(5000);
		txtProjectAddress1.sendKeys("salt lake");
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectCity.sendKeys("Kolkata");
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectZip.sendKeys("700091");
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(5000);
		Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText("USA");
		Log.message("state has been selected");
		SkySiteUtils.waitTill(5000);
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText("California");
		Log.message("state has been selected");
		SkySiteUtils.waitTill(5000);
		chkFav.click();
		Log.message("add favorite check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(CollectionName))
		{
			Log.message("Collection create successfull");
			SkySiteUtils.waitTill(2000);
		}
		else
		{
			Log.message("Collection create Unsuccessfull");
			SkySiteUtils.waitTill(2000);
		}
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(2000);
		spaHeader.click();
		Log.message("Header has been clicked");
		SkySiteUtils.waitTill(2000);
		btnFavSearch.click();
		Log.message("fav search button has been clicked");
		SkySiteUtils.waitTill(2000);
		int Collectio_Count = driver.findElements(By.xpath("//*[@id='plctrl_divProjectList']/ul/li/a/span/label"))
				.size();
		Log.message("Collection Count is:" + Collectio_Count);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= Collectio_Count; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span/label)[" + prj + "]"))
					.getText();
			Log.message("Collection name is:" + collName);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.contains(CollectionName)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		if (icongreen.isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//span[@class='filter-option pull-left'])[3]")

	WebElement filtercount;

	@FindBy(xpath = "//*[@id='divPaginationBelow']/div[2]/div/div[1]")

	WebElement paginationcount;

	@FindBy(xpath = "//*[@id='divPaginationBelow']/div[2]/div/div[3]/a/i")

	WebElement btnpagifarrow;

	@FindBy(xpath = "//*[@id='divPaginationBelow']/div[2]/div/div[2]/a/i")

	WebElement btnpagiback;

	@FindBy(xpath = "//*[@id='txtCurPage_GridBottom']")

	WebElement beforepagi;

	/**
	 * Method written for pagination in collections Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean pagination_Collection() throws IOException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);

		String pagecount = driver.findElement(By.xpath("(//span[@class='filter-option pull-left'])[3]")).getText();
		int Afterpagecount = Integer.parseInt(pagecount);
		Log.message("per page count is:" + Afterpagecount);

		String File_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();// Getting
																														// label
																														// counts
		Log.message("count is: " + File_Count); // Getting counts

		SkySiteUtils.waitTill(5000);
		String[] x = File_Count.split(" ");
		String Avl_Count = x[3];
		int CountAfter = Integer.parseInt(Avl_Count);
		Log.message("Total Avl count is:" + CountAfter); // Getting counts
		if (Afterpagecount < CountAfter) 
		{
			Log.message("Pagination is requried");
		} 
		else 
		{
			Log.message("Pagination is NOT requried");
		}
		String beforecount = driver.findElement(By.xpath("//div/div/div[2]/div[1]/div[2]/div/div[2]/div/div[1]/input"))
				.getAttribute("value");
		int CountB = Integer.parseInt(beforecount);
		Log.message("before count is:" + CountB);
		SkySiteUtils.waitTill(5000);
		btnpagifarrow.click();
		Log.message("pagination forrowed button has been clicked.");
		SkySiteUtils.waitTill(10000);
		String Aftercount = driver.findElement(By.xpath("//div/div/div[2]/div[1]/div[2]/div/div[3]/div/div[1]/input"))
				.getAttribute("value");
		int CountA = Integer.parseInt(Aftercount);
		Log.message("After count is:" + CountA);
		if (btnpagiback.isDisplayed() && (CountA == (CountB + 1)))
			return true;
		else
			return false;										
	}

	/**
	 * Method written for create collections
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Create_Collections(String CollectionName) throws IOException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		
		SkySiteUtils.waitForElement(driver, btncollection, 30);
		btncollection.click();
		Log.message("Add collection has been clicked.");
		SkySiteUtils.waitTill(7000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(2000);
		// String CollectionName=FnaHomePage.this.Random_Collectionname();
		Log.message(CollectionName);
		txtProjectName.sendKeys(CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(CollectionName);
		Log.message("Collection number is:" + CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(3000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(3000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);
		String Descrip = PropertyReader.getProperty("Description");
		txtProjectDesc.sendKeys(Descrip);
		SkySiteUtils.waitTill(2000);
		txtProjectAddress1.sendKeys("salt lake");
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectCity.sendKeys("Kolkata");
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectZip.sendKeys("700091");
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(5000);
		/*Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText("USA");
		SkySiteUtils.waitTill(5000);
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText("California");
		Log.message("state has been selected");
		SkySiteUtils.waitTill(3000);*/
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(2000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		//SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(CollectionName)) 
		{
			Log.message("Create Collection successfull" + CollectionName);
			SkySiteUtils.waitTill(2000);
			return true;
		}
		else
		{
			Log.message("Create Collection Unsuccessfull" + CollectionName);
			SkySiteUtils.waitTill(2000);
			return false;
		}
	}

	@FindBy(css = "#ProjectMenu1_Teams>a")

	WebElement btnteam;

	@FindBy(css = "#Button1")

	WebElement moreoption;

	@FindBy(xpath = "(//i[@class='icon icon-add-teamMemberAddress'])[3]")

	WebElement teamMemberAddress;

	@FindBy(css = "#btnAddClose")

	WebElement btnAddClose;

	@FindBy(css = ".noty_text")

	WebElement Notify_msg;

	/**
	 * Method written for adding Contact from address book in teams Scripted By:
	 * Sekhar
	 * 
	 * @return
	 */
	public boolean adding_contact_from_address_book(String Employeename) throws IOException
	{

		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		btnteam.click();
		Log.message("Teams button has been clicked");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		moreoption.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(2000);
		teamMemberAddress.click();
		Log.message("add team fron address has been clicked");
		SkySiteUtils.waitTill(5000);

		int contact_count = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("contact Count is:" + contact_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= contact_count; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String ContactName = driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])[" + i + "]")).getText();
			Log.message("Collection name is:" + ContactName);
			SkySiteUtils.waitTill(5000);
			Log.message("Employee name is:" + Employeename);
			// Checking collection name equal with each row of table
			if (ContactName.trim().equalsIgnoreCase(Employeename)) 
			{
				Log.message("Exp Contact name has been validated!!!");
				break;
			}
		}
		driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img)[" + i + "]")).click();
		Log.message("Select exp contact has been clicked");
		SkySiteUtils.waitTill(5000);
		btnAddClose.click();
		Log.message("Add&close button has been clicked");
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='divTeamGridData']/div[2]/table/tbody/tr[3]/td[2]")).isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(css = "#Button1")

	WebElement acceptescollection;

	@FindBy(css = "#btnAccept")

	WebElement btnAccept;

	/**
	 * Method written for Accepted Collection flag from employee Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Accepted_Collections_Flag(String CollectionName) throws IOException 
	{

		SkySiteUtils.waitTill(5000);

		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++)
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[5]/a/i)[" + i + "]")).click();
		Log.message("Exp collection flag has been clicked");
		SkySiteUtils.waitTill(3000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		btnAccept.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		if (Notify.equals("Your response saved successfully"))
			return true;
		else
			return false;
	}

	@FindBy(css = ".icon.icon-collection.ico-lg")

	WebElement btncollections;

	/**
	 * Method written for edit Collection Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Edit_Collection(String CollectionName, String Edit_Colle_Name, String Descrip, String Address,
			String city, String Zip, String Country, String state) throws IOException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName))
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a/i)[" + i + "]")).click();
		Log.message("Exp Edit icon button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		txtProjectName.clear();
		Log.message("Collection name has been cleared");
		SkySiteUtils.waitTill(2000);
		Log.message("Edit Collection name is:" + Edit_Colle_Name);
		SkySiteUtils.waitTill(2000);
		txtProjectName.sendKeys(Edit_Colle_Name);
		Log.message("Collection name has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.clear();
		Log.message("Collection number has been cleared");
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(Edit_Colle_Name);
		Log.message("Collection number has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(5000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(5000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);
		// String Descrip = PropertyReader.getProperty("CollectionDesc");
		txtProjectDesc.clear();
		Log.message("Description has been cleared");
		SkySiteUtils.waitTill(5000);
		txtProjectDesc.sendKeys(Descrip);
		Log.message("Description has been entered");
		SkySiteUtils.waitTill(5000);
		// String Address = PropertyReader.getProperty("CollectionAddress");
		txtProjectAddress1.clear();
		txtProjectAddress1.sendKeys(Address);
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		// String city = PropertyReader.getProperty("Collectioncity");
		txtProjectCity.clear();
		txtProjectCity.sendKeys(city);
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		// String Zip = PropertyReader.getProperty("CollectionZip");
		txtProjectZip.clear();
		txtProjectZip.sendKeys(Zip);
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(2000);
		// String Country = PropertyReader.getProperty("CollectionCountry");
		Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText(Country);
		Log.message("Country has been entered");
		SkySiteUtils.waitTill(5000);
		// String state = PropertyReader.getProperty("CollectionState");
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText(state);
		Log.message("state has been selected");
		SkySiteUtils.waitTill(5000);
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);

		int collectioncheck = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("collection check box Count is:" + collectioncheck);
		// Loop start '2' as projects list start from tr[2]
		int j = 0;
		for (j = 1; j <= collectioncheck; j++)
		{
			// x path as dynamically providing all rows(j) tr[j]
			String Edit_Col_name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + j + "]")).getText();
			Log.message("Exp Edit Collec name is:" + Edit_Col_name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Edit Collection name is:" + Edit_Colle_Name);
			// Checking collection name equal with each row of table
			if (Edit_Col_name.contains(Edit_Colle_Name))
			{
				Log.message("Exp Edit Collection name has been validated!!!");
				break;
			}
		}
		String Edit_coll = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + j + "]")).getText();
		Log.message("After Edit Collection name is:" + Edit_coll);
		SkySiteUtils.waitTill(5000);
		if (Edit_Colle_Name.contains(Edit_coll))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Folder sorting default in Collection Setting 
	 * Scripted By: Sekhar 
	 * @return
	 */
	public boolean Folder_sorting_Collection_Setting(String CollectionName) throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++)
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName))
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a/i)[" + i + "]")).click();
		Log.message("Exp Edit icon button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		clneditsetting.click();
		Log.message("Setting button has been clicked");
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='radName']")).isSelected())
			return true;
		else
			return false;
	}

	@FindBy(css = "#ddlProjectStatus")
	WebElement ddlProjectStatus;

	/**
	 * Method written for Current Collection Status in collection Setting Scripted
	 * By: Sekhar
	 * 
	 * @return
	 */
	public boolean Current_Collection_Status_Setting(String CollectionName) throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName))
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a/i)[" + i + "]")).click();
		Log.message("Exp Edit icon button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		clneditsetting.click();
		Log.message("Setting button has been clicked");
		SkySiteUtils.waitTill(5000);
		Select drpActive = new Select(driver.findElement(By.id("ddlProjectStatus")));
		drpActive.selectByVisibleText("Active");
		Log.message("Active has been selected");
		SkySiteUtils.waitTill(5000);
		Select drpOnHold = new Select(driver.findElement(By.id("ddlProjectStatus")));
		drpOnHold.selectByVisibleText("On Hold");
		Log.message("On Hold has been selected");
		SkySiteUtils.waitTill(5000);
		Select drpComplete = new Select(driver.findElement(By.id("ddlProjectStatus")));
		drpComplete.selectByVisibleText("Complete");
		Log.message("Complete has been selected");
		SkySiteUtils.waitTill(5000);
		if (ddlProjectStatus.isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//input[@id='Button1']")

	WebElement btnButton1;

	/**
	 * Method written for Accepted Collection from employee Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Accepted_Collections(String CollectionName, String CollectionName1) throws IOException {

		SkySiteUtils.waitTill(5000);

		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName))
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)[" + i + "]")).click();
		Log.message("Exp collection check box has been clicked");
		SkySiteUtils.waitTill(5000);
		int collectioncheck_count1 = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count1);
		// Loop start '2' as projects list start from tr[2]
		int j = 0;
		for (j = 1; j <= collectioncheck_count1; j++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name1 = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + j + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name1);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName1);
			// Checking collection name equal with each row of table
			if (Collection_Name1.contains(CollectionName1)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)[" + j + "]")).click();
		Log.message("Exp collection check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnButton1.click();
		Log.message("accept selected collections button has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		if(Notify.equalsIgnoreCase("Your response successfully saved for the selected collection."))
			return true;
		else
			return false;
	}

	@FindBy(css = ".dhxgrid_sort_asc")

	WebElement sortasc;

	@FindBy(css = ".dhxgrid_sort_desc")

	WebElement sortdesc;

	@FindBy(xpath = "//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[3]/a")

	WebElement fristcoll;

	/**
	 * Method written for sorting collection name Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Sorting_Collectionname_Collectionlist() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath(".//*[@id='listGrid']/div[1]/table/tbody/tr[2]/td[3]/div")).click();
		SkySiteUtils.waitTill(3000);		
		String Before_collname = fristcoll.getText();
		Log.message("Before collection name is:" + Before_collname);
		SkySiteUtils.waitTill(6000);
		sortasc.click();
		Log.message("ascending has been clicked.");
		SkySiteUtils.waitTill(5000);
		int usercount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("User count is:" + usercount);
		SkySiteUtils.waitTill(3000);
		String AftercollName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + usercount + "]")).getText();
		Log.message("After Ascending the collection name is:" + AftercollName);
		SkySiteUtils.waitTill(5000);
		if ((sortdesc.isDisplayed()) && (Before_collname.equals(AftercollName)))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='listGrid']/div[1]/table/tbody/tr[2]/td[2]")

	WebElement clkfavorite;

	/**
	 * Method written for sorting favorite Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean sorting_favorite_Collectionlist() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		//clkfavorite.click();
		//Log.message("favorite list has been clicked");
		//SkySiteUtils.waitTill(8000);
		int usercount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("User count is:" + usercount);
		SkySiteUtils.waitTill(3000);
		String BeforecollName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[1]")).getText();
		Log.message("Before the collection name is:" + BeforecollName);
		SkySiteUtils.waitTill(8000);
		sortasc.click();
		Log.message("ascending has been clicked.");
		SkySiteUtils.waitTill(5000);
		String After_collname = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+usercount+"]")).getText();
		Log.message("After ascending collection name is:" + After_collname);
		SkySiteUtils.waitTill(5000);

		if ((sortdesc.isDisplayed()) && (After_collname.equals(BeforecollName)))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='listGrid']/div[1]/table/tbody/tr[2]/td[4]")

	WebElement clkcllnumber;

	@FindBy(xpath = "//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[4]/a")

	WebElement collnum;

	/**
	 * Method written for sorting collection number 
	 * Scripted By: Sekhar
	 * @return
	 */
	public boolean Sorting_Collectionnumber_Collectionlist() throws IOException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		clkcllnumber.click();
		Log.message("collection number list has been clicked");
		SkySiteUtils.waitTill(8000);

		String Before_collname = collnum.getText();
		Log.message("Before collection number is:" + Before_collname);
		SkySiteUtils.waitTill(6000);
		sortasc.click();
		Log.message("ascending has been clicked.");
		SkySiteUtils.waitTill(5000);
		int usercount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("User count is:" + usercount);
		SkySiteUtils.waitTill(3000);
		String AftercollName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[4]/a)[" + usercount + "]")).getText();
		Log.message("After Ascending the collection number is:" + AftercollName);
		SkySiteUtils.waitTill(5000);
		if ((sortdesc.isDisplayed()) && (Before_collname.equals(AftercollName)))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='listGrid']/div[1]/table/tbody/tr[2]/td[9]")

	WebElement clkstorage;

	/**
	 * Method written for sorting storage Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean sorting_Storage_Collectionlist() throws IOException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		btncollections.click();
		Log.message("collections button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		clkstorage.click();
		Log.message("storage list has been clicked");
		SkySiteUtils.waitTill(8000);
		int usercount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("User count is:" + usercount);
		SkySiteUtils.waitTill(3000);
		String BeforecollName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + usercount + "]")).getText();
		Log.message("Before the collection name is:" + BeforecollName);
		SkySiteUtils.waitTill(8000);
		sortasc.click();
		Log.message("ascending has been clicked.");
		SkySiteUtils.waitTill(5000);
		String After_collname = fristcoll.getText();
		Log.message("After ascending collection name is:" + After_collname);
		SkySiteUtils.waitTill(5000);
		if ((sortdesc.isDisplayed()) && (After_collname.equals(BeforecollName)))
			return true;
		else
			return false;
	}

	/**
	 * Method written for adding Contact employee from address book in teams
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean adding_contactemployee_from_address_book(String Employeename) throws IOException 
	{

		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		btnteam.click();
		Log.message("Teams button has been clicked");
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		moreoption.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(2000);
		teamMemberAddress.click();
		Log.message("add team fron address has been clicked");
		SkySiteUtils.waitTill(2000);

		int contact_count = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("contact Count is:" + contact_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= contact_count; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String ContactName = driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])[" + i + "]")).getText();
			Log.message("Contact name is:" + ContactName);
			SkySiteUtils.waitTill(5000);
			Log.message("Employee name is:" + Employeename);
			// Checking collection name equal with each row of table
			if (ContactName.contains(Employeename)) 
			{
				Log.message("Exp Contact name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img)[" + i + "]")).click();
		Log.message("Select exp contact has been clicked");
		SkySiteUtils.waitTill(5000);
		btnAddClose.click();
		Log.message("Add&close button has been clicked");
		SkySiteUtils.waitTill(3000);
		if (driver.findElement(By.xpath("//*[@id='divTeamGridData']/div[2]/table/tbody/tr[3]/td[2]")).isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//button[@id='btnRemoveProject']")

	WebElement btnRemoveProject;

	/**
	 * Method written for Accepted Collection from employee Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Accepted_Collection_FromEmployeee(String CollectionName, String uName, String pWord, String uName1,
			String pWord1) throws IOException 
	{

		SkySiteUtils.waitTill(5000);

		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)[" + i + "]")).click();
		Log.message("Exp collection check box has been clicked");
		SkySiteUtils.waitTill(5000);
		btnButton1.click();
		Log.message("accept selected collections button has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		if (Notify.equals("Your response successfully saved for the selected collection.")) 
		{
			Log.message("Exp Accepted collection invitation successfull");
			SkySiteUtils.waitTill(5000);
		}
		else
		{
			Log.message("Exp Accepted collection invitation Unsuccessfull");
			SkySiteUtils.waitTill(5000);
		}
		SkySiteUtils.waitTill(5000);
		clkcollection.click();
		Log.message("collections has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		String File_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();// Getting label counts
		Log.message("count is: " + File_Count); // Getting counts
		SkySiteUtils.waitTill(5000);
		String[] x = File_Count.split(" ");
		String Avl_Count = x[3];
		int Before_Count = Integer.parseInt(Avl_Count);
		Log.message("Delete Before Total Avl count is:" + Before_Count); // Getting counts

		this.logout();
		this.loginWithOwnerUser(uName, pWord);

		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		int collection_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collection_count);
		// Loop start '2' as projects list start from tr[2]
		int j = 0;
		for (j = 1; j <= collection_count; j++)
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + j + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img)[" + j + "]")).click();
		Log.message("Exp Check box has been clicked");
		SkySiteUtils.waitTill(5000);
		btnRemoveProject.click();
		Log.message("delete button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		btnyes.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify1 = notifymsg.getText();
		Log.message("Notify Message is:" + Notify1);
		SkySiteUtils.waitTill(1000);
		if (Notify1.equals("Selected collection(s) successfully deleted."))
		{
			Log.message("Exp collection deleted Successfull");
			SkySiteUtils.waitTill(5000);
		} 
		else
		{
			Log.message("Exp collection deleted UnSuccessfull");
			SkySiteUtils.waitTill(5000);
		}
		SkySiteUtils.waitTill(5000);
		this.logout();
		this.loginWithOwnerUser(uName1, pWord1);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		clkcollection.click();
		Log.message("collections has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		String File_Count1 = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();
		Log.message("count is: " + File_Count1); // Getting counts
		SkySiteUtils.waitTill(5000);
		String[] y = File_Count1.split(" ");
		String Avl_Count1 = y[3];
		int AfterDelete_Count = Integer.parseInt(Avl_Count1);
		Log.message("Delete After Total Avl count is:" + AfterDelete_Count); // Getting counts
		if (AfterDelete_Count == (Before_Count - 1))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//i[@class='icon icon-favourite ico-lg']")

	WebElement btnfavorite;

	/**
	 * Method written for making multiple collection favorite Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Multiple_collection_favorite(String CollectionName) throws IOException
	{

		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		String File_Count1 = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();// Getting
																														// label
																														// counts
		Log.message("count is: " + File_Count1); // Getting counts
		SkySiteUtils.waitTill(5000);
		String[] y = File_Count1.split(" ");
		String Avl_Count1 = y[3];
		int beforeadding_Count = Integer.parseInt(Avl_Count1);
		Log.message("Before adding Total Avl count is:" + beforeadding_Count); // Getting counts
		SkySiteUtils.waitTill(3000);
		this.Create_Collections(CollectionName);
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		btncollection.click();
		Log.message("Add collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		String CollectionName1 = FnaHomePage.this.Random_Collectionname();
		Log.message(CollectionName1);
		txtProjectName.sendKeys(CollectionName1);
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(CollectionName1);
		Log.message("Collection number is:" + CollectionName1);
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(5000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(5000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);
		String Descrip = PropertyReader.getProperty("Description");
		txtProjectDesc.sendKeys(Descrip);
		SkySiteUtils.waitTill(5000);
		txtProjectAddress1.sendKeys("salt lake");
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectCity.sendKeys("Kolkata");
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectZip.sendKeys("700091");
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(2000);
		Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText("USA");
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText("California");
		Log.message("state has been selected");
		SkySiteUtils.waitTill(5000);
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection1 = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection1);
		SkySiteUtils.waitTill(2000);
		if (Collection1.contains(CollectionName1)) 
		{
			Log.message("Collection create successfull");
			SkySiteUtils.waitTill(2000);
		} 
		else 
		{
			Log.message("Collection create Unsuccessfull");
			SkySiteUtils.waitTill(2000);
		}
		// this.Create_Collections(CollectionName1);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		String coll_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();// Getting label	
		Log.message(" collection count is: " + coll_Count); // Getting counts
		SkySiteUtils.waitTill(5000);
		String[] x = coll_Count.split(" ");
		String Avl_Count = x[3];
		int Afteradding_Count = Integer.parseInt(Avl_Count);
		Log.message("After adding Total Avl count is:" + Afteradding_Count); // Getting counts
		SkySiteUtils.waitTill(3000);
		int adding_count = Afteradding_Count - beforeadding_Count;
		Log.message("Adding collection count is:" + adding_count); // Getting counts
		SkySiteUtils.waitTill(3000);
		int Beforefavoriteorange_count = driver.findElements(By.xpath("//i[@class='icon icon-favourite icon-orange']"))
				.size();
		Log.message("Before favorite orange Count is:" + Beforefavoriteorange_count);
		SkySiteUtils.waitTill(3000);

		int contactimg_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("contact img Count is:" + contactimg_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= contactimg_count; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String ContactName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message("collection name is:" + ContactName);
			SkySiteUtils.waitTill(5000);
			Log.message("Exp collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (ContactName.contains(CollectionName))
			{
				Log.message("Exp collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img)[" + i + "]")).click();
		Log.message("Exp collection has been clicked");
		SkySiteUtils.waitTill(5000);

		int contactimg_count1 = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("contact img Count is:" + contactimg_count1);
		// Loop start '2' as projects list start from tr[2]
		int j = 0;
		for (j = 1; j <= contactimg_count1; j++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String ContactName1 = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + j + "]")).getText();
			Log.message("collection name1 is:" + ContactName1);
			SkySiteUtils.waitTill(5000);
			Log.message("Exp collection name is:" + CollectionName1);
			// Checking collection name equal with each row of table
			if (ContactName1.contains(CollectionName1)) 
			{
				Log.message("Exp collection name1 has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img)[" + j + "]")).click();
		Log.message("Exp collection has been clicked");
		SkySiteUtils.waitTill(5000);
		btnfavorite.click();
		Log.message("Favorite button has been clicked");
		SkySiteUtils.waitTill(5000);

		int Afterfavoriteorange_count = driver.findElements(By.xpath("//i[@class='icon icon-favourite icon-orange']")).size();
		Log.message("After favorite orange Count is:" + Afterfavoriteorange_count);
		SkySiteUtils.waitTill(3000);
		int favoriteorange_count = Afterfavoriteorange_count - Beforefavoriteorange_count;
		Log.message("Exp Favorite orange Count is:" + favoriteorange_count);
		SkySiteUtils.waitTill(3000);
		if (adding_count == favoriteorange_count)
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='ancProjectOwner']")

	WebElement btnProjectOwner;

	@FindBy(xpath = "//input[@id='btn_changeOwner']")

	WebElement btnchangeOwner;

	@FindBy(xpath = "//*[@id='UserGrid']/div[2]/table/tbody/tr[3]/td[1]/img")

	WebElement btnchangeemp;

	@FindBy(xpath = "//input[@id='btnNext_page']")

	WebElement btnnext;

	@FindBy(xpath = "//*[@id='TeamGrid']/div[2]/table/tbody/tr[2]/td[1]/img")

	WebElement ownerchkbox;

	/**
	 * Method written for owner change from collection owner tab
	 *  Scripted By: Sekhar	
	 * @return
	 */
	public boolean ownerchangecollectionownertab(String CollectionName,String ContactName) throws IOException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);

		int collimg_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("coll img Count is:" + collimg_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collimg_count; i++)
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Exp_Coll_Name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message("collection name is:" + Exp_Coll_Name);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (Exp_Coll_Name.contains(CollectionName)) 
			{
				Log.message("Exp collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)[" + i + "]")).click();
		Log.message("Exp collection edit has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		btnProjectOwner.click();
		Log.message("collection Owner button has been clicked");
		SkySiteUtils.waitTill(3000);
		btnchangeOwner.click();
		Log.message("collection Owner button has been clicked");
		SkySiteUtils.waitTill(3000);		
		int coll_radio = driver.findElements(By.xpath("//*[@id='UserGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("coll radio Count is:" + coll_radio);
		// Loop start '2' as projects list start from tr[2]
		int j = 0;
		for (j = 1; j <= coll_radio; j++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Exp_Contact_Name = driver.findElement(By.xpath("(//*[@id='UserGrid']/div[2]/table/tbody/tr/td[2])[" +j+ "]")).getText();
			Log.message("collection name is:" + Exp_Contact_Name);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (Exp_Contact_Name.contains(ContactName))
			{
				Log.message("Exp collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='UserGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
		SkySiteUtils.waitTill(5000);		
		btnSave.click();
		Log.message("save & closr button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);		
		String Act_Coll_owner = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[6])["+i+"]")).getText();
		Log.message("Exp collection name is:" + Act_Coll_owner);
		SkySiteUtils.waitTill(5000);
		if (ContactName.equals(Act_Coll_owner))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Account expired created project population Scripted By:
	 * Sekhar
	 * 
	 * @return
	 */
	public boolean Accountexpired_createdcollection(String CollectionExpiredName) throws IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);

		int collimg_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("coll img Count is:" + collimg_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collimg_count; i++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Exp_Coll_Name = driver
					.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message("collection name is:" + Exp_Coll_Name);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (!Exp_Coll_Name.equals(CollectionExpiredName))
			{
				result1 = true;
				Log.message("Exp collection name has been validated!!!");
			}
			else 
			{
				result1 = false;
				Log.message("Exp collection name has been NOT validated!!!");
			}
		}
		if (result1 == true)
			return true;
		else
			return false;
	}

	@FindBy(css = "#btnAddFolder")

	WebElement btnAddFolder;

	@FindBy(css = "#txtFolderName")

	WebElement txtFolderName;

	@FindBy(css = "#btnNewFolderSave")

	WebElement btnNewFolderSave;

	@FindBy(xpath = "(//span[@class='selectedTreeRow'])[1]")

	WebElement slttreefolder;

	@FindBy(xpath = "//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span")

	WebElement sltrootfolder;

	@FindBy(xpath = "(//i[@class='icon icon-option ico-lg'])[1]")

	WebElement btnfldmore;

	@FindBy(xpath = "//i[@class='icon icon-folder']")

	WebElement btnuploadfolder;

	@FindBy(xpath = "(//i[@class='icon icon-refresh ico-lg'])[1]")

	WebElement btnfolderrefresh;

	/**
	 * Method written for Adding selected folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Select_Folder() throws AWTException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		btnAddFolder.click();		
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		String FolderName = PropertyReader.getProperty("ColleFolderName");
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(3000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName)) 
		{
			result1 = true;
			Log.message("Adding the selected folder validation successfull ");
		}
		else 
		{
			result1 = false;
			Log.message("Adding the selected folder validation Unsuccessfull ");
		}
		SkySiteUtils.waitTill(3000);
		sltrootfolder.click();
		Log.message("Root folder has been clicked");
		SkySiteUtils.waitTill(3000);
		btnfldmore.click();
		Log.message("folder more option has been clicked");
		SkySiteUtils.waitTill(3000);
		btnuploadfolder.click();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//button[@id='UploadWithTurbo']")).click();// click on upload with turbo
		SkySiteUtils.waitTill(40000);
		// Handling Download PopUp using robot
		Robot robot = null;
		robot = new Robot();		
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_LEFT);
		robot.keyRelease(KeyEvent.VK_LEFT);
		SkySiteUtils.waitTill(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(70000);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		SkySiteUtils.waitTill(15000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_RIGHT);
		robot.keyRelease(KeyEvent.VK_RIGHT);
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		SkySiteUtils.waitTill(3000);
		robot.keyPress(KeyEvent.VK_SHIFT);
		robot.keyPress(KeyEvent.VK_SPACE);
		robot.keyRelease(KeyEvent.VK_SPACE);
		robot.keyRelease(KeyEvent.VK_SHIFT);
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		SkySiteUtils.waitTill(10000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(10000);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		SkySiteUtils.waitTill(15000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(60000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		btnfolderrefresh.click();
		Log.message("refresh button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[2]")).click();
		SkySiteUtils.waitTill(5000);
		// Validate After File Upload
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[4]")).isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * Method written for select collection
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean selectcollection(String Collection_Name) 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, drpSetting, 60);
		SkySiteUtils.waitTill(3000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("prjCount_prjList :" + prjCount_prjList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= prjCount_prjList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(2000);
			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name)) 
			{
				Log.message("Select Collection button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		// Click on project name where it equal with specified project name
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(Collection_Name)) 
		{
			Log.message("Select Collection successfully");
			SkySiteUtils.waitTill(2000);
			return true;
		} 
		else
		{
			Log.message("Select Collection Unsuccessfully");
			SkySiteUtils.waitTill(2000);
			return false;
		}

	}

	/**
	 * Method written for Adding folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Folder(String FolderName) throws AWTException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switch to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName)) 
		{
			Log.message("Adding the folder validation successfully");
			SkySiteUtils.waitTill(2000);
			return true;
		} 
		else 
		{
			Log.message("Adding the folder validation Unsuccessfully");
			SkySiteUtils.waitTill(2000);
			return false;
		}
	}
		
	@FindBy(css = ".icon.icon-files.ico-lg")
	WebElement clkdocuments;

	/**
	 * Method written for Adding folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_SubFolder_Expand_Structure(String SubFolderName) throws AWTException 
	{
		boolean result1 = true;
		SkySiteUtils.waitTill(5000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(SubFolderName);
		Log.message("Sub Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String SubFolder_name = slttreefolder.getText();
		Log.message("Act Sub Folder Name is:" + SubFolder_name);
		if (SubFolder_name.contains(SubFolderName))
		{
			result1 = true;
			Log.message("Adding the Subfolder validation successfull ");
		}
		else 
		{
			result1 = false;
			Log.message("Adding the Subfolder validation Unsuccessfull ");
		}
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		clkcollection.click();
		Log.message("collections has been clicked");
		SkySiteUtils.waitTill(10000);
		clkdocuments.click();
		Log.message("Documents has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		String SubFolder_name1 = slttreefolder.getText();
		Log.message("Act Sub Folder Name1 is:" + SubFolder_name1);
		SkySiteUtils.waitTill(2000);
		if (SubFolder_name1.contains(SubFolderName))
			return true;
		else
			return false;
	}

	@FindBy(css = "#btnUploadFile")

	WebElement btnUploadFile;

	//@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")
	@FindBy(css = "#btnSelectFiles")//modify by sekhar
	WebElement btnselctFile;

	/**
	 * Method written for Upload file Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean UploadFile(String FolderPath) throws AWTException, IOException
	{
		// boolean result1=true;
		SkySiteUtils.waitTill(3000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();//modify by sekhar
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(20000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(css = "#ac_ationselectCopy")
	WebElement radionewcopy;

	/**
	 * Method written for Upload file Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean UploadFile_NewCopy(String FolderPath) throws AWTException, IOException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(8000);
		radionewcopy.click();
		Log.message("newcopy radio button has been clicked");
		SkySiteUtils.waitTill(5000);
		btnselctFile.click();
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();
		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle1);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[2]/a[3]")).isDisplayed()) 
		{
			result1 = true;
			Log.message("Upload file with new copy validation Successfull");
		} 
		else
		{
			result1 = false;
			Log.message("Upload file with new copy validation UnSuccessfull");
		}
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//i[@class='icon icon-info icon-orange'])[2]")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//*[@id='tbPreviousVersion']/a")).click();
		SkySiteUtils.waitTill(5000);		
		String Before_Revision_Count = driver.findElement(By.xpath("//*[@id='spnHistoryCount']")).getText();		
		Log.message("count is: " + Before_Revision_Count); // Getting counts

		SkySiteUtils.waitTill(5000);
		String[] x = Before_Revision_Count.split(" ");
		String Avl_Count = x[3];
		int CountBefore = Integer.parseInt(Avl_Count);	
		Log.message("Before Avl Revision count is:" + CountBefore); // Getting counts
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='div_otherInfo']/div/div/div[1]/button")).click();
		SkySiteUtils.waitTill(5000);
		
		this.UploadFile(FolderPath);
		SkySiteUtils.waitTill(15000);
		
		driver.findElement(By.xpath("(//i[@class='icon icon-info icon-orange'])[2]")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//*[@id='tbPreviousVersion']/a")).click();
		SkySiteUtils.waitTill(5000);		
		String After_Revision_Count = driver.findElement(By.xpath("//*[@id='spnHistoryCount']")).getText();		
		Log.message("count is: " + After_Revision_Count); // Getting counts
		SkySiteUtils.waitTill(5000);
		String[] y = After_Revision_Count.split(" ");
		String Avl_Count1 = y[3];
		int CountAfter = Integer.parseInt(Avl_Count1);		
		Log.message("After Avl Revision count is:" + CountAfter); // Getting counts		
		SkySiteUtils.waitTill(3000);
		
		if (CountAfter == (CountBefore + 1))
			return true;
		else
			return false;
	}

	@FindBy(css = "#ac_ationselectCopy")

	WebElement radione;

	/**
	 * Method written for Bread crumb available in the File Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Bread_crumb_File() throws AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[4]")).isDisplayed()) 
		{
			Log.message("Exp bread crumb file available");
		} 
		else 
		{
			Log.message("Exp bread crumb file NOT available");
		}
		String Foldername = driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[4]")).getText();
		Log.message("Exp Folder name is:" + Foldername);
		driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[4]")).click();
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Act Folder name is:" + Collection);
		if (Foldername.contains(Collection))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[2]")

	WebElement folderlevel;

	@FindBy(css = "#lftpnlMore")

	WebElement lftpnlMore;

	@FindBy(css = "#tdMoveFolder>a")

	WebElement tdMoveFolder;

	@FindBy(xpath = "//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")

	WebElement popuptree;

	@FindBy(xpath = "//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")

	WebElement popuptree2;

	@FindBy(xpath = "//*[@id='popup']/div/div/div[3]/div[2]/input[1]")

	WebElement popupok;

	/**
	 * Method written for Move Folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Move_Folder(String FolderName) throws AWTException, IOException 
	{

		SkySiteUtils.waitTill(5000);
		folderlevel.click();
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		tdMoveFolder.click();
		Log.message("move folder has been clicked");
		SkySiteUtils.waitTill(3000);
		popuptree.click();
		Log.message("popup folder has been clicked");
		SkySiteUtils.waitTill(3000);
		popupok.click();
		Log.message("popup ok has been clicked");
		SkySiteUtils.waitTill(3000);
		String After_MoveFile = driver.findElement(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span"))
				.getText();
		Log.message("After move Folder name is :" + After_MoveFile);
		SkySiteUtils.waitTill(5000);
		if (After_MoveFile.equals(FolderName))
			return true;
		else
			return false;
	}

	// Deleting files from a folder
	public boolean Delete_Files_From_Folder(String Folder_Path) 
	{
		try
		{
			SkySiteUtils.waitTill(5000);
			Log.message("Cleaning download folder!!! ");
			File file = new File(Folder_Path);
			String[] myFiles;
			if (file.isDirectory()) 
			{
				myFiles = file.list();
				for (int i = 0; i < myFiles.length; i++) 
				{
					File myFile = new File(file, myFiles[i]);
					myFile.delete();
					SkySiteUtils.waitTill(5000);
				}
				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
		} // end try
		catch (Exception e) 
		{
			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		}
		return false;
	}

	@FindBy(xpath = "(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[1]")

	WebElement clkproject;

	/**
	 * Method written for Adding folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Folder_Project(String FolderName) throws AWTException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		clkproject.click();// click on Project
		SkySiteUtils.waitTill(5000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName)) 
		{
			Log.message("Adding the folder validation successfull ");
			return true;
		}
		else 
		{
			Log.message("Adding the folder validation Unsuccessfull ");
			return false;
		}
	}

	@FindBy(xpath = "(//i[@class='icon icon-download'])[1]")

	WebElement fddownload;

	/**
	 * Method written for Download Folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Download_Folder(String DownloadPath, String FolderName1) throws AWTException, IOException 
	{

		SkySiteUtils.waitTill(5000);
		folderlevel.click();
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);
		fddownload.click();
		Log.message("Download folder button has been clicked");
		SkySiteUtils.waitTill(8000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(FolderName1))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//i[@class='icon icon-link-files'])[1]")

	WebElement fdlink;

	@FindBy(xpath = ".//*[@id='btnAddToCommunication_folder']")

	WebElement sendcommu;

	@FindBy(xpath = "//*[@id='txtMessageBody_container']/table/tbody/tr[2]/td")

	WebElement txtMessageBody;

	/**
	 * Method written for send link Folder download Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean SendLink_Folder_Download(String DownloadPath, String FolderName) throws AWTException, IOException 
	{

		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);		
		fdlink.click();
		Log.message("link Folder has been clicked");
		SkySiteUtils.waitTill(3000);
		sendcommu.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		SkySiteUtils.waitTill(3000);
		
		WebElement R1 = driver.findElement(By.xpath("html/body/p/a"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String Folder_Link = R1.getAttribute("href");
		Log.message("Link is: " + Folder_Link);
		SkySiteUtils.waitTill(10000);		
		driver.get(Folder_Link);
		SkySiteUtils.waitTill(20000);
		
		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot = null;
   			robot = new Robot();   			
   			robot.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot.keyRelease(KeyEvent.VK_ALT);
   			robot.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot.keyPress(KeyEvent.VK_ENTER);
   			robot.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(25000);		
		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0)
				{
					Log.message("Downloaded folder is available in downloads!!!");
					break;
				} 
				else 
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(FolderName))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Adding folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_SubFolder_Expand30thlevel(String SubFolderName) throws AWTException 
	{
		SkySiteUtils.waitTill(5000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(SubFolderName);
		Log.message("Sub Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(15000);
		String SubFolder_name = slttreefolder.getText();
		Log.message("Act Sub Folder Name is:" + SubFolder_name);
		SkySiteUtils.waitTill(5000);
		if (SubFolder_name.contains(SubFolderName))
		{
			Log.message("Adding the Subfolder validation successfull ");
			return true;
		}
		else 
		{
			Log.message("Adding the Subfolder validation Unsuccessfull ");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")

	WebElement popuptree1;

	/**
	 * Method written for Move Folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Move_SubFolder30thlevel() throws AWTException, IOException 
	{

		SkySiteUtils.waitTill(5000);
		String SubFolder_name = slttreefolder.getText();
		Log.message("Act Sub Folder Name is:" + SubFolder_name);
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		tdMoveFolder.click();
		Log.message("move folder has been clicked");
		SkySiteUtils.waitTill(3000);
		popuptree1.click();
		Log.message("popup folder has been clicked");
		SkySiteUtils.waitTill(3000);
		popupok.click();
		Log.message("popup ok has been clicked");
		SkySiteUtils.waitTill(8000);
		String After_MoveFolder = slttreefolder.getText();
		Log.message("After move Folder name is :" + After_MoveFolder);		
		SkySiteUtils.waitTill(5000);
		if (After_MoveFolder.equals(SubFolder_name) && (driver
				.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed()))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//i[@class='icon icon-modify-folder'])[1]")

	WebElement fdrename;

	/**
	 * Method written for Adding rename folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Rename_Folder(String FolderName) throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		fdrename.click();
		Log.message("Rename folder has been clicked");
		SkySiteUtils.waitTill(3000);
		txtFolderName.clear();
		Log.message("folder name has been cleared");
		SkySiteUtils.waitTill(3000);
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName))
		{
			Log.message("Adding rename folder validation successfull ");
			return true;
		} 
		else 
		{
			Log.message("Adding rename folder validation Unsuccessfull ");
			return false;
		}
	}

	@FindBy(xpath = "//input[@id='chkAllProjectdocs']")

	WebElement sltallchkbox;

	@FindBy(css = "#Button1")

	WebElement btnmore;

	@FindBy(xpath = "(//i[@class='icon icon-link-files'])[3]")

	WebElement drplinkfiles;

	@FindBy(xpath = "//*[@id='btnAddToCommunication']")

	WebElement sendviacommunica;

	/**
	 * Method written for select multiple files then send link and download.
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Multiplefiles_sendlink_download(String DownloadPath) throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a[1]")).click();
		SkySiteUtils.waitTill(3000);
		String Filename = driver.findElement(By.xpath("//*[@id='tinymce']/p/a[1]")).getText();
		Log.message("File name is:" + Filename);
		
		WebElement R1 = driver.findElement(By.xpath("html/body/p/a[1]"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String File_Link = R1.getAttribute("href");
		Log.message("Link is: " + File_Link);
		SkySiteUtils.waitTill(10000);

		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		// SkySiteUtils.waitTill(10000);
		driver.get(File_Link);
		SkySiteUtils.waitTill(20000);
		
		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot = null;
   			robot = new Robot();   			
   			robot.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot.keyRelease(KeyEvent.VK_ALT);
   			robot.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot.keyPress(KeyEvent.VK_ENTER);
   			robot.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(25000);

		// After checking whether download folder or not
		String ActualFilename = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
					break;
				} 
				else 
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFilename.contains(Filename))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Upload multiple files Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Upload_MultipleFiles(String FolderPath) throws AWTException, IOException 
	{
		// boolean result1=true;
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click();
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(8000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(8000);
		String BeforeUpload_Count = driver.findElement(By.xpath("//*[@id='lblFileCount']")).getText();// Getting label
																										// counts
		Log.message("Before upload count is: " + BeforeUpload_Count);// Getting Lite user counts
		String[] y = BeforeUpload_Count.split(" ");
		String AvlUpload_Count = y[0];
		int CountBefore = Integer.parseInt(AvlUpload_Count);
		Log.message("Avl before upload count is: " + CountBefore);// Getting Lite user counts
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		String AfterUpload_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div")).getText();// Getting label counts
		Log.message("After upload count is: " + AfterUpload_Count);// Getting Lite user counts
		String[] x = AfterUpload_Count.split(" ");
		String AvlAfterUpload_Count = x[1];
		int CountAfter = Integer.parseInt(AvlAfterUpload_Count);
		Log.message("Avl After upload count is: " + CountAfter);// Getting Lite user counts
		SkySiteUtils.waitTill(5000);
		if(CountBefore == CountAfter)
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='ProjectMenu1_Album']/a")

	WebElement btnalbum;

	@FindBy(xpath = "//*[@id='divFirstAlbum']/div/div/button")

	WebElement btncreatealbum;

	@FindBy(xpath = "//*[@id='txtAlbumName']")

	WebElement txtalbumname;

	@FindBy(xpath = "//*[@id='btnAlbumSave']")

	WebElement btnAlbumSave;

	/**
	 * Method written for Adding Album 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Album(String AlbumName) throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnalbum.click();
		Log.message("Album button has been clicked");
		SkySiteUtils.waitTill(5000);
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switch to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		btncreatealbum.click(); // Adding on New Folder
		Log.message("Create album button has been clicked");
		SkySiteUtils.waitTill(5000);
		txtalbumname.sendKeys(AlbumName);
		Log.message("Album name has been entered");
		SkySiteUtils.waitTill(3000);
		btnAlbumSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Album_name = slttreefolder.getText();
		Log.message("Act Album Name is:" + Album_name);
		if (Album_name.contains(AlbumName)) 
		{
			Log.message("Adding the Album validation successfull ");
			return true;
		} 
		else 
		{
			Log.message("Adding the Album validation Unsuccessfull ");
			return false;
		}
	}

	@FindBy(xpath = "//*[@id='treeRefresh']")

	WebElement treeRefresh;

	@FindBy(xpath = "//*[@id='dvAlbumImageContainer']/button")

	WebElement btnuploadphoto;
	
	@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")		
	WebElement btnselctFile1;//modify sekhar
	
	@FindBy(xpath = "//*[@id='dvAlbumImageContainer']/div")		
	WebElement dvAlbumImageContainer;

	/**
	 * Method written for Upload multiple photos 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Upload_MultiplePhotos(String PhotosPath) throws AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);
		treeRefresh.click();
		Log.message("refresh button has been clicked");
		SkySiteUtils.waitTill(5000);
		btnuploadphoto.click();
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile1.click();
		Log.message("select photos has been clicked");
		SkySiteUtils.waitTill(15000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(PhotosPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + PhotosPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else 
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		String BeforeUpload_Count = driver.findElement(By.xpath("//*[@id='lblFileCount']")).getText().toString();// Getting label counts
		Log.message("Before upload count is: " + BeforeUpload_Count);// Getting Lite user counts
		SkySiteUtils.waitTill(5000);
		String[] y = BeforeUpload_Count.split(" ");
		String AvlUpload_Count = y[0];
		int CountBefore = Integer.parseInt(AvlUpload_Count);
		Log.message("Avl before upload photo count is: " + CountBefore);// Getting Lite user counts
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//button[@id='btnFileUpload']")).click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(50000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		//SkySiteUtils.waitForElementLoadTime(driver, dvAlbumImageContainer, 60);
		int AfterUpload_Count = driver.findElements(By.xpath("//*[@id='dvAlbumImageContainer']/div")).size();// Getting	label counts
		Log.message("After upload photo count is: " + AfterUpload_Count);// Getting Lite user counts
		SkySiteUtils.waitTill(5000);
		if (CountBefore == AfterUpload_Count)
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='dvAlbumImageContainer']/div[1]/div/a/img")

	WebElement sltimage;

	@FindBy(xpath = "//*[@id='divMore']/div/button")

	WebElement sltmore;

	@FindBy(xpath = "//i[@class='icon icon-share']")

	WebElement sltshare;

	@FindBy(xpath = "(//input[@type='checkbox'])[3]")

	WebElement photocheckbox;

	@FindBy(xpath = "//*[@id='btnLink']")

	WebElement btnshare;

	@FindBy(xpath = "//*[@id='Button3']")

	WebElement btnsendcommu;

	@FindBy(xpath = "//*[@id='tinymce']/a/img")

	WebElement btnimage;

	/**
	 * Method written for select multiple photos then share and download. Scripted
	 * By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Multiplephoto_share_download(String DownloadPath) throws AWTException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(15000);

		sltimage.click();
		Log.message("photo has been clicked");
		SkySiteUtils.waitTill(5000);
		slttreefolder.click();
		SkySiteUtils.waitTill(5000);
		sltimage.click();
		Log.message("photo has been clicked");
		SkySiteUtils.waitTill(5000);
		sltmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(5000);
		sltshare.click();
		Log.message("share has been clicked");
		SkySiteUtils.waitTill(20000);		
		btnshare.click();
		Log.message("share button has been clicked");
		SkySiteUtils.waitTill(20000);
		btnsendcommu.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(40000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSubject']")).sendKeys("Test");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("switch to frame");
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/a")).click();
		Log.message("Image clicked");
		SkySiteUtils.waitTill(10000);		
		
		WebElement R1 = driver.findElement(By.xpath("html/body/a"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String Photo_Link = R1.getAttribute("href");
		Log.message("Link is: " + Photo_Link);
		SkySiteUtils.waitTill(10000);		
		driver.get(Photo_Link);
		SkySiteUtils.waitTill(20000);		

		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot = null;
   			robot = new Robot();   			
   			robot.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot.keyRelease(KeyEvent.VK_ALT);
   			robot.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot.keyPress(KeyEvent.VK_ENTER);
   			robot.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		File[] files = new File(DownloadPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 			
			{
				SkySiteUtils.waitTill(5000);
				Long ActualPhotoSize = file.length();
				Log.message("Actual Photo size is:" + ActualPhotoSize);
				SkySiteUtils.waitTill(5000);
				if (ActualPhotoSize != 0) 
				{
					Log.message("Downloaded Photo is Validation Successfull");
					result1 = true;
					break;
				} 
				else 
				{
					result1 = false;
					Log.message("Downloaded Photo is Validation UnSuccessfull");
				}
			}
		}
		if(result1==true)
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//input[@id='btnCancel']")

	WebElement btnCancel;

	/**
	 * Method written for Autoupdate send link and download. Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Autoupdate_sendlink_download(String DownloadPath, String FilePath) throws AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		SkySiteUtils.waitTill(20000);	
		driver.findElement(By.xpath(".//*[@id='tinymce']/p/a")).click();
		SkySiteUtils.waitTill(3000);
		String Filename = driver.findElement(By.xpath(".//*[@id='tinymce']/p/a")).getText();
		Log.message("File name is:" + Filename);
		SkySiteUtils.waitTill(5000);

		WebElement R1 = driver.findElement(By.xpath(".//*[@id='tinymce']/p/a"));
		Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		builder.contextClick(R1).sendKeys(Keys.ARROW_DOWN).perform();
		SkySiteUtils.waitTill(5000);
		Robot robot1 = new Robot();
		SkySiteUtils.waitTill(5000);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		SkySiteUtils.waitTill(5000);
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(10000);
		robot1.keyPress(KeyEvent.VK_CONTROL);
		robot1.keyPress(KeyEvent.VK_T);
		robot1.keyRelease(KeyEvent.VK_T);
		robot1.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(20000);

		/*
		 * driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		 * SkySiteUtils.waitTill(10000);
		 */

		Robot robot = new Robot();
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);

		// After checking whether download folder or not
		String ActualFilename = null;
		Long ActualFolderSize = null;
		File[] files = new File(DownloadPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile())
			{
				ActualFilename = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(10000);
				if (ActualFolderSize != 0)
				{
					Log.message("Downloaded folder is available in downloads!!!");
					break;
				}
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFilename.contains(Filename)) 
		{
			result1 = true;
			Log.message("Download files Successfully!!!");
		} 
		else
		{
			result1 = false;
			Log.message("Download files UnSuccessfully!!!");
		}
		SkySiteUtils.waitTill(5000);
		Robot robot5 = new Robot();
		SkySiteUtils.waitTill(5000);
		robot5.keyPress(KeyEvent.VK_CONTROL);
		robot5.keyPress(KeyEvent.VK_W);
		robot5.keyRelease(KeyEvent.VK_W);
		robot5.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(10000);

		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnCancel.click();
		Log.message("Back button has been clicked");
		SkySiteUtils.waitTill(8000);
		this.UploadFile(FilePath);

		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		SkySiteUtils.waitTill(10000);

		String Filename1 = driver.findElement(By.xpath(".//*[@id='tinymce']/p/a")).getText();
		Log.message("File name1 is:" + Filename1);
		SkySiteUtils.waitTill(5000);
		
		WebElement R2 = driver.findElement(By.xpath("html/body/p/a"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String Photo_Link = R2.getAttribute("href");
		Log.message("Link is: " + Photo_Link);
		SkySiteUtils.waitTill(10000);		
		driver.get(Photo_Link);
		SkySiteUtils.waitTill(20000);
		
		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot2 = null;
   			robot2 = new Robot();   			
   			robot2.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot2.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot2.keyRelease(KeyEvent.VK_ALT);
   			robot2.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot2.keyPress(KeyEvent.VK_ENTER);
   			robot2.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		String ActualFilename1 = null;
		Long ActualFileSize1 = null;
		File[] files1 = new File(DownloadPath).listFiles();

		for (File file : files1)
		{
			if (file.isFile())
			{
				ActualFilename1 = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name1 is:" + ActualFilename1);
				SkySiteUtils.waitTill(1000);
				ActualFileSize1 = file.length();
				Log.message("Actual File size1 is:" + ActualFileSize1);
				SkySiteUtils.waitTill(10000);
				if (ActualFileSize1 != 0) 
				{
					Log.message("Downloaded file is available in downloads!!!");
					break;
				} 
				else
				{
					Log.message("Downloaded file is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFilename1.contains(Filename1)) 
		{
			result2 = true;
			Log.message("Download file Successfully!!!");
		} 
		else 
		{
			result2 = false;
			Log.message("Download file UnSuccessfully!!!");
		}
		if ((ActualFolderSize != ActualFileSize1) && (result1 == true) && (result2 == true))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//input[@id='chkEnableViewer']")

	WebElement chkEnableViewer;

	/**
	 * Method written for Enable Viewer send link and file revision. Scripted By:
	 * Sekhar
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean EnableViewer_sendlink_revision(String FilePath) throws AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		chkEnableViewer.click();
		Log.message("enable viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		SkySiteUtils.waitTill(5000);	

		WebElement R1 = driver.findElement(By.xpath("//*[@id='tinymce']/p/a"));
		Actions builder = new Actions(driver);
		Log.message("selete the path frame");
		SkySiteUtils.waitTill(10000);
		builder.contextClick(R1).sendKeys(Keys.ARROW_DOWN).perform();
		SkySiteUtils.waitTill(5000);
		Robot robot1 = new Robot();
		SkySiteUtils.waitTill(5000);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		robot1.keyPress(KeyEvent.VK_DOWN);
		robot1.keyRelease(KeyEvent.VK_DOWN);
		SkySiteUtils.waitTill(5000);
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(10000);
		robot1.keyPress(KeyEvent.VK_CONTROL);
		robot1.keyPress(KeyEvent.VK_T);
		robot1.keyRelease(KeyEvent.VK_T);
		robot1.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(20000);

		/*
		 * driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		 * SkySiteUtils.waitTill(10000);
		 */

		Robot robot = new Robot();
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);
		String parentHandle = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).click();
		Log.message("viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		String FileName = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("Act File Name is:" + FileName);
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() 
				&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())) 
		{
			result1 = true;
			Log.message("Enable Viewer is Successfully!!!!!");
		} 
		else
		{
			result1 = false;
			Log.message("Enable Viewer is UnSuccessfully!!!!!");
		}
		SkySiteUtils.waitTill(5000);
		Robot robot5 = new Robot();
		SkySiteUtils.waitTill(5000);
		robot5.keyPress(KeyEvent.VK_CONTROL);
		robot5.keyPress(KeyEvent.VK_W);
		robot5.keyRelease(KeyEvent.VK_W);
		robot5.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(parentHandle);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnCancel.click();
		Log.message("Back button has been clicked");
		SkySiteUtils.waitTill(8000);
		this.UploadFile(FilePath);// upload file for revision
		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		chkEnableViewer.click();
		Log.message("Enable viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		SkySiteUtils.waitTill(3000);
		
		WebElement R2 = driver.findElement(By.xpath("html/body/p/a"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String File_Link = R2.getAttribute("href");
		Log.message("Link is: " + File_Link);
		SkySiteUtils.waitTill(10000);		
		driver.get(File_Link);
		SkySiteUtils.waitTill(20000);  			
		String FileName1 = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("Act File Name is:" + FileName1);
		if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed()
				&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())) 
		{
			result2 = true;
			Log.message("Enable Viewer is Successfully!!!!!");
		}
		else
		{
			result2 = false;
			Log.message("Enable Viewer is UnSuccessfully!!!!!");
		}
		SkySiteUtils.waitTill(5000);
		if ((FileName != FileName1) && (result1 == true) && (result2 == true))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")

	WebElement chk1stbox;

	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[1]/img")

	WebElement chk2ndbox;

	@FindBy(css = "#liSubscribeAlert1>a")

	WebElement liSubscribeAlert1;

	@FindBy(css = "#chkFileTOp")

	WebElement chkFileTOp;

	@FindBy(css = "#btnSaveAlertSettings")

	WebElement btnSaveAlertSettings;

	@FindBy(css = "#liDeleteFile1>a")

	WebElement liDeleteFile1;

	@FindBy(xpath = "//*[@id='ProjectMenu1_Communications']/div/a[2]")

	WebElement clkdrpcommu;

	@FindBy(css = "#ProjectMenu1_liMessageBoard>a")

	WebElement liMessageBoard;

	@FindBy(xpath = ".//*[@id='divGrid1']/div[2]/table/tbody/tr[2]/td[2]")

	WebElement clkgridsubject;

	/**
	 * Method written for Set Alert for Two files then delete files and checking
	 * messsage board Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean SetAlert_DeleteFiles_MessageBoard() throws AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitTill(5000);
		chk1stbox.click();
		Log.message("1st check box has been clicked");
		SkySiteUtils.waitTill(3000);
		chk2ndbox.click();
		Log.message("2nd check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option button has been clicked");
		SkySiteUtils.waitTill(3000);
		liSubscribeAlert1.click();
		Log.message("Set Alert has been clicked");
		SkySiteUtils.waitTill(5000);
		chkFileTOp.click();
		Log.message("File Subscription has been clicked");
		SkySiteUtils.waitTill(3000);
		btnSaveAlertSettings.click();
		Log.message("Set alerts has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		//String notify_msg = driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		//Log.message("Notify message is:" + notify_msg);
		
		if (Notify_msg.contains("Alert settings successfully saved."))
		{
			result1 = true;
			Log.message("Alert setting Successfull");
		} 
		else 
		{
			result1 = false;
			Log.message("Alert setting UnSuccessfull");
		}
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnmore.click();
		Log.message("more option button has been clicked");
		SkySiteUtils.waitTill(3000);
		liDeleteFile1.click();
		Log.message("Delete files has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes has been clicked");
		SkySiteUtils.waitTill(5000);
		String notify_msg1 = driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		Log.message("Notify message is:" + notify_msg1);
		if (notify_msg1.contains("File(s) successfully deleted.")) 
		{
			result2 = true;
			Log.message("Files deleted Successfull");
		} 
		else 
		{
			result2 = false;
			Log.message("Files deleted UnSuccessfull");
		}
		SkySiteUtils.waitTill(5000);
		clkdrpcommu.click();
		Log.message("communication drop down has been clicked");
		SkySiteUtils.waitTill(3000);
		liMessageBoard.click();
		Log.message("Messsage board has been clicked");
		SkySiteUtils.waitTill(8000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		clkgridsubject.click();
		Log.message("Grid Subject has been clicked");
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='ViewGrid']/div/table/tbody/tr[3]/td/div/table/tbody/tr[1]/td[2]")).isDisplayed()
				&& (driver.findElement(By.xpath("//*[@id='ViewGrid']/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td[2]")).isDisplayed())
				&& (result1 == true) && (result2 == true))
			return true;
		else
			return false;
	}

	@FindBy(css = "#spanMessage")

	WebElement spanMessage;

	@FindBy(xpath = "//*[@id='liRecycle']/a")

	WebElement liRecycle;

	@FindBy(xpath = "//*[@id='divRecycleItem']/div[2]/table/tbody/tr[2]/td[8]")

	WebElement btnRestore;

	@FindBy(xpath = "//i[@class='icon icon-files ico-lg']")

	WebElement btndocuments;

	/**
	 * Method written for Enable Viewer send linkfile then delete file and check
	 * link viewing or not Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean EnableViewer_sendlinkfile_Deletefile_RestoreFile(String Collection_Name)
			throws AWTException, IOException
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;
		boolean result5 = false;

		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		chkEnableViewer.click();
		Log.message("enable viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitTill(15000);

		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		Log.message("files clicked");
		SkySiteUtils.waitTill(10000);
		WebElement R1 = driver.findElement(By.xpath("html/body/p/a"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String File_Link = R1.getAttribute("href");
		Log.message("File Link is: " + File_Link);
		SkySiteUtils.waitTill(10000);		

		/*String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, "t");
		driver.findElement(By.cssSelector("body")).sendKeys(selectLinkOpeninNewTab);
		SkySiteUtils.waitTill(5000);
*/
		driver.get(File_Link);
		SkySiteUtils.waitTill(20000);
	
		driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).click();
		Log.message("viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		String FileName = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("Act File Name is:" + FileName);
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() 
				&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())) 
		{
			result1 = true;
			Log.message("Enable Viewer is Successfully!!!!!");
		} 
		else 
		{
			result1 = false;
			Log.message("Enable Viewer is UnSuccessfully!!!!!");
		}
		SkySiteUtils.waitTill(5000);
		driver.navigate().back();
		Log.message("Navigate the browser Back");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 60);
		clkcollection.click();
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collCount_collList :" + collCount_collList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= collCount_collList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(collName);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.equals(Collection_Name)) 
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(6000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liDeleteFile1.click();
		Log.message("Delete files has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes has been clicked");
		SkySiteUtils.waitTill(5000);
		String notify_msg1 = driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		Log.message("Notify message is:" + notify_msg1);
		if (notify_msg1.contains("File(s) successfully deleted."))
		{
			result2 = true;
			Log.message("Files deleted Successfull");
		}
		else
		{
			result2 = false;
			Log.message("Files deleted UnSuccessfull");
		}
		SkySiteUtils.waitTill(5000);
		driver.get(File_Link);
		SkySiteUtils.waitTill(20000);
		String span_message = spanMessage.getText();
		Log.message("span message is:" + span_message);
		if (span_message.contains("File is deleted.")) 
		{
			result3 = true;
			Log.message("Files deleted successfully");
		} 
		else
		{
			result3 = false;
			Log.message("Files deleted Unsuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		driver.navigate().back();
		Log.message("Navigate the browser Back 2nd time");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 60);
		clkcollection.click();
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList1 = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collCount_collList :" + collCount_collList1);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collCount_collList1; i++)
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName1 = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message(collName1);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName1.equals(Collection_Name)) 
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(6000);
		driver.switchTo().defaultContent();
		btnmyprofile.click();
		Log.message("Myprofile button has been clicked");
		SkySiteUtils.waitTill(3000);
		liRecycle.click();
		Log.message("recycle button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		btnRestore.click();
		Log.message("restore button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		String Notify_msg = driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		Log.message("Notify message is:" + Notify_msg);
		if (Notify_msg.contains("Successfully restored"))
		{
			result4 = true;
			Log.message("file restored successfully");
		} 
		else 
		{
			result4 = false;
			Log.message("file restored Unsuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		btndocuments.click();
		Log.message("documents button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
		{
			result5 = true;
			Log.message("file validation succesfully");
		} 
		else 
		{
			result5 = false;
			Log.message("file validation Unsuccesfully");
		}
		SkySiteUtils.waitTill(5000);
		driver.get(File_Link);
		SkySiteUtils.waitTill(20000);
		// SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).click();
		Log.message("viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		String FileName1 = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("Act File Name is:" + FileName1);
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed()
				&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())
				&& (result1 == true) && (result2 == true) && (result3 == true) && (result4 == true)	&& (result5 == true))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//i[@class='icon icon-link-files'])[1]")

	WebElement lnkfolder;

	@FindBy(xpath = "//*[@id='btnAddToCommunication_folder']")

	WebElement sendviacommunicafolder;

	@FindBy(xpath = "(//i[@class='icon icon-delete'])[1]")

	WebElement lidelete;

	@FindBy(css = "#spanAfterZip")

	WebElement spanAfterZip;

	/**
	 * Method written for Download send linkfolder then delete folder and check link
	 * downloading or not Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Download_sendlinkfolder_Deletefolder_RestoreFolder(String Collection_Name, String DownloadPath,
			String FolderName) throws AWTException, IOException
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;
		boolean result5 = false;
		SkySiteUtils.waitTill(5000);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("More option button has been clicked");
		SkySiteUtils.waitTill(3000);
		lnkfolder.click();
		Log.message("link folder has been clicked");
		SkySiteUtils.waitTill(5000);
		sendviacommunicafolder.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		Log.message("folder clicked");
		SkySiteUtils.waitTill(10000);
		WebElement R1 = driver.findElement(By.xpath("html/body/p/a"));
		// Actions builder = new Actions(driver);
		SkySiteUtils.waitTill(10000);
		String Folder_Link = R1.getAttribute("href");
		Log.message("Folder Link is: " + Folder_Link);
		SkySiteUtils.waitTill(10000);
		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, "t");
		driver.findElement(By.cssSelector("body")).sendKeys(selectLinkOpeninNewTab);
		SkySiteUtils.waitTill(5000);
		driver.get(Folder_Link);
		SkySiteUtils.waitTill(20000);

		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile())
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
				}
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(FolderName)) 
		{
			result1 = true;
			Log.message("Download folder Successfully");
		}
		else
		{
			result1 = false;
			Log.message("Download folder UnSuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		driver.navigate().back();
		Log.message("Navigate the browser Back");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 60);
		clkcollection.click();
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collCount_collList :" + collCount_collList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= collCount_collList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(collName);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.equals(Collection_Name))
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("More option button has been clicked");
		SkySiteUtils.waitTill(3000);
		lidelete.click();
		Log.message("remove folder has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		btnyes.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Notify_msg = driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		Log.message("Notify message is:" + Notify_msg);
		if (Notify_msg.contains("Folder(s) and subfolder(s) are successfully deleted.")) 
		{
			result2 = true;
			Log.message("Folder Deleted Successfully");
		} 
		else
		{
			result2 = false;
			Log.message("Folder Deleted UnSuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		driver.get(Folder_Link);
		SkySiteUtils.waitTill(20000);
		String span_message = spanAfterZip.getText();
		Log.message("span message is:" + span_message);
		if (span_message.contains("Folder has no file(s) or file(s) are deleted.")) {
			result3 = true;
			Log.message("Folder span message validation successfully ");
		} else {
			result3 = false;
			Log.message("Folder span message validation Unsuccessfully ");
		}
		SkySiteUtils.waitTill(5000);
		driver.navigate().back();
		Log.message("Navigate the browser Back 2nd time");
		SkySiteUtils.waitTill(10000);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 60);
		clkcollection.click();
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList1 = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("collCount_collList :" + collCount_collList1);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collCount_collList1; i++) {
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName1 = driver
					.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message(collName1);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName1.equals(Collection_Name)) {
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnmyprofile.click();
		Log.message("Myprofile button has been clicked");
		SkySiteUtils.waitTill(3000);
		liRecycle.click();
		Log.message("recycle button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		btnRestore.click();
		Log.message("restore button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		String Notify_msg1 = driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		Log.message("Notify message is:" + Notify_msg1);
		if (Notify_msg1.contains("Successfully restored")) {
			result4 = true;
			Log.message("file restored successfully");
		} else {
			result4 = false;
			Log.message("file restored Unsuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		btndocuments.click();
		Log.message("documents button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed()) {
			result5 = true;
			Log.message("file validation succesfully");
		} else {
			result5 = false;
			Log.message("file validation Unsuccesfully");
		}
		SkySiteUtils.waitTill(5000);
		driver.get(Folder_Link);
		SkySiteUtils.waitTill(20000);
		// After checking whether download folder or not
		String ActualFoldername1 = null;

		File[] files1 = new File(DownloadPath).listFiles();

		for (File file : files1) {
			if (file.isFile()) {
				ActualFoldername1 = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername1);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize1 = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize1);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize1 != 0) {
					Log.message("Downloaded folder is available in downloads!!!");
				} else {
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if ((ActualFoldername1.contains(FolderName)) && (result1 == true) && (result2 == true) && (result3 == true)
				&& (result4 == true) && (result5 == true))
			return true;
		else
			return false;
	}

	@FindBy(css = "#liCheckoutFile1>a")

	WebElement liCheckoutFile;

	@FindBy(css = "#liCheckInFile1>a")

	WebElement liCheckInFile;

	@FindBy(xpath = "//i[@class='icon icon-check-outFiles icon-orange']")

	WebElement Checkouticon;

	@FindBy(xpath = "//input[@id='txt_changeSetName']")

	WebElement txtchangeSetName;

	@FindBy(xpath = "//textarea[@id='txt_comment']")

	WebElement txtcomment;

	@FindBy(xpath = "//button[@class='btn-primary']")

	WebElement btnprimary;

	@FindBy(xpath = "//input[@id='btn_Checkout']")

	WebElement btnCheckin;

	/**
	 * Method written for Checkout any file now checkin with new version file and
	 * comment Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Checkout_Ckeckin_File(String RevisionName, String Comment, String FilePath)	throws AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liCheckoutFile.click();
		Log.message("Check out files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Handling Download PopUp using robot
		Robot robot = null;
		robot = new Robot();
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_LEFT);
		robot.keyRelease(KeyEvent.VK_LEFT);
		SkySiteUtils.waitTill(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(50000);
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_F4);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_F4);
		SkySiteUtils.waitTill(5000);
		if (Checkouticon.isDisplayed())
		{
			result1 = true;
			Log.message("selected file has been Check out successfully");
		} 
		else 
		{
			result1 = false;
			Log.message("selected file has been Check out Unsuccessfully");
		}
		SkySiteUtils.waitTill(10000);		
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(10000);
		liCheckInFile.click();
		Log.message("Check in files has been clicked");
		SkySiteUtils.waitTill(20000);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//*[@id='commontitle2']")).click();// Clicking on header
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("Container2"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		txtchangeSetName.sendKeys(RevisionName);
		Log.message("Revision name has been entered");
		SkySiteUtils.waitTill(5000);
		txtcomment.sendKeys(Comment);
		Log.message("Comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnprimary.click();
		Log.message("Choose all button has been clicked");
		SkySiteUtils.waitTill(5000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FilePath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FilePath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(15000);
		driver.switchTo().defaultContent();
		btnCheckin.click();
		Log.message("Check in button has been clicked");
		SkySiteUtils.waitTill(15000);			
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//i[@class='icon icon-info icon-orange']")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//*[@id='tbPreviousVersion']/a")).click();
		SkySiteUtils.waitTill(5000);		
		String Revision_Count = driver.findElement(By.xpath("//*[@id='spnHistoryCount']")).getText();		
		Log.message("count is: " + Revision_Count); // Getting counts

		SkySiteUtils.waitTill(5000);
		String[] x = Revision_Count.split(" ");
		String Avl_Count = x[3];
		int CountBefore = Integer.parseInt(Avl_Count);	
		Log.message("Avl Revision count is:" + CountBefore); // Getting counts
		SkySiteUtils.waitTill(5000);		
		
		if ((CountBefore==2)&&(result1 == true))
			return true;
		else
			return false;
	}

	@FindBy(css = "#liMoveFile1>a")

	WebElement liMoveFile;

	@FindBy(xpath = "//span[@class='noty_text']")

	WebElement notytext;

	/**
	 * Method written for Checkout any file now then same file move one folder to another folder.
	 * Scripted By: Sekhar	
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Checkout_movefile() throws AWTException, IOException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liCheckoutFile.click();
		Log.message("Check out files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Handling Download PopUp using robot
		Robot robot = null;
		robot = new Robot();
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_LEFT);
		robot.keyRelease(KeyEvent.VK_LEFT);
		SkySiteUtils.waitTill(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_F4);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_F4);
		SkySiteUtils.waitTill(5000);
		if (Checkouticon.isDisplayed())
		{
			result1 = true;
			Log.message("selected file has been Check out successfully");
		} 
		else 
		{
			result1 = false;
			Log.message("selected file has been Check out Unsuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(8000);
		liMoveFile.click();
		Log.message("move files has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		if ((Notify_msg.contains("You can not move checked out files(s).")) && (result1 == true))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//i[@class='icon icon-link-files'])[3]")

	WebElement iconlinkfiles;

	/**
	 * Method written for shared link file with checkout and checkin using enable
	 * viewer. Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean sharedlinkfile_Checkout_checkin_enableviewer(String Collection_Name, String RevisionName,
			String Comment, String FilePath) throws AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;

		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liCheckoutFile.click();
		Log.message("Check out files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Handling Close turbo PopUp using robot
		Robot robot = null;
		robot = new Robot();
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_LEFT);
		robot.keyRelease(KeyEvent.VK_LEFT);
		SkySiteUtils.waitTill(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_F4);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_F4);
		SkySiteUtils.waitTill(5000);
		if (Checkouticon.isDisplayed()) 
		{
			result1 = true;
			Log.message("selected file has been Check out successfully");
		} 
		else 
		{
			result1 = false;
			Log.message("selected file has been Check out Unsuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(8000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(6000);
		chkEnableViewer.click();
		Log.message("enable viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		Log.message("files clicked");
		SkySiteUtils.waitTill(10000);
		WebElement R1 = driver.findElement(By.xpath("html/body/p/a"));
		SkySiteUtils.waitTill(20000);
		String File_Link = R1.getAttribute("href");
		Log.message("File Link is: " + File_Link);
		SkySiteUtils.waitTill(20000);
		driver.get(File_Link);
		SkySiteUtils.waitTill(20000);
		driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).click();
		Log.message("viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		String FileName = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("Act File Name is:" + FileName);
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() && (driver
				.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())) 
		{
			result2 = true;
			Log.message("Enable Viewer is Successfully!!!!!");
		}
		else
		{
			result2 = false;
			Log.message("Enable Viewer is UnSuccessfully!!!!!");
		}
		SkySiteUtils.waitTill(5000);
		driver.navigate().back();
		Log.message("Navigate the browser Back");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 60);
		clkcollection.click();
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int collCount_collList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("collCount_collList :" + collCount_collList);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collCount_collList; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message(collName);
			SkySiteUtils.waitTill(5000);
			// Checking collection name equal with each row of table
			if (collName.equals(Collection_Name)) 
			{
				// Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).click();
				Log.message("Exp Collection has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(5000);
		liCheckInFile.click();
		Log.message("Check in files has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//*[@id='commontitle2']")).click();// Clicking on header
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("Container2"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		txtchangeSetName.sendKeys(RevisionName);
		Log.message("Revision name has been entered");
		SkySiteUtils.waitTill(5000);
		txtcomment.sendKeys(Comment);
		Log.message("Comment has been entered");
		SkySiteUtils.waitTill(5000);
		btnprimary.click();
		Log.message("Choose all button has been clicked");
		SkySiteUtils.waitTill(20000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FilePath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FilePath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete())
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else 
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnCheckin.click();
		Log.message("Ckeck in button has been clicked");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		
		driver.findElement(By.xpath("//i[@class='icon icon-info icon-orange']")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//*[@id='tbPreviousVersion']/a")).click();
		SkySiteUtils.waitTill(5000);		
		String Revision_Count = driver.findElement(By.xpath("//*[@id='spnHistoryCount']")).getText();		
		Log.message("count is: " + Revision_Count); // Getting counts

		SkySiteUtils.waitTill(5000);
		String[] x = Revision_Count.split(" ");
		String Avl_Count = x[3];
		int CountBefore = Integer.parseInt(Avl_Count);	
		Log.message("Avl Revision count is:" + CountBefore); // Getting counts
		SkySiteUtils.waitTill(5000);		
		if(CountBefore==2) 
		{
			result3 = true;
			Log.message("file Check in successfully ");
		}
		else 
		{
			result3 = false;
			Log.message("file Check in Unsuccessfully ");
		}
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath(".//*[@id='div_otherInfo']/div/div/div[1]/button")).click();
		SkySiteUtils.waitTill(5000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(10000);
		drplinkfiles.click();
		Log.message("link files has been clicked");
		SkySiteUtils.waitTill(3000);
		chkEnableViewer.click();
		Log.message("enable viewer has been clicked");
		SkySiteUtils.waitTill(10000);
		sendviacommunica.click();
		Log.message("send via communication has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
		driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
		Log.message("Switch to frame");
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		Log.message("files clicked");
		SkySiteUtils.waitTill(10000);
		WebElement R2 = driver.findElement(By.xpath("html/body/p/a"));
		SkySiteUtils.waitTill(10000);
		String File_Link1 = R2.getAttribute("href");
		Log.message("File Link is: " + File_Link1);
		SkySiteUtils.waitTill(30000);
		driver.get(File_Link1);
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).click();
		Log.message("viewer has been clicked");
		SkySiteUtils.waitTill(3000);
		String FileName1 = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("Act File Name is:" + FileName1);
		SkySiteUtils.waitTill(5000);
		if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() 
			&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed()))
		{
			result4 = true;
			Log.message("Enable Viewer is Successfully!!!!!");
		}
		else
		{
			result4 = false;
			Log.message("Enable Viewer is UnSuccessfully!!!!!");
		}
		SkySiteUtils.waitTill(5000);
		if((FileName != FileName1)&&(result1 == true)&&(result2 == true)&&(result3 == true)&&(result4 == true))
			return true;
		else
			return false;
	}
		
	/**
	 * Method written for Adding Subfolder 
	 * Scripted By: Sekhar	
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_SubFolder(String SubFolderName) throws AWTException
	{
		
		SkySiteUtils.waitTill(3000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(SubFolderName);
		Log.message("Sub Folder name has been entered");
		SkySiteUtils.waitTill(6000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(15000);		
		int count = driver.findElements(By.xpath("//*[contains(@style,'minus2.gif')]")).size();
		Log.message("count is :"+ count);			
		if(count==1)
		{			
			Log.message("Menas button has been successfully");
		}
		else
		{			
			SkySiteUtils.waitTill(3000);
			btnplus.click();
			Log.message("Plus button has been clicked");
			SkySiteUtils.waitTill(3000);
			btnsubfldr.click();
			Log.message("Subfolder has been clicked");
		}
		SkySiteUtils.waitTill(3000);
		String SubFolder_name = slttreefolder.getText();
		Log.message("Act Sub Folder Name is:" + SubFolder_name);
		SkySiteUtils.waitTill(5000);
		if (SubFolder_name.contains(SubFolderName))
		{
			Log.message("Adding the Sub folder validation successfull ");
			return true;
		} 
		else 
		{
			Log.message("Adding the Sub folder validation Unsuccessfull ");
			return false;
		}

	}

	/**
	 * Method written for Adding Subfolder 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_SubFolder1(String SubFolderName) throws AWTException 
	{
		SkySiteUtils.waitTill(3000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(SubFolderName);
		Log.message("Sub Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(15000);
		int count = driver.findElements(By.xpath("//*[contains(@style,'minus2.gif')]")).size();
		Log.message("count is :"+ count);			
		if(count==1)
		{			
			Log.message("Menas button has been successfully");
		}
		else 
		{
			SkySiteUtils.waitTill(3000);
			btnplus.click();
			Log.message("Plus button has been clicked");
			SkySiteUtils.waitTill(3000);
			btnsubfldr.click();
			Log.message("Subfolder has been clicked");
		}
		String SubFolder_name = slttreefolder.getText();
		Log.message("Act Sub Folder Name is:" + SubFolder_name);
		if (SubFolder_name.contains(SubFolderName)) 
		{
			Log.message("Adding the Sub folder validation successfull ");
			return true;
		} 
		else
		{
			Log.message("Adding the Sub folder validation Unsuccessfull ");
			return false;
		}
	}

	@FindBy(xpath = "(//i[@class='icon icon-delete'])[1]")

	WebElement icondelete;

	@FindBy(xpath = "//*[@id='divRecycleItem']/div[2]/table/tbody/tr[3]/td[8]")

	WebElement btnRestore1;

	@FindBy(xpath = "(//table/tbody/tr[2]/td[2]/table/tbody/tr/td[1]/div)[2]")

	WebElement btnplus;

	@FindBy(xpath = "(//table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span)[2]")

	WebElement btnsubfldr;

	/**
	 * Method written for Remove SubFolder with file and restore SubFolder
	 *  Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Remove_SubFolderwithfile_Restore() throws AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("More option button has been clicked");
		SkySiteUtils.waitTill(3000);
		icondelete.click();
		Log.message("Remove Folder button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		if (Notify_msg.contains("Folder(s) and subfolder(s) are successfully deleted.")) 
		{
			result1 = true;
			Log.message("Sub Folder Deleted successfully");
		} 
		else 
		{
			result1 = false;
			Log.message("Sub Folder Deleted Unsuccessfully");
		}
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("More option button has been clicked");
		SkySiteUtils.waitTill(3000);
		icondelete.click();
		Log.message("Remove Folder button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg1 = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg1);
		if (Notify_msg1.contains("Folder(s) and subfolder(s) are successfully deleted."))
		{
			result2 = true;
			Log.message("Folder Deleted successfully");
		}
		else 
		{
			result2 = false;
			Log.message("Folder Deleted Unsuccessfully");
		}
		SkySiteUtils.waitTill(8000);
		btnmyprofile.click();
		Log.message("Myprofile button has been clicked");
		SkySiteUtils.waitTill(3000);
		liRecycle.click();
		Log.message("Recycle button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		btnRestore1.click();
		Log.message("Restore button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg2 = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg2);
		if (Notify_msg2.contains("Successfully restored")) 
		{
			result3 = true;
			Log.message("Sub Folder Restore successfully");
		}
		else
		{
			result3 = false;
			Log.message("Sub Folder Restore Unsuccessfully");
		}
		SkySiteUtils.waitTill(8000);
		btndocuments.click();
		Log.message("Document button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		btnplus.click();
		Log.message("Plus button has been clicked");
		SkySiteUtils.waitTill(3000);
		btnsubfldr.click();
		Log.message("Subfolder has been clicked");
		SkySiteUtils.waitTill(3000);
		if ((driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).isDisplayed())
				&& (result1 == true) && (result2 == true) && (result3 == true))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//i[@class='icon icon-view-files'])[2]")

	WebElement iconviewfiles;

	/**
	 * Method written for ViewFile Filelevel using more option. Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean ViewFile_Filelevel() throws AWTException
	{
		SkySiteUtils.waitTill(3000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(5000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(5000);
		iconviewfiles.click();
		Log.message("View files button has been clicked");
		SkySiteUtils.waitTill(20000);
		String parentHandle = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		String View_FileName = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("View File Name is:" + View_FileName);
		SkySiteUtils.waitTill(5000);
		if ((driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() &&
				(driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())))
		{
			Log.message("FileView Successfully ");
			return true;
		} 
		else 
		{
			Log.message("FileView UnSuccessfully ");
			return false;
		}
	}

	/**
	 * Method written for ViewFile Filelevel using more option. Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean CopyFile_UploadFilewithNewCopy(String FolderPath) throws AWTException, IOException
	{	
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(8000);
		radionewcopy.click();
		Log.message("newcopy radio button has been clicked");
		SkySiteUtils.waitTill(5000);
		btnselctFile.click();
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete())
			{
				Log.message(file.getName() + " is deleted!");
			}
			else 
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle1);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
		{
			Log.message("Upload file with new copy validation Successfull");
			return true;
		} 
		else 
		{
			Log.message("Upload file with new copy validation UnSuccessfull");
			return false;
		}
	}

	@FindBy(xpath = "(//i[@class='icon icon-copy-files'])[2]")

	WebElement iconcopyfiles;

	@FindBy(xpath = "(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[2]")

	WebElement clkstfolder;

	@FindBy(xpath = "//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")

	WebElement clksubrootfld;

	@FindBy(xpath = "//*[@id='popup']/div/div/div[3]/div[2]/input[1]")

	WebElement clkOk;

	/**
	 * Method written for Copy files within Project. Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean CopyFiles_WithinProject() throws AWTException
	{
		boolean result1 = false;	
		SkySiteUtils.waitTill(3000);
		clkstfolder.click();
		Log.message("1 st folder has been clicked");
		SkySiteUtils.waitTill(3000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(2000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		iconcopyfiles.click();
		Log.message("Copyfiles has been clicked");
		SkySiteUtils.waitTill(5000);
		Select drprole = new Select(driver.findElement(By.id("ddlCopyOption")));
		drprole.selectByVisibleText("Create new copy");
		SkySiteUtils.waitTill(3000);
		clksubrootfld.click();
		Log.message("select copied folder has been clicked");
		SkySiteUtils.waitTill(3000);
		clkOk.click();
		Log.message("Ok button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		if (Notify_msg.contains("File(s) successfully copied")) 
		{
			result1 = true;
		}
		else 
		{
			result1 = false;
		}
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switch to Frame");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")).click();// click on 2nd Folder
		SkySiteUtils.waitTill(3000);
		String After_CopyFile = driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).getText();
		Log.message("After Copy File name is :" + After_CopyFile);
		if ((driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]"))).isDisplayed()
				&& (result1 == true)) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	/**
	 * Method written for select folder 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Select_Folder(String FolderName) throws AWTException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		// Counting available folders
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[2]")).click();
		SkySiteUtils.waitTill(3000);
		int Folders_Count = 0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])"));
		for (WebElement Element : allElements) 
		{
			Folders_Count = Folders_Count + 1;
		}
		Log.message("Folder Count: " + Folders_Count);
		// Getting Folder Names and machine
		for (i = 2; i <= Folders_Count; i++) 
		{
			String Exp_FolderName = driver.findElement(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])[" + i + "]")).getText();
			Log.message("Exp Folder Name: " + Exp_FolderName);
			if (Exp_FolderName.contentEquals(FolderName))
			{
				result1 = true;
				Log.message("Exp Folder is Available");
				break;
			}
		}
		if (result1 == true)
		{
			// select the Folder
			driver.findElement(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])[" + i + "]")).click();// Select Folder
			Log.message("selected Folder has been clicked ");
			SkySiteUtils.waitTill(10000);
		} 
		else
		{
			result1 = false;
			Log.message("Folder is Not Available.....");
		}
		// Final Condition
		if ((result1 == true))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method written for Copy files to other project with create new copy option
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean CopyFiles_different_Project(String projectName1, String FolderName1) throws AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='chkAllProjectdocs']")).click();// select check box
		Log.message("all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//i[@class='icon icon-option ico-lg'])[3]")).click();// click on More Option
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//i[@class='icon icon-copy-files'])[2]")).click();// click on Copy files
		Log.message("copyfiles button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//button[@id='btnProjectListSearch']")).click();// select the project Target
		Log.message("target project has been clicked");
		SkySiteUtils.waitTill(3000);

		int Project_Count = 0;
		int i = 0;
		List<WebElement> allElements = driver
				.findElements(By.xpath("//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label"));
		for (WebElement Element : allElements) 
		{
			Project_Count = Project_Count + 1;
		}
		Log.message("Project Count is: " + Project_Count);
		// Getting Project Names and machine
		for (i = 1; i <= Project_Count; i++) 
		{
			String Exp_ProjectName1 = driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)[" + i + "]"))
					.getText();
			Log.message("Exp Project Name is: " + Exp_ProjectName1);

			if (Exp_ProjectName1.contains(projectName1)) 
			{
				result1 = true;
				Log.message("Select Project is Available");
				driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)[" + i + "]")).click();
				Log.message("Exp project has been clicked");
				SkySiteUtils.waitTill(3000);
				break;
			}
		}
		if (result1 == true) 
		{
			Log.message("Exp Project Successfully!!!!");

			int Folders_Count = 0;
			int j = 0;
			List<WebElement> allElements1 = driver.findElements(By.xpath("(//span[@class='standartTreeRow'])"));
			for (WebElement Element : allElements1)
			{
				Folders_Count = Folders_Count + 1;
			}
			Log.message("Folder Count: " + Folders_Count);
			// Getting Folder Names and machine
			for (j = 1; j <= Folders_Count; j++) 
			{
				String Exp_FolderName1 = driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + j + "]")).getText();
				Log.message("Exp Folder Name: " + Exp_FolderName1);
				if (Exp_FolderName1.contentEquals(FolderName1))
				{
					result2 = true;
					Log.message("Select Folder is Available");
					driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + j + "]")).click();
					Log.message("Exp root folder has been clicked");
					break;
				}
			}
			if (result2 == true)
			{
				// select the Folder
				Log.message("Select Folder is Successfully");
				SkySiteUtils.waitTill(5000);
				Select drprole = new Select(driver.findElement(By.id("ddlCopyOption")));
				drprole.selectByVisibleText("Create new copy");
				Log.message("Select create new copy option is Successfully");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("//*[@id='popup']/div/div/div[3]/div[2]/input[1]")).click();// click on OK Button
				Log.message("Ok button has been clicked");
				SkySiteUtils.waitTill(3000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitForElement(driver, notytext, 60);
				String Notify_msg = notytext.getText();
				Log.message("Notify Message is:" + Notify_msg);
				if (Notify_msg.contentEquals("File(s) successfully copied")) 
				{
					result3 = true;
					Log.message("Files Copied Successfully!!!");
					SkySiteUtils.waitTill(3000);
				}
				else
				{
					result3 = false;
					Log.message("Files Copied Failed!!!");
				}
			} 
			else
			{
				result2 = false;
				Log.message("Select Folder is Failed!!!");
			}
		} 
		else
		{
			result1 = false;
			Log.message("Select Project Failed!!!");
		}
		// Final Condition
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	/**
	 * Method written for delete file from Sub Folder and rename Subfolder then
	 * restore the file Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Deletefile_Subfolder_RenameSubfolder_Restorefile(String SubFolderName) throws AWTException
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		SkySiteUtils.waitTill(3000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(2000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liDeleteFile1.click();
		Log.message("Delete files button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes has been clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		if (Notify_msg.contains("File(s) successfully deleted.")) 
		{
			result1 = true;
			Log.message("Files deleted Successfull");
		}
		else
		{
			result1 = false;
			Log.message("Files deleted UnSuccessfull");
		}
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switch to Frame");
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		fdrename.click();
		Log.message("Rename folder has been clicked");
		SkySiteUtils.waitTill(3000);
		txtFolderName.clear();
		Log.message("folder name has been cleared");
		SkySiteUtils.waitTill(3000);
		txtFolderName.sendKeys(SubFolderName);
		Log.message("Sub Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String SubFolder_name = slttreefolder.getText();
		Log.message("Act Sub Folder Name is:" + SubFolder_name);
		if (SubFolder_name.contains(SubFolderName))
		{
			result2 = true;
			Log.message("Adding rename sub folder validation successfull ");
		}
		else
		{
			result2 = false;
			Log.message("Adding rename sub folder validation Unsuccessfull ");
		}
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnmyprofile.click();
		Log.message("Myprofile button has been clicked");
		SkySiteUtils.waitTill(3000);
		liRecycle.click();
		Log.message("Recycle button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		btnRestore.click();
		Log.message("Restore button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg2 = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg2);
		if (Notify_msg2.contains("Successfully restored"))
		{
			result3 = true;
			Log.message("File Restore successfully");
			SkySiteUtils.waitTill(3000);
		} 
		else
		{
			result3 = false;
			Log.message("File Restore Unsuccessfully");
		}
		SkySiteUtils.waitTill(6000);
		btndocuments.click();
		Log.message("Document button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).isDisplayed())
		{
			Log.message("Menas button has been successfully");
		} 
		else
		{
			btnplus.click();
			Log.message("Plus button has been clicked");
			SkySiteUtils.waitTill(3000);
			btnsubfldr.click();
			Log.message("Subfolder has been clicked");
			SkySiteUtils.waitTill(3000);
		}
		if ((driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).isDisplayed())
				&& (result1 == true) && (result2 == true) && (result3 == true))
			return true;
		else
			return false;
	}

	/**
	 * Method written for delete file from Sub Folder 
	 * Scripted By: Sekhar	 
	 * @return
	 * @throws AWTException
	 */
	public boolean Deletefile_Subfolder() throws AWTException 
	{
		SkySiteUtils.waitTill(3000);
		sltallchkbox.click();
		Log.message("select all check box has been clicked");
		SkySiteUtils.waitTill(2000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		liDeleteFile1.click();
		Log.message("Delete files button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes has been clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		if (Notify_msg.contains("File(s) successfully deleted."))
		{
			Log.message("Files deleted Successfull");
			return true;
		} 
		else
		{
			Log.message("Files deleted UnSuccessfull");
			return false;
		}
	}

	/**
	 * Method written for Move sub Folder 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Move_SubFolder(String SubFolderName) throws AWTException, IOException 
	{

		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		tdMoveFolder.click();
		Log.message("move folder has been clicked");
		SkySiteUtils.waitTill(3000);
		popuptree2.click();
		Log.message("popup folder has been clicked");
		SkySiteUtils.waitTill(3000);
		popupok.click();
		Log.message("popup ok has been clicked");
		SkySiteUtils.waitTill(3000);
		String After_MoveFile = driver.findElement(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span"))
				.getText();
		Log.message("After move Folder name is :" + After_MoveFile);
		SkySiteUtils.waitTill(5000);
		if (After_MoveFile.equals(SubFolderName))
			return true;
		else
			return false;
	}
	
	
	@FindBy(xpath = "//i[@class='icon icon-lite-user ico-lg']")

	WebElement btnmyprofile;

	/**
	 * Method written for move Subfolder then restore the file 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean MoveSubfolder_Restorefile() throws AWTException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnmyprofile.click();
		Log.message("Myprofile button has been clicked");
		SkySiteUtils.waitTill(3000);
		liRecycle.click();
		Log.message("Recycle button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		btnRestore.click();
		Log.message("Restore button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg2 = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg2);
		if (Notify_msg2.contains("Successfully restored"))
		{
			result1 = true;
			Log.message("File Restore successfully");
			SkySiteUtils.waitTill(3000);
		} 
		else
		{
			result1 = false;
			Log.message("File Restore Unsuccessfully");
		}
		SkySiteUtils.waitTill(6000);
		btndocuments.click();
		Log.message("Document button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(3000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).isDisplayed()) 
		{
			Log.message("Menas button has been successfully");
		} 
		else 
		{
			btnplus.click();
			Log.message("Plus button has been clicked");
			SkySiteUtils.waitTill(3000);
			btnsubfldr.click();
			Log.message("Subfolder has been clicked");
			SkySiteUtils.waitTill(3000);
		}
		if ((driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).isDisplayed())
				&& (result1 == true))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "(//i[@class='icon icon-modify-folder'])[1]")

	WebElement modifyfolder;

	/**
	 * Method written for RenameFolder and RemoveFolder 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean RenameFolder_RemoveFolder(String RenameFoldername) throws AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		modifyfolder.click();
		Log.message("Rename button has been clicked");
		SkySiteUtils.waitTill(3000);
		txtFolderName.clear();
		Log.message("folder name has been cleared");
		SkySiteUtils.waitTill(3000);
		txtFolderName.sendKeys(RenameFoldername);
		Log.message("folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(3000);
		String RenameFolder_name = slttreefolder.getText();
		Log.message("Act Rename Folder Name is:" + RenameFolder_name);
		if (RenameFolder_name.contains(RenameFoldername)) 
		{
			result1 = true;
			Log.message("Adding rename folder validation successfull ");
		} 
		else
		{
			result1 = false;
			Log.message("Adding rename folder validation Unsuccessfull ");
		}
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		lidelete.click();
		Log.message("Remove folder has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify message is:" + Notify_msg);
		//Notify_msg.contains("If any file of this folder present in package it will automatically remove from the package")
		if(Notify_msg.contains("Are you sure you want to delete the folder: '"+RenameFoldername+"'? (Note: Deleting will also remove this file from any packages)"))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//span[@id='lblProjectName']")

	WebElement lblProjectName;

	@FindBy(xpath = "//span[@id='lblProjectNumber']")

	WebElement lblProjectNumber;

	@FindBy(xpath = "//span[@id='lblProjectAddress']")

	WebElement lblProjectAddress;

	@FindBy(xpath = "//span[@id='lblCity']")

	WebElement lblCity;

	@FindBy(xpath = "//span[@id='lblZip']")

	WebElement lblZip;

	@FindBy(xpath = "//span[@id='lblState']")

	WebElement lblState;

	@FindBy(xpath = "//span[@id='lblCountry']")

	WebElement lblCountry;

	/**
	 * Method written for Accepted Collection from employee Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean New_Collection_Invitation(String Editcollection_Name, String Address, String city, String Zip,
			String Country, String state) throws IOException
	{

		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		SkySiteUtils.waitTill(5000);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++)
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]"))
					.getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + Editcollection_Name);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(Editcollection_Name)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]")).click();
		Log.message("Exp collection has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		String Editname = lblProjectName.getText();
		Log.message("collection name is:" + Editname);
		SkySiteUtils.waitTill(2000);
		String Editnumber = lblProjectNumber.getText();
		Log.message("collection number is:" + Editnumber);
		SkySiteUtils.waitTill(2000);
		String Editaddress = lblProjectAddress.getText();
		Log.message("Edit address is:" + Editaddress);
		SkySiteUtils.waitTill(2000);
		String Editcity = lblCity.getText();
		Log.message("Edit city is:" + Editcity);
		SkySiteUtils.waitTill(2000);
		String Editzip = lblZip.getText();
		Log.message("Edit zip is:" + Editzip);
		SkySiteUtils.waitTill(2000);
		String Editstate = lblState.getText();
		Log.message("Edit state is:" + Editstate);
		SkySiteUtils.waitTill(2000);
		String Editcountry = lblCountry.getText();
		Log.message("Edit country is:" + Editcountry);
		SkySiteUtils.waitTill(3000);
		if ((Editcollection_Name.contains(Editname)) && (Editcollection_Name.contains(Editnumber))
				&& (Address.contains(Editaddress)) && (city.contains(Editcity)) && (Zip.contains(Editzip))
				&& (state.contains(Editstate)) && (Country.contains(Editcountry)))
			return true;
		else
			return false;

	}

	/**
	 * Method written for Accepted Collection from owner Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Accepted_Collection_FromOwner(String CollectionName, String Edit_Colle_Name, String Descrip,
			String Address, String city, String Zip, String Country, String state) throws IOException
	{

		SkySiteUtils.waitTill(5000);

		int collection_Invi_check_count = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collection_Invi_check_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collection_Invi_check_count; i++)
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]"))
					.getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName))
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)[" + i + "]")).click();
		Log.message("Exp collection check box has been clicked");
		SkySiteUtils.waitTill(5000);
		btnButton1.click();
		Log.message("accept selected collections button has been clicked");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		Log.message("Notify Message is:" + Notify);
		SkySiteUtils.waitTill(1000);
		if (Notify.equals("Your response successfully saved for the selected collection.")) 
		{
			Log.message("Exp Accepted collection invitation successfull");
			SkySiteUtils.waitTill(5000);
		}
		else
		{
			Log.message("Exp Accepted collection invitation Unsuccessfull");
			SkySiteUtils.waitTill(5000);
		}
		SkySiteUtils.waitTill(5000);
		clkcollection.click();
		Log.message("collections has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		int collection_count = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collection_count);
		// Loop start '2' as projects list start from tr[2]
		int j = 0;
		for (j = 1; j <= collection_count; j++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + j + "]")).getText();
			Log.message("Exp Collection name is:" + Collection);
			SkySiteUtils.waitTill(5000);

			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection.contains(CollectionName)) 
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)[" + j + "]")).click();
		Log.message("Exp Check box has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);
		txtProjectName.clear();
		Log.message("Collection name has been cleared");
		SkySiteUtils.waitTill(2000);
		Log.message("Edit Collection name is:" + Edit_Colle_Name);
		SkySiteUtils.waitTill(2000);
		txtProjectName.sendKeys(Edit_Colle_Name);
		Log.message("Collection name has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.clear();
		Log.message("Collection number has been cleared");
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(Edit_Colle_Name);
		Log.message("Collection number has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(5000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(5000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);		
		txtProjectDesc.clear();
		Log.message("Description has been cleared");
		SkySiteUtils.waitTill(5000);
		txtProjectDesc.sendKeys(Descrip);
		Log.message("Description has been entered");
		SkySiteUtils.waitTill(5000);
		txtProjectAddress1.clear();
		txtProjectAddress1.sendKeys(Address);
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectCity.clear();
		txtProjectCity.sendKeys(city);
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectZip.clear();
		txtProjectZip.sendKeys(Zip);
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(2000);	
		Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText(Country);
		Log.message("Country has been entered");
		SkySiteUtils.waitTill(5000);
		// String state = PropertyReader.getProperty("CollectionState");
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText(state);
		Log.message("state has been selected");
		SkySiteUtils.waitTill(5000);
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);

		int collectioncheck = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck);
		// Loop start '2' as projects list start from tr[2]
		int k = 0;
		for (k = 1; k <= collectioncheck; k++)
		{
			// x path as dynamically providing all rows(j) tr[j]
			String Edit_Col_name = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + k + "]")).getText();
			Log.message("Exp Edit Collec name is:" + Edit_Col_name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Edit Collection name is:" + Edit_Colle_Name);
			// Checking collection name equal with each row of table
			if (Edit_Col_name.contains(Edit_Colle_Name))
			{
				Log.message("Exp Edit Collection name has been validated!!!");
				break;
			}
		}
		String Edit_coll = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + k + "]")).getText();
		Log.message("After Edit Collection name is:" + Edit_coll);
		SkySiteUtils.waitTill(5000);
		if (Edit_Colle_Name.contains(Edit_coll))
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validation for Edit collection Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Validation_Edit_collection(String Collection_Name)
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, drpSetting, 60);
		SkySiteUtils.waitTill(3000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("prjCount_prjList :" + prjCount_prjList);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= prjCount_prjList; i++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(5000);
			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name))
			{
				Log.message("Select Collection edit button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		// Click on project name where it equal with specified project name
		String Edit_collection = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + i + "]")).getText();
		if (Edit_collection.contains(Collection_Name))
			return true;
		else
			return false;
	}

	/**
	 * Method written for Accepted Collection from second admin 
	 * Scripted By: Sekhar	
	 * @return
	 */
	public boolean Validation_Collections_From_Secondadmin(String CollectionName) throws IOException
	{

		SkySiteUtils.waitTill(5000);
		int collectioncheck_count = driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:" + collectioncheck_count);
		// Loop start '2' as projects list start from tr[2]
		int i = 0;
		for (i = 1; i <= collectioncheck_count; i++) 
		{
			// x path as dynamically providing all rows(i) tr[i]
			String Collection_Name = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]")).getText();
			Log.message("Exp Collection name is:" + Collection_Name);
			SkySiteUtils.waitTill(5000);
			Log.message("Act Collection name is:" + CollectionName);
			// Checking collection name equal with each row of table
			if (Collection_Name.contains(CollectionName))
			{
				Log.message("Exp Collection name has been validated!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(3000);
		String Edit_Collection = driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])[" + i + "]")).getText();
		Log.message("Exp Collection name is:" + Edit_Collection);
		if (Edit_Collection.contains(CollectionName))
			return true;
		else
			return false;
	}

	@FindBy(xpath = "//*[@id='divInvitedProjects']/div/div/div[1]")
	WebElement accpopup;

	@FindBy(xpath = "//*[@id='divInvitedProjects']/div/div/div[1]/button")
	WebElement accpopupclose;

	/**
	 * Method written for display Accepted popup 
	 * Scripted By: Sekhar	
	 * @return
	 */
	public boolean Accepted_Popup_close() throws IOException
	{
		SkySiteUtils.waitTill(3000);
		if (accpopup.isDisplayed())
		{
			accpopupclose.click();
			Log.message("Accepted popup close button is clicked");
			SkySiteUtils.waitTill(3000);
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	@FindBy(xpath = "//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	WebElement accpopup1;

	@FindBy(xpath = "//input[@id='Button1' and @class='btn btn-secondary']")
	WebElement btnButton;
	
	/**
	 * Method written for display Accepted popup 
	 * Scripted By: Sekhar	
	 * @return
	 */
	public boolean Accepted_Popup_close1() throws IOException
	{
		SkySiteUtils.waitTill(3000);
		if (accpopup.isDisplayed())
		{
			accpopup1.click();
			Log.message("Accepted popup close button is clicked");
			btnButton.click();
			Log.message("Accepted selected clicked");
			SkySiteUtils.waitTill(5000);
			return true;
		}
		else 
		{
			return false;
		}
	}

	/** 
 	* Method written for Validate new collection
 	* Scripted By: Sekhar
 	* @return
 	*/	
	public boolean Validate_New_Collection_Invitation(String CollectionName) throws IOException 
	{	
			
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		int collectioncheck_count=driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:"+collectioncheck_count);			 
		SkySiteUtils.waitTill(5000);
		//Loop start '2' as projects list start from tr[2]
		int i=0;
		for(i = 1; i <= collectioncheck_count; i++) 
		{
			//x path as dynamically providing all rows(i) tr[i] 		
			String Collection_Name=driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])["+i+"]")).getText();
			Log.message("Exp Collection name is:"+Collection_Name);
			SkySiteUtils.waitTill(5000);
			
			Log.message("Act Collection name is:"+CollectionName);
			//Checking collection name equal with each row of table
			if(Collection_Name.contains(CollectionName))
			{	        	 
				Log.message("Exp Collection name has been validated!!!");					
				break;
			}		
		}
		SkySiteUtils.waitTill(5000);
		String Collec_Name=driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])["+i+"]")).getText();
		SkySiteUtils.waitTill(5000);
		if(Collec_Name.contains(CollectionName))	
			return true;
		else
			return false;
		
	}
	
	 @FindBy(xpath="//button[@id='titleStat']")
	 WebElement titleStat;
	 
	 @FindBy(xpath="//*[@id='ancInactiveProjects']")
	 WebElement ancInactiveProjects;
	 
	 @FindBy(xpath="//*[@id='ancActiveProjects']")
	 WebElement ancActiveProjects;
	
	/** 
   	 * Method written for Validation for Edit collection
   	 * Scripted By: Sekhar
   	 * @return
   	 */	
   	public boolean Collection_Edit_Setting(String Collection_Name) 
   	{		
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
   		SkySiteUtils.waitTill(3000);
   		clkcollection.click();//click on collection Selection 
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("prjCount_prjList :"+prjCount_prjList);			 
   		//Loop start '2' as projects list start from tr[2]
   		int i=0;
   		for(i = 1; i <= prjCount_prjList; i++) 
   		{
   			//x path as dynamically providing all rows(prj) tr[prj] 		
   			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
   			Log.message(prjName);
   			SkySiteUtils.waitTill(5000);  				
   			
   			//Checking project name equal with each row of table
   			if(prjName.equals(Collection_Name))
   			{	        	 
   				Log.message("Select Collection edit button has been clicked!!!");		                  
   				break;
   	        }		
   		}
   		SkySiteUtils.waitTill(5000);  
   		//Click on project name edit button where it equal with specified project name
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+i+"]")).click();//clickon edit button
   		SkySiteUtils.waitTill(5000);
   		String parentHandle = driver.getWindowHandle(); 
		Log.message(parentHandle);                           
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}  
		SkySiteUtils.waitTill(5000);
		clneditsetting.click();
		Log.message("edit collection setting button has been clicked.");	
   		SkySiteUtils.waitTill(5000);
   		Select drpOnHold =new Select(driver.findElement(By.id("ddlProjectStatus")));
		drpOnHold.selectByVisibleText("On Hold");
		Log.message("On Hold has been selected");
		SkySiteUtils.waitTill(5000);		
		btnSave.click();
		Log.message("save change & close has been selected");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(parentHandle);//Switch back to page
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame	
		titleStat.click();
		Log.message("Active collection button has been selected");
		SkySiteUtils.waitTill(3000);
		ancInactiveProjects.click();
		Log.message("Inactive collection button has been selected");
		SkySiteUtils.waitTill(3000);
		int collectionCount=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection Count is:"+collectionCount);			 
   		//Loop start '2' as projects list start from tr[2]
   		int j=0;
   		for(j = 1; j <= collectionCount; j++) 
   		{
   			//x path as dynamically providing all rows(prj) tr[prj] 		
   			String collName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[2]/a)["+j+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(5000);  				
   			
   			//Checking project name equal with each row of table
   			if(collName.equals(Collection_Name))
   			{	        	 
   				Log.message("Select Collection edit button has been clicked!!!");		                  
   				break;
   	        }		
   		}
   		SkySiteUtils.waitTill(5000); 
   		String collName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[2]/a)["+j+"]")).getText();
   		SkySiteUtils.waitTill(5000); 
   		if(collName.contains(Collection_Name))
   			return true;
   		else 
   			return false;	
   	}	
	
   	/** 
 	* Method written for Accepted Collection from employee
 	* Scripted By: Sekhar
 	* @return
 	*/	
	public boolean Accepted_Collection_FromEmployee_Secondadmin(String CollectionName,String uName1,String pWord1,String uName2,String pWord2,String uName3,String pWord3,String uName4,String pWord4) throws IOException 
	{	
		
		boolean result1=false;
		boolean result2=false;
		boolean result3=false;
		boolean result4=false;		
		boolean result5=false;
		
		
		SkySiteUtils.waitTill(5000);			
		int collectioncheck_count=driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:"+collectioncheck_count);			 
		//Loop start '2' as projects list start from tr[2]
		int i=0;
		for(i = 1; i <= collectioncheck_count; i++) 
		{
			//x path as dynamically providing all rows(i) tr[i] 		
			String Collection_Name=driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])["+i+"]")).getText();
			Log.message("Exp Collection name is:"+Collection_Name);
			SkySiteUtils.waitTill(5000);
			
			Log.message("Act Collection name is:"+CollectionName);
			//Checking collection name equal with each row of table
			if(Collection_Name.contains(CollectionName))
			{	        	 
				Log.message("Exp Collection name has been validated!!!");					
				break;
			}		
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
		Log.message("Exp collection check box has been clicked");		
		SkySiteUtils.waitTill(5000);	
		btnButton1.click();
		Log.message("accept selected collections button has been clicked");		
		SkySiteUtils.waitTill(3000);	
		SkySiteUtils.waitForElement(driver,notifymsg, 60);      		
		String Notify=notifymsg.getText();
		Log.message("Notify Message is:"+Notify);
		SkySiteUtils.waitTill(1000);                  			
		if(Notify.equals("Your response successfully saved for the selected collection."))					
		{	
			result1=true;
		}
		else								
		{
			result1=false;
		}
		SkySiteUtils.waitTill(5000);
		
		this.Accepted_Popup_close();	
		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();   	
   		SkySiteUtils.waitTill(3000);
   		clkcollection.click();//click on collections
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String File_Count=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("count is: "+File_Count); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] x= File_Count.split(" ");
        String Avl_Count=x[3];        
        int Totalcount=Integer.parseInt(Avl_Count);  		
        Log.message("Total Avl count is:"+Totalcount); //Getting counts        
       
        this.logout();
        this.loginWithEmpuser(uName1,pWord1);  
        
        
        
        int collectiocheckcount=driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:"+collectiocheckcount);			 
		//Loop start '2' as projects list start from tr[2]
		int j=0;
		for(j = 1; j <= collectiocheckcount; j++) 
		{
			//x path as dynamically providing all rows(i) tr[i] 		
			String Collection_Name1=driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])["+j+"]")).getText();
			Log.message("Exp Collection name is:"+Collection_Name1);
			SkySiteUtils.waitTill(5000);
			
			Log.message("Act Collection name is:"+CollectionName);
			//Checking collection name equal with each row of table
			if(Collection_Name1.contains(CollectionName))
			{	        	 
				Log.message("Exp Collection name has been validated!!!");					
				break;
			}		
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();
		Log.message("Exp collection check box has been clicked");		
		SkySiteUtils.waitTill(5000);	
		btnButton1.click();
		Log.message("accept selected collections button has been clicked");		
		SkySiteUtils.waitTill(3000);	
		SkySiteUtils.waitForElement(driver,notifymsg, 60);      		
		String Notify1=notifymsg.getText();
		Log.message("Notify Message1 is:"+Notify1);
		SkySiteUtils.waitTill(1000);                  			
		if(Notify1.equals("Your response successfully saved for the selected collection."))					
		{	
			result2=true;
		}
		else								
		{
			result2=false;
		}
		SkySiteUtils.waitTill(5000);		
		this.Accepted_Popup_close();
		
		SkySiteUtils.waitTill(5000);		
   		clkcollection.click();//click on collections
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String collepagcount=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepagcount); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] y= collepagcount.split(" ");
        String Avl_Count1=y[3];        
        int Totalcount1=Integer.parseInt(Avl_Count1);  		
        Log.message("Total Avl count is:"+Totalcount1); //Getting counts   
        
        this.logout();
        
        this.loginWithEmpuser(uName2,pWord2);  
        
        
        
        int collectiocheckc=driver.findElements(By.xpath("//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("collection check box Count is:"+collectiocheckc);			 
		//Loop start '2' as projects list start from tr[2]
		int k=0;
		for(k = 1; k <= collectiocheckc; k++) 
		{
			//x path as dynamically providing all rows(i) tr[i] 		
			String Collection_Name2=driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]/a[2])["+k+"]")).getText();
			Log.message("Exp Collection name is:"+Collection_Name2);
			SkySiteUtils.waitTill(5000);
			
			Log.message("Act Collection name is:"+CollectionName);
			//Checking collection name equal with each row of table
			if(Collection_Name2.contains(CollectionName))
			{	        	 
				Log.message("Exp Collection name has been validated!!!");					
				break;
			}		
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[1]/img)["+k+"]")).click();
		Log.message("Exp collection check box has been clicked");		
		SkySiteUtils.waitTill(5000);	
		btnButton1.click();
		Log.message("accept selected collections button has been clicked");		
		SkySiteUtils.waitTill(3000);	
		SkySiteUtils.waitForElement(driver,notifymsg, 60);      		
		String Notify2=notifymsg.getText();
		Log.message("Notify Message2 is:"+Notify2);
		SkySiteUtils.waitTill(1000);                  			
		if(Notify2.equals("Your response successfully saved for the selected collection."))					
		{	
			result3=true;
		}
		else								
		{
			result3=false;
		}
		SkySiteUtils.waitTill(5000);		
		this.Accepted_Popup_close();
		
		SkySiteUtils.waitTill(5000);		
   		clkcollection.click();//click on collections
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String collepagcount1=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepagcount1); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] h= collepagcount1.split(" ");
        String Avl_Count3=h[3];        
        int Totalcount2=Integer.parseInt(Avl_Count3);  		
        Log.message("Total Avl count is:"+Totalcount2); //Getting counts   
        
        this.logout();        
        
        this.loginWithEmpuser(uName3,pWord3);
        this.Accepted_Popup_close();
        this.Collection_Edit_Setting(CollectionName);
        this.logout();
        
        this.loginWithEmpuser(uName4,pWord4);
        
        this.Accepted_Popup_close();
        
        SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();   	
   		//SkySiteUtils.waitTill(3000);
   		clkcollection.click();//click on collections
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String collepagco=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepagco); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] z= collepagco.split(" ");
        String Avl_Count2=z[3];        
        int Totalcount3=Integer.parseInt(Avl_Count2);  		
        Log.message("Total Avl count is:"+Totalcount3); //Getting counts   
        
        if((Totalcount-1)==Totalcount3)
        {
        	result4=true;
        }	
        else
        {
        	result4=false;
        }
        
        this.logout();        
        
        this.loginWithEmpuser(uName3,pWord3);
        this.Accepted_Popup_close();
        this.Collection_Inactive_Active(CollectionName);
        this.logout();         
        
        this.loginWithEmpuser(uName4,pWord4);
        
        this.Accepted_Popup_close();
        
        SkySiteUtils.waitTill(5000);
		clkcollection.click();//click on collections
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String collep=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collep); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] a= collep.split(" ");
        String Avl_Co=a[3];        
        int Totalempcount=Integer.parseInt(Avl_Co);  		
        Log.message("Total Avl count is:"+Totalempcount); //Getting counts   
        if(Totalempcount==Totalcount)
        	return true;
        else
        	return false;
       
	} 	
   	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	/** 
     * Method written for doing login Employee a/c with valid credential.
    * @throws AWTException 
     */
    public boolean loginWithEmpuser(String uName,String pWord)
    {
           SkySiteUtils.waitTill(5000);
           if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
  		 	{
  			 	driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
  		 	}
           SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);          
           txtBoxUserName.clear();
           txtBoxUserName.sendKeys(uName);
           Log.message("Username: " + uName + " " + "has been engtered in Username text box." );
           txtBoxPassword.clear();
           txtBoxPassword.sendKeys(pWord);
           Log.message("Password: " + pWord + " " + "has been engtered in Password text box." );
           SkySiteUtils.waitForElement(driver, buttonLogin, 20);
           buttonLogin.click();
           Log.message("LogIn button clicked.");      
           SkySiteUtils.waitTill(5000);                           
           String Exptitle = "SKYSITE Facilities & Archive";//Expected Page title            
           String Acttitle_AfterLogin = driver.getTitle();//Getting Actual Page title
           SkySiteUtils.waitTill(5000);
           /*int Feedback_Alert = driver.findElements(By.xpath("//span[@id='btnlater']")).size();//Check feedback alert comes
  			Log.message("Checking feedback alert is there or not : "+Feedback_Alert);
  			if(Feedback_Alert>0)
  			{
  			driver.findElement(By.xpath("//span[@id='btnlater']")).click();//Click on 'Ask me later' link
  			Log.message("Clicked on 'Ask me later' link from feedback!!!");
  			}*/
          
           if(driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[1]/h4")).isDisplayed())
           {
        	   driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[2]/div[1]/div[2]/div")).click();
        	   driver.findElement(By.xpath("//*[@id='autofeedback']")).sendKeys("Hi");
        	   driver.findElement(By.xpath("//*[@id='Savefeedback']")).click();
        	   SkySiteUtils.waitTill(8000);
           }
           Log.message("Login Validation:");        
           if(Acttitle_AfterLogin.equals(Exptitle))
                  return true;         
           else
                  return false;
    }      
		
    /** 
   	 * Method written for Validation for Inactive and Active collection
   	 * Scripted By: Sekhar
   	 * @return
   	 */	
   	public boolean Collection_Inactive_Active(String Collection_Name) 
   	{		
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
   		SkySiteUtils.waitTill(3000);   		
		clkcollection.click();//click on collections
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String collepagcount=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepagcount); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] x= collepagcount.split(" ");
        String Avl_Count=x[3];        
        int TotalBeforecount=Integer.parseInt(Avl_Count);  		
        Log.message("Total Avl count is:"+TotalBeforecount); //Getting counts   
      
        SkySiteUtils.waitTill(5000); 
		titleStat.click();
		Log.message("Active collection button has been selected");
		SkySiteUtils.waitTill(3000);
		ancInactiveProjects.click();
		Log.message("Inactive collection button has been selected");
		SkySiteUtils.waitTill(3000);
		int collectionCount=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection Count is:"+collectionCount);			 
   		//Loop start '2' as projects list start from tr[2]
   		int j=0;
   		for(j = 1; j <= collectionCount; j++) 
   		{
   			//x path as dynamically providing all rows(prj) tr[prj] 		
   			String collName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[2]/a)["+j+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(5000);  				
   			
   			//Checking project name equal with each row of table
   			if(collName.equals(Collection_Name))
   			{	        	 
   				Log.message("Select Collection edit button has been clicked!!!");		                  
   				break;
   	        }		
   		}
   		SkySiteUtils.waitTill(5000); 
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[10])["+j+"]")).click();
   		Log.message("edit button has been clicked");   
   		SkySiteUtils.waitTill(5000); 
   		String parentHandle = driver.getWindowHandle(); 
		Log.message(parentHandle);                           
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}  
		SkySiteUtils.waitTill(5000);
		clneditsetting.click();
		Log.message("edit collection setting button has been clicked.");	
   		SkySiteUtils.waitTill(5000);
   		Select drpActive =new Select(driver.findElement(By.id("ddlProjectStatus")));
   		drpActive.selectByVisibleText("Active");
		Log.message("Active has been selected");
		SkySiteUtils.waitTill(5000);		
		btnSave.click();
		Log.message("save change & close has been selected");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(parentHandle);//Switch back to page
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame	
		titleStat.click();
		Log.message("Title Active collection button has been selected");
		SkySiteUtils.waitTill(3000);
		ancActiveProjects.click();
		Log.message("Active collections button has been selected");
		SkySiteUtils.waitTill(3000);			
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);   		
   		String collepag=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepag); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] y= collepag.split(" ");
        String Avl_Count1=y[3];        
        int TotalAftercount=Integer.parseInt(Avl_Count1);  		
        Log.message("Total Avl count is:"+TotalAftercount); //Getting counts  
        
        if(TotalAftercount==(TotalBeforecount+1))
        	return true;
        else
        	return false;
      
   	}	
   	
   	@FindBy(xpath="//i[@class='icon icon-delete ico-lg']")
	WebElement btnicondelete;
   	
   	@FindBy(xpath=".//button[@id='btnDelete']")
	WebElement btnDeleteclose;
   	
   	@FindBy(xpath="//*[@id='libtnResendNfty']/a/i")
	WebElement libtnResendNfty;
   	
   	/** 
   	 * Method written for Validation for Delete collection
   	 * Scripted By: Sekhar
   	 * @return
   	 * @throws AWTException 
   	 * @throws IOException 
   	 */	
   	public boolean Collection_Delete(String Collection_Name,String uName1,String pWord1,String Contact_Name,String uName2,String pWord2) throws AWTException, IOException 
   	{		
   		boolean result1=false;
   		boolean result2=false;
   		
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
   		SkySiteUtils.waitTill(3000);
   		clkcollection.click();//click on collection Selection 
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);
   		   	 		
   		String collepagcount=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepagcount); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] x= collepagcount.split(" ");
        String Avl_Count=x[3];        
        int Before_Totalcount=Integer.parseInt(Avl_Count);  		
        Log.message("Total Avl count is:"+Before_Totalcount); //Getting counts  
        SkySiteUtils.waitTill(5000);
   		
   		
   		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("prjCount_prjList :"+prjCount_prjList);			 
   		//Loop start '2' as projects list start from tr[2]
   		int i=0;
   		for(i = 1; i <= prjCount_prjList; i++) 
   		{
   			//x path as dynamically providing all rows(prj) tr[prj] 		
   			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
   			Log.message(prjName);
   			SkySiteUtils.waitTill(5000);  				
   			
   			//Checking project name equal with each row of table
   			if(prjName.equals(Collection_Name))
   			{	        	 
   				Log.message("Select Collection edit button has been clicked!!!");		                  
   				break;
   	        }		
   		}
   		SkySiteUtils.waitTill(5000);  
   		//Click on project name edit button where it equal with specified project name
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();// img check box button
   		SkySiteUtils.waitTill(5000);
   		btnicondelete.click();
   		Log.message("Delete button has been clicked");	
   		SkySiteUtils.waitTill(5000); 
   		btnDeleteclose.click();	
   		Log.message("Delete and close button has been clicked");	
   		SkySiteUtils.waitTill(3000);
   		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify = notifymsg.getText();
		SkySiteUtils.waitTill(5000);
		if(Notify.contains("Selected collection(s) successfully deleted."))
		{
			result1=true;
		}
		else
		{
			result1=false;
		}
		this.logout();
		this.loginWithEmpuser(uName1,pWord1);
		this.loginValidation();
		
		SkySiteUtils.waitTill(5000);
   		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
   		SkySiteUtils.waitTill(3000);
   		clkcollection.click();//click on collection Selection 
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);
   		
   		int collec_list=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collec_list is :"+collec_list);			 
   		//Loop start '2' as projects list start from tr[2]
   		int j=0;
   		for(j = 1; j <= collec_list; j++) 
   		{
   			//x path as dynamically providing all rows(prj) tr[prj] 		
   			String ColName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+j+"]")).getText();
   			Log.message(ColName);
   			SkySiteUtils.waitTill(5000);  				
   			
   			//Checking project name equal with each row of table
   			if(ColName.equals(Collection_Name))
   			{	        	 
   				Log.message("Select Collection edit button has been clicked!!!");		                  
   				break;
   	        }		
   		}
   		SkySiteUtils.waitTill(5000);  
   		//Click on project name edit button where it equal with specified project name
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+j+"]")).click();// img check box button
   		SkySiteUtils.waitTill(5000);
   		
   		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		btnteam.click();
		Log.message("Teams button has been clicked");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		
		
		int Contactlist=driver.findElements(By.xpath("//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("Contact list is :"+Contactlist);			 
   		//Loop start '2' as projects list start from tr[2]
   		int k=0;
   		for(k = 1; k <= Contactlist; k++) 
   		{
   			//x path as dynamically providing all rows(prj) tr[prj] 		
   			String ContactName=driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[2])["+k+"]")).getText();
   			Log.message(ContactName);
   			SkySiteUtils.waitTill(5000);  				
   			
   			//Checking project name equal with each row of table
   			if(ContactName.contains(Contact_Name))
   			{	        	 
   				Log.message("Select Collection edit button has been clicked!!!");		                  
   				break;
   	        }		
   		}
   		SkySiteUtils.waitTill(5000);  
   		//Click on project name edit button where it equal with specified project name
   		driver.findElement(By.xpath("(//*[@id='divTeamGridData']/div[2]/table/tbody/tr/td[1]/img)["+k+"]")).click();// img check box button
   		SkySiteUtils.waitTill(5000);
   		
		moreoption.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(2000);
		libtnResendNfty.click();
		Log.message("resend button has been clicked");
		SkySiteUtils.waitTill(2000);
		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver, notifymsg, 30);
		String Notify1 = notifymsg.getText();
		SkySiteUtils.waitTill(5000);
		if(Notify1.contains("Notification is successfully sent."))
		{
			result2=true;
			Log.message("Notification Successfully");
		}
		else
		{
			result2=false;
			Log.message("Notification UnSuccessfully");
		}
		this.logout();
		
		this.loginWithEmpuser(uName2,pWord2);
		this.loginValidation();
		this.Accepted_Popup_close();
		
		driver.switchTo().defaultContent();
   		SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
   		SkySiteUtils.waitTill(3000);
   		clkcollection.click();//click on collection Selection 
   		Log.message("collection has been clicked.");	
   		SkySiteUtils.waitTill(5000);	
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
   		resetcollection.click();//click on reset button
   		Log.message("reset button has been clicked.");	
   		SkySiteUtils.waitTill(5000);
   		   	 		
   		String collepagcount1=driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div[1]")).getText();//Getting label counts
        Log.message("colle page count is: "+collepagcount1); //Getting  counts
        
        SkySiteUtils.waitTill(5000);       
        String[] y= collepagcount1.split(" ");
        String Avl_Count1=y[3];        
        int After_Totalcount=Integer.parseInt(Avl_Count1);  		
        Log.message("Total Avl count is:"+After_Totalcount); //Getting counts  
        SkySiteUtils.waitTill(5000);
		if(((Before_Totalcount-1)==After_Totalcount)&&(result1==true)&&(result2==true))
			return true;
		else
			return false;
		
   	}	
	
   @FindBy(css=".custom-check-label>label")
   WebElement addfavoritetit;
   
   @FindBy(css="#chkFav")
   WebElement addfavoritecheck;
   
   @FindBy(css="#btnSave")
   WebElement btnsavetab;
   
   @FindBy(css="#btnClose")
   WebElement btnclosetab;
   
   @FindBy(xpath="//*[@id='lblProjectOwnere']")
   WebElement lblProjectOwnere;
   
   @FindBy(css="#btnPrev_page")
   WebElement btnPrevpage;
   
   @FindBy(css="#btnNext_page")
   WebElement btnNextpage;
   	
	/** 
     * Method written for doing collection owner add favorite and save&close. 
     * Scripted By: Sekhar
     * @throws AWTException 
     */
    public boolean collection_Owner_Addfavorite_save_close(String Collection_Name)
    {
           boolean result1=false;
           
           SkySiteUtils.waitTill(5000);
           driver.switchTo().defaultContent();
           SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
           SkySiteUtils.waitTill(3000);
           clkcollection.click();//click on collection Selection 
           Log.message("collection has been clicked.");	
           SkySiteUtils.waitTill(5000);	
           driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
           resetcollection.click();//click on reset button
           Log.message("reset button has been clicked.");	
           SkySiteUtils.waitTill(5000);
      		     		     		
      		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
      		Log.message("prjCount_prjList :"+prjCount_prjList);			 
      		//Loop start '2' as projects list start from tr[2]
      		int i=0;
      		for(i = 1; i <= prjCount_prjList; i++) 
      		{
      			//x path as dynamically providing all rows(prj) tr[prj] 		
      			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
      			Log.message(prjName);
      			SkySiteUtils.waitTill(5000);  				
      			
      			//Checking project name equal with each row of table
      			if(prjName.equals(Collection_Name))
      			{	        	 
      				Log.message("Select Collection edit button has been clicked!!!");		                  
      				break;
      	        }		
      		}
      		SkySiteUtils.waitTill(5000);  
      		//Click on project name edit button where it equal with specified project name
      		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+i+"]")).click();// img check box button
      		SkySiteUtils.waitTill(5000);  
      		String parentHandle = driver.getWindowHandle();
    		Log.message(parentHandle);
    		for (String winHandle : driver.getWindowHandles())
    		{
    			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
    		}
    		SkySiteUtils.waitTill(3000); 
    		btnProjectOwner.click();
    		Log.message("collection Owner button has been clicked");
    		SkySiteUtils.waitTill(3000);
    		if((addfavoritecheck.isDisplayed())&&(addfavoritetit.isDisplayed())
    				&&(btnsavetab.isDisplayed())&&(btnclosetab.isDisplayed()))
    		{
    			result1=true;
    			Log.message("Validation for add favorite and save&close successfully");
    		}
    		else
    		{
    			result1=false;
    			Log.message("Validation for add favorite and save&close Unsuccessfully");
    		}
    		SkySiteUtils.waitTill(3000);
    		btnchangeOwner.click();
    		Log.message("collection Owner button has been clicked");
    		SkySiteUtils.waitTill(3000);
    		if((lblProjectOwnere.isDisplayed())&&(btnclosetab.isDisplayed())
    				&&(btnsavetab.isDisplayed())&&(result1==true))
    			return true;
    		else
    			return false;
    }      
    
   
    	
 	/** 
      * Method written for Change collection owner 
       * Scripted By: Sekhar
      * @throws AWTException 
      */
     public boolean Change_collection_owner(String Collection_Name)
     {
            boolean result1=false;
            
            SkySiteUtils.waitTill(5000);
            driver.switchTo().defaultContent();
            SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
            SkySiteUtils.waitTill(3000);
            clkcollection.click();//click on collection Selection 
            Log.message("collection has been clicked.");	
            SkySiteUtils.waitTill(5000);	
            driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
            resetcollection.click();//click on reset button
            Log.message("reset button has been clicked.");	
            SkySiteUtils.waitTill(5000);
       		     		     		
       		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
       		Log.message("prjCount_prjList :"+prjCount_prjList);			 
       		//Loop start '2' as projects list start from tr[2]
       		int i=0;
       		for(i = 1; i <= prjCount_prjList; i++) 
       		{
       			//x path as dynamically providing all rows(prj) tr[prj] 		
       			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
       			Log.message(prjName);
       			SkySiteUtils.waitTill(5000);  				
       			
       			//Checking project name equal with each row of table
       			if(prjName.equals(Collection_Name))
       			{	        	 
       				Log.message("Select Collection edit button has been clicked!!!");		                  
       				break;
       	        }		
       		}
       		SkySiteUtils.waitTill(5000);  
       		//Click on project name edit button where it equal with specified project name
       		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+i+"]")).click();// img check box button
       		SkySiteUtils.waitTill(5000);  
       		String parentHandle = driver.getWindowHandle();
     		Log.message(parentHandle);
     		for (String winHandle : driver.getWindowHandles())
     		{
     			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
     		}
     		SkySiteUtils.waitTill(3000); 
     		btnProjectOwner.click();
     		Log.message("collection Owner button has been clicked");
     		SkySiteUtils.waitTill(3000);
     		btnchangeOwner.click();
     		Log.message("collection Owner button has been clicked");
     		SkySiteUtils.waitTill(3000);     		
     		driver.findElement(By.xpath("//*[@id='UserGrid']/div[1]/table/tbody/tr[2]/td[1]")).click();
     		Log.message("Asecding button has been clicked");     		
     		SkySiteUtils.waitTill(3000);
     		if((driver.findElement(By.xpath("//*[@id='UserGrid']/div[1]/div")).isDisplayed())
     		&&(driver.findElement(By.xpath("//*[@id='UserGrid']/div[2]/table/tbody/tr[3]/td[2]")).isDisplayed()))
     			return true;
     		else
     			return false;	    		
     }      
         
     /** 
      * Method written for Change collection owner then select new collection 
      * Scripted By: Sekhar     
      * @throws AWTException 
      */
     public boolean Change_collection_Owner_SelectNewCollection(String Collection_Name,String EmployeeName)
     {
            boolean result1=false;
            
            SkySiteUtils.waitTill(5000);
            driver.switchTo().defaultContent();
            SkySiteUtils.waitForElement(driver,drpSetting, 60);	   			
            SkySiteUtils.waitTill(3000);
            clkcollection.click();//click on collection Selection 
            Log.message("collection has been clicked.");	
            SkySiteUtils.waitTill(5000);	
            driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
            resetcollection.click();//click on reset button
            Log.message("reset button has been clicked.");	
            SkySiteUtils.waitTill(5000);
       		     		     		
       		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
       		Log.message("prjCount_prjList :"+prjCount_prjList);			 
       		//Loop start '2' as projects list start from tr[2]
       		int i=0;
       		for(i = 1; i <= prjCount_prjList; i++) 
       		{
       			//x path as dynamically providing all rows(prj) tr[prj] 		
       			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
       			Log.message(prjName);
       			SkySiteUtils.waitTill(5000);  				
       			
       			//Checking project name equal with each row of table
       			if(prjName.equals(Collection_Name))
       			{	        	 
       				Log.message("Select Collection edit button has been clicked!!!");		                  
       				break;
       	        }		
       		}
       		SkySiteUtils.waitTill(5000);  
       		//Click on project name edit button where it equal with specified project name
       		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+i+"]")).click();// img check box button
       		SkySiteUtils.waitTill(5000);  
       		String parentHandle = driver.getWindowHandle();
     		Log.message(parentHandle);
     		for (String winHandle : driver.getWindowHandles())
     		{
     			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
     		}
     		SkySiteUtils.waitTill(3000); 
     		btnProjectOwner.click();
     		Log.message("collection Owner button has been clicked");
     		SkySiteUtils.waitTill(3000);
     		btnchangeOwner.click();
     		Log.message("collection Owner button has been clicked");
     		SkySiteUtils.waitTill(3000);     		
     			
     		int Owner_list=driver.findElements(By.xpath("//*[@id='UserGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
       		Log.message("Owner list count is :"+Owner_list);			 
       		//Loop start '2' as projects list start from tr[2]
       		int j=0;
       		for(j = 1; j <= Owner_list; j++) 
       		{
       			//x path as dynamically providing all rows(j) tr[j] 		
       			String Owner_Name=driver.findElement(By.xpath("(//*[@id='UserGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
       			Log.message(Owner_Name);
       			SkySiteUtils.waitTill(5000);  				
       			
       			//Checking Collection name equal with each row of table
       			if(Owner_Name.equals(EmployeeName))
       			{	        	 
       				Log.message("Select Collection edit button has been clicked!!!");		                  
       				break;
       	        }		
       		}
       		SkySiteUtils.waitTill(5000);  
       		//Click on collection name radio button where it equal with specified collection name
       		driver.findElement(By.xpath("(//*[@id='UserGrid']/div[2]/table/tbody/tr/td[1]/img)["+j+"]")).click();// img check box button
       		Log.message("Select radio button has been clicked!!!!");
       		SkySiteUtils.waitTill(3000);
       		/*btnNextpage.click();
       		Log.message("Next button has been clicked");
       		SkySiteUtils.waitTill(3000);       		
       		driver.findElement(By.xpath("//*[@id='TeamGrid']/div[2]/table/tbody/tr[2]/td[1]/img")).click();
       		Log.message("owner check box button has been clicked");
       		SkySiteUtils.waitTill(3000);*/
       		btnsavetab.click();
       		Log.message("Save changes & close button has been clicked");
       		SkySiteUtils.waitTill(3000);
       		driver.switchTo().window(parentHandle);// Switch back to folder page
       		SkySiteUtils.waitTill(3000);
    		clkcollection.click();
    		SkySiteUtils.waitTill(5000);
    		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame		
    		
    		int Collectionlist=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
       		Log.message("Collection list count is :"+Collectionlist);			 
       		//Loop start '2' as projects list start from tr[2]
       		int k=0;
       		for(k = 1; k <= Collectionlist; k++) 
       		{
       			//x path as dynamically providing all rows(j) tr[j] 		
       			String CollectionName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3])["+k+"]")).getText();
       			Log.message(CollectionName);
       			SkySiteUtils.waitTill(5000);  				
       			
       			//Checking Collection name equal with each row of table
       			if(CollectionName.equals(Collection_Name))
       			{	        	 
       				Log.message("Exp Collection has been Selected!!!");		                  
       				break;
       	        }		
       		}
       		SkySiteUtils.waitTill(5000); 
       		String Collection_Owner=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[6])["+k+"]")).getText(); 
           	Log.message(Collection_Owner);
           	SkySiteUtils.waitTill(5000);
           	if(Collection_Owner.equals(EmployeeName))
           		return true;
           	else
           		return false;      
           	
     } 
         
     
     /**
 	 * Method written for select collection 
 	 * Scripted By: Sekhar
 	 * 
 	 * @return
 	 */
 	public boolean select_collection(String Collection_Name) 
 	{
 		SkySiteUtils.waitForElement(driver, drpSetting, 30);
 		driver.switchTo().defaultContent();
 		clkcollection.click();// click on collection Selection
 		Log.message("collection has been clicked.");
 		SkySiteUtils.waitTill(5000);
 		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
 		resetcollection.click();// click on reset button
 		Log.message("reset button has been clicked.");
 		SkySiteUtils.waitTill(5000);
 		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
 		Log.message("prjCount_prjList :" + prjCount_prjList);
 		// Loop start '2' as projects list start from tr[2]
 		int i = 0;
 		for (i = 1; i <= prjCount_prjList; i++)
 		{
 			// x path as dynamically providing all rows(prj) tr[prj]
 			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" +i +"]")).getText();
 			Log.message(prjName);
 			SkySiteUtils.waitTill(5000);
 			
 			// Checking project name equal with each row of table
 			if (prjName.equals(Collection_Name)) 
 			{
 				Log.message("Select Collection edit button has been clicked!!!");
 				break;
 			}
 		}
 		SkySiteUtils.waitTill(5000);
 		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" +i+ "]")).click(); 		
 		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(Collection_Name)) 
		{
			Log.message("Create Collection successfull" + Collection_Name);
			SkySiteUtils.waitTill(2000);
			return true;
		}
		else 
		{
			Log.message("Create Collection Unsuccessfull" + Collection_Name);
			SkySiteUtils.waitTill(2000);
			return false;
		}

 	}

     
     
	// ----------------------Tarak
	// code----------------------------------------------------------------------
	// Find Contact Icon
	@FindBy(css = "#Contacts>a")
	WebElement ContactIcon;

	/**
	 * Method for verifying presence of Contact icon Scripted By : Tarak
	 */
	public boolean contactIconPresent() {
		Log.message("Check Contact Icon is Present");
		SkySiteUtils.waitForElement(driver, ContactIcon, 30);
		if (ContactIcon.isDisplayed())
			return true;

		else
			return false;
	}

	/**
	 * Method to click Contact icon Scripted by:Tarak
	 */
	public FnaContactTabPage contactClick() {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", ContactIcon);
	
			Log.message("ContactIcon button is clicked");
		

		
	
		return new FnaContactTabPage(driver).get();
	}

	/******************************************
	 * Recycle bin module
	 */

	@FindBy(xpath = ".//*[@id='liRecycle']/a")
	@CacheLookup
	WebElement recyclebinbtn;
	@FindBy(css = "#lftpnlMore")
	@CacheLookup
	WebElement Moreoption;
	@FindBy(xpath=".//*[@onclick='javascript:RemoveFolder();']")
	@CacheLookup
	WebElement Removefolderoption;

	/**
	 * Method to click Recycle bin button Scripted By:Tarak
	 * Edited By Trinanjwan on 09-11-2018
	 */
	
	@FindBy(css="#liRecycle > a:nth-child(1)")
	@CacheLookup
	WebElement Recyclebinbtn;
	
	
	
	public FnaRecyclebinTabPage clickRecyclebin() {
		SkySiteUtils.waitTill(3000);
		Log.message("Waiting for My Profile button to display");
		SkySiteUtils.waitForElement(driver, MyProfilebtn, 10);
		Log.message("My Profile button is displayed");
		MyProfilebtn.click();
		Log.message("Clicked on My Profile button");
		SkySiteUtils.waitTill(3000);
		Recyclebinbtn.click();
		Log.message("Clicked on Recycle bin button from list");
		
		SkySiteUtils.waitTill(5000);

		return new FnaRecyclebinTabPage(driver).get();
	}
   
	public FnaRecyclebinTabPage deleteFolderFromCollectionfiles() {
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("SWitched to frame");
		SkySiteUtils.waitTill(4000);
		// SkySiteUtils.waitForElement(driver, Moreoption, 50);

		Moreoption.click();
		SkySiteUtils.waitTill(4000);
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		Removefolderoption.click();
		Log.message("Remove folder button is clicked");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(6000);
		btnyes.click();
		Log.message("yes button has been clicked.");
		SkySiteUtils.waitTill(6000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", recyclebinbtn);
		// recyclebinbtn.click();
		Log.message("Recyclebin button is clicked");
		SkySiteUtils.waitTill(5000);
		// String Notify=messagefordeletion.getText();
		// Log.message("Notify Message is:"+Notify);

		return new FnaRecyclebinTabPage(driver).get();

	}
    @FindBy(xpath=".//*[@id='btnSelectFiles']")
    WebElement selectbtn;
	/**
	 * Method To UploadFileforDeletion Scripted by:Tarak
	 */
	public void UploadFileForDeletion(String FolderPath,String tempfilepath) throws AWTException, IOException {
		// boolean result1=true;
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		selectbtn.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(30000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
		//driver.findElement(By.xpath("//button[@id='btnFileUpload']")).click();//Clicking on Upload button  
  		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();
  		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}

	@FindBy(css = "#Button1")
	//@FindBy(css = "#rgtpnlMore")
    WebElement moreoptionfordelete;
	
	@FindBy(css = "#liDeleteFile1")
	//@FindBy(css = "#liDeleteFile>a")
	WebElement deletefilesoption;
	
	@FindBy(xpath = "//ul[@id='FileActionMenu']/li/a[contains( text(),'Delete')]")
	//@FindBy(css = "#liDeleteFile>a")
	WebElement deletefilesoptionLocal;
	
	@FindBy(css = "#button-1")
	WebElement yesbtnfordelete;
	
	
	@FindBy(css = "#user-info")
	WebElement MyProfileFnA;
	
	
	@FindBy(css = "#liRecycle > a:nth-child(1)")
	WebElement RecycleBindrpdwn;
	
	
	
	/**
	 * Method to delete the uploaded file Scripted by:Tarak
	 * Modified By Trinanjwan on 16-11-2018 | The more option locator keeps on changing system to system so condition is added below.
	 */
	public FnaRecyclebinTabPage deleteUploadedFileFromDocuments() {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(3000);
		WebElement uploadedfile = driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img"));
		uploadedfile.click();
		Log.message("Uploaded file is selected");
		SkySiteUtils.waitTill(3000);
		
		// More option condition

		if (moreLocal.isDisplayed()) {
			moreLocal.click();
			Log.message("Clicked on Local more");
			SkySiteUtils.waitTill(5000);
			deletefilesoptionLocal.click();
			Log.message("Delete option is clicked");
		}else {
			moreoptionfordelete.click();
			Log.message("Clicked on remote more");
			SkySiteUtils.waitTill(5000);
			deletefilesoption.click();
			Log.message("Delete option is clicked");
		}

		Log.message("delete files is clicked");
		SkySiteUtils.waitTill(8000);
		driver.switchTo().defaultContent();
		Log.message("switched to default content");
		SkySiteUtils.waitTill(5000);
		yesbtnfordelete.click();
		Log.message("yes button has been clicked.");
		SkySiteUtils.waitTill(15000);
		Actions act=new Actions(driver);
	    act.moveToElement(MyProfileFnA).click(MyProfileFnA).build().perform();
		Log.message("My profile button is clicked");
		SkySiteUtils.waitTill(8000);
		RecycleBindrpdwn.click();
		SkySiteUtils.waitTill(5000);

		return new FnaRecyclebinTabPage(driver).get();

	 }
	@FindBy(xpath = ".//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")
	WebElement foldertraverse;

	/**
	 *  Method to verify restored folder present in document page along with the file
	 * Scripted by:Tarak
	 * 
	 */
	public boolean restoredFolderPresentInCollectionFilePage(String foldername) {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(4000);
		String Deletedfilename = PropertyReader.getProperty("DeleteFilename");
		int Count_no = driver.findElements(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span")).size();
		Log.message("the number folder present are " + Count_no);
		SkySiteUtils.waitTill(5000);
		int k = 0;
		for (k = 1; k <= Count_no; k++) {

			String foldernamepresent = driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+k+"]")).getText();
			if (foldernamepresent.contains(foldername)) {
				Log.message("Restored folder is present at " + k + " position in the tree");
				driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+ k + "]")).click();
				Log.message("Folder restored is selected");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		int Itemlist = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("the number files present are " + Itemlist);
		SkySiteUtils.waitTill(5000);
		int j = 0;
		for (j = 1; j <= Itemlist; j++) {

			String filepresent=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
			if (filepresent.contains(Deletedfilename)) {
				Log.message("Restored file is present at " + j + " position in the tree");
				Log.message("file also  restored successfully and found in the folder ");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		String filepresent12=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+j+"]")).getText();
		Log.message("Restored file is " + filepresent12);
		if (filepresent12.contains(Deletedfilename))

		{
			Log.message("File is also restored and present in the folder ");

		} else {
			Log.message("File is not restored and present in the folder ");
		}
		int Countlist = driver.findElements(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span")).size();
		Log.message("the number folder present are " + Countlist);
		int i = 0;
		for (i = 1; i <= Countlist; i++) {

			String foldernamepresent = driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+ i + "]")).getText();
			if (foldernamepresent.contains(foldername)) {
				Log.message("Restored folder is present at " + i + " position in the tree");
				Log.message("Folder restored successfully and found under collection tab ");
				SkySiteUtils.waitTill(5000);
				break;
			}

		}
		String Deletedfolderincollectionlist = driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+ i + "]")).getText();
		Log.message("Restored folder is " + Deletedfolderincollectionlist);
		if (Deletedfolderincollectionlist.contains(foldername))
			return true;
		else
			return false;

	}

	/**
	 * Method to verify Restored file present in Document page
	 * 
	 */
	public boolean restoredFilePresentInDocumentPage() {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(4000);
		String Deletedfilename = PropertyReader.getProperty("DeleteFilename");
		int Count_list = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("the number files present are " + Count_list);
		SkySiteUtils.waitTill(5000);
		int i = 0;
		for (i = 1; i <= Count_list; i++) {

			String filepresent = driver
					.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])[" + i + "]")).getText();
			if (filepresent.contains(Deletedfilename)) {
				Log.message("Restored file is present at " + i + " position in the tree");
				Log.message("file restored successfully and found under collection tab ");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		String filepresent12 = driver
				.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])[" + i + "]")).getText();
		Log.message("Restored file is " + filepresent12);
		if (filepresent12.contains(Deletedfilename))
			return true;
		else
			return false;

	}

	@FindBy(xpath = ".//*[@id='btnProjectListSearch']")
	@CacheLookup
	WebElement collectioname;

	/**
	 * Method to verify Collection selected in recycle bin is equal to collection in
	 * document tab scripted by :Tarak
	 */
	public void collectionSelected() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		String collectionameselected = collectioname.getText();
		Log.message("Collection selected in document tab is" + collectionameselected);
		SkySiteUtils.waitTill(4000);
	}

	
	@FindBy(xpath="//*[@placeholder='Search']")
	WebElement textBoxSearchCollection;
	
	@FindBy(xpath=".//*[@id='btnSearch']")
	WebElement btnSearchCollection;
	
	
	/**
	 * Method written for select collection and mark it as favourite Scripted By:
	 * Sekhar,Edited by Tarak
	 * 
	 * @return
	 */
	public FnaRecyclebinTabPage selectcollectionWithFavourite(String Collection_Name) 
	{
		
		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, drpSetting, 60);
		SkySiteUtils.waitTill(8000);
		clkcollection.click(); // click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(10000);
		
		Log.message("Switching frame");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, resetcollection, 60); // Back the Switch to Frame
		resetcollection.click(); // click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(10000);
		
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();		
		Log.message("prjCount_prjList :" + prjCount_prjList);
		
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= prjCount_prjList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			
			// Log.message(prjName);
			SkySiteUtils.waitTill(5000);

			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name)) 
			{
				Log.message("Select Collection edit button has been clicked!!!");
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[2]/a)[" + prj + "]")).click();
				Log.message("favourite button clicked");
				SkySiteUtils.waitTill(4000);
				
				break;
			}
			
		}
		
		SkySiteUtils.waitForElement(driver, textBoxSearchCollection, 60);
		textBoxSearchCollection.clear();
		textBoxSearchCollection.sendKeys(Collection_Name);
		SkySiteUtils.waitForElement(driver, btnSearchCollection, 60);
		btnSearchCollection.click();
		Log.message("clicked on search collection button");
		SkySiteUtils.waitTill(5000);
				
		driver.findElement(By.xpath(".//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[3]/a")).click();
		Log.message("clicked on search collection name");
		
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		
		if (Collection.contains(Collection_Name)) 
		{
			Log.message("Select Collection successfull");
			SkySiteUtils.waitTill(2000);

		} 
		else 
		{
			Log.message("Select Collection Unsuccessfull");
			SkySiteUtils.waitTill(2000);

		}
		
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(6000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", recyclebinbtn);
		// recyclebinbtn.click();
		Log.message("Recyclebin button is clicked");
		SkySiteUtils.waitTill(5000);
		return new FnaRecyclebinTabPage(driver).get();
	}


	@FindBy(xpath=".//*[@id='btnRemoveProject']")
	WebElement btnDeleteCollection;
	
	@FindBy(xpath=".//*[@id='button-1']")
	WebElement btnYes;
	
	/**
	 * Method written for searching and then deleting collection 
	 * Scripted By: Ranadeep
	 * 
	 * @return
	 */
	public void deleteCollection(String collectionName) 
	{
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, textBoxSearchCollection, 60);
		textBoxSearchCollection.clear();
		textBoxSearchCollection.sendKeys(collectionName);
		SkySiteUtils.waitForElement(driver, btnSearchCollection, 60);
		btnSearchCollection.click();
		Log.message("clicked on search collection button");
		SkySiteUtils.waitTill(5000);
		
		List<WebElement> listOfElements = driver.findElements(By.xpath(".//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"));
		int size= listOfElements.size();
		
		if(size == 0)
		{
			Log.message("Project is not in the List");
			
			SkySiteUtils.waitForElement(driver, resetsearch, 30);
			resetsearch.click();
			Log.message("Clicked on Search Reset button");
			SkySiteUtils.waitTill(5000);
			
		}	
		else
		{

			driver.findElement(By.xpath(".//*[@id='listGrid']/div[2]/table/tbody/tr[2]/td[1]/img")).click();
			Log.message("Clicked on searced collection check box");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, btnDeleteCollection, 60);
			btnDeleteCollection.click();
			Log.message("Clicked on delete colection button");
			
			Log.message("Switching to default content");
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, btnYes, 60);
			btnYes.click();
			Log.message("Clicked on Yes button");
			SkySiteUtils.waitTill(5000);
			
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switching frame");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, resetsearch, 30);
			resetsearch.click();
			Log.message("Clicked on Search Reset button");
			SkySiteUtils.waitTill(5000);
			
		}
			
	}
	
	
	/**
	 * Method written for searching and then deleting collection 
	 * Scripted By: Ranadeep
	 * 
	 * @return
	 */
	public void deleteCollectionValidation(String collectionName) 
	{
		
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, textBoxSearchCollection, 60);
		textBoxSearchCollection.clear();
		textBoxSearchCollection.sendKeys(collectionName);
		SkySiteUtils.waitForElement(driver, btnSearchCollection, 60);
		btnSearchCollection.click();
		Log.message("clicked on search collection button");
		SkySiteUtils.waitTill(5000);
		
		List<WebElement> listOfElements = driver.findElements(By.xpath(".//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"));
		int size= listOfElements.size();
		
		if(size == 0)
		{
			Log.message("Deletion successfull");
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver, resetsearch, 30);
			resetsearch.click();
			Log.message("Clicked on Search Reset button");
			SkySiteUtils.waitTill(5000);
			
		}
		else
		{
			Log.message("Deletion failed");
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver, resetsearch, 30);
			resetsearch.click();
			Log.message("Clicked on Search Reset button");
			SkySiteUtils.waitTill(5000);
		}
		
		
	}
	
	/**
	 * Method to check the presence of My Profile Button Scripted Trinanjwan
	 */

	public boolean presenceOfMyProfileBtn() {

		if (MyProfilebtn.isDisplayed() == true) {
			Log.message("My Profile Button is displayed");
			return true;
		}
		return false;
	}

	/**
	 * Method for landing into the My Profile page Scripted Trinanjwan
	 */

	public FnAMyProfilePage landingIntoMyProfilePage()

	{

		SkySiteUtils.waitTill(3000);
		Log.message("Waiting for My Profile button to display");
		SkySiteUtils.waitForElement(driver, MyProfilebtn, 10);
		Log.message("My Profile button is displayed");
		MyProfilebtn.click();
		Log.message("Clicked on My Profile button");
		SkySiteUtils.waitTill(3000);
		MyProfilebtndrpdwn.click();
		Log.message("Clicked on My Profile button from list");
		return new FnAMyProfilePage(driver).get();

	}

	/**
	 * Method for handling the new feature pop over window Scripted Trinanjwan
	 */

	public void handlingNewFeaturepopover() {
		SkySiteUtils.waitTill(5000);
		int Feedback_Alert = driver
				.findElements(By.cssSelector(
						"#whatsNew > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)"))
				.size();
		Log.message("Checking feedback alert is there or not : " + Feedback_Alert);
		if (Feedback_Alert > 0) {
			driver.findElement(By.cssSelector(
					"#whatsNew > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)"))
					.click();
			Log.message("Clicked on 'Got it' link!!!");
		}
	}

	/**
	 * Method to Verify the PROJECT ROLE inside project Scripted Trinanjwan
	 */
	
	@FindBy(css="tr.ev_material:nth-child(2) > td:nth-child(3) > a:nth-child(1)")
	@CacheLookup
	WebElement projectlist;
	
	@FindBy(css="#btnCreateFolder")
	@CacheLookup
	WebElement CreateFolderbtn;
	
	@FindBy(css="#ProjectMenu1_Teams")
	@CacheLookup
	WebElement Teambtn;
	
	@FindBy(css="#btnAddNewTeam")
	@CacheLookup
	WebElement Addbtn;
	
	@FindBy(css=".custom-select-dropdown")
	@CacheLookup
	WebElement Projectroledrpdown;
	
	
	
	public boolean verifyProjectRole()
	
	
	{	
		
		driver.switchTo().defaultContent();
		Log.message("Switch to default content");
		SkySiteUtils.waitForElement(driver, Teambtn, 10);
		Log.message("Team button is displayed");
		Teambtn.click();
		Log.message("Clicked on the Team button");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame("myFrame");
	    Log.message("Switch to My Frame");
	    SkySiteUtils.waitForElement(driver, Addbtn, 10);
	    Log.message("Add button is available and it is navigated to team section");
	    Select select1 = new Select(Projectroledrpdown);
	    String option = select1.getFirstSelectedOption().getText();
	    Log.message("Actual Project Role: "+option);
        String exprole=PropertyReader.getProperty("Projectrole1");
        Log.message("Expected Project Role: "+exprole);
        if((option).contentEquals(exprole))
        	return true;
        else
        	return false;

		
	}
		
	
	
		    /**Method for album
			* Scripted by tarak
			*/
			@FindBy(xpath=".//*[@id='ProjectFiles']/a")
			WebElement documenttabalbum;
			public FNA_HomeExtraModulePAge clickdocuments() {
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(4000);
				documenttabalbum.click();
				System.out.println("document tab is clicked");
				return new FNA_HomeExtraModulePAge(driver).get();
				
			}	
		
			
			/**
			 * Method for landing into the Account Settings page Scripted Trinanjwan
			 */
			
			
			@FindBy(css = "#setting")
			@CacheLookup
			WebElement Settingsbtn;
			
			
			@FindBy(css = "#liAccountSettings > a:nth-child(1)")
			@CacheLookup
			WebElement AccountSettingsbtn;
			
			


			public FnaAccountSettingsPage landingAccountSettingsPage()

			{
				driver.switchTo().defaultContent();
		        SkySiteUtils.waitTill(5000);
				Log.message("Waiting for Settings button to display");
				SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
				Log.message("Settings button is displayed");
				Settingsbtn.click();
				Log.message("Clicked on Settings button");
				SkySiteUtils.waitTill(3000);
				AccountSettingsbtn.click();
				Log.message("Clicked on Account Settings button from list");
				return new FnaAccountSettingsPage(driver).get();

			}
			
			 /**Method for batch attribute
			* Scripted by tarak
			*/
			@FindBy(xpath=".//*[@id='ProjectFiles']/a")
			WebElement documenttabbatchattribute;
			public FNA_BatchAttributePage clickdocumentsforbatchattribute() {
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(4000);
				documenttabbatchattribute.click();
				Log.message("document tab is clicked");

				return new FNA_BatchAttributePage(driver).get();
				
			}
			
			 /**Method to land in the account settings for updating security information
			* Scripted by Trinanjwan
			*/
			
			
			@FindBy(css="#button-0")
			@CacheLookup
			WebElement UpdateSecurityInfobtn;
			
			
			public FnAMyProfilePage clickupdateSecurityInformation()
			
			{
				
				SkySiteUtils.waitForElement(driver, UpdateSecurityInfobtn, 10);
				UpdateSecurityInfobtn.click();
				Log.message("Clicked on Update Security Information button");
				
				return new FnAMyProfilePage(driver).get();
				
				
				
			}
			   /**Method for album
						* Scripted by tarak
						*/
						@FindBy(xpath=".//*[@id='ProjectFiles']/a")
						WebElement documentviewer;
						public FNA_ViewerPage clickdocumentsforViewer() {
							driver.switchTo().defaultContent();
							SkySiteUtils.waitTill(4000);
							documentviewer.click();
							System.out.println("document tab is clicked");
							return new FNA_ViewerPage(driver).get();
							
						}	
					
	
						/**
						 * Method for handling demo video pop over window | Scripted Trinanjwan
						 */

						public void handlingDemoVideoepopover() {
							SkySiteUtils.waitTill(5000);
							int dem_video = driver
									.findElements(By.cssSelector(
											"#divWelcomeVid > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1)"))
									.size();
							Log.message("Checking demo video pop over is present or not : " + dem_video);
							if (dem_video > 0) {
								driver.findElement(By.cssSelector(
										"#divWelcomeVid > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1)"))
										.click();
								Log.message("Clicked on 'X' button to close the pop over");
							}
						}
                        /**Method for account setting landing 
                         * Scripted by : Tarak
                         * @return
                         */
						
						public FNA_AccountSettingPage navigateToAccountSetting()

						{
							driver.switchTo().defaultContent();
					        SkySiteUtils.waitTill(5000);
							Log.message("Waiting for Settings button to display");
							SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
							Log.message("Settings button is displayed");
							Settingsbtn.click();
							Log.message("Clicked on Settings button");
							SkySiteUtils.waitTill(3000);
							AccountSettingsbtn.click();
							Log.message("Clicked on Account Settings button from list");
							return new FNA_AccountSettingPage(driver).get();

						}
						
	/**Method to click document for print cart
	 * 
	 */
     public FNA_PrintCartPage clickdocumentsforPrintCart() {
    	 
    	 driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(4000);
			documentviewer.click();
			System.out.println("document tab is clicked");
			return new FNA_PrintCartPage(driver).get();
    	 
    	 
    	 
 }
     
 	@FindBy(xpath = "//*[text()='Remove all']")
 	WebElement removallbutton;
 	@FindBy(xpath = "//*[text()='Yes']")
 	WebElement yesbtn2;
 	@FindBy(xpath=".//*[@id='liPrintCart']/a")
 	WebElement pcartmenu;
 	@FindBy(xpath=".//*[@id='liPrintCart']/a")
 	List<WebElement> ele;
 	/**method to verify presence of print cart item and delete item if exist.
 	 * 
 	 */
 	public void verifyPrintCartItemExist() {
 		driver.switchTo().defaultContent();
 		Log.message("Switching to default content");
 		List printcart=driver.findElements(By.xpath(".//*[@id='liPrintCart']/a"));
 	//	int size=;
 		if(printcart.size()>0) {

            SkySiteUtils.waitForElement(driver, pcartmenu,40);
            pcartmenu.click();
            Log.message("print cart option is clicked");
 			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 			Log.message("Switched to frame");
 		    SkySiteUtils.waitForElement(driver, removallbutton, 60);
 		    removallbutton.click();
 			Log.message("Remove all button is clicked");
 			SkySiteUtils.waitForElement(driver, yesbtn2, 60);
 			yesbtn2.click();
 			Log.message("yes button is clicked");
 			SkySiteUtils.waitTill(4000);
 		
 		}
 		
 		else {
 			Log.message("print cart icon is not present");
 		}
 		

}

 	@FindBy(xpath=".//*[@id='btnNewProject']")
 	WebElement btnAddCollection;
 	
 	@FindBy(xpath=".//*[@id='ProjectMenu1_Album']/a")
 	WebElement btnAlbum;
 	
 	/**Method to enter Collection List Screen from Home Page
 	 * Scripted by: Ranadeep
 	 * 
 	 */
	public boolean enterCollectionList() 
	{
		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver, clkcollection, 60);
		SkySiteUtils.waitTill(3000);
		clkcollection.click();
		Log.message("Clicked on collection button on the top menu");
		SkySiteUtils.waitTill(5000);
		
		Log.message("Switching frame");
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
		SkySiteUtils.waitTill(5000);
		
		if(btnAddCollection.isDisplayed())
		{
			return true;
			
		}
		else 
		{
			return false;
		}
	}

	
	
	/**Method to validate presence of Album without selecting Collection 
 	 * Scripted by: Ranadeep
 	 * 
 	 */
	public boolean validateAlbumWithoutSelectCollection() 
	{

		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		
		if(!btnAlbum.isDisplayed())
		{
			return true;
		}		
		else
		{
			return false;
		}

	}

	
	
	/**Method to validate presence of Album after selecting Collection 
 	 * Scripted by: Ranadeep
 	 * 
 	 */
	public boolean validateAlbumAfterEnteringCollection() 
	{
		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		
		if(btnAlbum.isDisplayed())
		{
			return true;
		}		
		else
		{
			return false;
		}
	}

	
	/**
	 * Method written for Adding folder structure
	 *  Scripted By: Ranadeep Ghosh
	 * @throws AWTException
	 */
	public void Add_Folder_Structure(String parentFolder, String childFolder1, String childFolder2) 
	{	
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message(" Switching Frame ");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));	
		SkySiteUtils.waitTill(5000);
		
		btnAddFolder.click();
		Log.message(" Clicked on Add Folder button ");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(parentFolder);
		Log.message(" Parent Folder name entered is " +parentFolder);
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message(" Clicked on save button ");
		SkySiteUtils.waitTill(5000);
				
		String Parent_Folder_name = slttreefolder.getText();
		Log.message("Selected folder name is " + Parent_Folder_name);
		
		if (Parent_Folder_name.contains(parentFolder)) 
		{
			Log.message("Parent folder added successfully");
			SkySiteUtils.waitTill(3000);
			btnAddFolder.click();
			Log.message(" Clicked on Add Folder button ");
			SkySiteUtils.waitTill(5000);
			txtFolderName.sendKeys(childFolder1);
			Log.message(" Child Folder name entered is " +childFolder1);
			SkySiteUtils.waitTill(3000);
			btnNewFolderSave.click();
			Log.message(" Clicked on save button ");
			SkySiteUtils.waitTill(5000);
			
			String Child_Folder_name = slttreefolder.getText();
			Log.message("Selected folder name is " + Child_Folder_name);
			
			if(Child_Folder_name.contains(childFolder1))
			{
				Log.message("1st Child folder added successfully");
				SkySiteUtils.waitTill(3000);
				
				driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[3]")).click();
				Log.message("Clicked on parent folder");
				SkySiteUtils.waitTill(3000);
				
				btnAddFolder.click();
				Log.message(" Clicked on Add Folder button ");
				SkySiteUtils.waitTill(5000);
				txtFolderName.sendKeys(childFolder2);
				Log.message(" Child Folder name entered is " +childFolder2);
				SkySiteUtils.waitTill(3000);
				btnNewFolderSave.click();
				Log.message(" Clicked on save button ");
				SkySiteUtils.waitTill(5000);
				
				String Child_Folder_name1 = slttreefolder.getText();
				Log.message("Selected folder name is " + Child_Folder_name1);
				
				if(Child_Folder_name1.contains(childFolder2))
				{
					Log.message("1st Child folder added successfully");
					SkySiteUtils.waitTill(3000);
					Log.message("Folder structure added successfully");
				
				}
				
			}
			
		} 
		else 
		{
			Log.message("Folder structure added successfully");
			SkySiteUtils.waitTill(2000);
		
		}
		
	}

	
	
	/**
	 * Method written for Adding folder structure
	 *  Scripted By: Ranadeep Ghosh
	 * 
	 */
	public CollectionTeamsPage enterTeamPage() 
	{
		
		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default frame");
        driver.switchTo().defaultContent();
        SkySiteUtils.waitTill(5000);
        btnteam.click();
        Log.message("Teams button has been clicked");
        SkySiteUtils.waitTill(5000);
        
		return new CollectionTeamsPage(driver).get();
		
	}

	
	/**
	 * Method written for Accepting Project Invitation
	 *  Scripted By: Ranadeep Ghosh
	 * 
	 */
	public void acceptProjectInvitation(String collectionname) 
	{
		if(projectInvitationPopover.isDisplayed())
		{
			Log.message("User has pending project Invitations");
			SkySiteUtils.waitTill(5000);
			int count = driver.findElements(By.xpath(".//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr/td[2]")).size();
			for(int i=1;i<=count;i++)
			{
				if(driver.findElement(By.xpath(".//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]")).getText().contains(collectionname))
				{
					driver.findElement(By.xpath(".//*[@id='divInvitedProjectsGrid']/div[2]/table/tbody/tr["+i+"+"+1+"]/td[1]/img")).click();
					Log.message("Selected required project to be accepted");
					SkySiteUtils.waitTill(5000);
					
					SkySiteUtils.waitForElement(driver, btnAcceptSelecetedCollection, 30);
					btnAcceptSelecetedCollection.click();
					Log.message("Clicked Accepted selected collections button");
					SkySiteUtils.waitTill(6000);
					
					break;
					
				}
			}
		}
		else
		{
			Log.message("User has no no pending project Invitations");
		}
		
	}

	
	
	/**
	 * Method written for Validating folder structure for Read Folder Permission
	 *  Scripted By: Ranadeep Ghosh
	 * 
	 */
	public boolean verifyReadFolderPermissionFolderStructure(String collectionname,String ParentFolder,String ChildFolder1,String ChildFolder2) 
	{
		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message(" Switching Frame ");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));	
		SkySiteUtils.waitTill(5000);
		
		String actualCollectionName=null;
		String actualParentFolderName=null;
		String actualChildFolder1Name=null;
		String actualChildFolder2Name=null;
		
		
		if(driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).isDisplayed())
		{
			driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).click();
			SkySiteUtils.waitTill(5000);
			actualCollectionName = driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).getText();
			Log.message("Actual Collection name is: " +actualCollectionName);
			
		}
		
		if(driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[2]")).isDisplayed())
		{
			driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[2]")).click();
			SkySiteUtils.waitTill(5000);
			actualParentFolderName = driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).getText();		
			Log.message("Actual parent folder name is: " +actualParentFolderName);
			
		}
		
		if(driver.findElement(By.xpath("(.//*[@class='standartTreeImage'])[4]")).isDisplayed())
		{
			
			driver.findElement(By.xpath("(.//*[@class='standartTreeImage'])[4]")).click();
			Log.message("Clicked to open child folders");
			SkySiteUtils.waitTill(5000);
			
		}
			
		if(driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[3]")).isDisplayed())
		{
			driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[3]")).click();
			SkySiteUtils.waitTill(5000);
			actualChildFolder1Name = driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).getText();			
			Log.message("1st Child folder name is: " +actualChildFolder1Name);
			
		}
		
		if(driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[4]")).isDisplayed())
		{
			driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[4]")).click();
			SkySiteUtils.waitTill(5000);
			actualChildFolder2Name = driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).getText();			
			Log.message("1st Child folder name is: " +actualChildFolder2Name);
			
		}
		
		if(actualCollectionName.contains(collectionname) && actualParentFolderName.contains(ParentFolder) && actualChildFolder1Name.contains(ChildFolder1) && actualChildFolder2Name.contains(ChildFolder2))
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
		
	}

	
	
	/**
	 * Method written for Validating folder structure for Read Folder Permission
	 *  Scripted By: Ranadeep Ghosh
	 * 
	 */
	public void navigateToCollectionList() 
	{
		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		clkcollection.click();
		Log.message("Collection has been clicked from the top menu");
		SkySiteUtils.waitTill(5000);
		
		Log.message("Switching frame");
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(5000);
		
		if(btncollection.isDisplayed())
		{
			Log.message("Landed in the collection listing screen successfully");
		}
		else
		{
			Log.message("Landing in the collection listing screen failed!!!");
		}
		
		SkySiteUtils.waitTill(5000);
		Log.message("Switching to default content");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
	}

	

	/**
	 * Method written for Validating folder structure for No Permission
	 *  Scripted By: Ranadeep Ghosh
	 * @throws
	 */
	public boolean verifyNoPermissionFolderStructure(String collectionname)throws Throwable
	{
		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message(" Switching Frame ");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));	
		SkySiteUtils.waitTill(5000);
		
		String actualCollectionName= null;
		
		if(driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).isDisplayed())
		{
			driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).click();
			SkySiteUtils.waitTill(5000);
			actualCollectionName = driver.findElement(By.xpath("(//span[@class='selectedTreeRow'])[1]")).getText();
			Log.message("Actual Collection name is: " +actualCollectionName);
			
			if(actualCollectionName.contains(collectionname))
			{
				Log.message("Correct project is displayed");
			}
				
		}
		 
		SkySiteUtils.waitTill(8000);
		if(driver.findElements(By.xpath(".//*[@class='standartTreeImage']")).size() == 3)
		{
			return true;			
			
		}
			
		else
		{
			return false;
			
		}

	}
	
 	
	/**
	 * Method written for create collections with 
	 * collection already existing checking and delete
	 * Scripted By: Ranadeep
	 * 
	 * @return
	 */
	public boolean Create_Collections_New(String CollectionName) throws IOException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		
		this.deleteCollection(CollectionName);
		this.deleteCollectionValidation(CollectionName);
		
		SkySiteUtils.waitForElement(driver, btncollection, 30);
		btncollection.click();
		Log.message("Add collection has been clicked.");
		SkySiteUtils.waitTill(7000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(2000);
		// String CollectionName=FnaHomePage.this.Random_Collectionname();
		Log.message(CollectionName);
		txtProjectName.sendKeys(CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(CollectionName);
		Log.message("Collection number is:" + CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(3000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(3000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);
		String Descrip = PropertyReader.getProperty("Description");
		txtProjectDesc.sendKeys(Descrip);
		SkySiteUtils.waitTill(2000);
		txtProjectAddress1.sendKeys("salt lake");
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectCity.sendKeys("Kolkata");
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectZip.sendKeys("700091");
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(5000);
		/*Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText("USA");
		SkySiteUtils.waitTill(5000);
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText("California");
		Log.message("state has been selected");
		SkySiteUtils.waitTill(3000);*/
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(2000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		//SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(CollectionName)) 
		{
			Log.message("Create Collection successfull" + CollectionName);
			SkySiteUtils.waitTill(2000);
			return true;
		}
		else
		{
			Log.message("Create Collection Unsuccessfull" + CollectionName);
			SkySiteUtils.waitTill(2000);
			return false;
		}
	}
	
}
			