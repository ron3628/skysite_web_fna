package com.arc.fna.utils;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.arcautoframe.utils.WebDriverFactory;

public class SkySiteAppLaunch {
	
	static WebDriver driver;
	
	public static WebDriver launchURL() throws IOException, AWTException
	{
		WebDriverFactory webfac = new WebDriverFactory();
		return webfac.browserLaunch(PropertyReader.getProperty("BrowserName"), PropertyReader.getProperty("SkysiteProdURL"));
		//return driver;
	}
	
	/*public static WebDriver launchURL() throws IOException, AWTException
	{
		WebDriverFactory webfac = new WebDriverFactory();
		return webfac.browserLaunch(PropertyReader.getProperty("BrowserName"), PropertyReader.getProperty("SkysiteProdURL"));
		//return driver;
	}*/
	

}
