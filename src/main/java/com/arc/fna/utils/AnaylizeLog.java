package com.arc.fna.utils;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import com.arcautoframe.utils.Log;

public class AnaylizeLog {
	
	public static void analyzeLog(WebDriver driver) 
	{
	    LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
	    for (LogEntry entry : logEntries) 
	{
	                        Log.message(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
	            }
	}

}
