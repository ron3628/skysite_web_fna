package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.ArchivedCollectionsPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.ProjectsFilesPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_ProjectsFiles 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	ProjectsFilesPage projectsFilesPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	/** TC_001 (Projects Files): Verify Copy files within collection(new revision).
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Projects Files): Verify Copy files within collection(new revision).")
	public void verifyCopyFiles_Within_Collection() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (Projects Files): Verify Copy files within collection(new revision).");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.findElement(By.xpath("(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[1]")).click();//click on Project
			SkySiteUtils.waitTill(5000);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();
			Log.assertThat(projectsFilesPage.CopyFiles_Within_Collection(), "Copy files within collection Successfull", "Copy files within collection UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Projects Files): Verify Move files within collection(Multiple).
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Projects Files): Verify Move files within collection(Multiple).")
	public void verifyMoveFiles_Within_Collection () throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_002 (Projects Files): Verify Move files within collection(Multiple).");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.findElement(By.xpath("(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[1]")).click();//click on Project
			SkySiteUtils.waitTill(5000);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();
			Log.assertThat(projectsFilesPage.MoveFiles_Within_Collection(), "Move files within collection Successfull", "Move files within collection UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Projects Files): Verify Delete file within Folder.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Projects Files): Verify Delete file within Folder.")
	public void verifyDeleteFile_Within_Folder() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_003 (Projects Files): Verify Delete file within Folder.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();
			Log.assertThat(projectsFilesPage.DeleteFile_Within_Folder(), "Delete file within folder Successfull", "Delete file within folder UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (Projects Files): Verify Undo changes file within Folder.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (Projects Files): Verify Undo changes file within Folder.")
	public void verifyUndochanges_Within_Folder() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (Projects Files): Verify Undo changes file within Folder.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();
			Log.assertThat(projectsFilesPage.Undochanges_Within_Folder(), "Undochanges file within folder Successfull", "Undochanges file within folder UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	

	/** TC_005 (Projects Files): Verify single file download from menu within Folder.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (Projects Files): Verify single file download from menu within Folder.")
	public void verifySinglefile_Download_Within_Folder() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_005 (Projects Files): Verify single file download from menu within Folder.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();			
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(projectsFilesPage.Singlefile_Download_Within_Folder(DownloadPath), "Single file download within folder Successfull", "Single file download within folder UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_006 (Projects Files): Verify Copy files different collection with create new revision option.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true, description = "TC_006 (Projects Files): Verify Copy files different collection with create new revision option.")
	public void verifyCopyFiles_DifferentProject() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_006 (Projects Files): Verify Copy files different collection with create new revision option.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			String CollectionName1= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName1);	
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection11");			
			fnaHomePage.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("FNASelectFolder11");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();
			Log.assertThat(projectsFilesPage.CopyFiles_different_Collection(CollectionName1,FolderName1), "Copy file different project Successfull", "Copy file different project UnSuccessful", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_007 (Projects Files): Verify File history download from file more option.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 6, enabled = true, description = "TC_007 (Projects Files): Verify File history download from file more option.")
	public void verifyFilehistory_Download_fromfile_moreoption() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_007 (Projects Files): Verify File history download from file more option.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();			
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(projectsFilesPage.Filehistory_Download_fromfile_moreoption(DownloadPath), "File history download from more option Successfull", "File history download from more option UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_008 (Projects Files): Verify File history view from file more option.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_008 (Projects Files): Verify File history view from file more option.")
	public void verifyFilehistory_View_fromfile_moreoption() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_008 (Projects Files): Verify File history view from file more option.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			projectsFilesPage = new ProjectsFilesPage(driver).get();			
			Log.assertThat(projectsFilesPage.Filehistory_View_fromfile_moreoption(), "File history View from more option Successfull", "File history View from more option UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
}