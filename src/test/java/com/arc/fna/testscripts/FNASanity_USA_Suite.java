package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.ActivityReportPage;
import com.arc.fna.pages.FNASanity_USA_Page;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.GridExportPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;




@Listeners(EmailReport.class)

public class FNASanity_USA_Suite
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;	
	FNASanity_USA_Page fNASanity_USA_Page;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysitehealthURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysitehealthURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysitehealthURL"));
		}
		return driver;
	}
	
	/** TC_001 (FNAHealth Sanity): Verify Multiple Files Upload From File Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (FNAHealth Sanity): Verify Multiple Files Upload From File Level.")
	public void verifyMultiple_UploadFiles() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_001 (FNAHealth Sanity): Verify Multiple Files Upload From File Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName= fNASanity_USA_Page.Random_Foldername();		
			Log.assertThat(fNASanity_USA_Page.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("HealthFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fNASanity_USA_Page.Multiple_UploadFile(FolderPath), "Multiple Upload file Successfull", "Multiple Upload file UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	

	/** TC_002 (FNAHealth Sanity): Verify Download File From Menu in File Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (FNAHealth Sanity): Verify Download File From Menu in File Level.")
	public void verifyDownload_File_FromMenu() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_002 (FNAHealth Sanity): Verify Download File From Menu in File Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName= fNASanity_USA_Page.Random_Foldername();		
			Log.assertThat(fNASanity_USA_Page.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fNASanity_USA_Page.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(fNASanity_USA_Page.DownloadFile_FromMenu(DownloadPath), "Download file from menu Successfull", "Download file from menu UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (FNAHealth Sanity): Verify File View(PDF,TIFF) in File level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (FNAHealth Sanity): Verify File View(PDF,TIFF) in File level.")
	public void verifyFile_View_PDFTIF() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_003 (FNAHealth Sanity): Verify File View(PDF,TIFF) in File level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName= fNASanity_USA_Page.Random_Foldername();		
			Log.assertThat(fNASanity_USA_Page.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("HealthFolderPath1"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fNASanity_USA_Page.Multiple_UploadFile(FolderPath), "Multiple Upload file Successfull", "Multiple Upload file UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.ViewFile_Filelevel(), "Files(PDF,TIF) view Successfull", "Files(PDF,TIF) view UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (FNAHealth Sanity): Verify Edit File From Menu in File Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (FNAHealth Sanity): Verify Edit File From Menu in File Level.")
	public void verifyEdit_File_FromMenu() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_004 (FNAHealth Sanity): Verify Edit File From Menu in File Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname1");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("HealthFoldername");			
			fNASanity_USA_Page.Select_Folder(FolderName);				
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			String Rename= fNASanity_USA_Page.Random_Rename();
			Log.assertThat(fNASanity_USA_Page.EditFile_Filelevel(Title,Description,SearchTags,Rename), "Edit file from menu Successfull", "Edit file from menu UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005 (FNAHealth Sanity): Verify File Grid Search in File level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (FNAHealth Sanity): Verify File Grid Search in File level.")
	public void verifyFile_Grid_Search() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_005 (FNAHealth Sanity): Verify File Grid Search in File level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname1");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("HealthFoldername");			
			fNASanity_USA_Page.Select_Folder(FolderName);				
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			String Rename= fNASanity_USA_Page.Random_Rename();
			Log.assertThat(fNASanity_USA_Page.EditFile_Filelevel(Title,Description,SearchTags,Rename), "Edit file from menu Successfull", "Edit file from menu UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.File_Grid_Search(Rename), "Grid search Successfull", "Grid search UnSuccessful", driver);
					
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_006 (FNAHealth Sanity): Verify Module Search with file name in Project Files.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true, description = "TC_006 (FNAHealth Sanity): Verify Module Search with file name in Project Files.")
	public void verifyModule_SearchwithFilename() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_006 (FNAHealth Sanity): Verify Module Search with file name in Project Files.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname1");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("HealthFoldername");			
			fNASanity_USA_Page.Select_Folder(FolderName);					
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			String Rename= fNASanity_USA_Page.Random_Rename();
			Log.assertThat(fNASanity_USA_Page.EditFile_Filelevel(Title,Description,SearchTags,Rename), "Edit file from menu Successfull", "Edit file from menu UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Module_Searchwithfilename(Rename,Collection_Name), "Module search with file name Successfull", "Module search with file name UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_007 (FNAHealth Sanity): Verify Global Search with file name in Project Files.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 6, enabled = true, description = "TC_007 (FNAHealth Sanity): Verify Global Search with file name in Project Files.")
	public void verifyGlobal_SearchwithFilename() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_007 (FNAHealth Sanity): Verify Global Search with file name in Project Files.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname1");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("HealthFoldername");			
			fNASanity_USA_Page.Select_Folder(FolderName);					
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			String Rename= fNASanity_USA_Page.Random_Rename();
			Log.assertThat(fNASanity_USA_Page.EditFile_Filelevel(Title,Description,SearchTags,Rename), "Edit file from menu Successfull", "Edit file from menu UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Global_Searchwithfilename(Rename,Collection_Name), "Global search with file name Successfull", "Global search with file name UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_008 (FNAHealth Sanity): Verify Download Folder in folder Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_008 (FNAHealth Sanity): Verify Download Folder in folder Level.")
	public void verifyDownload_Floder() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_008 (FNAHealth Sanity): Verify Download Folder in folder Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname");
			fNASanity_USA_Page.selectcollection(Collection_Name);	
			String FolderName= fNASanity_USA_Page.Random_Foldername();		
			Log.assertThat(fNASanity_USA_Page.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fNASanity_USA_Page.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(fNASanity_USA_Page.Download_Folder(DownloadPath), "Download file from menu Successfull", "Download file from menu UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_009 (FNAHealth Sanity): Verify Modify Folder in folder Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 8, enabled = true, description = "TC_009 (FNAHealth Sanity): Verify Modify Folder in folder Level.")
	public void verifyModify_Floder() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_009 (FNAHealth Sanity): Verify Modify Folder in folder Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname2");
			fNASanity_USA_Page.selectcollection(Collection_Name);
			String FolderName= fNASanity_USA_Page.Random_Foldername();							
			Log.assertThat(fNASanity_USA_Page.Modify_Folder(FolderName), "Modify folder Successfull", "Modify folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally	
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_010 (FNAHealth Sanity): Verify multiple Photos upload in album.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 9, enabled = true, description = "TC_010 (FNAHealth Sanity): Verify multiple Photos upload in album.")
	public void verifyMultiplePhotos_Upload() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_010 (FNAHealth Sanity): Verify multiple Photos upload in album.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname3");
			fNASanity_USA_Page.selectcollection(Collection_Name);
			String AlbumName= fNASanity_USA_Page.Random_Albumname();							
			Log.assertThat(fNASanity_USA_Page.Adding_Album(AlbumName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("HealthMultiplePhotos"));			
			String PhotoPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fNASanity_USA_Page.Upload_MultiplePhotos(PhotoPath), "Multiple photos upload in album Successfull", "Multiple photos upload in album UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_011 (FNAHealth Sanity): Verify Download album in album level.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 10, enabled = true, description = "TC_011 (FNAHealth Sanity): Verify Download album in album level.")
	public void verifyDownload_Album() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_011 (FNAHealth Sanity): Verify Download album in album level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname4");
			fNASanity_USA_Page.selectcollection(Collection_Name);
			String AlbumName= fNASanity_USA_Page.Random_Albumname();
							
			Log.assertThat(fNASanity_USA_Page.Adding_Album(AlbumName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("HealthSinglePhoto"));			
			String PhotoPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fNASanity_USA_Page.Upload_MultiplePhotos(PhotoPath), "photo upload in album Successfull", "photo upload in album UnSuccessful", driver);
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			Log.assertThat(fNASanity_USA_Page.Album_Download(DownloadPath), "Download album Successfull", "Download album UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_012 (FNAHealth Sanity): Verify album slide show in photo level.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 11, enabled = true, description = "TC_012 (FNAHealth Sanity): Verify album slide show in photo level.")
	public void verifyAlbum_Slideshow() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_012 (FNAHealth Sanity): Verify album slide show in photo level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname5");
			fNASanity_USA_Page.selectcollection(Collection_Name);			
			String AlbumName = PropertyReader.getProperty("HealthFoldername1");					
			Log.assertThat(fNASanity_USA_Page.Select_Album(AlbumName), "Select Album Successfull", "Select Album UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Album_Slideshow(), "Album slide show Successfull", "Album slide show UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_013 (FNAHealth Sanity): Verify Modify album in album level.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 12, enabled = true, description = "TC_013 (FNAHealth Sanity): Verify Modify album in album level.")
	public void verifyModify_Album() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_013 (FNAHealth Sanity): Verify Modify album in album level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname6");
			fNASanity_USA_Page.selectcollection(Collection_Name);
			String AlbumName= fNASanity_USA_Page.Random_Albumname();						
			Log.assertThat(fNASanity_USA_Page.Modify_Album(AlbumName), "Modify Album Successfull", "Modify Album UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_014 (FNAHealth Sanity): Verify Edit photo name in image level.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 13, enabled = true, description = "TC_014 (FNAHealth Sanity): Verify Edit photo name in image level.")
	public void verifyEdit_Photoname() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_014 (FNAHealth Sanity): Verify Edit photo name in image level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();	
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname7");
			fNASanity_USA_Page.selectcollection(Collection_Name);							
			String AlbumName = PropertyReader.getProperty("HealthFoldername1");					
			Log.assertThat(fNASanity_USA_Page.Select_Album(AlbumName), "Select Album Successfull", "Select Album UnSuccessful", driver);
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			Log.assertThat(fNASanity_USA_Page.Edit_Photo(Title,Description,SearchTags), "Edit Photo in Image level Successfull", "Edit Photo in Image level UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_015 (FNAHealth Sanity): Verify Module search with photo Title name.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 14, enabled = true, description = "TC_015 (FNAHealth Sanity): Verify Module search with photo Title name.")
	public void verifyModule_Search_Album() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_015 (FNAHealth Sanity): Verify Module search with photo Title name.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname7");
			fNASanity_USA_Page.selectcollection(Collection_Name);								
			String AlbumName = PropertyReader.getProperty("HealthFoldername1");					
			Log.assertThat(fNASanity_USA_Page.Select_Album(AlbumName), "Select Album Successfull", "Select Album UnSuccessful", driver);
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			Log.assertThat(fNASanity_USA_Page.Edit_Photo(Title,Description,SearchTags), "Edit Photo in Image level Successfull", "Edit Photo in Image level UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Module_SearchwithPhotoname(Title,Description,Collection_Name), "Module search with photo name Successfull", "Module search with photo name UnSuccessful", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_016 (FNAHealth Sanity): Verify Global search with photo Title name.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 15, enabled = true, description = "TC_016 (FNAHealth Sanity): Verify Global search with photo Title name.")
	public void verifyGlobal_Search_Album() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_016 (FNAHealth Sanity): Verify Global search with photo Title name.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();		
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname7");
			fNASanity_USA_Page.selectcollection(Collection_Name);						
			String AlbumName = PropertyReader.getProperty("HealthFoldername1");					
			Log.assertThat(fNASanity_USA_Page.Select_Album(AlbumName), "Select Album Successfull", "Select Album UnSuccessful", driver);
			String Title= fNASanity_USA_Page.Random_Title();
			String Description= fNASanity_USA_Page.Random_Description();
			String SearchTags= fNASanity_USA_Page.Random_SearchTags();
			Log.assertThat(fNASanity_USA_Page.Edit_Photo(Title,Description,SearchTags), "Edit Photo in Image level Successfull", "Edit Photo in Image level UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Global_SearchwithPhotoname(Title,Description,Collection_Name), "Global search with photo name Successfull", "Global search with photo name UnSuccessful", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_017 (FNAHealth Sanity): Verify Create Communication with Attachment.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 16, enabled = true, description = "TC_017 (FNAHealth Sanity): Verify Create Communication with Attachment.")
	public void verifyCommunication_Withattachment() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_017 (FNAHealth Sanity): Verify Create Communication with Attachment.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();		
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname8");
			fNASanity_USA_Page.selectcollection(Collection_Name);						
			String SubjectName= fNASanity_USA_Page.Random_Subject();
			String ContactName = PropertyReader.getProperty("HealthContactname");	
			String Email = PropertyReader.getProperty("HealthContactEmail");	
			File Path=new File(PropertyReader.getProperty("HealthFolderPath2"));			
			String FolderPath = Path.getAbsolutePath().toString();		
			Log.assertThat(fNASanity_USA_Page.Attchments_communication(ContactName,Email,SubjectName,FolderPath), "With Attachments in communications Successful with valid credential", "With Attachments in communications unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_018 (FNAHealth Sanity): Verify Create Task with attachment.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 17, enabled = true, description = "TC_017 (FNAHealth Sanity): Verify Create Task with attachment.")
	public void verifyTask_Withattachment() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_017 (FNAHealth Sanity): Verify Create Task with attachment.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();		
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname9");
			fNASanity_USA_Page.selectcollection(Collection_Name);				
			String TaskName= fNASanity_USA_Page.Random_Task();
			String SubjectName = fNASanity_USA_Page.Random_Subject();
			String ContactName = PropertyReader.getProperty("HealthContactname");	
			String Email = PropertyReader.getProperty("HealthContactEmail");
			Log.assertThat(fNASanity_USA_Page.Add_Task_withSubject(TaskName,SubjectName,ContactName,Email), "Add team member Successful with valid credential", "Add team member unsuccessful with valid credential", driver);
			File Path=new File(PropertyReader.getProperty("HealthFolderPath2"));			
			String FolderPath = Path.getAbsolutePath().toString();		
			Log.assertThat(fNASanity_USA_Page.Attach_file_task(FolderPath,TaskName,SubjectName,ContactName), "With Attach file in collection task Successful with valid credential", "With Attach file in collection task unsuccessful with valid credential", driver);
		
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_019 (FNAHealth Sanity): Verify user exporting the csv and excel in collection File Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 18, enabled = true, description = "TC_019 (FNAHealth Sanity): Verify user exporting the excel in collection File Level.")
	public void verifyExport_Collection_FileLevel() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_019 (FNAHealth Sanity): Verify user exporting the excel in collection File Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();		
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname10");
			fNASanity_USA_Page.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("HealthFoldername2");			
			fNASanity_USA_Page.Select_Folder(FolderName);	
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename1");
			Log.assertThat(fNASanity_USA_Page.Export_CSV_CollectionFile(DownloadPath,DownloadPath1,fileName), "Export as CSV in file level Successfull", "Export as CSV in file level  UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Export_Excel_CollectionFile(DownloadPath,DownloadPath1), "Export as Excel in file level  Successfull", "Export as Excel in file level  UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_020 (FNAHealth Sanity): Verify user exporting the csv and excel in collection folder Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 19, enabled = true, description = "TC_020 (FNAHealth Sanity): Verify user exporting the excel in collection folder Level.")
	public void verifyExport_Collection_FolderLevel() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_020 (FNAHealth Sanity): Verify user exporting the excel in collection folder Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();		
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname10");
			fNASanity_USA_Page.selectcollection(Collection_Name);							
			String FolderName = PropertyReader.getProperty("HealthFoldername2");			
			fNASanity_USA_Page.Select_Folder(FolderName);							
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename1");
			Log.assertThat(fNASanity_USA_Page.Export_CSV_CollectionFolder(DownloadPath,DownloadPath1,fileName), "Export as CSV in folder Successfull", "Export as CSV in folder UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Export_Excel_CollectionFolder(DownloadPath,DownloadPath1), "Export as Excel in folder Successfull", "Export as Excel in folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	/** TC_021 (FNAHealth Sanity): Verify user exporting the csv and excel in Communication.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 20, enabled = true, description = "TC_021 (FNAHealth Sanity): Verify user exporting the excel in Communication.")
	public void verifyExport_Communication() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_021 (FNAHealth Sanity): Verify user exporting the excel in Communication.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();		
			String Collection_Name = PropertyReader.getProperty("HealthCollectionname10");
			fNASanity_USA_Page.selectcollection(Collection_Name);										
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("HealthFilename");
			Log.assertThat(fNASanity_USA_Page.Export_CSV_Communication(DownloadPath,DownloadPath1,fileName), "Export as CSV in communications Successfull", "Export as CSV in communications UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Export_Excel_Communication(DownloadPath,DownloadPath1), "Export as Excel in communications Successfull", "Export as Excel in communications UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_022 (FNAHealth Sanity): Verify user exporting the csv and excel in Contact.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 21, enabled = true, description = "TC_022 (FNAHealth Sanity): Verify user exporting the excel in Contact.")
	public void verifyExport_Contact() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_022 (FNAHealth Sanity): Verify user exporting the excel in Contact.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("HealthUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			fNASanity_USA_Page = new FNASanity_USA_Page(driver).get();														
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("HealthContactname1");
			Log.assertThat(fNASanity_USA_Page.Export_CSV_Contact(DownloadPath,DownloadPath1,fileName), "Export as CSV in communications Successfull", "Export as CSV in communications UnSuccessful", driver);
			Log.assertThat(fNASanity_USA_Page.Export_Excel_Contact(DownloadPath,DownloadPath1), "Export as Excel in communications Successfull", "Export as Excel in communications UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
}