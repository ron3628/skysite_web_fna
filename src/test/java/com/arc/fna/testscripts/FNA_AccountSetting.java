package com.arc.fna.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FNA_AccountSettingPage;
import com.arc.fna.pages.FNA_HomeExtraModulePAge;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_AccountSetting 
{
	static WebDriver driver;
	LoginPage loginpage;
	FnaHomePage fnahomepage;
	FNAAlbumPage fnaalbumpage;
	FNA_HomeExtraModulePAge fnahomextramodule;
	FNA_AccountSettingPage fnaaccountsetting;
	
	@Parameters("browser")
	@BeforeMethod
	
	public WebDriver beforeTest(String browser) 
	{

		if (browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} 
		
		else if (browser.equalsIgnoreCase("chrome")) 
		{
			
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator + System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} 		
		else if (browser.equalsIgnoreCase("safari")) 
		{
			System.setProperty("webdriver.safari.noinstall", "true"); // To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
			
		}
		
		return driver;

	}

	/**
	 * TC_001(Account Setting):Add Logo and verify it is display in the left most corner of tool
	 * @throws Exception
	 * 
	 */
	@Test(priority = 0, enabled = true, description = "Add Logo and verify it is display in the left most corner of tool")
	public void verifyAddandDisplayLogo() throws Exception 
	{
		try 
		{
			
			Log.testCaseInfo(" TC_001(Account Setting):Add Logo and verify it is display in the left most corner of tool ");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectBrandingTab();
			
			File fis = new File(PropertyReader.getProperty("logopath"));
			String filepath = fis.getAbsolutePath().toString();
			File fis1 = new File(PropertyReader.getProperty("tempfilepath"));
			String tempfilepath = fis1.getAbsolutePath().toString();
						
			Log.assertThat(fnaaccountsetting.addLogo(tempfilepath, filepath), " Logo is added successfully ", " Logo cannot be added ");
			
			Log.assertThat(fnaaccountsetting.deleteLogo(), " Logo is deleted successfully "," Logo deletion got failed ",driver);
			
		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
		
		}

	}

	
	/**
	 * TC_002(Account Setting):Add Logo and Change logo and verify logo is changed
	 * @throws Exception
	 * 
	 */
	@Test(priority = 1, enabled = true, description = " Add Logo and Change logo and verify logo is changed ")
	public void verifyChangeLogo() throws Exception 
	{
		try 
		{
			
			Log.testCaseInfo(" TC_002(Account Setting):Add Logo and Change logo and verify logo is changed ");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectBrandingTab();
			
			File fis = new File(PropertyReader.getProperty("logopath"));
			String filepath = fis.getAbsolutePath().toString();
			File fis1 = new File(PropertyReader.getProperty("tempfilepath"));
			String tempfilepath = fis1.getAbsolutePath().toString();
						
			Log.assertThat(fnaaccountsetting.addLogo(tempfilepath, filepath), " Logo is added successfully ", " Logo cannot be added ");
			
			File fis12 = new File(PropertyReader.getProperty("changelogo"));
			String filepath12 = fis12.getAbsolutePath().toString();
			
			File fiscapture = new File(PropertyReader.getProperty("SaveScreenshotforlogo"));
			String filepath1 = fiscapture.getAbsolutePath().toString();
			
			File fiscapture12 = new File(PropertyReader.getProperty("screenshotforchngelogo"));
			String filepathtemp = fiscapture12.getAbsolutePath().toString();
			
			Log.assertThat(fnaaccountsetting.changeLogo(tempfilepath, filepath12, filepath1,filepathtemp), " Logo is changed successfully ", " Logo cannot be changed ");
			
			Log.assertThat(fnaaccountsetting.deleteLogo(), " Logo is deleted successfully "," Logo deletion got failed ",driver);			
			fnaaccountsetting.delteScreenShot(filepathtemp);
			
		}

		catch (Exception e) 
		{
			
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}
	
	
	
	/**
	 * TC_003(Account Setting):Add Logo and verify it is deleted successfully
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 2, enabled = true, description = "Add Logo and verify it is deleted successfully")
	public void verifyDeleteLogo() throws Exception 
	{
		try 
		{
			
			Log.testCaseInfo("TC_003(Account Setting):Add Logo and verify it is deleted successfully");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectBrandingTab();
			
			File fis = new File(PropertyReader.getProperty("logopath"));
			String filepath = fis.getAbsolutePath().toString();
			File fis1 = new File(PropertyReader.getProperty("tempfilepath"));
			String tempfilepath = fis1.getAbsolutePath().toString();
			
			Log.assertThat(fnaaccountsetting.addLogo(tempfilepath, filepath), " Logo is added successfully "," Logo cannot be added ");
			
			Log.assertThat(fnaaccountsetting.deleteLogo(), " Logo is deleted successfully "," Logo deletion got failed ",driver);
			
		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}

	

	/**
	 * TC_004(Account Setting):Select folder sorting option, save and verify the
	 * same in document tab
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 3, enabled = true, description = "Select folder sorting option, save and verify the same in document tab")
	public void selectFolderSortingOption() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_004(Account Setting):Select folder sorting option, save and verify the same in document tab");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			//String collectionname = PropertyReader.getProperty("Collectioname4");
			String CollectionName = fnahomepage.Random_Collectionname();
			fnahomepage.Create_Collections(CollectionName);
			// fnahomepage.selectcollection(collectionname);
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectGeneralTab();
			
			Log.assertThat(fnaaccountsetting.selectFolderSortingOption(),"Folder sorting option is selected successfully from account setting","Folder sorting option is not selected from account setting");
			
			fnahomepage = fnaaccountsetting.closeFolderOption();
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.setOptionToManuallyOrganized();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
				
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}

	
	/**
	 * TC_006(Account Setting):select the recycle bin option and verify the same in
	 * setting tab of recycbin tab
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 4, enabled = true, description = "select the recycle bin option and verify the same in setting tab of recycbin tab")
	public void verifyRecyclebinOptionRefelectedinRecyclbinmodule() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_006(Account Setting):select the recycle bin option and verify the same in setting tab of recycbin tab");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectGeneralTab();
			
			Log.assertThat(fnaaccountsetting.setRecyclebinOption(),"Delete Permanent option  is selected successfully from account setting","Delete Permanent option is not selected from account setting");
			
			fnahomepage = fnaaccountsetting.closeSettingTab();
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.setOptionMoveToRecyclebin();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}

	
	
	/**
	 * TC_007(Account_Settings):Set and check the default link expiration(Day) for
	 * file.
	 * 
	 */
	@Test(priority = 5, enabled = true, description = "TC_006(Account_Settings):Set and check the default link expiration(Day) for file.")
	public void verifyAndSetLinkExpirationForFile() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_007(Account_Settings):Set and check the default link expiration(Day) for file.");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectGeneralTab();
			
			//code is written as per noofdays with 5
			String noofday = "5";		
			fnaaccountsetting.setExpirationInDaysForFiles(noofday);
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnaaccountsetting.selectcollection(collectionname);
			
			Log.assertThat(fnaaccountsetting.openLinkFileOptionVerifySetDayPeriod(noofday),"Default Link Expiration in days has been set and  verified","Failed to set the default link expiration in Days ");
			
			fnaaccountsetting.closeLinkFileWindow();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
		}
		
	}

	
	
	/**
	 * TC_008(Account_Settings):Set and check the default link expiration(weeks) for
	 * for folder.
	 * 
	 */
	@Test(priority = 6, enabled = true, description = "TC_008(Account_Settings):Set and check the default link expiration(weeks) for folder.")
	public void verifyAndSetLinkExpirationForFolderInweeks() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_008(Account_Settings):Set and check the default link expiration(weeks) for folder.");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectGeneralTab();
			String noofweeks = "1";
			// Log.assertThat(fnaaccountsetting.setExpirationInDaysForFiles(),"Delete
			// Permanent option is selected successfully from account setting","Delete
			// Permanent option is not selected from account setting");
			fnaaccountsetting.setExpirationInDaysForFolderLink(noofweeks);
			// fnahomepage=fnaaccountsetting.closeSettingTab();
			// fnaaccountsetting=fnahomepage.navigateToAccountSetting();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnaaccountsetting.selectcollection(collectionname);
			
			Log.assertThat(fnaaccountsetting.openLinkFolderOptionVerifySetWeekPeriod(noofweeks),"Default Link Expiration in weeks has been set and  verified","Failed to set the default link expiration in weeks ");
			
			fnaaccountsetting.closeLinkFolderWindow();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}
		
	}

	
	/**
	 * TC_009(Account_Settings):Set and check the default link expiration(months)
	 * for album.
	 * 
	 */
	@Test(priority = 7, enabled = true, description = "TC_009(Account_Settings):Set and check the default link expiration(months) for album.")
	public void verifyAndSetLinkExpirationForAlbumINMonths() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_009(Account_Settings):Set and check the default link expiration(months) for album.");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			String noofmonths = "3";
			// Log.assertThat(fnaaccountsetting.setExpirationInDaysForFiles(),"Delete
			// Permanent option is selected successfully from account setting","Delete
			// Permanent option is not selected from account setting");
			fnaaccountsetting.setExpirationInMonthsForShareAlbum(noofmonths);
			// fnahomepage=fnaaccountsetting.closeSettingTab();
			// fnaaccountsetting=fnahomepage.navigateToAccountSetting();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnaaccountsetting.selectcollection(collectionname);
			fnaaccountsetting.clickAlbumIcon();
			
			Log.assertThat(fnaaccountsetting.openShareAlbumOptionVerifySetMonthPeriod(noofmonths),"Default Link Expiration in weeks has been set and  verified","Failed to set the default link expiration in weeks ");
			
			fnaaccountsetting.closeShareAlbumWindow();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}
		
	}

	
	
	/**
	 * TC_0010(Account_Settings):Set and check the default link expiration(Year) for
	 * Package link.
	 * 
	 */
	@Test(priority = 8, enabled = true, description = "TC_0010(Account_Settings):Set and check the default link expiration(Year) for Package link.")
	public void verifyAndSetLinkExpirationForPackageInYear() throws Exception 
	{
		try 
		{
			
			Log.testCaseInfo("TC_0010(Account_Settings):Set and check the default link expiration(Year) for Package link.");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			String noofyears = "3";
			fnaaccountsetting.setExpirationInYearForPackage(noofyears);

			Log.assertThat(fnaaccountsetting.openSharePackageOptionVerifySetMonthYear(noofyears),"Default Link Expiration in years has been set and  verified","Failed to set the default link expiration in years  ");
			
			fnaaccountsetting.closepackagewindow();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}
		
	}

	
	/**
	 * TC_005(Account Setting):verify password expiry selected does not change after save
	 * setting tab of recycbin tab
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 9, enabled = true, description = "verify password expiry selected does not change after save")
	public void verifypasswordExpiryIsSetSuccessfully() throws Exception {
		try 
		{
			
			Log.testCaseInfo("TC_005(Account Setting):verify password expiry selected does not change after save");
			
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforAccount");
			String pWord = PropertyReader.getProperty("passwordaccount");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			fnaaccountsetting = fnahomepage.navigateToAccountSetting();
			fnaaccountsetting.selectPasswordSettingtab();
			fnaaccountsetting.setPasswordExpiry();
			
			Log.assertThat(fnaaccountsetting.verifySetExpiry(), "Password expiry has been set successfully","Password expiry has not been set");
			
			fnaaccountsetting.setDefaultExpiry();
			//fnaaccountsetting = fnahomepage.navigateToAccountSetting();

			//fnaaccountsetting.setOptionMoveToRecyclebin();

		}

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}

}
