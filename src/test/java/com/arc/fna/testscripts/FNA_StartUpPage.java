package com.arc.fna.testscripts;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)
public class FNA_StartUpPage {
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	  } 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", ""); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (Start Up Page): Verify default view selection.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Start Up Page): Verify default view selection.")
	public void verifyDefaultview() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_001 (Start Up Page): Verify default view selection.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			Log.assertThat(fnaHomePage.start_Up_Page_Default_View(), "Default view in start up page Successful with valid credential", "Default view in start up page unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_002 (Start Up Page): Verify start file and the desired file for search.
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 *//*
	@Test(priority = 1, enabled = true, description = "TC_002 (Start Up Page): Verify start file and the desired file for search")
	public void verifydesiredfile() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_002 (Start Up Page): Verify start file and the desired file for search");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();			
			Log.assertThat(fnaHomePage.start_Up_Page_desiredfile_select(), "start file and the desired file for search Successful with valid credential", "start file and the desired file for search unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}*/
	
	/** TC_003 (Start Up Page): Verify Delete the search on cross button.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Start Up Page): Verify Delete the search on cross button.")
	public void verifydeletecrossbutton() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_003 (Start Up Page): Verify Delete the search on cross button.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();
			fnaHomePage.start_Up_Page_desiredfile_select();
			Log.assertThat(fnaHomePage.Delete_Search_Cross(), "Delete the search on cross button Successful with valid credential", "Delete the search on cross button unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (Start Up Page): Verify Pagination in select folder. 
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (Start Up Page): Verify Pagination in select folder.")
	public void verifyPagination() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_004 (Start Up Page): Verify Pagination in select folder.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();			
			Log.assertThat(fnaHomePage.pagination_SelectFolder(), "Pagination in selected folder Successful with valid credential", "Pagination in selected folder unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005 (Start Up Page): Verify go back button from folder. 
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (Start Up Page): Verify go back button from folder.")
	public void verifygobackbuttonfromfolder() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_005 (Start Up Page): Verify go back button from folder.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();			
			Log.assertThat(fnaHomePage.goback_Fromfile(), "Go back button from folder Successful with valid credential", "Go back button from folder unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/*
	*//** TC_006 (Start Up Page): Verify Search with folder name and file name. 
	 *  Scripted By: Sekhar
	 * @throws Exception
	 *//*
	@Test(priority = 5, enabled = true, description = "TC_006 (Start Up Page): Verify Search with folder name and file name.")
	public void verifySearchwithfolderfile() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_006 (Start Up Page): Verify Search with folder name and file name.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();			
			Log.assertThat(fnaHomePage.Search_withfolderandfile(), "Search with folder name and file name Successful with valid credential", "Search with folder name and file name unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}*/
	
	
	/** TC_007&008 (Start Up Page): Verify change start file and delete file. 
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 6, enabled = true, description = "TC_007&008 (Start Up Page): Verify change start file and delete file.")
	public void verifychangestartfile() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_007&008 (Start Up Page): Verify change start file and delete file.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();	
			fnaHomePage.Search_withfolderandfile();
			Log.assertThat(fnaHomePage.changestartfile_Option(), "change start file and delete file Successful with valid credential", "change start file and delete file unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	

	/** TC_009 (Start Up Page): Verify change default view to folder and file. 
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_009 (Start Up Page): Verify change default view to folder and file.")
	public void verifychangedefaultview() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_009 (Start Up Page): Verify change default view to folder and file.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();	
			fnaHomePage.Search_withfolderandfile();
			Log.assertThat(fnaHomePage.change_default_view(), "change default view Successful with valid credential", "change default view unsuccessful with valid credential", driver);
		
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_010 (Start Up Page): Verify document 1st page as start file. 
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 8, enabled = true, description = "TC_010 (Start Up Page): Verify document 1st page as start file.")
	public void verifydocument1stpage() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_010 (Start Up Page): Verify document 1st page as start file.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();		
			Log.assertThat(fnaHomePage.select_collection_document_1stpage_startfile(), "document 1st page as start file Successful with valid credential", "document 1st page as start file unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_011 (Start Up Page): Verify document view in different a/c
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 9, enabled = true, description = "TC_011 (Start Up Page): Verify document view as start file in different users")
	public void verifydocumentviewemployee() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_011 (Start Up Page): Verify document view as start file in different users");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("StartEmployeeuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName,pWord);	
			fnaHomePage.loginValidation();		
			Log.assertThat(fnaHomePage.select_collection_document_view_Employee(), "document view employee side Successful with valid credential", "document view employee side unsuccessful with valid credential", driver);
			fnaHomePage.logout();
			String uName1 = PropertyReader.getProperty("Startliteuser");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithLiteUserCredential(uName1,pWord1);                           
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.select_collection_document_view_Liteuser(), "document view lite user side Successful with valid credential", "document view lite user side unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
}
