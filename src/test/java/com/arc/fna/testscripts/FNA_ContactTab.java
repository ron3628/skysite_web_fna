package com.arc.fna.testscripts;

import java.awt.AWTException;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arc.fna.pages.FnaContactTabPage;
import com.arc.fna.pages.FnaCreateGroupPage;
import com.arc.fna.pages.FnaHomePage;

@Listeners(EmailReport.class)
public class FNA_ContactTab {
    static WebDriver driver;
    LoginPage loginpage;
    FnaHomePage fnahomepage;
    FnaContactTabPage fnacontacttabpage;
    FnaCreateGroupPage fnacreategrppage;
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) {
		
    	if(browser.equalsIgnoreCase("firefox")) {
    		File dest = new File("./drivers/win/geckodriver.exe");
    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
    		driver = new FirefoxDriver();
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
    	else if (browser.equalsIgnoreCase("chrome")) { 
    		File dest = new File("./drivers/win/chromedriver.exe");
    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            ChromeOptions options = new ChromeOptions();
        	options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
    	else if (browser.equalsIgnoreCase("safari"))
    	{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
    	}
   return driver;
}
   /** TC_001(Contact Tab) : Verify Log in to the Application Contacts icon should be a Present
 * @throws Exception 
    *  
    */
    @Test(priority=0,enabled=true,description="TC_001(Contact Tab) : Verify Log in to the Application Contacts icon should be a Present")
    public void verifyContactIcon() throws Exception {
      try {
    	    Log.testCaseInfo("TC_001(Contact Tab) : Verify Log in to the Application Contacts icon should be a Present");
    	    loginpage = new LoginPage(driver).get();
    	    String uName=PropertyReader.getProperty("Emailfortest");
    	    String pWord=PropertyReader.getProperty("passwordcontact");
    	    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
    	    fnahomepage.loginValidation();
    	    Log.assertThat(fnahomepage.contactIconPresent(),"Contact Icon is Present after Application logging","Contact Icon is not present after Application Logging");
    	
          }
      catch(Exception e)
      {
             e.getCause();
             Log.exception(e, driver);
      }
      finally
      {
             Log.endTestCase();
             driver.quit();
      }
	
    }
    /** TC_002(Contact Tab) : Verify after click on Contact button  Address book page is landed
     * .
     * @throws Exception 
     */
     @Test(priority=1,enabled=true,description="TC_002(Contact Tab) : Verify after click on Contact button  Address book page is landed")
     public void verifyAddressBookPageLanded() throws Exception{
       try
         {
    	  Log.testCaseInfo("TC_002(Contact Tab) : Verify after click on Contact button  Address book page is landed");
    	  loginpage = new LoginPage(driver).get();
     	  String uName=PropertyReader.getProperty("Emailfortest");
     	  String pWord=PropertyReader.getProperty("passwordcontact");
     	  fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
     	  fnahomepage.loginValidation();
     	  fnahomepage.contactIconPresent();
     	  fnacontacttabpage=fnahomepage.contactClick();
    	  Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
        }
     catch(Exception e)
     {
            e.getCause();
            Log.exception(e, driver);
     }
     finally
     {
            Log.endTestCase();
            driver.quit();
     }
  }
     /** TC_003(Contact):Verify successful creation of new contact.
     * @throws Exception 
      * 
      */
     @Test(priority=2,enabled=true,description="Verify successful creation of new contact.")
     public void verifySuccessfullCreationNewContact() throws Exception {
    	 try {
    		     Log.testCaseInfo("TC_003(Contact):Verify successful creation of new contact.");
    		     loginpage = new LoginPage(driver).get();
    	     	 String uName=PropertyReader.getProperty("Emailfortest");
    	     	 String pWord=PropertyReader.getProperty("passwordcontact");
    	     	 fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
    	     	 fnahomepage.loginValidation();
    	     	 fnahomepage.contactIconPresent();
    	     	 fnacontacttabpage=fnahomepage.contactClick();
    	     	 Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	     	 String contactname=fnacontacttabpage.Random_Contactname();
    	      	 String Lastname1=fnacontacttabpage.Random_LastName();
    	     	 String emailid=fnacontacttabpage.Random_Email();
    	     	 Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    		     
    		 }
    	 catch(Exception e){
    		 e.getCause();
             Log.exception(e, driver);
    		 
    	 }
    	 finally {
    		 
    		 Log.endTestCase();
             driver.quit();
    	 }
     }	
     
     
	 /** TC_007(Contact):Verify Export of Address book
	 * @throws Exception 
	 * 
	 */
     @Test(priority=3,enabled=true,description="TC_007(Contact):Verify Export of Address book ")
     public void verifyExportAddressBook() throws Exception 
     {
    	 try 
    	 {
    		 Log.testCaseInfo("TC_007(Contact):Verify Export of Address book ");
    		 
    		 loginpage = new LoginPage(driver).get();
       	     String uName=PropertyReader.getProperty("Emailfortest");
       	     String pWord=PropertyReader.getProperty("passwordcontact");
       	     fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
       	     fnahomepage.loginValidation();
       	     fnahomepage.contactIconPresent();
       	     fnacontacttabpage=fnahomepage.contactClick();
       	     String usernamedir=System.getProperty("user.name");
       	     String downloadpath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
       	     
       	     Log.assertThat(fnacontacttabpage.exportAddressBook(downloadpath),"Contact file has been downloaded and found in the folder and the file is not blank","Contact file is not found in the folder");
       	     	 
		  }
    	  catch(Exception e) 
    	  {
    		  e.getCause();
              Log.exception(e, driver);
	      }
    	  finally 
    	  {
    		  Log.endTestCase();
              driver.quit();
    	  }
     }
     
     
    	 /** TC_008(Contact): Verify Search with contactname
    	 * @throws Exception 
    	  * 
    	  */
    	 @Test(priority=4,enabled=true,description="Verify Search with Contactname")
    	 public void verifySearchWithContactName() throws Exception {
    		 try {
    			 Log.testCaseInfo("TC_008(Contact): Verify Search with contactname");
    			 loginpage=new LoginPage(driver).get();
    			 String uName=PropertyReader.getProperty("Emailfortest");
     	     	 String pWord=PropertyReader.getProperty("passwordcontact");
     	     	 fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
     	     	 fnahomepage.loginValidation();
     	     	 fnahomepage.contactIconPresent();
     	     	 fnacontacttabpage=fnahomepage.contactClick();
     	     	 Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
     	     	 String contactname=fnacontacttabpage.Random_Contactname();
     	     	 String Lastname1=fnacontacttabpage.Random_LastName();
    	     	 String emailid=fnacontacttabpage.Random_Email();
    	     	 Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
     	     	 Log.assertThat(fnacontacttabpage.searchForContact(contactname),"Contact name has been searched and found in the list successfully", "Contactname searched cannot be found in the list");
     	     	}
    		 catch(Exception e)
    		 {
    			 e.getCause();
    			 Log.exception(e,driver);
    			 
    		 }
    		 finally {
        		 
        		 Log.endTestCase();
                 driver.quit();
        	 }
        }
    	 /**TC_009(Contact):Verify Search with Contact Last Name
    	     * @throws Exception 
    	      * 
    	      */
    	     @Test(priority=5,enabled=true,description="Verify Search with Contact LastName")
    	     public void searchWithContactLastName() throws Exception {
    	    	 try {
    	    		 Log.testCaseInfo("TC_009(Contact):Verify Search with Contact Last Name");
    	    		 loginpage=new LoginPage(driver).get();
    				 String uName=PropertyReader.getProperty("Emailfortest");
    	 	     	 String pWord=PropertyReader.getProperty("passwordcontact");
    	 	     	 fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
    	 	     	 fnahomepage.loginValidation();
    	 	     	 fnahomepage.contactIconPresent();
    	 	     	 fnacontacttabpage=fnahomepage.contactClick();
    	 	     	 Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	 	     	 String contactname=fnacontacttabpage.Random_Contactname();
    	 	     	 String Lastname1=fnacontacttabpage.Random_LastName();
    		     	 String emailid=fnacontacttabpage.Random_Email();
    		     	 Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    		     	 Log.assertThat(fnacontacttabpage.searchForLastName(Lastname1),"Last name entered in the search box is found in the list", "Last name entered in the searchbox not found in the list");
    	    		 
    	    	 }
    	    	 catch(Exception e)
    			 {
    				 e.getCause();
    				 Log.exception(e,driver);
    				 
    			 }
    	         finally 
    	         {
    	    		 
    	    		 Log.endTestCase();
    	             driver.quit();
    	    	 }
    	    }
    	     
    	     /** TC_0011(Contact):Verify Search with Email
    	      * @throws Exception 
    	      *
    	      */
    	       @Test(priority=6,enabled=true,description="Verify Search with Email")
    	       public void searchWithEmail() throws Exception {
    	      	 try {
    	      	       Log.testCaseInfo("TC_0011(Contact):Verify Search with Email");
    	      	       loginpage=new LoginPage(driver).get();
    	      	       String uName=PropertyReader.getProperty("Emailfortest");
    	      	       String pWord=PropertyReader.getProperty("passwordcontact");
    	      	       fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
    	      	       fnahomepage.loginValidation();
    	      	       fnahomepage.contactIconPresent();
    	      	       fnacontacttabpage=fnahomepage.contactClick();
    	      	       Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	      	       String contactname=fnacontacttabpage.Random_Contactname();
    	      	       String Lastname1=fnacontacttabpage.Random_LastName();
    	      	       String emailid=fnacontacttabpage.Random_Email();
    	      	       Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    	                 Log.assertThat(fnacontacttabpage.searchForEmail(emailid),"Email searched is found in the list","Email searched is not found in the list");
    	                  	       
    	      	 }
    	      	 catch(Exception e)
    	  		 {
    	  			 e.getCause();
    	  			 Log.exception(e,driver);
    	  			 
    	  		 }
    	           finally 
    	           {
    	      		 
    	      		 Log.endTestCase();
    	               driver.quit();
    	      	 }
    	     }

    	    	/**TC_0012(Contact):Verify Search with phone number
    	    	 * @throws Exception 
    	    	 * 
    	    	 */
    	    	@Test(priority=7,enabled=true,description="Verify  search with phone number")
    	    	public void searchWithPhoneNumber() throws Exception {
    	                try {
    	                	
    	                	 
    	                 		Log.testCaseInfo("TC_0012(Contact):Verify Search with phone number");
    	              	       	loginpage=new LoginPage(driver).get();
    	              	       	String uName=PropertyReader.getProperty("Emailfortest");
    	              	       	String pWord=PropertyReader.getProperty("passwordcontact");
    	              	       	fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
    	              	       	fnahomepage.loginValidation();
    	              	       	fnahomepage.contactIconPresent();
    	              	       	fnacontacttabpage=fnahomepage.contactClick();																								
    	              	       	Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	              	       	String contactname=fnacontacttabpage.Random_Contactname();
    	              	       	String Lastname1=fnacontacttabpage.Random_LastName();
    	              	       	String emailid=fnacontacttabpage.Random_Email();
    	              	       	Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    	                	    Log.assertThat(fnacontacttabpage.searchWithPhone(),"Phone no searched is found in the list","Phone no is not visible in the list");
    	                    }
    	    		
    	                catch(Exception e)
    	         	   {
    	         		 e.getCause();
    	         		 Log.exception(e,driver);
    	         		 
    	         	   }
    	               finally 
    	               {
    	         		 
    	         		 Log.endTestCase();
    	                 driver.quit();
    	         	  }
    	    	}
    	    	
    	    	/**TC_013(Contact):Verify Search with Role
    	    	 * @throws Exception 
    	    	 * 
    	    	 */
    	    	@Test(priority=8,enabled=true,description="Verify Search with Role")
    	    	public void verifySearchWithRole() throws Exception {
    	    		try {
    	    			Log.testCaseInfo("TC_013(Contact):Verify Search with Role");
    	      	       	loginpage=new LoginPage(driver).get();
    	      	       	String uName=PropertyReader.getProperty("Emailfortest");
    	      	       	String pWord=PropertyReader.getProperty("passwordcontact");
    	      	       	fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
    	      	       	fnahomepage.loginValidation();
    	      	       	fnahomepage.contactIconPresent();
    	      	       	fnacontacttabpage=fnahomepage.contactClick();																								
    	      	       	Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	      	       	String contactname=fnacontacttabpage.Random_Contactname();
    	      	       	String Lastname1=fnacontacttabpage.Random_LastName();
    	      	       	String emailid=fnacontacttabpage.Random_Email();
    	      	       	Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    	            	Log.assertThat(fnacontacttabpage.searchWithRole(contactname),"Contact name with the matching role is found in the list","Contact name with the matching role is not found in the list ",driver);
    	    		}
    	    		 catch(Exception e)
    	      	   {
    	      		 e.getCause();
    	      		 Log.exception(e,driver);
    	      		 
    	      	   }
    	            finally 
    	            {
    	      		 
    	      		 Log.endTestCase();
    	              driver.quit();
    	      	  }
    	     }
    	    	//failing due to existing issue
    	        /** TC_0010(Contact):Verify Search with Company name
    	        * @throws Exception 
    	         * 
    	         */
    	       @Test(priority=9,enabled=true,description="Verify Search with Company name")
    	       public void searchWithCompanyName() throws Exception {
    	       	try {
    	       		Log.testCaseInfo("TC_0010(Contact):Verify Search with Company name");
    	    	       	loginpage=new LoginPage(driver).get();
    	    	       	String uName=PropertyReader.getProperty("Emailfortest");
    	    	       	String pWord=PropertyReader.getProperty("passwordcontact");
    	    	       	fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
    	    	       	fnahomepage.loginValidation();
    	    	       	fnahomepage.contactIconPresent();
    	    	       	fnacontacttabpage=fnahomepage.contactClick()																								;
    	    	       	Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	    	       	String contactname=fnacontacttabpage.Random_Contactname();
    	    	       	String Lastname1=fnacontacttabpage.Random_LastName();
    	    	       	String emailid=fnacontacttabpage.Random_Email();
    	    	       	String companyname=fnacontacttabpage.Random_CompanyName();
    	    	       	Log.assertThat(fnacontacttabpage.CreateNewSharedUser(contactname,emailid,Lastname1,companyname),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    	       	    Log.assertThat(fnacontacttabpage.searchWithCompanyName(companyname),"Company name searched is found in the list","Company name is not found in the list",driver);
    	       	}
    	      
    	   	   catch(Exception e)
    	   	   {
    	   		 e.getCause();
    	   		 Log.exception(e,driver);
    	   		 
    	   	   }
    	         finally 
    	         {
    	   		 
    	   		 Log.endTestCase();
    	           driver.quit();
    	   	  }
    	       
    	       }
    	       /**TC_014(Contact): Verify Search with User License Type
   	    	 * @throws Exception 
   	    	 * 
   	    	 */
   	    	@Test(priority=10,enabled=true,description="Verify Search with User license type(Employee)")
   	    	public void searchWithUserLicenseType() throws Exception {
   	    		try {
   	    			   Log.testCaseInfo("TC_014(Contact): Verify Search with User License Type");
   	    				loginpage=new LoginPage(driver).get();
   	          	       	String uName=PropertyReader.getProperty("Emailfortest");
   	          	       	String pWord=PropertyReader.getProperty("passwordcontact");
   	          	       	fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
   	          	       	fnahomepage.loginValidation();
   	          	       	fnahomepage.contactIconPresent();
   	          	       	fnacontacttabpage=fnahomepage.contactClick();																								
   	          	       	Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
   	          	       	String contactname=fnacontacttabpage.Random_Contactname();
   	          	       	String Lastname1=fnacontacttabpage.Random_LastName();
   	          	       	String emailid=fnacontacttabpage.Random_Email();
   	          	       	Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
   	    			    Log.assertThat(fnacontacttabpage.searchUserWithLicenseType(contactname),"Employee type user added is found in the list after searching","Employee type user added is found in the list after searching",driver);
   	    		}
   	    		 catch(Exception e)
   	       	   {
   	       		 e.getCause();
   	       		 Log.exception(e,driver);
   	       		 
   	       	   }
   	             finally 
   	             {
   	       		 
   	       		 Log.endTestCase();
   	               driver.quit();
   	       	  }
   	   }
   	    	
   	     /**TC_017(Contact):Verify Edit functionality option
   	    	 * @throws Exception 
   	    	    * 
   	    	    */
   	    	   @Test(priority=11,enabled=true,description="Verify Edit Functionality")
   	    	   public void verifyEditFunctionality() throws Exception {
   	    		   try {

   	    			   	Log.testCaseInfo("TC_017(Contact):Verify Edit functionality option");
   	    				loginpage=new LoginPage(driver).get();
   	    	  	       	String uName=PropertyReader.getProperty("Emailfortest");
   	    	  	       	String pWord=PropertyReader.getProperty("passwordcontact");
   	    	  	       	fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
   	    	  	       	fnahomepage.loginValidation();
   	    	  	       	fnahomepage.contactIconPresent();
   	    	  	       	fnacontacttabpage=fnahomepage.contactClick();																								
   	    	  	       	Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
   	    	  	       	String contactname=fnacontacttabpage.Random_Contactname();
   	    	  	       	String Lastname1=fnacontacttabpage.Random_LastName();
   	    	  	       	String emailid=fnacontacttabpage.Random_Email();
   	    	  	       	Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
   	    	  	  	    String emailidtoupdate=fnacontacttabpage.Random_updateEmail();
   	    	  	  	    Log.assertThat(fnacontacttabpage.editFunctionalityoption(emailidtoupdate,contactname),"Edit functionality working fine","Edit functionality not working fine",driver);	       	
   	    			  }
   	    		   catch(Exception e) 
   	    		   {
   	    			   e.getCause();
   	    				 Log.exception(e,driver);
   	    		   }
   	    		   finally {
   	    			   
   	    			   Log.endTestCase();
   	    		        driver.quit(); 
   	    		   }
   	    		   
   	    		}
   	    	   /**TC_015(Contact): Verify Reset functionality for search option
   	    	 * @throws Exception 
   	    	    * 
   	    	    */
   	    	   @Test(priority=12,enabled=true,description="Verify Reset functionality for search option")
   	    	   public void verifyResetFunctionality() throws Exception {
   	    		   try {
   	    		   Log.testCaseInfo("TC_015(Contact): Verify Reset functionality for search option");
   	    			 loginpage=new LoginPage(driver).get();
   	    			 String uName=PropertyReader.getProperty("Emailfortest");
   	    	    	 String pWord=PropertyReader.getProperty("passwordcontact");
   	    	    	 fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
   	    	    	 fnahomepage.loginValidation();
   	    	    	 fnahomepage.contactIconPresent();
   	    	    	 fnacontacttabpage=fnahomepage.contactClick();
   	    	    	 Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
   	    	    	 String contactname=fnacontacttabpage.Random_Contactname();
   	    	    	 String Lastname1=fnacontacttabpage.Random_LastName();
   	    	   	     String emailid=fnacontacttabpage.Random_Email();
   	    	   	     Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
   	    	   	     Log.assertThat(fnacontacttabpage.searchForEmail(emailid),"Email searched is found in the list","Email searched is not found in the list"); 
   	    	   	     Log.assertThat(fnacontacttabpage.resetFunctionality(contactname),"Reset functionality working fine for search option","Reset function is not working fine for search option");
   	    		   }
   	    		   catch(Exception e) {
   	    			   e.getCause();
   	    				 Log.exception(e,driver);
   	    		   }
   	    		   finally {
   	    			   
   	    			   Log.endTestCase();
   	    		        driver.quit(); 
   	    		   }
   	    		}
   	   
               /**TC_06(Contact):Verify Addressbook import 
    	        * 
    	        */
    	         	
    	        @Test(priority=13,enabled=true,description="Verify import of address book using override option")
    	        public void verifyImportAddressBook() throws Exception {
    	         		  try {
    	         			   
    	         			Log.testCaseInfo("TC_06(Contact):Verify Addressbook import ");
    	      		       	loginpage=new LoginPage(driver).get();
    	      		       	String uName=PropertyReader.getProperty("Emailfortest");
    	      		       	String pWord=PropertyReader.getProperty("passwordcontact");
    	      		       	fnahomepage=loginpage.loginWithValidCredential(uName, pWord);
    	      		       	fnahomepage.loginValidation();
    	      		       	fnahomepage.contactIconPresent();
    	      		       	fnacontacttabpage=fnahomepage.contactClick();																					
    	      		       	Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	      		        String radomexcelvalue=fnacontacttabpage.random_ExcelValue();
    	      		       	File fis=new  File(PropertyReader.getProperty("FilePath"));
    	      		       	String filepath=fis.getAbsolutePath().toString();
    	      		  	   File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
	      		       	    String tempfilepath=fis1.getAbsolutePath().toString();
    	      		     	fnacontacttabpage.searchValueIntable(filepath,radomexcelvalue);	  
    	      		     	Log.assertThat(fnacontacttabpage.importAddressBook(filepath,radomexcelvalue,tempfilepath),"Contact import done successfully with override option","Contact import fails");
    	       		        }
    	         		  
    	         		   catch(Exception e) {
    	         			   e.getCause();
    	         				 Log.exception(e,driver);
    	         		   }
    	         		   finally {
    	         			   
    	         			   Log.endTestCase();
    	         		        driver.quit(); 
    	         		   }
    	        }
    	        
    	        /** TC_004(Contact):Verify successful creation of new Group.
	            * @throws Exception (By Trinanjwan)
	            * 
	            */
	            @Test(priority=14,enabled=true,description="Verify successful creation of new group.")
	            public void verifySuccessfullCreationNewgroup() throws Exception {
                try
                {
                	Log.testCaseInfo("TC_004(Contact):Verify successful creation of new Group.");
                	
                    loginpage = new LoginPage(driver).get();
    	            Log.message("Login Page objects initiated successfully");
    	            String uName=PropertyReader.getProperty("unametri");
    	            String pWord=PropertyReader.getProperty("pwdtri");
    	            fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
    	            fnahomepage.loginValidation();
    	            
    	            fnahomepage.contactIconPresent();
    	            fnacontacttabpage=fnahomepage.contactClick();
    	            fnacontacttabpage.AddressPage_IsLanded();
    	            fnacreategrppage=fnacontacttabpage.clickingOnCreateGroupButton();
    	            fnacreategrppage.verifylandingofcontactgrouppage();
    	            String groupname=fnacreategrppage.generateRandomgroupname();
    	            fnacreategrppage.create_newgroup(groupname);
    	            String Countfromtestdata =PropertyReader.getProperty("Countdata");
    	            Log.message("count is:"+Countfromtestdata);
    	            int total_count = Integer.parseInt(Countfromtestdata);
    	            Log.message("Total count is:" +total_count);
    	                    
    	            for(int i=1;i<=total_count;i++)
    	            {
    	            	String contactname1=fnacontacttabpage.Random_Contactname();
    	                String Lastname2=fnacontacttabpage.Random_LastName();
    	                String emailid1=fnacontacttabpage.Random_Email();   
    	                fnacreategrppage.additionOfNewContactInGroup(contactname1, Lastname2, emailid1);
    	                
    	            }
    	                     
    	            fnacreategrppage.clickingOnBackBtn();
    	            fnacontacttabpage.verificationOfCreatedGroup(groupname);
    	            fnacontacttabpage.navigationInsideTheCreatedGroup(groupname);
    	            Log.assertThat(fnacreategrppage.verificationOfaddedCntinGrp(), "New group is created with the number of contact added", "New group is not created with the number of contact added");
                }
                catch(Exception e)
                {
                	e.getCause();
    	            Log.exception(e, driver);
	            }
    	        finally 
    	        {
    	        	Log.endTestCase();
    	            driver.quit();
    	        }
    	                             
            }




    	                     /** TC_005(Contact):Verify deletion of contact.
    	                    * @throws Exception (By Trinanjwan)
    	                    * 
    	                    */
    	                     @Test(priority=15,enabled=true,description="Verify deletion of contact.")
    	                    public void verifyDeletionOfContct() throws Exception {
    	                          try {
    	                      Log.testCaseInfo("TC_005(Contact):Verify deletion of contact.");
    	                      loginpage = new LoginPage(driver).get();
    	                      Log.message("Login Page objects initiated successfully");
    	                      String uName=PropertyReader.getProperty("unametri");
    	                     String pWord=PropertyReader.getProperty("pwdtri");
    	                     fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
    	                     fnahomepage.loginValidation();
    	                     fnahomepage.contactIconPresent();
    	                     fnacontacttabpage=fnahomepage.contactClick();
    	                      fnacontacttabpage.AddressPage_IsLanded();
    	                      String contactname=fnacontacttabpage.Random_Contactname();
    	                      String Lastname1=fnacontacttabpage.Random_LastName();
    	                      String emailid=fnacontacttabpage.Random_Email();
    	                      Log.assertThat(fnacontacttabpage.CreateNewContact_cntcsel(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    	                      fnacontacttabpage.deletionOfContactFrmList(contactname);
    	                      Log.assertThat(fnacontacttabpage.verificationOfDeletedCntct(contactname), "Contact deleted successfully", "Contact is not deleted successfully", driver);
    	                      
    	                     }
    	               catch(Exception e){
    	                      e.getCause();
    	                    Log.exception(e, driver);
    	                      
    	               }
    	               finally 
    	               {
    	                      
    	                      Log.endTestCase();
    	                    driver.quit();
    	               }
    	                             
    	        }
    	            
    	                      
    	                      /** TC_016(Contact): Verify search with the group name.
    	            * @throws Exception (By Trinanjwan)
    	             * 
    	             */
    	            @Test(priority=16,enabled=true,description="Verify search with the group name.")
    	            public void verifySearchWithGroupName() throws Exception {
    	               try {
    	                      
    	                      Log.testCaseInfo("TC_016(Contact): Verify search with the group name.");
    	                      loginpage = new LoginPage(driver).get();
    	                      Log.message("Login Page objects initiated successfully");
    	                      String uName=PropertyReader.getProperty("unametri");
    	                     String pWord=PropertyReader.getProperty("pwdtri");
    	                     fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
    	                     fnahomepage.loginValidation();
    	                     fnahomepage.contactIconPresent();
    	                     fnacontacttabpage=fnahomepage.contactClick();
    	                      fnacontacttabpage.AddressPage_IsLanded();
    	                      fnacreategrppage=fnacontacttabpage.clickingOnCreateGroupButton();
    	                     fnacreategrppage.verifylandingofcontactgrouppage();
    	                     String groupname=fnacreategrppage.generateRandomgroupname();
    	                     fnacreategrppage.create_newgroup(groupname);
    	                     fnacreategrppage.clickingOnBackBtn();
    	                     Log.assertThat(fnacontacttabpage.searchgroupname(groupname), "Group search is working properly", "Group search is not working properly");
    	                     

    	               }
    	                     catch(Exception e){
    	                            e.getCause();
    	                          Log.exception(e, driver);
    	                            
    	                     }
    	                     finally {
    	                            
    	                            Log.endTestCase();
    	                          driver.quit();
    	                     }
    	                                   
    	            }

}
