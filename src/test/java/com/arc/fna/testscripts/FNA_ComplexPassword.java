package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommonLoginPage;
import com.arc.fna.pages.FnAMyProfilePage;
import com.arc.fna.pages.FnARegistrationPage;
import com.arc.fna.pages.FnaAccountSettingsPage;
import com.arc.fna.pages.FnaContactTabPage;
import com.arc.fna.pages.FnaCreateGroupPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.MarketingPage;
import com.arc.fna.pages.ProjectsMyProfPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.Log;

public class FNA_ComplexPassword {
    static WebDriver driver;
    LoginPage loginpage;
    FnaHomePage fnahomepage;
    FnaContactTabPage fnacontacttabpage;
    FnaCreateGroupPage fnacreategrppage;
    FnAMyProfilePage myprofpage;
    FnARegistrationPage regpage;
    ProjectsMyProfPage projectmyprogpage;
    CommonLoginPage commonpage;
    MarketingPage marketing;
    FnaAccountSettingsPage accountsettingspage;
    
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) {
		
    	if(browser.equalsIgnoreCase("firefox")) {
    		File dest = new File("./drivers/win/geckodriver.exe");
    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
    		driver = new FirefoxDriver();
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
    	else if (browser.equalsIgnoreCase("chrome")) { 
    		File dest = new File("./drivers/win/chromedriver.exe");
    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
    		ChromeOptions options = new ChromeOptions();
    		options.addArguments("--start-maximized");
    		driver = new ChromeDriver( options );
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
    	else if (browser.equalsIgnoreCase("safari"))
    	{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
    	}
   return driver;
}

    /**     TC_001(Complex_Password): Verify availability of Password Settings section under account settings
     * @throws Exception 
        *  Created by Trinanjwan
        */
        @Test(priority=0,enabled=true,description="TC_001(Complex_Password): Verify the availability of Password Settings section under account settings")
        public void availabilityOfComplexPasswordSection() throws Exception {
          try {
        	  	Log.testCaseInfo("TC_001(Complex_Password): Verify the availability of Password Settings section under account settings");
        	  	loginpage = new LoginPage(driver).get();
        	    regpage=new FnARegistrationPage(driver).get();
        	    String Emailrad=regpage.Random_Email();
        	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
        	    projectmyprogpage.handlingsamplevideoPROJECTS();
        	    projectmyprogpage.handlingNewFeaturepopover();
        	    projectmyprogpage.presenceOfPrjDashboardButton();
        	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
        	    fnahomepage=commonpage.activateFnAModule();
        	    fnahomepage.handlingNewFeaturepopover();
        	    fnahomepage.handlingDemoVideoepopover();
        	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
        	    Log.assertThat(accountsettingspage.availabilityOfComplexPasswordSection(),"Password Settings section is available",
        	    		"Password settings section is not available", driver);
        	    
              }
          catch(Exception e)
          {
                 e.getCause();
                 Log.exception(e, driver);
          }
          finally
          {
                 Log.endTestCase();
                 driver.quit();
          }
    	
        }
        
        
        /** TC_002(Complex_Password): Verify the the user should be able to set complex password.
         * @throws Exception 
            *  Created by Trinanjwan
            */
            @Test(priority=1,enabled=true,description="TC_002(Complex_Password): Verify the the user should be able to set complex password.")
            public void settingOfComplexPassword() throws Exception {
              try {
            	  	Log.testCaseInfo("TC_002(Complex_Password): Verify the the user should be able to set complex password.");
            	  	loginpage = new LoginPage(driver).get();
            	    regpage=new FnARegistrationPage(driver).get();
            	    String Emailrad=regpage.Random_Email();
            	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
            	    projectmyprogpage.handlingsamplevideoPROJECTS();
            	    projectmyprogpage.handlingNewFeaturepopover();
            	    projectmyprogpage.presenceOfPrjDashboardButton();
            	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
            	    fnahomepage=commonpage.activateFnAModule();
            	    fnahomepage.handlingNewFeaturepopover();
            	    fnahomepage.handlingDemoVideoepopover();
            	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
            	    accountsettingspage.availabilityOfComplexPasswordSection();
            	    accountsettingspage.enablingComplexPassword();
            	    Log.assertThat(accountsettingspage.settingComplexPassword(), "Complex Password authentication is saved successfully",
            	    		"Complex Password authentication is not saved successfully", driver);
            	    
                  }
              catch(Exception e)
              {
                     e.getCause();
                     Log.exception(e, driver);
              }
              finally
              {
                     Log.endTestCase();
                     driver.quit();
              }
        	
            }
            
            /** TC_003(Complex_Password): Verify the user should be able to set 2 step authentication of a normal account.
             * @throws Exception 
                *  Created by Trinanjwan
                */
                @Test(priority=2,enabled=true,description="TC_003(Complex_Password): Verify the user should be able to set 2 step authentication of a normal account.")
                public void settingOf2StepAuthentication() throws Exception {
                  try {
                	  	Log.testCaseInfo("TC_003(Complex_Password): Verify the user should be able to set 2 step authentication of a normal account.");
                	  	loginpage = new LoginPage(driver).get();
                	    regpage=new FnARegistrationPage(driver).get();
                	    String Emailrad=regpage.Random_Email();
                	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                	    projectmyprogpage.handlingsamplevideoPROJECTS();
                	    projectmyprogpage.handlingNewFeaturepopover();
                	    projectmyprogpage.presenceOfPrjDashboardButton();
                	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                	    fnahomepage=commonpage.activateFnAModule();
                	    fnahomepage.handlingNewFeaturepopover();
                	    fnahomepage.handlingDemoVideoepopover();
                	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                	    accountsettingspage.availabilityOfComplexPasswordSection();
                	    accountsettingspage.enabling2StepAuthentication(); 
                	    Log.assertThat(accountsettingspage.setting2StepAuthentication(), "2-step authentication of normal account is saved successfully",
                	    		"2-step authentication of normal account is not saved successfully", driver);
                	    
                      }
                  catch(Exception e)
                  {
                         e.getCause();
                         Log.exception(e, driver);
                  }
                  finally
                  {
                         Log.endTestCase();
                         driver.quit();
                  }
            	
                }
 
                
                
                /** TC_004(Complex_Password): Verify login after enabling the complex password.
                 * @throws Exception 
                    *  Created by Trinanjwan
                    */
                    @Test(priority=3,enabled=true,description="TC_004(Complex_Password): Verify login after enabling the complex password.")
                    public void loginwithComplexPassword() throws Exception {
                      try {
                    	  	Log.testCaseInfo("TC_004(Complex_Password): Verify login after enabling the complex password.");
                    	  	loginpage = new LoginPage(driver).get();
                    	    regpage=new FnARegistrationPage(driver).get();
                    	    String Emailrad=regpage.Random_Email();
                    	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                    	    projectmyprogpage.handlingsamplevideoPROJECTS();
                    	    projectmyprogpage.handlingNewFeaturepopover();
                    	    projectmyprogpage.presenceOfPrjDashboardButton();
                    	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                    	    fnahomepage=commonpage.activateFnAModule();
                    	    fnahomepage.handlingNewFeaturepopover();
                    	    fnahomepage.handlingDemoVideoepopover();
                    	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                    	    accountsettingspage.availabilityOfComplexPasswordSection();
                    	    accountsettingspage.enablingComplexPassword();
                    	    accountsettingspage.settingComplexPassword();
                    	    loginpage = new LoginPage(driver).get();
                    	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                    	    fnahomepage=loginpage.loginWithValidCredential(Emailrad,pWord);
                    	    Log.assertThat(fnahomepage.loginValidation(), "The user is able to login with the set Complex Password",    		
                    	    	"The user is not able to login with the set Complex Password", driver);
                    	    
                          }
                      catch(Exception e)
                      {
                             e.getCause();
                             Log.exception(e, driver);
                      }
                      finally
                      {
                             Log.endTestCase();
                             driver.quit();
                      }
                	
                    }
 
                    
                    /** TC_005(Complex_Password): Verify login after enabling the 2-step authentication.
                     * @throws Exception 
                        *  Created by Trinanjwan
                        */
                        @Test(priority=4,enabled=true,description="TC_005(Complex_Password): Verify login after enabling the 2-step authentication.")
                        public void loginwith2stepenabled() throws Exception {
                          try {
                        	  	Log.testCaseInfo("TC_005(Complex_Password): Verify login after enabling the 2-step authentication.");
                        	  	loginpage = new LoginPage(driver).get();
                        	    regpage=new FnARegistrationPage(driver).get();
                        	    String Emailrad=regpage.Random_Email();
                        	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                        	    projectmyprogpage.handlingsamplevideoPROJECTS();
                        	    projectmyprogpage.handlingNewFeaturepopover();
                        	    projectmyprogpage.presenceOfPrjDashboardButton();
                        	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                        	    fnahomepage=commonpage.activateFnAModule();
                        	    fnahomepage.handlingNewFeaturepopover();
                        	    fnahomepage.handlingDemoVideoepopover();
                        	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                        	    accountsettingspage.availabilityOfComplexPasswordSection();
                        	    accountsettingspage.enabling2StepAuthentication(); 
                        	    accountsettingspage.setting2StepAuthentication();
                        	    loginpage = new LoginPage(driver).get();
                        	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                        	    String loginpin=PropertyReader.getProperty("pintri");
                        	    loginpage.loginWithValidCredential(Emailrad,pWord);
                        	    fnahomepage=loginpage.loginWithPIN(loginpin);
                        	    Log.assertThat(fnahomepage.loginValidation(), "The user is able to login after enabling 2step authentication", 
                        	    		"The user is not able to login after enabling 2step authentication", driver);

                              }
                          catch(Exception e)
                          {
                                 e.getCause();
                                 Log.exception(e, driver);
                          }
                          finally
                          {
                                 Log.endTestCase();
                                 driver.quit();
                          }
                    	
                        }

                        
                        /** TC_006(Complex_Password): Verify the user should be able to enable 2 step authentication of a complex password enabled account.
                         * @throws Exception 
                            *  Created by Trinanjwan
                            */
                            @Test(priority=5,enabled=true,description="TC_006(Complex_Password): Verify the user should be able to enable 2 step authentication of a complex password enabled account.")
                            public void TwostepenabledOfComplexPasswordAccount() throws Exception {
                              try {
                            	  	Log.testCaseInfo("TC_006(Complex_Password): Verify the user should be able to enable 2 step authentication of a complex password enabled account.");
                            	  	loginpage = new LoginPage(driver).get();
                            	    regpage=new FnARegistrationPage(driver).get();
                            	    String Emailrad=regpage.Random_Email();
                            	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                            	    projectmyprogpage.handlingsamplevideoPROJECTS();
                            	    projectmyprogpage.handlingNewFeaturepopover();
                            	    projectmyprogpage.presenceOfPrjDashboardButton();
                            	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                            	    fnahomepage=commonpage.activateFnAModule();
                            	    fnahomepage.handlingNewFeaturepopover();
                            	    fnahomepage.handlingDemoVideoepopover();
                            	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                            	    accountsettingspage.availabilityOfComplexPasswordSection();
                            	    accountsettingspage.enablingComplexPassword();
                            	    accountsettingspage.settingComplexPassword();
                            	    loginpage = new LoginPage(driver).get();
                            	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                            	    fnahomepage=loginpage.loginWithValidCredential(Emailrad,pWord);
                            	    fnahomepage.loginValidation();
                            	    fnahomepage.handlingNewFeaturepopover();
                            	    myprofpage=fnahomepage.clickupdateSecurityInformation();
                            	    myprofpage.updateSecurityQuestions();
                            	    accountsettingspage=myprofpage.landingAccountSettingsPageprof();
                            	    accountsettingspage.availabilityOfComplexPasswordSection();
                            	    accountsettingspage.enabling2StepAuthentication(); 
                            	    accountsettingspage.settingPIN();
                            	    loginpage = new LoginPage(driver).get();
                            	    String loginpin=PropertyReader.getProperty("pintri");
                            	    loginpage.loginWithValidCredential(Emailrad,pWord);
                            	    fnahomepage=loginpage.loginWithPIN(loginpin);
                            	    Log.assertThat(fnahomepage.loginValidation(), "The user is able to set 2 step authentication of a complex password enabled account", 
                            	    		"The user is not able to login after enabling 2step authentication", driver);
                            	    
                                  }
                              catch(Exception e)
                              {
                                     e.getCause();
                                     Log.exception(e, driver);
                              }
                              finally
                              {
                                     Log.endTestCase();
                                     driver.quit();
                              }
                        	
                            }
                            
                            
                            
                            
                            /** TC_007(Complex_Password): To verify the Account Lock scenario after providing 5 times wrong password
                             * @throws Exception 
                                *  Created by Trinanjwan
                                */
                                @Test(priority=6,enabled=true,description="TC_007(Complex_Password): To verify the Account Lock scenario after providing 5 times wrong password")
                                public void accountLockingWrongPassword() throws Exception {
                                  try {
                                	  	Log.testCaseInfo("TC_007(Complex_Password): To verify the Account Lock scenario after providing 5 times wrong password");
                                	  	loginpage = new LoginPage(driver).get();
                                	    regpage=new FnARegistrationPage(driver).get();
                                	    for(int i=1;i<=5;i++)
                                	    {
                                	     String uName=PropertyReader.getProperty("unametri1");
                   	                     String pWord=PropertyReader.getProperty("pwdtri");
                   	                     loginpage.loginWithValidCredential(uName,pWord);
                                	    	   	
                                	    }
                                	    
                                	    Log.assertThat(loginpage.passwordLockMessageVerification(), "The lock message is verified successfully", 
                                	    		"The lock message is verification got failed", driver);
                                	    
                                	    
                                      }
                                  catch(Exception e)
                                  {
                                         e.getCause();
                                         Log.exception(e, driver);
                                  }
                                  finally
                                  {
                                         Log.endTestCase();
                                         driver.quit();
                                  }
                            	
                                }
                                
                                
                                
                                
                                /** TC_008(Complex_Password): To verify the Account Lock scenario after providing 5 times wrong PIN
                                 * @throws Exception 
                                    *  Created by Trinanjwan
                                    */
                                    @Test(priority=7,enabled=true,description="TC_008(Complex_Password): To verify the Account Lock scenario after providing 5 times wrong PIN")
                                    public void accountLockingWrongPIN() throws Exception {
                                      try {
                                    	  	Log.testCaseInfo("TC_008(Complex_Password): To verify the Account Lock scenario after providing 5 times wrong PIN");
                                    	  	loginpage = new LoginPage(driver).get();
                                    	    regpage=new FnARegistrationPage(driver).get();
                                    	     String uName=PropertyReader.getProperty("unametri2");
                       	                     String pWord=PropertyReader.getProperty("complexpasswordtri");
                       	                     loginpage.loginWithValidCredential(uName,pWord);
                       	                     for(int i=1;i<=5;i++)
                       	                     {
                       	                     String loginpin=PropertyReader.getProperty("pintri");
                       	                     loginpage.loginWithPIN(loginpin);
                       	                     loginpage.handleToastMessage();
                       	                     
                       	                     
                       	                     }	   	
                                    	    
                                    	    Log.assertThat(loginpage.passwordLockMessageVerificationpin(), "The lock message is verified successfully", 
                                    	    		"The lock message is verification got failed", driver);
                                    	    
                                    	    
                                          }
                                      catch(Exception e)
                                      {
                                             e.getCause();
                                             Log.exception(e, driver);
                                      }
                                      finally
                                      {
                                             Log.endTestCase();
                                             driver.quit();
                                      }
                                	
                                    }
                            
                                    
                                    
                                    /** TC_009(Complex_Password): Verify if owner activate the complex password and 2 step authentications then all its employee need to set up in the next login.
                                     * @throws Exception 
                                        *  Created by Trinanjwan
                                        */
                                        @Test(priority=8,enabled=true,description="TC_009(Complex_Password): Verify if owner activate the complex password and 2 step authentications then all its employee need to set up in the next login.")
                                        public void empUpdatePassword() throws Exception {
                                          try {
                                        	  	Log.testCaseInfo("TC_009(Complex_Password): Verify if owner activate the complex password and 2 step authentications then all its employee need to set up in the next login.");
                                        	  	loginpage = new LoginPage(driver).get();
                                        	    regpage=new FnARegistrationPage(driver).get();
                                        	     String uName=PropertyReader.getProperty("unametri4");
                           	                     String pWord=PropertyReader.getProperty("pwdtri");
                           	                     loginpage.loginWithValidCredential(uName,pWord);

                                        	    Log.assertThat(loginpage.verificationofUpdatePasswordsection(), "Update Password section is available for the employee", 
                                        	    		"Update Password section is not available for the employee", driver);
                                        	    
                                        	    
                                              }
                                          catch(Exception e)
                                          {
                                                 e.getCause();
                                                 Log.exception(e, driver);
                                          }
                                          finally
                                          {
                                                 Log.endTestCase();
                                                 driver.quit();
                                          }
                                        }
                                          
                                          
                                          
                                          /** TC_0010(Complex_Password): To verify While user provide 3 times wrong Password Application should ask the user for Security questions if set.
                                           * @throws Exception 
                                              *  Created by Trinanjwan
                                              */
                                              @Test(priority=9,enabled=true,description="TC_0010(Complex_Password): To verify While user provide 3 times wrong Password Application should ask the user for Security questions if set.")
                                              public void securityQuesWrongPassword() throws Exception {
                                                try {
                                              	  	Log.testCaseInfo("TC_0010(Complex_Password): To verify While user provide 3 times wrong Password Application should ask the user for Security questions if set.");
                                              	  	loginpage = new LoginPage(driver).get();
                                              	    regpage=new FnARegistrationPage(driver).get();
                                              	    for(int i=1;i<=3;i++)
                                              	    {
                                              	     String uName=PropertyReader.getProperty("unametri5");
                                 	                 String pWord=PropertyReader.getProperty("pwdtri");
                                 	                 loginpage.loginWithValidCredential(uName,pWord);
                                              	    	   	
                                              	    }
                                              	    
                                              	    loginpage.loginWithSecurityQues();
                                              	    String loginpin=PropertyReader.getProperty("pinoritri");
                                              	    fnahomepage=loginpage.loginWithPIN(loginpin);
                                              	    Log.assertThat(fnahomepage.loginValidation(), "The user is able to login properly after providing security Questions", 
                                          	    		"The user is not able to login properly after providing security Questions", driver);
                                              	  
                                              	    
                                              	    
                                                    }
                                                catch(Exception e)
                                                {
                                                       e.getCause();
                                                       Log.exception(e, driver);
                                                }
                                                finally
                                                {
                                                       Log.endTestCase();
                                                       driver.quit();
                                                }
                                          	
                                              }
                                              
                                              
                                              
                                              /** TC_0013(Complex_Password): If user check the Complex Password and 2 step authentication in F&A then it should reflect in PROJECTS as well.
                                               * @throws Exception 
                                                  *  Created by Trinanjwan
                                                  */
                                                  @Test(priority=10,enabled=true,description="TC_0013(Complex_Password): If user check the Complex Password and 2 step authentication in F&A then it should reflect in PROJECTS as well.")
                                                  public void optionCheckedFnAtoProjects() throws Exception {
                                                    try {
                                                  	  	Log.testCaseInfo("TC_0013(Complex_Password): If user check the Complex Password and 2 step authentication in F&A then it should reflect in PROJECTS as well.");
                                                  	  	loginpage = new LoginPage(driver).get();
                                                  	    regpage=new FnARegistrationPage(driver).get();
                                                  	    String Emailrad=regpage.Random_Email();
                                                  	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                                                  	    projectmyprogpage.handlingsamplevideoPROJECTS();
                                                  	    projectmyprogpage.handlingNewFeaturepopover();
                                                  	    projectmyprogpage.presenceOfPrjDashboardButton();
                                                  	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                                                  	    fnahomepage=commonpage.activateFnAModule();
                                                  	    fnahomepage.handlingNewFeaturepopover();
                                                  	    fnahomepage.handlingDemoVideoepopover();
                                                  	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                                                  	    accountsettingspage.availabilityOfComplexPasswordSection();
                                                  	    accountsettingspage.enabling2StepAuthentication(); 
                                                  	    accountsettingspage.setting2StepAuthentication();
                                                  	    loginpage = new LoginPage(driver).get();
                                                  	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                                                  	    String loginpin=PropertyReader.getProperty("pintri");
                                                  	    loginpage.loginWithValidCredential(Emailrad,pWord);
                                                  	    fnahomepage=loginpage.loginWithPIN(loginpin);
                                                  	    fnahomepage.loginValidation();
                                                  	    fnahomepage.handlingNewFeaturepopover();
                                                  	    myprofpage=fnahomepage.clickupdateSecurityInformation();
                                                  	    myprofpage.updateSecurityQuestions();
                                                  	    projectmyprogpage=myprofpage.navigateToProjects();
                                                  	    Log.assertThat(projectmyprogpage.verifyCheckUncheckofComplexPassword(),"Both the Complex Password and 2-step authentication is checked in PROJECTS",
                                                  	    		"Both the Complex Password and 2-step authentication is not checked in PROJECTS", driver);
                                                  	    
                                                  	    

                                                        }
                                                    catch(Exception e)
                                                    {
                                                           e.getCause();
                                                           Log.exception(e, driver);
                                                    }
                                                    finally
                                                    {
                                                           Log.endTestCase();
                                                           driver.quit();
                                                    }
                                                    
                                                  }
                                                    
                                                    
                                                    /** TC_0014(Complex_Password): If user check the Complex Password and 2 step authentication in PROJECTS then it should reflect in F&A as well.
                                                     * @throws Exception 
                                                        *  Created by Trinanjwan
                                                        */
                                                        @Test(priority=11,enabled=true,description="TC_0014(Complex_Password): If user check the Complex Password and 2 step authentication in PROJECTS then it should reflect in F&A as well.")
                                                        public void optionCheckedProjectstoFnA() throws Exception {
                                                          try {
                                                        	  	Log.testCaseInfo("TC_0014(Complex_Password): If user check the Complex Password and 2 step authentication in PROJECTS then it should reflect in F&A as well.");
                                                        	  	loginpage = new LoginPage(driver).get();
                                                        	    regpage=new FnARegistrationPage(driver).get();
                                                        	    String Emailrad=regpage.Random_Email();
                                                        	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                                                        	    projectmyprogpage.handlingsamplevideoPROJECTS();
                                                        	    projectmyprogpage.handlingNewFeaturepopover();
                                                        	  //  projectmyprogpage.presenceOfPrjDashboardButton();
                                                        	    accountsettingspage=projectmyprogpage.enablingtwowayinPROJECTS();
                                                        	    accountsettingspage.setting2StepAuthentication();
                                                        	    loginpage = new LoginPage(driver).get();
                                                        	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                                                        	    String loginpin=PropertyReader.getProperty("pintri");
                                                        	    loginpage.loginWithValidCredential(Emailrad,pWord);
                                                        	    loginpage.loginWithPIN(loginpin);
                                                        	    commonpage=new CommonLoginPage(driver).get();    
                                                        	    fnahomepage=commonpage.activateFnAModule();
                                                        	    //fnahomepage.handlingNewFeaturepopover();
                                                          	    fnahomepage.handlingDemoVideoepopover();
                                                        	    myprofpage=fnahomepage.clickupdateSecurityInformation();
                                                        	    fnahomepage = new FnaHomePage(driver).get();
                                                        	    myprofpage.updateSecurityQuestions();
                                                        	    accountsettingspage=myprofpage.landingAccountSettingsPageprof();
                                                        	    accountsettingspage.availabilityOfComplexPasswordSection();
                                                        	    Log.assertThat(accountsettingspage.verifyCheckUncheckofComplexPassword(),"Both the Complex Password and 2-step authentication is checked in F&A",
                                                      	    		"Both the Complex Password and 2-step authentication is not checked in F&A", driver);
                                                        	    

                                                              }
                                                          catch(Exception e)
                                                          {
                                                                 e.getCause();
                                                                 Log.exception(e, driver);
                                                          }
                                                          finally
                                                          {
                                                                 Log.endTestCase();
                                                                 driver.quit();
                                                          }
                                                    	
                                                        }                   
                                                          /** TC_0015(Complex_Password): Verify the user should be able to save the security question after enabling 2 way authentication.
                                                           * @throws Exception 
                                                              *  Created by Trinanjwan
                                                              */
                                                              @Test(priority=12,enabled=true,description="TC_0015(Complex_Password): Verify the user should be able to save the security question after enabling 2 way authentication.")
                                                              public void settingSecurityQuestions() throws Exception {
                                                                try 
                                                                {
                                                              	  	Log.testCaseInfo("TC_0015(Complex_Password): If user check the Complex Password and 2 step authentication in F&A then it should reflect in PROJECTS as well.");
                                                              	  	loginpage = new LoginPage(driver).get();
                                                              	    regpage=new FnARegistrationPage(driver).get();
                                                              	    String Emailrad=regpage.Random_Email();
                                                              	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                                                              	    projectmyprogpage.handlingsamplevideoPROJECTS();
                                                              	    projectmyprogpage.handlingNewFeaturepopover();
                                                              	    projectmyprogpage.presenceOfPrjDashboardButton();
                                                              	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                                                              	    fnahomepage=commonpage.activateFnAModule();
                                                              	    fnahomepage.handlingNewFeaturepopover();
                                                              	    fnahomepage.handlingDemoVideoepopover();
                                                              	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                                                              	    accountsettingspage.availabilityOfComplexPasswordSection();
                                                              	    accountsettingspage.enabling2StepAuthentication(); 
                                                              	    accountsettingspage.setting2StepAuthentication();
                                                              	    loginpage = new LoginPage(driver).get();
                                                              	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                                                              	    String loginpin=PropertyReader.getProperty("pintri");
                                                              	    loginpage.loginWithValidCredential(Emailrad,pWord);
                                                              	    fnahomepage=loginpage.loginWithPIN(loginpin);
                                                              	    fnahomepage.loginValidation();
                                                              	    fnahomepage.handlingNewFeaturepopover();
                                                              	    myprofpage=fnahomepage.clickupdateSecurityInformation();
                                                              	    myprofpage.updateSecurityQuestions();
                                                              	    Log.assertThat(myprofpage.verifyAnswers(),"The answers are verified successfully",
                                                              	    		"The answers are not verified successfully", driver);
                                                              	    
                                                                }
                                                                catch(Exception e)
                                                                {
                                                                       e.getCause();
                                                                       Log.exception(e, driver);
                                                                }
                                                                finally
                                                                {
                                                                       Log.endTestCase();
                                                                       driver.quit();
                                                                }
                                                              	    
                                              
                                                    
                                                              }             
                                       
                                                              
                                                              
                                                              /** TC_0011(Complex_Password): To verify While user provide 3 times wrong PIN Application should ask the user for Security questions if set.
                                                               * @throws Exception 
                                                                  *  Created by Trinanjwan
                                                                  */
                                                                  @Test(priority=13,enabled=true,description="TC_0011(Complex_Password): To verify While user provide 3 times wrong PIN Application should ask the user for Security questions if set.")
                                                                  public void securityQuesWrongPin() throws Exception {
                                                                    try {
                                                                  	  	Log.testCaseInfo("TC_0011(Complex_Password): To verify While user provide 3 times wrong PIN Application should ask the user for Security questions if set.");
                                                                  	  	loginpage = new LoginPage(driver).get();
                                                                  	    regpage=new FnARegistrationPage(driver).get();
                                                                  	     String uName=PropertyReader.getProperty("unametri6");
                                                     	                 String pWord=PropertyReader.getProperty("complexpasswordtri");
                                                     	                 loginpage.loginWithValidCredential(uName,pWord);
                                                                  	    	   	
                                                   	                     for(int i=1;i<=3;i++)
                                                   	                     {
                                                   	                     String loginpin=PropertyReader.getProperty("pintri");
                                                   	                     loginpage.loginWithPIN(loginpin);
                                                   	                     loginpage.handleToastMessage();
  
                                                   	                     }	
                                                                  	    
                                                   	                    loginpage.loginWithSecurityQues_pin();
                                                   	                    fnahomepage = new FnaHomePage(driver).get();
                                                                	    Log.assertThat(fnahomepage.loginValidation(), "The user is able to login properly after providing security Questions", 
                                                            	    		"The user is not able to login properly after providing security Questions", driver);
                                                                  	    
                                                                  	  
                                                                  	    
                                                                  	    
                                                                        }
                                                                    catch(Exception e)
                                                                    {
                                                                           e.getCause();
                                                                           Log.exception(e, driver);
                                                                    }
                                                                    finally
                                                                    {
                                                                           Log.endTestCase();
                                                                           driver.quit();
                                                                    }
                                                              	
                                                                    
                                                                    
                                                                    
                                                                  }        
                                                                    
                                                                    
                                                                    /** TC_0016(Complex_Password): If user check the Security questions in F&A module then it should reflect in PROJECTS module as well.
                                                                     * @throws Exception 
                                                                        *  Created by Trinanjwan
                                                                        */
                                                                        @Test(priority=14,enabled=true,description="If user check the Security questions in F&A module then it should reflect in PROJECTS module as well.")
                                                                        public void reflectionofsecurityquestionsinProjects() throws Exception {
                                                                          try 
                                                                          {
                                                                        	  	Log.testCaseInfo("TC_0016(Complex_Password): If user check the Security questions in F&A module then it should reflect in PROJECTS module as well.");
                                                                        	  	loginpage = new LoginPage(driver).get();
                                                                        	    regpage=new FnARegistrationPage(driver).get();
                                                                        	    String Emailrad=regpage.Random_Email();
                                                                        	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                                                                        	    projectmyprogpage.handlingsamplevideoPROJECTS();
                                                                        	    projectmyprogpage.handlingNewFeaturepopover();
                                                                        	    projectmyprogpage.presenceOfPrjDashboardButton();
                                                                        	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                                                                        	    fnahomepage=commonpage.activateFnAModule();
                                                                        	    fnahomepage.handlingNewFeaturepopover();
                                                                        	    fnahomepage.handlingDemoVideoepopover();
                                                                        	    accountsettingspage=fnahomepage.landingAccountSettingsPage();
                                                                        	    accountsettingspage.availabilityOfComplexPasswordSection();
                                                                        	    accountsettingspage.enabling2StepAuthentication(); 
                                                                        	    accountsettingspage.setting2StepAuthentication();
                                                                        	    loginpage = new LoginPage(driver).get();
                                                                        	    String pWord=PropertyReader.getProperty("complexpasswordtri");
                                                                        	    String loginpin=PropertyReader.getProperty("pintri");
                                                                        	    loginpage.loginWithValidCredential(Emailrad,pWord);
                                                                        	    fnahomepage=loginpage.loginWithPIN(loginpin);
                                                                        	    fnahomepage.loginValidation();
                                                                        	    fnahomepage.handlingNewFeaturepopover();
                                                                        	    myprofpage=fnahomepage.clickupdateSecurityInformation();
                                                                        	    
                                                                        	    myprofpage.updateSecurityQuestions();                                                               
                                                                        	    myprofpage.navigateToProjects();
                                                                        	    projectmyprogpage = new ProjectsMyProfPage(driver).get();
                                                                        	    Log.assertThat(projectmyprogpage.verifysecurityquestions(),"Answers are verified successfully in Projects module",
                                                                        	    		"Answers are not verified successfully in Projects module", driver);
                                                                        	    

                                                                        	    
                                                                          }
                                                                          catch(Exception e)
                                                                          {
                                                                                 e.getCause();
                                                                                 Log.exception(e, driver);
                                                                          }
                                                                          finally
                                                                          {
                                                                                 Log.endTestCase();
                                                                                 driver.quit();
                                                                          }
                                                                        	    
                                                              
                                                                        }             
                                                                                                                               
}
                                                                  
                                              	
                                 
                                              
                                        