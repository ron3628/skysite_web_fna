package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommonLoginPage;
import com.arc.fna.pages.FnAMyProfilePage;
import com.arc.fna.pages.FnARegistrationPage;
import com.arc.fna.pages.FnaContactTabPage;
import com.arc.fna.pages.FnaCreateGroupPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.MarketingPage;
import com.arc.fna.pages.ProjectsMyProfPage;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FNA_Registration {
    static WebDriver driver;
    LoginPage loginpage;
    FnaHomePage fnahomepage;
    FnaContactTabPage fnacontacttabpage;
    FnaCreateGroupPage fnacreategrppage;
    FnAMyProfilePage myprofpage;
    FnARegistrationPage regpage;
    ProjectsMyProfPage projectmyprogpage;
    CommonLoginPage commonpage;
    MarketingPage marketing;
    
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) {
		
    	if(browser.equalsIgnoreCase("firefox")) {
    		File dest = new File("./drivers/win/geckodriver.exe");
    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
    		driver = new FirefoxDriver();
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
    		
    		SkySiteUtils.waitTill(3000);
    		String currentURL = driver.getCurrentUrl();
    		Log.message("Current url is:- "+currentURL);
        }
    	else if (browser.equalsIgnoreCase("chrome")) { 
    		File dest = new File("./drivers/win/chromedriver.exe");
    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
    		ChromeOptions options = new ChromeOptions();
    		options.addArguments("--start-maximized");
    		driver = new ChromeDriver( options );
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
    		
    		SkySiteUtils.waitTill(3000);
    		String currentURL = driver.getCurrentUrl();
    		Log.message("Current url is:- "+currentURL);
 
        } 
    	else if (browser.equalsIgnoreCase("safari"))
    	{
			System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
			
			SkySiteUtils.waitTill(3000);
			String currentURL = driver.getCurrentUrl();
			Log.message("Current url is:- "+currentURL);
    	}
   return driver;
}

    /** TC_001(Registration) : Verify registration with valid credentials
     * @throws Exception 
        *  Created by Trinanjwan
        */
    
        @Test(priority=0,enabled=true,description="TC_001(Registration): Verify registration with valid credentials")
        public void registrationWithValidCred() throws Exception {
          try {
        	    Log.testCaseInfo("TC_001(Registration) : Verify registration with valid credentials");
        	    loginpage = new LoginPage(driver).get();
        	    regpage=new FnARegistrationPage(driver).get();
        	    String Emailrad=regpage.Random_Email();
        	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
        	    projectmyprogpage.handlingsamplevideoPROJECTS();
        	    projectmyprogpage.handlingNewFeaturepopover();
        	    Log.assertThat(projectmyprogpage.presenceOfPrjDashboardButton(),"Registration with valid credentials sucessfully done", 
        	    		"Registration with valid credentials got failed",driver);      	    
        	    
              }
          catch(Exception e)
          {
                 e.getCause();
                 Log.exception(e, driver);
          }
          finally
          {
                 Log.endTestCase();
                 driver.quit();
          }
    	
        }
        
  
        
        /** TC_002(Registration) : Verify activation of F&A after registration
         * @throws Exception 
            *  Created by Trinanjwan
            */
            @Test(priority=1,enabled=true,description="TC_002(Registration) : Verify activation of F&A after registration")
            public void activationOFFnA() throws Exception {
              try {
            	    Log.testCaseInfo("TC_002(Registration) : Verify activation of F&A after registration");
            	    loginpage = new LoginPage(driver).get();
            	    regpage=new FnARegistrationPage(driver).get();
            	    String Emailrad=regpage.Random_Email();
            	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
            	    projectmyprogpage.handlingsamplevideoPROJECTS();
            	    projectmyprogpage.handlingNewFeaturepopover();
            	    projectmyprogpage.presenceOfPrjDashboardButton();
            	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
            	    fnahomepage=commonpage.activateFnAModule();
            	    fnahomepage.handlingNewFeaturepopover();
            	    fnahomepage.handlingDemoVideoepopover();
            	    Log.assertThat(fnahomepage.presenceOfMyProfileBtn(), "Activation of FnA module sucessfully done", 
            	    		"Activation of F&A module got failed",driver);       	    
            	    
                  }
              catch(Exception e)
              {
                     e.getCause();
                     Log.exception(e, driver);
              }
              finally
              {
                     Log.endTestCase();
                     driver.quit();
              }
        	
            }
        
            
            /** TC_003(Registration) : Registration with valid credentials from marketing page
             * @throws Exception 
                *  Created by Trinanjwan
                */

                @Test(priority=2,enabled=false,description="TC_003(Registration) : Registration with valid credentials from marketing page")
                public void activationOFFnAmarketing() throws Exception {
                  try {
                	    Log.testCaseInfo("TC_003(Registration) : Registration with valid credentials from marketing page");
                	    
                	    
                	    marketing=new MarketingPage(driver).get();
                	    loginpage=marketing.landingofLoginfromMarketing();
                	    regpage=new FnARegistrationPage(driver).get();
                	    String Emailrad=regpage.Random_Email();
                	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                	    projectmyprogpage.handlingsamplevideoPROJECTS();
                	    projectmyprogpage.handlingNewFeaturepopover();
                	    projectmyprogpage.presenceOfPrjDashboardButton();
                	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                	    fnahomepage=commonpage.activateFnAModule();
                	    fnahomepage.handlingNewFeaturepopover();
                	    fnahomepage.handlingDemoVideoepopover();
                	    Log.assertThat(fnahomepage.presenceOfMyProfileBtn(), "Registration with valid credentials from marketing page sucessfully done", 
                	    		"Registration with valid credentials from marketing page got failed",driver);       	    
                	    
                      }
                  catch(Exception e)
                  {
                         e.getCause();
                         Log.exception(e, driver);
                  }
                  finally
                  {
                         Log.endTestCase();
                         driver.quit();
                  }
            	
                }
                
                
                
                /** TC_009(Registration) : Verify the trial period of any account after registration.
                 * @throws Exception 
                    *  Created by Trinanjwan
                    */
                
                    @Test(priority=3,enabled=true,description="TC_009(Registration) : Verify the trial period of any account after registration.")
                    public void verifytrialperiod() throws Exception {
                      try {
                    	    Log.testCaseInfo("TC_009(Registration) : Verify the trial period of any account after registration.");
                    	    loginpage = new LoginPage(driver).get();
                    	    regpage=new FnARegistrationPage(driver).get();
                    	    String Emailrad=regpage.Random_Email();
                    	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                    	    projectmyprogpage.handlingsamplevideoPROJECTS();
                    	    projectmyprogpage.handlingNewFeaturepopover();
                    	    Log.assertThat(projectmyprogpage.verificationoftrial(), "The trial period is verified successfully",
                    	    		"The trial period verification got failed");

                    	    
                          }
                      catch(Exception e)
                      {
                             e.getCause();
                             Log.exception(e, driver);
                      }
                      finally
                      {
                             Log.endTestCase();
                             driver.quit();
                      }
                	
                    }
  
        
        
}

    
    
    	