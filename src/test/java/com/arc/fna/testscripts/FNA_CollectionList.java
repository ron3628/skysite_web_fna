package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)
public class FNA_CollectionList {
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	  } 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_005&006&008&010 (Collection List): Verify create collection with Add to favorite.
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_005&006&008&010 (Collection List): Verify create collection with Add to favorite.")
	public void verifycreatecollectionwithAddtofavorate () throws Exception
	{
		try
		{
			
			Log.testCaseInfo("TC_005&006&008&010 (Collection List): Verify create collection with Add to favorite");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.Create_Collection_Favourite(), "create collection Successful with valid credential", "create collection unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{			
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_007 (Collection List): Verify pagination.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_007 (Collection List): Verify pagination.")
	public void verifypagination() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_007 (Collection List): Verify pagination..");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("CollectionUserName");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.pagination_Collection(), "Pagination Successful with valid credential", "Pagination unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_009 (Collection List): Verify Accepted collection flag from employee.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_009 (Collection List): Verify Accepted collection flag from employee")
	public void verifyAcceptedcollectionflagemployee() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_009 (Collection List): Verify Accepted collection flag from employee");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String Employeename = PropertyReader.getProperty("CollectionEmployeeName");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(Employeename), "Adding contact fron address book Successful with valid credential", "adding contact fron address book unsuccessful with valid credential", driver);
			fnaHomePage.logout();
			String uName1 = PropertyReader.getProperty("CollectionEmploy_User");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName1,pWord1);
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.Accepted_Collections_Flag(CollectionName), "Accepted collections flag Successful with valid credential", "Accepted collections falg unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_011 (Collection List): Verify edit collection in owner user
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_011 (Collection List): Verify edit collection in owner user")
	public void verifyEditCollection() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_011 (Collection List): Verify edit collection in owner user");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String Edit_Colle_Name= fnaHomePage.Random_EditUser();	
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress"); 
			String city = PropertyReader.getProperty("Collectioncity"); 
			String Zip = PropertyReader.getProperty("CollectionZip"); 
			String Country = PropertyReader.getProperty("CollectionCountry"); 
			String state = PropertyReader.getProperty("CollectionState"); 			
			Log.assertThat(fnaHomePage.Edit_Collection(CollectionName,Edit_Colle_Name,Descrip,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
		
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_012 (Collection List): Verify Folder sorting default in collection setting.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_012 (Collection List): Verify Folder sorting default in collection setting.")
	public void verifyFoldersortingoption() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_012 (Collection List): Verify Folder sorting default in collection setting.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			Log.assertThat(fnaHomePage.Folder_sorting_Collection_Setting(CollectionName), "Folder sorting default in collection setting Successful with valid credential", "Folder sorting default in collection setting unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
		
	/** TC_013 (Collection List): Verify Current Collection Status in collection Setting
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true, description = "TC_013 (Collection List): Verify Current Collection Status in collection Setting")
	public void verifyCurrentCollectionStatus() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_013 (Collection List): Verify Current Collection Status in collection Setting");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			Log.assertThat(fnaHomePage.Current_Collection_Status_Setting(CollectionName), "Current Collection Status three options in collection setting Successful with valid credential", "Current Collection Status three options in collection setting unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	/** TC_014 (Collection List): Verify Multiple collection acceptance invitation
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 6, enabled = true, description = "TC_014 (Collection List): Verify Multiple collection acceptance invitation")
	public void verifymultiplecollectionacceptanceinvitation() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_014 (Collection List): Verify Multiple collection acceptance invitation");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String Employeename = PropertyReader.getProperty("CollectionEmployeeName");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(Employeename), "Adding contact fron address book Successful with valid credential", "adding contact fron address book unsuccessful with valid credential", driver);
		
			String CollectionName1= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName1);
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(Employeename), "Adding contact fron address book Successful with valid credential", "adding contact fron address book unsuccessful with valid credential", driver);
			
			fnaHomePage.logout();
			String uName1 = PropertyReader.getProperty("CollectionEmploy_User");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName1,pWord1);
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.Accepted_Collections(CollectionName,CollectionName1), "Multiple collections acceptance Successful with valid credential", "Multiple collections acceptance unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	/** TC_015 (Collection List): Verify sorting in collection list
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_015 (Collection List): Verify sorting in collection list.")
	public void verifysortingcollectionlist() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_015 (Collection List): Verify sorting in collection list");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("CollectionSorting");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.Sorting_Collectionname_Collectionlist(), "sorting collection name Successful with valid credential", "sorting collection name unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.sorting_favorite_Collectionlist(), "sorting favorite Successful with valid credential", "sorting favorite  unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.Sorting_Collectionnumber_Collectionlist(), "sorting collection number Successful with valid credential", "sorting collection number unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.sorting_Storage_Collectionlist(), "sorting storage Successful with valid credential", "sorting storage unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_017 (Collection List): Verify delete the project in owner and validate the employee user
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 8, enabled = true, description = "TC_017 (Collection List): Verify delete the project in owner and validate the employee user")
	public void verifydeleteprojectowner() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_017 (Collection List): Verify delete the project in owner and validate the employee user");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String Employeename = PropertyReader.getProperty("CollectionEmployeeName");  
			Log.assertThat(fnaHomePage.adding_contactemployee_from_address_book(Employeename), "Adding contact employee fron address book Successful with valid credential", "adding contact employee fron address book unsuccessful with valid credential", driver);
			fnaHomePage.logout();			
			String uName1 = PropertyReader.getProperty("CollectionEmploy_User");
			String pWord1 = PropertyReader.getProperty("Password");		
			fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName1,pWord1);
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.Accepted_Collection_FromEmployeee(CollectionName,uName,pWord,uName1,pWord1), "Delete the project in owner and validate the employee user Successful", "Delete the project in owner and validate the employee user UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_019 (Collection List): Verify making multiple collection favorite
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 9, enabled = true, description = "TC_019 (Collection List): Verify making multiple collection favorite")
	public void verifymultiplecollectionfavorite() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_019 (Collection List): Verify making multiple collection favorite");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();		
			//String CollectionName1= fnaHomePage.Random_Collectionname();		
			Log.assertThat(fnaHomePage.Multiple_collection_favorite(CollectionName), "Making Multiple collection favorite Successful", "Making Multiple collection favorite UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	/** TC_018 (Collection List): Verify owner change from collection owner tab.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 10, enabled = true, description = "TC_018 (Collection List): Verify owner change from collection owner tab.")
	public void verifyownerchangecollectionownertab() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_018 (Collection List): Verify owner change from collection owner tab.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);		
			String ContactName = PropertyReader.getProperty("CollectionEmployeeName");
			Log.assertThat(fnaHomePage.ownerchangecollectionownertab(CollectionName,ContactName), "owner change from collection Successful", "owner change from collection UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_016 (Collection List): Verify Account expired created collection.
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 11, enabled = true, description = "TC_016 (Collection List): Verify Account expired created collection")
	public void verifyAccountExpiredproject() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_016 (Collection List): Verify Account expired created collection.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("CollecExpiredusername");
			String pWord = PropertyReader.getProperty("CollecExpiredpassword");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionExpiredName = PropertyReader.getProperty("ColleExpiredName");
			Log.assertThat(fnaHomePage.Accountexpired_createdcollection(CollectionExpiredName), "Account Expired created collection population Successful", "Account Expired created collection population UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_020 (Collection List): Verify collection owner update the collection then it should update "New collection invitation"in collection is not accepted.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 12, enabled = true, description = "TC_020 (Collection List): Verify collection owner update the collection then it should update New collection invitation in collection is not accepted.")
	public void verifyUpdate_Collection() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_020 (Collection List): Verify collection owner update the collection then it should update  New collection invitation in collection is not accepted.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
			String Employeename = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(Employeename), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			String Edit_Colle_Name= fnaHomePage.Random_EditUser();				
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress"); 
			String city = PropertyReader.getProperty("Collectioncity"); 
			String Zip = PropertyReader.getProperty("CollectionZip"); 
			String Country = PropertyReader.getProperty("CollectionCountry"); 
			String state = PropertyReader.getProperty("CollectionState"); 			
			Log.assertThat(fnaHomePage.Edit_Collection(CollectionName,Edit_Colle_Name,Descrip,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			String uName1 = PropertyReader.getProperty("CollectionEmployee");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.New_Collection_Invitation(Edit_Colle_Name,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_021 (Collection List): Verify account owner updated the collection in inactive list then it should  update/display the updated collection for secondary.
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 13, enabled = true, description = "TC_021 (Collection List): Verify account owner updated the collection in inactive list then it should  update/display the updated collection for secondary.")
	public void verifyUpdate_Collection_owner_Second() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_021 (Collection List): Verify account owner updated the collection in inactive list then it should  update/display the updated collection for secondary.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("CollectionEmployee");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			fnaHomePage.Accepted_Popup_close();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
			String ownername = PropertyReader.getProperty("Collectionowner");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(ownername), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			
			String secondadmin = PropertyReader.getProperty("Collectionsecond");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(secondadmin), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			
			String uName1 = PropertyReader.getProperty("Collectionlistpro");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);	
			fnaHomePage.loginValidation();	
		
			String Edit_Colle_Name= fnaHomePage.Random_EditUser();				
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress"); 
			String city = PropertyReader.getProperty("Collectioncity"); 
			String Zip = PropertyReader.getProperty("CollectionZip"); 
			String Country = PropertyReader.getProperty("CollectionCountry"); 
			String state = PropertyReader.getProperty("CollectionState"); 			
			Log.assertThat(fnaHomePage.Accepted_Collection_FromOwner(CollectionName,Edit_Colle_Name,Descrip,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			
			String uName2 = PropertyReader.getProperty("CollectionEmployee");
			String pWord2 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName2,pWord2);	
			fnaHomePage.loginValidation();
			fnaHomePage.Accepted_Popup_close();
			Log.assertThat(fnaHomePage.Validation_Edit_collection(Edit_Colle_Name), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			
			String uName3 = PropertyReader.getProperty("Collectionsecondadmin");
			String pWord3 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName3,pWord3);	
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.New_Collection_Invitation(Edit_Colle_Name,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_022 (Collection List): Verify collection should update to account owner(collection owner) and secondary admin only.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 14, enabled = true, description = "TC_022 (Collection List): Verify collection should update to account owner(collection owner) and secondary admin only.")
	public void verifyUpdate_Collection_owner() throws Exception
	{
		
		try
		{
			Log.testCaseInfo("TC_022 (Collection List): Verify collection should update to account owner(collection owner) and secondary admin only.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
			String Secondadmin = PropertyReader.getProperty("Collectionsecond");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(Secondadmin), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			String Edit_Colle_Name= fnaHomePage.Random_EditUser();				
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress"); 
			String city = PropertyReader.getProperty("Collectioncity"); 
			String Zip = PropertyReader.getProperty("CollectionZip"); 
			String Country = PropertyReader.getProperty("CollectionCountry"); 
			String state = PropertyReader.getProperty("CollectionState"); 			
			Log.assertThat(fnaHomePage.Edit_Collection(CollectionName,Edit_Colle_Name,Descrip,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			String uName1 = PropertyReader.getProperty("Collectionsecondadmin");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.New_Collection_Invitation(Edit_Colle_Name,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_023 (Collection List): Verify secondary admin updated the inactive collection then it should update to collection owner and account owner also.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 15, enabled = true, description = "TC_023 (Collection List): Verify secondary admin updated the inactive collection then it should update to collection owner and account owner also.")
	public void verifyUpdate_Collection_Secondadmin() throws Exception
	{
		
		try
		{
			Log.testCaseInfo("TC_023 (Collection List): Verify secondary admin updated the inactive collection then it should update to collection owner and account owner also.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Collectionsecondadmin");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			fnaHomePage.Accepted_Popup_close();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
			
			String collectionowner = PropertyReader.getProperty("Collectionowner");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionowner), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			
			String collectionemp = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionemp), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
		
			
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			
			
			String uName1 = PropertyReader.getProperty("Collectionlistpro");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);	
			fnaHomePage.loginValidation();			
			String Edit_Colle_Name= fnaHomePage.Random_EditUser();				
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress"); 
			String city = PropertyReader.getProperty("Collectioncity"); 
			String Zip = PropertyReader.getProperty("CollectionZip"); 
			String Country = PropertyReader.getProperty("CollectionCountry"); 
			String state = PropertyReader.getProperty("CollectionState"); 			
			Log.assertThat(fnaHomePage.Accepted_Collection_FromOwner(CollectionName,Edit_Colle_Name,Descrip,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			String uName2 = PropertyReader.getProperty("CollectionEmployee");
			String pWord2 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName2,pWord2);
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.New_Collection_Invitation(Edit_Colle_Name,Address,city,Zip,Country,state), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_024 (Collection List): Verify account owner updated the inactive collection or made the collection to inactive then this inactive collection should not display in "New collection
	invitation" popup.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 16, enabled = true, description = "TC_024 (Collection List): Verify account owner updated the inactive collection or made the collection to inactive then this inactive collection should not display.")
	public void verifyAccount_owner_update_inactive_collection_Onhold() throws Exception
	{
			
		try
		{
			Log.testCaseInfo("TC_024 (Collection List): Verify account owner updated the inactive collection or made the collection to inactive then this inactive collection should not display.");
			
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
			String collectionemp = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionemp), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			String uName1 = PropertyReader.getProperty("CollectionEmployee");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);	
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.Validate_New_Collection_Invitation(CollectionName), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
			fnaHomePage.Accepted_Popup_close();
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
			
			String uName2 = PropertyReader.getProperty("Collectionlistpro");
			String pWord2 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName2,pWord2);	
			fnaHomePage.loginValidation();	
			Log.assertThat(fnaHomePage.Collection_Edit_Setting(CollectionName), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
								
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_025 (Collection List): Verify account owner updated the inactive collection or made the collection to inactive then this inactive inactive collection to active collection for all the users in this
		collection.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 17, enabled = true, description = "TC_025 (Collection List): Verify account owner updated the inactive collection or made the collection to inactive then this inactive inactive collection to active collection users in this collection.")
	public void verifyAccount_owner_update_inactive_collection_Active() throws Exception
	{
		
		try
		{
			Log.testCaseInfo("TC_025 (Collection List): Verify account owner updated the inactive collection or made the collection to inactive then this inactive inactive collection to active collection  users in this collection.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
			String collectionemp = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionemp), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			String collectionSecond = PropertyReader.getProperty("Collectionsecond");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionSecond), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
			String CollectionLite = PropertyReader.getProperty("Collectionlite");
			Log.assertThat(fnaHomePage.adding_contact_from_address_book(CollectionLite), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
		
			
			Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
						
			String uName1 = PropertyReader.getProperty("CollectionEmployee");
			String pWord1 = PropertyReader.getProperty("Password");
			fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);	
			fnaHomePage.loginValidation();	
			String uName2 = PropertyReader.getProperty("Collectionsecondadmin");
			String pWord2 = PropertyReader.getProperty("Password");
			
			String uName3 = PropertyReader.getProperty("CollectionLiteemp");
			String pWord3 = PropertyReader.getProperty("Password");
			
			String uName4 = PropertyReader.getProperty("Collectionlistpro");
			String pWord4 = PropertyReader.getProperty("Password");
			
			String uName5 = PropertyReader.getProperty("CollectionEmployee");
			String pWord5 = PropertyReader.getProperty("Password");
			
			Log.assertThat(fnaHomePage.Accepted_Collection_FromEmployee_Secondadmin(CollectionName,uName2,pWord2,uName3,pWord3,uName4,pWord4,uName5,pWord5), "Accepted Collection Successful with valid credential", "Accepted Collection unsuccessful with valid credential", driver);
					
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_026 (Collection List): Verify second admin updated the inactive collection or made the collection to inactive then this inactive collection to active collection for all the users in this
	collection.
  *  Scripted By: Sekhar
 * @throws Exception
 */
@Test(priority = 18, enabled = true, description = "TC_026 (Collection List): Verify second admin updated the inactive collection or made the collection to inactive then this inactive collection to active collection users in this collection.")
public void verifyAccount_Second_update_inactive_collection_Active() throws Exception
{
	
	try
	{
		Log.testCaseInfo("TC_026 (Collection List): Verify second admin updated the inactive collection or made the collection to inactive then this inactive collection to active collection  users in this collection.");
		loginPage = new LoginPage(driver).get();
		String uName = PropertyReader.getProperty("Collectionsecondadmin");
		String pWord = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
		fnaHomePage.loginValidation();
		fnaHomePage.Accepted_Popup_close();		
		String CollectionName= fnaHomePage.Random_Collectionname();								
		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
		String collectionemp = PropertyReader.getProperty("CollectionEmp");
		Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionemp), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
		String collectionSecond = PropertyReader.getProperty("Collectionowner");
		Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionSecond), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
		String CollectionLite = PropertyReader.getProperty("Collectionlite");
		Log.assertThat(fnaHomePage.adding_contact_from_address_book(CollectionLite), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
	
		
		Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
					
		String uName1 = PropertyReader.getProperty("CollectionEmployee");
		String pWord1 = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);	
		fnaHomePage.loginValidation();	
		String uName2 = PropertyReader.getProperty("Collectionlistpro");
		String pWord2 = PropertyReader.getProperty("Password");
		
		String uName3 = PropertyReader.getProperty("CollectionLiteemp");
		String pWord3 = PropertyReader.getProperty("Password");
		
		String uName4 = PropertyReader.getProperty("Collectionsecondadmin");
		String pWord4 = PropertyReader.getProperty("Password");
		
		String uName5 = PropertyReader.getProperty("CollectionEmployee");
		String pWord5 = PropertyReader.getProperty("Password");
		
		Log.assertThat(fnaHomePage.Accepted_Collection_FromEmployee_Secondadmin(CollectionName,uName2,pWord2,uName3,pWord3,uName4,pWord4,uName5,pWord5), "Accepted Collection Successful with valid credential", "Accepted Collection unsuccessful with valid credential", driver);
				
	}
	catch(Exception e)
	{
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

	/** TC_027 (Collection List): Verify employee delete the shared collection(before or after accepting the collection).
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
@Test(priority = 19, enabled = true, description = "TC_027 (Collection List): Verify employee delete the shared collection(before or after accepting the collection).")
public void verifyAccount_Second_delete_collection() throws Exception
{
	
	try
	{
		Log.testCaseInfo("TC_027 (Collection List): Verify employee delete the shared collection(before or after accepting the collection).");
		loginPage = new LoginPage(driver).get();
		String uName = PropertyReader.getProperty("Collectionlistpro");
		String pWord = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
		fnaHomePage.loginValidation();
		fnaHomePage.Accepted_Popup_close();		
		String CollectionName= fnaHomePage.Random_Collectionname();								
		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
		String collectionemp = PropertyReader.getProperty("CollectionEmp");
		Log.assertThat(fnaHomePage.adding_contact_from_address_book(collectionemp), "Adding contact Successfull", "Adding contact UnSuccessful", driver);
		Log.assertThat(fnaHomePage.logout(), "Logout Successful with valid credential", "Logout unsuccessful with valid credential", driver);
		
		String uName1 = PropertyReader.getProperty("CollectionEmployee");
		String pWord1 = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName1,pWord1);	
		fnaHomePage.loginValidation();	
		Log.assertThat(fnaHomePage.Validate_New_Collection_Invitation(CollectionName), "Edit Collection Successful with valid credential", "Edit Collection unsuccessful with valid credential", driver);
		fnaHomePage.Accepted_Popup_close();
		String uName2 = PropertyReader.getProperty("Collectionlistpro");
		String pWord2 = PropertyReader.getProperty("Password");
		String Contact_Name = PropertyReader.getProperty("CollectionEmp");
		String uName3 = PropertyReader.getProperty("CollectionEmployee");
		String pWord3 = PropertyReader.getProperty("Password");
		
		Log.assertThat(fnaHomePage.Collection_Delete(CollectionName,uName2,pWord2,Contact_Name,uName3,pWord3), "Delete the Collection Successful with valid credential", "Delete the Collection unsuccessful with valid credential", driver);
				
		
	}
	catch(Exception e)
	{
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_028&029 (Collection List): Verify In collection owner tab verify 'Add to favorite tab check box, save changes & close,close button.
  *  Scripted By: Sekhar
 * @throws Exception
 */
@Test(priority = 20, enabled = true, description = "TC_028&029 (Collection List): Verify In collection owner tab verify 'Add to favorite tab check box, save changes & close,close button.")
public void verifyCollection_owner_Addfavorite_save_close() throws Exception
{	
	try
	{
		Log.testCaseInfo("TC_028&029 (Collection List): Verify In collection owner tab verify 'Add to favorite tab check box, save changes & close,close button.");
		loginPage = new LoginPage(driver).get();
		String uName = PropertyReader.getProperty("Collectionlistpro");
		String pWord = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
		fnaHomePage.loginValidation();
		fnaHomePage.Accepted_Popup_close();		
		String CollectionName= fnaHomePage.Random_Collectionname();								
		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
		Log.assertThat(fnaHomePage.collection_Owner_Addfavorite_save_close(CollectionName), "In Collection owner tab addfavorite and save &close Successfull", "In Collection owner tab addfavorite and save &close UnSuccessful", driver);
			
	}
	catch(Exception e)
	{
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}


/** TC_030&031 (Collection List): Verify collection owner tab when user click on the 'Change collection owner' should display list.
  *  Scripted By: Sekhar
 * @throws Exception
 */
@Test(priority = 21, enabled = true, description = "TC_030&031 (Collection List): Verify collection owner tab when user click on the 'Change collection owner' should display list.")
public void verifyChange_collection_owner() throws Exception
{	
	try
	{
		Log.testCaseInfo("TC_030&031 (Collection List): Verify collection owner tab when user click on the 'Change collection owner' should display list.");

		loginPage = new LoginPage(driver).get();
		String uName = PropertyReader.getProperty("Collectionlistpro");
		String pWord = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
		fnaHomePage.loginValidation();
		fnaHomePage.Accepted_Popup_close();		
		String CollectionName= fnaHomePage.Random_Collectionname();								
		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
		Log.assertThat(fnaHomePage.Change_collection_owner(CollectionName), "Change collection owner Successfull", "Change collection owner UnSuccessful", driver);
			
	}
	catch(Exception e)
	{
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_032&033 (Collection List): Verify once account owner made his employee add collection owner then this employee added in owner place. 
 *  Scripted By: Sekhar
 * @throws Exception
 */
@Test(priority = 22, enabled = true, description = "TC_032&033 (Collection List): Verify once account owner made his employee add collection owner then this employee added in owner place")
public void verifyChange_collection_owner_employee() throws Exception
{
	try
	{
		Log.testCaseInfo("TC_032&033 (Collection List): Verify once account owner made his employee add collection owner then this employee added in owner place.");
		loginPage = new LoginPage(driver).get();
		String uName = PropertyReader.getProperty("Collectionlistpro");
		String pWord = PropertyReader.getProperty("Password");
		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
		fnaHomePage.loginValidation();
		fnaHomePage.Accepted_Popup_close();		
		String CollectionName= fnaHomePage.Random_Collectionname();								
		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
		String EmployeeName = PropertyReader.getProperty("CollectionEmp");
		Log.assertThat(fnaHomePage.Change_collection_Owner_SelectNewCollection(CollectionName,EmployeeName), "Change collection owner Successfull", "Change collection owner UnSuccessful", driver);
				
	}
	catch(Exception e)
	{
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}


}
