package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommonLoginPage;
import com.arc.fna.pages.FnAMyProfilePage;
import com.arc.fna.pages.FnARegistrationPage;
import com.arc.fna.pages.FnaContactTabPage;
import com.arc.fna.pages.FnaCreateGroupPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.ProjectsMyProfPage;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FNA_MyProfile {
    static WebDriver driver;
    LoginPage loginpage;
    FnaHomePage fnahomepage;
    FnaContactTabPage fnacontacttabpage;
    FnaCreateGroupPage fnacreategrppage;
    FnAMyProfilePage myprofpage;
    FnARegistrationPage regpage;
    ProjectsMyProfPage projectmyprogpage;
    CommonLoginPage commonpage;
    
    
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) {
		
    	if(browser.equalsIgnoreCase("firefox")) {
    		File dest = new File("./drivers/win/geckodriver.exe");
    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
    		driver = new FirefoxDriver();
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
    	else if (browser.equalsIgnoreCase("chrome")) { 
    		File dest = new File("./drivers/win/chromedriver.exe");
    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
    		ChromeOptions options = new ChromeOptions();
    		options.addArguments("--start-maximized");
    		driver = new ChromeDriver( options );
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
    	else if (browser.equalsIgnoreCase("safari"))
    	{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
    	}
   return driver;
}

    /** TC_001(My Profile) : Verify editing of data in all fields of My Profile section
     * @throws Exception 
        *  Created by Trinanjwan
        */
        @Test(priority=0,enabled=true,description="TC_001(My Profile) : Verify editing of data in all fields of My Profile section")
        public void editingFillingOfData() throws Exception {
          try {
        	    Log.testCaseInfo("TC_001(My Profile) : Verify editing of data in all fields of My Profile section");
        	    loginpage = new LoginPage(driver).get();
        	    String uName=PropertyReader.getProperty("unametri");
        	    String pWord=PropertyReader.getProperty("pwdtri");
        	    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
        	    fnahomepage.loginValidation();
        	    fnahomepage.presenceOfMyProfileBtn();
        	    myprofpage= fnahomepage.landingIntoMyProfilePage();
        	    myprofpage.verifyLandingOfMyProfilepage();
        	    String rfirstname=myprofpage.generateRandomfirstname();
        	    String rlastname=myprofpage.generateRandomlastname();
        	    String rtitle=myprofpage.generateRandomtitle();
        	    String rcompany=myprofpage.generateRandomcompanyname();
        	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
         Log.assertThat(myprofpage.validatingMyProfileData(rfirstname, rlastname, rtitle, rcompany),"Editing of data in all fields of My Profile section done successfully",
         				"Verify editing of data in all fields of My Profile section got failed",driver);
        	    
              }
          catch(Exception e)
          {
                 e.getCause();
                 Log.exception(e, driver);
          }
          finally
          {
                 Log.endTestCase();
                 driver.quit();
          }
    	
        }
        
        
        
        /** TC_002(My Profile) : Verify filling of data in all fields of My Profile section.
         * Implemented this after fresh registration 
         * @throws Exception 
            *  Created by Trinanjwan
            */
            @Test(priority=1,enabled=true,description="TC_002(My Profile) : Verify filling of data in all fields of My Profile section.")
            public void verifyFillingOfData() throws Exception {
              try {
            	    Log.testCaseInfo("TC_002(My Profile) : Verify filling of data in all fields of My Profile section.");
            	    
            	    loginpage = new LoginPage(driver).get();
            	    regpage=new FnARegistrationPage(driver).get();
            	    String Emailrad=regpage.Random_Email();
            	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
            	    projectmyprogpage.handlingsamplevideoPROJECTS();
            	    projectmyprogpage.handlingNewFeaturepopover();
            	    projectmyprogpage.presenceOfPrjDashboardButton();
            	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
            	    fnahomepage=commonpage.activateFnAModule();
            	    fnahomepage.handlingNewFeaturepopover();
            	    fnahomepage.handlingDemoVideoepopover();
            	    myprofpage= fnahomepage.landingIntoMyProfilePage();
            	    myprofpage.verifyLandingOfMyProfilepage();
            	    String rfirstname=myprofpage.generateRandomfirstname();
            	    String rlastname=myprofpage.generateRandomlastname();
            	    String rtitle=myprofpage.generateRandomtitle();
            	    String rcompany=myprofpage.generateRandomcompanyname();
            	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
             Log.assertThat(myprofpage.validatingMyProfileData(rfirstname, rlastname, rtitle, rcompany),"Filling of data in all fields of My Profile section done successfully",
             				"Verify filling of data in all fields of My Profile section got failed",driver);
            	        
                  }
              catch(Exception e)
              {
                     e.getCause();
                     Log.exception(e, driver);
              }
              finally
              {
                     Log.endTestCase();
                     driver.quit();
              }
        	
            }
        
    
    
    
        /** TC_003(My Profile) : Verify changing of Password.
        * Implemented this after fresh registration 
         * @throws Exception 
        *  Created by Trinanjwan
         */
        @Test(priority=2,enabled=true,description="TC_003(My Profile) : Verify changing of Password.")
        public void verifyChangingOfPassword() throws Exception {
          try {
        	    Log.testCaseInfo("TC_003(My Profile) : Verify changing of Password.");
        	    
        	    loginpage = new LoginPage(driver).get();
        	    regpage=new FnARegistrationPage(driver).get();
        	    String Emailrad=regpage.Random_Email();
        	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
        	    projectmyprogpage.handlingsamplevideoPROJECTS();
        	    projectmyprogpage.handlingNewFeaturepopover();
        	    projectmyprogpage.presenceOfPrjDashboardButton();
        	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
        	    fnahomepage=commonpage.activateFnAModule();
        	    fnahomepage.handlingNewFeaturepopover();
        	    fnahomepage.handlingDemoVideoepopover();
        	    myprofpage= fnahomepage.landingIntoMyProfilePage();
        	    myprofpage.verifyLandingOfMyProfilepage();
        	    String rfirstname=myprofpage.generateRandomfirstname();
        	    String rlastname=myprofpage.generateRandomlastname();
        	    String rtitle=myprofpage.generateRandomtitle();
        	    String rcompany=myprofpage.generateRandomcompanyname();
        	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
        	    String rpass=myprofpage.generateRandompassword();
        	    loginpage=myprofpage.changingOfPassword(rpass);
        	    fnahomepage=loginpage.loginWithValidCredential(Emailrad,rpass);
        	    Log.assertThat(fnahomepage.loginValidation(), "Change Password functionality is verfied successfully", 
        	    		"Change Password functionality got failed",driver);
        	       
              }
          catch(Exception e)
          {
                 e.getCause();
                 Log.exception(e, driver);
          }
          finally
          {
                 Log.endTestCase();
                 driver.quit();
          }
    	
        }
    
      
        /** TC_004(My Profile) : Verify changing of Email ID getting changed in PROJECTS
         * Implemented this after fresh registration 
          * @throws Exception 
         *  Created by Trinanjwan
          */
         @Test(priority=3,enabled=true,description="TC_004(My Profile) : Verify changing of Email ID getting changed in PROJECTS")
         public void verifyChangingOfemail() throws Exception {
           try {
         	    Log.testCaseInfo("TC_004(My Profile) : Verify changing of Email ID getting changed in PROJECTS");
         	    
         	    loginpage = new LoginPage(driver).get();
         	    regpage=new FnARegistrationPage(driver).get();
         	    String Emailrad=regpage.Random_Email();
         	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
         	    projectmyprogpage.handlingsamplevideoPROJECTS();
         	    projectmyprogpage.handlingNewFeaturepopover();
         	    projectmyprogpage.presenceOfPrjDashboardButton();
         	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
         	    fnahomepage=commonpage.activateFnAModule();
         	    fnahomepage.handlingNewFeaturepopover();
         	    fnahomepage.handlingDemoVideoepopover();
         	    myprofpage= fnahomepage.landingIntoMyProfilePage();
         	    myprofpage.verifyLandingOfMyProfilepage();
         	    String rfirstname=myprofpage.generateRandomfirstname();
         	    String rlastname=myprofpage.generateRandomlastname();
         	    String rtitle=myprofpage.generateRandomtitle();
         	    String rcompany=myprofpage.generateRandomcompanyname();
         	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
         	    String remail=myprofpage.Random_Email();
         	    projectmyprogpage=myprofpage.updateEmail(remail);
         	    Log.assertThat(projectmyprogpage.verificationOfChangedEmail(remail),"Email is validated successfully",
         			   "Email validation got failed",driver);

               }
           catch(Exception e)
           {
                  e.getCause();
                  Log.exception(e, driver);
           }
           finally
           {
                  Log.endTestCase();
                  driver.quit();
           }
     	
         }
         
         
         /** TC_005(My Profile) : Verify changing of Company name getting changed in PROJECTS
          * Implemented this after fresh registration 
           * @throws Exception 
          *  Created by Trinanjwan
           */
          @Test(priority=4,enabled=true,description="TC_005(My Profile) : Verify changing of Company name getting changed in PROJECTS")
          public void verifyChangingOfCompanyName() throws Exception {
            try {
          	    Log.testCaseInfo("TC_005(My Profile) : Verify changing of Company name getting changed in PROJECTS");
          	    
          	    loginpage = new LoginPage(driver).get();
          	    regpage=new FnARegistrationPage(driver).get();
          	    String Emailrad=regpage.Random_Email();
          	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
          	    projectmyprogpage.handlingsamplevideoPROJECTS();
          	    projectmyprogpage.handlingNewFeaturepopover();
          	    projectmyprogpage.presenceOfPrjDashboardButton();
          	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
          	    fnahomepage=commonpage.activateFnAModule();
          	    fnahomepage.handlingNewFeaturepopover();
          	    fnahomepage.handlingDemoVideoepopover();
          	    myprofpage= fnahomepage.landingIntoMyProfilePage();
          	    myprofpage.verifyLandingOfMyProfilepage();
          	    String rfirstname=myprofpage.generateRandomfirstname();
          	    String rlastname=myprofpage.generateRandomlastname();
          	    String rtitle=myprofpage.generateRandomtitle();
          	    String rcompany=myprofpage.generateRandomcompanyname();
          	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
          	    String rcompanynew=myprofpage.generateRandomcompanyname();
          	    projectmyprogpage=myprofpage.updatecompany(rcompanynew);
          	    Log.assertThat(projectmyprogpage.verificationOfChangedcompany(rcompanynew),"Company Name is validated successfully",
          			   "Company Name validation got failed",driver);

                }
            catch(Exception e)
            {
                   e.getCause();
                   Log.exception(e, driver);
            }
            finally
            {
                   Log.endTestCase();
                   driver.quit();
            }
      	
          }
     
          
          
          /** TC_006(My Profile) : Verify changing of Phone Number getting changed in PROJECTS.
           * Implemented this after fresh registration 
            * @throws Exception 
           *  Created by Trinanjwan
            */
           @Test(priority=5,enabled=true,description="TC_006(My Profile) : Verify changing of Phone Number getting changed in PROJECTS.")
           public void verifyChangingOfPhnumber() throws Exception {
             try {
           	    Log.testCaseInfo("TC_006(My Profile) : Verify changing of Phone Number getting changed in PROJECTS.");
           	    
           	    loginpage = new LoginPage(driver).get();
           	    regpage=new FnARegistrationPage(driver).get();
           	    String Emailrad=regpage.Random_Email();
           	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
           	    projectmyprogpage.handlingsamplevideoPROJECTS();
           	    projectmyprogpage.handlingNewFeaturepopover();
           	    projectmyprogpage.presenceOfPrjDashboardButton();
           	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
           	    fnahomepage=commonpage.activateFnAModule();
           	    fnahomepage.handlingNewFeaturepopover();
           	    fnahomepage.handlingDemoVideoepopover();
           	    myprofpage= fnahomepage.landingIntoMyProfilePage();
           	    myprofpage.verifyLandingOfMyProfilepage();
           	    String rfirstname=myprofpage.generateRandomfirstname();
           	    String rlastname=myprofpage.generateRandomlastname();
           	    String rtitle=myprofpage.generateRandomtitle();
           	    String rcompany=myprofpage.generateRandomcompanyname();
           	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
           	    myprofpage.updatePhone();
           	    Log.assertThat(projectmyprogpage.verificationOfChangedphonenumber(),"Phone Number is validated successfully",
           			   "Phone number validation got failed",driver);

                 }
             catch(Exception e)
             {
                    e.getCause();
                    Log.exception(e, driver);
             }
             finally
             {
                    Log.endTestCase();
                    driver.quit();
             }
       	
           }
          
 

           /** TC_007(My Profile) : Verify the user should be able to login with the changed Email ID.
            * Implemented this after fresh registration 
             * @throws Exception 
            *  Created by Trinanjwan
             */
            @Test(priority=6,enabled=true,description="TC_007(My Profile) : Verify the user should be able to login with the changed Email ID.")
            public void verifyChangingOfemailandLogin() throws Exception {
              try {
            	    Log.testCaseInfo("TC_007(My Profile) : Verify the user should be able to login with the changed Email ID.");
            	    
            	    loginpage = new LoginPage(driver).get();
            	    regpage=new FnARegistrationPage(driver).get();
            	    String Emailrad=regpage.Random_Email();
            	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
            	    projectmyprogpage.handlingsamplevideoPROJECTS();
            	    projectmyprogpage.handlingNewFeaturepopover();
            	    projectmyprogpage.presenceOfPrjDashboardButton();
            	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
            	    fnahomepage=commonpage.activateFnAModule();
            	    fnahomepage.handlingNewFeaturepopover();
            	    fnahomepage.handlingDemoVideoepopover();
            	    myprofpage= fnahomepage.landingIntoMyProfilePage();
            	    myprofpage.verifyLandingOfMyProfilepage();
            	    String rfirstname=myprofpage.generateRandomfirstname();
            	    String rlastname=myprofpage.generateRandomlastname();
            	    String rtitle=myprofpage.generateRandomtitle();
            	    String rcompany=myprofpage.generateRandomcompanyname();
            	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
            	    String remail=myprofpage.Random_Email();
            	    projectmyprogpage=myprofpage.updateEmail(remail);
            	    fnahomepage=projectmyprogpage.navigatingToFnAHome();
            	    fnahomepage.presenceOfMyProfileBtn();
            	    fnahomepage.logout();
            	    
            	    loginpage = new LoginPage(driver).get();
            	    String pWord=PropertyReader.getProperty("pwdtri");
            	    fnahomepage=loginpage.loginWithValidCredential(remail,pWord);
            	    
            	    Log.assertThat(fnahomepage.loginValidation(),"The user able to login with the chnaged Email ID",
            	    		"The user unable to login with the chnaged Email ID",driver);

                  }
              catch(Exception e)
              {
                     e.getCause();
                     Log.exception(e, driver);
              }
              finally
              {
                     Log.endTestCase();
                     driver.quit();
              }
        	
            }
           
           
            /** TC_008(My Profile) : Verify user able login with updated user ID
             * Implemented this after fresh registration 
              * @throws Exception 
             *  Created by Trinanjwan
              */
             @Test(priority=7,enabled=true,description="TC_008(My Profile) : Verify user able login with updated user ID")
             public void verifyChangingOfUserID() throws Exception {
               try {
             	    Log.testCaseInfo("TC_008(My Profile) : Verify user able login with updated user ID");
             	    
             	    loginpage = new LoginPage(driver).get();
             	    regpage=new FnARegistrationPage(driver).get();
             	    String Emailrad=regpage.Random_Email();
             	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
             	    projectmyprogpage.handlingsamplevideoPROJECTS();
             	    projectmyprogpage.handlingNewFeaturepopover();
             	    projectmyprogpage.presenceOfPrjDashboardButton();
             	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
             	    fnahomepage=commonpage.activateFnAModule();
             	    fnahomepage.handlingNewFeaturepopover();
             	    fnahomepage.handlingDemoVideoepopover();
             	    myprofpage= fnahomepage.landingIntoMyProfilePage();
             	    myprofpage.verifyLandingOfMyProfilepage();
             	    String rfirstname=myprofpage.generateRandomfirstname();
             	    String rlastname=myprofpage.generateRandomlastname();
             	    String rtitle=myprofpage.generateRandomtitle();
             	    String rcompany=myprofpage.generateRandomcompanyname();
             	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
             	    String raduserid=myprofpage.Random_Userid();
             	    myprofpage.updateUserid(raduserid);
             	    fnahomepage = new FnaHomePage(driver).get();
             	    fnahomepage.logout();
 
             	    loginpage = new LoginPage(driver).get();
             	    String pWord=PropertyReader.getProperty("pwdtri");
             	    fnahomepage=loginpage.loginWithValidCredential(raduserid,pWord);
             	    Log.assertThat(fnahomepage.loginValidation(),"The user able to login with the updated User ID",
             	    		"The user unable to login with the updated User ID",driver);

                   }
               catch(Exception e)
               {
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
         	
             }
            
             

             /** TC_009(My Profile) : Verify if user select other occupation user able to input in text box & that should be reflected.
              * Implemented this after fresh registration 
               * @throws Exception 
              *  Created by Trinanjwan
               */
              @Test(priority=8,enabled=true,description="TC_009(My Profile) : Verify if user select other occupation user able to input in text box & that should be reflected.")
              public void verifyOtherField() throws Exception {
                try {
              	    Log.testCaseInfo("TC_008(My Profile) : Verify user able login with updated user ID");
              	    
              	    loginpage = new LoginPage(driver).get();
              	    regpage=new FnARegistrationPage(driver).get();
              	    String Emailrad=regpage.Random_Email();
              	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
              	    projectmyprogpage.handlingsamplevideoPROJECTS();
              	    projectmyprogpage.handlingNewFeaturepopover();
              	    projectmyprogpage.presenceOfPrjDashboardButton();
              	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
              	    fnahomepage=commonpage.activateFnAModule();
              	    fnahomepage.handlingNewFeaturepopover();
              	    fnahomepage.handlingDemoVideoepopover();
              	    myprofpage= fnahomepage.landingIntoMyProfilePage();
              	    myprofpage.verifyLandingOfMyProfilepage();
              	    String rfirstname=myprofpage.generateRandomfirstname();
              	    String rlastname=myprofpage.generateRandomlastname();
              	    String rtitle=myprofpage.generateRandomtitle();
              	    String rcompany=myprofpage.generateRandomcompanyname();
              	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
              	    myprofpage.checkOtherField();
              	    Log.assertThat(myprofpage.validateOtherfielddata(), "The OTHER field data is validated successfully",
              	    		"The OTHER field data is not validated successfully");
  
                    }
                catch(Exception e)
                {
                       e.getCause();
                       Log.exception(e, driver);
                }
                finally
                {
                       Log.endTestCase();
                       driver.quit();
                }
          	
              }
             
              
              /** TC_0010(My Profile) : Verify changing of Country Name and State getting  changed in PROJECTS.
               * Implemented this after fresh registration 
                * @throws Exception 
               *  Created by Trinanjwan
                */
               @Test(priority=9,enabled=true,description="TC_0010(My Profile) : Verify changing of Country Name and State getting  changed in PROJECTS.")
               public void verifychangingofStateCountry() throws Exception {
                 try {
               	    Log.testCaseInfo("TC_0010(My Profile) : Verify changing of Country Name and State getting  changed in PROJECTS.");
               	    
               	    loginpage = new LoginPage(driver).get();
               	    regpage=new FnARegistrationPage(driver).get();
               	    String Emailrad=regpage.Random_Email();
               	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
               	    projectmyprogpage.handlingsamplevideoPROJECTS();
               	    projectmyprogpage.handlingNewFeaturepopover();
               	    projectmyprogpage.presenceOfPrjDashboardButton();
               	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
               	    fnahomepage=commonpage.activateFnAModule();
               	    fnahomepage.handlingNewFeaturepopover();
               	    fnahomepage.handlingDemoVideoepopover();
               	    myprofpage= fnahomepage.landingIntoMyProfilePage();
               	    myprofpage.verifyLandingOfMyProfilepage();					
               	    String rfirstname=myprofpage.generateRandomfirstname();		
               	    String rlastname=myprofpage.generateRandomlastname();
               	    String rtitle=myprofpage.generateRandomtitle();
               	    String rcompany=myprofpage.generateRandomcompanyname();
               	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
               	    projectmyprogpage=myprofpage.updateCountryState();
               	    Log.assertThat(projectmyprogpage.verificationOfChangedStateandCountry(), "Country and State are validated successfully",
               	    		"Country and State validation got failed",driver);
   
                     }
                 catch(Exception e)
                 {
                        e.getCause();
                        Log.exception(e, driver);
                 }
                 finally
                 {
                        Log.endTestCase();
                        driver.quit();
                 }
           	
               }
     
              
               /** TC_0011(My Profile) : Verify updated project role should display in team user section.
                * Implemented this after fresh registration 
                 * @throws Exception 
                *  Created by Trinanjwan
                 */
                @Test(priority=10,enabled=true,description="TC_0011(My Profile) : Verify updated project role should display in team user section.")
                public void updateProjectRole() throws Exception {
                  try {
                	    Log.testCaseInfo("TC_0011(My Profile) : Verify updated project role should display in team user section.");
                	    
                	    loginpage = new LoginPage(driver).get();
                	    regpage=new FnARegistrationPage(driver).get();
                	    String Emailrad=regpage.Random_Email();
                	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
                	    projectmyprogpage.handlingsamplevideoPROJECTS();
                	    projectmyprogpage.handlingNewFeaturepopover();
                	    projectmyprogpage.presenceOfPrjDashboardButton();
                	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
                	    fnahomepage=commonpage.activateFnAModule();
                	    fnahomepage.handlingNewFeaturepopover();
                	    fnahomepage.handlingDemoVideoepopover();
                	    myprofpage= fnahomepage.landingIntoMyProfilePage();
                	    myprofpage.verifyLandingOfMyProfilepage();					
                	    String rfirstname=myprofpage.generateRandomfirstname();		
                	    String rlastname=myprofpage.generateRandomlastname();
                	    String rtitle=myprofpage.generateRandomtitle();
                	    String rcompany=myprofpage.generateRandomcompanyname();
                	    myprofpage.savingOfMyProfileData(rfirstname, rlastname, rtitle, rcompany);
                	    fnahomepage=myprofpage.navigateToCollection();
                	    String CollectionName= fnahomepage.Random_Collectionname();	
                	    fnahomepage.Create_Collections(CollectionName);
                	    Log.assertThat(fnahomepage.verifyProjectRole(),"The PROJECT ROLE is verified successfully",
                	    		"The PROJECT ROLE is not verified successfully",driver);
                	      	 
    
                      }
                  catch(Exception e)
                  {
                         e.getCause();
                         Log.exception(e, driver);
                  }
                  finally
                  {
                         Log.endTestCase();
                         driver.quit();
                  }
            	
                }
               
                             
               
}

