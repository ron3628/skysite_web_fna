package com.arc.fna.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommunicationPage;
import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FNA_HomeExtraModulePAge;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.RecordingClass;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_AlbumTab 
{
	static WebDriver driver;
    LoginPage loginpage;    
	FnaHomePage fnahomepage;    
	FNAAlbumPage fnaalbumpage;
	FNA_HomeExtraModulePAge fnahomextramodule;
	CommunicationPage compage;
	    
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) 
    {
    	
    	if(browser.equalsIgnoreCase("firefox")) 
    	{
    		File dest = new File("./drivers/win/geckodriver.exe");
    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
    		driver = new FirefoxDriver();
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
    	else if (browser.equalsIgnoreCase("chrome")) 
    	{ 
    		File dest = new File("./drivers/win/chromedriver.exe");
    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            ChromeOptions options = new ChromeOptions();
        	options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
        	driver = new ChromeDriver( options );
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
        } 
    	else if (browser.equalsIgnoreCase("safari"))
    	{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
			
    	}
	   
    	return driver;
	}

    
 
    
    /**TC_001(Album):Verify creation of album
     * @throws Exception 
     *     
     */
    @Test(priority=0, enabled=true, description=" Verify creation of album ")
    public void verifyCreationAlbum() throws Exception 
    {
    	try 
    	{
		  	
    		Log.testCaseInfo(" TC_001(Album):Verify creation of album ");
		  	
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
		    
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
		    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname)," Album is created successfully "," Album cannot be created ");
		    
		    fnaalbumpage.deleteAlbum();
			   
    	}
		   
    	catch(Exception e)
    	{	  
    		e.getCause();
    		Log.exception(e, driver);
     
    	}
    	finally
    	{
    		Log.endTestCase();   
    		driver.quit();
    	}
		 
    }
	   

    
    
    /**TC_002(Album):Verify upload of photo to a album
     * @throws Exception 
     *     
     */
    @Test(priority=1, enabled=true, description=" Verify upload of photo to album ")
    public void verifyuploadPhotoToAlbum() throws Exception  
    {	       
    	try 
    	{
	    	     
    		Log.testCaseInfo(" TC_002(Album):Verify upload of photo to a album ");
			    
    		loginpage = new LoginPage(driver).get();			    
    		String uName=PropertyReader.getProperty("USERID");			    
    		String pWord=PropertyReader.getProperty("PASSFA");			    
    		fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
    		fnahomepage.loginValidation();
			    
		 
    		String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");				
    		fnahomepage.selectcollection(collectionname);				
    		fnahomextramodule=fnahomepage.clickdocuments();			
    		fnaalbumpage=fnahomextramodule.clickAlbumIcon();			
    		fnaalbumpage.checkAnyAlbumExist();		    
    		String albumname=fnaalbumpage.Random_Albumname();			    
    		Log.assertThat(fnaalbumpage.createAlbum(albumname)," Album is created successfully "," Album cannot be created "); 
			    		 
    		File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum")); 		       	
    		String filepath=fis.getAbsolutePath().toString(); 		          
    		File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	       	    
    		String tempfilepath=fis1.getAbsolutePath().toString();	            
    		String filename=PropertyReader.getProperty("photoname");
			    
    		Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath)," Photo has been successfully uploaded to the album "," Photo cannot be uploaded to the album ");
			   
    		fnaalbumpage.deleteAlbum();
	       
    	}	       
    	catch(Exception e)		      
    	{	             
    		e.getCause();		             
    		Log.exception(e, driver);
	 		      
    	}	      
    	finally		      
    	{		             
    		Log.endTestCase();		             
    		driver.quit();
		       		      
    	}
		  
    }

    
    
    
	/**TC_003(Album):Verify deletion of a album
	 * @throws Exception 
	 *     
	 */
    @Test(priority=2, enabled=true, description=" Verify deletion of a album ")
    public void deletionAlbum() throws Exception 
    {			   
    	try 
    	{
    		Log.testCaseInfo(" TC_003(Album):Verify deletion of a album ");
    		
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
		    
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
		    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname)," Album is created successfully "," Album cannot be created ");
		    
		    Log.assertThat(fnaalbumpage.verifyDeletionOfAlbum(albumname)," Album is deleted successfully ", " Album is not deleted successfully ");
					   
    	} 
	    catch(Exception e)
    	{	    	
             e.getCause();
             Log.exception(e, driver);
			      
    	}				      
    	finally		      
    	{		             
    		Log.endTestCase();
			driver.quit();
			
        }
			   
    }
		   
    
    
    
    /**TC_004(Album):Verify rename of a album
	 * @throws Exception 
     * 
     */
    @Test(priority=3, enabled=true, description=" Verify rename of a album ")
    public void renameAlbum() throws Exception 
    {			    
    	try 
    	{
    		Log.testCaseInfo(" TC_004(Album):Verify rename of a album ");
    		
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
		    
	    	String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
		    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname)," Album is created successfully "," Album cannot be created ");
		    
		    String renamedalbum=fnaalbumpage.Random_Albumrename();
		    
		    Log.assertThat(fnaalbumpage.renameAlbum(renamedalbum)," Album renamed successfully "," Album cannot be renamed ");
		    
		    fnaalbumpage.deleteAlbum();
		    
		  }
					   
    	  catch(Exception e)
	      {
    		  e.getCause();
              Log.exception(e, driver);
              
	      }
	      finally
	      {
              Log.endTestCase();
         	  driver.quit();
         	  
	      }
				   
	}
    
    
    
    
	/**TC_005(Album):Verify download of album
	 * @throws Exception 
	 *    
	 */
    @Test(priority=4, enabled=true, description=" Verify download of album ")
    public void verifyDownloadAlbum() throws Exception 
    {
    	try 
    	{
    		Log.testCaseInfo("TC_005(Album):Verify download of album");
    		
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
						    
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
		    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname)," Album is created successfully "," Album cannot be created ");
		    
		    File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));
	       	String filepath=fis.getAbsolutePath().toString();
	       	File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
       	    String tempfilepath=fis1.getAbsolutePath().toString();
            String filename=PropertyReader.getProperty("photoname");
            
		    Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath)," Photo has been successfully uploaded to the album "," Photo cannot be uploaded to the album ");
		    
		    String usernamedir=System.getProperty("user.name");
		    String downloadpath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
		    
     	    Log.assertThat(fnaalbumpage.downloadAlbum(downloadpath,albumname)," Album is downloaded successfully and found in the download folder "," Album is not found in the download folder ");
     	    fnaalbumpage.deleteAlbum();
						   
		  }
    	  catch(Exception e)
    	  {
    		  e.getCause();
              Log.exception(e, driver);
              
	      }
	      finally
	      {
	    	  Log.endTestCase();
              driver.quit();
					      
	      }
					   
    }
			
    
    
    
    /**TC_006(Album):Verify share album with communication option 
     * @throws Exception 
     * 
     */					
    @Test(priority=5, enabled=true, description=" Verify share album with communication option ")   
    public void verifyShareAlbumSendViaCommunicationOption() throws Exception 
    {								
    	try 
    	{																	    
    		Log.testCaseInfo("TC_006(Album):Verify share album with communication option ");
									 	
    		RecordingClass.recordTestCase("ShareAlbumSendViaCommunication");		
    		loginpage = new LoginPage(driver).get();		
    		String uName=PropertyReader.getProperty("USERID");		
    		String pWord=PropertyReader.getProperty("PASSFA");		
    		fnahomepage=loginpage.loginWithValidCredential(uName,pWord);		
    		fnahomepage.loginValidation();
									    
    		String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");		
    		fnahomepage.selectcollection(collectionname);		
    		fnahomextramodule=fnahomepage.clickdocuments();	
    		fnaalbumpage=fnahomextramodule.clickAlbumIcon();		
    		fnaalbumpage.checkAnyAlbumExist();	
    		String albumname=fnaalbumpage.Random_Albumname();
		
    		Log.assertThat(fnaalbumpage.createAlbum(albumname),"Album is created successfully","Album cannot be created");
		
    		File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));		
    		String filepath=fis.getAbsolutePath().toString();		
    		File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	
    		String tempfilepath=fis1.getAbsolutePath().toString();		
    		String filename=PropertyReader.getProperty("Filenameforcommunication");		
    		String downloadedalbum=albumname + ".zip";
		
    		Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath),"Photo has been successfully uploaded to the album","Photo cannot be uploaded to the album");
				
    		String usernamedir=System.getProperty("user.name");	
    		String downloadpath="C:\\" + "Users\\" + usernamedir + "\\Downloads";		
    		String SubjectCom=fnaalbumpage.generateRandomCmnctnSbjct();		
    		fnaalbumpage.shareViaCommunication(downloadpath,downloadedalbum,SubjectCom);		
    		fnaalbumpage.landingcomnctnpage();	
    		compage= new CommunicationPage(driver).get();
		
    		Log.assertThat(compage.verifycommunication(SubjectCom),"Album shared via communication is verified successfully" ,"Album shared via communication got failed");
								
    	}								 
    	catch(Exception e)							      
    	{						            
    		e.getCause();							             
    		Log.exception(e, driver);
							      
    	}							      
    	finally							      
    	{							             
    		Log.endTestCase();							             
    		RecordingClass.stopRecording();						             
    		driver.quit();
							      
    	}					
    }	
    
    
    
							
	/**TC_008(Album):Verify Manage album option for updating infomation
	 * @throws Exception 
	 * 
	 */ 
    @Test(priority=6, enabled=true, description=" Verify Manage album option for updating information ")
    public void verifyManageAlbumUodateInfo() throws Exception 
    {
    	try 
    	{
    		Log.testCaseInfo("TC_008(Album):Verify Manage album option for updating infomation");
    		
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
										    
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
		    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname),"Album is created successfully","Album cannot be created");
		    
		    File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));
	       	String filepath=fis.getAbsolutePath().toString();
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
       	    String tempfilepath=fis1.getAbsolutePath().toString();
            String filename=PropertyReader.getProperty("photoname");
								            
		    Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath),"Photo has been successfully uploaded to the album","Photo cannot be uploaded to the album");
									        
		    Log.assertThat(fnaalbumpage.manageAlbumUpdateInfo(),"image information  details are updated","image information are not updated");	    
									        
		    fnaalbumpage.deleteAlbum();
		    
    	}
		catch(Exception e)
    	{
			e.getCause();
			Log.exception(e, driver);
			
    	}
    	finally
    	{
    		Log.endTestCase();
            driver.quit();
            
    	}
								
  	}
    
    
    
    
	/**TC_009(Album):Method to verify manage option for delete image
	 * @throws Exception 
	 * 
	 */
	@Test(priority=7, enabled=true, description=" Method to verify manage option for delete image ")
	public void manageOptionDeleteImage() throws Exception 
	{				
		try 
		{
									
			Log.testCaseInfo(" TC_009(Album):Method to verify manage option for delete image ");
			
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
								    
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
								    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname),"Album is created successfully","Album cannot be created"); 
								    
		    File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));
	       	String filepath=fis.getAbsolutePath().toString();
	       	File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
       	    String tempfilepath=fis1.getAbsolutePath().toString();
            String filename=PropertyReader.getProperty("photoname");
						            
		    Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath),"Photo has been successfully uploaded to the album","Photo cannot be uploaded to the album");
								    
		    Log.assertThat(fnaalbumpage.deleteImageManageAlbumOption(),"Delete option for the uploaded file working fine","Delete of uploaded image not working");
								    
		    fnaalbumpage.deleteAlbum();
															
		}
		catch(Exception e)
		{
             e.getCause();
             Log.exception(e, driver);
							             
		}
        finally
        {
    		Log.endTestCase();
            driver.quit();
            
        }
							
	}
	
	
	
							
	/**TC_010(Album):Select a album from parent collection and verify it is navigated to correct album
	 * @throws Exception 
	 * 
	 */
	@Test(priority=8,enabled=true,description=" Select a album from parent collection and verify it is navigated to correct album ")
	public void navigatetoAlbumFromCollection() throws Exception 
	{				                 
		try 
		{
        	 Log.testCaseInfo("TC_010(Album):select a album from parent collection and verify it is navigated to correct album");
        	 
		     loginpage = new LoginPage(driver).get();
		     String uName=PropertyReader.getProperty("USERID");
		     String pWord=PropertyReader.getProperty("PASSFA");
		     fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		     fnahomepage.loginValidation();
					     			     
		     String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			 fnahomepage.selectcollection(collectionname);
			 fnahomextramodule=fnahomepage.clickdocuments();
			 fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			 fnaalbumpage.checkAnyAlbumExist();
		     String albumname=fnaalbumpage.Random_Albumname();
					     			     
		     Log.assertThat(fnaalbumpage.createAlbum(albumname),"Album is created successfully","Album cannot be created");
		     
		     File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));
	       	 String filepath=fis.getAbsolutePath().toString();
	         File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
       	     String tempfilepath=fis1.getAbsolutePath().toString();
             String filename=PropertyReader.getProperty("photoname");
					            
			 Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath),"Photo has been successfully uploaded to the album","Photo cannot be uploaded to the album");
             String multialbumname=fnaalbumpage.Random_MultiAlbumname();
             fnaalbumpage.addMultipleAlbum(multialbumname);
             
             File fis13=new  File(PropertyReader.getProperty("uploadphoto1"));
  		     String filepath1=fis13.getAbsolutePath().toString();
  		     String filename1=PropertyReader.getProperty("Filename1");
  		     
  		     Log.assertThat(fnaalbumpage.uploadPhoto(multialbumname,filepath1,filename1,tempfilepath),"Photo has been successfully uploaded to the album","Photo cannot be uploaded to the album");
					                     
  		     Log.assertThat(fnaalbumpage.navigateToAlbumFromCollection(albumname),"Selected album is navigated successfully","Album is not selected from collection and cannot be navigated");     
					                     
  		     fnaalbumpage.deleteAlbum();
  		     fnaalbumpage.deleteMultipleAlbum(multialbumname); 
					                 	
         }
    	 catch(Exception e)
		 {
    		 e.getCause();
             Log.exception(e, driver);
             
		 }
		 finally
         {
             Log.endTestCase();
             driver.quit();
								      
         }
							
	}
	
	
	
	
	/**TC_011(Album):Verify Maximised functionality of album photo
	 * @throws Exception 
	 * 
	 */
	@Test(priority=9, enabled=true, description=" Verify Maximised functionality of album photo ")
	public void verifyMaximumFunctionalityOfPhotos() throws Exception 
	{								
		try 
		{
			Log.testCaseInfo(" TC_011(Album):Verify Maximised functionality of album photo");
			
			loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
									    
	    	String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
									    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname)," Album is created successfully "," Album cannot be created "); 
		    
		    File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));
	       	String filepath=fis.getAbsolutePath().toString();
	       	
	        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
       	    String tempfilepath=fis1.getAbsolutePath().toString();
       	    
            String filename=PropertyReader.getProperty("photoname");	
            File fisscreenshot=new  File(PropertyReader.getProperty("screenshotlocation"));
            String screenshotpath=fisscreenshot.getAbsolutePath().toString();
       	    File Defaultfisscreenshot=new  File(PropertyReader.getProperty("DefaultScrenshotpath"));
       	    String defaultscreenshotpath=Defaultfisscreenshot.getAbsolutePath().toString();
							       	    
		    Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath)," Photo has been successfully uploaded to the album "," Photo cannot be uploaded to the album ");
									    
		    Log.assertThat(fnaalbumpage.maximizePhoto(screenshotpath,defaultscreenshotpath)," Photo has been maximized successfully "," Photo has not been maximized ");    ;
									    
		    fnaalbumpage.minimizePhoto(screenshotpath);
		    fnaalbumpage.deleteMultipleAlbum(albumname); 
									
		}	
		catch(Exception e)
		{
         	e.getCause();
         	Log.exception(e, driver);
         	
		}				      
		finally
		{
             Log.endTestCase();
             driver.quit();
             
		}
	}
	
	
	
	
	/**TC_012(Album):Verify Album menu would not be displayed without entering a Collection
	 * @throws Exception 
	 * 
	 */
	@Test(priority=10, enabled=true, description=" Verify Album menu would not be displayed without entering a Collection. ")
	public void verifyAlbumMenuDisplayWithoutEnteringCollection() throws Exception 
	{								
		try 
		{
			Log.testCaseInfo(" TC_012(Album):Verify Album menu would not be displayed without entering a Collection ");
			
			loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
			
		    Log.assertThat(fnahomepage.enterCollectionList()," Collection page landing successfull "," Collection page landing failed!!! ", driver);
		    
		    Log.assertThat(fnahomepage.validateAlbumWithoutSelectCollection()," Album is not displayed without selecting collection "," Album is displayed without selecting collection!!! ", driver);
		    								
		}	
		catch(Exception e)
		{
         	e.getCause();
         	Log.exception(e, driver);
         	
		}				      
		finally
		{
             Log.endTestCase();
             driver.quit();
             
		}
	}
	
	
	
	/**TC_013(Album):Verify Album menu would be displayed after entering a Collection.
	 * @throws Exception 
	 * 
	 */
	@Test(priority=11, enabled=true, description=" Verify Album menu would be displayed after entering a Collection. ")
	public void verifyAlbumMenuDisplayAfterSelectingCollection() throws Exception 
	{								
		try 
		{
			Log.testCaseInfo(" TC_013(Album):Verify Album menu would be displayed after entering a Collection. ");
			
			loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
			
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			
		    Log.assertThat(fnahomepage.validateAlbumAfterEnteringCollection()," Album is displayed after entering a collection "," Album is not displayed after entering a collection!!! ", driver);
		    								
		}	
		catch(Exception e)
		{
         	e.getCause();
         	Log.exception(e, driver);
         	
		}				      
		finally
		{
             Log.endTestCase();
             driver.quit();
             
		}
	}
	
	
	
   /**TC_07(Album):Verify share album with send via email option
    * @throws Exception 
    * 
    */
	@Test(priority=12, enabled=true, description=" Verify share album with send via email option ")   
	public void verifyShareAlbumSendViaEmailOption() throws Exception 
	{								
		try 
		{
			Log.testCaseInfo(" TC_007(Album):Verify share album with send via email option ");
			
		    loginpage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
		    fnahomepage.loginValidation();
											    
		    String collectionname=PropertyReader.getProperty("ALBUMTESTCOLLECTION");
			fnahomepage.selectcollection(collectionname);
			fnahomextramodule=fnahomepage.clickdocuments();
			fnaalbumpage=fnahomextramodule.clickAlbumIcon();
			fnaalbumpage.checkAnyAlbumExist();
		    String albumname=fnaalbumpage.Random_Albumname();
											    
		    Log.assertThat(fnaalbumpage.createAlbum(albumname),"Album is created successfully","Album cannot be created");
											    
		    File fis=new  File(PropertyReader.getProperty("uploadphotoforalbum"));
	       	String filepath=fis.getAbsolutePath().toString();
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
            String tempfilepath=fis1.getAbsolutePath().toString();
            String filename=PropertyReader.getProperty("photoname");
									            
			Log.assertThat(fnaalbumpage.uploadPhoto(albumname,filepath,filename,tempfilepath),"Photo has been successfully uploaded to the album","Photo cannot be uploaded to the album");
										   
			Log.assertThat(fnaalbumpage.sendviaEmailOption(),"Share via email option is working fine and outlook window appears on clicking the button","Share via email button not working");     
										   
			fnaalbumpage.clickClose();   
			fnaalbumpage.deleteMultipleAlbum(albumname);
										
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
									      
		}
		finally
		{
         	Log.endTestCase();
            driver.quit();
            
		}
										
	}	
	
}
						

	
				
