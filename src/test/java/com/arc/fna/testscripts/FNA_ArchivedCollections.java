package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.Options;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.ArchivedCollectionsPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_ArchivedCollections 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	ArchivedCollectionsPage archivedCollectionsPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	
	/** TC_001 (Archived Collections): Verify new Project and go to edit button then see export option present in settings.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Archived Collections): Verify new Project and go to edit button then see export option present in settings")
	public void verifyaddteam_member_Task () throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (Archived Collections): Verify new Project and go to edit button then see export option present in settings");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			archivedCollectionsPage = new ArchivedCollectionsPage(driver).get();
			Log.assertThat(archivedCollectionsPage.Verify_Archive_Export(CollectionName), "Archive Export displayed Successfull", "Archive Export displayed UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Archived Collections): Verify Active Archive button in collection setting.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Archived Collections): Verify Active Archive button in collection setting.")
	public void verifyActive_Archive () throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_002 (Archived Collections): Verify Active Archive button in collection setting.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			archivedCollectionsPage = new ArchivedCollectionsPage(driver).get();
			Log.assertThat(archivedCollectionsPage.Verify_Active_Archivebutton(CollectionName), "Active Archive button Successfull", "Active Archive button UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Archived Collections): Verify Archived Edit button.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003  (Archived Collections): Verify Archived Edit button.")
	public void verifyArchived_Edit_Download () throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_003  (Archived Collections): Verify Archived Edit button.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			archivedCollectionsPage = new ArchivedCollectionsPage(driver).get();
			Log.assertThat(archivedCollectionsPage.Verify_Edit_Archivedcollections(CollectionName), "Edit Archived collections Successfull", "Edit Archived collections UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	

	/** TC_004 (Archived Collections): Verify Archived Download.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004  (Archived Collections): Verify Archived Download.")
	public void verifyArchived_Download () throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004  (Archived Collections): Verify Archived Download.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			archivedCollectionsPage = new ArchivedCollectionsPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(archivedCollectionsPage.Verify_Download_Archivedcollections(CollectionName,DownloadPath), "Download Archived collections Successfull", "Download Archived collections UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
}