package com.arc.fna.testscripts;

import java.awt.List;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.AdvancedSearchPage;
import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FNA_BatchAttributePage;
import com.arc.fna.pages.FNA_HomeExtraModulePAge;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.RecordingClass;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_BatchAttribute 
{
	 
		static WebDriver driver;
		LoginPage loginpage;
	    FnaHomePage fnahomepage;
	    FNAAlbumPage fnaalbumpage;
	    FNA_HomeExtraModulePAge fnahomextramodule;
	    FNA_BatchAttributePage fnabatchattribute;
	    //......sekhar...........
	    LoginPage loginPage;
		FnaHomePage fnaHomePage;
		
	    @Parameters("browser")
	    @BeforeMethod(groups="sekhar_test")
	    public WebDriver beforeTest(String browser) 
	    {
			
	    	if(browser.equalsIgnoreCase("firefox")) 
	    	{
	    		File dest = new File("./drivers/win/geckodriver.exe");
	    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	    		driver = new FirefoxDriver();
	    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	    	else if (browser.equalsIgnoreCase("chrome")) 
	    	{ 
	    		File dest = new File("./drivers/win/chromedriver.exe");
	    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
	            Map<String, Object> prefs = new HashMap<String, Object>();
	            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	            ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--start-maximized");
	            options.setExperimentalOption("prefs", prefs);
	            driver = new ChromeDriver( options );
	    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	        } 
	    	else if (browser.equalsIgnoreCase("safari"))
	    	{
				System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
				driver = new SafariDriver();
				driver.get(PropertyReader.getProperty("SkysiteProdURL"));
				
	    	}
	   
	    	return driver;
	}
	   
	    
	    
	    /**TC_001(Batch attributes):Verify and update general properties of the file
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=0, enabled=true, description= "TC_001(Batch attributes):Verify and update general properties of the file")
	    public void modifyattributesVendorPName() throws Exception 
	    {
	    	  try 
	    	  {
				    Log.testCaseInfo("TC_001(Batch attributes):Verify and update general properties of the file");
				    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					
					String filename =fnabatchattribute.Random_filename();
				    String  title=fnabatchattribute.Random_title();
				    String description=fnabatchattribute.Random_description();
				  	String Foldername= PropertyReader.getProperty("Foldername");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
					Log.assertThat(fnabatchattribute.modifyGeneralAttributes(filename,title,description)," File details are updated and now visible under web table "," File details are not  updated and not visible under web table ", driver);
				   
			  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		             
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		             
		      }
			 
			 
		}
	    
	    
	    /**TC_002(Batch Attrbibutes):Update and verify custom properties like Vendor and Project no of a file
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=1, enabled=true, description="Update and verify custom properties like Vendor and Project no of a file")
	    public void updateAndVerifyVendorPNo() throws Exception 
	    {
	    	  try 
	    	  {
				    Log.testCaseInfo("TC_002(Batch Attrbibutes):Update and verify custom properties like Vendor and Project no of a file");
				    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("FolderPathFileForSearch"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnahomepage.UploadFileForDeletion(filepath,tempfilepath);
		       	    
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					String Projectno=fnabatchattribute.Random_Projectno();
					
	     		    Log.assertThat(fnabatchattribute.customPropertiesVendorAndPNo(Projectno),"Project no and vendor name has been updated","Project no and vendor has not been updated");
	     		    
	     		    fnabatchattribute.deleteFolderFromCollection();
	    					   
			  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
			 
	   }
	    
	    
	    
	    /**TC_003(Batch Attrbibutes):Update and verify custom properties like Document type,invoice and Project name of a file
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=2, enabled=true, description="Update and verify custom properties like Document type,invoice and Project name of a file")
	    public void updateAndVerifyDtypeAndInvoiceAndProName() throws Exception 
	    {
	    	  try 
	    	  {
				    Log.testCaseInfo("TC_003(Batch Attrbibutes):Update and verify custom properties like Document type,invoice and Project name of a file");
				    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("FolderPathFileForSearch"));
    		       	String filepath=fis.getAbsolutePath().toString();
    		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnahomepage.UploadFileForDeletion(filepath,tempfilepath);
					
		       	    fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
				    String projectname=fnabatchattribute.Random_Projectname();
				    String invoice=fnabatchattribute.Random_Invoice();
				    
				    Log.assertThat(fnabatchattribute.modifyDocumentTypeInvoiceAndPName(projectname,invoice),"Document type,invoice and project name are updated and visible","Document type,invoice and project name are not updated and visible");
				    
				    fnabatchattribute.deleteFolderFromCollection();
			    	   
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	     }
	    
	    
	    
	    /**TC_004(Batch_Attribute):Verify Search with document type 
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=3, enabled=true, description="Verify Search with document type")
	    public void verifySearchDocumentType() throws Exception 
	    {
	    	  try 
	    	  {
	    		  	
	    		  	Log.testCaseInfo("TC_004(Batch_Attribute):Verify Search with document type ");
	    		  
	    		  	loginpage = new LoginPage(driver).get();
	    		  	String uName=PropertyReader.getProperty("USERID");
	    		  	String pWord=PropertyReader.getProperty("PASSFA");
	    		  	fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
	    		  	fnahomepage.loginValidation();
				  
	    		  	String collectionname=PropertyReader.getProperty("Collectioname");
	    		  	fnahomepage.selectcollection(collectionname);
	    		  	fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.DocumentTypeSearch()," Document Search with document type attribute is successfull","Document cannot be searched with document type attribute");
		        			
			  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	  
	  }
	    
	    
	    
	    /**TC_005(Batch_Attribute):Verify Search with invoice
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=4, enabled=true, description="Verify Search with invoice")
	    public void verifySearchInvoice() throws Exception 
	    {
	    	  try 
	    	  {
	    		  
	    		  	Log.testCaseInfo("TC_005(Batch_Attribute):Verify Search with invoice");
	    		  	
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchInvoice(),"Search with invoice is successfull", "Search with invoice is not  successfull");
		            
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
	    	  
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	    
	    
	    /**TC_006(Batch_Attribute):Verify Search with Project number
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=5, enabled=true, description="Verify Search with Project number")
	    public void verifySearchwithProjectNumber() throws Exception 
	    {
	    	  try 
	    	  {
	    		    Log.testCaseInfo("TC_006(Batch_Attribute):Verify Search with Project number");
	    		    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithProjectNumber(),"Search with project number is successfull", "Search with project number is not  successfull");
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
	    	  
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	 }
	    
	    
	    /**TC_007(Batch_Attribute):Verify Search with Vendor
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=6, enabled=true, description="Verify Search with Vendor")
	    public void verifySearchwithVendor() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_007(Batch_Attribute):Verify Search with Vendor");
	    		  	
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithVendor(),"Search with vendor is successfull", "Search with invoice is not  successfull");
		            
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	    
	    
	    
	    /**TC_008(Batch_Attribute):Verify Search with project name
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=7, enabled=true, description="Verify Search with project name")
	    public void verifySearchwithProjectName() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_008(Batch_Attribute):Verify Search with project name");
	    		    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithProjectName(),"Search with  project name is successfull", "Search with project name is not working");
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	    
	    
	    
	    /**TC_009(Batch_Attribute):Verify Search with Keyword
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=8, enabled=true, description="Verify Search with Keyword")
	    public void verifySearchwithKeyword() throws Exception 
	    {
	    	  try 
	    	  {
	    		    Log.testCaseInfo("TC_009(Batch_Attribute):Verify Search with Keyword");
				    
	    		    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Keywordsearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithKeyword(),"Search with Keyword is successfull", "Search with Keyword is not working");
		            
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	
	    
	    
	    /**TC_0010(Batch_Attribute):Verify update default attribute to file while upload and search with that attribute
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=9, enabled=true, description="Verify add attribute to file while upload and search with that attribute")
	    public void verifyAddAttributetoFileAndSearch() throws Exception 
	    {
	    	  try 
	    	  {
	    		    Log.testCaseInfo("TC_0010(Batch_Attribute):Verify add attribute to file while upload and search with that attribute");
				    
	    		    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("Filepathforupload"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String invoice=fnabatchattribute.Random_Invoice();
					fnabatchattribute.UploadFileAddAttribute(filepath, tempfilepath,invoice);
					
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					
			        Log.assertThat(fnabatchattribute.searchInvoiceonupdatedfile(invoice),"Search with updated invoice is successfull", "Search with updated invoice is not successfull");
			        
			        fnabatchattribute.deleteFolderFromCollection();
			        
	    	  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }   
	    
	    
	    
	    
	    /**TC_0011(Batch_Attribute):Verify update custom attribute to file while upload and search with that attribute
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=10, enabled=true, description="Verify update custom attribute to file while upload and search with that attribute")
	    public void verifyAddCustomAttributetoFileAndSearch() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_0011(Batch_Attribute):Verify update custom attribute to file while upload and search with that attribute");
				    
	    		  	loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname2");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("Filepathforupload"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.UploadFileAddCustomAttribute(filepath, tempfilepath);
					
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					
			        Log.assertThat(fnabatchattribute.searchWithVendorwhenaddingattribute(),"Search with vendor is successfull", "Search with vendor is not successfull");
			        
			        fnabatchattribute.deleteFolderFromCollection();
	    	  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    } 
	    
	    
	    
	    
	    /**TC_0012(Batch_Attribute):Verify search with All attribute & And operator
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=11, enabled=true, description="Verify search with All attribute & And operator")
	    public void verifySearchWithAllAttributeAndOperator() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_0012(Batch_Attribute):Verify search with All attribute & And operator");
				    
	    		  	loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearchallattribute");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
		           
					Log.assertThat(fnabatchattribute.searchWithAllAttributeAndOperator()," Document Search with All attribute using and & operator working successfully","Document Search with All attribute using & operator not  working ");
		          	
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		     
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    
	    } 	
	    
	    
	    
	    
	    /**TC_0013(Batch Attrbibutes):Update attribute then modify then search with that attribute
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=12, enabled=true, description="Update attribute then modify then search with that attribute")
	    public void VerifyadditionofAttributeandmodifyingThenSearching() throws Exception 
	    {
	    	  try 
	    	  {
				   
	    		  	Log.testCaseInfo("TC_0013(Batch Attrbibutes):Update attribute then modify then search with that attribute");
	    		  	
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
					File fis=new  File(PropertyReader.getProperty("FolderPathFileForSearch"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnahomepage.UploadFileForDeletion(filepath,tempfilepath);
		       	    
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					String Projectno=fnabatchattribute.Random_Projectno();
					
	     		    Log.assertThat(fnabatchattribute.customPropertiesVendorAndPNo(Projectno),"Project no and vendor name has been updated","Project no and vendor has not been updated");
	     			
	     		    fnabatchattribute.selectFileandModifyAttribute(FolderName);	     			
	     		    String updatedprojectno=fnabatchattribute.Random_UpdateProjectno();
	     			String vendornoupdate=PropertyReader.getProperty("UpdatedVendor2");	     			
	     			fnabatchattribute.modifyAddedAttributes(updatedprojectno,vendornoupdate);   
	     			
	     			fnabatchattribute.selectFileandModifyAttribute(FolderName);
	     			fnabatchattribute.searchUpdatedValue(updatedprojectno,vendornoupdate);
	     		    fnabatchattribute.deleteFolderFromCollection();     		    
	    	
	    	  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }    
	    }
	    
//-----------------------------sekhar--------------------------------
	    
	    /** TC_014 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign vendor,project number and save.
	     * Scripted By: Sekhar
	     * @throws Exception
	    */
	    @Test(priority = 13, enabled = true, description = "TC_014 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign vendor,project number and save.")
	    public void verifyModify_Attribute_ProjectNumber_Vendor() throws Exception
	    {
	    	try
	    	{
	    		Log.testCaseInfo("TC_014 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign vendor,project number and save.");
	    		loginPage = new LoginPage(driver).get();                
	    		String uName = PropertyReader.getProperty("CollectionAttributename");
	    		String pWord = PropertyReader.getProperty("Password");			
	    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
	    		fnaHomePage.loginValidation();
	    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection25");			
	 			fnaHomePage.selectcollection(Collection_Name);	
	 			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
	 			fnaHomePage.Select_Folder(FolderName);	
	 			driver.switchTo().defaultContent();
	 			fnabatchattribute = new FNA_BatchAttributePage(driver).get();	 			
	 			Log.assertThat(fnabatchattribute.Modify_Attribute_ProjectNumber_Vendor(), "Project number and Vendor modified Successfully", "Project number and Vendor modified UnSuccessfully", driver);
	    	}
	    	catch(Exception e)
	    	{
	    		AnaylizeLog.analyzeLog(driver);
	           	e.getCause();
	           	Log.exception(e, driver);
	    	}
	    	finally
	    	{
	    		Log.endTestCase();
	           	driver.quit();
	    	}	
	    }     
	    
	    /** TC_015 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign Document name,invoice and vendor the save.
	     * Scripted By: Sekhar
	     * @throws Exception
	    */
	    @Test(priority = 14, enabled = true, description = "TC_015 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign Document name,invoice and vendor the save.")
	    public void verifyModify_Attribute_Documentname_Invoice_Vendor() throws Exception
	    {
	    	try
	    	{
	    		Log.testCaseInfo("TC_015 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign Document name,invoice and vendor the save.");
	    		loginPage = new LoginPage(driver).get();                
	    		String uName = PropertyReader.getProperty("CollectionAttributename");
	    		String pWord = PropertyReader.getProperty("Password");			
	    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
	    		fnaHomePage.loginValidation();
	    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection25");			
	 			fnaHomePage.selectcollection(Collection_Name);	
	 			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
	 			fnaHomePage.Select_Folder(FolderName);	
	 			driver.switchTo().defaultContent();
	 			fnabatchattribute = new FNA_BatchAttributePage(driver).get();	
	 			String Invoice = PropertyReader.getProperty("AttributeInvoice");
	 			String Temp_Invoice = PropertyReader.getProperty("AttributeTempInvoice");
	 			Log.assertThat(fnabatchattribute.Modify_Attribute_Documentname_Invoice_Vendor(Invoice,Temp_Invoice), "Document name and Invoice and Vendor modified Successfully", "Document name and Invoice and Vendor modified UnSuccessfully", driver);
	    	}
	    	catch(Exception e)
	    	{
	    		AnaylizeLog.analyzeLog(driver);
	           	e.getCause();
	           	Log.exception(e, driver);
	    	}
	    	finally
	    	{
	    		Log.endTestCase();
	           	driver.quit();
	    	}	
	    } 

	    /** TC_016 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign project name,Boolean, date.
	     * Scripted By: Sekhar
	     * @throws Exception
	    */
	    @Test(priority = 15, enabled = true, description = "TC_016 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign project name,Boolean, date.")
	    public void verifyModify_Attribute_Projectname_Boolean_Date() throws Exception
	    {
	    	try
	    	{
	    		Log.testCaseInfo("TC_016 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign project name,Boolean, date.");
	    		loginPage = new LoginPage(driver).get();                
	    		String uName = PropertyReader.getProperty("CollectionAttributename");
	    		String pWord = PropertyReader.getProperty("Password");			
	    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
	    		fnaHomePage.loginValidation();
	    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection25");			
	 			fnaHomePage.selectcollection(Collection_Name);	
	 			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
	 			fnaHomePage.Select_Folder(FolderName);	
	 			driver.switchTo().defaultContent();
	 			fnabatchattribute = new FNA_BatchAttributePage(driver).get();	
	 			String ProjectName = PropertyReader.getProperty("AttributeProjectname");
	 			String Temp_ProjectName = PropertyReader.getProperty("AttributeTempProjectname");
	 			Log.assertThat(fnabatchattribute.Modify_Attribute_Projectname_Boolean_Date(ProjectName,Temp_ProjectName), "Project Name and boolean and Date modified Successfully", "Project Name and boolean and Date modified UnSuccessfully", driver);
	    	}
	    	catch(Exception e)
	    	{
	    		AnaylizeLog.analyzeLog(driver);
	           	e.getCause();
	           	Log.exception(e, driver);
	    	}
	    	finally
	    	{
	    		Log.endTestCase();
	           	driver.quit();
	    	}	
	    } 
	    
	    /** TC_017 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign date,vendor,project number.
	     * Scripted By: Sekhar
	     * @throws Exception
	    */
	    @Test(priority = 16, enabled = true, description = "TC_017 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign date,vendor,project number.")
	    public void verifyModify_Attribute_Date_Vendor_ProjectNumber() throws Exception
	    {
	    	try
	    	{
	    		Log.testCaseInfo("TC_017 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign date,vendor,project number.");
	    		loginPage = new LoginPage(driver).get();                
	    		String uName = PropertyReader.getProperty("CollectionAttributename");
	    		String pWord = PropertyReader.getProperty("Password");			
	    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
	    		fnaHomePage.loginValidation();
	    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection25");			
	 			fnaHomePage.selectcollection(Collection_Name);	
	 			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
	 			fnaHomePage.Select_Folder(FolderName);	
	 			driver.switchTo().defaultContent();
	 			fnabatchattribute = new FNA_BatchAttributePage(driver).get();	 			
	 			Log.assertThat(fnabatchattribute.Modify_Attribute_Date_Vendor_ProjectNumber(), "Project Number and Vendor and Date modified Successfully", "Project Number and Vendor and Date modified UnSuccessfully", driver);
	    	}
	    	catch(Exception e)
	    	{
	    		AnaylizeLog.analyzeLog(driver);
	           	e.getCause();
	           	Log.exception(e, driver);
	    	}
	    	finally
	    	{
	    		Log.endTestCase();
	           	driver.quit();
	    	}	
	    } 

	    /** TC_018 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign all the attributes and save then copy the same files to other folder check the attributes.
	     * Scripted By: Sekhar
	     * @throws Exception
	    */
	    @Test(priority = 17, enabled = true, description = "TC_018 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign all the attributes and save then copy the same files to other folder check the attributes.")
	    public void verifyModify_All_Attributr_Copyfiles_OtherFolder() throws Exception
	    {
	    	try
	    	{
	    		Log.testCaseInfo("TC_018 (Batch Attributes): Verify Select all the files then open in the modify attribute window assign all the attributes and save then copy the same files to other folder check the attributes.");
	    		loginPage = new LoginPage(driver).get();                
	    		String uName = PropertyReader.getProperty("CollectionAttributename");
	    		String pWord = PropertyReader.getProperty("Password");			
	    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
	    		fnaHomePage.loginValidation();
	    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection26");			
	 			fnaHomePage.selectcollection(Collection_Name);
	 			String FolderName1= fnaHomePage.Random_Foldername();		
				Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
				driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[2]")).click();
				Thread.sleep(10000);	 			
	 			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
	 			fnaHomePage.Select_Folder(FolderName);	
	 			driver.switchTo().defaultContent();
	 			fnabatchattribute = new FNA_BatchAttributePage(driver).get();
	 			String Invoice = PropertyReader.getProperty("AttributeInvoice");
	 			String Temp_Invoice = PropertyReader.getProperty("AttributeTempInvoice");	 			
	 			String ProjectName = PropertyReader.getProperty("AttributeProjectname");
	 			String Temp_ProjectName = PropertyReader.getProperty("AttributeTempProjectname");	 			
	 			String Amount = PropertyReader.getProperty("AttributeAmount");
	 			String Temp_Amount = PropertyReader.getProperty("AttributeTempAmount");	 			
	 			String Cheque_Numb = PropertyReader.getProperty("AttributeChequeNumb");
	 			String Temp_Cheque_Numb = PropertyReader.getProperty("AttributeTempChequeNumb");	 			
	 			Log.assertThat(fnabatchattribute.Modify_All_Attribute_Required_Validations(Invoice,Temp_Invoice,
	 					ProjectName,Temp_ProjectName,Amount,Temp_Amount,Cheque_Numb,Temp_Cheque_Numb), "Modified all Attributes Successfully", "Modified all Attributes UnSuccessfully", driver);
	 			Log.assertThat(fnabatchattribute.CopyFiles_Within_Collection_Attributes(FolderName1), "Modified Attributes Copy files different folder Validation Successfully", "Modified Attributes Copy files different folder Validation UnSuccessfully", driver);
	    	
	    	}
	    	catch(Exception e)
	    	{
	    		AnaylizeLog.analyzeLog(driver);
	           	e.getCause();
	           	Log.exception(e, driver);
	    	}
	    	finally
	    	{
	    		Log.endTestCase();
	           	driver.quit();
	    	}	
	    } 
	    
	/** TC_019 (Batch Attributes): Verify Navigate to the page select all the files then click on the modify attributes button assign all the attributes then check.
	 * Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 18, enabled = true, description = "TC_019 (Batch Attributes): Verify Navigate to the page select all the files then click on the modify attributes button assign all the attributes then check.")
	public void verifyNavigate_Modify_All_Attribute() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_019 (Batch Attributes): Verify Navigate to the page select all the files then click on the modify attributes button assign all the attributes then check.");
			loginPage = new LoginPage(driver).get();                
			String uName = PropertyReader.getProperty("CollectionAttributename");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection27");			
			fnaHomePage.selectcollection(Collection_Name);	 			 			
			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			fnabatchattribute = new FNA_BatchAttributePage(driver).get();
			String Invoice = PropertyReader.getProperty("AttributeInvoice");
			String Temp_Invoice = PropertyReader.getProperty("AttributeTempInvoice");	 			
			String ProjectName = PropertyReader.getProperty("AttributeProjectname");
			String Temp_ProjectName = PropertyReader.getProperty("AttributeTempProjectname");	 			
			String Amount = PropertyReader.getProperty("AttributeAmount");
			String Temp_Amount = PropertyReader.getProperty("AttributeTempAmount");	 			
			String Cheque_Numb = PropertyReader.getProperty("AttributeChequeNumb");
			String Temp_Cheque_Numb = PropertyReader.getProperty("AttributeTempChequeNumb");	 			
			Log.assertThat(fnabatchattribute.Navigate_Modify_All_Attribute(Invoice,Temp_Invoice,
					ProjectName,Temp_ProjectName,Amount,Temp_Amount,Cheque_Numb,Temp_Cheque_Numb), "Navigate Modified all Attributes Successfully", "Navigate Modified all Attributes UnSuccessfully", driver);
		}	
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
	    }	
	} 
	  
  /** TC_020 (Batch Attributes): Verify move files the one folder to other folder after modified attributes.
      * Scripted By: Sekhar
      * @throws Exception
    */
    @Test(priority = 19, enabled = true, description = "TC_020 (Batch Attributes): Verify move files the one folder to other folder after modified attributes.")
    public void verifyMoveFiles_Within_Collection_Modify_Attributes() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_020 (Batch Attributes): Verify move files the one folder to other folder after modified attributes.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionAttributename");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);		
 			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
            driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[2]")).click();
			Thread.sleep(10000);			
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
 			driver.switchTo().defaultContent();
 			fnabatchattribute = new FNA_BatchAttributePage(driver).get();
 			Log.assertThat(fnabatchattribute.MoveFiles_Within_Collection_Modify_Attributes(FolderName,FolderName1), "Move files one floder to other folder Successfully", "Move files one floder to other folder UnSuccessfully", driver);
 			String Invoice = PropertyReader.getProperty("AttributeInvoice");
 			String ProjectName = PropertyReader.getProperty("AttributeProjectname");
 			String Amount = PropertyReader.getProperty("AttributeAmount");
 			String Cheque_Numb = PropertyReader.getProperty("AttributeChequeNumb");
 			Log.assertThat(fnabatchattribute.Modify_All_Attribute_Move_Folder(Invoice,ProjectName,Amount,Cheque_Numb), "Move files Modified all Attributes Successfully", "Move files Modified all Attributes UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    

}