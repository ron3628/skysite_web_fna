package com.arc.fna.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FNA_HomeExtraModulePAge;
import com.arc.fna.pages.FNA_ViewerPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.RecordingClass;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

import atu.testrecorder.exceptions.ATUTestRecorderException;

@Listeners(EmailReport.class)
public class FNA_Viewer extends RecordingClass {

	static WebDriver driver;
	LoginPage loginpage;
	FnaHomePage fnahomepage;
	FNAAlbumPage fnaalbumpage;
	FNA_HomeExtraModulePAge fnahomextramodule;
	FNA_ViewerPage fna_viewerpage;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) throws ATUTestRecorderException {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} else if (browser.equalsIgnoreCase("chrome")) {

			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));

		} else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "true"); // To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	/**
	 * TC_0029(Viewer):Verify whether Line annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 0, enabled = true, description = "Verify whether Line annotations getting saved properly.")
	public void verifyLineAnnotation() throws Exception {
		try {

			Log.testCaseInfo("TC_0029(Viewer):Verify whether Line annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			String foldername = PropertyReader.getProperty("selectFolder_File");
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawLineAnnotation(markupname, filepath1),
					"Drawn annotation is saved successfully", "Drawn annotation is not saved");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_001(Viewer):Verify whether Square annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 1, enabled = true, description = "Verify whether Square annotations getting saved properly.")
	public void verifySquareAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_001(Viewer):Verify whether Square annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawSquareAnnotation(markupname, filepath1),
				"Square annotation drawn and saved successfully", "Square annotation cannot be drawn");
			//fna_viewerpage.drawSquareAnnotation(markupname, filepath1);
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	
	/**
	 * TC_0031(Viewer):Verify whether Text annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 2, enabled = true, description = "TC_0031(Viewer):Verify whether Text annotations getting saved properly.")
	public void verifyTextAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_0031(Viewer):Verify whether Text annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawTextAnnotation(markupname, filepath1),
					"Text annotation drawn and saved successfully", "Text annotation cannot be drawn");
            
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_004(Viewer):Verify whether FreeHand annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 3, enabled = true, description = "TC_004(Viewer):Verify whether FreeHand annotations getting saved properly.")
	public void verifyFreeHandAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_004(Viewer):Verify whether FreeHand annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawFreeHandAnnotation(markupname, filepath1),
					"Free hand annotation drawn and saved successfully", "Free hand annotation cannot be drawn");

			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_002(Viewer):Verify whether Arrow annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 4, enabled = true, description = "TC_002(Viewer):Verify whether  Arrow annotations getting saved properly.")
	public void verifyArrowAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_002(Viewer):Verify whether  Arrow annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawArrowAnnotation(markupname, filepath1),
					"Arrow annotation drawn and saved successfully", "Arrow annotation cannot be drawn");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_0027(Viewer):Verify whether Circle annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 5, enabled = true, description = "TC_027(Viewer):Verify whether Circle annotations getting saved properly.")
	public void verifyCircleAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_027(Viewer):Verify whether Circle annotations getting saved properly.");
			RecordingClass.recordTestCase("Verify Circle Annotation");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawCircleAnnotation(markupname, filepath1),
					"Circle annotation drawn and saved successfully", "Circle annotation cannot be drawn");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
	
		}

	}

	/**
	 * TC_0028(Viewer):Verify whether Cloud annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 6, enabled = true, description = "TC_028(Viewer):Verify whether Cloud annotations getting saved properly.")
	public void verifyCloudAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_028(Viewer):Verify whether Cloud annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawCloudAnnotation(markupname, filepath1),
					"Cloud annotation drawn and saved successfully", "Cloud annotation cannot be drawn");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_003(Viewer):Verify whether Tax Callout annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 7, enabled = true, description = "TC_003(Viewer):Verify Callout annotations getting saved properly.")
	public void verifytextCallOut() throws Exception {
		try {
			Log.testCaseInfo("TC_003(Viewer):Verify Callout annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawTextCallOutAnnotation(markupname, filepath1),
					"Text Call Out annotation drawn and saved successfully",
					"Text Call Out annotation cannot be drawn and saved ");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	
	/**
	 * TC_032(Viewer):Verify whether Text Note annotations getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 8, enabled = true, description = "TC_032(Viewer):Verify whether Text Note annotations getting saved properly.")
	public void verifytextNote() throws Exception {
		try {
			Log.testCaseInfo("TC_032(Viewer):Verify whether Text Note annotations getting saved properly.");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");
			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawTextNoteAnnotation(markupname, filepath1),
					"Text Note annotation drawn and saved successfully",
					"Text Note annotation cannot be drawn and saved ");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_030(Viewer):Verify whether HighLighterAnnotation annotations getting saved
	 * properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 9, enabled = true, description = "TC_030(Viewer):Verify whether HighLighterAnnotation annotations getting saved properly.")
	public void verifyHighLighterAnnotation() throws Exception {
		try {
			Log.testCaseInfo("TC_030(Viewer):Verify whether HighLighterAnnotation annotations getting saved properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("selectFolder_File");
			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawHighLighterAnnotation(markupname, filepath1),
					"High Lighter annotation drawn and saved successfully",
					"High Lighter annotation cannot be drawn and saved");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_008(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * same folder different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */

	@Test(priority = 10, enabled = true, description = "TC_008(Viewer):Verify whether Square Hyperlink getting saved properly within same folder different file and working properly.")
	public void verifySquareHyperLinkSameFolderDiffFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_008(Viewer):Verify whether Square Hyperlink getting saved properly within same folder different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLink(markupname, filepath1),
				//	"Square hyper link drawn and save successfully", "Square hyperlink caanot be drawn and saved");
			fna_viewerpage.drawAndSaveSquareHyperLink(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkSameFolderDiffFile(markupname, filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for same folder and diff file",
					"Square hyper link is not working for same folder and diff fileas expected");
			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_009(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */

	@Test(priority = 11, enabled = true, description = "TC_009(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder different file and working properly.")
	public void verifySquareHyperLinkDiffFolderDiffFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_009(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname3");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffile(markupname, filepath1),
					//"Square hyper link drawn and save successfully", "Square hyperlink caanot be drawn and saved");
			fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffile(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderDiffFile(markupname, filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for different folder and different file",
					"Square hyper link is not working for for different folder and different file as expected");
			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_010(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder same file with diff version and working properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 12, enabled = true, description = "Verify whether Square Hyperlink getting saved properly within diff folder same file with diff version and working properly.")
	public void verifySquareHyperLinkdiffFolderSameFileDiffVersion() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_010(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder same file with diff version and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(
				//	fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderSamefileDiffVersion(markupname, filepath1),
				//	"Square hyper link drawn and save successfully", "Square hyperlink caanot be drawn and saved");
			fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderSamefileDiffVersion(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderSameFile(markupname, filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for diff folder and same file with diff version",
					"Square hyper link is not working for diff folder and same file with diff version as expected");
			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_033(Viewer):Verify whether Circle Hyperlink getting saved properly within
	 * same folder different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 13, enabled = true, description = "TC_033(Viewer):Verify whether Circle Hyperlink getting saved properly within same folder different file and working properly.")
	public void verifyCircleHyperLinkSameFolderDiffFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_033(Viewer):Verify whether Circle Hyperlink getting saved properly within same folder different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname3");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
		//	Log.assertThat(fna_viewerpage.drawCircleHyperLink(markupname, filepath1),
					//"Circle hyper link drawn and save successfully", "Circle hyperlink caanot be drawn and saved");
			fna_viewerpage.drawCircleHyperLink(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalityCircleHyperLinkSameFolderDiffFile(markupname, filepath1),
					"Linked file is opene in new tab and Circle hyperlink working properly for same folder and diff file ",
					"Circle hyperlink not working properly for same folder and diff file");
			fna_viewerpage.clickDefaultFileForCircleHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_034(Viewer):Verify whether Cloud Hyperlink getting saved properly within
	 * same folder different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 14, enabled = true, description = "TC_034(Viewer):Verify whether Cloud Hyperlink getting saved properly within same folder different file and working properly.")
	public void verifyCloudHyperLinkSameFolderDiffFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_034(Viewer):Verify whether Cloud Hyperlink getting saved properly within same folder different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawCloudHyperLink(markupname, filepath1),
				//	"Circle hyper link drawn and save successfully", "Circle hyperlink caanot be drawn and saved");
			fna_viewerpage.drawCloudHyperLink(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalityCloudHyperLinkSameFolderDiffFile(markupname, filepath1),
					"Linked file is opene in new tab and Cloud hyperlink working properly for same folder and diff file ",
					"Cloud hyperlink not working properly for same folder and diff file");
			fna_viewerpage.clickDefaultFileForCircleHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_011(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder(Parent-->Child) different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 15, enabled = true, description = "TC_011(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder(Parent-->Child) different file and working properly.")
	public void verifySquareHyperLinkDiffFolderParentAndChildDiffFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_011(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder(Parent-->Child) different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname3");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
		//	Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinkParentChildFolder(markupname, filepath1),
				//	"Square hyper link drawn and save successfully for diff folder(PArent-->Child)",
					//"Square hyperlink caanot be drawn and saved for diff folder(Parent-->Child)");
			
			fna_viewerpage.drawAndSaveSquareHyperLinkParentChildFolder(markupname, filepath1);
			Log.assertThat(
					fna_viewerpage.VerifyfunctionalitySquareHyperLinkParentCHildFolderDiffFile(markupname, filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for diff  folder(Parent-->Child) and diff file",
					"Square hyper link is not working for for diff  folder(Parent-->Child) and diff file");

			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_012(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder(Child-->Parent) different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 16, enabled = true, description = "Verify whether Square Hyperlink getting saved properly within diff folder(Child-->Parent) different file and working properly.")
	public void verifySquareHyperLinkDiffFolderChildAndParentDiffFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_012(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder(Child-->Parent) different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinkChildParentFolder(markupname, filepath1),
				//	"Square hyper link drawn and save successfully for diff folder(Child-->Parent)",
				//	"Square hyperlink caanot be drawn and saved for diff folder(Parent-->Child)");
			fna_viewerpage.drawAndSaveSquareHyperLinkChildParentFolder(markupname, filepath1);
			Log.assertThat(
					fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderChildParentDiffFile(markupname,
							filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for diff  folder(Child-->Parent) and diff file",
					"Square hyper link is not working for for diff  folder(Child-->Parent) and diff file");
			fna_viewerpage.clickDefaultFileForEname();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	
	/**
	 * TC_013(Viewer):Verify whether Square Hyperlink getting saved properly and
	 * working for multi page file
	 * 
	 * @throws Exception
	 * Modified By Trinanjwan | 26-November-2018
	 * 
	 */
	@Test(priority = 17, enabled = true, description = "TC_013(Viewer):Verify whether Square Hyperlink getting saved properly and working for multi page file")
	public void verifySquareHyperLinkWorkingForMultiPage() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_013(Viewer):Verify whether Square Hyperlink getting saved properly and working for multi page file");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
		//	Log.assertThat(fna_viewerpage.drawAndSaveHyperLinkMultiPage(markupname, filepath1),
		//	"Square hyper link drawn and save successfully for multipage file",
		//	"Square hyperlink caanot be drawn and saved for multipage file");
			fna_viewerpage.drawAndSaveHyperLinkMultiPage(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkMultiPage(markupname, filepath1),
					"The hyperlink destination is verfied successfully",
					"The hyperlink destination is not verfied successfully");
			
			// fna_viewerpage.clickDefaultFileForEname();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_035(Viewer):Verify whether Square Hyperlink getting saved properly and
	 * working for external hyperlink
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 18, enabled =true, description = "TC_035(Viewer):Verify whether Square Hyperlink getting saved properly and working for external hyperlink")
	public void verifySquareHyperLinkWorkingForExternalHyperlink() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_035(Viewer):Verify whether Square Hyperlink getting saved properly and working for external hyperlink");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
		//	Log.assertThat(fna_viewerpage.drawAndSaveHyperLinKforExternalhyperlink(markupname, filepath1),
				//	"Square hyper link drawn and save successfully for external hyperlink",
				//	"Square hyperlink caanot be drawn and saved for external hyperlink");
			fna_viewerpage.drawAndSaveHyperLinKforExternalhyperlink(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperExternalHyperlink(markupname, filepath1),
					"Linked url is opene in new tab and Square hyperlink working properly for external hyperlink",
					"Square hyper link is not working for external hyperlink");
			// fna_viewerpage.clickDefaultFileForEname();
			// fna_viewerpage.VerifyfunctionalitySquareHyperExternalHyperlink(markupname,filepath1);
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_014(Viewer):Verify the Hyperlink functionality working fine with Folder
	 * level(diff folder).
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 19, enabled = true, description = " TC_014(Viewer):Verify the Hyperlink functionality working fine with Folder level.")
	public void verifySquareHyperLinkDiffFolder() throws Exception {
		try {
			Log.testCaseInfo("TC_014(Viewer):Verify the Hyperlink functionality working fine with Folder level.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveHyperlinkWithFolderLevel(markupname, filepath1),
				//	"Square hyper link drawn and save successfully with folder",
					//"Square hyperlink caanot be drawn and saved with folder");
			fna_viewerpage.drawAndSaveHyperlinkWithFolderLevel(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperWithFolderLevel(markupname, filepath1),
					"Hyperlink functionality working fine with folder level",
					"Hyperlink functionality not  working fine with folder level");
			fna_viewerpage.switchToViewerTab();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_045(Viewer):Verify the Hyperlink functionality working fine with sub Folder level
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 20, enabled = true, description = "TC_045(Viewer):Verify the Hyperlink functionality working fine with sub Folder level ")
	public void verifySquareHyperLinkSubFolder() throws Exception {
		try {
			Log.testCaseInfo("TC_045(Viewer):Verify the Hyperlink functionality working fine with sub Folder level ");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname5");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveHyperlinkWithSubFolderLevel(markupname, filepath1),
				//	"Square hyper link drawn and save successfully with folder",
					//"Square hyperlink caanot be drawn and saved with folder");
			fna_viewerpage.drawAndSaveHyperlinkWithSubFolderLevel(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperWithSubFolderLevel(markupname, filepath1),
					"Hyperlink functionality working fine with folder level",
					"Hyperlink functionality not  working fine with folder level");
			fna_viewerpage.switchToViewerTab();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_026(Viewer):Verify whether clock wise 90  Rotation getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 21, enabled = true, description = "TC_026(Viewer):Verify whether clock wise 90  Rotation getting saved properly. ")
	public void verifyRotationSavedProperply() throws Exception {
		try {
			Log.testCaseInfo("TC_026(Viewer):Verify whether clock wise 90  Rotation getting saved properly. ");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("temorotationfolder"));
			String filepath1 = fis1.getAbsolutePath().toString();
			File fistemp12 = new File(PropertyReader.getProperty("runtimescreenshot"));
			String filepathtemp12 = fistemp12.getAbsolutePath().toString();
			Log.assertThat(fna_viewerpage.saveRotationofFile(markupname, filepath1,filepathtemp12),
				"90 degree clockwise rotation of file is successfull",
				"90 degree clockwise rotation of file is failed");
			//fna_viewerpage.saveRotationofFile(markupname, filepath1);
			fna_viewerpage.changeFileToDefault();
			fna_viewerpage.delteScreenShot(filepathtemp12);
			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_006(Viewer):Verify whether Calibrator getting saved properly
	 * 
	 * @throws Exception
	 * Modified by Trinanjwan | 28-11-2018
	 */
	@Test(priority = 22, enabled = true, description = "TC_006(Viewer):Verify whether Calibrator getting saved properly ")
	public void verifyCalibratorGettingSaved() throws Exception {
		try {
			Log.testCaseInfo("TC_006(Viewer):Verify whether Calibrator getting saved properly ");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			File fis1cal = new File(PropertyReader.getProperty("Screenshotcalibrator"));
			String filepath1cal = fis1cal.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.draWaANDSaveCalibrator(markupname, filepath1,filepath1cal),
					"Calibrator is saved properly ", "Calibrator is not  saved properly ");

			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1cal);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	 }

	/**
	 * TC_024(Viewer):Verify Document Export with Calibrator.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 22, enabled = true, description = "TC_024(Viewer):Verify Document Export with Calibrator. ")
	public void verifyDocumentExportWithCalibrator() throws Exception {
		try {
			Log.testCaseInfo("TC_024(Viewer):Verify Document Export with Calibrator.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			File fis1cal = new File(PropertyReader.getProperty("Screenshotcalibrator"));
			String filepath1cal = fis1cal.getAbsolutePath().toString();
			fna_viewerpage.draWaANDSaveCalibrator(markupname, filepath1,filepath1cal);
			String usernamedir = System.getProperty("user.name");
			String downloadpath = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			String filename = PropertyReader.getProperty("Downloadcalibfile");
			Log.assertThat(fna_viewerpage.downLoadCalibrator(downloadpath, filename),
					"Export with Calibrator is working properly",
					"Export with Calibrator is not working properly");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_023(Viewer):Verify Saving the ruler and reference calibrators in a
	 * separate mark ups.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 24, enabled = true, description = "TC_023(Viewer):Verify Saving the ruler and reference calibrators in a separate mark ups. ")
	public void verifyCalibratorandRulerGettingSavedDiffMarkup() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_023(Viewer):Verify Saving the ruler and reference calibrators in a separate mark ups.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname1 = fna_viewerpage.Random_Markupname();
			String markupname2 = fna_viewerpage.Random_SecondMarkupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.saveCalibratorAndRulerSeperatly(markupname1, markupname2, filepath1),
					"Able to save ruler and calibrator in seperate markup ",
					" Not Able to save ruler and calibrator in seperate markup ");

			fna_viewerpage.deletemarkup(markupname2);

			fna_viewerpage.deleteSecondmarkup(markupname1);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_015(Viewer):Verify that the user should be able to draw multiple
	 * calibration ruler on any page of the document.
	 * 
	 * @throws Exception
	 * Modified by Trinanjwan | 30-11-2018
	 */
	@Test(priority = 25, enabled = true, description = "TC_015(Viewer):Verify that the user should be able to draw multiple calibration ruler on any page of the document.")
	public void verifyDrawingMultiPleRularSinglePage() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_015(Viewer):Verify that the user should be able to draw multiple calibration ruler on any page of the document.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			String secondmarkupname = fna_viewerpage.Random_SecondMarkupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.drawMultipleRulerOnsinglePage(markupname, secondmarkupname, filepath1),
					"Able to draw multiple ruler on a single document successfully",
					"Not Able to draw multiple ruler on a single document successfully ");

			fna_viewerpage.deletemarkup(secondmarkupname);

			fna_viewerpage.deleteSecondmarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_018(Viewer):Verify clicking on drawn calibration ruler, user can redefine
	 * the measurement unit.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 26, enabled = true, description = "TC_018(Viewer):Verify clicking on drawn calibration ruler, user can redefine the measurement unit. ")
	public void verifyMeasurementChangeForRuler() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_018(Viewer):Verify clicking on drawn calibration ruler, user can redefine the measurement unit.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname3");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("runtimescreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.changeMeasurementRuler(markupname, filepath1),
					"Calibrator measurement can be edited", "Calibrator measurement cannot be edited after drawing ");

			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_046(Viewer):Verify clicking on recalibrate option of the calibration
	 * ruler, user can delete the drawn ruler.
	 * 
	 * @throws Exception
	 * Modified By Trinanjwan | 29-November-2018
	 */
	@Test(priority = 27, enabled = true, description = " TC_046(Viewer):Verify clicking on recalibrate option of the calibration ruler, user can delete the drawn ruler. ")
	public void verifyMeasurementRulerDelete() throws Exception {
		try {
			Log.testCaseInfo(
					" TC_046(Viewer):Verify clicking on recalibrate option of the calibration ruler, user can delete the drawn ruler.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname3");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.deleteDrawnRuler(filepath1), "Calibrator measurement ruler can be deleted",
					"Calibrator measurement ruler cannot  be deleted");

			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
	     	driver.quit();
		}

	}

	/**
	 * TC_019(Viewer):Verify whether unit of all the measurment calibrator is
	 * getting changed or not with respect to Active calibrator ruler
	 * 
	 * @throws Exception
	 * Modified By Trinanjwan | 03-December-2018
	 */
	@Test(priority = 28, enabled = true, description = "Verify whether unit of all the measurment calibrator is getting changed or not with respect to Active calibrator ruler")
	public void verifyCalibratorReferenceTakingActiveRulerlength() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_019(Viewer):Verify whether unit of all the measurment calibrator is getting changed or not with respect to Active calibrator ruler");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			String secondmarkupname = fna_viewerpage.Random_SecondMarkupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			
			File fis3 = new File(PropertyReader.getProperty("Screenshotpath"));
			String filepath3 = fis1.getAbsolutePath().toString();
			
			
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(
					fna_viewerpage.MultiCalibratorTakingActiveRulerMeasurementSize(markupname, secondmarkupname,
							filepath1, filepath3),
					"All reference calibrator taking length of active ruler successfully",
					"All reference calibrator not taking length of active ruler successfully");

			fna_viewerpage.deletemarkup(secondmarkupname);

			fna_viewerpage.deleteSecondmarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_038(Viewer):Verify whether Square annotations getting saved properly after
	 * zooming
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 29, enabled = true, description = "TC_038(Viewer):Verify whether Square annotations getting saved properly after zooming")
	public void verifySquareAnnotationwithZooming() throws Exception {
		try {
			Log.testCaseInfo("TC_038(Viewer):Verify whether Square annotations getting saved properly after zooming");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("Folderforzoomfile");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawSquareAnnotationOnZooming(markupname, filepath1),
					"Square annotation drawn and saved successfully after zooming",
					"Square annotation cannot be drawn after zooming");

			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	/**
	 * TC_039(Viewer):Verify whether Arrow annotations getting saved properly on
	 * Zooming
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 30, enabled = true, description = "TC_039(Viewer):Verify whether Arrow annotations getting saved properly on Zooming")
	public void verifyArrowAnnotationOnZooming() throws Exception {
		try {
			Log.testCaseInfo("TC_039(Viewer):Verify whether Arrow annotations getting saved properly on Zooming");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("Folderforzoomfile");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawArrowAnnotationOnZooming(markupname, filepath1),
					"Arrow annotation drawn and saved successfully on zooming",
					"Arrow annotation cannot be drawn on zooming");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}
	}

	/**
	 * TC_040(Viewer):Verify whether Tax Callout annotations getting saved properly
	 * on zooming
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 31, enabled = true, description = "TC_040(Viewer):Verify whether Tax Callout annotations getting saved properly on zooming")
	public void verifytextCallOutOnZooming() throws Exception {
		try {
			Log.testCaseInfo("TC_040(Viewer):Verify whether Tax Callout annotations getting saved properly on zooming");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("Folderforzoomfile");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawTextCallOutAnnotationOnZooming(markupname, filepath1),
					"Text Call Out annotation drawn and saved successfully on zooming",
					"Text Call Out annotation cannot be drawn and saved on zooming ");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}
	}

	/**
	 * TC_0041(Viewer):Verify whether FreeHand annotations getting saved properly on
	 * zooming
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 32, enabled = true, description = "TC_0041(Viewer):Verify whether FreeHand annotations getting saved properly on zooming")
	public void verifyFreeHandAnnotationOnZooming() throws Exception {
		try {
			Log.testCaseInfo("TC_0041(Viewer):Verify whether FreeHand annotations getting saved properly on zooming");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("Folderforzoomfile");

			fna_viewerpage.selectFolder_File(foldername);
			Log.assertThat(fna_viewerpage.drawFreeHandAnnotationOnZooming(markupname, filepath1),
					"Free hand annotation drawn and saved successfully on zooming",
					"Free hand annotation cannot be drawn on zooming");

			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}
	}

	/**
	 * TC_042(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * same folder different file and working properly on zooming
	 * 
	 * @throws Exception
	 * Modified By Trinanjwan | 27-November-2018
	 */

	@Test(priority = 33, enabled =true, description = "TC_042(Viewer):Verify whether Square Hyperlink getting saved properly within same folder different file and working properly on zooming")
	public void verifySquareHyperLinkSameFolderDiffFileOnZooming() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_042(Viewer):Verify whether Square Hyperlink getting saved properly within same folder different file and working properly on zooming");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			String foldername = PropertyReader.getProperty("FolderForSquareHyperLinkZoom");

			fna_viewerpage.selectFolder_File(foldername);
			fna_viewerpage.drawAndSaveSquareHyperLinkonzooming(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkSameFolderDiffFileOnZooming(markupname, filepath1),
					"The hyperlink is verfied successfully", "The hyperlink verification got failed");
			fna_viewerpage.returnToSource("(R-1) sum123.pdf");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}
	}

	/**
	 * TC_043(Viewer):Verify that the user should be able to draw multiple
	 * only calibration ruler on any page of the document without any reference calibrator
	 * Modified By Trinanjwan | 30-November-2018
	 */

	@Test(priority = 34, enabled = true, description = "TC_043(Viewer):Verify that the user should be able to draw multiple calibration ruler on any page of the document.")
	public void verifyDrawingMultiCalibrationRuler() throws Exception {

		try {

			Log.testCaseInfo(
					"TC_043(Viewer):Verify that the user should be able to draw multiple calibration ruler on any page of the document.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			String secondmarkupname = fna_viewerpage.Random_SecondMarkupname();
			String thirdmarkup = fna_viewerpage.Random_ThirdMarkupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(
					fna_viewerpage.drawMultipleCalibrationRuler(markupname, secondmarkupname, thirdmarkup, filepath1),
					"Able to save ruler and calibrator in seperate markup ",
					"Not Able to save ruler and calibrator in seperate markup");

			fna_viewerpage.deletemarkup(secondmarkupname);

			fna_viewerpage.deleteSecondmarkup(markupname);
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	/** TC_044(Viewer):Method to draw Combination of ruler and reference calibrator in one markup
	 * 
	 * @throws Exception
	 */
	
	
	@Test(priority = 35, enabled = true, description = "TC_044(Viewer):Method to draw Combination of ruler and reference calibrator in one markup")
	public void verifyDrawingCombinationOfRulerAndCalibrator() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_044(Viewer):Method to draw Combination of ruler and reference calibrator");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			String secondmarkupname = fna_viewerpage.Random_SecondMarkupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.drawCombinationRulerAndcalibrator(markupname,filepath1),
					"Able to draw combination of ruler and reference calibrator",
					"Not Able to draw combination of ruler and reference calibrator ");

			//fna_viewerpage.deletemarkup(secondmarkupname);

			fna_viewerpage.deleteSecondmarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		  driver.quit();
		}
	}
    /**
     * 	TC_47(Viewer):Verify Square hyperlink with noviewable file.
     */
	@Test(priority = 36, enabled = true, description = "TC_47(Viewer):Verify Square hyperlink with noviewable file")
	public void verifySquareHyperLinkWithNonviewablefile() throws Exception {
		try {
			Log.testCaseInfo(
					"	TC_47(Viewer):Verify Square hyperlink with noviewable file");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinktolinknonviewablefile(markupname, filepath1),
				//	"Square hyperlinked succesfully with non veiwable file", "Square hyperlinked can be done with non veiwable file");
			fna_viewerpage.drawAndSaveSquareHyperLinktolinknonviewablefile(markupname, filepath1);
			String usernamedir = System.getProperty("user.name");
			String downloadpath = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			String filename = PropertyReader.getProperty("DownloadfileHyperlink");
			Log.assertThat(fna_viewerpage.VerifyLinkedNonViewableFileDownloaded(markupname, filepath1,downloadpath,filename),
					"Linked non viewable file is downloaded successfully",
					"Linked non viewable file cannot be downloaded");
		
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	
}
	
	/**
	 * TC_055(Viewer):Verify whether counter clock wise 180  Rotation getting saved properly.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 37, enabled = true, description = "TC_055(Viewer):Verify whether counter clock wise 180  Rotation getting saved properly.")
	public void verifyCounterRotationSavedProperply() throws Exception {
		try {
			Log.testCaseInfo("TC_055(Viewer):Verify whether counter clock wise 180  Rotation getting saved properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(fna_viewerpage.rotateCounterClockWise(markupname, filepath1),
					"180 degree counter clockwise rotation of file is successfull",
					"1800 degree counter clockwise rotation of file is failed");
               
			fna_viewerpage.changecounterrotatedfile();
			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

		   driver.quit();
		}

	}
	/**TC_56(Viewer):Method to open a password protected file in viewer
	 * 
	 */
	
	@Test(priority = 38, enabled =true, description = "TC_56(Viewer):Method to open a password protected file in viewer")
	public void verifyPasswordProtectedfileOpenInViewer() throws Exception {
		try {
			Log.testCaseInfo("TC_56(Viewer):Method to open a password protected file in viewer");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			
			
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			// Log.assertThat(fna_viewerpage.
			// saveRotationofFile(markupname,filepath1),"Circle hyper link drawn and save
			// successfully","Circle hyperlink caanot be drawn and saved");
			Log.assertThat(fna_viewerpage.openPasswordProtectedFile(markupname,filepath1),
					"Able to open password protected file in viewer",
					"Not Able to open password protected file in viewer ");

		//	fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);

		}

		catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	
}
	//will be always  true  
	
	// This test case is the duplicate of TC_013 so commenting it as of now
/**	TC_049(Viewer):Verify hyperlink with diff page of same file and verify markup drawn are visible in the destination linked page.
 * 
 
	
	@Test(priority = 39, enabled =true, description = "TC_049(Viewer):Verify hyperlink with diff page of same file and verify markup drawn are visible in the destination linked page.")
	public void verifyMultiPageHyperLinkWithMArkup () throws Exception {
		try {
			Log.testCaseInfo(
					"TC_049(Viewer):Verify hyperlink with diff page of same file and verify markup drawn are visible in the destination linked page.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(fna_viewerpage.drawAndSaveHyperLinkMultiPagewithmarkup(markupname, filepath1),
					"Square hyper link drawn and save successfully for multipage file",
					"Square hyperlink caanot be drawn and saved for multipage file");
			Log.assertThat(fna_viewerpage.multipageLinkWithMarkupDrawing(markupname, filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for multipage file",
					"Square hyper link is not working for for multipage file");
			// fna_viewerpage.clickDefaultFileForEname();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	*/
	
	
	/**
	 * TC_048(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder different file with markup and working properly.
	 * 
	 * @throws Exception
	 * Modified By Trinanjwan | 27-November-2018
	 */

	@Test(priority = 40, enabled = true, description = "TC_048(Viewer):Verify whether Square Hyperlink getting saved properly within   diff folder different file with markup and working properly.")
	public void verifySquareHyperLinkDiffFolderDiffFileWithMarkup() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_048(Viewer):Verify whether Square Hyperlink getting saved properly within   diff folder different file with markup and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffileWithMarkup(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileWithMarkup(markupname, filepath1),
					"The hyperlink is verfied successfully", "The hyperlink verification got failed");
			fna_viewerpage.returnToSource("(R-1) 1 mb.pdf");
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}
	
	/**
	 * TC_053(Viewer):Verify whether Square Hyperlink creating on full screen getting saved properly within
	 * same folder different file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */

	@Test(priority = 41, enabled =true, description = "TC_053(Viewer):Verify whether Square Hyperlink creating on full screen getting saved properly within  same folder different file and working properly.")
	public void verifySquareHyperLinkSameFolderDiffFileOnfullScreen() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_053(Viewer):Verify whether Square Hyperlink creating on full screen getting saved properly within  same folder different file and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinkOmnFullScreen(markupname, filepath1),
				//	"Square hyper link drawn and save successfully on full screen", "Square hyperlink caanot be drawn and saved on full screen");
			fna_viewerpage.drawAndSaveSquareHyperLinkOmnFullScreen(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkSameFolderDiffFileOnFullScreen(markupname, filepath1),
					"Linked file is opene in new tab and Square hyperlink working properly for same folder and diff file when  file is in full screen mode",
					"Square hyper link is not working  when file is in full screen mode for same folder and diff fileas expected");
			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
}
	
	/**
	 * 
	 * TC_052(Viewer):Verify hyperlink change link functionality while creating hyperlink(diff file diff folder)
	 * 
	 * 
	 * @throws Exception
	 * Modified By Trinanjwan | 27-November-2018
	 */

	@Test(priority = 42, enabled = true, description = "TC_052(Viewer):Verify hyperlink change link functionality while creating hyperlink(diff file diff folder)")
	public void verifyChangeHyperLinkFunctionality() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_052(Viewer):Verify hyperlink change link functionality while creating hyperlink(diff file diff folder)");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			fna_viewerpage.drawAndSaveSquareHyperlinkafterChanging(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderDiffFile(markupname, filepath1),
					"The hyperlink change link functionality is verfied successfully",
					"The hyperlink change link functionality verfication got failed");
			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

		driver.quit();
		}

}
	/**
	 * TC_051(Viewer):Verify Delete hyperlink functionality.
	 * 
	 * 
	 * @throws Exception
	 * 
	 * 
	 * 
	 * 
	 */

	@Test(priority = 43, enabled = true, description = " TC_051(Viewer):Verify Delete hyperlink functionality.")
	public void verifyDeleteHyperLinkOption() throws Exception {
		try {
			Log.testCaseInfo(
					" TC_051(Viewer):Verify Delete hyperlink functionality.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname2");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(fna_viewerpage.drawHyperLinkAndDelete(markupname, filepath1),
					"Delete Functioanlity for square hyperlink working fine", "Delete Functioanlity for square hyperlink not working");
			
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			
			
			driver.quit();
		}

}
	/**
	 * TC_054(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder different type of  file(DWG) and working properly.
	 * 
	 * @throws Exception
	 * 
	 */

	@Test(priority = 44, enabled =true, description = "TC_054(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder different type of  file(DWG) and working properly.")
	public void verifySquareHyperLinkWithDiffFolderDiffDWGFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_054(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder different type of  file(DWG) and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffileDWGFile(markupname, filepath1),
					//"Square hyper link drawn and save successfully for diff file type DWG", "Square hyperlink caanot be drawn and saved for DWG type file");
			fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffileDWGFile(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileDWGFile(markupname, filepath1),
					"Linked DWG  file is opene in new tab and Square hyperlink working properly for different folder and different file",
					"Square hyper link is not working for for different folder and different file(DWG) as expected");
			fna_viewerpage.clickDefaultFileForHyperLink();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}


}  
	
/**TC_062(Viewer):Search Document then open the file in viewer
 * 
 */
	

	@Test(priority = 45, enabled = true, description = "TC_062(Viewer):Search Document then open the file in viewer")
	public void verifySearchedDocumentOpenInViewer() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_062(Viewer):Search Document then open the file in viewer");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
	Log.assertThat(fna_viewerpage.searchAndOpenFileInViewer(),"After searching the file,the resultant file can be opened in viewer","After searching the file,the resultant file cannot be opened in viewer");		
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			
			
			driver.quit();
		}

}
	
	/**
	 * TC_060(Viewer):Verify whether Square Hyperlink getting saved properly within
	 * diff folder different type of  file(DWF) with pdf file and working properly.
	 * 
	 * @throws Exception
	 * 
	 */

	@Test(priority = 46, enabled =true, description = "TC_060(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder different type of  file(DWF) and working properly.")
	public void verifySquareHyperLinkWithDiffFolderDiffDWFFFile() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_060(Viewer):Verify whether Square Hyperlink getting saved properly within diff folder different type of  file(DWF) and working properly.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
			String markupname = fna_viewerpage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			//Log.assertThat(fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffileDWFFile(markupname, filepath1),
					//"Square hyper link drawn and save successfully for diff file type DWF", "Square hyperlink caanot be drawn and saved for DWF type file");
			fna_viewerpage.drawAndSaveSquareHyperLinkForDiffFolderDifffileDWFFile(markupname, filepath1);
			Log.assertThat(fna_viewerpage.VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileDWFFile(markupname, filepath1),
					"Linked DWG  file is opene in new tab and Square hyperlink working properly for different folder and different file",
					"Square hyper link is not working for for different folder and different file(DWG) as expected");
			fna_viewerpage.clickDefaultFileForHyperLinkDWFFile();
			fna_viewerpage.deletemarkup(markupname);
			fna_viewerpage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}
	}
    
	/**
	 * TC_0059(Viewer):Document Navigation from Viewer.
	 * 
	 * @throws Exception
	 * 
	 */

	@Test(priority = 11, enabled = true, description = "TC_0059(Viewer):Document Navigation from Viewer.")
	public void verifyDocumentNavigationfromViewer() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_0059(Viewer):Document Navigation from Viewer.");
			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Email1");
			String pWord = PropertyReader.getProperty("Password1");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectioname4");
			fnahomepage.selectcollection(collectionname);
			fna_viewerpage = fnahomepage.clickdocumentsforViewer();
		Log.assertThat(fna_viewerpage.documentNavigation(),"navigation is successfull","navigation is successfull");	
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();

			driver.quit();
		}

	}

	
}