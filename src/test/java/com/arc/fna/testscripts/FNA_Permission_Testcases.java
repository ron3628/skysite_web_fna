package com.arc.fna.testscripts;

import java.io.File;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionTeamsPage;
import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FNA_BatchAttributePage;
import com.arc.fna.pages.FNA_HomeExtraModulePAge;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import org.testng.annotations.Test;


@Listeners(EmailReport.class)
public class FNA_Permission_Testcases 
{
	
	static WebDriver driver;
	LoginPage loginpage;
    FnaHomePage fnahomepage;
    FNAAlbumPage fnaalbumpage;
    FNA_HomeExtraModulePAge fnahomextramodule;
    FNA_BatchAttributePage fnabatchattribute;
    CollectionTeamsPage collectionTeamsPage;
	
	
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) 
    {
		
    	if(browser.equalsIgnoreCase("firefox")) 
    	{
    		File dest = new File("./drivers/win/geckodriver.exe");
    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
    		driver = new FirefoxDriver();
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
    	else if (browser.equalsIgnoreCase("chrome")) 
    	{ 
    		File dest = new File("./drivers/win/chromedriver.exe");
    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            ChromeOptions options = new ChromeOptions();
        	options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
    	else if (browser.equalsIgnoreCase("safari"))
    	{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
			
    	}
   
    	return driver;
    }
	
    /**TC_001(Permission): Verify folder structure display for team level Read Only permission
     * @throws Exception 
     * 
     */
    @Test(priority=0, enabled=true, description= " TC_001(Permission): Verify folder structure display for team level Read Only permission ")
    public void Verify_TeamLevelReadOnlyPermissionFolderStructure() throws Exception 
    {
    	  try 
    	  {
			    Log.testCaseInfo(" TC_001(Permission): Verify folder structure display for team level Read Only permission ");
				    
			    loginpage = new LoginPage(driver).get();
			    String uName=PropertyReader.getProperty("USERID1");
			    String pWord=PropertyReader.getProperty("PASSFA1");
			    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
			    fnahomepage.loginValidation();
				    
			    String collectionname=PropertyReader.getProperty("COLLECTIONPERMISSION");
			    fnahomepage.Create_Collections_New(collectionname);
			    
			    String ParentFolder=PropertyReader.getProperty("PARENTFOLDER");
			    String ChildFolder1=PropertyReader.getProperty("CHILDFOLDER1");
			    String ChildFolder2=PropertyReader.getProperty("CHILDFOLDER2");		    
			    fnahomepage.Add_Folder_Structure(ParentFolder, ChildFolder1, ChildFolder2);
			    
			    String TeamName=PropertyReader.getProperty("TEAMNAMEPERMISSION");
			    String ContactName=PropertyReader.getProperty("TEAMCONTACTNAME");
			    collectionTeamsPage=fnahomepage.enterTeamPage();
			    collectionTeamsPage.Add_team_member(TeamName,ContactName);
			    
			    String PermissionName=PropertyReader.getProperty("TEAMPERMISSION");
			    collectionTeamsPage.AddTeamPermission(TeamName, PermissionName);
			    Log.assertThat(collectionTeamsPage.logout()," Logout successfull " , " Logout failled!!! ", driver);
			    
			    String uName1=PropertyReader.getProperty("USEREMPID");
			    String pWord1=PropertyReader.getProperty("USEREMPPASS");
			    fnahomepage=loginpage.loginWithValidCredential(uName1,pWord1);
			    fnahomepage.loginValidation();
			    
			    fnahomepage.acceptProjectInvitation(collectionname);		    
				fnahomepage.selectcollection(collectionname);
				
				Log.assertThat(fnahomepage.verifyReadFolderPermissionFolderStructure(collectionname, ParentFolder, ChildFolder1, ChildFolder2)," Folders are displayed as desired for Read folder permission successfully " , " Folders are not displayed as desired for Read folder permission!!! ", driver);
			    
				Log.assertThat(fnahomepage.logout()," Logout successfull " , " Logout failed!!! ", driver);
				fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
			    fnahomepage.loginValidation();
			    fnahomepage.navigateToCollectionList();
			 		   			   			
		  }		   
		  catch(Exception e)
	      {
	             e.getCause();
	             Log.exception(e, driver);
		             
	      }
	      finally
	      {
	             Log.endTestCase();
	             driver.quit();
		             
	      }
			 		 
	}  
    
    
    
    
    /**TC_002(Permission): Verify folder structure for team level No Permission
     * @throws Throwable 
     * 
     */
    @Test(priority=1, enabled=true, description= " TC_002(Permission): Verify folder structure for team level No Permission ")
    public void Verify_TeamLevelNoPermissionFolderStructure() throws Throwable 
    {
    	  try 
    	  {
			    Log.testCaseInfo(" TC_002(Permission): Verify folder structure for team level No Permission ");
				    
			    loginpage = new LoginPage(driver).get();
			    String uName=PropertyReader.getProperty("USERID1");
			    String pWord=PropertyReader.getProperty("PASSFA1");
			    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
			    fnahomepage.loginValidation();
				    
			    String collectionname=PropertyReader.getProperty("COLLECTIONPERMISSION");
			    fnahomepage.Create_Collections_New(collectionname);
			    
			    String ParentFolder=PropertyReader.getProperty("PARENTFOLDER");
			    String ChildFolder1=PropertyReader.getProperty("CHILDFOLDER1");
			    String ChildFolder2=PropertyReader.getProperty("CHILDFOLDER2");		    
			    fnahomepage.Add_Folder_Structure(ParentFolder, ChildFolder1, ChildFolder2);
			    
			    String TeamName=PropertyReader.getProperty("TEAMNAMEPERMISSION");
			    String ContactName=PropertyReader.getProperty("TEAMCONTACTNAME");
			    collectionTeamsPage=fnahomepage.enterTeamPage();
			    collectionTeamsPage.Add_team_member(TeamName,ContactName);
			    
			    collectionTeamsPage.AddTeamNoPermission(TeamName);
			    Log.assertThat(collectionTeamsPage.logout()," Logout successfull " , " Logout failled!!! ", driver);
			    
			    String uName1=PropertyReader.getProperty("USEREMPID");
			    String pWord1=PropertyReader.getProperty("USEREMPPASS");
			    fnahomepage=loginpage.loginWithValidCredential(uName1,pWord1);
			    fnahomepage.loginValidation();
			    
			    fnahomepage.acceptProjectInvitation(collectionname);		    
				fnahomepage.selectcollection(collectionname);
				
				Log.assertThat(fnahomepage.verifyNoPermissionFolderStructure(collectionname)," No Folders are displayed for No Permission from team level " , " Folders are displayed for No Permission from team level!!! ", driver);
			    
				Log.assertThat(fnahomepage.logout()," Logout successfull " , " Logout failed!!! ", driver);
				fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
			    fnahomepage.loginValidation();
			    fnahomepage.navigateToCollectionList();
			 
			   			   			
		  }		   
		  catch(Exception e)
	      {
	             e.getCause();
	             Log.exception(e, driver);
		             
	      }
	      finally
	      {
	             Log.endTestCase();
	             driver.quit();
		             
	      }
    	  
	}
    	  

}
