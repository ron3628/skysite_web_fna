package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.MessageboardPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_Messageboard
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	MessageboardPage messageboardPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	/** TC_001 (Message board): Verify Add file After set alert.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Message board): Verify Add file After set alert.")
	public void verifyAddfile_After_Setalert_Messageboard() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (Message board): Verify Add file After set alert.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			driver.switchTo().defaultContent();
			messageboardPage = new MessageboardPage(driver).get();		
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(messageboardPage.Addfile_After_Setalert_Verify_Messageboard(FolderPath), "Add file After set alert in message board Successfull", "Add file After set alert in message board UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Message board): Verify Edit and Delete file After set alert.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Message board): Verify Edit and Delete file After set alert.")
	public void verifyEdit_Delete_file_After_Setalert_Messageboard() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (Message board): Verify Edit and Delete file After set alert.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			messageboardPage = new MessageboardPage(driver).get();
			String FileName= messageboardPage.Random_FileName();
			Log.assertThat(messageboardPage.Edit_Delete_file_After_Setalert_Verify_Messageboard(FileName,CollectionName), "Edit file After set alert in message board Successfull", "Edit file After set alert in message board UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Message board): Verify Add file via turbo after set alert and download in message board.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Message board): Verify Add file via turbo after set alert and download in message board.")
	public void verifyAddfile_ViaTurbo_After_Setalert_Download_Messageboard() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_003 (Message board): Verify Add file via turbo after set alert and download in message board.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			driver.switchTo().defaultContent();
			messageboardPage = new MessageboardPage(driver).get();
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FilePath = Path.getAbsolutePath().toString();	
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(messageboardPage.Addfile_Viaturbo_After_Setalert_Download_Messageboard(FilePath,CollectionName,DownloadPath), "Add file via turbo After set alert and Download in message board Successfull", "Add file via turbo After set alert and Download in message board UnSuccessful", driver);
		
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (Message board): Verify Search with subject in message board.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 *//*
	@Test(priority = 3, enabled = true, description = "TC_004 (Message board): Verify Search with subject in message board.")
	public void verifySearch_withSubject_Messageboard() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_004 (Message board): Verify Search with subject in message board.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection22");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			messageboardPage = new MessageboardPage(driver).get();
			String SearchSubject = PropertyReader.getProperty("SearchwithSubject");				
			Log.assertThat(messageboardPage.Search_WithSubject_Messageboard(SearchSubject), "Search with Subject in message board Successfull", "Search with Subject in message board UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}*/
	
	/** TC_005 (Message board): Verify Download link the add file after set alert in message board section.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (Message board): Verify Download link the add file after set alert in message board section.")
	public void verifyDownloadlink_Addfile_After_setalert_Messageboard() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_005 (Message board): Verify Download link the add file after set alert in message board section.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			driver.switchTo().defaultContent();
			messageboardPage = new MessageboardPage(driver).get();		
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FilePath = Path.getAbsolutePath().toString();
			
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			
			Log.assertThat(messageboardPage.Downloadlink_Addfile_After_Setalert_Messageboard(FilePath,DownloadPath), "Add file After set alert in message board Successfull", "Add file After set alert in message board UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
		
}
