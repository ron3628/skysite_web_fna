package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.AccountTeamPage;
import com.arc.fna.pages.EquipmentPage;
import com.arc.fna.pages.FNA_BatchAttributePage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_Equipment 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	EquipmentPage equipmentPage;
	
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
						
		
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("safebrowsing.enabled", "true"); 
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);		
	
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	 /** TC_001 (Equipment): Verify Equipment Management launch in FNA.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 0, enabled = true, description = "TC_001 (Equipment): Verify Equipment Management launch in FNA.")
    public void verifyEquipment_Management_launch() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_001 (Equipment): Verify Equipment Management launch in FNA.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
      
    
    /** TC_002 (Equipment): Verify addition of an equipment with all the required fields.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 1, enabled = true, description = "TC_002 (Equipment): Verify addition of an equipment with all the required fields.")
    public void verifyEquipment_addition_requiredfields() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_002 (Equipment): Verify addition of an equipment with all the required fields.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 	    	
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    
    
    /** TC_003 (Equipment): Verify latest created equipment is displayed at the top in recently viewed items
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 2, enabled = true, description = " TC_003 (Equipment): Verify latest created equipment is displayed at the top in recently viewed items ")
    public void verifyEquipmentAdditionAndViewInRecentlyViewedListTop() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_003 (Equipment): Verify latest created equipment is displayed at the top in recently viewed items ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();
 			String Serialnumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			
 			String Area = PropertyReader.getProperty("AREAEQUIP");
 			String Floor = PropertyReader.getProperty("FLOOREQUIP");
 			
 			Log.assertThat(equipmentPage.Equipment_Addition(Name,Serialnumber,Building,Area,Floor), " Equipment addition required fields Successfully ", " Equipment addition required fields Unsuccessfully ", driver);
 	    		
 			Log.assertThat(equipmentPage.Latest_Added_Viewed_Equipment_On_Top_Recently_Viewed(Name,Serialnumber,Building,Area,Floor), " Latest added Equipment is displayed on the top of the Recently Viewed list Successfully ", "Latest added Equipment display on top of the Recently Viewed list Failed!!! ", driver);
 	    				
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_004 (Equipment): Verify last viewed equipment is displayed at the top in recently viewed items.
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 3, enabled = true, description = " TC_003 (Equipment): Verify last viewed equipment is displayed at the top in recently viewed items. ")
    public void verifyEquipmentAndViewInRecentlyViewedListTop() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_004 (Equipment): Verify last viewed equipment is displayed at the top in recently viewed items. ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String Name = PropertyReader.getProperty("EQUIPMENTNAME");
    		String SerialNumber = PropertyReader.getProperty("SERIALNOEQUIP");
 			String Building = PropertyReader.getProperty("BUILDINGEQUIP");
 			String Area = PropertyReader.getProperty("AREAEQUIP");
 			String Floor = PropertyReader.getProperty("FLOOREQUIP");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment(Name,SerialNumber,Building,Area,Floor), " Searched Equipment is displayed Successfully ", " Search for the required equipment Failed!!! ", driver);
 			Log.assertThat(equipmentPage.logout(), " Logout Successfull " , " Logout failed " , driver);
 			
 			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			Log.assertThat(equipmentPage.Latest_Added_Viewed_Equipment_On_Top_Recently_Viewed(Name, SerialNumber, Building, Area, Floor), " Latest viewed Equipment is displayed on the top of the Recently Viewed list Successfully ", "Latest viewed Equipment display on top of the Recently Viewed list Failed!!! ", driver);
 	    	
 			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    /** TC_005 (Equipment): Verify tag add to find with items in Equipment details..
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 4, enabled = true, description = "TC_005 (Equipment): Verify tag add to find with items in Equipment details.")
    public void verifyEquipment_Add_Tag() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_005 (Equipment): Verify tag add to find with items in Equipment details.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			String TagName= equipmentPage.Random_TagName();
 			Log.assertThat(equipmentPage.Equipment_Add_Tag(TagName), "Add Tag in Equipment details Successfully", "Add Tag in Equipment details UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_006 (Equipment): Verify Module Search With TagName in Equipment.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 5, enabled = true, description = "TC_006 (Equipment): Verify Module Search With TagName in Equipment.")
    public void verifyEquipment_ModuleSearch_WithTagName() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_006 (Equipment): Verify Module Search With TagName in Equipment.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			String TagName= equipmentPage.Random_TagName();
 			Log.assertThat(equipmentPage.Equipment_Add_Tag(TagName), "Add Tag in Equipment details Successfully", "Add Tag in Equipment details UnSuccessfully", driver);
 			Log.assertThat(equipmentPage.Equipment_ModuleSearch_WithTagName(TagName), "Module search with tag name Successfully", "Module search with tag name UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_007 (Equipment): Verify user can delete equipment from Recently Viewed Equipment list.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 6, enabled = true, description = "TC_007 (Equipment): Verify user can delete equipment from Recently Viewed Equipment list.")
    public void verifyDelete_Equipment_Recently_ViewedEquipmentlist() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_007 (Equipment): Verify user can delete equipment from Recently Viewed Equipment list.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection28");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfull", "select Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			Log.assertThat(equipmentPage.Delete_Equipment_Recently_ViewedEquipmentlist(Name), "Delete Equipment from Recently Viewed Equipment list Successfully", "Delete Equipment from Recently Viewed Equipment list UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_008 (Equipment): Verify user can delete equipment from Equipment details page.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 7, enabled = true, description = "TC_008 (Equipment): Verify user can delete equipment from Equipment details page.")
    public void verifydelete_equipment_Equipment_detailspage() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_008 (Equipment): Verify user can delete equipment from Equipment details page.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection28");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfull", "select Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			Log.assertThat(equipmentPage.Delete_Equipment_detailspage(Name), "Delete Equipment from details page Successfully", "Delete Equipment from details page UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_009 (Equipment): Verify user can Edit equipment name from Equipment details page.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 8, enabled = true, description = "TC_009 (Equipment): Verify user can Edit equipment name from Equipment details page.")
    public void verifyEdit_Equipmentname_Equipment_detailspage() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_009 (Equipment): Verify user can Edit equipment name from Equipment details page.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			String EditName= equipmentPage.Random_EditName();
 			Log.assertThat(equipmentPage.Edit_Equipmentname_Equipment_detailspage(EditName), "Edit name from Equipment details page Successfully", "Edit name from Equipment details page UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    
    
    /** TC_011 (Equipment): Verify The number of recently viewed Maximum equipment is 50.
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 10, enabled = false, description = " TC_011 (Equipment): Verify The number of recently viewed Maximum equipment is 50.")
    public void verifyRecentlyViewedEquipmentCount() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_011 (Equipment): Verify The number of recently viewed Maximum equipment is 50. ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String count = PropertyReader.getProperty("RECENTLYVIEWEDEQPLISTCOUNT");
 			Log.assertThat(equipmentPage.Verify_Recently_Viewed_Equipment_count(count), " Desired count of equipments are displayed in the Recently Viewed equipment list ", " Desired count of equipments are not displayed in the Recently Viewed equipment list", driver);
 			
 			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    } 
    
    
    
    /** TC_012 (Equipment): Verify user can Filter Search with Equipment name.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 11, enabled = true, description = "TC_012 (Equipment): Verify user can Filter Search with Equipment name.")
    public void verifyFilter_Search_Equipmentname() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_012 (Equipment): Verify user can Filter Search with Equipment name.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfull", "select Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String EditName= equipmentPage.Random_EditName();
 			Log.assertThat(equipmentPage.Filter_Search_Equipmentname(EditName), "Filter search with Equipment name Successfully", "Filter search with Equipment name UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_013 (Equipment): Verify Equipment is searchable using Serial Number(Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 12, enabled = true, description = "TC_013 (Equipment): Verify Equipment is searchable using Serial Number(Filter Search).")
    public void verifyFilter_Search_SerialNumber() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_013 (Equipment): Verify Equipment is searchable using Serial Number(Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();    		
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String SerialNum= equipmentPage.Random_SerialNumber();
 			Log.assertThat(equipmentPage.Filter_Search_SerialNumber(SerialNum), "Filter search with Serial Number Successfully", "Filter search with SerialNumber UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
   /** TC_014 (Equipment): Verify Equipment is searchable using Description.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 13, enabled = true, description = "TC_014 (Equipment): Verify Equipment is searchable using Description.( Filter Search).")
    public void verifyFilter_Search_Description() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_014 (Equipment): Verify Equipment is searchable using Description.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Description= equipmentPage.Random_Description();
 			Log.assertThat(equipmentPage.Filter_Search_Description(Description), "Filter search with Description Successfully", "Filter search with Description UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_015 (Equipment): Verify Equipment is searchable using Building.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 14, enabled = true, description = "TC_015 (Equipment): Verify Equipment is searchable using Building.( Filter Search).")
    public void verifyFilter_Search_Building() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_015 (Equipment): Verify Equipment is searchable using Building.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Filter_Search_Building(Building), "Filter search with Building Successfully", "Filter search with Building UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_016 (Equipment): Verify Equipment is searchable using Floor.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 15, enabled = true, description = "TC_016 (Equipment): Verify Equipment is searchable using Floor.( Filter Search).")
    public void verifyFilter_Search_Floor () throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_016 (Equipment): Verify Equipment is searchable using Floor.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Floor= equipmentPage.Random_Floor();
 			Log.assertThat(equipmentPage.Filter_Search_Floor(Floor), "Filter search with Floor Successfully", "Filter search with Floor UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_017 (Equipment): Verify Equipment is searchable using Area.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 16, enabled = true, description = "TC_017 (Equipment): Verify Equipment is searchable using Area.( Filter Search).")
    public void verifyFilter_Search_Area() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_017 (Equipment): Verify Equipment is searchable using Area.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Area= equipmentPage.Random_Area();
 			Log.assertThat(equipmentPage.Filter_Search_Area(Area), "Filter search with Area Successfully", "Filter search with Area UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
    
    /** TC_018 (Equipment): Verify Equipment is searchable using Model.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 17, enabled = true, description = "TC_018 (Equipment): Verify Equipment is searchable using Model.( Filter Search).")
    public void verifyFilter_Search_Model() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_018 (Equipment): Verify Equipment is searchable using Model.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Model= equipmentPage.Random_Model();
 			Log.assertThat(equipmentPage.Filter_Search_Model(Model), "Filter search with Model Successfully", "Filter search with Model UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }     
    
    /** TC_019 (Equipment): Verify Equipment is searchable using Manufacture.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 18, enabled = true, description = "TC_019 (Equipment): Verify Equipment is searchable using Manufacture.( Filter Search).")
    public void verifyFilter_Search_Manufacture () throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_019 (Equipment): Verify Equipment is searchable using Manufacture.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection29");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Manufacture= equipmentPage.Random_Manufacture();
 			Log.assertThat(equipmentPage.Filter_Search_Manufacture(Manufacture), "Filter search with Manufacture  Successfully", "Filter search with Manufacture  UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }     
    
    /** TC_022 (Equipment): Verify Equipment is searchable using Parts.( Filter Search).
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 21, enabled = true, description = "TC_022 (Equipment): Verify Equipment is searchable using Parts.( Filter Search).")
    public void verifyFilter_Search_Parts() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_022 (Equipment): Verify Equipment is searchable using Parts.( Filter Search).");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			String Parts= equipmentPage.Random_Parts();
 			Log.assertThat(equipmentPage.Filter_Search_Parts(Parts), "Filter search with Parts  Successfully", "Filter search with Parts  UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    } 
        
    /** TC_025 (Equipment): User can search equipment using the Name (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 24, enabled = true, description = " TC_025 (Equipment): User can search equipment using the Name (Modular Search) ")
    public void verifyEquipmentSearchByName() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_025 (Equipment): User can search equipment using the Name (Modular Search) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String Name = PropertyReader.getProperty("EQUIPMENTNAME");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment_Name(Name), " Equipment Search with Name workes Successfully ", " Equipment Search with name Failed!!! ", driver);

    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_026 (Equipment): User can search equipment using the Description (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 25, enabled = true, description = " TC_026 (Equipment): User can search equipment using the Description (Modular Search) ")
    public void verifyEquipmentSearchByDesciption() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_026 (Equipment): User can search equipment using the Description (Modular Search) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String descriptionText = PropertyReader.getProperty("DESCRIPTIONTEXTEQUIP");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment_Description(descriptionText), " Equipment Search with Description works Successfully ", " Equipment Search with Description Failed!!! ", driver);

    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_027 (Equipment): User can search equipment using the Manufacturer (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 26, enabled = true, description = " TC_027 (Equipment): User can search equipment using the Manufacturer (Modular Search) ")
    public void verifyEquipmentSearchByManufacturer() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_027 (Equipment): User can search equipment using the Manufacturer (Modular Search) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String Manufacturer = PropertyReader.getProperty("MANUFACTUREREQUIP");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment_Manufacturer(Manufacturer), " Equipment Search with Manufacturer works Successfully ", " Equipment Search with Manufacturer Failed!!! ", driver);

    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_028 (Equipment): User can search equipment using the Model (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 27, enabled = true, description = " TC_028 (Equipment): User can search equipment using the Model (Modular Search) ")
    public void verifyEquipmentSearchByModel() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_028 (Equipment): User can search equipment using the Model (Modular Search) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String Model = PropertyReader.getProperty("MODELEQUIP");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment_Model(Model), " Equipment Search with Model works Successfully ", " Equipment Search with Model Failed!!! ", driver);

    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_029 (Equipment): User can search equipment using the Serial Number (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 28, enabled = true, description = " TC_029 (Equipment): User can search equipment using the Serial Number (Modular Search) ")
    public void verifyEquipmentSearchBySerialNo() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_029 (Equipment): User can search equipment using the Serial Number (Modular Search) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String SerialNo = PropertyReader.getProperty("SERIALNOEQUIP");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment_SerialNo(SerialNo), " Equipment Search with Seial No workes Successfully ", " Equipment Search with Serial No Failed!!! ", driver);

    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_030 (Equipment): User can search equipment using the Type (Modular Search)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 29, enabled = true, description = " TC_030 (Equipment): User can search equipment using the Type (Modular Search) ")
    public void verifyEquipmentSearchByType() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_030 (Equipment): User can search equipment using the Type (Modular Search) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		String Type = PropertyReader.getProperty("TYPEEQUIP");
 			
    		fnaHomePage.selectcollection(collectionname);   		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Search_Equipment_Type(Type), " Equipment Search with Type works Successfully ", " Equipment Search with Type Failed!!! ", driver);

    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_031 (Equipment): Verify add equipment should have a default pre-filled Untitled value as name
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 30, enabled = true, description = " TC_031 (Equipment): Verify add equipment should have a default pre-filled Untitled value as name ")
    public void verifyEquipmentAdditionDefaultUntitledName() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_031 (Equipment): Verify add equipment should have a default pre-filled Untitled value as name ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 					
 			Log.assertThat(equipmentPage.Equipment_Addition_Default_Name(), " Add equipment has pre-filled Untitled value as name and equipment is added successfully ", " Add equipment does not have pre-filled Untitled value as name and equipment is addition failed!!! ", driver);
 	    			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    
    /** TC_038 (Equipment): Verify user can add photo from computer white creating Equipment
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 37, enabled = true, description = " TC_038 (Equipment): Verify user can add photo from computer white creating Equipment ")
    public void verifyEquipmentAdditionWithPhoto() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_038 (Equipment): Verify user can add photo from computer white creating Equipment ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();
 			
 			File fis=new  File(PropertyReader.getProperty("PHOTOFOREQUIPMENT")); 		       	
    		String filepath=fis.getAbsolutePath().toString(); 		          
    		File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	       	    
    		String tempfilepath=fis1.getAbsolutePath().toString();	            
 			
 			Log.assertThat(equipmentPage.Equipment_Addition_With_Photo(Name,filepath,tempfilepath), " Equipment addition with photo Successfully ", " Equipment addition with photo failed!!! ", driver);
 	    					
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }


    
    /** TC_039 (Equipment): Verify user can add video from computer white creating Equipment
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 38, enabled = true, description = " TC_039 (Equipment): Verify user can add video from computer white creating Equipment ")
    public void verifyEquipmentAdditionWithVideo() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_039 (Equipment): Verify user can add video from computer white creating Equipment ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();
 			
 			File fis=new  File(PropertyReader.getProperty("VIDEOFOREQUIPMENT")); 		       	
    		String filepath=fis.getAbsolutePath().toString(); 		          
    		File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	       	    
    		String tempfilepath=fis1.getAbsolutePath().toString();	            
    		
 			
 			Log.assertThat(equipmentPage.Equipment_Addition_With_Video(Name,filepath,tempfilepath), " Equipment addition with video Successfully ", " Equipment addition with video failed!!! ", driver);
 	    					
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    
    /** TC_040 (Equipment): Verify user can add multiple media from computer white creating Equipment(10 photos,5 videos)
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 39, enabled = true, description = " TC_040 (Equipment): Verify user can add multiple media from computer white creating Equipment(10 photos,5 videos) ")
    public void verifyEquipmentAdditionWithMultipleMedia() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_040 (Equipment): Verify user can add multiple media from computer white creating Equipment(10 photos,5 videos) ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();
 			
 			File fis=new  File(PropertyReader.getProperty("MULTIPLEMEDIAFOREQUIPMENT")); 		       	
    		String filepath=fis.getAbsolutePath().toString(); 		          
    		File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	       	    
    		String tempfilepath=fis1.getAbsolutePath().toString();	            
    		
 			Log.assertThat(equipmentPage.Equipment_Addition_With_Multiple_Media(Name,filepath,tempfilepath), " Equipment addition with Multiple Media Successfully ", " Equipment addition with Multiple Media failed!!! ", driver);
 	    					
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}
    	
    }
    
    /** TC_041 (Equipment): Verify user can add parts with name in Equipment details.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 40, enabled = true, description = "TC_041 (Equipment): Verify user can add parts with name in Equipment details.")
    public void verifyAddparts_name_Equipmentdetails() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_041 (Equipment): Verify user can add parts with name in Equipment details.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String CollectionName= fnaHomePage.Random_Collectionname();								
    		Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfull", "Adding Collection UnSuccessful", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Name= equipmentPage.Random_Name();
 			String SerialNumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();
 			Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 			String Parts= equipmentPage.Random_Parts();
 			Log.assertThat(equipmentPage.addparts_name_Equipmentdetails(Parts), "add parts with name in Equipmentdetails Successfully", "add parts with name in Equipmentdetails UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }
    
    /** TC_042 (Equipment): Verify user can delete part in Equipment details page.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 41, enabled = true, description = "TC_042 (Equipment): Verify user can delete part in Equipment details page.")
    public void verifyDeleteparts_Equipmentdetails() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_042 (Equipment): Verify user can delete part in Equipment details page.");
    		loginPage = new LoginPage(driver).get();                
    		String uName = PropertyReader.getProperty("CollectionEquipment");
    		String pWord = PropertyReader.getProperty("Password");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		String Collection_Name = PropertyReader.getProperty("FNASelectCollection30");     								
    		Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
    		driver.switchTo().defaultContent();
 			equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 			String Parts= equipmentPage.Random_Parts();
 			Log.assertThat(equipmentPage.Deleteparts_Equipmentdetails(Parts), "Delete part in Equipment details Successfully", "Delete part in Equipment details UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }
    
 /** TC_043 (Equipment): Verify user can Edit part with name in Equipment details page.
  * Scripted By: Sekhar
  * @throws Exception
  */
 @Test(priority = 42, enabled = true, description = "TC_043 (Equipment): Verify user can Edit part with name in Equipment details page.")
 public void verifyEditpart_Equipmentdetails() throws Exception
 {
	 try
	 {
		 Log.testCaseInfo("TC_043 (Equipment): Verify user can Edit part with name in Equipment details page.");
 		 loginPage = new LoginPage(driver).get();                
 		 String uName = PropertyReader.getProperty("CollectionEquipment");
 		 String pWord = PropertyReader.getProperty("Password");			
 		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
 		 fnaHomePage.loginValidation();
 		 String CollectionName= fnaHomePage.Random_Collectionname();								
 		 Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfully", "Adding Collection UnSuccessfully", driver);
 		 driver.switchTo().defaultContent();
 		 equipmentPage = new EquipmentPage(driver).get();	 			
 		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 		 String Name= equipmentPage.Random_Name();
 		 String SerialNumber= equipmentPage.Random_SerialNumber();
 		 String Building= equipmentPage.Random_Building();
 		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 		 String Parts= equipmentPage.Random_Parts();
 		 String EditParts= equipmentPage.Random_Parts();
 		 Log.assertThat(equipmentPage.Editpart_Equipmentdetails(Parts,EditParts), "Parts edited with name in Equipment details Successfully", "Parts edited with name in Equipment details UnSuccessfully", driver);
 	 }	
	 catch(Exception e)
	 {
		 AnaylizeLog.analyzeLog(driver);
		 e.getCause();
		 Log.exception(e, driver);
	 }
	 finally
	 {
		 Log.endTestCase();
		 driver.quit();
    }	
 }
 
 /** TC_044 (Equipment): Verify user can add linked files from computer in Equipment details page.
  * Scripted By: Sekhar
  * @throws Exception
  */
 @Test(priority = 43, enabled = true, description = "TC_044 (Equipment): Verify user can add linked files from computer in Equipment details page.")
 public void verifyAddlinkedfiles_computer_Equipmentdetails() throws Exception
 {
	 try
	 {
		 Log.testCaseInfo("TC_044 (Equipment): Verify user can add linked files from computer in Equipment details page.");
 		 loginPage = new LoginPage(driver).get();                
 		 String uName = PropertyReader.getProperty("CollectionEquipment");
 		 String pWord = PropertyReader.getProperty("Password");			
 		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
 		 fnaHomePage.loginValidation();
 		 String CollectionName= fnaHomePage.Random_Collectionname();								
 		 Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfully", "Adding Collection UnSuccessfully", driver);
 		 driver.switchTo().defaultContent();
 		 equipmentPage = new EquipmentPage(driver).get();	 			
 		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 		 String Name= equipmentPage.Random_Name();
 		 String SerialNumber= equipmentPage.Random_SerialNumber();
 		 String Building= equipmentPage.Random_Building();
 		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
 		 File Path=new File(PropertyReader.getProperty("FolderPath"));			
 		 String FolderPath = Path.getAbsolutePath().toString();
 		 Log.assertThat(equipmentPage.Addlinkedfiles_computer_Equipmentdetails(FolderPath), "Add linkedfiles from computer in Equipment details Successfully", "Add linkedfiles from computer in Equipment details UnSuccessfully", driver);
	 }	
	 catch(Exception e)
	 {
		 AnaylizeLog.analyzeLog(driver);
		 e.getCause();
		 Log.exception(e, driver);
    }
	 finally
	 {
		 Log.endTestCase();
		 driver.quit();
    }	
 }
 
 /** TC_045 (Equipment): Verify user can add linked files from SKYSITE in Equipment details page.
  * Scripted By: Sekhar
  * @throws Exception
  */
 @Test(priority = 44, enabled = true, description = "TC_045 (Equipment): Verify user can add linked files from SKYSITE in Equipment details page.")
 public void verifyAddlinkedfiles_SKYSITE_Equipmentdetails() throws Exception
 {
	 try
	 {
		 Log.testCaseInfo("TC_045 (Equipment): Verify user can add linked files from SKYSITE in Equipment details page.");
		 loginPage = new LoginPage(driver).get();                
		 String uName = PropertyReader.getProperty("CollectionEquipment");
		 String pWord = PropertyReader.getProperty("Password");			
		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
		 fnaHomePage.loginValidation();
		 String Collection_Name = PropertyReader.getProperty("FNASelectCollection31");     								
 		 Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
 		 driver.switchTo().defaultContent();
		 equipmentPage = new EquipmentPage(driver).get();	 			
		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
		 String Name= equipmentPage.Random_Name();
		 String SerialNumber= equipmentPage.Random_SerialNumber();
		 String Building= equipmentPage.Random_Building();
		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
		 Log.assertThat(equipmentPage.Addlinkedfiles_SKYSITE_Equipmentdetails(), "Add linkedfiles from SKYSITE in Equipment details Successfully", "Add linkedfiles from SKYSITE in Equipment details UnSuccessfully", driver);
	 }	
	 catch(Exception e)
	 {
		 AnaylizeLog.analyzeLog(driver);
		 e.getCause();
		 Log.exception(e, driver);
    }
	 finally
	 {
		 Log.endTestCase();
		 driver.quit();
    }	
 }
 
 /** TC_046 (Equipment): Verify user can remove file from linked files in Equipment details page.
  * Scripted By: Sekhar
  * @throws Exception
  */
 @Test(priority = 45, enabled = true, description = "TC_046 (Equipment): Verify user can remove file from linked files in Equipment details page.")
 public void verifyRemovefile_linkedfiles_Equipmentdetails() throws Exception
 {
	 try
	 {
		 Log.testCaseInfo("TC_046 (Equipment): Verify user can remove file from linked files in Equipment details page.");
		 loginPage = new LoginPage(driver).get();                
		 String uName = PropertyReader.getProperty("CollectionEquipment");
		 String pWord = PropertyReader.getProperty("Password");			
		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
		 fnaHomePage.loginValidation();
		 String Collection_Name = PropertyReader.getProperty("FNASelectCollection32");     								
 		 Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
 		 driver.switchTo().defaultContent();
 		 equipmentPage = new EquipmentPage(driver).get();	 			
 		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
 		 Log.assertThat(equipmentPage.Removefile_linkedfiles_Equipmentdetails(), "Add linkedfiles from computer in Equipment details Successfully", "Add linkedfiles from computer in Equipment details UnSuccessfully", driver);
	 }	
	 catch(Exception e)
	 {
		 AnaylizeLog.analyzeLog(driver);
		 e.getCause();
		 Log.exception(e, driver);
    }
	 finally
	 {
		 Log.endTestCase();
		 driver.quit();
    }	
 }
  
 /** TC_047 (Equipment): Verify user can add single photo from SKYSITE Album while creating Equipment
  * Scripted By: Ranadeep Ghosh
  * @throws Exception
  */
 @Test(priority = 46, enabled = true, description = " TC_047 (Equipment): Verify user can add single photo from SKYSITE Album while creating Equipment ")
 public void verifyEquipmentAdditionWithPhotoFromSkysite() throws Exception
 {
	 try
	 {
		 Log.testCaseInfo("TC_047 (Equipment): Verify user can add single photo from SKYSITE Album while creating Equipment ");
		 
		 loginPage = new LoginPage(driver).get(); 
		 
		 String uName = PropertyReader.getProperty("USERID");
		 String pWord = PropertyReader.getProperty("PASSFA");			
		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
		 fnaHomePage.loginValidation();
		 
		 String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
		 fnaHomePage.selectcollection(collectionname);
		 
		 driver.switchTo().defaultContent();
		 equipmentPage = new EquipmentPage(driver).get();	 			
		 Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
		 
		 String Name= equipmentPage.Random_Name();       
		 String EquipmentAlbum=PropertyReader.getProperty("EQUIPALBUM01");
		 
		 Log.assertThat(equipmentPage.Equipment_Addition_Photo_From_Skysite(Name,EquipmentAlbum), " Equipment addition with photo from SKYSITE Successfully ", " Equipment addition with photo from SKYSITE failed!!! ", driver);
		 
	 }
	 catch(Exception e)
	 {
		 AnaylizeLog.analyzeLog(driver);
		 e.getCause();
		 Log.exception(e, driver);
		 
	 }
	 finally
	 {
		 Log.endTestCase();
		 driver.quit();
	 }	
    	
  }    
    
    /** TC_048 (Equipment): Verify user can add multiple photos from SKYSITE Album while creating Equipment
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 47, enabled = true, description = " TC_048 (Equipment): Verify user can add multiple photos from SKYSITE Album while creating Equipment ")
    public void verifyEquipmentAdditionWithMultiplePhotoFromSkysite() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_048 (Equipment): Verify user can add multiple photos from SKYSITE Album while creating Equipment ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();       
 			String EquipmentAlbum=PropertyReader.getProperty("EQUIPALBUM02");
 			
 			Log.assertThat(equipmentPage.Equipment_Addition_Photo_From_Skysite(Name,EquipmentAlbum), " Equipment addition with multiple photos from SKYSITE is Successfully ", " Equipment addition with multiple photos from SKYSITE failed!!! ", driver);
 	    					
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
           	
    	}
    	
    }
    
    
    
    
    /** TC_049 (Equipment): Verify user should not be able to create equipment with blank Name value
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 48, enabled = true, description = " TC_049 (Equipment): Verify user should not be able to create equipment with blank Name value ")
    public void verifyEquipmentAdditionWithBlankName() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_049 (Equipment): Verify user should not be able to create equipment with blank Name value ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= "    ";       
 			
 			Log.assertThat(equipmentPage.Equipment_Addition_Blank_Name_Validation(Name), " Save button is disabled for blank value entered in Equipment name. Validation passed. ", " Save button is enabled for blank value entered in Equipment name. Validation failed!!! ", driver);
 	    					
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
           	
    	}
    	
    }
 
    
    
    /** TC_050 (Equipment): Verify user should be able to view photo in an equipment
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 49, enabled = true, description = " TC_050 (Equipment): Verify user should be able to view photo in an equipment ")
    public void verifyEquipmentViewPhoto() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_050 (Equipment): Verify user should be able to view photo in an equipment ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();       
 			String EquipmentAlbum=PropertyReader.getProperty("EQUIPALBUM01");
 			
 			Log.assertThat(equipmentPage.Equipment_Addition_Photo_From_Skysite(Name,EquipmentAlbum), " Equipment addition with photo from SKYSITE Successfully ", " Equipment addition with photo from SKYSITE failed!!! ", driver);
 			Log.assertThat(equipmentPage.Equipment_View_Media(Name), " Photo view in an Equipment Successfully ", " Photo view in an Equipment Failed!!! ", driver);
 	    			 			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
           	
    	}
    	
    }
    
    
    
    /** TC_051 (Equipment): Verify user should be able to view video in an equipment
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 50, enabled = true, description = " TC_051 (Equipment): Verify user should be able to view video in an equipment ")
    public void verifyEquipmentViewVideo() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_051 (Equipment): Verify user should be able to view video in an equipment ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();
 			
 			File fis=new  File(PropertyReader.getProperty("VIDEOFOREQUIPMENT")); 		       	
    		String filepath=fis.getAbsolutePath().toString(); 		          
    		File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	       	    
    		String tempfilepath=fis1.getAbsolutePath().toString();	            
    		
 			
 			Log.assertThat(equipmentPage.Equipment_Addition_With_Video(Name,filepath,tempfilepath), " Equipment addition with video Successfully ", " Equipment addition with video failed!!! ", driver);
 	    				
    		Log.assertThat(equipmentPage.Equipment_View_Media(Name), " Video view in an Equipment is Successfully ", " Video view in an Equipment Failed!!! ", driver);
 	    			 			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();           	
    	}    	
    }
    
   
    
    /** TC_052 (Equipment): Verify user should be able to delete media from an equipment
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 51, enabled = true, description = " TC_052 (Equipment): Verify user should be able to delete media from an equipment ")
    public void verifyEquipmentDeleteMedia() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_052 (Equipment): Verify user should be able to delete media from an equipment ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String Name= equipmentPage.Random_Name();
 			String EquipmentAlbum=PropertyReader.getProperty("EQUIPALBUM01");
 			
    		Log.assertThat(equipmentPage.Equipment_Addition_Photo_From_Skysite(Name,EquipmentAlbum), " Equipment addition with photo from SKYSITE Successfully ", " Equipment addition with photo from SKYSITE failed!!! ", driver);
    		
    		Log.assertThat(equipmentPage.Equipment_Delete_Media(), " Equipment media deleted Successfully ", " Equipment media deletion failed!!! ", driver);
 	    	
 					 			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
           	
    	}
    	
    }
    
    
    
    /** TC_053 (Equipment): Verify user should be able to make an added photo as the equipment display picture
     * Scripted By: Ranadeep Ghosh
     * @throws Exception
    */
    @Test(priority = 52, enabled = true, description = " TC_053 (Equipment): Verify user should be able to make an added photo as the equipment display picture ")
    public void verifyDisplayPicture() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo(" TC_053 (Equipment): Verify user should be able to make an added photo as the equipment display picture ");
    		
    		loginPage = new LoginPage(driver).get(); 
    		
    		String uName = PropertyReader.getProperty("USERID");
    		String pWord = PropertyReader.getProperty("PASSFA");			
    		fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		fnaHomePage.loginValidation();
    		
    		String collectionname=PropertyReader.getProperty("COLLECTIONEQUIP");
    		fnaHomePage.selectcollection(collectionname);
    		
    		driver.switchTo().defaultContent();
    		equipmentPage = new EquipmentPage(driver).get();	 			
 			Log.assertThat(equipmentPage.Equipment_Management_launch(), " Equipment Management launch Successfully ", " Equipment Management launch UnSuccessfully ", driver);
 			
 			String EquipmentAlbum=PropertyReader.getProperty("EQUIPALBUM01");		
 			String Name= equipmentPage.Random_Name();
 			String Serialnumber= equipmentPage.Random_SerialNumber();
 			String Building= equipmentPage.Random_Building();			
 			String Area = PropertyReader.getProperty("AREAEQUIP");
 			String Floor = PropertyReader.getProperty("FLOOREQUIP");
 			
 			Log.assertThat(equipmentPage.Equipment_Addition(Name,Serialnumber,Building,Area,Floor), " Equipment addition required fields Successfully ", " Equipment addition required fields Unsuccessfully ", driver);
 	    
 			Log.assertThat(equipmentPage.Equipment_Dislay_Picture_Addition(EquipmentAlbum), " Equipment display picture is added successfully " , " Equipment display picture is addition failed!!! " , driver);
 					 			
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
           	
    	}
    	
    }
    
    /** TC_055 (Equipment): Verify user can information of file name view on linked files in Equipment.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 54, enabled = true, description = "TC_055 (Equipment): Verify user can information of file name view on linked files in Equipment.")
    public void verifyFile_Information_View_Linkedfiles_Equipmentdetails() throws Exception
    {
    	try
    	{
    		 Log.testCaseInfo("TC_055 (Equipment): Verify user can information of file name view on linked files in Equipment.");
    		 loginPage = new LoginPage(driver).get();                
    		 String uName = PropertyReader.getProperty("CollectionEquipment");
    		 String pWord = PropertyReader.getProperty("Password");			
    		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		 fnaHomePage.loginValidation();
    		 String Collection_Name = PropertyReader.getProperty("FNASelectCollection31");     								
     		 Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
     		 driver.switchTo().defaultContent();
    		 equipmentPage = new EquipmentPage(driver).get();	 			
    		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
    		 String Name= equipmentPage.Random_Name();
    		 String SerialNumber= equipmentPage.Random_SerialNumber();
    		 String Building= equipmentPage.Random_Building();
    		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
    		 Log.assertThat(equipmentPage.Addlinkedfiles_SKYSITE_Equipmentdetails(), "Add linkedfiles from SKYSITE in Equipment details Successfully", "Add linkedfiles from SKYSITE in Equipment details UnSuccessfully", driver);
    		 Log.assertThat(equipmentPage.Fileinformation_View_linkedfiles_Equipmentdetails(), "Information of file name Viewed in Equipment details Successfully", "Information of file name Viewed in Equipment details UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }
    
  /** TC_056 (Equipment): Verify user can Delete file from file information in Equipment details page.
   * Scripted By: Sekhar
   * @throws Exception
   */
    @Test(priority = 55, enabled = true, description = "TC_056 (Equipment): Verify user can Delete file from file information in Equipment details page.")
    public void verifyFile_Information_Delete_Linkedfiles_Equipmentdetails() throws Exception
    {
    	try
    	{
    		 Log.testCaseInfo("TC_056 (Equipment): Verify user can Delete file from file information in Equipment details page.");
    		 loginPage = new LoginPage(driver).get();                
    		 String uName = PropertyReader.getProperty("CollectionEquipment");
    		 String pWord = PropertyReader.getProperty("Password");			
    		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		 fnaHomePage.loginValidation();
    		 String Collection_Name = PropertyReader.getProperty("FNASelectCollection33");     								
     		 Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
     		 driver.switchTo().defaultContent();
    		 equipmentPage = new EquipmentPage(driver).get();	 			
    		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
    		 Log.assertThat(equipmentPage.Fileinformation_Delete_linkedfiles_Equipmentdetails(), "Information of file deleted in Equipment details Successfully", "Information of file deleted in Equipment details UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }
    
    
    /** TC_057 (Equipment): Verify user can Download file from linked file information in Equipment.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 56, enabled = true, description = "TC_057 (Equipment): Verify user can Download file from linked file information in Equipment.")
    public void verifyFile_Information_Download_Linkedfiles_Equipmentdetails() throws Exception
    {
    	try
    	{
    		 Log.testCaseInfo("TC_057 (Equipment): Verify user can Download file from linked file information in Equipment.");
    		 loginPage = new LoginPage(driver).get();                
    		 String uName = PropertyReader.getProperty("CollectionEquipment");
    		 String pWord = PropertyReader.getProperty("Password");			
    		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
    		 fnaHomePage.loginValidation();
    		 String Collection_Name = PropertyReader.getProperty("FNASelectCollection31");     								
     		 Log.assertThat(fnaHomePage.selectcollection(Collection_Name), "select Collection Successfully", "select Collection UnSuccessfully", driver);
     		 driver.switchTo().defaultContent();
    		 equipmentPage = new EquipmentPage(driver).get();	 			
    		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
    		 String Name= equipmentPage.Random_Name();
    		 String SerialNumber= equipmentPage.Random_SerialNumber();
    		 String Building= equipmentPage.Random_Building();
    		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
    		 Log.assertThat(equipmentPage.Addlinkedfiles_SKYSITE_Equipmentdetails(), "Add linkedfiles from SKYSITE in Equipment details Successfully", "Add linkedfiles from SKYSITE in Equipment details UnSuccessfully", driver);
    		 String usernamedir=System.getProperty("user.name");
             String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads"; 			
    		 Log.assertThat(equipmentPage.FileDownload_information_linkedfiles_Equipmentdetails(DownloadPath), "Information of file download in Equipment details Successfully", "Information of file download in Equipment details UnSuccessfully", driver);
    	}
    	catch(Exception e)
    	{
    		AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
           	driver.quit();
    	}	
    }
    
    /** TC_058 (Equipment): Verify user can Add file using Add to SKYSITE on linked files and validated file added or not in Files.
     * Scripted By: Sekhar
     * @throws Exception
     */
    @Test(priority = 57, enabled = true, description = "TC_058 (Equipment): Verify user can Add file using Add to SKYSITE on linked files and validated file added or not in Files.")
    public void verifyAddFile_linkedfiles_AddtoSKYSITE_Equipmentdetails() throws Exception
    {
   	 try
   	 {
   		 Log.testCaseInfo("TC_058 (Equipment): Verify user can Add file using Add to SKYSITE on linked files and validated file added or not in Files.");
   		 loginPage = new LoginPage(driver).get();                
   		 String uName = PropertyReader.getProperty("CollectionEquipment");
   		 String pWord = PropertyReader.getProperty("Password");			
   		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
   		 fnaHomePage.loginValidation();
   		 String CollectionName= fnaHomePage.Random_Collectionname();								
   		 Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfully", "Adding Collection UnSuccessfully", driver);
   		 String FolderName= fnaHomePage.Random_Foldername();		
   		 Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
   		 driver.switchTo().defaultContent();
   		 equipmentPage = new EquipmentPage(driver).get();	 			
   		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
   		 String Name= equipmentPage.Random_Name();
   		 String SerialNumber= equipmentPage.Random_SerialNumber();
   		 String Building= equipmentPage.Random_Building();
   		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
   		 File Path=new File(PropertyReader.getProperty("EquipmentFilePath"));			
   		 String FolderPath = Path.getAbsolutePath().toString();
   		 Log.assertThat(equipmentPage.Addlinkedfiles_computer_Equipmentdetails(FolderPath), "Add linkedfiles from computer in Equipment details Successfully", "Add linkedfiles from computer in Equipment details UnSuccessfully", driver);
   		 Log.assertThat(equipmentPage.AddFile_linkedfiles_AddtoSKYSITE_Equipmentdetails(), "Add file using Add to SKYSITE on linkedfiles Successfully", "Add file using Add to SKYSITE on linkedfiles UnSuccessfully", driver);
   	 }		
   	 catch(Exception e)
   	 {
   		 AnaylizeLog.analyzeLog(driver);
   		 e.getCause();
   		 Log.exception(e, driver);
       }
   	 finally
   	 {
   		 Log.endTestCase();
   		 driver.quit();
       }	
    }
    
    /** TC_059 (Equipment): Verify user can Add file using Add to SKYSITE from information on linked files and validated file added or not in Files. 
     * Scripted By: Sekhar
     * @throws Exception
     */
    @Test(priority = 58, enabled = true, description = "TC_059 (Equipment): Verify user can Add file using Add to SKYSITE from information on linked files and validated file added or not in Files.")
    public void verifyAddFile_information_AddtoSKYSITE_Equipmentdetails() throws Exception
    {
   	 try
   	 {
   		 Log.testCaseInfo("TC_059 (Equipment): Verify user can Add file using Add to SKYSITE from information on linked files and validated file added or not in Files.");
   		 loginPage = new LoginPage(driver).get();                
   		 String uName = PropertyReader.getProperty("CollectionEquipment");
   		 String pWord = PropertyReader.getProperty("Password");			
   		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
   		 fnaHomePage.loginValidation();
   		 String CollectionName= fnaHomePage.Random_Collectionname();								
   		 Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfully", "Adding Collection UnSuccessfully", driver);
   		 String FolderName= fnaHomePage.Random_Foldername();		
   		 Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
   		 driver.switchTo().defaultContent();
   		 equipmentPage = new EquipmentPage(driver).get();	 			
   		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
   		 String Name= equipmentPage.Random_Name();
   		 String SerialNumber= equipmentPage.Random_SerialNumber();
   		 String Building= equipmentPage.Random_Building();
   		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
   		 File Path=new File(PropertyReader.getProperty("EquipmentFilePath"));			
   		 String FolderPath = Path.getAbsolutePath().toString();
   		 Log.assertThat(equipmentPage.Addlinkedfiles_computer_Equipmentdetails(FolderPath), "Add linkedfiles from computer in Equipment details Successfully", "Add linkedfiles from computer in Equipment details UnSuccessfully", driver);
   		 Log.assertThat(equipmentPage.AddFile_information_AddtoSKYSITE_Equipmentdetails(), "Add file using Add to SKYSITE from information on linkedfiles Successfully", "Add file using Add to SKYSITE from information on linkedfiles UnSuccessfully", driver);
   	   	
   	 }		
   	 catch(Exception e)
   	 {
   		 AnaylizeLog.analyzeLog(driver);
   		 e.getCause();
   		 Log.exception(e, driver);
       }
   	 finally
   	 {
   		 Log.endTestCase();
   		 driver.quit();
       }	
    }
    
    /** TC_062 (Equipment): Verify user can send link view in equipment details page. 
     * Scripted By: Sekhar
     * @throws Exception
     */
    @Test(priority = 61, enabled = true, description = "TC_062 (Equipment): Verify user can send link view in equipment details page.")
    public void verifySendlink_View_Equipmentdetails() throws Exception
    {
   	 try
   	 {
   		 Log.testCaseInfo("TC_062 (Equipment): Verify user can send link view in equipment details page.");
   		 loginPage = new LoginPage(driver).get();                
   		 String uName = PropertyReader.getProperty("CollectionEquipment");
   		 String pWord = PropertyReader.getProperty("Password");			
   		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
   		 fnaHomePage.loginValidation();
   		 String CollectionName= fnaHomePage.Random_Collectionname();								
   		 Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfully", "Adding Collection UnSuccessfully", driver);
   		 String FolderName= fnaHomePage.Random_Foldername();		
   		 Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
   		 driver.switchTo().defaultContent();
   		 equipmentPage = new EquipmentPage(driver).get();	 			
   		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
   		 String Name= equipmentPage.Random_Name();
   		 String SerialNumber= equipmentPage.Random_SerialNumber();
   		 String Building= equipmentPage.Random_Building();
   		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
   		 Log.assertThat(equipmentPage.sendlinks_View_Equipmentdetails(), "Send link view Successfully", "Send link view UnSuccessfully", driver);
   	 }		
   	 catch(Exception e)
   	 {
   		 AnaylizeLog.analyzeLog(driver);
   		 e.getCause();
   		 Log.exception(e, driver);
       }
   	 finally
   	 {
   		 Log.endTestCase();
   		 driver.quit();
       }	
    }
    
    /** TC_063 (Equipment): Verify user can send link Download in equipment details page. 
     * Scripted By: Sekhar
     * @throws Exception
     */
    @Test(priority = 62, enabled = false, description = "TC_063 (Equipment): Verify user can send link Download in equipment details page.")
    public void verifySendlink_DownloadEquipmentdetails() throws Exception
    {
   	 try
   	 {
   		 Log.testCaseInfo("TC_063 (Equipment): Verify user can send link Download in equipment details page.");
   		 loginPage = new LoginPage(driver).get();                
   		 String uName = PropertyReader.getProperty("CollectionEquipment");
   		 String pWord = PropertyReader.getProperty("Password");			
   		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
   		 fnaHomePage.loginValidation();
   		 String CollectionName= fnaHomePage.Random_Collectionname();								
   		 Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Adding Collection Successfully", "Adding Collection UnSuccessfully", driver);
   		 String FolderName= fnaHomePage.Random_Foldername();		
   		 Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
   		 driver.switchTo().defaultContent();
   		 equipmentPage = new EquipmentPage(driver).get();	 			
   		 Log.assertThat(equipmentPage.Equipment_Management_launch(), "Equipment Management launch Successfully", "Equipment Management launch UnSuccessfully", driver);
   		 String Name= equipmentPage.Random_Name();
   		 String SerialNumber= equipmentPage.Random_SerialNumber();
   		 String Building= equipmentPage.Random_Building();
   		 Log.assertThat(equipmentPage.Equipment_addition_requiredfields(Name,SerialNumber,Building), "Equipment addition required fields Successfully", "Equipment addition required fields UnSuccessfully", driver);
   		 File Path=new File(PropertyReader.getProperty("EquipmentFilePath"));			
 		 String FolderPath = Path.getAbsolutePath().toString();
 		 Log.assertThat(equipmentPage.Addlinkedfiles_computer_Equipmentdetails(FolderPath), "Add linked files from computer in Equipment details Successfully", "Add linkedfiles from computer in Equipment details UnSuccessfully", driver);
 		 String usernamedir=System.getProperty("user.name");
         String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
   		 Log.assertThat(equipmentPage.sendlinks_Download_Equipmentdetails(DownloadPath), "Send link Download Successfully", "Send link Download UnSuccessfully", driver);
   	 }		
   	 catch(Exception e)
   	 {
   		 AnaylizeLog.analyzeLog(driver);
   		 e.getCause();
   		 Log.exception(e, driver);
       }
   	 finally
   	 {
   		 Log.endTestCase();
   		 driver.quit();
       }	
    }
    
}