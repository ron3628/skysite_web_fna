package com.arc.fna.testscripts;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_Feedback {
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	  } 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001&004 (FeedBack): Verify  User able to enter the proper data and max size.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	
	@Test(priority = 0, enabled = true, description = "TC_001&004 (FeedBack): Verify  User able to enter the proper data.")
	public void verifyUserable() throws Exception 
	{
		try
		{
			Log.testCaseInfo("TC_001&004 (FeedBack): Verify  User able to enter the proper data.");			
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation(); 
			File Path=new File(PropertyReader.getProperty("FeedbackPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.enterdata_Maxsize_feedback(FolderPath), "user able to enter the data and max size in Feedback successful with valid credential", "user unable to enter the data and max size in Feedback unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_005&006 (FeedBack): Verify Thank you for the feedback.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	
	@Test(priority = 1, enabled = true, description = "TC_005&006 (FeedBack): Verify Thank you for the feedback.")
	public void verifythanksyou() throws Exception 
	{
		try
		{
			Log.testCaseInfo("TC_005&006 (FeedBack): Verify Thank you for the feedback.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			 fnaHomePage.loginValidation(); 
			Log.assertThat(fnaHomePage.thankyou_feedback(), "Thank you for the Feedback successful with valid credential", "Thank you for the Feedback unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_003 (FeedBack): Verify Attachment files with the special chars.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	
	@Test(priority = 2, enabled = true, description = "TC_003 (FeedBack):  Verify Attachment files with the special chars")
	public void verifyattachmentfile() throws Exception 
	{
		try
		{
			Log.testCaseInfo("TC_003 (FeedBack): Verify Attachment files with the special chars");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			 fnaHomePage.loginValidation(); 
			 File Path=new File(PropertyReader.getProperty("Attachment"));			
			 String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.attachmentfile_feedback(FolderPath), "Attachment files with the special chars successful with valid credential", "Attachment files with the special chars unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (FeedBack): Verify Attachment files with network.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	
	@Test(priority = 3, enabled = true, description = "TC_002 (FeedBack):  Verify Attachment files with network")
	public void verifyattachmentfilenetwork() throws Exception 
	{
		try
		{
			Log.testCaseInfo("TC_002 (FeedBack): Verify Attachment files with network");			
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation(); 			
			Log.assertThat(fnaHomePage.attachmentfile_harddrive_feedback(), "Attachment files with network successful with valid credential", "Attachment files with network unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
}
