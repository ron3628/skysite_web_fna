package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommunicationPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.ModuleSearchPage;
import com.arc.fna.pages.UtilitiesPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_Utilities 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	UtilitiesPage utilitiesPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
						
		
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("safebrowsing.enabled", "true"); 
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);		
	
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (Utilities): Verify user Download the Outlook add-in in Utilities.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Utilities): Verify user Download the Outlook add-in in Utilities.")
	public void verifyDownload_Outlook_Addin_Utilities() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_001 (Utilities): Verify user Download the Outlook add-in in Utilities.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			utilitiesPage = new UtilitiesPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(utilitiesPage.Download_Outlook_Addin_Utilities(DownloadPath), "Outlook add-in download successfull with valid credential", "Outlook add-in download unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_002 (Utilities): Verify user Accelerator Download in Utilities.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Utilities): Verify user Accelerator Download in Utilities.")
	public void verifyAccelerator_TurboDownload_Utilities() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (Utilities): Verify user Accelerator Download in Utilities.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			utilitiesPage = new UtilitiesPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(utilitiesPage.Accelerator_Download_Utilities(DownloadPath), "Accelerator Tubro download successfull with valid credential", "Accelerator Tubro download unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Utilities): Verify user Sync app 64 bit Download in Utilities.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Utilities): Verify user Sync app 64 bit Download in Utilities.")
	public void verifySyncApp64bit_Download_Utilities() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003 (Utilities): Verify user Sync app 64 bit Download in Utilities.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			utilitiesPage = new UtilitiesPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(utilitiesPage.Syncapp64bit_Download_Utilities(DownloadPath), "Sync app 64 bit download successfull with valid credential", "Sync app 64 bit download unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	/** TC_004 (Utilities): Verify user Sync app 32 bit Download in Utilities.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	
	@Test(priority = 3, enabled = true, description = "TC_004 (Utilities): Verify user Sync app 32 bit Download in Utilities.")
	public void verifySyncApp32bit_Download_Utilities() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (Utilities): Verify user Sync app 32 bit Download in Utilities.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			driver.switchTo().defaultContent();
			utilitiesPage = new UtilitiesPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(utilitiesPage.Syncapp32bit_Download_Utilities(DownloadPath), "Sync app 32 bit download successfull with valid credential", "Sync app 32 bit download unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
}