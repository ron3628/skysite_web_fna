package com.arc.fna.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FNA_PrintCartPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.Log;

import atu.testrecorder.exceptions.ATUTestRecorderException;

public class FNA_PrintCart {
	static WebDriver driver;
	LoginPage loginpage;
	FnaHomePage fnahomepage;
	FNA_PrintCartPage fnaprintcart;
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) throws ATUTestRecorderException {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} else if (browser.equalsIgnoreCase("chrome")) {

			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));

		} else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "true"); // To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
    
	

	/**
	 * TC_001(PrintCart):Verify folder can be added to print cart from folder level
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 0, enabled = true, description = "TC_001(PrintCart):Verify folder can be added to print cart from folder level")
	public void verifyFolderAddPrintCart() throws Exception {
		try {

			Log.testCaseInfo("TC_001(PrintCart):Verify folder can be added to print cart from folder level");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
		Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
		fnaprintcart.deleteItemFromCart();
		}
	

	catch (Exception e) {
		e.getCause();
		Log.exception(e, driver);
	} finally {
		Log.endTestCase();

		driver.quit();
	}
	}
	
	
	
	/**
	 * TC_002(PrintCart):Verify file can be added to print cart from folder level
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 1, enabled = true, description = "TC_002(PrintCart):Verify file can be added to print cart from folder level")
	public void verifyFileAdditiontoPrintCart() throws Exception 
	{
		try 
		{

			Log.testCaseInfo("TC_002(PrintCart):Verify file can be added to print cart from folder level");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			
			Log.assertThat(fnaprintcart.addFileToPrintCart(),"addition of file to print cart is working fine","addition of file to print cart is not working");       
		    
			fnaprintcart.deleteItemFromCart();
			
		}
		
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}
		
	}
	
	
	
	
	/**
	 * TC_003(PrintCart):Verify delete option for individual in print cart
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 2, enabled = true, description = "TC_003(PrintCart):Verify delete option for individual in print cart")
	public void verifyDeleteOption() throws Exception 
	{
		try 
		{

			Log.testCaseInfo("TC_003(PrintCart):Verify delete option for individual in print cart");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			
			Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			Log.assertThat(fnaprintcart.addFileToPrintCart(),"addition of file to print cart is working fine","addition of file to print cart is not working");       
		
			Log.assertThat(fnaprintcart.deletionOfMultiItem(), "All the items removed and delete option is working","Delete option is not working and all items are not deleted");	
		
		}
	

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
		}
		
	}
	
	
	
	
	/**
	 * TC_005(PrintCart):Verify remove all item remove all item from print cart
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 3, enabled = true, description = "TC_005(PrintCart):Verify remove all item remove all item from print cart")
	public void verifyRemoeAllOption() throws Exception
	{
		try 
		{

			Log.testCaseInfo("TC_005(PrintCart):Verify remove all item remove all item from print cart");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			
			Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			
			Log.assertThat(fnaprintcart.addFileToPrintCart(),"addition of file to print cart is working fine","addition of file to print cart is not working");       
		
			Log.assertThat(fnaprintcart.removeAllButton(), "All item are removed successfully and remove all button is working","All item are removed successfully and remove all button not working");	
		
		}
		
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
		}
		
	}
	
	
	
	/**
	 * TC_006(PrintCart): verify 'back to file' option navigate to last collection selected in document tab
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 4, enabled = true, description = "TC_006(PrintCart): verify 'back to file' option navigate to last collection selected in document tab")
	public void verifyBackToFileOption() throws Exception 
	{
		try 
		{

			Log.testCaseInfo("TC_006(PrintCart): verify 'back to file' option navigate to last collection selected in document tab");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();		
			Log.assertThat(fnaprintcart.addFileToPrintCart(),"addition of file to print cart is working fine","addition of file to print cart is not working");       
			
			Log.assertThat(fnaprintcart.BackToFileOption(collectionname), "Back to file option is working fine","Back to file option is not working");	
			
			fnaprintcart.ClickPrintCartTab();
			
			Log.assertThat(fnaprintcart.removeAllButton(), "All item are removed successfully and remove all button is working","All item are removed successfully and remove all button not working");	
		
		}
	

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
		} 
		
		finally 
		{
			Log.endTestCase();
			driver.quit();
		}
		
	}
	
	
	
	/**
	 * TC_007(PrintCart):Verify file count is also visible for folder added in print cart
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 5, enabled = true, description = "TC_007(PrintCart):Verify file count is also visible for folder added in print cart")
	public void verifyFileCountAlsoVisibleInPrintCart() throws Exception 
	{
		try 
		{

			Log.testCaseInfo("TC_007(PrintCart):Verify file count is also visible for folder added in print cart");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
		    Log.assertThat(fnaprintcart.verifyNoOfFilesforFolder(),"same no of file are prsent in printcart with folder name","unequal no of file are present with folder name in print cart");	
			fnaprintcart.deleteItemFromCart();
		}
		catch (Exception e) 
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		} 
		/*finally 
		{
			Log.endTestCase();
			driver.quit();
		}*/
	}
	
	
	/**
	 * TC_008(PrintCart):Verify no of copies option in print cart for each line item
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 6, enabled = true, description = "TC_008(PrintCart):Verify no of copies option in print cart for each line item")
	public void verifyNoOfCopiesForItemAdded() throws Exception {
		try {

			Log.testCaseInfo("TC_008(PrintCart):Verify no of copies option in print cart for each line item");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
		 //  fnahomepage.verifyPrintCartItemExist();
			int nofofcopies=2;
			for(int i=1;i<=nofofcopies;i++) {
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
		Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			}
		Log.assertThat(fnaprintcart.verifyNoOfCopiesForItemAdded(nofofcopies),"No of added folder is correct in print cart","No of count of added folder is not correct in print cart");	
		    fnaprintcart.deleteItemFromCart();
		}
	

	catch (Exception e) {
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	} finally {
		Log.endTestCase();

		driver.quit();
	}
	}
	/**
	 * TC_009(PrintCart):Verify the new delivery address  in print cart
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 7, enabled = true, description = "TC_009(PrintCart):Verify the new delivery address  in print cart")
	public void verifyNewDeliveryAddress() throws Exception {
		try {

			Log.testCaseInfo("TC_009(PrintCart):Verify the new delivery address  in print cart ");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
		    Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			String firstname=fnaprintcart.Random_FirstName();
			String email=fnaprintcart.Random_Email();
		Log.assertThat(fnaprintcart.deliveryAddress(firstname, email),"Address is changed and reflected in print cart successfully","Address cannot be changed in print cart");	

		    fnaprintcart.deleteItemFromCart();
		}
	

	catch (Exception e) {
		e.getCause();
		Log.exception(e, driver);
	} finally {
		AnaylizeLog.analyzeLog(driver);
		Log.endTestCase();

		driver.quit();
	}
	}
	/**
	 * TC_010(PrintCart):Verify upload is working in "Upload Transmittals or Document distribution addresses" section 
	 * 
	 * @throws Exception
	 * 
	 */
	@Test(priority = 8, enabled = true, description = "Verify upload is working in \"Upload Transmittals or Document distribution addresses\" section ")
	public void verifyUploadOfFileinPrintCart() throws Exception {
		try {

			Log.testCaseInfo("TC_009(PrintCart):Verify the new delivery address  in print cart");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			fnaprintcart.uploadfileinprintcart();
			fnaprintcart.deleteItemFromCart();
		}
	

	catch (Exception e) {
		AnaylizeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
		
	} finally {
	
		Log.endTestCase();
        
		//driver.quit();
	}
	}
	
	
	
	/**TC_012(Print Cart):Verify order placed successfully and visible in my order section
	 * 
	 */
	@Test(priority = 9, enabled = true, description = "TC_012(Print Cart):Verify order placed successfully and visible in my order section")
	public void verifyOrderPlacedSuccessfully() throws Exception 
	{
		try 
		{

			Log.testCaseInfo("TC_012(Print Cart):Verify order placed successfully and visible in my order section");

			loginpage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("emailp");
			String pWord = PropertyReader.getProperty("passwordp");
			fnahomepage = loginpage.loginWithValidCredential(uName, pWord);
			fnahomepage.loginValidation();
			
			String collectionname = PropertyReader.getProperty("Collectionpcart");
			fnahomepage.selectcollection(collectionname);
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			
			Log.assertThat(fnaprintcart.addFolderToPrintCart(),"Folder added to printcart successfully","Folder cannot be added to printcart");	
			
			fnaprintcart = fnahomepage.clickdocumentsforPrintCart();
			
			Log.assertThat(fnaprintcart.addFileToPrintCart(),"addition of file to print cart is working fine","addition of file to print cart is not working");       
			
			String firstname=fnaprintcart.Random_FirstName();
			String email=fnaprintcart.Random_Email();
			
			Log.assertThat(fnaprintcart.deliveryAddress(firstname, email),"Address is changed and reflected in print cart successfully","Address cannot be changed in print cart");	
			
			fnaprintcart.fillDetailsAndConfirmOrder();
			
			Log.assertThat(fnaprintcart.validateOrderPlaced(),"Order in Print Cart is placed successfully and ordr id generated", "Order cannot be placed and order id is not generated");	
			
		}
	

		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
		}
		
	}
	
}
	

