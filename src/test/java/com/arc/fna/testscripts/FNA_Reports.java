package com.arc.fna.testscripts;


import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.ActivityReportPage;
import com.arc.fna.pages.FNA_ReportsPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.GridExportPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;




@Listeners(EmailReport.class)

public class FNA_Reports
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;	
	FNA_ReportsPage fNA_ReportsPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="Ranjan_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	//ranjan new repository
	
	/** TC_001 (PDF Reports): Verify the Reports home page while user is landed first time- no reports 
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (PDF Report): Verify the Reports home page while user is landed first time ")
	public void EmptyCreation_Validation() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (PDF Report):  Verify the Reports home page while user is landed first time- no reports");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();	
			fNA_ReportsPage.Reports();	
			Log.assertThat(fNA_ReportsPage.Element_visible(), "Report Template is Empty ", "Report Template is Not Empty",driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	/** TC_002 (PDF Reports): Verify user is able to add  particular template  and Validate
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (PDF Report): Verify user is able to add  particular template and Validate")
	public void verifyTemplateCreation() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_002 (PDF Report): Verify user is able to add  particular template and Validate");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();	
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
						
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	/** TC_003 (PDF Reports): Verify user is able to Edit Template and Validate
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (PDF Report): Verify user is able to Edit Template and Validate")
	public void verifyEdit_Template() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_003(PDF Report): Verify user is able to Edit Template and Validate");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();	
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.EditTemplate(), "Edit template Verified Successfully", "Edit template Not verified Successfully", driver);
						
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_004 (PDF Reports): Verify user is able to add  template ,And download template and Validate 
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (PDF Report): Verify user is able to add  particular template ,download template and Validate")
	public void verifyTemplateDownload() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_004 (PDF Report): Verify user is able to add  particular template , Download template and Validate");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(fNA_ReportsPage.DownloadFiledownload(Sys_Download_Path), "Download Verified Successfully", "Download Not Verified Successfully",
					driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005(PDF Reports): Verify user is able to Delete template  and Validate 
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (PDF Report): Verify user is able to Delete template  and Validate ")
	public void verifyDeleteTemplate() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_005 (PDF Report): Verify user is able to Delete template  and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.DeleteTemplate(), "Delete Template Verified Successfully", "Delete Template Not Verified Successfully",
					driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_006(PDF Reports): Verify user can  upload a PDF template file and Validation.
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true , description = "TC_006 (PDF Report):Verify user can  upload a PDF template file and Validate ")
	public void UploadTemplate() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_006 (PDF Report): Verify user can  upload a PDF template file and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.UploadTemplate(FolderPath), "Upload template Verified Successfully", "uploade template Not verified Successfully", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_007(PDF Reports): Verify user is able to navigate to Pending review/approval screen .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 6, enabled = true , description = "TC_007 (PDF Report):Verify user can  upload a PDF template file and Validate ")
	public void Pending_Approval_ScreenValidation() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_007 (PDF Report): Verify user can  upload a PDF template file and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.ApprovalScreenValidation(), " Draft ,Pending Approved button Verified Successfully", "Draft ,Pending Approved button Verified Successfully", driver);
			
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	/** TC_008(PDF Reports): Verify the flow for, "No approval required ." .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_008 (PDF Report): Verify the flow for No approval required and Validate ")
	public void NoApprovalrequired_Flow_Validation() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_008(PDF Report):  Verify the flow for No approval required and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.ApprovalSubmit_validation(), "No Approval required flow  Verified Successfully", "No Approval required not flow Verified Successfully", driver);
			
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	
	
	
	/** TC_009(PDF Reports): Verify user can view draft reports tapping on Drafts button at the top to view the draft reports under this template." .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 8, enabled = true, description = "TC_009 (PDF Report): Verify user can view draft reports tapping on Drafts button at the top to view the draft reports under this template.")
	public void DraftReport_Validation() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_009(PDF Report):Verify user can view draft reports tapping on Drafts button at the top to view the draft reports under this template.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.SaveAsDraft_validation(), "Draft Report Verified Successfully", "Draft Report Not verified Successfully", driver);
			
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_010(PDF Reports): Verify the keyword entered for template search can be related to template name, uploader name, tags." .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 9, enabled = true, description = "TC_010 (PDF Report): Verify the keyword entered for template search can be related to template name, uploader name, tags.")
	public void template_search() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_010(PDF Report):Verify the keyword entered for template search can be related to template name, uploader name, tags.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();			
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.TemplateSearch(FolderPath, Template), "Search template Verified Successfully", "Search Template Not verified Successfully", driver);
			
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	

	
	/** TC_011(PDF Reports): Verify after a report has been marked as approved, the status is Approved and the report is no longer editable." .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 10, enabled = true , description = "TC_011 (PDF Report): Verify after a report has been marked as approved, the status is Approved and the report is no longer editable.")
	public void verifyReport_editable() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_011(PDF Report):Verify after a report has been marked as approved, the status is Approved and the report is no longer editable.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.Reportsubmit_Edit_validation(), "Edit Status Verified Successfully", "Edit Status Not verified Successfully", driver);
			
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_012(PDF Reports): Verify user is able to enter the name of the template within the �Template name� textbox" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority =11, enabled = true, description = "TC_012 (PDF Report): Verify user is able to enter the name of the template within the �Template name� textbox")
	public void VerifyTemplate_Editable() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_012(PDF Report):Verify user is able to enter the name of the template within the �Template name� textbox");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.Template_Validation(FolderPath,Template), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
			fnaHomePage.logout();
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_013(PDF Reports): Verify user is able to export reports template .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority =12, enabled = true, description = "TC_013 (PDF Report):Verify user is able to export reports template")
	public void verifyExport_template() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_013(PDF Report):Verify user is able to export reports template");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			final String Template = randomFileName.RandamName();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath, Approver), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			String noonecanapprove = PropertyReader.getProperty("Approval_detail2");
			fNA_ReportsPage.Export_Templates_Validate_validate_Template(Template,noonecanapprove,Sys_Download_Path);
			driver.switchTo().defaultContent();
			Log.assertThat(fnaHomePage.logout(),"Logout verified sucessfully","Logout Not verified sucessfull",driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	
	
	
	/** TC_014(PDF Reports): Verify the flow for, "Anyone can approve."" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 13, enabled = true, description = "TC_014 (PDF Report): Verify the flow for, Anyone can approve. and Validate")
	public void Single_Approve_flow() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_014(PDF Report):  Verify the flow for Anyone can approve. and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();	
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.CreateTemplate_Any_One_Approve(FolderPath,Template), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
			fnaHomePage.logout();
			//final String Template = "IndiaArc24286 ";
			String uName2 = PropertyReader.getProperty("ApproverEmail");
			String pWord2 = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName2,pWord2);
			fnaHomePage.loginValidation();
			String CollectionName1= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName1), "Collection selection Successfully", "Collection selection got Failed",driver);			
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();		
			Log.assertThat(fNA_ReportsPage.Employee_Template_Any_One_Approve(Template),"Approved Status Verified Successfully", "Approved Status Verified Not Verified Sucessfully",driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_015(PDF Reports): Verify duplicate functionality"" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 14, enabled = true, description = "TC_015 (PDF Report):Verify duplicate functionality and Validate")
	public void Duplicate_Report_flow() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_015(PDF Report): Verify duplicate functionality and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();			
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();	
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.CreateTemplate_Any_One_Approve(FolderPath,Template), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
    		Log.assertThat(fNA_ReportsPage.DuplicateReport_validation(Template), "Duplicate function Verified Successfully", "Duplicate function  Not verified Successfully", driver);
			driver.switchTo().defaultContent();
			fnaHomePage.logout();
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_016(PDF Reports): Verify Photo  will be displayed to the user if user had previously attached photos during report creation" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 15, enabled = true , description = "TC_016 (PDF Report):Verify Photo  will be displayed to the user if user had previously attached photos during report creation")
	public void Photo_Tagging() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_016(PDF Report): Verify Photo  will be displayed to the user if user had previously attached photos during report creation");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();			
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();	
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.CreateTemplate_Any_One_Approve(FolderPath,Template), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.AttachPhoto(), "Photo Verified Successfully", "Photo Not verified Successfully", driver);
			driver.switchTo().defaultContent();
			fnaHomePage.logout();
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	/** TC_017(PDF Reports): Verify file section will be displayed to the user if user had previously attached files during report creation" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 16, enabled = true, description = "TC_017 (PDF Report):Verify file section will be displayed to the user if user had previously attached files during report creation")
	public void file_Tagging() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_017(PDF Report): Verify duplicate functionality and Validate ");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();			
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();	
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.CreateTemplate_Any_One_Approve(FolderPath,Template), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
     		Log.assertThat(fNA_ReportsPage.AttachlinkFile(), "Attached File Verified Successfully", "Attached File  Not verified Successfully", driver);
			driver.switchTo().defaultContent();
			fnaHomePage.logout();
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	
	
	/** TC_018(PDF Reports): Verify after a report has been marked as Rejected. The status is listed as Rejected. If a user wants to resubmit, the user will select �Resubmit�" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 17, enabled = true, description = "TC_018 (PDF Report): Verify after a report has been marked as Rejected. The status is listed as Rejected. If a user wants to resubmit, the user will select �Resubmit�")
	public void resubmit_work_flow() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_018(PDF Report): Verify after a report has been marked as Rejected. The status is listed as Rejected. If a user wants to resubmit, the user will select �Resubmit�");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();	
			final String Template = randomFileName.RandamName();
			Log.assertThat(fNA_ReportsPage.CreateTemplate_Any_One_Approve(FolderPath,Template), "Create Template Verified Successfully", "Create Template Not verified Successfully", driver);
			fnaHomePage.logout();
			String uName2 = PropertyReader.getProperty("ApproverEmail");
			String pWord2 = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName2,pWord2);
			fnaHomePage.loginValidation();			
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();		
			fNA_ReportsPage.Employee_Reject_report(Template);
			fnaHomePage.logout();
			//=====Login to Owner Account
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);										
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_019(PDF Reports): Verify user gets option for uploading a new report template" .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 18, enabled = true, description = "TC_019(PDF Report): Verify user gets option for uploading a new report template")
	public void New_ReportUpload_Option() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_019(PDF Report): Verify user gets option for uploading a new report template");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= PropertyReader.getProperty("Static_Collection");								
			Log.assertThat(fnaHomePage.select_collection(CollectionName), "Collection selection Successfully", "Collection selection got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			Log.assertThat(fNA_ReportsPage.newUploadvalidation(), "Upload new report template button Verified Successfully", "Upload new report template button Not verified Successfully", driver);
			fnaHomePage.logout();
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	
	/** TC_020(PDF Reports): Verify all users with the ability to see the report can submit comments at any time." .
	 *  Scripted By: Ranjan P
	 * @throws Exception
	 */
	@Test(priority = 19, enabled = true, description = "TC_020(PDF Report): Verify all users with the ability to see the report can submit comments at any time.")
	public void AllUserCommentValidation() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_020(PDF Report): Verify all users with the ability to see the report can submit comments at any time.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("UserId123");
			String pWord = PropertyReader.getProperty("Password123");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			Log.assertThat(fnaHomePage.Create_Collections(CollectionName), "Collection creation Successfully", "Collection creation got Failed",driver);	
			driver.switchTo().defaultContent();
			fNA_ReportsPage = new FNA_ReportsPage(driver).get();			
			File Path=new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile"));				
			String FolderPath = Path.getAbsolutePath().toString();
			//String Template = randomFileName.RandamName();
			String Approver = PropertyReader.getProperty("Approval_detail2");
			Log.assertThat(fNA_ReportsPage.CreateTemplate(FolderPath,Approver), "create template Verified Successfully", "create template Not verified Successfully", driver);
			Log.assertThat(fNA_ReportsPage.CommentValidation(), " Comment  Verified Successfully", "Comment Not Verified Successfully", driver);
			
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	

	
}