package com.arc.fna.testscripts;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.FnaRecyclebinTabPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_RecylceBinTab {
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	FnaRecyclebinTabPage fnarecyclebintabpage;
	
	
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	/**TC_001(Recyclebin):Verify Manage Recylce bin page landed on clicking recycle bin button
	 * @throws Exception 
	 * 
	 */
	@Test(priority=0,enabled=true,description="Verify Manage Recylce bin page landed on clicking recycle bin button")
	public void verifyRecycleBinPageLanded() throws Exception {
		try {
			    Log.testCaseInfo("TC_001(Recyclebin) : Verify Manage Recylce bin page landed on clicking recycle bin button");
	    	    loginPage = new LoginPage(driver).get();
	    	    String uName=PropertyReader.getProperty("USERID");
	    	    String pWord=PropertyReader.getProperty("PASSFA");
	    	    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
	    	    fnaHomePage.loginValidation();
	    	    fnarecyclebintabpage=fnaHomePage.clickRecyclebin();
	    	    Log.assertThat(fnarecyclebintabpage.RecycleBinPageLanded(),"Manage Recyclebin page is landed successfully","Manage Recyclebin page is landed successfully is not landed");
			   
		
		}
		   catch(Exception e)
	      {
	             e.getCause();
	             Log.exception(e, driver);
	      }
	      finally
	      {
	             Log.endTestCase();
	             driver.quit();
	      }
		
      }

	/**TC_002(Recyclebin):Verify sub folder of collection files on deleting is seen under Recycle bin tab
	 * @throws Exception 
	 * 
	 */
	 @Test(priority=1,enabled=true,description="Verify sub folder of collection files on deleting is seen under Recycle bin tab")
	 public void verifyDeletesubfolderPresentInRecycleBin() throws Exception {
			try {
				
			Log.testCaseInfo("TC_002(Recyclebin) :Verify sub folder of collection files on deleting is seen under Recycle bin tab");
		    loginPage = new LoginPage(driver).get();
		    String uName=PropertyReader.getProperty("USERID");
		    String pWord=PropertyReader.getProperty("PASSFA");
		    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
		    fnaHomePage.loginValidation();
		    String collectionname=PropertyReader.getProperty("Collectioname");
			fnaHomePage.selectcollection(collectionname);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
			Log.assertThat(fnarecyclebintabpage.verifyDeletedFolderInRecycleBin(FolderName),"Deleted folder is visible in recycle bin after deletion","Deleted folder is not visible in recyclebin after deletion");	    
		    	    
			}
		  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
		  }
	 /** TC_003(Recyclebin):Verify deleted file from collection is present in recycle bin
	 * @throws Exception 
	  * 
	  */
	 @Test(priority=2,enabled=true,description="Verify deleted file from collection is present in recycle bin")
	 public void verifyFileDeletedPresetInRecycleBin() throws Exception {
		try {
				 	Log.testCaseInfo("TC_003(Recyclebin) Verify deleted file from collection is present in recycle bin");
		    	    loginPage = new LoginPage(driver).get();
		    	    String uName=PropertyReader.getProperty("USERID");
		    	    String pWord=PropertyReader.getProperty("PASSFA");
		    	    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
		    	    fnaHomePage.loginValidation();
		    	    String collectionname=PropertyReader.getProperty("Collectioname");
					fnaHomePage.selectcollection(collectionname);	
					String FolderName= fnaHomePage.Random_Foldername();		
					Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					//String filepath=PropertyReader.getProperty("FolderPathFile");
					File fis=new  File(PropertyReader.getProperty("FolderPathFile"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		          File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
					fnaHomePage.UploadFileForDeletion(filepath,tempfilepath);
					fnarecyclebintabpage=fnaHomePage.deleteUploadedFileFromDocuments();
			        Log.assertThat(fnarecyclebintabpage.verifyDeletedFileinRecycleBin(FolderName),"Deleted file is availabe in Recycle Bin !!","Deleted file is not  availabe in Recycle Bin");    
		    	    
			 }
			 catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		     }
		
	 }
	 /**TC_005(Recyclebin): Verify restore functionality for deleted folder with file
	 * @throws Exception 
	  * 
	  */
	 @Test(priority=3,enabled=true,description=" Verify restore functionality for deleted folder")
      public void verifyRestoreFunctionalityDeletedfolderwithfile() throws Exception{
          try {
        	  
              Log.testCaseInfo("TC_005(Recyclebin): Verify restore functionality for deleted folder");
        	  loginPage = new LoginPage(driver).get();
		      String uName=PropertyReader.getProperty("USERID");
		      String pWord=PropertyReader.getProperty("PASSFA");
		      fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
		      fnaHomePage.loginValidation();
		      String collectionname=PropertyReader.getProperty("Collectioname");
			  fnaHomePage.selectcollection(collectionname);	
			  String FolderName= fnaHomePage.Random_Foldername();		
		      Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		      File fis=new  File(PropertyReader.getProperty("FolderPathFile"));
		       	String filepath=fis.getAbsolutePath().toString();
		          File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
	       	    String tempfilepath=fis1.getAbsolutePath().toString();
				fnaHomePage.UploadFileForDeletion(filepath,tempfilepath);
		      fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
			  fnaHomePage=fnarecyclebintabpage.restoreDeletedFolder(FolderName);
         	  Log.assertThat(fnaHomePage.restoredFolderPresentInCollectionFilePage(FolderName),"Deleted folder has been restored successfully along with the file deleted","Deleted folder and the file has not been restored and not found under collection list");
        	  
          }
          catch(Exception e)
	      {
	             e.getCause();
	             Log.exception(e, driver);
	      }
	      finally
	      {
	             Log.endTestCase();
	             driver.quit();
	      }
    	  
     }

	 /**TC_004(Recycle):Verify restore functionality for deleted file
		 * @throws Exception 
		  * 
		  */
		 @Test(priority=4,enabled=true,description="Verify restore functionality for deleted file")
		 public void verifyRestoreFunctionalityDeletedFile() throws Exception {
			 try {
			     	Log.testCaseInfo("TC_004(Recyclebin) Verify restore functionality for deleted file");
	 	            loginPage = new LoginPage(driver).get();
	 	            String uName=PropertyReader.getProperty("USERID");
	 	            String pWord=PropertyReader.getProperty("PASSFA");
	 	            fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
	 	            fnaHomePage.loginValidation();
	 	            String collectionname=PropertyReader.getProperty("Collectioname");
				    fnaHomePage.selectcollection(collectionname);	
				    String FolderName= fnaHomePage.Random_Foldername();		
				    Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
				    File fis=new  File(PropertyReader.getProperty("FolderPathFile"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		          File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
					fnaHomePage.UploadFileForDeletion(filepath,tempfilepath);
				    fnarecyclebintabpage=fnaHomePage.deleteUploadedFileFromDocuments();
				    fnaHomePage=fnarecyclebintabpage.restoreDeletedFile(FolderName);
				    Log.assertThat(fnaHomePage.restoredFilePresentInDocumentPage(),"File restored is successfully present in document page","File restored is not present in document page",driver);
			 
		        }
			   catch(Exception e)
		       {
		             e.getCause();
		             Log.exception(e, driver);
		       }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
		 }
		 /**TC_006(Recycle):Verify Single Delete option in recycle bin
			 * @throws Exception 
			  * 
			  */
			 @Test(priority=5,enabled=true,description="Verify Single Delete option in recycle bin")
			 public void verifySingleDeleteOptionInRecycleBin() throws Exception {
				 try {
					 	Log.testCaseInfo("TC_006(Recycle):Verify Single Delete option in recycle bin");
					    loginPage = new LoginPage(driver).get();
					    String uName=PropertyReader.getProperty("USERID");
					    String pWord=PropertyReader.getProperty("PASSFA");
					    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
					    fnaHomePage.loginValidation();
					    String collectionname=PropertyReader.getProperty("Collectioname");
						fnaHomePage.selectcollection(collectionname);	
						String FolderName= fnaHomePage.Random_Foldername();		
						Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
						fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
						Log.assertThat(fnarecyclebintabpage.singleDeleteForDeletedFolder(FolderName),"Folder deleted sucessfully from recycle bin and not found in the list ","Folder not deleted  from recycle bin using delete option ");
				 }
			  catch(Exception e)
			       {
			             e.getCause();
			             Log.exception(e, driver);
			       }
			  finally
			      {
			             Log.endTestCase();
			             driver.quit();
			      }

	 }
			 
			 
  /**TC_007(Recycle):Verify Search with File
  * @throws Exception 
  * 
  */
  @Test(priority=6, enabled=true, description= " Verify Search with File ")
  public void verifySearchWithFile() throws Exception 
  {
		        	   
	  try 
	  {
		        		   	 
		  Log.testCaseInfo(" TC_007(Recycle):Verify Search with File ");
				    	     
		  loginPage = new LoginPage(driver).get();				    	     
		  String uName=PropertyReader.getProperty("USERID");			    	     
		  String pWord=PropertyReader.getProperty("PASSFA");				    	     
		  fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);				    	     
		  fnaHomePage.loginValidation();
				    	     			    	     
		  String collectionname=PropertyReader.getProperty("Collectioname");							 
		  fnaHomePage.selectcollection(collectionname);							 
		  String FolderName= fnaHomePage.Random_Foldername();			  
		  Log.assertThat(fnaHomePage.Adding_Folder(FolderName), " Adding folder Successfull ", " Adding folder UnSuccessful ", driver);						 
		  
		  File fis=new  File(PropertyReader.getProperty("FolderPathFile"));		
		  String filepath=fis.getAbsolutePath().toString();		
		  File fis1=new  File(PropertyReader.getProperty("tempfilepath"));	
		  String tempfilepath=fis1.getAbsolutePath().toString();	 
		  fnaHomePage.UploadFileForDeletion(filepath,tempfilepath);
		  
		  fnarecyclebintabpage=fnaHomePage.deleteUploadedFileFromDocuments();
		  String filename=PropertyReader.getProperty("DeleteFilename");	 
		  
		  Log.assertThat(fnarecyclebintabpage.searchAndVerifyWithFileName(FolderName,filename)," File searched successfully and found in the list "," File searched cannot be find the file in the list ",driver);
							 		        	   
	  }		        	   
	  catch(Exception e)				       
	  {				             
		  e.getCause();				             
		  Log.exception(e, driver);
				       
	  }				      
	  finally				      
	  {			             
		  Log.endTestCase();		
		  driver.quit();
			
	  }
		        	 		      
  }
				 
				 
				 
				 /**TC_008(Recycle):Verify Search with Folder
					 * @throws Exception 
					  * 
					  */
				 @Test(priority=7,enabled=true,description="Verify Search with Folder")
				 public void verifySearchDeleteFolder() throws Exception {
					 try {
						 
						 	
						 Log.testCaseInfo("TC_008(Recycle):Verify Search with Folder");
						    loginPage = new LoginPage(driver).get();
						    String uName=PropertyReader.getProperty("USERID");
						    String pWord=PropertyReader.getProperty("PASSFA");
						    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
						    fnaHomePage.loginValidation();
						    String collectionname=PropertyReader.getProperty("Collectioname");
							fnaHomePage.selectcollection(collectionname);	
							String FolderName= fnaHomePage.Random_Foldername();		
							Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
							fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
					        Log.assertThat(fnarecyclebintabpage.searchAndVerifyWithFolderName(FolderName),"Deleted folder searched successfully and found in the list","Deleted folder searched is not found in the list");
						}
					  catch(Exception e)
				       {
				             e.getCause();
				             Log.exception(e, driver);
				       }
				  finally
				      {
				             Log.endTestCase();
				             driver.quit();
				       }
				}

				 
				 /**TC_009(Recycle):Verify reset functionality in recycle bin tab
				 * @throws Exception 
				  * 
				  */
				 @Test(priority=8,enabled=true,description="Verify reset functionality in recycle bin tab")
				 public void verifyResetOptionInRecycleBin() throws Exception {
					 try {
						    Log.testCaseInfo("TC_009(Recycle):Verify reset functionality in recycle bin tab");
						    loginPage = new LoginPage(driver).get();
						    String uName=PropertyReader.getProperty("USERID");
						    String pWord=PropertyReader.getProperty("PASSFA");
						    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
						    fnaHomePage.loginValidation();
						    String collectionname=PropertyReader.getProperty("Collectioname");
							fnaHomePage.selectcollection(collectionname);	
							String FolderName= fnaHomePage.Random_Foldername();		
							Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
							fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
					        Log.assertThat(fnarecyclebintabpage.searchAndVerifyWithFolderName(FolderName),"Deleted folder searched successfully and found in the list","Deleted folder searched is not found in the list");
						    Log.assertThat(fnarecyclebintabpage.resetOptionInRecycleBin(), " Search  Reset  Functionality is working" , "Search reset functionality is not working ");
					  }
					 catch(Exception e)
				       {
				             e.getCause();
				             Log.exception(e, driver);
				       }
				      finally
				      {
				             Log.endTestCase();
				             driver.quit();
				       }
					 
				}
				 
				       /**TC(010):Verify delete permanently button of setting tab
					 * @throws Exception 
					  * 
					  */
					  @Test(priority=9,enabled=true,description="Verify delete permanently button of setting tab")
					  public void verifyDeletePermanentlybutton() throws Exception {
						  try {
							  
							    Log.testCaseInfo("TC(010):Verify delete permanently button of setting tab");
							    loginPage = new LoginPage(driver).get();
							    String uName=PropertyReader.getProperty("USERID");
							    String pWord=PropertyReader.getProperty("PASSFA");
							    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
							    fnaHomePage.loginValidation();
							    fnarecyclebintabpage=fnaHomePage.clickRecyclebin();
							    fnarecyclebintabpage.selectDeletePermanentOption();
							    String collectionname=PropertyReader.getProperty("Collectioname");
								fnaHomePage.selectcollection(collectionname);	
								String FolderName= fnaHomePage.Random_Foldername();		
								Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
								fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
						        Log.assertThat(fnarecyclebintabpage.verifyfolderdeletedpermanently(FolderName),"Deleted folder not present in recycle bin and deleted permanently","Deleted folder didn't get deleted permanently");
						        fnarecyclebintabpage.selectMovetoRecyclebinbtn();
							  }
						  catch(Exception e)
					       {
					             e.getCause();
					             Log.exception(e, driver);
					       }
					      finally
					      {
					             Log.endTestCase();
					             driver.quit();
					       }
						  
					
					 }
					  /**TC_011(Recycle):Verify Move to recyclebin button in setting tab
						 * @throws Exception 
						   * 
						   */
						  @Test(priority=10,enabled=true,description="Verify Move to recyclebin button in setting tab")
						  public void movetoRecycleBinButton() throws Exception {
							  try {
								  	Log.testCaseInfo("TC_011(Recyclebin) :Verify Move to recyclebin button in setting tab");
								    loginPage = new LoginPage(driver).get();
								    String uName=PropertyReader.getProperty("USERID");
								    String pWord=PropertyReader.getProperty("PASSFA");
								    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
								    fnaHomePage.loginValidation();
								    fnarecyclebintabpage=fnaHomePage.clickRecyclebin();
								    fnarecyclebintabpage.selectMovetoRecyclebinbtn();
								    String collectionname=PropertyReader.getProperty("Collectioname");
									fnaHomePage.selectcollection(collectionname);	
									String FolderName= fnaHomePage.Random_Foldername();		
									Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
									fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
									Log.assertThat(fnarecyclebintabpage.verifyDeletedFolderInRecycleBin(FolderName),"Deleted folder is visible in recycle bin after deletion","Deleted folder is not visible in recyclebin after deletion");	   
								}
							  catch(Exception e)
						       {
						             e.getCause();
						             Log.exception(e, driver);
						       }
						      finally
						      {
						             Log.endTestCase();
						             driver.quit();
						       }
							  
						}
		/**TC_012(Recycle):Verify Delete option in recylebin
		 * @throws Exception 
		 * 
		 */
		  @Test(priority=11,enabled=true,description="TC_012(Recycle):Verify Delete option in recylebin")
		  public void verifyDeleteOptinInRecyclebin() throws Exception {
			try {
									
					Log.testCaseInfo("TC_012(Recycle):Verify Delete option in recylebin");
				    loginPage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
					String pWord=PropertyReader.getProperty("PASSFA");
					fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
					fnaHomePage.loginValidation();
			        String collectionname=PropertyReader.getProperty("Collectioname");
					fnaHomePage.selectcollection(collectionname);	
					String FolderName= fnaHomePage.Random_Foldername();		
					Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
					fnaHomePage.selectcollection(collectionname);
					String FolderName1= fnaHomePage.Random_Foldername();		
					Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					File fis=new  File(PropertyReader.getProperty("FolderPathFile"));
				    String filepath=fis.getAbsolutePath().toString();
				    File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
					 String tempfilepath=fis1.getAbsolutePath().toString();
					  fnaHomePage.UploadFileForDeletion(filepath,tempfilepath);
					  fnarecyclebintabpage=fnaHomePage.deleteUploadedFileFromDocuments();
					 Log.assertThat(fnarecyclebintabpage.selectFolderFileAndDelete(FolderName,FolderName1),"Folder and file selected in recyclebin deleted successfuly using delete option","Folder and file selected in recyclebin not  deleted using delete option",driver);
				}
				 catch(Exception e)
				     {
					       e.getCause();
					      Log.exception(e, driver);
					  }
                finally
				 {
						      Log.endTestCase();
						      driver.quit();
				 }
								  
		  }
		  /**TC_013(Recyclebin):Method to verify collection selected in document tab is selected in recyclebin tab
		   * @throws Exception 
		   * 
		   * 
		   */
	       @Test(priority=12,enabled=true,description="Method to verify collection selected in document tab is selected in recyclebin tab")
	       public void verifyCollectionSelectedinRecyclebin() throws Exception {
	    	   try {
	    		      
	    			Log.testCaseInfo("TC_013(Recyclebin):Method to verify collection selected in document tab is selected in recyclebin tab");
				    loginPage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("USERID");
				    String pWord=PropertyReader.getProperty("PASSFA");
				    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
				    fnaHomePage.loginValidation();
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnaHomePage.selectcollection(collectionname);
				    fnaHomePage.collectionSelected();
				    fnarecyclebintabpage=fnaHomePage.clickRecyclebin();
	    		   Log.assertThat(fnarecyclebintabpage.verifyCollectionSelectedinRecyclebin(collectionname),"Collection selected in Document page is same collection selected in Recyclebin","Collection selected in document page is not selected in recyclebin");
	    		   
	    		   
	    	   }
	    	   catch(Exception e)
			      {
			             e.getCause();
			             Log.exception(e, driver);
			      }
			      finally
			      {
			             Log.endTestCase();
			             driver.quit();
			      }
	 
	       }	
	       /**TC_014(Recycle):Method to verify change in collection in recycle bin tab changes the content
	     * @throws Exception 
	        * 
	        */
             @Test(priority=13,enabled=true,description="Method to verify change in collection in recycle bin tab changes the content") 
             public void verifyConentChangeinRecycleOnChangingCollection() throws Exception {
            	 try {
            		 
            		 Log.testCaseInfo("TC_014(Recycle):Method to verify change in collection in recycle bin tab changes the content");
         		      loginPage = new LoginPage(driver).get();
         		     String uName=PropertyReader.getProperty("USERID");
         		     String pWord=PropertyReader.getProperty("PASSFA");
         		    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
         		    fnaHomePage.loginValidation();
         		    String collectionname1=PropertyReader.getProperty("Collectioname");
         			fnaHomePage.selectcollection(collectionname1);	
         			String Foldername1= fnaHomePage.Random_Foldername();		
         			Log.assertThat(fnaHomePage.Adding_Folder(Foldername1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
         			fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
         			String collectionname2=PropertyReader.getProperty("Collectioname2");
          			fnaHomePage.selectcollection(collectionname2);	
          			String FolderName2= fnaHomePage.Random_Foldername();		
          			Log.assertThat(fnaHomePage.Adding_Folder(FolderName2), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
          			File fis=new  File(PropertyReader.getProperty("FolderPathFile"));
				    String filepath=fis.getAbsolutePath().toString();
				    File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
					 String tempfilepath=fis1.getAbsolutePath().toString();
					  fnaHomePage.UploadFileForDeletion(filepath,tempfilepath);
				   fnarecyclebintabpage=fnaHomePage.deleteUploadedFileFromDocuments();
          		   Log.assertThat(fnarecyclebintabpage.changeCollectionInRecycleBin(collectionname2,FolderName2,filepath,collectionname1,Foldername1),"Collection changed and also the content changed ","Deleted folder from searched collection is not visible in the recycle bin");	    
            		 
            	 }
            	 catch(Exception e)
			      {
			             e.getCause();
			             Log.exception(e, driver);
			      }
			      finally
			      {
			             Log.endTestCase();
			             driver.quit();
			      }
            	 
           }
             /**TC_015(Recycle bin):Verify Search and rest functionality of collection in recyclebin tab
              * @throws Exception 
               * 
               */
                  @Test(priority=14,enabled=true,description="Verify Search and rest functionality of collection in recyclebin tab")
                  public void veriyfSearchAndResetFunctionality() throws Exception {
                 	 try {
                 		 
                 		 Log.testCaseInfo("TC_015(Recycle bin):Verify Search and rest functionality of collection in recyclebin tab");
             		      loginPage = new LoginPage(driver).get();
             		     String uName=PropertyReader.getProperty("USERID");
             		     String pWord=PropertyReader.getProperty("PASSFA");
             		    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
             		    fnaHomePage.loginValidation();
             		    String collectionname1=PropertyReader.getProperty("Collectioname");
             			fnaHomePage.selectcollection(collectionname1);	
             			String Foldername1= fnaHomePage.Random_Foldername();		
             			Log.assertThat(fnaHomePage.Adding_Folder(Foldername1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
             			fnarecyclebintabpage=fnaHomePage.deleteFolderFromCollectionfiles();
             	        Log.assertThat(fnarecyclebintabpage.searchAndResetCollectioninRecycletab(collectionname1), "Reset and search functionality working fine","Reset and search functionality not  working fine");
                 	 }
                 	 catch(Exception e)
     			      {
     			             e.getCause();
     			             Log.exception(e, driver);
     			      }
     			      finally
     			      {
     			             Log.endTestCase();
     			             driver.quit();
     			      }
              
               }
                  
                  
                  
  /**TC_016(Recyclebin):Verify View favourite collection option
  * @throws Exception 
  * 
  */                 
  @Test(priority=15 , enabled=true , description="Verify View favourite collection option")
  public void verifyViewFavouriteCollectionOption() throws Exception 
  {                	
	  try 
	  {                 		
          	Log.testCaseInfo("TC_016(Recyclebin):Verify View favourite collection option");
          	
          	loginPage = new LoginPage(driver).get();
 		    String uName=PropertyReader.getProperty("USERID");
 		    String pWord=PropertyReader.getProperty("PASSFA");
 		    fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
 		    fnaHomePage.loginValidation();
 		     
 		    String CollectionName=PropertyReader.getProperty("FavRecycleBinCreateCollection");		   
		    fnaHomePage.Create_Collections(CollectionName);
		    fnarecyclebintabpage=fnaHomePage.selectcollectionWithFavourite(CollectionName);	
             		              		    
		    Log.assertThat(fnarecyclebintabpage.verifyFavouriteCollection(CollectionName),"favourite collection visible in the list of favourite","favourite collection not  visible in the list of favourite");	    ;
		    
		    fnaHomePage=fnarecyclebintabpage.navigateToCollectionList();
		    fnaHomePage.deleteCollection(CollectionName);
		    
	  }
                  	
                    
	  catch(Exception e)     		      
	  {
		  e.getCause();
          Log.exception(e, driver); 
	  }	                  		      
      		      
	  finally    		      
	  {
      		            
		  Log.endTestCase();     		             
		  driver.quit();
      }
  }

}                 
          	 
          	 
          	 
          	 


